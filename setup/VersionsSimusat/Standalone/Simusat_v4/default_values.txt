! --------------------------------
! Default values for each variable
! --------------------------------
!
! Central body
! ------------
 50                    Body Mass
 1                     Body Heigth
 1                     Body Width
 0                     Body CG
 1                     Body Inertia (Ixx, Iyy, Izz)
 0                     Body Inertia (Iyz, Ixy, Ixz)
 0                     Gravity Boom Mass
 1                     Gravity Boom Distance
!
! Solar panels
! ------------
 1                     Panel Mass
 1                     Panel Length
 1                     Panel Width
!
! Magnetic dipole (because of passive properties)
! -----------------------------------------------
 1.732                 Magnetic moment
!
! Pressure coefficients (Normal)
! ------------------------------
 0.9                   CN +X
 0.9                   CN +Y
 0.9                   CN +Z
 0.81                  CN -X
 0.81                  CN -Y
 0.81                  CN -Z
!
! Pressure coefficients (Tangential)
! ----------------------------------
 0.85                  CT +X
 0.3                   CT +Y
 0.85                  CT +Z
 0.77                  CT -X
 0.27                  CT -Y
 0.77                  CT -Z
!
! Sun radiation coefficients (Reflective)
! ---------------------------------------
 0.9                   CR +X
 0.5                   CR +Y
 0.9                   CR +Z
 0.81                  CR -X
 0.45                  CR -Y
 0.81                  CR -Z
!
! Sun radiation coefficients (Diffuse)
! ------------------------------------
 0.05                  CD +X
 0.2                   CD +Y
 0.05                  CD +Z
 0.04                  CD -X
 0.18                  CD -Y
 0.04                  CD -Z
!
! Magnetic dipole (because of magnetic control system)
! ----------------------------------------------------
 0                     Magnetic moment
!
! Flywheels
! ---------
 0                     Moment of inertia
 0                     Rotational speed
!
! Dampers
! -------
 0                     Mass
 0                     Position at rest
 0                     Spring constant
 0                     Damper constant
!
! Nozzles
! -------
 0                     Control moment
 