﻿Imports System.IO
Imports Tao.OpenGl
Imports Tao.Platform.Windows
Imports System.Math

Public Class frmDisplay

    Dim indexChange As Boolean = True

    Private Sub frmDisplay_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        Dim i As Short
        '
        'frmMain.gl_initialize()
        '
        ' Setting data for attitude
        ' -------------------------
        ' fill combo boxes
        For i = 0 To attsph.getAxisUbound
            cmbPrinAxis.Items.Add(attsph.getAxis(i))
            cmbOptAxis.Items.Add(attsph.getAxis(i))
        Next i
        '
        For i = 0 To attsph.getOrientationUbound
            cmbPrinOrient.Items.Add(attsph.getOrientation(i))
            cmbOptOrient.Items.Add(attsph.getOrientation(i))
        Next i
        '
        cmbPrinAxis.SelectedIndex = 5
        cmbPrinOrient.SelectedIndex = 0
        cmbOptAxis.SelectedIndex = 0
        cmbOptOrient.SelectedIndex = 3
        '
        cmbPrinAxis_SelectedIndexChanged(cmbPrinAxis, New System.EventArgs)
        cmbOptAxis_SelectedIndexChanged(cmbOptAxis, New System.EventArgs)
        '
        chkDayNight.Checked = True
        blnSunTrace = True
        chkSphere.Checked = True
        '
        displayOpened = True
        frmMain.Sim_update()
        simloop = True
        '

        Timerrefresh3d.Interval = 10
        Timerrefresh3d.Enabled = True
        ''
    End Sub

    Private Sub frmshown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        Timerrefresh3d.Interval = 10
        Timerrefresh3d.Enabled = True
    End Sub
    
    Private Sub Timerrefresh3d_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timerrefresh3d.Tick
        frmMain.Sim_update()
        Timerrefresh3d.Enabled = False
    End Sub

    ''' <summary>
    ''' Refresh the View
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub picMapView_Paint1(ByVal sender As Object, ByVal eventArgs As System.Windows.Forms.PaintEventArgs) Handles picMapView.Paint
        '
        ' Update the OpenGLWindow on repaint
        '
        If blnSimulationInitialized = True Then
            'Update

            Select Case selectedView
                Case VIEW_ATTITUDE
                    attsph.gl_Paint()
                Case VIEW_2D
                    gl_draw_2D()
                Case VIEW_3D
                    gl_draw_3D()
            End Select
        End If
        '
    End Sub

    ''' <summary>
    ''' Draw the 2D view
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub gl_draw_2D()
        '
        ' Déclarations
        ' ------------
        Dim intTrackCounter As Short
        Dim dblTrackStep, dblTrackTime As Double
        Dim intTableIndex As Short
        Dim tr_new, tr_old As sngPos
        Dim cosl As Double
        Dim cosd, sind As Double
        Dim dt, cosdt As Double
        Dim dl, cosdl As Double
        Dim Az, trans As Double
        Dim update As New frmMain
        Dim lbFrmOrbit As New frmOrbit
        '

        '
        'Wgl.wglMakeCurrent(User.GetDC(picMapView.Handle), hGLRC)
        Wgl.wglMakeCurrent(m_intptrHdc, hGLRC)

        'il faut recalculer le point de vue avec viewport et perpesctive a chaque itération
        Gl.glPushMatrix()
        Gl.glMatrixMode(Gl.GL_PROJECTION)
        Gl.glLoadIdentity()
        Gl.glViewport(0, 0, picMapView.Width, picMapView.Height)
        Glu.gluPerspective(30, picMapView.Width / picMapView.Height, 0.5, 30000)
        Glu.gluLookAt(0, 0, 20, 0, 0, 0, 0, 1, 0)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glPopMatrix()


        Gl.glLoadIdentity()
        Gl.glOrtho(-180, 180, -90, 90, 1, 10)
        Glu.gluLookAt(0, 0, 5, 0, 0, 0, 0, 1, 0)
        '
        Gl.glMatrixMode(Gl.GL_PROJECTION)
        Gl.glEnable(Gl.GL_TEXTURE_2D)
        Gl.glLoadIdentity()

        Gl.glClear(Gl.GL_COLOR_BUFFER_BIT Or Gl.GL_DEPTH_BUFFER_BIT)
        ' ------------------------------
        ' Planisphere background picture
        ' ------------------------------
        Gl.glPushMatrix()
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, earthtexture)
        Gl.glColor4f(1, 1, 1, 1)
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(-180, -90, 0)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(-180, 90, 0)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(180, 90, 0)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(180, -90, 0)
        Gl.glPopMatrix()
        Gl.glEnd()
        '
        ' ------------------------------
        ' Day/Night sun and moon drawing
        ' ------------------------------
        Dim sun_Geodetic As Lon_Lat
        Dim moon_Geodetic As Lon_Lat
        'Dim sngPoint, sngOldPoint As sngPos
        Dim lat_old, lat, lng As Double
        Dim coslat, lng0 As Double
        '
        ' Sun coordinates
        ' ---------------
        coord_gamma_geodetic(dblSimulationTime, SunPos, sun_Geodetic) ' Longitude et latitude
        coord_gamma_geodetic(dblSimulationTime, MoonPos, moon_Geodetic) ' Longitude et latitude
        '
        ' Day / Night drawing
        ' -------------------
        Dim Opacity_Renamed As Double
        If blnSunTrace = True Then
            '
            Opacity_Renamed = 0.4
            '
            ' Latitude ligne jour/nuit en fct de la longitude
            For lng = -180 To 180
                If sun_Geodetic.lat > 0 Then
                    lng0 = -90 - lng + sun_Geodetic.lon
                Else
                    lng0 = 90 - lng + sun_Geodetic.lon
                End If
                If lng0 > 180 Then lng0 = lng0 - 360
                If lng0 < -180 Then lng0 = lng0 + 360
                coslat = Sqrt(Sin(sun_Geodetic.lat * rad) ^ 2 / (Sin(sun_Geodetic.lat * rad) ^ 2 * Cos(lng0 * rad) ^ 2 + Sin(lng0 * rad) ^ 2))
                If coslat = 1 Then
                    lat = 0
                Else
                    lat = (Atan(-coslat / Sqrt(-coslat * coslat + 1)) + pi / 2) * 180 / pi
                End If
                If lng0 < 0 Then lat = -lat
                If lng <> -180 Then
                    ' Tracé
                    If sun_Geodetic.lat < 0 Then

                        Gl.glPushMatrix()
                        Gl.glBegin((Gl.GL_QUADS))
                        Gl.glColor4f(0, 0, 0, Opacity_Renamed)
                        Gl.glVertex3f(lng - 1, lat_old, 0.005)
                        Gl.glColor4f(0, 0, 0, Opacity_Renamed)
                        Gl.glVertex3f(lng - 1, 90, 0.005)
                        Gl.glColor4f(0, 0, 0, Opacity_Renamed)
                        Gl.glVertex3f(lng, 90, 0.005)
                        Gl.glColor4f(0, 0, 0, Opacity_Renamed)
                        Gl.glVertex3f(lng, lat, 0.005)
                        Gl.glEnd()
                        Gl.glPopMatrix()

                    Else
                        Gl.glPushMatrix()
                        Gl.glBegin((Gl.GL_QUADS))
                        Gl.glColor4f(0, 0, 0, Opacity_Renamed)
                        Gl.glVertex3f(lng - 1, -90, 0.005)
                        Gl.glColor4f(0, 0, 0, Opacity_Renamed)
                        Gl.glVertex3f(lng - 1, lat_old, 0.005)
                        Gl.glColor4f(0, 0, 0, Opacity_Renamed)
                        Gl.glVertex3f(lng, lat, 0.005)
                        Gl.glColor4f(0, 0, 0, Opacity_Renamed)
                        Gl.glVertex3f(lng, -90, 0.005)
                        Gl.glEnd()
                        Gl.glPopMatrix()
                    End If
                End If
                lat_old = lat
            Next
        End If
        '
        '
        ' Moon trace
        ' ----------
        Gl.glPushMatrix()
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, tx_lune)
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glColor4f(1, 1, 1, 1)
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(moon_Geodetic.lon - 3, moon_Geodetic.lat - 3, 0.008)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(moon_Geodetic.lon - 3, moon_Geodetic.lat + 3, 0.008)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(moon_Geodetic.lon + 3, moon_Geodetic.lat + 3, 0.008)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(moon_Geodetic.lon + 3, moon_Geodetic.lat - 3, 0.008)
        Gl.glEnd()
        Gl.glPopMatrix()
        '
        ' Sun trace
        ' ---------
        Gl.glPushMatrix()
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, tx_soleil)
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glColor4f(1, 1, 1, 1)
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(sun_Geodetic.lon - 3, sun_Geodetic.lat - 3, 0.01)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(sun_Geodetic.lon - 3, sun_Geodetic.lat + 3, 0.01)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(sun_Geodetic.lon + 3, sun_Geodetic.lat + 3, 0.01)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(sun_Geodetic.lon + 3, sun_Geodetic.lat - 3, 0.01)
        Gl.glEnd()
        Gl.glPopMatrix()
        '
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, 0) ' Supprime textures
        '
        ' Tracks
        If Satellite.satBTrackSize <> 0 Or Satellite.satFTrackSize <> 0 Then

            intTableIndex = 0
            ' The choice of the track timestep(dblTrackStep) is made to avoid problems in the simulation. 
            ' Possible problems arise because of the time interval and the overwriting of the satBTrackSize/satFTrackSize variable.
            If testOrbit = True Then
                dblTrackStep = Timerrefresh3d.Interval
            Else
                ' Le pas pour le tracé est de 1/48ème d'orbite
                dblTrackStep = ((2 * pi * ((SatOrbit.SemiMajor_Axis ^ 3) / myEarth) ^ 0.5) / 480 / secday)
            End If

            dblLastTrackStep = dblSimulationTime

            For intTrackCounter = -Satellite.satBTrackSize To Satellite.satFTrackSize
                dblTrackTime = dblSimulationTime + intTrackCounter * dblTrackStep
                update_Orbit(dblTrackTime, SatOrbit)
                Kepler_to_PosVel(SatOrbit, SatPos, SatVel)
                coord_gamma_geodetic(dblTrackTime, SatPos, SatGeoPos)
                sngTrackTable(intTableIndex).X = SatGeoPos.lon
                sngTrackTable(intTableIndex).Y = SatGeoPos.lat
                intTableIndex = intTableIndex + 1
            Next

            update_Orbit(dblSimulationTime, SatOrbit)
            Kepler_to_PosVel(SatOrbit, SatPos, SatVel)
            coord_gamma_geodetic(dblSimulationTime, SatPos, SatGeoPos)

        End If '
        ' Trace satellite
        ' ---------------
        For intTableIndex = 1 To (Satellite.satBTrackSize + Satellite.satFTrackSize)

            Gl.glColor3d(CShort(Satellite.satColor And &HFF) / 255, CShort(Satellite.satColor \ &H100 And &HFF) / 255, CShort(Satellite.satColor \ &H10000 And &HFF) / 255)
            '
            tr_old.X = sngTrackTable(intTableIndex - 1).X
            tr_old.Y = sngTrackTable(intTableIndex - 1).Y
            tr_new.X = sngTrackTable(intTableIndex).X
            tr_new.Y = sngTrackTable(intTableIndex).Y
            If System.Math.Abs(tr_old.X - tr_new.X) > 216 Then
                If tr_old.X > 0 Then
                    Gl.glBegin((Gl.GL_LINES))
                    Gl.glVertex3f(tr_old.X, tr_old.Y, 0.015)
                    Gl.glVertex3f(tr_new.X + 360, tr_new.Y, 0.015)
                    Gl.glVertex3f(tr_old.X - 360, tr_old.Y, 0.015)
                    Gl.glVertex3f(tr_new.X, tr_new.Y, 0.015)
                    Gl.glEnd()
                Else
                    Gl.glBegin((Gl.GL_LINES))
                    Gl.glVertex3f(tr_old.X, tr_old.Y, 0.015)
                    Gl.glVertex3f(tr_new.X - 360, tr_new.Y, 0.015)
                    Gl.glVertex3f(tr_old.X + 360, tr_old.Y, 0.015)
                    Gl.glVertex3f(tr_new.X, tr_new.Y, 0.015)
                    Gl.glEnd()
                End If
            Else
                Gl.glBegin((Gl.GL_LINES))
                Gl.glVertex3f(tr_old.X, tr_old.Y, 0.015)
                Gl.glVertex3f(tr_new.X, tr_new.Y, 0.015)
                Gl.glEnd()
            End If
        Next
        '
        ' Cercle de visibilité
        ' --------------------
        If Satellite.satFootprint Then
            '
            Gl.glBindTexture(Gl.GL_TEXTURE_2D, 0)
            cosl = RE / (RE + SatGeoPos.alt)
            cosd = Cos(SatGeoPos.lat * rad)
            sind = Sin(Abs(SatGeoPos.lat) * rad)
            If SatGeoPos.lon > 0 Then trans = 360 Else trans = -360
            '
            For Az = 0 To 360 Step 2
                cosdt = cosl * sind + Sqrt(1 - Pow(cosl, 2)) * cosd * Cos(Az * rad)
                If cosdt > 1 Then cosdt = 1
                dt = Math.Acos(cosdt)
                If dt > pi Then dt = Abs(dt - 2 * pi)
                cosdl = (cosl - sind * Cos(dt)) / (cosd * Sin(dt))
                If cosdl > 1 Then cosdl = 1
                dl = Math.Acos(cosdl)
                If Az = 0 Or Az = 360 Then
                    If Abs(Sin(SatGeoPos.lat * rad)) < cosl Then dl = 0 Else dl = pi
                End If
                tr_new.Y = ((pi / 2) - dt) * deg
                If SatGeoPos.lat < 0 Then tr_new.Y = -tr_new.Y
                If Az <= 180 Then
                    tr_new.X = SatGeoPos.lon + dl * deg
                Else
                    tr_new.X = SatGeoPos.lon - dl * deg
                End If
                If Az <> 0 Then

                    Gl.glColor4f(CShort((Satellite.satColor) And &HFF) / 1024 + 0.75, CShort((Satellite.satColor) \ &H100 And &HFF) / 1024 + 0.75, CShort((Satellite.satColor) \ &H10000 And &HFF) / 1024 + 0.75, 0.2)
                    If Abs(Sin(SatGeoPos.lat * rad)) < cosl Then
                        Gl.glPushMatrix()
                        Gl.glBegin(Gl.GL_TRIANGLES)
                        Gl.glVertex3f(SatGeoPos.lon, SatGeoPos.lat, 0.014)
                        Gl.glVertex3f(tr_old.X, tr_old.Y, 0.01)
                        Gl.glVertex3f(tr_new.X, tr_new.Y, 0.01)
                        Gl.glVertex3f(SatGeoPos.lon - trans, SatGeoPos.lat, 0.014)
                        Gl.glVertex3f(tr_old.X - trans, tr_old.Y, 0.01)
                        Gl.glVertex3f(tr_new.X - trans, tr_new.Y, 0.01)
                        Gl.glEnd()
                        Gl.glPopMatrix()
                    Else
                        Gl.glPushMatrix()
                        Gl.glBegin((Gl.GL_QUADS))
                        If SatGeoPos.lat > 0 Then
                            Gl.glVertex3f(tr_new.X, 90, 0.01)
                            Gl.glVertex3f(tr_old.X, 90, 0.01)
                            Gl.glVertex3f(tr_old.X, tr_old.Y, 0.01)
                            Gl.glVertex3f(tr_new.X, tr_new.Y, 0.01)
                            Gl.glVertex3f(tr_new.X - trans, 90, 0.01)
                            Gl.glVertex3f(tr_old.X - trans, 90, 0.01)
                            Gl.glVertex3f(tr_old.X - trans, tr_old.Y, 0.01)
                            Gl.glVertex3f(tr_new.X - trans, tr_new.Y, 0.01)
                        Else
                            Gl.glVertex3f(tr_new.X, -90, 0.01)
                            Gl.glVertex3f(tr_old.X, -90, 0.01)
                            Gl.glVertex3f(tr_old.X, tr_old.Y, 0.01)
                            Gl.glVertex3f(tr_new.X, tr_new.Y, 0.01)
                            Gl.glVertex3f(tr_new.X - trans, -90, 0.01)
                            Gl.glVertex3f(tr_old.X - trans, -90, 0.01)
                            Gl.glVertex3f(tr_old.X - trans, tr_old.Y, 0.01)
                            Gl.glVertex3f(tr_new.X - trans, tr_new.Y, 0.01)
                        End If
                        Gl.glEnd()
                        Gl.glPopMatrix()
                    End If
                End If
                tr_old = tr_new
            Next Az
            '
            ' Tracé des cercles visibilité
            For Az = 0 To 360 Step 2
                cosdt = cosl * sind + Sqrt(1 - Pow(cosl, 2)) * cosd * Cos(Az * rad)
                If cosdt > 1 Then cosdt = 1
                dt = Math.Acos(cosdt)
                If dt > pi Then dt = Abs(dt - 2 * pi)
                cosdl = (cosl - sind * Cos(dt)) / (cosd * Sin(dt))
                If cosdl > 1 Then cosdl = 1
                dl = Math.Acos(cosdl)
                If Az = 0 Or Az = 360 Then
                    If Abs(Sin(SatGeoPos.lat * rad)) < cosl Then dl = 0 Else dl = pi
                End If
                tr_new.Y = ((pi / 2) - dt) * deg
                If SatGeoPos.lat < 0 Then tr_new.Y = -tr_new.Y
                If Az <= 180 Then
                    tr_new.X = SatGeoPos.lon + dl * deg
                Else
                    tr_new.X = SatGeoPos.lon - dl * deg
                End If
                If Az <> 0 Then

                    Gl.glPushMatrix()
                    Gl.glColor3d(CShort((Satellite.satColor) And &HFF) / 255, CShort((Satellite.satColor) \ &H100 And &HFF) / 255, CShort((Satellite.satColor) \ &H10000 And &HFF) / 255)
                    Gl.glBegin((Gl.GL_LINES))
                    Gl.glVertex3f(tr_old.X, tr_old.Y, 0.02)
                    Gl.glVertex3f(tr_new.X, tr_new.Y, 0.02)
                    Gl.glVertex3f(tr_old.X - trans, tr_old.Y, 0.02)
                    Gl.glVertex3f(tr_new.X - trans, tr_new.Y, 0.02)
                    Gl.glEnd()
                    Gl.glPopMatrix()
                End If
                tr_old = tr_new
            Next Az
            '
        End If
        '
        ' Satellite avec test eclipse
        ' ---------------------------
        Gl.glPushMatrix()
        If blnEclipse Then
            Gl.glBindTexture(Gl.GL_TEXTURE_2D, sat_nuit)
        Else
            Gl.glBindTexture(Gl.GL_TEXTURE_2D, sat_jour)
        End If

        Gl.glColor4f(1, 1, 1, 1)
        Gl.glBegin(Gl.GL_QUADS)
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(SatGeoPos.lon - 3, SatGeoPos.lat - 3, 0.1)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(SatGeoPos.lon - 3, SatGeoPos.lat + 3, 0.1)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(SatGeoPos.lon + 3, SatGeoPos.lat + 3, 0.1)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(SatGeoPos.lon + 3, SatGeoPos.lat - 3, 0.1)
        Gl.glEnd()
        Gl.glPopMatrix()
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, 0) ' Supprime textures
        '
        ' Latitude, longitude, altitude
        ' -----------------------------
        Gl.glPushMatrix()
        '
        Gl.glBegin(Gl.GL_QUADS)
        Gl.glColor4f(1, 1, 1, 0.5)
        Gl.glVertex3f(90, -82, 0.02)
        Gl.glVertex3f(175, -82, 0.02)
        Gl.glVertex3f(175, -62, 0.02)
        Gl.glVertex3f(90, -62, 0.02)
        Gl.glEnd()
        '
        Gl.glColor4f(0, 0, 0, 1)
        gl_DrawText("Longitude : ", 90 + 5, -62 - 5, 0.03, font2)
        gl_DrawText(Format(SatGeoPos.lon, "0.00") & " °", 90 + 5 + 35, -62 - 5, 0.03, font2)
        gl_DrawText("Latitude : ", 90 + 5, -62 - 10, 0.03, font2)
        gl_DrawText(Format(SatGeoPos.lat, "0.00") & " °", 90 + 5 + 35, -62 - 10, 0.03, font2)
        gl_DrawText("Altitude : ", 90 + 5, -62 - 15, 0.03, font2)
        gl_DrawText(Format(SatGeoPos.alt, "0.00") & " Km", 90 + 5 + 35, -62 - 15, 0.03, font2)
        Gl.glEnable(Gl.GL_DEPTH_TEST)
        Gl.glPopMatrix()
        '
        ' Finish
        ' ------
        'Gl.glFinish()
        Gl.glFlush()
        Gdi.SwapBuffers(m_intptrHdc)
        GC.Collect()
        '
        '
    End Sub

    ''' <summary>
    ''' Draw the 3D view
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub gl_draw_3D()
        '
        ' Déclarations
        ' ------------
        Dim QuadOBJ As Tao.OpenGl.Glu.GLUquadric
        '
        Dim intTableIndex, intTrackCounter As Short
        Dim dblTrackTime, dblOrbitStep As Double
        Dim lightpos(3) As Single
        Dim tps_sideral_deg As Double
        '
        Dim MatAmbient(3) As Single
        Dim MatDiffuse(3) As Single
        Dim MatSpec(3) As Single
        Dim ambient(3) As Single
        '
        tps_sideral_deg = temps_sideral(dblSimulationTime) * deg
        '
        'Wgl.wglMakeCurrent(User.GetDC(Me.picMapView.Handle), hGLRC)
        Wgl.wglMakeCurrent(m_intptrHdc, hGLRC)
        '
        'il faut recalculer le point de vue avec viewport et perspective a chaque iteration
        Gl.glPushMatrix()
        Gl.glMatrixMode(Gl.GL_PROJECTION)
        Gl.glLoadIdentity()
        Gl.glViewport(0, 0, picMapView.Width, picMapView.Height)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glPopMatrix()

        Gl.glMatrixMode(Gl.GL_PROJECTION)
        Gl.glLoadIdentity()
        Glu.gluPerspective(30, picMapView.Width / picMapView.Height, 0.5, 32000)
        Gl.glClear(Gl.GL_COLOR_BUFFER_BIT Or Gl.GL_DEPTH_BUFFER_BIT)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        QuadOBJ = Glu.gluNewQuadric
        Glu.gluQuadricTexture(QuadOBJ, Gl.GL_TRUE)
        Gl.glLoadIdentity()
        '
        Glu.gluLookAt(OeilX, OeilY, OeilZ, 0.0#, 0.0#, 0.0#, 0.0#, 1.0#, 0.0#)
        '

        ' Eclairage
        ' ---------
        If blnSunTrace = True Then
            lightpos(0) = SunPos.Y / RE
            lightpos(1) = SunPos.Z / RE
            lightpos(2) = SunPos.X / RE
            lightpos(3) = 1
            '
            Gl.glLightf(Gl.GL_LIGHT0, Gl.GL_CONSTANT_ATTENUATION, 0.1)
            Gl.glEnable(Gl.GL_LIGHT0)
            Gl.glEnable(Gl.GL_LIGHTING)
            'Paramètres de couleur
            MatAmbient(0) = 1 '1.5
            MatAmbient(1) = 1 '1.5
            MatAmbient(2) = 1 '1.5
            MatAmbient(3) = 0
            '
            Gl.glMaterialfv(Gl.GL_FRONT, Gl.GL_AMBIENT, MatAmbient(0))
            MatDiffuse(0) = 1
            MatDiffuse(1) = 1
            MatDiffuse(2) = 1
            MatDiffuse(3) = 1
            '
            Gl.glMaterialfv(Gl.GL_FRONT, Gl.GL_DIFFUSE, MatDiffuse(0))
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_POSITION, lightpos(0))
            ambient(0) = 0
            ambient(1) = 0
            ambient(2) = 0
            ambient(3) = 1
            '
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_AMBIENT, ambient(0))
        End If

        '
        ' Tracé de la terre
        ' -----------------
        Gl.glEnable(Gl.GL_TEXTURE_2D)
        Gl.glPushMatrix()
        Gl.glRotated(-90, 1, 0, 0)
        Gl.glRotated(90, 0, 0, 1)
        Gl.glPushMatrix()
        Gl.glRotated(tps_sideral_deg, 0, 0, 1)
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, earthtexture)
        Gl.glColor4f(1, 1, 1, 1)
        Gl.glCallList(glSphere)
        ' Axe nord sud
        Gl.glColor3f(1, 1, 0)
        Gl.glBegin(Gl.GL_LINES)
        Gl.glVertex3f(0, 0, 1.5)
        Gl.glVertex3f(0, 0, -1.5)
        Gl.glEnd()
        Gl.glPopMatrix()
        Gl.glPopMatrix()
        '
        ' Satellite
        ' ---------
        Gl.glDisable(Gl.GL_TEXTURE_2D)
        MatDiffuse(0) = CShort((Satellite.satColor) And &HFF) / 255
        MatDiffuse(1) = CShort((Satellite.satColor) \ &H100 And &HFF) / 255
        MatDiffuse(2) = CShort((Satellite.satColor) \ &H10000 And &HFF) / 255
        MatDiffuse(3) = 1
        '
        Gl.glMaterialfv(Gl.GL_FRONT, Gl.GL_DIFFUSE, MatDiffuse(0))
        '
        Gl.glPushMatrix()
        Gl.glTranslated(Satellite.getPos(dblSimulationTime).Y / RE, Satellite.getPos(dblSimulationTime).Z / RE, Satellite.getPos(dblSimulationTime).X / RE)
        Glu.gluSphere(QuadOBJ, 0.02 * distance_oeil / 5, 10, 10)
        Gl.glPopMatrix()
        '
        ' Trace orbites
        ' -------------
        intTableIndex = 0
        dblOrbitStep = (get_period(SatOrbit) / 1000) / minday
        For intTrackCounter = -500 To 500
            dblTrackTime = dblSimulationTime + intTrackCounter * dblOrbitStep
            update_Orbit(dblTrackTime, SatOrbit)
            Kepler_to_PosVel(SatOrbit, SatPos, SatVel)
            dblOrbitTable(intTableIndex).X = (SatPos.X / RE)
            dblOrbitTable(intTableIndex).Y = (SatPos.Y / RE)
            dblOrbitTable(intTableIndex).Z = (SatPos.Z / RE)
            intTableIndex = intTableIndex + 1
        Next intTrackCounter
        '
        Gl.glDisable(Gl.GL_LIGHTING)
        Gl.glColor3f(CShort((Satellite.satColor) And &HFF) / 255, CShort((Satellite.satColor) \ &H100 And &HFF) / 255, CShort((Satellite.satColor) \ &H10000 And &HFF) / 255)
        '
        Gl.glPushMatrix()
        Gl.glBegin(Gl.GL_LINE_STRIP)
        For intTableIndex = 0 To 1000
            Gl.glVertex3f(dblOrbitTable(intTableIndex).Y, dblOrbitTable(intTableIndex).Z, dblOrbitTable(intTableIndex).X)
        Next intTableIndex
        Gl.glEnd()
        Gl.glPopMatrix()
        '
        ' Soleil
        ' ------
        Gl.glPushMatrix()
        Gl.glEnable(Gl.GL_TEXTURE_2D)
        Gl.glColor4f(1, 1, 0, 1)
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, tx_soleil3D)
        Gl.glTranslated(SunPos.Y / RE, SunPos.Z / RE, SunPos.X / RE)
        Gl.glRotated(Atan2(SunPos.Y, SunPos.X) * 180 / pi, 0, 1, 0)
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(-800, -800, 0)
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(-800, 800, 0)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(800, 800, 0)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(800, -800, 0)
        Gl.glEnd()
        Gl.glPopMatrix()
        '
        ' Lune
        ' ------
        Call update_Moon(dblSimulationTime, MoonPos) ' Position dans gamma50
        Gl.glPushMatrix()
        Gl.glEnable(Gl.GL_TEXTURE_2D)
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, tx_moon)
        Gl.glColor4f(1, 1, 1, 1)
        Gl.glTranslated(MoonPos.Y / RE, MoonPos.Z / RE, MoonPos.X / RE)
        Glu.gluSphere(QuadOBJ, 0.25, 30, 30)
        Gl.glPopMatrix()
        '
        ' Tracé étoiles
        ' -------------
        gl_draw_stars()
        '
        ' Latitude, longitude, altitude
        ' -----------------------------
        Gl.glPushMatrix()
        Gl.glEnable(Gl.GL_BLEND)
        Gl.glBlendFunc(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA)
        ' Render white quad with alpha transparency texture..
        Gl.glMatrixMode(Gl.GL_PROJECTION)
        Gl.glLoadIdentity()
        Glu.gluOrtho2D(0, 0, 0, 0) ' Init coordinates

        Gl.glDisable(Gl.GL_DEPTH_TEST)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glLoadIdentity()

        Gl.glBegin(Gl.GL_QUADS)
        Gl.glColor4f(1, 1, 1, 0.5)
        Gl.glVertex2f(0.5, -0.7)
        Gl.glVertex2f(0.5, -0.9)
        Gl.glVertex2f(0.97, -0.9)
        Gl.glVertex2f(0.97, -0.7)
        Gl.glEnd()

        Gl.glColor4f(1, 1, 1, 1)
        gl_DrawText("Longitude : ", 0.5 + 0.02, -0.7 - 0.05, 0, font2)
        gl_DrawText(Format(SatGeoPos.lon, "0.00") & " °", 0.5 + 0.02 + 0.2, -0.7 - 0.05, 0, font2)
        gl_DrawText("Latitude : ", 0.5 + 0.02, -0.7 - 0.1, 0, font2)
        gl_DrawText(Format(SatGeoPos.lat, "0.00") & " °", 0.5 + 0.02 + 0.2, -0.7 - 0.1, 0, font2)
        gl_DrawText("Altitude : ", 0.5 + 0.02, -0.7 - 0.15, 0, font2)
        gl_DrawText(Format(SatGeoPos.alt, "0.00") & " Km", 0.5 + 0.02 + 0.2, -0.7 - 0.15, 0, font2)
        Gl.glEnable(Gl.GL_DEPTH_TEST)
        Gl.glPopMatrix()
        '
        ' Finish
        ' ------
        Gl.glFlush()
        Gdi.SwapBuffers(m_intptrHdc)
        GC.Collect()
        ''
    End Sub

    ''' <summary>
    ''' Draw stars
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub gl_draw_stars()
        '
        Dim intensite, rh As Single
        Dim i As Short

        Gl.glBlendFunc(Gl.GL_SRC_COLOR, Gl.GL_ONE)

        Gl.glBindTexture(Gl.GL_TEXTURE_2D, tx_halo)
        For i = 0 To 4649
            'With m_colStars(i + 1)
            If stars(i).Mg > 300 Then ' Draws a point if magnitude is > 300
                intensite = -0.0017 * stars(i).Mg + 1.52

                Gl.glDisable(Gl.GL_TEXTURE_2D)

                Gl.glColor4f(intensite, intensite, intensite, 1)

                Gl.glBegin(Gl.GL_POINTS)

                Gl.glVertex3f(30000 * stars(i).sinAsc * stars(i).cosDec, 30000 * stars(i).sinDec, 30000 * stars(i).cosAsc * stars(i).cosDec)

                Gl.glEnd()
            Else ' Draws a halo if magnitude < 300
                rh = 158 - 0.26 * stars(i).Mg

                Gl.glEnable(Gl.GL_TEXTURE_2D)

                Gl.glColor4f(1, 1, 1, 1)

                Gl.glPushMatrix()

                Gl.glTranslated(30000 * stars(i).sinAsc * stars(i).cosDec, 30000 * stars(i).sinDec, 30000 * stars(i).cosAsc * stars(i).cosDec)

                Gl.glRotated(stars(i).Ad * 180 / pi, 0, 1, 0)

                Gl.glRotated(phi * 180 / pi, 1, 0, 0)

                Gl.glBegin((Gl.GL_QUADS))

                Gl.glTexCoord2f(0, 1)

                Gl.glVertex3f(-rh, -rh, 0)

                Gl.glTexCoord2f(0, 0)

                Gl.glVertex3f(-rh, rh, 0)

                Gl.glTexCoord2f(1, 0)

                Gl.glVertex3f(rh, rh, 0)

                Gl.glTexCoord2f(1, 1)

                Gl.glVertex3f(rh, -rh, 0)

                Gl.glEnd()

                Gl.glPopMatrix()
            End If
            ' End With
        Next

        Gl.glBlendFunc(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA)
    End Sub

#Region "Mouse Controls"

    ''' <summary>
    ''' Adapte the size of the display to the size of the window
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub frmMain_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize

        If picMapView.Height <> (Me.ClientRectangle.Height - 58) Or picMapView.Width <> (Me.ClientRectangle.Width - 20) Then

            picMapView.Height = Me.ClientRectangle.Height - 58
            picMapView.Width = Me.ClientRectangle.Width - 20
            picMapView.Left = 10
            picMapView.Top = 23 + 28
            resizeGLWindow(CInt(picMapView.Width), CInt(picMapView.Height))
            frmMain.Sim_update()

        End If
        
        frmMain.Sim_update()
        resizeGLWindow(CInt(picMapView.Width), CInt(picMapView.Height))
        frmMain.Sim_update()
    End Sub

    ''' <summary>
    ''' Open the Display Window in fullscreen if not or resize it at normal size if already in fullscreen
    ''' by double clicking on the picture
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 


    Private Sub picMapView_Click(sender As Object, e As EventArgs) Handles picMapView.DoubleClick
        If Me.WindowState <> FormWindowState.Maximized Then
            Me.WindowState = FormWindowState.Maximized
            Me.Focus()
            Me.Refresh()
            frmMain.Sim_update()
            LBLDoubleClick.Text = "Double Click to resize"
            'LBLDoubleClick.Top = Me.Size.Height - 120
            'LBLDoubleClick.Left = Me.Size.Width / 2 - LBLDoubleClick.Size.Width / 2
            Me.Refresh()
            frmMain.Sim_update()
        Else
            Me.WindowState = FormWindowState.Normal
            Me.Focus()
            Me.Refresh()
            frmMain.Sim_update()
            LBLDoubleClick.Text = "Double Click to fullscreen"
            'LBLDoubleClick.Top = Me.Size.Height - 80
            'LBLDoubleClick.Left = Me.Size.Width / 2 - LBLDoubleClick.Size.Width / 2
            Me.Refresh()
            frmMain.Sim_update()
        End If


    End Sub


    Private Sub picMapView_MouseDown(ByVal sender As Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles picMapView.MouseDown

        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim X As Single = eventArgs.X
        Dim Y As Single = eventArgs.Y
        '
        '
        Select Case selectedView
            Case VIEW_3D
                If Button = 1 Then
                    sngViewFormerX = X
                    sngViewFormerY = Y
                ElseIf Button = 2 Then
                    sngZoomFormerY = Y
                End If
                '
            Case VIEW_ATTITUDE
                attsph.MouseDown(Button, Shift, X, Y)
                '
                '
        End Select
        ''
    End Sub
    Private Sub picMapView_MouseMove(ByVal sender As Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles picMapView.MouseMove

        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim X As Single = eventArgs.X
        Dim Y As Single = eventArgs.Y
        '
        Select Case selectedView
            Case VIEW_3D
                If Button = 1 Then
                    phi = phi - (sngViewFormerY - Y) / 50 'phi = phi + (sngViewFormerY - Y) / 50
                    psi = psi + (X - sngViewFormerX) / 50
                    If phi > pi / 2 Then phi = pi / 2
                    If phi < -pi / 2 Then phi = -pi / 2
                    If psi > 2 * pi Then psi = psi - 2 * pi
                    If psi < 0 Then psi = psi + 2 * pi
                    sngViewFormerX = X
                    sngViewFormerY = Y
                    frmMain.Sim_update()
                ElseIf Button = 2 Then
                    distance_oeil = distance_oeil - (sngZoomFormerY - Y) / 20 'distance_oeil = distance_oeil + (sngZoomFormerY - Y) / 20
                    If distance_oeil < 2 Then distance_oeil = 2
                    sngZoomFormerY = Y
                    frmMain.Sim_update()
                End If
                '

                OeilX = distance_oeil * Cos(phi) * Cos(psi)
                OeilY = distance_oeil * Sin(phi)
                OeilZ = distance_oeil * Cos(phi) * Sin(psi)
                '
            Case VIEW_ATTITUDE
                attsph.MouseMove(Button, Shift, X, Y)
                '
        End Select
        ''
    End Sub
    Private Sub picMapView_MouseUp(ByVal sender As Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles picMapView.MouseUp

        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim X As Single = eventArgs.X
        Dim Y As Single = eventArgs.Y
        '
        picMapView.Cursor = System.Windows.Forms.Cursors.Default
        '
        If selectedView = VIEW_ATTITUDE Then
            attsph.MouseUp(Button, Shift, X, Y)
        End If
    End Sub


#End Region

#Region "Buttons"

    Private Sub chkDayNight_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDayNight.Click
        '
        If chkDayNight.Checked Then blnSunTrace = True Else blnSunTrace = False
        frmMain.Sim_update()
        ''
    End Sub

    Private Sub ComboBoxAxisChanges(ByRef cmbDepend As System.Windows.Forms.ComboBox, ByRef cmbChange As System.Windows.Forms.ComboBox)
        Dim i As Short
        Dim tmpaxis, index As Integer
        indexChange = False

        For i = 0 To attsph.getAxisUbound
            ' searching for the axis in the combobox
            If cmbDepend.Text = attsph.getAxis(i) Then
                If i >= 3 Then
                    tmpaxis = i - 3
                Else
                    tmpaxis = i
                End If
            End If
            If cmbChange.Text = attsph.getAxis(i) Then
                index = i
            End If
        Next i
        cmbChange.Items.Clear()
        For i = 0 To attsph.getAxisUbound
            If tmpaxis <> i And tmpaxis + 3 <> i Then
                cmbChange.Items.Add(attsph.getAxis(i))
            End If
        Next i
        If index <> tmpaxis And index <> tmpaxis + 3 Then
            cmbChange.Text = attsph.getAxis(index)
        Else
            cmbChange.SelectedIndex = 0
        End If
    End Sub
    Private Sub ComboBoxOrientChanges(ByRef cmbDepend As System.Windows.Forms.ComboBox, ByRef cmbChange As System.Windows.Forms.ComboBox)
        Dim i As Short
        Dim tmporient, index As Integer
        indexChange = False

        For i = 0 To attsph.getOrientationUbound
            ' searching for the axis in the combobox
            If cmbDepend.Text = attsph.getOrientation(i) Then
                tmporient = i
            End If
            If cmbChange.Text = attsph.getOrientation(i) Then
                index = i
            End If
        Next i
        cmbChange.Items.Clear()
        For i = 0 To attsph.getOrientationUbound
            If tmporient <> i Then
                cmbChange.Items.Add(attsph.getOrientation(i))
            End If
        Next i
        If index <> tmporient Then
            cmbChange.Text = attsph.getOrientation(index)
        Else
            cmbChange.SelectedIndex = 0
        End If
    End Sub

    Public Sub cmbPrinAxis_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbPrinAxis.SelectedIndexChanged
        If allow_event And indexChange Then
            attsph.prinAxis = cmbPrinAxis.Text
            ComboBoxAxisChanges(cmbPrinAxis, cmbOptAxis)
            frmMain.Sim_update()
            indexChange = True
        End If
    End Sub
    Public Sub cmbPrinOrient_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbPrinOrient.SelectedIndexChanged
        '
        If allow_event And indexChange Then
            attsph.prinOrient = cmbPrinOrient.Text
            ComboBoxOrientChanges(cmbPrinOrient, cmbOptOrient)
            frmMain.Sim_update()
            indexChange = True
        End If
        ''
    End Sub

    Public Sub cmbOptAxis_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbOptAxis.SelectedIndexChanged
        If allow_event And indexChange Then
            attsph.optAxis = cmbOptAxis.Text
            ComboBoxAxisChanges(cmbOptAxis, cmbPrinAxis)
            frmMain.Sim_update()
            indexChange = True
        End If
    End Sub
    Public Sub cmbOptOrient_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbOptOrient.SelectedIndexChanged
        '
        If allow_event And indexChange Then
            attsph.optOrient = cmbOptOrient.Text
            ComboBoxOrientChanges(cmbOptOrient, cmbPrinOrient)
            frmMain.Sim_update()
            indexChange = True
        End If
    End Sub

#End Region

    Private Sub cmdClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        'Me.Close()
        Me.Hide()
        displayOpened = False
        frmMain.optView_0.Checked = False
        frmMain.optView_1.Checked = False
        frmMain.optView_3.Checked = False
        Wgl.wglMakeCurrent(0, 0)
        Wgl.wglDeleteContext(User.GetDC(Me.picMapView.Handle))

    End Sub

    'Private Sub frmDisplay_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    '    displayOpened = False
    '    frmMain.optView_0.Checked = False
    '    frmMain.optView_1.Checked = False
    '    frmMain.optView_3.Checked = False
    '    Wgl.wglMakeCurrent(0, 0)
    '    Wgl.wglDeleteContext(User.GetDC(Me.picMapView.Handle))

    'End Sub

    'Forbid the closing of the window by using the red cross (in case she is reactivated) or alt+F4
    Private Sub FermetureForm_closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        e.Cancel = True
    End Sub


End Class