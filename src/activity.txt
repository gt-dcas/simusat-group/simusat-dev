h2. Simusat

!https://openforge.isae.fr/attachments/download/19224/view-from-sat.PNG!

h3. Get repository:

<pre>
git clone https://openforge.isae.fr/git/simusat
</pre>

h3. Branches

* Change branch:

<pre>
git checkout <branchname>
</pre>

* Active branch: master

* Last developement done: Angel_Stage2019

!https://openforge.isae.fr/attachments/download/19227/space-view.PNG!

h2. Installation and launching

* Installation:

<pre>
Download the installer from: https://openforge.isae.fr/attachments/download/19223/SimusatInstallerPackage.zip
Follow the instruction in the README 
</pre>

* Launch:

<pre>
Two options:
* Use the version you have installed like any Windows program.
* Use the StandAloneVersion that you will find in the folder StandAloneVersions of the repository (this is useful if you do not have admin rights)
</pre>


!https://openforge.isae.fr/attachments/download/19226/planisphere.PNG!

h3. Manual


To take a look on what can be done, download the Quickstart "manual":https://openforge.isae.fr/projects/simusat/repository/raw/DOC_Simusat/Angel2019/TutoSimusat/Simusat%20Quick%20Start%20Tutorial.pdf?rev=Angel_Stage2019 


!https://openforge.isae.fr/attachments/download/19225/new-main.PNG!

