Option Strict Off
Option Explicit On
Imports System.Math

Module basDeclaration
    '
	' Module containing many global constants and unfortunately variables
	'
	' This module contains many global constants like pi and e, the earth radius; there are also state constants
	' like simulation state; furthermore, there are type declarations of statistics, orbits, mountpoints, global variables.
	'
	' todo refactoring : move global variables to their according classes and create set-/get-methods, then
	' redirect all communication to the moved global variable to this class
    '
    ' Boolean to disable events during initialisations
    Public allow_event As Boolean = True
	'
    ' Mathematic Constants
    Public Const pi As Double = System.Math.PI
    Public Const rad As Double = pi / 180.0# ' Converts deg angles into rad
	Public Const deg As Double = 180# / pi ' Converts rad angles into deg
	'
	' Earth Constant Parameter
	Public Const RE As Double = 6378.137 ' Earth radius in km (WGS 84)
	Public Const myEarth As Double = 398600.8 ' km^3/s^2
	Public Const minday As Integer = 1440# ' Minutes per day
	Public Const secday As Integer = 86400# ' Seconds per day
	'
	' Type for 3d vector of Doubles.
	Structure vector
		Dim X As Double ' x component
		Dim Y As Double ' y component
		Dim Z As Double ' z component
	End Structure
	'
	Structure Coords
		Dim X As vector ' component for first row
		Dim Y As vector ' component for second row
		Dim Z As vector ' component for third row
	End Structure
	'
	' Type for longitude, latitude and altitude (Double)
	Structure Lon_Lat
		Dim lon As Double '* longitude
		Dim lat As Double '* latitude
		Dim alt As Double '* altitude
	End Structure
	'
	' Type for 2d positional data (Single)
	Structure sngPos
		Dim X As Single '* x coordinate
		Dim Y As Single '* y coordinate
	End Structure
	'
	' Type for defining an orbit (Double)
    Structure orbit
        Dim Name As String
        Dim SemiMajor_Axis As Double '* semi major axis of the ellipse
        Dim Eccentricity As Double '* eccentricity of the ellipse
        Dim Inclination As Double '* inclination against the equatorial plane
        Dim Argument_Perigee As Double '* Argument of Perigee
        Dim RAAN As Double '* RAAN
        Dim Mean_Anomaly As Double '*Mean Anomaly
        Dim epoch As Double '* Epoch
        Dim dec1 As Double
        Dim dec2 As Double
        Dim bstar As Double
    End Structure
	'
    Public dblTimeStep As Double = 5 / secday
    Public Openfile As String
	Public OpenfileShort As String
	Public blnSunTrace As Boolean
    Public blnIsNewPrj As Boolean = False ' False : we don't need to save it (before closing for example)
	'
	' Type used to have a timestamp (w/o ms)
	' --------------------------------------
	Public Structure udtDate
        Dim year As Short ' year based on 0
        Dim month As Short ' month (1 = January, 2 = February...)
        Dim day As Short ' day
		Dim dayofweek As Short ' day of the week ( 0 = Sunday, 1 = Monday, 2 = Tuesday...)
        Dim hour As Short ' hour
        Dim minute As Short ' minute
        Dim second As Short ' second
	End Structure
    '
    ' Declarations for Attitudesphere
    ' -------------------------------
	Const PFD_TYPE_RGBA As Short = 0
	Const PFD_MAIN_PLANE As Short = 0
	Const PFD_DOUBLEBUFFER As Short = 1
	Const PFD_DRAW_TO_WINDOW As Integer = &H4
	Const PFD_SUPPORT_OPENGL As Integer = &H20
	'
	Public Declare Function GetTickCount Lib "kernel32" () As Integer
	'
	' ------------------------
	' System local information
	' ------------------------
	Public decimalSeparator As String
	'
	' functions and constants
	Declare Function GetSystemDefaultLCID Lib "kernel32" () As Integer
	Declare Function GetLocaleInfo Lib "kernel32"  Alias "GetLocaleInfoA"(ByVal Locale As Integer, ByVal LCType As Integer, ByVal lpLCData As String, ByVal cchData As Integer) As Integer
	Public Const LOCALE_SDECIMAL As Integer = &HE
	'
	' Variables relative to the current project
	' -----------------------------------------
	' Satellites
	Public Satellite As CSatellite
    Public SolarPanels As New Collection ' Collection of CSolarPanel
    'Public SolarPanels2 As New Collection ' Collection of CSolarPanel
    Public PanelsNumber As Short ' the number of panels
    Public SatOrbit As orbit ' Updated orbit
    Public SatOrbit0 As orbit ' Initial orbit
	Public SunPos As vector ' Position of the sun
	Public MoonPos As vector ' Position of the moon
    Public SatPos As vector ' Satellite position (X,Y,Z coordinates)
	Public SatVel As vector ' Satellite velocity (X,Y,Z coordinates)
	Public SatGeoPos As Lon_Lat ' Satellite geodetic position (longitude, latitude)
    '
    ' Attitude sphere
	Public attsph As CAttitudeSphere
    '
    ' Project data
    ' ------------
    Public dblSimulationStart As Double ' Expressed in true Julian days
    Public dblSimulationTime As Double  '       "
	Public projectName As String
    Public projectPath As String
    '
    ' Timer
    'Public refreshTimer As Timer
    '
    ' Simulation and view
    '
    Public sngTrackTable(1000) As sngPos
    Public dblOrbitTable(1000) As vector
    Public dblLastTrackStep As Double
    Public dblLastOrbit(200) As Object
    Public blnEclipse As Boolean
    '
    Public OeilY, OeilX, OeilZ As Single
    Public distance_oeil As Double
    Public sngViewFormerY, sngViewFormerX, sngZoomFormerY As Single
    Public phi, psi As Double
    Public pObj As Integer
    Public displayOpened As Boolean

    '
    Public filename As String
    '
    ' Etat simulation
    ' ---------------
    Public blnSimulationInitialized As Boolean
    '
    ' Events
    ' ------
    Public EventsList As New Collection ' List of events
    Public EventsNumber As Short ' Number of events
    '
    ' Time values for simulation loop
    ' -------------------------------
    Public TickNow As Integer
    Public TickLast As Integer
    Public ticks As Integer
    Public simloop As Boolean
    '
    ' Constants for the simulation
    ' -----------------------------
    '
    Public dblSliderTime As Double
    '
    ' Views for the main window
    ' ------------------------
    Public selectedView As Byte
    Public Const VIEW_2D As Short = 0
    Public Const VIEW_3D As Short = 1
    Public Const VIEW_ATTITUDE As Short = 3

    '
    ' Declaration of textures
    ' ------------------------
    Public tx_soleil As Integer ' Sun 2D
    Public tx_soleil3D As Integer ' Sun 3D
    Public tx_lune As Integer ' moon 2D
    Public tx_moon As Integer ' moon 3D
    Public tx_halo As Integer ' near star
    Public earthtexture As Integer '= 1S
    Public sat_jour As Integer ' satellite day
    Public sat_nuit As Integer ' satellite night
    '
    ' OpenGL display context
    ' ----------------------
    Public hGLRC As Integer
    Public hGLRC_sat As Integer
    '
    Public m_intptrHdc As System.IntPtr
    Public m_intptrHdc_sat As System.IntPtr
    '
    Public stars(4651) As CStar
    '
    ' D�claration des num�ros de listes opengl
    ' ----------------------------------------
    Public Const glSphere As Short = 1
    Public Const glStation As Short = 2
    Public Const glTerre As Short = 3
    Public Const glInfo As Short = 4
    '
    Public font1 As Short
    Public font2 As Short
    '
    ' Radiation
    Public SunPow As Double
    Public Albedo_month(11) As Single
    Public IR_month(11) As Single
    Public Alb_Cer_bln As Boolean = True ' By default is taken CERES data
    Public EarthIR_Cer_bln As Boolean = True ' By Default is taken CERES data
    '
    'Public XX As Double ' variable pour test
    '
#Region "Data range declaration"
    '
    Public BodyMass_min As Double
    Public BodyMass_max As Double
    Public BodyHeigth_min As Double
    Public BodyHeigth_max As Double
    Public BodyWidth_min As Double
    Public BodyWidth_max As Double
    '
    Public BodyInertia_min As Double
    Public BodyInertia_max As Double
    '
    Public PanelMass_min As Double
    Public PanelMass_max As Double
    Public PanelStrings_min As Double
    Public PanelStrings_max As Double
    Public PanelLength_min As Double
    Public PanelLength_max As Double
    Public PanelWidth_min As Double
    Public PanelWidth_max As Double
    '
    Public BoomMass_min As Double
    Public BoomMass_max As Double
    Public BoomDist_min As Double
    Public BoomDist_max As Double
    '
    Public Const Coeff_min As Double = 0
    Public Const Coeff_max As Double = 1
    '
    Public dipsat_passive_min As Double
    Public dipsat_passive_max As Double
    '
    Public dipsat_active_min As Double
    Public dipsat_active_max As Double
    '
    Public JR_min As Double
    Public JR_max As Double
    Public om_min As Double
    Public om_max As Double
    '
    Public DampersMass_min As Double
    Public DampersMass_max As Double
    Public DampersDist_min As Double
    Public DampersDist_max As Double
    Public c_min As Double
    Public c_max As Double
    Public kd_min As Double
    Public kd_max As Double
    '
    Public jet_min As Double
    Public jet_max As Double
    '
#End Region
    '
    ' Saving checked orbit (Needed in frmOrbit)
    ' -----------------------------------------
    Public orbit_checked(7) As Double
    Public testOrbit As Boolean
    '
    ' Thermal files
    ' -------------
    Public EarthConfFactorFile As String
    Public EarthTempFile As String
    Public AlbedoCoefFile As String
    '
    ' Test files
    ' ----------
    Public TestSat As String
    Public TestReport As String
    Public TestIntegration As String
    Public TestSatIntegration As String

    ' Yaml file
    Public TempPanelName As String

End Module