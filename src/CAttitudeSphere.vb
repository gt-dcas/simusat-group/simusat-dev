Option Strict Off
Option Explicit On
Imports Tao.OpenGl
Imports Tao.Platform.Windows
Imports System.Math

Friend Class CAttitudeSphere
	'
	' Time
	' ----
	Dim dblTimeNow As Double
	'
	' Rotation
	' --------
	Dim blnRotate As Boolean
	Dim angleX As Short
	Dim angleY As Short
	Dim actX As Short
	Dim actY As Short
	'
	' Zoom
	' ----
	Dim blnZoom As Boolean
	Dim actZoomX As Short
	Dim actZoomY As Short
    Dim zoom As Double = 1
	Const ScaleFactor As Short = 10
    '
	Dim blnActive As Boolean
	'
	' DisplayLists
	Dim arrow As Short
	Dim sphere As Short
	Dim font1 As Short
	Dim font2 As Short
	Dim hiResEarth As Integer
	Dim sunList As Short
	Dim panel3 As Short
	'
	' GLFonts
    Dim gmf(256) As Single 'GLYPHMETRICSFLOAT
	'
	' Textures
	Dim txtEarthTexture() As Short
	'Dim tx_gold() As GLuint
    'Dim tx_halo() As Byte
	'Dim tx_panel() As GLuint
	'
	Dim panel1 As Short
    Dim panel2 As Short
    '
    ' SunLight
    ' --------
	Dim sun_pos As vector
	Dim lightpos(3) As Single
	Dim ambientpos(3) As Single
	'
	' Settings for the Sphere Grid
	' ----------------------------
    Dim Object_Renamed As Tao.OpenGl.Glu.GLUquadric
    Dim arrowobj As Tao.OpenGl.Glu.GLUquadric
	'
	Public Segments As Short
	'
	' Attitude Sphere Settings
	' ------------------------
	Dim XAxis As vector
	Dim yaxis As vector
	Dim zaxis As vector
	Dim actCoords As Coords
    '
    ' id textures 
    '
    Dim tx_sat As Integer
    Dim tx_panel As Integer
    Dim tx_sun As Integer
    '
	' SatelliteModel
	' --------------
    Dim axis(5) As String           ' all available axis
    Dim orientation(7) As String    ' all available orientations
    Public prinAxis As String       ' axis of the principle direction
    Public prinOrient As String     ' orientation of the principle direction
    Public optAxis As String        ' axis of the optimized direction
    Public optOrient As String      ' orientation of the optimized direction
	'
	' Bitmapheader
	' ------------
	Private Structure BITMAP
		Dim bmType As Integer
		Dim bmWidth As Integer
		Dim bmHeight As Integer
		Dim bmWidthBytes As Integer
		Dim bmPlanes As Integer
		Dim bmBitsPixel As Integer
		Dim bmBits As Integer
	End Structure

    Private Structure BITMAPINFOHEADER '40 bytes
        Dim biSize As Integer
        Dim biWidth As Integer
        Dim biHeight As Integer
        Dim biPlanes As Short
        Dim biBitCount As Short
        Dim biCompression As Integer
        Dim biSizeImage As Integer
        Dim biXPelsPerMeter As Integer
        Dim biYPelsPerMeter As Integer
        Dim biClrUsed As Integer
        Dim biClrImportant As Integer
    End Structure

	Private Structure RGBQUAD
		Dim rgbBlue As Byte
		Dim rgbGreen As Byte
		Dim rgbRed As Byte
		Dim rgbReserved As Byte
	End Structure

	Private Structure BITMAPINFO
		Dim bmiHeader As BITMAPINFOHEADER
		Dim bmiColors As RGBQUAD
    End Structure

    ' Global rotational movement
    ' --------------------------
    Dim angle_X As Double           ' Rotation angle around X axis
    Dim angle_Y As Double           ' Rotation angle around Y axis
    Dim angle_Z As Double           ' Rotation angle around Z axis
    '
    ' Rotational movement due to flywheels
    ' ------------------------------------
    Dim previousAngles As vector = vct(0, 0, 0)
    Dim currentAngles As vector = vct(0, 0, 0)
    Dim flywheel_dt As Double = dblTimeStep * secday / 10   ' Step in seconds

    Public Sub New()
        '
        MyBase.New()
        '
        Dim i As Short
        '
        ' Initialize GLView for drawing OpenGL content
        ' gl_InitializeWindow((frmDisplay.picMapView), hGLRC)
        '
        ' Initialize rotation
        angleX = 0
        angleY = 0
        '
        ' Load textures
        ReDim txtEarthTexture(8)
        For i = 0 To 7
            frmFirst.ShowProgress((20 + i * 8))
            LoadTextureFromFile(My.Application.Info.DirectoryPath & "\" & "Data\Maps\earth" & CStr(i + 1) & "_2048.jpg", txtEarthTexture(i))
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_WRAP_S, Gl.GL_CLAMP)
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_WRAP_T, Gl.GL_CLAMP)
        Next
        '
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\" & "Data\Textures\goldfoil.jpg", tx_sat) ' Goldfoil cover
        frmFirst.ShowProgress((80))
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\" & "Data\Textures\SolarPanel.jpg", tx_panel) ' For panels drawing
        frmFirst.ShowProgress((90))
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\" & "Data\Textures\soleil_3D.jpg", tx_sun) ' For sun drawing
        frmFirst.ShowProgress((100))
        '
        ' Setup Display Liste
        sphere = Gl.glGenLists(1)
        hiResEarth = Gl.glGenLists(1)
        '
        GLSphereList()
        GLEarthList()
        GLPanelList()
        Object_Renamed = Glu.gluNewQuadric()
        arrowobj = Glu.gluNewQuadric
        '
        ' Fonts
        gl_BuildFont2D((frmDisplay.picMapView), font1, font2)
        '
        ' Sphere Attitude Settings
        axis(0) = "I" ' Available axes
        axis(1) = "J"
        axis(2) = "K"
        axis(3) = "-I"
        axis(4) = "-J"
        axis(5) = "-K"
        orientation(0) = "nadir" ' Available orientations
        orientation(1) = "sun-pointing"
        orientation(2) = "moon-pointing"
        orientation(3) = "velocity"
        orientation(4) = "ground speed"
        orientation(5) = "inertial x"
        orientation(6) = "inertial y"
        orientation(7) = "inertial z"
        '
        'set default values for the sphere attitude
        prinAxis = axis(5)
        prinOrient = orientation(0)
        optAxis = axis(0)
        optOrient = orientation(3)
        ''
    End Sub

	Sub GLPanelList()
		'
		Dim i As Short
		'
        ' Solar panel

		' -----------
        panel1 = Gl.glGenLists(1)
        Gl.glNewList(panel1, Gl.GL_COMPILE)
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, tx_panel) ' Solarpanel
        Gl.glColor4f(1, 1, 1, 1)
        Gl.glBegin((Gl.GL_QUADS)) ' Upper face
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(1, 1, 0.03)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(-1, 1, 0.03)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(-1, -1, 0.03)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(1, -1, 0.03)
        Gl.glEnd()
        '
        Gl.glDisable(Gl.GL_LIGHTING)
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, 0) ' Solarpanel
        Gl.glColor4f(0.2, 0.2, 0.2, 1)
        Gl.glBegin((Gl.GL_QUADS)) ' Lower face
        Gl.glVertex3f(1, 1, -0.03)
        Gl.glVertex3f(-1, 1, -0.03)
        Gl.glVertex3f(-1, -1, -0.03)
        Gl.glVertex3f(1, -1, -0.03)
        Gl.glEnd()
        '
        Gl.glColor4f(0.5, 0.5, 0.5, 1)
        For i = 0 To 3 ' Side faces
            Gl.glRotated(90 * i, 0, 0, 1)
            Gl.glBegin((Gl.GL_QUADS))
            Gl.glVertex3f(1, 1, -0.03)
            Gl.glVertex3f(1, 1, 0.03)
            Gl.glVertex3f(1, -1, 0.03)
            Gl.glVertex3f(1, -1, -0.03)
            Gl.glEnd()
        Next
        Gl.glEnable(Gl.GL_LIGHTING)
        '
        Gl.glEndList()
        '
        ' Mechanism
        ' ---------
        panel2 = Gl.glGenLists(1)
        Gl.glNewList(panel2, Gl.GL_COMPILE)
        Gl.glDisable(Gl.GL_LIGHTING)
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, 0)
        Gl.glColor4f(0.4, 0.4, 0.4, 1)
        Gl.glBegin((Gl.GL_QUADS)) ' Upper face
        Gl.glVertex3f(0, 0.1, 0.03)
        Gl.glVertex3f(0, -0.1, 0.03)
        Gl.glVertex3f(0.9, -0.9, 0.03)
        Gl.glVertex3f(0.9, -0.7, 0.03)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0, 0.1, 0.03)
        Gl.glVertex3f(0, -0.1, 0.03)
        Gl.glVertex3f(0.9, 0.7, 0.03)
        Gl.glVertex3f(0.9, 0.9, 0.03)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, -0.9, 0.03)
        Gl.glVertex3f(0.99, -0.9, 0.03)
        Gl.glVertex3f(0.99, 0.9, 0.03)
        Gl.glVertex3f(0.9, 0.9, 0.03)
        Gl.glEnd()
        Gl.glColor4f(0.4, 0.4, 0.4, 1)
        Gl.glBegin((Gl.GL_QUADS)) ' Upper face
        Gl.glVertex3f(0, 0.1, -0.03)
        Gl.glVertex3f(0, -0.1, -0.03)
        Gl.glVertex3f(0.9, -0.9, -0.03)
        Gl.glVertex3f(0.9, -0.7, -0.03)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0, 0.1, -0.03)
        Gl.glVertex3f(0, -0.1, -0.03)
        Gl.glVertex3f(0.9, 0.7, -0.03)
        Gl.glVertex3f(0.9, 0.9, -0.03)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, -0.9, -0.03)
        Gl.glVertex3f(0.99, -0.9, -0.03)
        Gl.glVertex3f(0.99, 0.9, -0.03)
        Gl.glVertex3f(0.9, 0.9, -0.03)

        Gl.glColor4f(0.3, 0.3, 0.3, 1)
        Gl.glBegin((Gl.GL_QUADS)) ' Sides
        Gl.glVertex3f(0.9, -0.9, -0.03)
        Gl.glVertex3f(0.9, -0.9, 0.03)
        Gl.glVertex3f(0, -0.1, 0.03)
        Gl.glVertex3f(0, -0.1, -0.03)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, 0.9, -0.03)
        Gl.glVertex3f(0.9, 0.9, 0.03)
        Gl.glVertex3f(0, 0.1, 0.03)
        Gl.glVertex3f(0, 0.1, -0.03)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, -0.7, -0.03)
        Gl.glVertex3f(0.9, -0.7, 0.03)
        Gl.glVertex3f(0, 0.1, 0.03)
        Gl.glVertex3f(0, 0.1, -0.03)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, 0.7, -0.03)
        Gl.glVertex3f(0.9, 0.7, 0.03)
        Gl.glVertex3f(0, -0.1, 0.03)
        Gl.glVertex3f(0, -0.1, -0.03)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, -0.9, -0.03)
        Gl.glVertex3f(0.9, -0.9, 0.03)
        Gl.glVertex3f(0.9, 0.9, 0.03)
        Gl.glVertex3f(0.9, 0.9, -0.03)
        Gl.glEnd()
        Gl.glEnable(Gl.GL_LIGHTING)
        Gl.glEndList()

    End Sub

    Public Sub GLEarthList()
        Dim n As Short
        Dim r As Double
        Dim lg0, lt0 As Short
        ' Dim tx_terre1() As Byte
        ' Dim tx_terre2() As Byte
        'Dim a, b, ta, tb
        'useless

        n = 200
        r = RE / ScaleFactor

        Dim i As Short

        ' The earth list is realized as a sphere seperated into 8 pieces
        ' These 8 pieces allow a very high resolution of the earthtexture (8192*4096)

        Gl.glNewList(hiResEarth, Gl.GL_COMPILE)
        For i = 0 To 3
            Gl.glEnable(Gl.GL_TEXTURE_2D)
            Gl.glBindTexture(Gl.GL_TEXTURE_2D, txtEarthTexture(2 * i))
            For lg0 = i * n / 4 To (i + 1) * n / 4 - 1
                For lt0 = -n / 4 To -1

                    Gl.glLineWidth(1)
                    Gl.glBegin((Gl.GL_QUADS))

                    Gl.glNormal3f(Cos(lt0 * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin(lt0 * pi / n * 2))
                    Gl.glTexCoord2f((lg0 / n * 4) - i, 1 + lt0 / n * 4)
                    Gl.glVertex3f(r * Cos(lt0 * pi / n * 2) * Cos(lg0 * pi / n * 2), r * Cos(lt0 * pi / n * 2) * Sin(lg0 * pi / n * 2), r * Sin(lt0 * pi / n * 2))

                    Gl.glNormal3f(Cos((lt0 + 1) * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                    Gl.glTexCoord2f((lg0 / n * 4) - i, 1 + (lt0 + 1) / n * 4)
                    Gl.glVertex3f(r * Cos((lt0 + 1) * pi / n * 2) * Cos(lg0 * pi / n * 2), r * Cos((lt0 + 1) * pi / n * 2) * Sin(lg0 * pi / n * 2), r * Sin((lt0 + 1) * pi / n * 2))

                    Gl.glNormal3f(Cos((lt0 + 1) * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                    Gl.glTexCoord2f(((lg0 + 1) / n * 4) - i, 1 + (lt0 + 1) / n * 4)
                    Gl.glVertex3f(r * Cos((lt0 + 1) * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), r * Cos((lt0 + 1) * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), r * Sin((lt0 + 1) * pi / n * 2))

                    Gl.glNormal3f(Cos(lt0 * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin(lt0 * pi / n * 2))
                    Gl.glTexCoord2f(((lg0 + 1) / n * 4) - i, 1 + lt0 / n * 4)
                    Gl.glVertex3f(r * Cos(lt0 * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), r * Cos(lt0 * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), r * Sin(lt0 * pi / n * 2))
                    Gl.glEnd()
                Next
            Next

            Gl.glBindTexture(Gl.GL_TEXTURE_2D, txtEarthTexture(2 * i + 1))
            For lg0 = i * n / 4 To (i + 1) * n / 4 - 1
                For lt0 = 0 To n / 4 - 1
                    Gl.glBegin((Gl.GL_QUADS))
                    Gl.glNormal3f(Cos(lt0 * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin(lt0 * pi / n * 2))
                    Gl.glTexCoord2f(((lg0 / n) * 4) - i, lt0 / n * 4)
                    Gl.glVertex3f(r * Cos(lt0 * pi / n * 2) * Cos(lg0 * pi / n * 2), r * Cos(lt0 * pi / n * 2) * Sin(lg0 * pi / n * 2), r * Sin(lt0 * pi / n * 2))
                    Gl.glNormal3f(Cos((lt0 + 1) * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                    Gl.glTexCoord2f(((lg0 / n) * 4) - i, (lt0 + 1) / n * 4)
                    Gl.glVertex3f(r * Cos((lt0 + 1) * pi / n * 2) * Cos(lg0 * pi / n * 2), r * Cos((lt0 + 1) * pi / n * 2) * Sin(lg0 * pi / n * 2), r * Sin((lt0 + 1) * pi / n * 2))
                    Gl.glNormal3f(Cos((lt0 + 1) * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                    Gl.glTexCoord2f((((lg0 + 1) / n) * 4) - i, (lt0 + 1) / n * 4)
                    Gl.glVertex3f(r * Cos((lt0 + 1) * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), r * Cos((lt0 + 1) * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), r * Sin((lt0 + 1) * pi / n * 2))
                    Gl.glNormal3f(Cos(lt0 * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin(lt0 * pi / n * 2))
                    Gl.glTexCoord2f((((lg0 + 1) / n) * 4) - i, lt0 / n * 4)
                    Gl.glVertex3f(r * Cos(lt0 * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), r * Cos(lt0 * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), r * Sin(lt0 * pi / n * 2))
                    Gl.glEnd()
                Next
            Next
        Next
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, 0)
        Gl.glDisable(Gl.GL_TEXTURE_2D)
        Gl.glEndList()

    End Sub

    Public Sub GLSphereList()
        '
        Dim i, j As Short
        Dim X, Y As Double
        Dim radius As Single
        '
        radius = 4
        '
        Gl.glNewList(sphere, Gl.GL_COMPILE)
        'calculate equator and meridian
        gl_Color3f(RGB(200, 255, 255))
        Gl.glPushMatrix()
        Gl.glTranslated(0, radius * Cos(pi / 2), 0)
        Gl.glRotated(90, 1, 0, 0)
        Gl.glBegin(Gl.GL_LINE_LOOP)
        For i = 1 To 50
            X = Cos((2 * pi) * (i / 50)) * radius * Sin(pi / 2)
            Y = Sin((2 * pi) * (i / 50)) * radius * Sin(pi / 2)
            Gl.glNormal3f(X / radius, Y / radius, 0)
            Gl.glVertex3d(X, Y, 0)
        Next
        Gl.glEnd()
        Gl.glPopMatrix()
        Gl.glPushMatrix()
        Gl.glBegin(Gl.GL_LINE_STRIP)
        For i = 0 To 50 / 2
            X = Sin((2 * pi) * (i / 50)) * radius
            Y = Cos((2 * pi) * (i / 50)) * radius
            Gl.glNormal3f(X / radius, Y / radius, 0)
            Gl.glVertex3d(X, Y, 0)
        Next
        'calculate other circles
        gl_Color3f(RGB(255, 255, 255))
        For i = 50 / 2 To 50
            X = Sin((2 * pi) * (i / 50)) * radius
            Y = Cos((2 * pi) * (i / 50)) * radius
            Gl.glNormal3f(X / radius, Y / radius, 0)
            Gl.glVertex3d(X, Y, 0)
        Next
        Gl.glEnd()
        Gl.glPopMatrix()
        Gl.glPushMatrix()
        For i = 1 To 5
            Gl.glPushMatrix()
            Gl.glRotated(((i / 6) * 180), 0, 1, 0)
            Gl.glBegin(Gl.GL_LINE_LOOP)
            For j = 0 To 50
                X = Cos((2 * pi) * (j / 50)) * radius
                Y = Sin((2 * pi) * (j / 50)) * radius
                Gl.glNormal3f(X / radius, Y / radius, 0)
                Gl.glVertex3d(X, Y, 0)
            Next
            Gl.glEnd()
            Gl.glPopMatrix()
        Next
        For i = 1 To 5
            Gl.glPushMatrix()
            Gl.glTranslated(0, radius * Cos(pi * (i / 6)), 0)
            Gl.glRotated(90, 1, 0, 0)
            Gl.glBegin(Gl.GL_LINE_LOOP)
            For j = 0 To 50
                X = Cos((2 * pi) * (j / 50)) * radius * Sin((i / 6) * pi)
                Y = Sin((2 * pi) * (j / 50)) * radius * Sin((i / 6) * pi)
                Gl.glNormal3f(X / radius, Y / radius, -Cos(pi * (i / 6)))
                Gl.glVertex3d(X, Y, 0)
            Next
            Gl.glEnd()
            Gl.glPopMatrix()
        Next
        Gl.glPopMatrix()
        Gl.glEndList()
    End Sub

#Region "Mouse Controls"

    Public Sub MouseDown(ByRef Button As Short, ByRef Shift As Short, ByRef X As Single, ByRef Y As Single)
        'left mouse button: rotate
        If Button = 1 Then
            blnRotate = True
            actX = X
            actY = Y
            'right mouse button: zoom
        ElseIf Button = 2 Then
            blnZoom = True
            actZoomX = X
            actZoomY = Y
        End If
    End Sub

    Public Sub MouseUp(ByRef Button As Short, ByRef Shift As Short, ByRef X As Single, ByRef Y As Single)
        If Button = 1 Then blnRotate = False
        If Button = 2 Then blnZoom = False
    End Sub

    Public Sub MouseMove(ByRef Button As Short, ByRef Shift As Short, ByRef X As Single, ByRef Y As Single)
        If blnRotate Then
            angleY = Modulo((angleY + (X - actX)), 360)
            angleX = Modulo((angleX + (Y - actY)), 360) 'Modulo((angleX - (Y - actY)), 360)
            actX = X
            actY = Y
            gl_Paint()
        ElseIf blnZoom Then
            zoom = zoom + (Y - actZoomY) 'zoom - (Y - actZoomY)
            If zoom < -3 Then zoom = -3
            If zoom > 10000 Then zoom = 10000

            actZoomY = Y
            gl_Paint()
        End If
    End Sub

#End Region

#Region "Buttons"

    Friend Function CalculateAxis(ByRef time As Double) As Coords
        '
        Dim last, prim, sec, temp As vector
        'Dim i
        'useless
        Dim v, u, w As Double
        '
        ' Vector of the primary axis
        prim = Satellite.getVectTo(time, prinOrient)
        u = prim.X
        v = prim.Y
        w = prim.Z
        ' vector to the target of the secondary direction
        sec = Satellite.getVectTo(time, optOrient)
        With sec
            u = .X
            v = .Y
            w = .Z
        End With

        ' vector (or inverse of it) of the last axis
        last = vctCross(prim, sec)
        ' vector (or inverse of it) of the secondary axis
        With last
            u = .X
            v = .Y
            w = .Z
        End With

        temp = vctCross(prim, last)
        ' determine the real secondary axis
        If vctNorm(vctAdd(sec, temp)) > vctNorm(vctAdd(sec, vctInverse(temp))) Then
            sec = temp
        Else
            sec = vctInverse(temp)
        End If

        'rot  x
        'gr�n y
        'blau z

        ' set the primary vector
        If prinAxis = "J" Then XAxis = prim
        If prinAxis = "K" Then yaxis = prim
        If prinAxis = "I" Then zaxis = prim
        If prinAxis = "-J" Then XAxis = vctInverse(prim)
        If prinAxis = "-K" Then yaxis = vctInverse(prim)
        If prinAxis = "-I" Then zaxis = vctInverse(prim)
        ' set the secondary vector
        If optAxis = "J" Then XAxis = sec
        If optAxis = "K" Then yaxis = sec
        If optAxis = "I" Then zaxis = sec
        If optAxis = "-J" Then XAxis = vctInverse(sec)
        If optAxis = "-K" Then yaxis = vctInverse(sec)
        If optAxis = "-I" Then zaxis = vctInverse(sec)
        ' set the third vector
        If (prinAxis <> "J" And prinAxis <> "-J" And optAxis <> "J" And optAxis <> "-J") Then XAxis = last
        If (prinAxis <> "K" And prinAxis <> "-K" And optAxis <> "K" And optAxis <> "-K") Then yaxis = last
        If (prinAxis <> "I" And prinAxis <> "-I" And optAxis <> "I" And optAxis <> "-I") Then zaxis = last

        temp = vctCross(XAxis, yaxis)

        If vctNorm(vctAdd(zaxis, temp)) < vctNorm(vctAdd(sec, vctInverse(temp))) Then
            last = vctInverse(last)
            If (prinAxis <> "J" And prinAxis <> "-J" And optAxis <> "J" And optAxis <> "-J") Then XAxis = last
            If (prinAxis <> "K" And prinAxis <> "-K" And optAxis <> "K" And optAxis <> "-K") Then yaxis = last
            If (prinAxis <> "I" And prinAxis <> "-I" And optAxis <> "I" And optAxis <> "-I") Then zaxis = last
        End If

        'to (force) norm the axes
        actCoords.X = vctToNorm(XAxis)
        actCoords.Y = vctToNorm(yaxis)
        actCoords.Z = vctToNorm(zaxis)

        CalculateAxis.X = vctToNorm(XAxis)
        CalculateAxis.Y = vctToNorm(yaxis)
        CalculateAxis.Z = vctToNorm(zaxis)

    End Function

    Public Function getAxis(ByRef i As Short) As String
        getAxis = axis(i)
    End Function
    Public Function getAxisUbound() As Short
        getAxisUbound = UBound(axis)
    End Function

    Public Function getOrientation(ByRef i As Short) As String
        getOrientation = orientation(i)
    End Function
    Public Function getOrientationUbound() As Short
        getOrientationUbound = UBound(orientation)
    End Function

#End Region

#Region "Drawing functions called in VIEW_ATTITUDE"

    Public Sub gl_Paint()
        '
        Dim rotMatrix(16) As Double
        Dim radius As Single
        '
        ' Allow only one call to Update
        If blnActive Then Exit Sub
        '
        blnActive = True
        'Wgl.wglMakeCurrent(User.GetDC(frmDisplay.picMapView.Handle), hGLRC)
        Wgl.wglMakeCurrent(m_intptrHdc, hGLRC)
        'il faut recalculer le point de vue avec viewport et perpesctive a chaque it�ration
        Gl.glPushMatrix()
        Gl.glMatrixMode(Gl.GL_PROJECTION)
        Gl.glLoadIdentity()
        Gl.glViewport(0, 0, frmDisplay.picMapView.Width, frmDisplay.picMapView.Height)
        Glu.gluPerspective(30, frmDisplay.picMapView.Width / frmDisplay.picMapView.Height, 0.5, 30000)
        Glu.gluLookAt(0, 0, 20, 0, 0, 0, 0, 1, 0)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glPopMatrix()

        Gl.glClearColor(0, 0, 0, 0)
        Gl.glClear(Gl.GL_COLOR_BUFFER_BIT Or Gl.GL_DEPTH_BUFFER_BIT)
        '
        ' Set current time
        dblTimeNow = dblSimulationTime
        '
        Gl.glEnable(Gl.GL_DEPTH_TEST)
        'Gl.glEnable(Gl.GL_POLYGON_SMOOTH)
        'on remplace le POLYGON_SMOOTH par POLYGON, sinon apparition de triangles sur certaines configs
        Gl.glEnable(Gl.GL_POLYGON)
        Gl.glShadeModel(Gl.GL_SMOOTH)
        Gl.glHint(Gl.GL_PERSPECTIVE_CORRECTION_HINT, Gl.GL_NICEST)
        Gl.glPolygonMode(Gl.GL_FRONT_AND_BACK, Gl.GL_FILL)
        Gl.glMatrixMode(Gl.GL_PROJECTION)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glLoadIdentity()
        Gl.glDisable(Gl.GL_LIGHTING)
        Gl.glDisable(Gl.GL_TEXTURE_2D)
        '
        ' Mouse zoom
        Gl.glTranslated(0, 0, -zoom)
        ' Mouse rotation
        Gl.glRotated(angleX, 1, 0, 0)
        Gl.glRotated(angleY, 0, 1, 0)
        '
        Gl.glColor3f(1, 1, 1)
        '
        ' Objects calculated in SphereCOS
        ' ----------------------------
        ' Draw sphere grid
        gl_DrawSphere()
        '
        ' Objects calculated in OpenGLCos
        ' -------------------------------
        ' Build rotationmatrix from inertial coordinatesystem to sphere coordinate system
        With CalculateAxis(dblTimeNow)
            '
            rotMatrix(0) = .X.Y
            rotMatrix(1) = .Y.Y
            rotMatrix(2) = .Z.Y
            rotMatrix(3) = 0
            '
            rotMatrix(4) = .X.Z
            rotMatrix(5) = .Y.Z
            rotMatrix(6) = .Z.Z
            rotMatrix(7) = 0
            '
            rotMatrix(8) = .X.X
            rotMatrix(9) = .Y.X
            rotMatrix(10) = .Z.X
            rotMatrix(11) = 0
            '
            rotMatrix(12) = 0
            rotMatrix(13) = 0
            rotMatrix(14) = 0
            rotMatrix(15) = 1
            '
        End With
        Gl.glMultMatrixd(rotMatrix(0)) ' Multiply OpenGL matrix stack with rotationmatrix
        '
        ' SetupSunLight
        gl_BuildSunLight()
        Gl.glColor3f(1, 1, 1)
        '
        ' Draw sphere axis
        ' ----------------
        radius = 4
        Gl.glDisable(Gl.GL_LIGHTING)
        Gl.glBegin(Gl.GL_LINES) ' Draw axis
        '
        Gl.glColor3f(1, 0, 0)
        With vctMultScalar(actCoords.Z, 6)
            Gl.glVertex3f(.Y * radius / 6, .Z * radius / 6, .X * radius / 6)
            Gl.glVertex3f(.Y, .Z, .X)
        End With
        '
        Gl.glColor3f(0, 1, 0)
        With vctMultScalar(actCoords.X, 6)
            Gl.glVertex3f(.Y * radius / 6, .Z * radius / 6, .X * radius / 6)
            Gl.glVertex3f(.Y, .Z, .X)
        End With
        '
        Gl.glColor3f(0, 1, 1)
        With vctMultScalar(actCoords.Y, 6)
            Gl.glVertex3f(.Y * radius / 6, .Z * radius / 6, .X * radius / 6)
            Gl.glVertex3f(.Y, .Z, .X)
        End With
        Gl.glEnd()
        With vctMultScalar(actCoords.Z, 6.2) ' Draw the labels
            gl_DrawTextColored("I ", .Y, .Z, .X, &HFF, font2)
        End With
        With vctMultScalar(actCoords.X, 6.2)
            gl_DrawTextColored("J ", .Y, .Z, .X, &HFF00, font2)
        End With
        With vctMultScalar(actCoords.Y, 6.2)
            gl_DrawTextColored("K ", .Y, .Z, .X, &HFFFF00, font2)
        End With
        Gl.glEnable(Gl.GL_LIGHTING)
        '
        Gl.glColor3f(1, 1, 1)
        '
        ' Draw earthsphere
        gl_DrawEarth()
        '
        ' Draw arrows to selected satellites
        gl_DrawArrows()
        '
        ' Draw sun
        gl_Drawsun()

        '
        ' Draw satellite model
        gl_DrawSatellite()
        '
        blnActive = False

        Gl.glPushMatrix()

        Gl.glDisable(Gl.GL_TEXTURE_2D)
        Gl.glEnable(Gl.GL_BLEND)
        Gl.glBlendFunc(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA)

        Gl.glMatrixMode(Gl.GL_PROJECTION)
        Glu.gluOrtho2D(0, 0, 0, 0) 'On initialise les coordonn�es

        Gl.glDisable(Gl.GL_DEPTH_TEST)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glLoadIdentity()

        Gl.glColor4f(1, 1, 1, 1)
        gl_DrawText("Longitude : ", 4.5 + 0.2, -3.8 - 0.25, 0, font2)
        gl_DrawText(Format$(SatGeoPos.lon, "#0.00") & " �", 4.5 + 0.2 + 2, -3.8 - 0.25, 0, font2)
        gl_DrawText("Latitude : ", 4.5 + 0.2, -3.8 - 0.55, 0, font2)
        gl_DrawText(Format$(SatGeoPos.lat, "#0.00") & " �", 4.5 + 0.2 + 2, -3.8 - 0.55, 0, font2)
        gl_DrawText("Altitude : ", 4.5 + 0.2, -3.8 - 0.85, 0, font2)
        gl_DrawText(Format$(SatGeoPos.alt, "#0.00") & " Km", 4.5 + 0.2 + 2, -3.8 - 0.85, 0, font2)

        Gl.glFlush()
        Gdi.SwapBuffers(m_intptrHdc)
        GC.Collect()
        Gl.glEnable(Gl.GL_DEPTH_TEST)
        Gl.glPopMatrix()

    End Sub

    Private Sub gl_DrawArrows()
        '
        Dim a, v As vector
        Dim targetvector As vector
        Dim textvector As vector
        Dim arrowlength As Single
        Dim i As Short
        Dim target As vector
        Dim name As String
        Dim color1 As Integer
        '
        arrowlength = 3
        name = ""
        '
        For i = 0 To 1
            Select Case i
                Case 0
                    v.X = 0 : v.Y = 0 : v.Z = 0
                    target = v : name = "Earth" : color1 = RGB(192, 128, 0)
                Case 1
                    target = SunPos : name = "Sun" : color1 = RGB(255, 200, 0)
            End Select
            '
            targetvector = Satellite.getOGLVectToPos(dblTimeNow, target)
            '
            ' Calculate rotation axis
            a.X = 1 : a.Y = 0 : a.Z = 0
            a = vctCross(a, targetvector)
            '
            Gl.glPushMatrix()
            ' Draw name
            textvector = vctMultScalar(vctToNorm(targetvector), arrowlength * 1.1 + 2)
            Gl.glDisable(Gl.GL_LIGHTING)
            gl_DrawTextColored(name, textvector.X, textvector.Y, textvector.Z, color1, font2)
            '
            ' Rotate arrow to desired orientation
            Gl.glEnable(Gl.GL_LIGHTING)
            Gl.glRotated(Math.Acos(vctToNorm(targetvector).X) / pi * 180, a.X, a.Y, a.Z)
            '
            ' Check if arrow has to be rendered solid (target can be see from the satellite position)
            ' or wireframe (target can NOT be see from the satellite position)
            If Not Satellite.canSatSeePos(target, dblTimeNow) Then
                Gl.glPolygonMode(Gl.GL_FRONT_AND_BACK, Gl.GL_LINE)
            End If
            Gl.glDisable(Gl.GL_LIGHTING)
            gl_Color3f(color1)
            Gl.glRotated(90, 0, 1, 0)
            Gl.glTranslated(0, 0, 2)
            Glu.gluCylinder(Object_Renamed, arrowlength * 0.02, arrowlength * 0.02, arrowlength * 0.75, 5, 1)
            Gl.glTranslated(0, 0, arrowlength * 0.75)
            gl_Color3f(color1)
            Glu.gluCylinder(Object_Renamed, arrowlength * 0.05, 0, arrowlength * 0.25, 5, 1)
            Gl.glPolygonMode(Gl.GL_FRONT_AND_BACK, Gl.GL_FILL)
            Gl.glDisable(Gl.GL_LIGHTING)
            Gl.glColor3f(1, 1, 1)
            Gl.glPopMatrix()
        Next
        ''
    End Sub

    Private Sub gl_DrawEarth()
        Dim tempVector As vector
        Dim tps_sideral_deg As Double
        '
        ' Calculate rotation of earth
        tps_sideral_deg = temps_sideral(dblTimeNow) * deg
        tempVector.X = 0
        tempVector.Y = 0
        tempVector.Z = 0
        Gl.glPushMatrix()
        tempVector = Satellite.getOGLVectToPos(dblTimeNow, tempVector)
        Gl.glTranslated(tempVector.X / ScaleFactor, tempVector.Y / ScaleFactor, tempVector.Z / ScaleFactor)
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, 1)
        Gl.glPushMatrix()
        Gl.glRotated(tps_sideral_deg, 0, 1, 0)
        Gl.glRotated(-90, 1, 0, 0) ' Rotate earth from yzx coordinate system to xyz coordinate system
        Gl.glRotated(90, 0, 0, 1)
        Gl.glCullFace(Gl.GL_FRONT)
        Gl.glEnable(Gl.GL_CULL_FACE)
        Gl.glEnable(Gl.GL_TEXTURE_2D)
        Gl.glEnable(Gl.GL_LIGHTING)
        Gl.glDisable(Gl.GL_LIGHT0)
        Gl.glEnable(Gl.GL_LIGHT1)
        Gl.glCallList(hiResEarth) ' Draw earth here
        Gl.glDisable(Gl.GL_LIGHT1)
        Gl.glDisable(Gl.GL_LIGHTING)
        Gl.glDisable(Gl.GL_TEXTURE_2D)
        Gl.glDisable(Gl.GL_CULL_FACE)
        Gl.glPopMatrix()
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, 0)
        Gl.glPopMatrix()
        ''
    End Sub

    Private Sub gl_BuildSunLight()
        '
        ' Set the sunposition relative to the satellite and scale the distance
        sun_pos = vctMultScalar(SunPos, 1 / ScaleFactor)
        sun_pos = vctSub(SunPos, SatPos)
        '
        Gl.glMatrixMode(Gl.GL_PROJECTION)
        '
        'light0 is used for satellites
        'light1 is used for the Earth (Day/Night contrast)
        '
        Gl.glLightfv(Gl.GL_LIGHT1, Gl.GL_CONSTANT_ATTENUATION, CSng(0.1))

        Gl.glEnable(Gl.GL_LIGHTING)
        '
        '
        ambientpos(0) = 1
        ambientpos(1) = 1
        ambientpos(2) = 1
        ambientpos(3) = 1
        Gl.glLightfv(Gl.GL_LIGHT1, Gl.GL_DIFFUSE, ambientpos(0))
        Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_DIFFUSE, ambientpos(0))
        '
        ambientpos(0) = 0.02
        ambientpos(1) = 0.02
        ambientpos(2) = 0.02
        ambientpos(3) = 1
        Gl.glLightfv(Gl.GL_LIGHT1, Gl.GL_AMBIENT, ambientpos(0))
        Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_AMBIENT, ambientpos(0))
        '
        lightpos(0) = sun_pos.Y
        lightpos(1) = sun_pos.Z
        lightpos(2) = sun_pos.X
        lightpos(3) = 1
        Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_POSITION, lightpos(0))
        Gl.glLightfv(Gl.GL_LIGHT1, Gl.GL_POSITION, lightpos(0))
        Gl.glEnable(Gl.GL_LIGHT0)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        ''
    End Sub

    Private Sub gl_DrawSphere()
        '
        Dim i As Short
        Dim alpha As Double
        Dim degrees As String
        Dim radius As Single
        '
        radius = 4
        '
        Gl.glPushMatrix()
        ' Draw sphere grid
        If frmDisplay.chkSphere.Checked = True Then
            Gl.glPushMatrix()
            Gl.glRotated(-90, 0, 1, 0)
            Gl.glCallList(sphere)
            ' Draw sphere inscription
            Gl.glPushMatrix()
            For i = 0 To 6
                Gl.glPushMatrix()
                alpha = i / (6) * pi
                degrees = CStr(90 - ((i / 6) * 180))
                gl_DrawText(degrees & "�", radius * 1.1 * Sin(alpha), radius * 1.1 * Cos(alpha), 0, font2)
                Gl.glPopMatrix()
            Next
            For i = 1 To 11
                Gl.glPushMatrix()
                alpha = i / 6 * pi
                degrees = CStr((i / 6) * 180)
                If i = 6 Then
                    degrees = "+/-180"
                ElseIf i > 6 Then
                    degrees = CStr(CDbl(degrees) - 360)
                End If
                gl_DrawText(degrees & "�", radius * 1.1 * Cos(alpha), 0, -radius * 1.1 * Sin(alpha), font2)
                Gl.glPopMatrix()
            Next
            Gl.glPopMatrix()
            Gl.glPopMatrix()
        End If
        Gl.glPopMatrix()
        Gl.glColor3f(1, 1, 1)
        ''
    End Sub

    Private Sub gl_Drawsun()
        '
        Dim modelmatrix(16) As Double
        Dim i As Short
        Dim j As Short
        '
        Gl.glPushMatrix()
        '
        sun_pos = vctMultScalar(SunPos, 1 / ScaleFactor)
        Gl.glTranslated(sun_pos.Y / 1000, sun_pos.Z / 1000, sun_pos.X / 1000)
        Gl.glGetDoublev(Gl.GL_MODELVIEW_MATRIX, modelmatrix(0))
        '
        ' The rectangle with the suntexture will always face the camera.
        For i = 0 To 2
            For j = 0 To 2
                If i = j Then
                    modelmatrix(i * 4 + j) = 1
                Else
                    modelmatrix(i * 4 + j) = 0
                End If
            Next j
        Next i
        '
        Gl.glLoadMatrixd(modelmatrix(0))
        Gl.glDisable(Gl.GL_LIGHTING)
        Gl.glDisable(Gl.GL_COLOR_MATERIAL)
        Gl.glColor4f(1, 1, 0, 0.8)
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, tx_sun)
        Gl.glBegin(Gl.GL_QUADS)
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(-1000, -1000, 0)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(-1000, 1000, 0)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(1000, 1000, 0)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(1000, -1000, 0)
        Gl.glEnd()
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, 0)
        Gl.glDisable(Gl.GL_TEXTURE_2D)

        '
        Gl.glPopMatrix()
        Gl.glEnable(Gl.GL_LIGHTING)
        Gl.glEnable(Gl.GL_COLOR_MATERIAL)
        ''
    End Sub

    Public Sub gl_DrawSatellite()
        '
        Dim rotMatrixSat(16) As Double
        calcul_couples(dblTimeNow)       ' The torques Caer(Aero), Cgg(gravity gradient), Csol(solar), Cmag(Magnatic) are calculated in this function
        '
        Dim rotMatrixSatX(16) As Double     ' Rotation matrix around X axis
        Dim rotMatrixSatY(16) As Double     ' Rotation matrix around Y axis
        Dim rotMatrixSatZ(16) As Double     ' Rotation matrix around Z axis
        '
        ' Dim blnCanSeeSun As Boolean
        Dim i As Integer
        Dim w, h, rmax As Single ' Width, height of central body and radius of bounding sphere
        Dim lp, wp As Double ' Length, width of solar panel
        Dim alf As Double ' Angle to rotate solar panel on the right face
        Dim satscale As Double ' Global scale of satellite (to fill well the sattitude sphere)
        Gl.glDisable(Gl.GL_CULL_FACE)
        ' Dim v As vector
        '
        ' Draw satellite
        ' --------------
        ' Build rotationmatrix from inertial coordinatesystem to satellite coordinate system
        rotMatrixSat(0) = Satellite.getX(dblTimeNow).Y
        rotMatrixSat(1) = Satellite.getX(dblTimeNow).Z
        rotMatrixSat(2) = Satellite.getX(dblTimeNow).X
        rotMatrixSat(3) = 0
        '
        rotMatrixSat(4) = Satellite.getY(dblTimeNow).Y
        rotMatrixSat(5) = Satellite.getY(dblTimeNow).Z
        rotMatrixSat(6) = Satellite.getY(dblTimeNow).X
        rotMatrixSat(7) = 0
        '
        rotMatrixSat(8) = Satellite.getZ(dblTimeNow).Y
        rotMatrixSat(9) = Satellite.getZ(dblTimeNow).Z
        rotMatrixSat(10) = Satellite.getZ(dblTimeNow).X
        rotMatrixSat(11) = 0
        '
        rotMatrixSat(12) = 0
        rotMatrixSat(13) = 0
        rotMatrixSat(14) = 0
        rotMatrixSat(15) = 1
        '
        Gl.glMultMatrixd(rotMatrixSat(0)) 'multiply OpenGL matrix stack with rotationmatrix
        '
        ' Calculation of the rotational movement due to flywheels
        ' -------------------------------------------------------
        For i = 1 To CInt(dblTimeStep * secday / flywheel_dt)
            currentAngles.X = currentAngles.X + (om(1) * Math.Sin(previousAngles.Z) + om(2) * Math.Cos(previousAngles.Z)) * 2 * pi / 360 / Math.Cos(previousAngles.Y) * flywheel_dt
            currentAngles.Y = currentAngles.Y + (om(1) * Math.Cos(previousAngles.Z) - om(2) * Math.Sin(previousAngles.Z)) * 2 * pi / 360 * flywheel_dt
            currentAngles.Z = currentAngles.Z + (om(0) + (om(1) * Math.Sin(previousAngles.Z) + om(2) * Math.Cos(previousAngles.Z)) * Math.Tan(previousAngles.Y)) * 2 * pi / 360 * flywheel_dt
            previousAngles = currentAngles
        Next
        '
        If frmDisplay.chkCouples.Checked Then
            '
            ' Rotation around X axis
            ' ----------------------
            angle_X = Ctot.X / SatInertia(0, 0) * (dblTimeNow - dblSimulationStart) ^ 2     ' Newton's 2nd Law (Rotation)
            'angle_X += currentAngles.X                                                      ' Motion due to flywheels
            '
            rotMatrixSatX(0) = 1
            rotMatrixSatX(1) = 0
            rotMatrixSatX(2) = 0
            rotMatrixSatX(3) = 0
            '
            rotMatrixSatX(4) = 0
            rotMatrixSatX(5) = Math.Cos(angle_X)
            rotMatrixSatX(6) = -Math.Sin(angle_X)
            rotMatrixSatX(7) = 0
            '
            rotMatrixSatX(8) = 0
            rotMatrixSatX(9) = Math.Sin(angle_X)
            rotMatrixSatX(10) = Math.Cos(angle_X)
            rotMatrixSatX(11) = 0
            '
            rotMatrixSatX(12) = 0
            rotMatrixSatX(13) = 0
            rotMatrixSatX(14) = 0
            rotMatrixSatX(15) = 1
            '
            Gl.glMultMatrixd(rotMatrixSatX)
            '
            ' Rotation around Y axis
            ' ----------------------
            angle_Y = Ctot.Y / SatInertia(1, 1) * (dblTimeNow - dblSimulationStart) ^ 2     ' Newton's 2nd Law (Rotation)
            'angle_Y += currentAngles.Y                                                      ' Motion due to flywheels
            '
            rotMatrixSatY(0) = Math.Cos(angle_Y)
            rotMatrixSatY(1) = 0
            rotMatrixSatY(2) = Math.Sin(angle_Y)
            rotMatrixSatY(3) = 0
            '
            rotMatrixSatY(4) = 0
            rotMatrixSatY(5) = 1
            rotMatrixSatY(6) = 0
            rotMatrixSatY(7) = 0
            '
            rotMatrixSatY(8) = -Math.Sin(angle_Y)
            rotMatrixSatY(9) = 0
            rotMatrixSatY(10) = Math.Cos(angle_Y)
            rotMatrixSatY(11) = 0
            '
            rotMatrixSatY(12) = 0
            rotMatrixSatY(13) = 0
            rotMatrixSatY(14) = 0
            rotMatrixSatY(15) = 1
            '
            Gl.glMultMatrixd(rotMatrixSatY)
            '
            ' Rotation around Z axis
            ' ----------------------
            angle_Z = Ctot.Z / SatInertia(2, 2) * (dblTimeNow - dblSimulationStart) ^ 2     ' Newton's 2nd Law (Rotation)
            'angle_Z += currentAngles.Z                                                      ' Motion due to flywheels
            '
            rotMatrixSatZ(0) = Math.Cos(angle_Z)
            rotMatrixSatZ(1) = -Math.Sin(angle_Z)
            rotMatrixSatZ(2) = 0
            rotMatrixSatZ(3) = 0
            '
            rotMatrixSatZ(4) = Math.Sin(angle_Z)
            rotMatrixSatZ(5) = Math.Cos(angle_Z)
            rotMatrixSatZ(6) = 0
            rotMatrixSatZ(7) = 0
            '
            rotMatrixSatZ(8) = 0
            rotMatrixSatZ(9) = 0
            rotMatrixSatZ(10) = 1
            rotMatrixSatZ(11) = 0
            '
            rotMatrixSatZ(12) = 0
            rotMatrixSatZ(13) = 0
            rotMatrixSatZ(14) = 0
            rotMatrixSatZ(15) = 1
            '
            Gl.glMultMatrixd(rotMatrixSatZ)
            '
        End If
        '
        Dim lightpos(3) As Single
        'With Satellite.getSatCOSCoords(dblTimeNow, SunPos)
        lightpos(0) = 10 '.Y / ScaleFactor
        lightpos(1) = 0 '.Z / ScaleFactor
        lightpos(2) = 0 '.X / ScaleFactor
        lightpos(3) = 0
        'End With
        'glLightfv ltLight0, GL_POSITION, lightpos(0) '
        '
        Gl.glLineWidth(1)
        Gl.glColor3f(1, 1, 1)
        Gl.glEnable(Gl.GL_LIGHTING)
        Gl.glEnable(Gl.GL_TEXTURE_2D)
        Gl.glEnable(Gl.GL_COLOR_MATERIAL)
        'Gl.glEnable(Gl.GL_POLYGON_SMOOTH)
        'on remplace le POLYGON_SMOOTH par POLYGON, sinon apparition de triangles sur certaines configs
        Gl.glEnable(Gl.GL_POLYGON)
        Gl.glEnable(Gl.GL_NORMALIZE)
        '
        ' Draw satellite
        ' --------------
        '
        Gl.glDisable(Gl.GL_COLOR_MATERIAL)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glEnable(Gl.GL_TEXTURE_2D)
        Gl.glEnable(Gl.GL_LIGHT0)
        '
        ' Draw satellite body
        ' -------------------
        ' Preliminary calculation for scaling
        h = Satellite.satBodyHeight
        w = Satellite.satBodyWidth
        rmax = Sqrt(2 * w * w / 4 + h * h / 4) ' Bounding sphere radius
        satscale = 0.5 * 4 / rmax ' So that rmax corresponds to 50% of the sphere radius
        '
        ' Drawing
        Gl.glPushMatrix()
        Gl.glScalef(w * satscale, w * satscale, h * satscale) ' Fill at 50% of the attitude sphere, the radius of which is 4
        '
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, tx_sat) ' Goldfoil cover
        Gl.glColor4f(1, 1, 1, 1)
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(0.5, 0.5, -0.5) ' +X
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(0.5, 0.5, 0.5)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(0.5, -0.5, 0.5)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(0.5, -0.5, -0.5)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(0.5, 0.5, -0.5) ' +Y
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(0.5, 0.5, 0.5)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(-0.5, 0.5, 0.5)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(-0.5, 0.5, -0.5)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(-0.5, 0.5, -0.5) ' -X
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(-0.5, 0.5, 0.5)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(-0.5, -0.5, 0.5)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(-0.5, -0.5, -0.5)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(0.5, -0.5, -0.5) ' +Y
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(0.5, -0.5, 0.5)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(-0.5, -0.5, 0.5)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(-0.5, -0.5, -0.5)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Upper face
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(0.5, 0.5, 0.5)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(-0.5, 0.5, 0.5)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(-0.5, -0.5, 0.5)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(0.5, -0.5, 0.5)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS)) ' Lower face
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(0.5, 0.5, -0.5)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(-0.5, 0.5, -0.5)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(-0.5, -0.5, -0.5)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(0.5, -0.5, -0.5)
        Gl.glEnd()
        Gl.glPopMatrix()
        '
        ' Draw panels
        ' -----------
        '
        For i = 1 To SolarPanels.Count()
            ' Begin panel drawing
            lp = SolarPanels.Item(i).PanelLength
            wp = SolarPanels.Item(i).PanelWidth
            '
            Gl.glPushMatrix()
            '
            Select Case SolarPanels.Item(i).PanelType
                '
                Case "Type1"
                    Dim teta As Single
                    teta = SolarPanels.Item(i).PanelRollAngle - pi / 2
                    Select Case SolarPanels.Item(i).PanelFace
                        Case "+ X"
                            alf = 0 : teta = -teta
                        Case "+ Y"
                            alf = 90 : teta = -teta
                        Case "- X"
                            alf = 180
                        Case "- Y"
                            alf = 270
                    End Select
                    Gl.glRotatef(alf, 0, 0, 1)
                    Gl.glPushMatrix()
                    Gl.glTranslatef(w / 2 * satscale, 0, -0.03) ' -0.03 to keep the axis visible (out of the panel)
                    Gl.glScalef(wp / 2 * satscale, wp / 2 * satscale, wp / 2 * satscale)
                    Gl.glRotatef(teta * 180 / pi, 1, 0, 0)
                    Gl.glCallList(panel2)
                    Gl.glPopMatrix()
                    Gl.glTranslatef((w / 2 + lp / 2 + wp / 2) * satscale, 0, -0.03) ' -0.03 to keep the axis visible (out of the panel)
                    Gl.glScalef(lp / 2 * satscale, wp / 2 * satscale, wp / 2 * satscale)
                    Gl.glRotatef(teta * 180 / pi, 1, 0, 0)
                    Gl.glCallList(panel1)
                    '
                Case "Type2"
                    Select Case SolarPanels.Item(i).PanelFace
                        Case "+ X"
                            Gl.glRotatef(90, 0, 1, 0)
                            Gl.glTranslatef(0, 0, (w / 2 + 0.025) * satscale)
                        Case "+ Y"
                            Gl.glRotatef(90, 0, 0, 1)       ' This rotation to have the length of the panel along z axis
                            Gl.glRotatef(90, 0, 1, 0)
                            Gl.glTranslatef(0, 0, (w / 2 + 0.025) * satscale)
                        Case "- X"
                            Gl.glRotatef(-90, 0, 1, 0)
                            Gl.glTranslatef(0, 0, (w / 2 + 0.025) * satscale)
                        Case "- Y"
                            Gl.glRotatef(90, 0, 0, 1)       ' This rotation to have the length of the panel along z axis
                            Gl.glRotatef(-90, 0, 1, 0)
                            Gl.glTranslatef(0, 0, (w / 2 + 0.025) * satscale)
                        Case "+ Z"
                            Gl.glTranslatef(0, 0, (h / 2 + 0.025) * satscale)
                        Case "- Z"
                            Gl.glRotatef(180, 1, 0, 0)
                            Gl.glTranslatef(0, 0, (h / 2 + 0.025) * satscale)
                    End Select
                    Gl.glScalef(lp / 2 * satscale, wp / 2 * satscale, 1 / 2 * satscale)
                    Gl.glCallList(panel1)
                    '
                Case "Type3"
                    Select Case SolarPanels.Item(i).PanelFace
                        Case "+ X" : alf = 0
                        Case "+ Y" : alf = 90
                        Case "- X" : alf = 180
                        Case "- Y" : alf = 270
                    End Select
                    Gl.glRotatef(alf, 0, 0, 1)
                    Gl.glTranslatef((w / 2 + lp / 2) * satscale, 0, 0)
                    Select Case SolarPanels.Item(i).PanelHpos
                        Case "Upper"
                            Gl.glTranslatef(0, 0, (h / 2 - 0.025) * satscale)
                        Case "Lower"
                            Gl.glTranslatef(0, 0, -(h / 2 - 0.025) * satscale)
                        Case "Middle"
                            Gl.glTranslatef(0, 0, -0.03 * satscale) ' to keep the axis visible (out of the panel)
                    End Select
                    Gl.glScalef(lp / 2 * satscale, wp / 2 * satscale, 1 / 2 * satscale)
                    If SolarPanels.Item(i).PanelDir = "- Z" Then
                        Gl.glRotatef(180, 1, 0, 0)
                    End If
                    Gl.glCallList(panel1)
                    '
            End Select
            '
            Gl.glPopMatrix()
            ' End of panel drawing
        Next

        '
        '
        ' Draw x,y,z axis
        ' ---------------
        '
        Call Gl.glDisable(Gl.GL_LIGHTING) ' Axis aren't textured nor lighted
        Call Gl.glDisable(Gl.GL_TEXTURE_2D)
        Gl.glLineWidth(3)
        Gl.glBegin(Gl.GL_LINES)
        '
        Call Gl.glColor3f(1, 0, 0) 'x-axis in red
        Call Gl.glVertex3f(0, 0, 0)
        Call Gl.glVertex3f(3 * w, 0, 0)
        '
        Call Gl.glColor3f(0, 1, 0) 'y-axis in green
        Call Gl.glVertex3f(0, 0, 0)
        Call Gl.glVertex3f(0, 3 * w, 0)
        '
        Call Gl.glColor3f(0, 1, 1) 'z-axis in cyan
        Call Gl.glVertex3f(0, 0, 0)
        Call Gl.glVertex3f(0, 0, h + 2 * w)
        '
        Gl.glEnd()
        '
        ' Draw labels (same colors as the axis)
        Call gl_DrawTextColored("X", 3 * w + 0.2, 0, 0, &HFF, font2)
        Call gl_DrawTextColored("Y", 0, 3 * w + 0.2, 0, &HFF00, font2)
        Call gl_DrawTextColored("Z", 0, 0, h + 2 * w + 0.2, &HFFFF00, font2)
        '
        ' Reactive lighting and texturing
        Gl.glLineWidth(1)
        Gl.glEnable(Gl.GL_LIGHTING)
        Gl.glEnable((Gl.GL_TEXTURE_2D))
        '
        ' Restore OpenGl Modelview Matrix
        Gl.glPopMatrix()

        Gl.glDisable(Gl.GL_NORMALIZE)
        'Gl.glDisable(Gl.GL_POLYGON_SMOOTH)
        'on remplace le POLYGON_SMOOTH par POLYGON, sinon apparition de triangles sur certaines configs
        Gl.glDisable(Gl.GL_POLYGON)
        Gl.glDisable(Gl.GL_COLOR_MATERIAL)
        Gl.glDisable(Gl.GL_TEXTURE_2D)
        Gl.glDisable(Gl.GL_LIGHTING)
        '
        ''
    End Sub

#End Region

End Class