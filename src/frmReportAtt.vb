Option Strict Off
Option Explicit On
'Imports Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6
Imports Excel = Microsoft.Office.Interop.Excel

Friend Class frmReportAtt
    Inherits System.Windows.Forms.Form

    Private Sub frmReportAtt_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        Dim i As Integer
        Dim date_temp As udtDate
        ' Initialisation date d�but par d�faut
        '
        convert_Julian_Gregorian(SatOrbit0.epoch, date_temp)
        With date_temp
            txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        '
        ' Initialisation date fin par d�faut
        '
        convert_Julian_Gregorian(SatOrbit0.epoch + 1 / 24, date_temp)
        With date_temp
            txt_date_fin.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                    & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00.0000") & " "
        End With
        '
        With grdReport
            .ColumnCount = 0
            .Columns.Add("", "Time")
            .Columns.Add("", "C_Aero_X (N.m)")
            .Columns.Add("", "C_Aero_Y (N.m)")
            .Columns.Add("", "C_Aero_Z (N.m)")
            .Columns.Add("", "C_Aero_Total (N.m)")
            .Columns.Add("", "C_Solar_X (N.m)")
            .Columns.Add("", "C_Solar_Y (N.m)")
            .Columns.Add("", "C_Solar_Z (N.m)")
            .Columns.Add("", "C_Solar_Total (N.m)")
            .Columns.Add("", "C_Mag_X (N.m)")
            .Columns.Add("", "C_Mag_Y (N.m)")
            .Columns.Add("", "C_Mag_Z (N.m)")
            .Columns.Add("", "C_Mag_Total (N.m)")
            .Columns.Add("", "C_Grav_X (N.m)")
            .Columns.Add("", "C_Grav_Y (N.m)")
            .Columns.Add("", "C_Grav_Z (N.m)")
            .Columns.Add("", "C_Grav_Total (N.m)")
            .Columns.Add("", "C_Total_X (N.m)")
            .Columns.Add("", "C_Total_Y (N.m)")
            .Columns.Add("", "C_Total_Z (N.m)")
            .Columns.Add("", "C_Total (N.m)")
            '
            ' Mise en forme
            .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            For i = 0 To .ColumnCount - 1
                .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.None
                .Columns(i).Width = 80
            Next
            .Columns(0).Width = 140
            .Columns(0).Frozen = True
        End With
        ''
    End Sub

    ''' <summary>
    ''' Define shortcuts Save / Report
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmReportAtt_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyData = (Keys.Control Or Keys.S) Then
            cmdSave_Click(cmdSave, New System.EventArgs)
            e.SuppressKeyPress = True ' Pas besoin que l'�v�nement KeyPress le g�re on vient de le faire
        ElseIf e.KeyData = (Keys.Control Or Keys.Enter) Then
            cmdReport_Click(cmdReport, New System.EventArgs)
            e.SuppressKeyPress = True ' Pas besoin que l'�v�nement KeyPress le g�re on vient de le faire
        End If
    End Sub

    Private Sub frmReportAtt_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        grdReport.Width = Me.Width - 50
        grdReport.Height = Me.Height - 149
    End Sub

    Private Sub cmdReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        '
        Dim CurrentRow As Integer
        Dim dblReportStart, dblReportTime As Double, dblReportEnd As Double, dblReportStep As Double
        Dim date_temp As udtDate
        Dim x$
        Dim i As Integer
        '
        ' Datagrid preparation
        With grdReport
            .ColumnCount = 0
            .Columns.Add("", "Time")
            .Columns.Add("", "C_Aero_X")
            .Columns.Add("", "C_Aero_Y")
            .Columns.Add("", "C_Aero_Z")
            .Columns.Add("", "C_Aero_Total")
            .Columns.Add("", "C_Solar_X")
            .Columns.Add("", "C_Solar_Y")
            .Columns.Add("", "C_Solar_Z")
            .Columns.Add("", "C_Solar_Total")
            .Columns.Add("", "C_Mag_X")
            .Columns.Add("", "C_Mag_Y")
            .Columns.Add("", "C_Mag_Z")
            .Columns.Add("", "C_Mag_Total")
            .Columns.Add("", "C_Grav_X")
            .Columns.Add("", "C_Grav_Y")
            .Columns.Add("", "C_Grav_Z")
            .Columns.Add("", "C_Grav_Total")
            .Columns.Add("", "C_Total_X")
            .Columns.Add("", "C_Total_Y")
            .Columns.Add("", "C_Total_Z")
            .Columns.Add("", "C_Total")
            '
            '
            ' Mise en forme
            .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            For i = 0 To .ColumnCount - 1
                .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.None
                .Columns(i).Width = 80
                .Columns(i).DefaultCellStyle.Format = "0.000E+00"

            Next
            .Columns(0).Width = 140
            .Columns(0).Frozen = True

        End With

        ' Read start date, end date, step
        With date_temp
            .year = Year(txt_date_debut.Value)
            .month = Month(txt_date_debut.Value)
            .day = Microsoft.VisualBasic.DateAndTime.Day(txt_date_debut.Value)
            .hour = Hour(txt_date_debut.Value)
            .minute = Minute(txt_date_debut.Value)
            .second = Second(txt_date_debut.Value)
        End With
        dblReportStart = convert_Gregorian_Julian(date_temp)
        '
        With date_temp
            .year = Year(txt_date_fin.Value)
            .month = Month(txt_date_fin.Value)
            .day = Microsoft.VisualBasic.DateAndTime.Day(txt_date_fin.Value)
            .hour = Hour(txt_date_fin.Value)
            .minute = Minute(txt_date_fin.Value)
            .second = Second(txt_date_fin.Value)
        End With
        '
        dblReportEnd = convert_Gregorian_Julian(date_temp)
        If dblReportEnd < dblReportStart Then
            MsgBox("End date is earlier than start date", MsgBoxStyle.OkOnly, "")
            Exit Sub
        End If
        x$ = updwTimeStep.Text
        x$ = Microsoft.VisualBasic.Left(x$, Len(x$) - 2)
        dblReportStep = CDbl(x$) / secday
        '
        If (dblReportEnd - dblReportStart) / dblReportStep > 6000 Then
            If (MessageBox.Show("You have selected a long analysis which may need much time to compute. Do you want to continue ?", "Long analysis...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No) Then
                Exit Sub
            End If
        End If
        '
        Me.Cursor = Cursors.WaitCursor
        '
        ' Grid initialisation
        grdReport.RowCount = CInt((dblReportEnd - dblReportStart) / dblReportStep + 1)
        dblReportTime = dblReportStart
        '
        '
        ' Calculate and write data in the grid
        CurrentRow = 0
        Do While dblReportTime <= dblReportEnd
            ' 
            ' Orbitography
            update_Sun(dblReportTime, SunPos)
            SatOrbit = Satellite.getOrbit(dblReportTime) ' Gives orbit parameters, SatPos, SatVel, SatGeoPos
            blnEclipse = Eclipse(SatPos, SunPos)
            '
            ' Simulation
            ' compute_torques
            calcul_couples(dblReportTime)  ' The torques Caer(Aero), Cgg(gravity gradient), Csol(solar), Cmag(Magnetic) are calculated in this function
            '
            ' Write data
            convert_Julian_Gregorian(dblReportTime, date_temp)
            With date_temp
                x$ = Format(.day, "00") & "/" & Format(.month, "00") & "/" & Format(.year, "0000") & " "
                x$ += Format(.hour, "00") & ":" & Format(.minute, "00") & ":" & Format(.second, "00.0000") & " "
            End With
            '
            With grdReport
                .Item(0, CurrentRow).Value = x$
                .Item(1, CurrentRow).Value = Caer.X
                .Item(2, CurrentRow).Value = Caer.Y
                .Item(3, CurrentRow).Value = Caer.Z
                .Item(4, CurrentRow).Value = vctNorm(Caer)
                .Item(5, CurrentRow).Value = Csol.X
                .Item(6, CurrentRow).Value = Csol.Y
                .Item(7, CurrentRow).Value = Csol.Z
                .Item(8, CurrentRow).Value = vctNorm(Csol)
                .Item(9, CurrentRow).Value = Cmag.X
                .Item(10, CurrentRow).Value = Cmag.Y
                .Item(11, CurrentRow).Value = Cmag.Z
                .Item(12, CurrentRow).Value = vctNorm(Cmag)
                .Item(13, CurrentRow).Value = Cgg.X
                .Item(14, CurrentRow).Value = Cgg.Y
                .Item(15, CurrentRow).Value = Cgg.Z
                .Item(16, CurrentRow).Value = vctNorm(Cgg)
                .Item(17, CurrentRow).Value = Ctot.X
                .Item(18, CurrentRow).Value = Ctot.Y
                .Item(19, CurrentRow).Value = Ctot.Z
                .Item(20, CurrentRow).Value = vctNorm(Ctot)
                '
                If blnEclipse Then .Item(0, CurrentRow).Style.BackColor = Color.LightGray Else .Item(0, CurrentRow).Style.BackColor = Color.LightGoldenrodYellow
            End With
            CurrentRow += 1
            dblReportTime = dblReportTime + dblReportStep
            '
        Loop
        '
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        '
        Dim intFile As Short
        Dim col As Integer, row As Integer
        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet
        Dim i As Integer
        '
        On Error GoTo err_SaveReport
        '
        If grdReport.RowCount < 1 Then Exit Sub ' Nothing to save
        '
        If optText.Checked Then
            dlgReportSave.FileName = "Sans titre"
            dlgReportSave.Filter = "(*.txt)|*.txt|(*.csv)|*.csv"
            If dlgReportSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
            '
            intFile = FreeFile()
            FileOpen(intFile, dlgReportSave.FileName, OpenMode.Output)
            '
            Me.Cursor = Cursors.WaitCursor
            '
            With grdReport
                ' Labels line
                For col = 0 To .ColumnCount - 2
                    Print(1, .Columns(col).HeaderText & "	")
                Next
                Print(1, .Columns(.ColumnCount - 1).HeaderText & vbCrLf)
                '
                ' Units row
                Print(1, "EUR_UTC" & "	")
                For col = 1 To .ColumnCount - 1
                    Print(1, "N.m" & "	")
                Next

                ' Data
                Print(1, vbCrLf)
                For row = 0 To .RowCount - 1
                    Print(1, .Item(0, row).Value & "	")
                    For col = 1 To .ColumnCount - 1
                        'Print(1, Format(.Item(col, row).Value, "#0.000"))
                        Print(1, .Item(col, row).Value)
                        If col = .ColumnCount - 1 Then Print(1, vbCrLf) Else Print(1, "	")
                    Next
                Next
                '
            End With
            '
            FileClose(intFile)
            '
        Else
            dlgReportSave.FileName = "Sans titre"
            dlgReportSave.Filter = "Excel Worksheets (*.xlsx)|*.xlsx|Excel Worksheets 97-2003 (*.xls)|*.xls"
            If dlgReportSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
            '
            xlApp = New Excel.Application
            xlWorkBook = xlApp.Workbooks.Add()
            xlWorkSheet = xlWorkBook.Sheets.Add(, , 1, )
            xlWorkSheet.Name = "Simusat attitude data"
            '
            Me.Cursor = Cursors.WaitCursor
            '
            ' Fill data
            With grdReport
                ' Labels line
                For col = 0 To .ColumnCount - 1
                    xlWorkSheet.Cells(1, col + 1) = .Columns(col).HeaderText
                Next
                ' Data
                For row = 0 To .RowCount - 1
                    xlWorkSheet.Cells(row + 2, 1) = .Item(0, row).Value
                    For col = 1 To .ColumnCount - 1
                        'xlWorkSheet.Cells(row + 2, col + 1) = Format(.Item(col, row).Value, "#0.000")
                        xlWorkSheet.Cells(row + 2, col + 1) = .Item(col, row).Value
                    Next
                Next
            End With
            '
            ' Formatting
            With xlWorkSheet
                With .Range(.Cells(1, 1), .Cells(1, grdReport.ColumnCount))
                    .EntireColumn.AutoFit()
                    .Interior.Color = Color.Cyan  'vbCyan 
                    .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                End With
                With .Range(.Cells(2, 2), .Cells(grdReport.RowCount + 1, grdReport.ColumnCount))
                    '.NumberFormat = "#0.000"
                    .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                End With
                With .Range(.Cells(2, 1), .Cells(grdReport.RowCount + 1, 1))
                    .Interior.Color = RGB(255, 255, 192)
                    '.NumberFormat = "jj/mm/aaaa hh:mm:ss"
                End With
            End With
            '
            xlWorkBook.SaveAs(dlgReportSave.FileName)
            xlApp.Quit()
            '
        End If
        '
        Me.Cursor = Cursors.Default
        '
        Exit Sub
err_SaveReport:
        MsgBox(Err.Description)
        ''
    End Sub

    Private Sub cmdClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        '
        Me.Hide()
        System.Windows.Forms.Application.DoEvents()
        '
    End Sub

  
End Class