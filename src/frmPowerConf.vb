﻿Imports Tao.OpenGl
Imports Tao.Platform.Windows
Imports System.Math
Imports Microsoft.VisualBasic


Public Class frmPowerConf
    Inherits System.Windows.Forms.Form

    'Public Sub power_conf_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
    '
    '   update_power_conf()
    ''
    'End Sub

    Private Sub frmPowerConf_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        update_power_conf()
    End Sub

    Public Sub update_power_conf()
        '
        Dim conversion_julien_date As Object
        Dim pan_txt_s As Object
        Dim pan_txt_h As Object
        Dim pan_list_conf As Object
        Dim pan_opt_pan As Object
        Dim pan_chk_fix As Object
        Dim pan_sld_ang As Object
        Dim pan_txt_ext_gap As Object
        Dim pan_txt_ext_lag As Object
        Dim pan_txt_ext_omega As Object
        '
        'Dim idat As UniversalTime
        Dim dj50 As Double
        Dim i As Short
        Dim X As String
        On Error GoTo err_2
        '
        ' -------------
        ' Batteries TAB
        ' -------------
        '
        bat_onglet_batterie.SelectedIndex = 0
        bat_ch_opt_courant.Checked = True
        '
        bat_txt_c.Text = CStr(cnom)
        bat_txt_ns.Text = CStr(bat_ns)
        bat_txt_np.Text = CStr(bat_np)
        bat_cmb_type.SelectedIndex = bat_type
        '
        ' Charge behaviour parameters
        ' ---------------------------
        bat_txt_0_0.Text = CStr(ch0(0))
        bat_txt_1_0.Text = CStr(ch1(0))
        bat_txt_2_0.Text = CStr(ch2(0))
        bat_txt_och_0.Text = CStr(och(0))
        '
        bat_txt_0_1.Text = CStr(ch0(1))
        bat_txt_1_1.Text = CStr(ch1(1))
        bat_txt_2_1.Text = CStr(ch2(1))
        bat_txt_och_1.Text = CStr(och(1))
        '
        bat_txt_0_2.Text = CStr(ch0(2))
        bat_txt_1_2.Text = CStr(ch1(2))
        bat_txt_2_2.Text = CStr(ch2(2))
        bat_txt_och_2.Text = CStr(och(2))
        '
        bat_txt_0_3.Text = CStr(ch0(3))
        bat_txt_1_3.Text = CStr(ch1(3))
        bat_txt_2_3.Text = CStr(ch2(3))
        bat_txt_och_3.Text = CStr(och(3))
        '
        bat_txt_0_4.Text = CStr(ch0(4))
        bat_txt_1_4.Text = CStr(ch1(4))
        bat_txt_2_4.Text = CStr(ch2(4))
        bat_txt_och_4.Text = CStr(och(4))
        '
        bat_txt_0_5.Text = CStr(ch0(5))
        bat_txt_1_5.Text = CStr(ch1(5))
        bat_txt_2_5.Text = CStr(ch2(5))
        bat_txt_och_5.Text = CStr(och(5))
        '
        Call trace_charge_bat()
        '
        ' Discharge behaviour parameters
        ' ------------------------------
        '
        bat_txt_d0_0.Text = CStr(de0(0))
        bat_txt_d1_0.Text = CStr(de1(0))
        bat_txt_d2_0.Text = CStr(de2(0))
        bat_txt_d100_0.Text = CStr(de100(0))
        '
        bat_txt_d0_1.Text = CStr(de0(1))
        bat_txt_d1_1.Text = CStr(de1(1))
        bat_txt_d2_1.Text = CStr(de2(1))
        bat_txt_d100_1.Text = CStr(de100(1))
        '
        bat_txt_d0_2.Text = CStr(de0(2))
        bat_txt_d1_2.Text = CStr(de1(2))
        bat_txt_d2_2.Text = CStr(de2(2))
        bat_txt_d100_2.Text = CStr(de100(2))
        '
        bat_txt_d0_3.Text = CStr(de0(3))
        bat_txt_d1_3.Text = CStr(de1(3))
        bat_txt_d2_3.Text = CStr(de2(3))
        bat_txt_d100_3.Text = CStr(de100(3))
        '
        bat_txt_d0_4.Text = CStr(de0(4))
        bat_txt_d1_4.Text = CStr(de1(4))
        bat_txt_d2_4.Text = CStr(de2(4))
        bat_txt_d100_4.Text = CStr(de100(4))
        '
        bat_txt_d0_5.Text = CStr(de0(5))
        bat_txt_d1_5.Text = CStr(de1(5))
        bat_txt_d2_5.Text = CStr(de2(5))
        bat_txt_d100_5.Text = CStr(de100(5))
        '
        bat_txt_cmax.Text = CStr(cmax_cnom)
        bat_txt_c10.Text = CStr(c10_cnom)
        bat_txt_tmin.Text = CStr(tbatmin)
        bat_txt_cmin.Text = CStr(cmin100)
        bat_txt_t99.Text = CStr(tbat99)
        Call trace_capacity_temp()
        Call trace_discharge_bat()
        '
        ' Self-discharge behaviour parameters
        ' -----------------------------------
        bat_txt_tc0.Text = CStr(tc0)
        bat_txt_tc50.Text = CStr(tc50)
        Call trace_autodecharge()
        '
        ' ------------------
        ' Thermical data TAB
        ' ------------------
        thr_lblPanName_1.Text = ""
        thr_lblPanName_2.Text = ""
        thr_lblPanName_3.Text = ""
        thr_lblPanName_4.Text = ""
        thr_lblPanName_5.Text = ""
        thr_lblPanName_6.Text = ""
        thr_lblPanName_7.Text = ""
        thr_lblPanName_8.Text = ""
        '
        For i = 0 To frmAttConf.mass_lstPanels.Items.Count - 1
            Select Case i
                Case 0 : thr_lblPanName_1.Text = frmAttConf.mass_lstPanels.Items.Item(i)
                Case 1 : thr_lblPanName_2.Text = frmAttConf.mass_lstPanels.Items.Item(i)
                Case 2 : thr_lblPanName_3.Text = frmAttConf.mass_lstPanels.Items.Item(i)
                Case 3 : thr_lblPanName_4.Text = frmAttConf.mass_lstPanels.Items.Item(i)
                Case 4 : thr_lblPanName_5.Text = frmAttConf.mass_lstPanels.Items.Item(i)
                Case 5 : thr_lblPanName_6.Text = frmAttConf.mass_lstPanels.Items.Item(i)
                Case 6 : thr_lblPanName_7.Text = frmAttConf.mass_lstPanels.Items.Item(i)
                Case 7 : thr_lblPanName_8.Text = frmAttConf.mass_lstPanels.Items.Item(i)
            End Select
        Next
        '
        update_thermique()
        '
        ' --------------
        ' Management TAB
        ' --------------
        gst_txt_chvmax.Text = CStr(chvmax)
        gst_txt_kcharge.Text = CStr(kcharge)
        gst_txt_ichmax.Text = CStr(ichmax)
        gst_txt_chini.Text = CStr(chini)
        gst_txt_cour_ent.Text = CStr(bat_courant_entretien)
        gst_txt_pdem_0.Text = CStr(pdem(0))
        gst_txt_diss_0.Text = CStr(diss(0))
        gst_txt_pdem_1.Text = CStr(pdem(1))
        gst_txt_diss_1.Text = CStr(diss(1))
        gst_txt_rendgs.Text = CStr(rendgs)
        gst_txt_Vregul.Text = CStr(Vregul)
        gst_txt_rendbat.Text = CStr(rendbat)
        If gst_type = 1 Then
            gst_opt_regul_0.Checked = True
            gst_opt_regul_1.Checked = False
            gst_txt_Vregul.Enabled = True
        Else
            gst_opt_regul_1.Checked = True
            gst_opt_regul_0.Checked = False
            gst_txt_Vregul.Enabled = False
        End If
        '
        ' ---------------
        ' Solar cells TAB
        ' ---------------
        Select Case cell_type
            Case 0
                cel_txt_isc.Enabled = False
                cel_txt_imp.Enabled = False
                cel_txt_voc.Enabled = False
                cel_txt_vmp.Enabled = False
                cel_txt_disc.Enabled = False
                cel_txt_dimp.Enabled = False
                cel_txt_dvoc.Enabled = False
                cel_txt_dvmp.Enabled = False
                cel_txt_surf.Enabled = False
                cel_txt_eff.Enabled = False
            Case 1
                cel_txt_isc.Enabled = False
                cel_txt_imp.Enabled = False
                cel_txt_voc.Enabled = False
                cel_txt_vmp.Enabled = False
                cel_txt_disc.Enabled = False
                cel_txt_dimp.Enabled = False
                cel_txt_dvoc.Enabled = False
                cel_txt_dvmp.Enabled = False
                cel_txt_surf.Enabled = False
                cel_txt_eff.Enabled = False
            Case 2
                cel_txt_isc.Enabled = True
                cel_txt_imp.Enabled = True
                cel_txt_voc.Enabled = True
                cel_txt_vmp.Enabled = True
                cel_txt_disc.Enabled = True
                cel_txt_dimp.Enabled = True
                cel_txt_dvoc.Enabled = True
                cel_txt_dvmp.Enabled = True
                cel_txt_surf.Enabled = True
                cel_txt_eff.Enabled = True
        End Select
        cel_cmb_type.SelectedIndex = cell_type
        cel_txt_surf.Text = Format(cell_surf, "#0.00")
        cel_txt_eff.Text = Format(cell_eff * 100, "0.0")
        cel_txt_tref.Text = CStr(cell_tref)
        cel_txt_isc.Text = CStr(cell_isc)
        cel_txt_imp.Text = CStr(cell_imp)
        cel_txt_voc.Text = CStr(cell_voc)
        cel_txt_vmp.Text = CStr(cell_vmp)
        cel_txt_disc.Text = CStr(cell_disc)
        cel_txt_dimp.Text = CStr(cell_dimp)
        cel_txt_dvoc.Text = CStr(cell_dvoc)
        cel_txt_dvmp.Text = CStr(cell_dvmp)
        cel_txt_radisc.Text = CStr(cell_radisc)
        cel_txt_radimp.Text = CStr(cell_radimp)
        cel_txt_radvoc.Text = CStr(cell_radvoc)
        cel_txt_radvmp.Text = CStr(cell_radvmp)
        '
        Select Case cel_opt_beol_0.Checked
            Case True ' BOL
                cel_txt_radisc.Enabled = False
                cel_txt_radimp.Enabled = False
                cel_txt_radvoc.Enabled = False
                cel_txt_radvmp.Enabled = False
                cel_txt_radisc.Text = "1"
                cel_txt_radimp.Text = "1"
                cel_txt_radvoc.Text = "1"
                cel_txt_radvmp.Text = "1"
                cel_txt_eff.Text = Format(cell_eff * 100, "0.0")
            Case False ' EOL
                cel_txt_radisc.Enabled = True
                cel_txt_radimp.Enabled = True
                cel_txt_radvoc.Enabled = True
                cel_txt_radvmp.Enabled = True
                cel_txt_radisc.Text = CStr(cell_radisc)
                cel_txt_radimp.Text = CStr(cell_radimp)
                cel_txt_radvoc.Text = CStr(cell_radvoc)
                cel_txt_radvmp.Text = CStr(cell_radvmp)
                cel_txt_eff.Text = Format(cell_eff * 100, "0.0")
        End Select
        '
        cel_txt_ns.Text = CStr(cell_ns)
        cel_txt_tetad.Text = CStr(cell_tetad)
        cel_txt_tetam.Text = CStr(cell_tetam)
        dj50 = CDbl(th_idat)
        cel_opt_puiss.Checked = False
        cel_opt_cour.Checked = True
        Call graph_cellule()
        Call graph_cosaprox()
        '
        Exit Sub
        '
err_2:
        ''
    End Sub

    Private Sub update_thermique()
        '
        ' Update the thermical tab
        '
        Dim i As Short
        '
        ' Default values
        thr_lblPanName_1.Text = "" : thr_txtCspec_1.Text = 0 : thr_txtCellAlf_1.Text = 0 : thr_txtCellEps_1.Text = 0 : thr_txtBackAlf_1.Text = 0 : thr_txtBackEps_1.Text = 0 : thr_txtCond_1.Text = 0
        thr_lblPanName_2.Text = "" : thr_txtCspec_2.Text = 0 : thr_txtCellAlf_2.Text = 0 : thr_txtCellEps_2.Text = 0 : thr_txtBackAlf_2.Text = 0 : thr_txtBackEps_2.Text = 0 : thr_txtCond_2.Text = 0
        thr_lblPanName_3.Text = "" : thr_txtCspec_3.Text = 0 : thr_txtCellAlf_3.Text = 0 : thr_txtCellEps_3.Text = 0 : thr_txtBackAlf_3.Text = 0 : thr_txtBackEps_3.Text = 0 : thr_txtCond_3.Text = 0
        thr_lblPanName_4.Text = "" : thr_txtCspec_4.Text = 0 : thr_txtCellAlf_4.Text = 0 : thr_txtCellEps_4.Text = 0 : thr_txtBackAlf_4.Text = 0 : thr_txtBackEps_4.Text = 0 : thr_txtCond_4.Text = 0
        thr_lblPanName_5.Text = "" : thr_txtCspec_5.Text = 0 : thr_txtCellAlf_5.Text = 0 : thr_txtCellEps_5.Text = 0 : thr_txtBackAlf_5.Text = 0 : thr_txtBackEps_5.Text = 0 : thr_txtCond_5.Text = 0
        thr_lblPanName_6.Text = "" : thr_txtCspec_6.Text = 0 : thr_txtCellAlf_6.Text = 0 : thr_txtCellEps_6.Text = 0 : thr_txtBackAlf_6.Text = 0 : thr_txtBackEps_6.Text = 0 : thr_txtCond_6.Text = 0
        thr_lblPanName_7.Text = "" : thr_txtCspec_7.Text = 0 : thr_txtCellAlf_7.Text = 0 : thr_txtCellEps_7.Text = 0 : thr_txtBackAlf_7.Text = 0 : thr_txtBackEps_7.Text = 0 : thr_txtCond_7.Text = 0
        thr_lblPanName_8.Text = "" : thr_txtCspec_8.Text = 0 : thr_txtCellAlf_8.Text = 0 : thr_txtCellEps_8.Text = 0 : thr_txtBackAlf_8.Text = 0 : thr_txtBackEps_8.Text = 0 : thr_txtCond_8.Text = 0
        ' All control are disabled
        thr_lblPanName_1.Enabled = False : thr_txtCspec_1.Enabled = False : thr_txtCellAlf_1.Enabled = False : thr_txtCellEps_1.Enabled = False : thr_txtBackAlf_1.Enabled = False : thr_txtBackEps_1.Enabled = False : thr_txtCond_1.Enabled = False
        thr_lblPanName_2.Enabled = False : thr_txtCspec_2.Enabled = False : thr_txtCellAlf_2.Enabled = False : thr_txtCellEps_2.Enabled = False : thr_txtBackAlf_2.Enabled = False : thr_txtBackEps_2.Enabled = False : thr_txtCond_2.Enabled = False
        thr_lblPanName_3.Enabled = False : thr_txtCspec_3.Enabled = False : thr_txtCellAlf_3.Enabled = False : thr_txtCellEps_3.Enabled = False : thr_txtBackAlf_3.Enabled = False : thr_txtBackEps_3.Enabled = False : thr_txtCond_3.Enabled = False
        thr_lblPanName_4.Enabled = False : thr_txtCspec_4.Enabled = False : thr_txtCellAlf_4.Enabled = False : thr_txtCellEps_4.Enabled = False : thr_txtBackAlf_4.Enabled = False : thr_txtBackEps_4.Enabled = False : thr_txtCond_4.Enabled = False
        thr_lblPanName_5.Enabled = False : thr_txtCspec_5.Enabled = False : thr_txtCellAlf_5.Enabled = False : thr_txtCellEps_5.Enabled = False : thr_txtBackAlf_5.Enabled = False : thr_txtBackEps_5.Enabled = False : thr_txtCond_5.Enabled = False
        thr_lblPanName_6.Enabled = False : thr_txtCspec_6.Enabled = False : thr_txtCellAlf_6.Enabled = False : thr_txtCellEps_6.Enabled = False : thr_txtBackAlf_6.Enabled = False : thr_txtBackEps_6.Enabled = False : thr_txtCond_6.Enabled = False
        thr_lblPanName_7.Enabled = False : thr_txtCspec_7.Enabled = False : thr_txtCellAlf_7.Enabled = False : thr_txtCellEps_7.Enabled = False : thr_txtBackAlf_7.Enabled = False : thr_txtBackEps_7.Enabled = False : thr_txtCond_7.Enabled = False
        thr_lblPanName_8.Enabled = False : thr_txtCspec_8.Enabled = False : thr_txtCellAlf_8.Enabled = False : thr_txtCellEps_8.Enabled = False : thr_txtBackAlf_8.Enabled = False : thr_txtBackEps_8.Enabled = False : thr_txtCond_8.Enabled = False
        '
        thr_txtTBat.Text = T_battery
        '
        thr_txtBodyCspec.Text = BodySpecHeat
        thr_txtInitBodyTemp.Text = T_init_body
        '
        thr_txtBodyAlf_px.Text = BodyAlf(0)
        thr_txtBodyAlf_mx.Text = BodyAlf(1)
        thr_txtBodyAlf_py.Text = BodyAlf(2)
        thr_txtBodyAlf_my.Text = BodyAlf(3)
        thr_txtBodyAlf_pz.Text = BodyAlf(4)
        thr_txtBodyAlf_mz.Text = BodyAlf(5)
        '
        thr_txtBodyEps_px.Text = BodyEps(0)
        thr_txtBodyEps_mx.Text = BodyEps(1)
        thr_txtBodyEps_py.Text = BodyEps(2)
        thr_txtBodyEps_my.Text = BodyEps(3)
        thr_txtBodyEps_pz.Text = BodyEps(4)
        thr_txtBodyEps_mz.Text = BodyEps(5)
        '
        If SolarPanels.Count() = 0 Then Exit Sub
        For i = 1 To SolarPanels.Count()
            With SolarPanels.Item(i)
                Select Case i
                    Case 1
                        thr_lblPanName_1.Enabled = True : thr_txtCspec_1.Enabled = True : thr_txtCellAlf_1.Enabled = True : thr_txtCellEps_1.Enabled = True : thr_txtBackAlf_1.Enabled = True : thr_txtBackEps_1.Enabled = True : thr_txtCond_1.Enabled = True
                        thr_lblPanName_1.Text = .PanelName : thr_txtCspec_1.Text = .PanelSpecHeat : thr_txtCellAlf_1.Text = .PanelCellAlf
                        thr_txtCellEps_1.Text = .PanelCellEps : thr_txtBackAlf_1.Text = .PanelBackAlf : thr_txtBackEps_1.Text = .PanelBackEps
                        thr_txtCond_1.Text = .PanelConduction
                    Case 2
                        thr_lblPanName_2.Enabled = True : thr_txtCspec_2.Enabled = True : thr_txtCellAlf_2.Enabled = True : thr_txtCellEps_2.Enabled = True : thr_txtBackAlf_2.Enabled = True : thr_txtBackEps_2.Enabled = True : thr_txtCond_2.Enabled = True
                        thr_lblPanName_2.Text = .PanelName : thr_txtCspec_2.Text = .PanelSpecHeat : thr_txtCellAlf_2.Text = .PanelCellAlf
                        thr_txtCellEps_2.Text = .PanelCellEps : thr_txtBackAlf_2.Text = .PanelBackAlf : thr_txtBackEps_2.Text = .PanelBackEps
                        thr_txtCond_2.Text = .PanelConduction
                    Case 3
                        thr_lblPanName_3.Enabled = True : thr_txtCspec_3.Enabled = True : thr_txtCellAlf_3.Enabled = True : thr_txtCellEps_3.Enabled = True : thr_txtBackAlf_3.Enabled = True : thr_txtBackEps_3.Enabled = True : thr_txtCond_3.Enabled = True
                        thr_lblPanName_3.Text = .PanelName : thr_txtCspec_3.Text = .PanelSpecHeat : thr_txtCellAlf_3.Text = .PanelCellAlf
                        thr_txtCellEps_3.Text = .PanelCellEps : thr_txtBackAlf_3.Text = .PanelBackAlf : thr_txtBackEps_3.Text = .PanelBackEps
                        thr_txtCond_3.Text = .PanelConduction
                    Case 4
                        thr_lblPanName_4.Enabled = True : thr_txtCspec_4.Enabled = True : thr_txtCellAlf_4.Enabled = True : thr_txtCellEps_4.Enabled = True : thr_txtBackAlf_4.Enabled = True : thr_txtBackEps_4.Enabled = True : thr_txtCond_4.Enabled = True
                        thr_lblPanName_4.Text = .PanelName : thr_txtCspec_4.Text = .PanelSpecHeat : thr_txtCellAlf_4.Text = .PanelCellAlf
                        thr_txtCellEps_4.Text = .PanelCellEps : thr_txtBackAlf_4.Text = .PanelBackAlf : thr_txtBackEps_4.Text = .PanelBackEps
                        thr_txtCond_4.Text = .PanelConduction
                    Case 5
                        thr_lblPanName_5.Enabled = True : thr_txtCspec_5.Enabled = True : thr_txtCellAlf_5.Enabled = True : thr_txtCellEps_5.Enabled = True : thr_txtBackAlf_5.Enabled = True : thr_txtBackEps_5.Enabled = True : thr_txtCond_5.Enabled = True
                        thr_lblPanName_5.Text = .PanelName : thr_txtCspec_5.Text = .PanelSpecHeat : thr_txtCellAlf_5.Text = .PanelCellAlf
                        thr_txtCellEps_5.Text = .PanelCellEps : thr_txtBackAlf_5.Text = .PanelBackAlf : thr_txtBackEps_5.Text = .PanelBackEps
                        thr_txtCond_5.Text = .PanelConduction
                    Case 6
                        thr_lblPanName_6.Enabled = True : thr_txtCspec_6.Enabled = True : thr_txtCellAlf_6.Enabled = True : thr_txtCellEps_6.Enabled = True : thr_txtBackAlf_6.Enabled = True : thr_txtBackEps_6.Enabled = True : thr_txtCond_6.Enabled = True
                        thr_lblPanName_6.Text = .PanelName : thr_txtCspec_6.Text = .PanelSpecHeat : thr_txtCellAlf_6.Text = .PanelCellAlf
                        thr_txtCellEps_6.Text = .PanelCellEps : thr_txtBackAlf_6.Text = .PanelBackAlf : thr_txtBackEps_6.Text = .PanelBackEps
                        thr_txtCond_6.Text = .PanelConduction
                    Case 7
                        thr_lblPanName_7.Enabled = True : thr_txtCspec_7.Enabled = True : thr_txtCellAlf_7.Enabled = True : thr_txtCellEps_7.Enabled = True : thr_txtBackAlf_7.Enabled = True : thr_txtBackEps_7.Enabled = True : thr_txtCond_7.Enabled = True
                        thr_lblPanName_7.Text = .PanelName : thr_txtCspec_7.Text = .PanelSpecHeat : thr_txtCellAlf_7.Text = .PanelCellAlf
                        thr_txtCellEps_7.Text = .PanelCellEps : thr_txtBackAlf_7.Text = .PanelBackAlf : thr_txtBackEps_7.Text = .PanelBackEps
                        thr_txtCond_7.Text = .PanelConduction
                    Case 8
                        thr_lblPanName_8.Enabled = True : thr_txtCspec_8.Enabled = True : thr_txtCellAlf_8.Enabled = True : thr_txtCellEps_8.Enabled = True : thr_txtBackAlf_8.Enabled = True : thr_txtBackEps_8.Enabled = True : thr_txtCond_8.Enabled = True
                        thr_lblPanName_8.Text = .PanelName : thr_txtCspec_8.Text = .PanelSpecHeat : thr_txtCellAlf_8.Text = .PanelCellAlf
                        thr_txtCellEps_8.Text = .PanelCellEps : thr_txtBackAlf_8.Text = .PanelBackAlf : thr_txtBackEps_8.Text = .PanelBackEps
                        thr_txtCond_8.Text = .PanelConduction
                End Select
            End With
        Next
        '
        thr_txtInitTemp.Text = T_init_panels
        ''
    End Sub

    ''' <summary>
    ''' Define shortcuts Enter
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmPowerConf_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyData = Keys.Enter Then
            pwr_lblmsg.Focus()
            e.SuppressKeyPress = True ' Pas besoin que l'évènement KeyPress le gère on vient de le faire
        End If
    End Sub

#Region "Batteries data TAB"

    Private Sub trace_discharge_bat()
        '
        Dim tabXM1 As ArrayList
        Dim tabYM1 As ArrayList
        Dim tabXM2 As ArrayList
        Dim tabYM2 As ArrayList
        Dim tabXM3 As ArrayList
        Dim tabYM3 As ArrayList
        Dim tabXM4 As ArrayList
        Dim tabYM4 As ArrayList
        '
        Dim q(2) As Double
        Dim p(2) As Double
        Dim m As Double
        Dim x1 As Double
        Dim x2 As Double
        Dim v0 As Single
        Dim v1 As Single
        Dim v2 As Single
        Dim v100 As Single
        Dim i As Integer, j As Integer, k As Integer
        Dim X As Double, Y As Double
        Dim curr(3) As Single
        Dim t As Integer
        '
        '    Drawing graph
        '    -------------
        tabXM1 = New ArrayList
        tabYM1 = New ArrayList
        tabXM2 = New ArrayList
        tabYM2 = New ArrayList
        tabXM3 = New ArrayList
        tabYM3 = New ArrayList
        tabXM4 = New ArrayList
        tabYM4 = New ArrayList '
        '
        If bat_dch_opt_courant.Checked = True Then
            ' Tracé des courbes de décharge
            curr(0) = 0.2
            curr(1) = 0.5
            curr(2) = 1
            curr(3) = 2
            j = 0
            For i = 0 To 3
                ' Correction with current at 25°C
                x1 = (de1(0) + de1(2) * Math.Log10(curr(i))) / 100
                x2 = (de2(0) + de2(2) * Math.Log10(curr(i))) / 100
                v0 = de0(3) + de0(5) * Math.Log10(curr(i))
                v1 = de1(3) + de1(5) * Math.Log10(curr(i))
                v2 = de2(3) + de2(5) * Math.Log10(curr(i))
                v100 = de100(3) + de100(5) * Math.Log10(curr(i))
                m = (v2 - v1) / (x2 - x1)
                q(0) = (m * x1 + v0 - v1) / x1 / x1
                q(1) = m - 2 * x1 * q(0)
                q(2) = v0
                p(0) = (v2 - v100 + m * (1 - x2)) / (2 * x2 - 1 - x2 * x2)
                p(1) = m - 2 * x2 * p(0)
                p(2) = p(0) * (2 * x2 - 1) - m + v100
                With bat_decharge_graph
                    For k = 0 To 100
                        X = 0.01 * k
                        If X <= x1 Then
                            Y = q(0) * X * X + q(1) * X + q(2)
                        ElseIf X > x1 And X < x2 Then
                            Y = m * (X - x1) + v1
                        Else
                            Y = p(0) * X * X + p(1) * X + p(2)
                        End If
                        Select Case i
                            Case 0
                                tabXM1.Add(X * 100)
                                tabYM1.Add(Y)
                            Case 1
                                tabXM2.Add(X * 100)
                                tabYM2.Add(Y)
                            Case 2
                                tabXM3.Add(X * 100)
                                tabYM3.Add(Y)
                            Case 2
                                tabXM4.Add(X * 100)
                                tabYM4.Add(Y)
                        End Select
                    Next k
                End With
                j = j + 1
            Next i
            '
            With bat_decharge_graph
                .ChartAreas("ChartArea1").AxisX.Title = "State of discharge [%]"
                .ChartAreas("ChartArea1").AxisY.Title = "Voltage [V]"
                .Series.Clear()
                .Series.Add("0.2 C")
                .Series("0.2 C").Points.DataBindXY(tabXM1, tabYM1)
                .Series("0.2 C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("0.2 C").ShadowOffset = 1
                .Series("0.2 C").Color = Color.Blue
                '
                .Series.Add("0.5 C")
                .Series("0.5 C").Points.DataBindXY(tabXM2, tabYM2)
                .Series("0.5 C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("0.5 C").ShadowOffset = 1
                .Series("0.5 C").Color = Color.Yellow
                '
                .Series.Add("1 C")
                .Series("1 C").Points.DataBindXY(tabXM3, tabYM3)
                .Series("1 C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("1 C").ShadowOffset = 1
                .Series("1 C").Color = Color.Orange
                '
                .Series.Add("2 C")
                .Series("2 C").Points.DataBindXY(tabXM3, tabYM3)
                .Series("2 C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("2 C").ShadowOffset = 1
                .Series("2 C").Color = Color.Red
                '.Titles(0).Text = "Discharge characteristics at 25°C"
            End With
            '
        Else
            ' Traces 4 discharge curves related to different temperatures (-25, 0, 25, 50)
            For j = 0 To 3
                t = -25 + j * 25
                ' Correction with temperature at 1Cnom
                x1 = (de1(0) + de1(1) * (t - 25)) / 100
                x2 = (de2(0) + de2(1) * (t - 25)) / 100
                v0 = de0(3) + de0(4) * (t - 25)
                v1 = de1(3) + de1(4) * (t - 25)
                v2 = de2(3) + de2(4) * (t - 25)
                v100 = de100(3) + de100(4) * (t - 25)
                m = (v2 - v1) / (x2 - x1)
                q(0) = (m * x1 + v0 - v1) / x1 / x1
                q(1) = m - 2 * x1 * q(0)
                q(2) = v0
                p(0) = (v2 - v100 + m * (1 - x2)) / (2 * x2 - 1 - x2 * x2)
                p(1) = m - 2 * x2 * p(0)
                p(2) = p(0) * (2 * x2 - 1) - m + v100
                For i = 0 To 100
                    X = 0.01 * i
                    If X < x1 Then
                        Y = q(0) * X * X + q(1) * X + q(2)
                    ElseIf X >= x1 And X <= x2 Then
                        Y = m * (X - x1) + v1
                    Else
                        Y = p(0) * X * X + p(1) * X + p(2)
                    End If
                    Select Case j
                        Case 0
                            tabXM1.Add(X * 100)
                            tabYM1.Add(Y)
                        Case 1
                            tabXM2.Add(X * 100)
                            tabYM2.Add(Y)
                        Case 2
                            tabXM3.Add(X * 100)
                            tabYM3.Add(Y)
                        Case 3
                            tabXM4.Add(X * 100)
                            tabYM4.Add(Y)
                    End Select
                Next i
            Next j
            With bat_decharge_graph
                .Titles(0).Text = "Discharge characteristics at Cnom"
                .Series.Clear()
                .Series.Add("-25°C")
                .Series("-25°C").Points.DataBindXY(tabXM1, tabYM1)
                .Series("-25°C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("-25°C").ShadowOffset = 1
                .Series("-25°C").BorderWidth = 2
                .Series("-25°C").Color = Color.Blue
                '
                .Series.Add("0°C")
                .Series("0°C").Points.DataBindXY(tabXM2, tabYM2)
                .Series("0°C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("0°C").ShadowOffset = 1
                .Series("0°C").BorderWidth = 2
                .Series("0°C").Color = Color.Yellow
                '
                .Series.Add("25°C")
                .Series("25°C").Points.DataBindXY(tabXM3, tabYM3)
                .Series("25°C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("25°C").ShadowOffset = 1
                .Series("25°C").BorderWidth = 2
                .Series("25°C").Color = Color.Orange
                '
                .Series.Add("50°C")
                .Series("50°C").Points.DataBindXY(tabXM4, tabYM4)
                .Series("50°C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("50°C").ShadowOffset = 1
                .Series("50°C").BorderWidth = 2
                .Series("50°C").Color = Color.Red
            End With
            '
        End If
        ''
    End Sub

    Private Sub trace_capacity_temp()
        '
        ' Traces available capacity vs. temperature for 0.1C, 1C, 10C
        '
        Dim tabXM1 As ArrayList
        Dim tabYM1 As ArrayList
        Dim tabXM2 As ArrayList
        Dim tabYM2 As ArrayList
        Dim tabXM3 As ArrayList
        Dim tabYM3 As ArrayList
        Dim X As Single, Y As Single
        Dim i As Integer
        Dim n As Integer
        Dim T1 As Integer
        Dim T2 As Integer
        Dim t As Double
        '
        '    Drawing graph
        '    -------------
        tabXM1 = New ArrayList
        tabYM1 = New ArrayList
        tabXM2 = New ArrayList
        tabYM2 = New ArrayList
        tabXM3 = New ArrayList
        tabYM3 = New ArrayList
        '
        '
        If tbatmin > tbat99 Then
            T2 = Int(tbatmin)
            T1 = Int(tbat99)
        Else
            T1 = Int(tbatmin)
            T2 = Int(tbat99)
        End If
        n = Int(T2 - T1)
        '
        For i = 2 To n + 1
            t = T1 + (i - 1)
            X = t
            Y = derating_current(0.1) * derating_temp(t) * 100
            tabXM1.Add(X)
            tabYM1.Add(Y)
            Y = derating_current(1) * derating_temp(t) * 100
            tabXM2.Add(X)
            tabYM2.Add(Y)
            Y = derating_current(10) * derating_temp(t) * 100
            tabXM3.Add(X)
            tabYM3.Add(Y)
        Next i
        '
        With bat_cdispo_graph
            .Titles(0).Text = "Available capacity"
            .ChartAreas("ChartArea1").AxisX.Title = "Temperature [°C]"
            .ChartAreas("ChartArea1").AxisY.Title = "Capacity [Ah]"
            .Series.Clear()
            '
            .Series.Add("0.1 C")
            .Series("0.1 C").Points.DataBindXY(tabXM1, tabYM1)
            .Series("0.1 C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
            .Series("0.1 C").ShadowOffset = 1
            .Series("0.1 C").Color = Color.Blue
            '
            .Series.Add("1 C")
            .Series("1 C").Points.DataBindXY(tabXM2, tabYM2)
            .Series("1 C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
            .Series("1 C").ShadowOffset = 1
            .Series("1 C").Color = Color.Orange
            '
            .Series.Add("10 C")
            .Series("10 C").Points.DataBindXY(tabXM3, tabYM3)
            .Series("10 C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
            .Series("10 C").ShadowOffset = 1
            .Series("10 C").Color = Color.Red
        End With
        ''
    End Sub

    Private Sub trace_charge_bat()
        ' Dessins des graphes
        Dim tabXM1 As ArrayList
        Dim tabYM1 As ArrayList
        Dim tabXM2 As ArrayList
        Dim tabYM2 As ArrayList
        Dim tabXM3 As ArrayList
        Dim tabYM3 As ArrayList
        Dim tabXM4 As ArrayList
        Dim tabYM4 As ArrayList
        '
        Dim q(2) As Double
        Dim p(3) As Double
        Dim m As Double
        Dim x1 As Double
        Dim x2 As Double
        Dim xoc As Double
        Dim v0 As Single
        Dim v1 As Single
        Dim v2 As Single
        Dim Voc As Single
        Dim i As Integer
        Dim j As Integer
        Dim k As Integer
        Dim X As Double, Y As Double
        Dim curr(2) As Single
        Dim xend As Double
        Dim t As Integer
        '
        '    Drawing graph
        '    -------------
        tabXM1 = New ArrayList
        tabYM1 = New ArrayList
        tabXM2 = New ArrayList
        tabYM2 = New ArrayList
        tabXM3 = New ArrayList
        tabYM3 = New ArrayList
        tabXM4 = New ArrayList
        tabYM4 = New ArrayList
        '
        If bat_ch_opt_courant.Checked = True Then
            ' Tracé des courbes en courant de charge (1C, 0.5C, 0.2C)
            If och(1) >= 0 Then
                xend = (och(0) + och(2)) / 100 + 0.1
            Else
                xend = (och(0) - och(2)) / 100 + 0.1
            End If
            '
            curr(0) = 0.2
            curr(1) = 0.5
            curr(2) = 1
            j = 0
            For i = 0 To 2
                ' Correction with current at 25°C
                x1 = (ch1(0) + ch1(2) * Math.Log10(curr(i))) / 100
                x2 = (ch2(0) + ch2(2) * Math.Log10(curr(i))) / 100
                xoc = (och(0) + och(2) * Math.Log10(curr(i))) / 100
                v0 = ch0(3) + ch0(5) * Math.Log10(curr(i))
                v1 = ch1(3) + ch1(5) * Math.Log10(curr(i))
                v2 = ch2(3) + ch2(5) * Math.Log10(curr(i))
                Voc = och(3) + och(5) * Math.Log10(curr(i))
                m = (v2 - v1) / (x2 - x1)
                q(0) = (m * x1 + v0 - v1) / x1 / x1
                q(1) = m - 2 * x1 * q(0)
                q(2) = v0
                Call define_p3(m, x2, xoc, v2, Voc, p)
                For k = 0 To Math.Truncate(xend / 0.02) - 1
                    X = 0.02 * k

                    If X <= x1 Then
                        Y = q(0) * X * X + q(1) * X + q(2)
                    ElseIf X > x1 And X < x2 Then
                        Y = m * (X - x1) + v1
                    Else
                        Y = p(0) * X ^ 3 + p(1) * X * X + p(2) * X + p(3)
                    End If
                    Select Case i
                        Case 0
                            tabXM1.Add(X * 100)
                            tabYM1.Add(Y)
                        Case 1
                            tabXM2.Add(X * 100)
                            tabYM2.Add(Y)
                        Case 2
                            tabXM3.Add(X * 100)
                            tabYM3.Add(Y)
                    End Select
                Next k
                j = j + 2
            Next i
            '
            With bat_charge_graph
                .Titles(0).Text = "Charge characteristics at 25°C"
                .ChartAreas("ChartArea1").AxisX.Title = "State of charge [%]"
                .ChartAreas("ChartArea1").AxisY.Title = "Voltage [V]"
                .Series.Clear()
                .Series.Add("0.2 C")
                .Series("0.2 C").Points.DataBindXY(tabXM1, tabYM1)
                .Series("0.2 C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("0.2 C").ShadowOffset = 1
                .Series("0.2 C").BorderWidth = 2
                .Series("0.2 C").Color = Color.Yellow
                '
                .Series.Add("0.5 C")
                .Series("0.5 C").Points.DataBindXY(tabXM2, tabYM2)
                .Series("0.5 C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("0.5 C").ShadowOffset = 1
                .Series("0.5 C").BorderWidth = 2
                .Series("0.5 C").Color = Color.Orange
                '
                .Series.Add("1 C")
                .Series("1 C").Points.DataBindXY(tabXM3, tabYM3)
                .Series("1 C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("1 C").ShadowOffset = 1
                .Series("1 C").BorderWidth = 2
                .Series("1 C").Color = Color.Red
            End With
            '
        Else
            ' Tracé des courbe en températures (-25, 0, 25, 50)
            If och(1) >= 0 Then
                xend = (och(0) + och(1) * 25) / 100 + 0.1
            Else
                xend = (och(0) - och(1) * 75) / 100 + 0.1
            End If
            '
            For j = 0 To 3
                t = -25 + j * 25
                ' Correction with temperature at 1Cnom
                x1 = (ch1(0) + ch1(1) * (t - 25)) / 100
                x2 = (ch2(0) + ch2(1) * (t - 25)) / 100
                xoc = (och(0) + och(1) * (t - 25)) / 100
                v0 = ch0(3) + ch0(4) * (t - 25)
                v1 = ch1(3) + ch1(4) * (t - 25)
                v2 = ch2(3) + ch2(4) * (t - 25)
                Voc = och(3) + och(4) * (t - 25)
                m = (v2 - v1) / (x2 - x1)
                q(0) = (m * x1 + v0 - v1) / x1 / x1
                q(1) = m - 2 * x1 * q(0)
                q(2) = v0
                Call define_p3(m, x2, xoc, v2, Voc, p)
                For i = 0 To Math.Truncate(xend / 0.02) - 1
                    X = 0.02 * i
                    If X <= x1 Then
                        Y = q(0) * X * X + q(1) * X + q(2)
                    ElseIf X > x1 And X < x2 Then
                        Y = m * (X - x1) + v1
                    Else
                        Y = p(0) * X ^ 3 + p(1) * X * X + p(2) * X + p(3)
                    End If
                    '
                    Select Case j
                        Case 0
                            tabXM1.Add(X * 100)
                            tabYM1.Add(Y)
                        Case 1
                            tabXM2.Add(X * 100)
                            tabYM2.Add(Y)
                        Case 2
                            tabXM3.Add(X * 100)
                            tabYM3.Add(Y)
                        Case 3
                            tabXM4.Add(X * 100)
                            tabYM4.Add(Y)
                    End Select
                Next i
            Next j
            '
            With bat_charge_graph
                .Titles(0).Text = "Charge characteristics at Cnom"
                .ChartAreas("ChartArea1").AxisX.Title = "State of charge [%]"
                .ChartAreas("ChartArea1").AxisY.Title = "Voltage [V]"
                .Series.Clear()
                .Series.Add("-25°C")
                .Series("-25°C").Points.DataBindXY(tabXM1, tabYM1)
                .Series("-25°C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("-25°C").ShadowOffset = 1
                .Series("-25°C").Color = Color.Blue
                '
                .Series.Add("0°C")
                .Series("0°C").Points.DataBindXY(tabXM2, tabYM2)
                .Series("0°C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("0°C").ShadowOffset = 1
                .Series("0°C").Color = Color.Yellow
                '
                .Series.Add("25°C")
                .Series("25°C").Points.DataBindXY(tabXM3, tabYM3)
                .Series("25°C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("25°C").ShadowOffset = 1
                .Series("25°C").Color = Color.Orange
                '
                .Series.Add("50°C")
                .Series("50°C").Points.DataBindXY(tabXM4, tabYM4)
                .Series("50°C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
                .Series("50°C").ShadowOffset = 1
                .Series("50°C").Color = Color.Red
            End With
            '
        End If

    End Sub

    Private Sub trace_autodecharge()
        '
        ' Traces self_discharge curves
        '
        Dim tabXM1 As ArrayList
        Dim tabYM1 As ArrayList
        Dim tabXM2 As ArrayList
        Dim tabYM2 As ArrayList
        Dim tabXM3 As ArrayList
        Dim tabYM3 As ArrayList
        Dim X As Single, Y As Single
        Dim i As Integer
        Dim n As Integer
        '
        '    Drawing graph
        '    -------------
        tabXM1 = New ArrayList
        tabYM1 = New ArrayList
        tabXM2 = New ArrayList
        tabYM2 = New ArrayList
        tabXM3 = New ArrayList
        tabYM3 = New ArrayList
        '
        n = Int(tc0 * 1.5)
        '
        For i = 0 To n - 1
            X = i
            Y = 100 * Math.Exp(-i / tc0)
            tabXM1.Add(X)
            tabYM1.Add(Y)
            Y = 100 * Math.Exp(-i / (tc0 * Math.Exp(Math.Log(tc50 / tc0) * 25 / 50)))
            tabXM2.Add(X)
            tabYM2.Add(Y)
            Y = 100 * Math.Exp(-i / tc50)
            tabXM3.Add(X)
            tabYM3.Add(Y)
        Next i
        '
        With bat_auto_graph
            .Titles(0).Text = "Batteries self-discharge"
            .ChartAreas("ChartArea1").AxisY.Title = "State of Charge [%]"
            .ChartAreas("ChartArea1").AxisX.Title = "Time [days]"
            .ChartAreas(0).AxisY.Minimum = 0
            .ChartAreas(0).AxisY.Maximum = 100
            .Series.Clear()
            '
            .Series.Add("0°C")
            .Series("0°C").Points.DataBindXY(tabXM1, tabYM1)
            .Series("0°C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
            .Series("0°C").ShadowOffset = 1
            .Series("0°C").Color = Color.Blue
            '
            .Series.Add("25°C")
            .Series("25°C").Points.DataBindXY(tabXM2, tabYM2)
            .Series("25°C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
            .Series("25°C").ShadowOffset = 1
            .Series("25°C").Color = Color.Orange
            '
            .Series.Add("50°C")
            .Series("50°C").Points.DataBindXY(tabXM3, tabYM3)
            .Series("50°C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
            .Series("50°C").ShadowOffset = 1
            .Series("50°C").Color = Color.Red
        End With
        '


        '
        '  
    End Sub

#Region "Reading values"

    Private Sub bat_cmb_type_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_cmb_type.SelectedIndexChanged
        '
        bat_type = bat_cmb_type.SelectedIndex
        '
        ' Mise à jour des caractéristiques selon type
        Select Case bat_type
            Case 0, 1 ' NiCd, NiH2
                ' Charge
                ch0(0) = 0 : ch0(1) = 0 : ch0(2) = 0 : ch0(3) = 1.3 : ch0(4) = 0 : ch0(5) = 0
                ch1(0) = 15 : ch1(1) = 0.1 : ch1(2) = -5 : ch1(3) = 1.4 : ch1(4) = -0.001 : ch1(5) = 0.01
                ch2(0) = 65 : ch2(1) = 0.5 : ch2(2) = -5 : ch2(3) = 1.41 : ch2(4) = -0.001 : ch2(5) = 0.01
                och(0) = 100 : och(1) = 0.1 : och(2) = -5 : och(3) = 1.59 : och(4) = -0.002 : och(5) = 0.02
                '  och(0) = 120 : ... old value. What means?, or it is a bug? 
                ' Discharge
                de0(0) = 0 : de0(1) = 0 : de0(2) = 0 : de0(3) = 1.3 : de0(4) = 0 : de0(5) = 0
                de1(0) = 15 : de1(1) = 0.1 : de1(2) = -5 : de1(3) = 1.21 : de1(4) = 0.001 : de1(5) = -0.01
                de2(0) = 95 : de2(1) = 0.1 : de2(2) = -2 : de2(3) = 1.2 : de2(4) = 0.001 : de2(5) = -0.01
                de100(0) = 100 : de100(1) = 0 : de100(2) = 0 : de100(3) = 1.0# : de100(4) = 0 : de100(5) = 0
                cmax_cnom = 1.2
                c10_cnom = 0.6
                tbatmin = -50
                cmin100 = 30
                tbat99 = 50
                ' Self-discharge
                tc0 = 100
                tc50 = 15
                bat_frm_charge.Enabled = False
                bat_frm_decharge.Enabled = False
                '
                ' coef de recharge et tension par défaut dans gestion énergie
                kcharge = 1.05
                gst_txt_kcharge.Text = CStr(kcharge)
                chvmax = 1.55
                gst_txt_chvmax.Text = CStr(chvmax)

            Case 2 ' Li-Ion
                ' Charge
                ch0(0) = 0 : ch0(1) = 0 : ch0(2) = 0 : ch0(3) = 3.3 : ch0(4) = 0 : ch0(5) = 0
                ch1(0) = 20 : ch1(1) = 0 : ch1(2) = -5 : ch1(3) = 4.15 : ch1(4) = -0.001 : ch1(5) = 0.4
                ch2(0) = 80 : ch2(1) = 0 : ch2(2) = -5 : ch2(3) = 4.15 : ch2(4) = -0.001 : ch2(5) = 0.15
                och(0) = 100 : och(1) = 0 : och(2) = -5 : och(3) = 4.15 : och(4) = -0.001 : och(5) = 0.15
                ' Discharge
                de0(0) = 0 : de0(1) = 0 : de0(2) = 0 : de0(3) = 4.1 : de0(4) = 0 : de0(5) = 0
                de1(0) = 20 : de1(1) = 0.1 : de1(2) = -5 : de1(3) = 3.6 : de1(4) = 0.002 : de1(5) = -0.4
                de2(0) = 80 : de2(1) = 0.1 : de2(2) = -2 : de2(3) = 3.4 : de2(4) = 0.002 : de2(5) = -0.4
                de100(0) = 100 : de100(1) = 0 : de100(2) = 0 : de100(3) = 2.8 : de100(4) = 0 : de100(5) = 0
                cmax_cnom = 1.0#
                c10_cnom = 0.8
                tbatmin = -50
                cmin100 = 30
                tbat99 = 50
                ' Self-discharge
                tc0 = 500
                tc50 = 300
                bat_frm_charge.Enabled = False
                bat_frm_decharge.Enabled = False
                '
                ' coef de recharge et tension par défaut dans gestion énergie
                kcharge = 1.0#
                gst_txt_kcharge.Text = CStr(kcharge)
                chvmax = 4.0#
                gst_txt_chvmax.Text = CStr(chvmax)
                '
            Case 3
                bat_frm_charge.Enabled = True
                bat_frm_decharge.Enabled = True
                '
        End Select
        '
        ' Mise à jour
        ' -----------
        update_power_conf()
        'blnIsNewPrj = True
        ''
    End Sub

    Private Sub bat_ch_opt_courant_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_ch_opt_courant.CheckedChanged
        '
        If eventSender.Checked Then
            trace_charge_bat()
        End If
        ''
    End Sub
    Private Sub bat_ch_opt_temp_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_ch_opt_temp.CheckedChanged
        '
        If eventSender.Checked Then
            trace_charge_bat()
        End If
        ''
    End Sub

    Private Sub bat_dch_opt_courant_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_dch_opt_courant.CheckedChanged
        '
        If eventSender.Checked Then
            trace_discharge_bat()
        End If
        ''
    End Sub
    Private Sub bat_dch_opt_temp_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_dch_opt_temp.CheckedChanged
        '
        If eventSender.Checked Then
            trace_discharge_bat()
        End If
        ''
    End Sub

    Private Sub bat_txt_ns_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_ns.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_ns.Text) AndAlso bat_txt_ns.Text = Int(bat_txt_ns.Text) Then
            X = CDbl(bat_txt_ns.Text)
            X = value_down3(X, 1, True)
            bat_ns = CLng(X)
            bat_txt_ns.Text = Format(bat_ns, "#0")
        Else
            MsgBox("Please enter a non-zero integer !")
            bat_txt_ns.Text = Format(bat_ns, "#0")
            bat_txt_ns.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_np_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_np.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_np.Text) AndAlso bat_txt_np.Text = Int(bat_txt_np.Text) Then
            X = CDbl(bat_txt_np.Text)
            X = value_down3(X, 1, True)
            bat_np = CLng(X)
            bat_txt_np.Text = Format(bat_np, "#0")
        Else
            MsgBox("Please enter a non-zero integer !")
            bat_txt_np.Text = Format(bat_np, "#0")
            bat_txt_np.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub bat_txt_0_0_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_0_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_0_0.Text) Then
            X = CDbl(bat_txt_0_0.Text)
            X = value_down3(X, 0, True)
            ch0(0) = CSng(X)
            bat_txt_0_0.Text = Format(ch0(0), "#0.0")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_0_0.Text = Format(ch0(0), "#0.0")
            bat_txt_0_0.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_0_1_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_0_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_0_1.Text) Then
            X = CDbl(bat_txt_0_1.Text)
            X = value_down3(X, 0, True)
            ch0(1) = CSng(X)
            bat_txt_0_1.Text = Format(ch0(1), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_0_1.Text = Format(ch0(1), "#0.000")
            bat_txt_0_1.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_0_2_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_0_2.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_0_2.Text) Then
            X = CDbl(bat_txt_0_2.Text)
            X = value_down3(X, 0, True)
            ch0(2) = CSng(X)
            bat_txt_0_2.Text = Format(ch0(2), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_0_2.Text = Format(ch0(2), "#0.000")
            bat_txt_0_2.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_0_3_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_0_3.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_0_3.Text) Then
            X = CDbl(bat_txt_0_3.Text)
            X = value_limiter3(X, 1, 4.9999)
            ch0(3) = CSng(X)
            bat_txt_0_3.Text = Format(ch0(3), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_0_3.Text = Format(ch0(3), "#0.000")
            bat_txt_0_3.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_0_4_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_0_4.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_0_4.Text) Then
            X = CDbl(bat_txt_0_4.Text)
            X = value_up3(Math.Abs(X), 0.01, True) * Math.Sign(X)
            ch0(4) = CSng(X)
            bat_txt_0_4.Text = Format(ch0(4), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_0_4.Text = Format(ch0(4), "#0.000")
            bat_txt_0_4.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_0_5_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_0_5.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_0_5.Text) Then
            X = CDbl(bat_txt_0_5.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            ch0(5) = CSng(X)
            bat_txt_0_5.Text = Format(ch0(5), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_0_5.Text = Format(ch0(5), "#0.000")
            bat_txt_0_5.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub bat_txt_1_0_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_1_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_1_0.Text) Then
            X = CDbl(bat_txt_1_0.Text)
            X = value_limiter3(X, ch0(0) + 0.0001, ch2(0) - 0.0001)
            ch1(0) = CSng(X)
            bat_txt_1_0.Text = Format(ch1(0), "#0.0")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_1_0.Text = Format(ch1(0), "#0.0")
            bat_txt_1_0.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_1_1_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_1_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_1_1.Text) Then
            X = CDbl(bat_txt_1_1.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            ch1(1) = CSng(X)
            bat_txt_1_1.Text = Format(ch1(1), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_1_1.Text = Format(ch1(1), "#0.000")
            bat_txt_1_1.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_1_2_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_1_2.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_1_2.Text) Then
            X = CDbl(bat_txt_1_2.Text)
            X = value_up3(Math.Abs(X), 10, True) * Math.Sign(X)
            ch1(2) = CSng(X)
            bat_txt_1_2.Text = Format(ch1(2), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_1_2.Text = Format(ch1(2), "#0.000")
            bat_txt_1_2.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_1_3_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_1_3.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_1_3.Text) Then
            X = CDbl(bat_txt_1_3.Text)
            X = value_limiter3(X, 1, 4.9999)
            ch1(3) = CSng(X)
            bat_txt_1_3.Text = Format(ch1(3), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_1_3.Text = Format(ch1(3), "#0.000")
            bat_txt_1_3.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_1_4_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_1_4.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_1_4.Text) Then
            X = CDbl(bat_txt_1_4.Text)
            X = value_up3(Math.Abs(X), 0.01, True) * Math.Sign(X)
            ch1(4) = CSng(X)
            bat_txt_1_4.Text = Format(ch1(4), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_1_4.Text = Format(ch1(4), "#0.000")
            bat_txt_1_4.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_1_5_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_1_5.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_1_5.Text) Then
            X = CDbl(bat_txt_1_5.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            ch1(5) = CSng(X)
            bat_txt_1_5.Text = Format(ch1(5), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_1_5.Text = Format(ch1(5), "#0.000")
            bat_txt_1_5.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub bat_txt_2_0_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_2_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_2_0.Text) Then
            X = CDbl(bat_txt_2_0.Text)
            X = value_limiter3(X, ch1(0) + 0.0001, och(0) - 0.0001)
            ch2(0) = CSng(X)
            bat_txt_2_0.Text = Format(ch2(0), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_2_0.Text = Format(ch2(0), "#0.000")
            bat_txt_2_0.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_2_1_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_2_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_2_1.Text) Then
            X = CDbl(bat_txt_2_1.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            ch2(1) = CSng(X)
            bat_txt_2_1.Text = Format(ch2(1), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_2_1.Text = Format(ch2(1), "#0.000")
            bat_txt_2_1.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_2_2_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_2_2.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_2_2.Text) Then
            X = CDbl(bat_txt_2_2.Text)
            X = value_up3(Math.Abs(X), 10, True) * Math.Sign(X)
            ch2(2) = CSng(X)
            bat_txt_2_2.Text = Format(ch2(2), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_2_2.Text = Format(ch2(2), "#0.000")
            bat_txt_2_2.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_2_3_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_2_3.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_2_3.Text) Then
            X = CDbl(bat_txt_2_3.Text)
            X = value_limiter3(X, 1, 9.9999)
            ch2(3) = CSng(X)
            bat_txt_2_3.Text = Format(ch2(3), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_2_3.Text = Format(ch2(3), "#0.000")
            bat_txt_2_3.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_2_4_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_2_4.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_2_4.Text) Then
            X = CDbl(bat_txt_2_4.Text)
            X = value_up3(Math.Abs(X), 0.01, True) * Math.Sign(X)
            ch2(4) = CSng(X)
            bat_txt_2_4.Text = Format(ch2(4), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_2_4.Text = Format(ch2(4), "#0.000")
            bat_txt_2_4.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_2_5_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_2_5.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_2_5.Text) Then
            X = CDbl(bat_txt_2_5.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            ch2(5) = CSng(X)
            bat_txt_2_5.Text = Format(ch2(5), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_2_5.Text = Format(ch2(5), "#0.000")
            bat_txt_2_5.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub bat_txt_c_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_c.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_c.Text) Then
            X = CDbl(bat_txt_c.Text)
            X = value_down3(X, 0, False)
            cnom = CSng(X)
            bat_txt_c.Text = Format(cnom, "#0.000")
        Else
            MsgBox("Please enter a number !")
            bat_txt_c.Text = Format(cnom, "#0.000")
            bat_txt_c.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub bat_txt_cmax_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_cmax.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_cmax.Text) Then
            X = CDbl(bat_txt_cmax.Text)
            X = value_limiter3(X, 1, 1.9999)
            cmax_cnom = CSng(X)
            bat_txt_cmax.Text = Format(cmax_cnom, "#0.000")
        Else
            MsgBox("Please enter a number !")
            bat_txt_cmax.Text = Format(cmax_cnom, "#0.000")
            bat_txt_cmax.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_c10_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_c10.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_c10.Text) Then
            X = CDbl(bat_txt_c10.Text)
            X = value_limiter3(X, 0.0001, 0.9999)
            c10_cnom = CSng(X)
            bat_txt_c10.Text = Format(c10_cnom, "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_c10.Text = Format(c10_cnom, "#0.000")
            bat_txt_c10.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_cmin_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_cmin.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_cmin.Text) Then
            X = CDbl(bat_txt_cmin.Text)
            X = value_limiter3(X, 0, 99.9999)
            cmin100 = CSng(X)
            bat_txt_cmin.Text = Format(cmin100, "#0.000")
            Call trace_capacity_temp()
        Else
            MsgBox("Please enter a number !")
            bat_txt_cmin.Text = Format(cmin100, "#0.000")
            bat_txt_cmin.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub bat_txt_d0_0_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d0_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d0_0.Text) Then
            X = CDbl(bat_txt_d0_0.Text)
            X = value_down3(X, 0, True)
            de0(0) = CSng(X)
            bat_txt_d0_0.Text = Format(de0(0), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d0_0.Text = Format(de0(0), "#0.000")
            bat_txt_d0_0.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d0_1_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d0_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d0_1.Text) Then
            X = CDbl(bat_txt_d0_1.Text)
            X = value_down3(X, 0, True)
            de0(1) = CSng(X)
            bat_txt_d0_1.Text = Format(de0(1), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d0_1.Text = Format(de0(1), "#0.000")
            bat_txt_d0_1.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d0_2_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d0_2.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d0_2.Text) Then
            X = CDbl(bat_txt_d0_2.Text)
            X = value_down3(X, 0, True)
            de0(2) = CSng(X)
            bat_txt_d0_2.Text = Format(de0(2), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d0_2.Text = Format(de0(2), "#0.000")
            bat_txt_d0_2.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d0_3_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d0_3.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d0_3.Text) Then
            X = CDbl(bat_txt_d0_3.Text)
            X = value_down3(X, 0, True)
            de0(3) = CSng(X)
            bat_txt_d0_3.Text = Format(de0(3), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d0_3.Text = Format(de0(3), "#0.000")
            bat_txt_d0_3.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d0_4_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d0_4.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d0_4.Text) Then
            X = CDbl(bat_txt_d0_4.Text)
            X = value_up3(Math.Abs(X), 0.01, True) * Math.Sign(X)
            de0(4) = CSng(X)
            bat_txt_d0_4.Text = Format(de0(4), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d0_4.Text = Format(de0(4), "#0.000")
            bat_txt_d0_4.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d0_5_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d0_5.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d0_5.Text) Then
            X = CDbl(bat_txt_d0_5.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            de0(5) = CSng(X)
            bat_txt_d0_5.Text = Format(de0(5), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d0_5.Text = Format(de0(5), "#0.000")
            bat_txt_d0_5.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub bat_txt_d1_0_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d1_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d1_0.Text) Then
            X = CDbl(bat_txt_d1_0.Text)
            X = value_limiter3(X, de0(0), de2(0))
            de1(0) = CSng(X)
            bat_txt_d1_0.Text = Format(de1(0), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d1_0.Text = Format(de1(0), "#0.000")
            bat_txt_d1_0.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d1_1_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d1_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d1_1.Text) Then
            X = CDbl(bat_txt_d1_1.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            de1(1) = CSng(X)
            bat_txt_d1_1.Text = Format(de1(1), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d1_1.Text = Format(de1(1), "#0.000")
            bat_txt_d1_1.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d1_2_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d1_2.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d1_2.Text) Then
            X = CDbl(bat_txt_d1_2.Text)
            X = value_up3(Math.Abs(X), 10, True) * Math.Sign(X)
            de1(2) = CSng(X)
            bat_txt_d1_2.Text = Format(de1(2), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d1_2.Text = Format(de1(2), "#0.000")
            bat_txt_d1_2.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d1_3_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d1_3.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d1_3.Text) Then
            X = CDbl(bat_txt_d1_3.Text)
            X = value_limiter3(X, 1, 4.9999)
            de1(3) = CSng(X)
            bat_txt_d1_3.Text = Format(de1(3), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d1_3.Text = Format(de1(3), "#0.000")
            bat_txt_d1_3.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d1_4_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d1_4.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d1_4.Text) Then
            X = CDbl(bat_txt_d1_4.Text)
            X = value_up3(Math.Abs(X), 0.01, True) * Math.Sign(X)
            de1(4) = CSng(X)
            bat_txt_d1_4.Text = Format(de1(4), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d1_4.Text = Format(de1(4), "#0.000")
            bat_txt_d1_4.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d1_5_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d1_5.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d1_5.Text) Then
            X = CDbl(bat_txt_d1_5.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            de1(5) = CSng(X)
            bat_txt_d1_5.Text = Format(de1(5), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d1_5.Text = Format(de1(5), "#0.000")
            bat_txt_d1_5.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub bat_txt_d100_0_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d100_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d100_0.Text) Then
            X = CDbl(bat_txt_d100_0.Text)
            X = value_down3(X, 0, True)
            de100(0) = CSng(X)
            bat_txt_d100_0.Text = Format(de100(0), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d100_0.Text = Format(de100(0), "#0.000")
            bat_txt_d100_0.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d100_1_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d100_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d100_1.Text) Then
            X = CDbl(bat_txt_d100_1.Text)
            X = value_down3(X, 0, True)
            de100(1) = CSng(X)
            bat_txt_d100_1.Text = Format(de100(1), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d100_1.Text = Format(de100(1), "#0.000")
            bat_txt_d100_1.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d100_2_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d100_2.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d100_2.Text) Then
            X = CDbl(bat_txt_d100_2.Text)
            X = value_down3(X, 0, True)
            de100(2) = CSng(X)
            bat_txt_d100_2.Text = Format(de100(2), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d100_2.Text = Format(de100(2), "#0.000")
            bat_txt_d100_2.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d100_3_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d100_3.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d100_3.Text) Then
            X = CDbl(bat_txt_d100_3.Text)
            X = value_limiter3(X, 1, 4.9999)
            de100(3) = CSng(X)
            bat_txt_d100_3.Text = Format(de100(3), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d100_3.Text = Format(de100(3), "#0.000")
            bat_txt_d100_3.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d100_4_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d100_4.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d100_4.Text) Then
            X = CDbl(bat_txt_d100_4.Text)
            X = value_up3(Math.Abs(X), 0.01, True) * Math.Sign(X)
            de100(4) = CSng(X)
            bat_txt_d100_4.Text = Format(de100(4), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d100_4.Text = Format(de100(4), "#0.000")
            bat_txt_d100_4.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d100_5_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d100_5.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d100_5.Text) Then
            X = CDbl(bat_txt_d100_5.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            de100(5) = CSng(X)
            bat_txt_d100_5.Text = Format(de100(5), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d100_5.Text = Format(de100(5), "#0.000")
            bat_txt_d100_5.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub bat_txt_d2_0_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d2_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d2_0.Text) Then
            X = CDbl(bat_txt_d2_0.Text)
            X = value_limiter3(X, de1(0) + 0.0001, de100(0) - 0.0001)
            de2(0) = CSng(X)
            bat_txt_d2_0.Text = Format(de2(0), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d2_0.Text = Format(de2(0), "#0.000")
            bat_txt_d2_0.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d2_1_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d2_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d2_1.Text) Then
            X = CDbl(bat_txt_d2_1.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            de2(1) = CSng(X)
            bat_txt_d2_1.Text = Format(de2(1), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d2_1.Text = Format(de2(1), "#0.000")
            bat_txt_d2_1.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d2_2_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d2_2.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d2_2.Text) Then
            X = CDbl(bat_txt_d2_2.Text)
            X = value_up3(Math.Abs(X), 10, True) * Math.Sign(X)
            de2(2) = CSng(X)
            bat_txt_d2_2.Text = Format(de2(2), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d2_2.Text = Format(de2(2), "#0.000")
            bat_txt_d2_2.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d2_3_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d2_3.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d2_3.Text) Then
            X = CDbl(bat_txt_d2_3.Text)
            X = value_limiter3(X, 1, 4.9999)
            de2(3) = CSng(X)
            bat_txt_d2_3.Text = Format(de2(3), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d2_3.Text = Format(de2(3), "#0.000")
            bat_txt_d2_3.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d2_4_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d2_4.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d2_4.Text) Then
            X = CDbl(bat_txt_d2_4.Text)
            X = value_up3(Math.Abs(X), 0.01, True) * Math.Sign(X)
            de2(4) = CSng(X)
            bat_txt_d2_4.Text = Format(de2(4), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d2_4.Text = Format(de2(4), "#0.000")
            bat_txt_d2_4.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_d2_5_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_d2_5.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_d2_5.Text) Then
            X = CDbl(bat_txt_d2_5.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            de2(5) = CSng(X)
            bat_txt_d2_5.Text = Format(de2(5), "#0.000")
            trace_discharge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_d2_5.Text = Format(de2(5), "#0.000")
            bat_txt_d2_5.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub bat_txt_och_0_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_och_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_och_0.Text) Then
            X = CDbl(bat_txt_och_0.Text)
            X = value_limiter3(X, ch2(0) + 0.0001, 199.9999)
            och(0) = CSng(X)
            bat_txt_och_0.Text = Format(och(0), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_och_0.Text = Format(och(0), "#0.000")
            bat_txt_och_0.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_och_1_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_och_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_och_1.Text) Then
            X = CDbl(bat_txt_och_1.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            och(1) = CSng(X)
            bat_txt_och_1.Text = Format(och(1), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_och_1.Text = Format(och(1), "#0.000")
            bat_txt_och_1.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_och_2_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_och_2.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_och_2.Text) Then
            X = CDbl(bat_txt_och_2.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            och(2) = CSng(X)
            bat_txt_och_2.Text = Format(och(2), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_och_2.Text = Format(och(2), "#0.000")
            bat_txt_och_2.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_och_3_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_och_3.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_och_3.Text) Then
            X = CDbl(bat_txt_och_3.Text)
            X = value_limiter3(X, 1, 4.9999)
            och(3) = CSng(X)
            bat_txt_och_3.Text = Format(och(3), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_och_3.Text = Format(och(3), "#0.000")
            bat_txt_och_3.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_och_4_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_och_4.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_och_4.Text) Then
            X = CDbl(bat_txt_och_4.Text)
            X = value_up3(Math.Abs(X), 0.01, True) * Math.Sign(X)
            och(4) = CSng(X)
            bat_txt_och_4.Text = Format(och(4), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_och_4.Text = Format(och(4), "#0.000")
            bat_txt_och_4.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_och_5_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_och_5.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_och_5.Text) Then
            X = CDbl(bat_txt_och_5.Text)
            X = value_up3(Math.Abs(X), 1, True) * Math.Sign(X)
            och(5) = CSng(X)
            bat_txt_och_5.Text = Format(och(5), "#0.000")
            trace_charge_bat()
        Else
            MsgBox("Please enter a number !")
            bat_txt_och_5.Text = Format(och(5), "#0.000")
            bat_txt_och_5.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub bat_txt_t99_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_t99.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_t99.Text) Then
            X = CDbl(bat_txt_t99.Text)
            If X <= tbatmin Then
                pwr_lblmsg.Text = "WARNING: Max and min temperature must be different !"
                X = tbatmin + 0.0001
            End If
            tbat99 = CSng(X)
            bat_txt_t99.Text = Format(tbat99, "#0.000")
            Call trace_capacity_temp()
        Else
            MsgBox("Please enter a number !")
            bat_txt_t99.Text = Format(tbat99, "#0.000")
            bat_txt_t99.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_tc0_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_tc0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_tc0.Text) Then
            X = CDbl(bat_txt_tc0.Text)
            X = value_down3(X, tc50, True)
            tc0 = CSng(X)
            bat_txt_tc0.Text = Format(tc0, "#0.000")
            Call trace_autodecharge()
        Else
            MsgBox("Please enter a number !")
            bat_txt_tc0.Text = Format(tc0, "#0.000")
            bat_txt_tc0.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_tc50_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_tc50.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_tc50.Text) Then
            X = CDbl(bat_txt_tc50.Text)
            X = value_limiter3(X, 0.0001, tc0)
            tc50 = CSng(X)
            bat_txt_tc50.Text = Format(tc50, "#0.000")
            Call trace_autodecharge()
        Else
            MsgBox("Please enter a number !")
            bat_txt_tc50.Text = Format(tc50, "#0.000")
            bat_txt_tc50.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub bat_txt_tmin_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles bat_txt_tmin.Leave
        '
        Dim X As Double
        '
        If IsNumeric(bat_txt_tmin.Text) Then
            X = CDbl(bat_txt_tmin.Text)
            If X >= tbat99 Then
                pwr_lblmsg.Text = "WARNING: Max and min temperature must be different !"
                X = tbat99 - 0.0001
            End If
            tbatmin = CSng(X)
            bat_txt_tmin.Text = Format(tbatmin, "#0.000")
            Call trace_capacity_temp()
        Else
            MsgBox("Please enter a number !")
            bat_txt_tmin.Text = Format(tbatmin, "#0.000")
            bat_txt_tmin.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

#End Region

#End Region

#Region "Solar cells data TAB"

    Private Sub graph_cosaprox()
        '
        ' Repaints graph showing influence of sun angle on cells output power.
        '
        Dim rad As Single
        Dim i As Integer
        Dim tabXM1 As ArrayList
        Dim tabYM1 As ArrayList
        Dim p(2) As Double
        Dim tm As Double
        Dim td As Double
        '
        tabXM1 = New ArrayList
        tabYM1 = New ArrayList
        '
        rad = Math.PI / 180
        tm = cell_tetam * rad
        td = cell_tetad * rad
        '
        ' Features of Parabolical section
        p(0) = (System.Math.Sin(td) * (tm - td) - System.Math.Cos(td)) / (tm - td) ^ 2
        p(1) = -System.Math.Sin(td) - 2 * p(0) * td
        p(2) = -p(0) * tm * tm - p(1) * tm
        '
        i = 0
        Do While i <= cell_tetad
            tabXM1.Add(i)
            tabYM1.Add(System.Math.Cos(i * rad))
            i = i + 1
        Loop
        Do While i <= cell_tetam
            tabXM1.Add(i)
            tabYM1.Add((p(0) * i * rad * i * rad + p(1) * i * rad + p(2)))
            i = i + 1
        Loop
        Do While i <= 90
            tabXM1.Add(i)
            tabYM1.Add(0)
            i = i + 1
        Loop

        With cel_cosaprox_graph
            ' Définition des axes X et Y
            .ChartAreas("ChartArea1").AxisY.Title = "Cosine law"
            .ChartAreas("ChartArea1").AxisX.Title = "Sun incidence [°]"
            .ChartAreas("ChartArea1").AxisX.Minimum = 0
            .ChartAreas("ChartArea1").AxisX.Maximum = 90
            .ChartAreas("ChartArea1").AxisY.Minimum = 0
            .ChartAreas("ChartArea1").AxisY.Maximum = 1
            .ChartAreas("ChartArea1").AxisX.Interval = 10
            .ChartAreas("ChartArea1").AxisY.Interval = 0.1
            '
            .Series.Clear()
            .Series.Add("s")
            .Series("s").Points.DataBindXY(tabXM1, tabYM1)
            .Series("s").ChartType = DataVisualization.Charting.SeriesChartType.Spline

            .Series("s").ShadowOffset = 1
            .Series("s").BorderColor = Color.AliceBlue
            .Series("s").BorderWidth = 2
            .Series("s").Color = Color.Navy
            '
        End With

    End Sub

    Private Sub graph_cellule()
        '
        ' Repaints graph showing characteristics of suncells.
        '
        Dim i As Short
        Dim intensite As Single
        Dim tension As Single
        Dim Voc, Vpm As Single
        Dim Isc, Ipm As Single
        Dim i0, Vt As Single
        '
        ' Dessins des graphes
        Dim tabXM1 As ArrayList
        Dim tabYM1 As ArrayList
        Dim tabXM2 As ArrayList
        Dim tabYM2 As ArrayList
        Dim tabXM3 As ArrayList
        Dim tabYM3 As ArrayList
        '
        '    Drawing graph
        '    -------------
        tabXM1 = New ArrayList
        tabYM1 = New ArrayList
        tabXM2 = New ArrayList
        tabYM2 = New ArrayList
        tabXM3 = New ArrayList
        tabYM3 = New ArrayList

        With cel_charac_graph
            ' Définition des axes X et Y
            .ChartAreas("ChartArea1").AxisX.Title = "Voltage [V]"
            If cel_opt_cour.Checked Then
                .ChartAreas("ChartArea1").AxisY.Title = "Current [A]"
            Else
                .ChartAreas("ChartArea1").AxisY.Title = "Power [W]"
            End If
            .ChartAreas("ChartArea1").AxisX.IntervalAutoMode = DataVisualization.Charting.IntervalAutoMode.VariableCount
            .ChartAreas("ChartArea1").AxisY.IntervalAutoMode = DataVisualization.Charting.IntervalAutoMode.VariableCount
            '
            ' Tracé courbe Tréf
            ' -----------------
            Voc = cell_voc
            Vpm = cell_vmp
            Ipm = cell_imp
            Isc = cell_isc
            If cel_opt_beol_1.Checked Then
                ' cas EOL
                Voc = cell_radvoc * Voc
                Vpm = cell_radvmp * Vpm
                Ipm = cell_radimp * Ipm
                Isc = cell_radisc * Isc
            End If

            For i = 1 To 101

                tension = Voc * (i - 1) / 100 * cell_ns
                Vt = (Voc - Vpm) / Math.Log(Isc / (Isc - Ipm))
                i0 = Isc / (Math.Exp(Voc / Vt) - 1)
                intensite = (Isc - i0 * (Math.Exp(tension / Vt / cell_ns) - 1)) ' Current is plotted for one string (since all panel don't have necessaraly the same number of strings)
                If intensite < 0 Then intensite = 0
                tabXM1.Add(tension)
                If cel_opt_cour.Checked Then
                    tabYM1.Add(intensite)
                Else
                    tabYM1.Add(intensite * tension)
                End If
            Next i
            '
            ' Tracé courbe 50°C
            ' -----------------
            Voc = cell_voc + cell_dvoc * (50 - cell_tref)
            Vpm = cell_vmp + cell_dvmp * (50 - cell_tref)
            Ipm = cell_imp + cell_dimp * (50 - cell_tref)
            Isc = cell_isc + cell_disc * (50 - cell_tref)
            If cel_opt_beol_1.Checked = True Then
                ' cas EOL
                Voc = cell_radvoc * Voc
                Vpm = cell_radvmp * Vpm
                Ipm = cell_radimp * Ipm
                Isc = cell_radisc * Isc
            End If
            For i = 1 To 101
                tension = Voc * (i - 1) / 100 * cell_ns
                Vt = (Voc - Vpm) / Math.Log(Isc / (Isc - Ipm))
                i0 = Isc / (Math.Exp(Voc / Vt) - 1)
                intensite = (Isc - i0 * (Math.Exp(tension / Vt / cell_ns) - 1))
                If intensite < 0 Then intensite = 0
                tabXM2.Add(tension)
                If cel_opt_cour.Checked Then
                    tabYM2.Add(intensite)
                Else
                    tabYM2.Add(intensite * tension)
                End If
            Next i
            '
            ' Tracé courbe 75°C
            ' -----------------
            Voc = cell_voc + cell_dvoc * (75 - cell_tref)
            Vpm = cell_vmp + cell_dvmp * (75 - cell_tref)
            Ipm = cell_imp + cell_dimp * (75 - cell_tref)
            Isc = cell_isc + cell_disc * (75 - cell_tref)
            If cel_opt_beol_1.Checked = True Then
                ' cas EOL
                Voc = cell_radvoc * Voc
                Vpm = cell_radvmp * Vpm
                Ipm = cell_radimp * Ipm
                Isc = cell_radisc * Isc
            End If
            For i = 1 To 101
                tension = Voc * (i - 1) / 100 * cell_ns
                Vt = (Voc - Vpm) / Math.Log(Isc / (Isc - Ipm))
                i0 = Isc / (Math.Exp(Voc / Vt) - 1)
                intensite = (Isc - i0 * (Math.Exp(tension / Vt / cell_ns) - 1))
                If intensite < 0 Then intensite = 0
                tabXM3.Add(tension)
                If cel_opt_cour.Checked Then
                    tabYM3.Add(intensite)
                Else
                    tabYM3.Add(intensite * tension)
                End If
            Next i
            '
            .Series.Clear()
            .Series.Add("Tref")
            .Series("Tref").Points.DataBindXY(tabXM1, tabYM1)
            .Series("Tref").ChartType = DataVisualization.Charting.SeriesChartType.Spline
            .Series("Tref").ShadowOffset = 1
            .Series("Tref").BorderWidth = 2
            .Series("Tref").Color = Color.Yellow
            '
            .Series.Add("50°C")
            .Series("50°C").Points.DataBindXY(tabXM2, tabYM2)
            .Series("50°C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
            .Series("50°C").ShadowOffset = 1
            .Series("50°C").BorderWidth = 2
            .Series("50°C").Color = Color.Orange
            '
            .Series.Add("75°C")
            .Series("75°C").Points.DataBindXY(tabXM3, tabYM3)
            .Series("75°C").ChartType = DataVisualization.Charting.SeriesChartType.Spline
            .Series("75°C").ShadowOffset = 1
            .Series("75°C").BorderWidth = 2
            .Series("75°C").Color = Color.Red
        End With
        ''
    End Sub

#Region "Reading values"

    Private Sub cel_cmb_type_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_cmb_type.SelectedIndexChanged
        '
        cell_type = cel_cmb_type.SelectedIndex
        '
        ' 0 : AsGa
        ' 1 : Silicium
        ' 2 : Other
        '
        Select Case cell_type
            Case 0
                cell_isc = 0.53
                cell_imp = 0.5
                cell_voc = 1
                cell_vmp = 0.86
                cell_disc = 0.0003
                cell_dimp = 0.00025
                cell_dvoc = -0.002
                cell_dvmp = -0.002
                cell_surf = 16
                cell_eff = 0.18
            Case 1
                cell_isc = 1
                cell_imp = 0.95
                cell_voc = 0.54
                cell_vmp = 0.45
                cell_disc = 0.0012
                cell_dimp = 0.0009
                cell_dvoc = -0.002
                cell_dvmp = -0.002
                cell_surf = 20
                cell_eff = 0.16
            Case 2
        End Select
        '
        ' Mise à jour
        ' -----------
        update_power_conf()
        'blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_opt_cour_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_opt_cour.CheckedChanged
        If eventSender.Checked Then
            '
            Call graph_cellule()
            '
        End If
    End Sub

    Private Sub cel_opt_puiss_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_opt_puiss.CheckedChanged
        If eventSender.Checked Then
            '
            Call graph_cellule()
            '
        End If
    End Sub

    Private Sub cel_opt_beol_0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cel_opt_beol_0.Click
        '
        Select Case cel_opt_beol_0.Checked
            Case True ' BOL
                cel_txt_radisc.Enabled = False
                cel_txt_radimp.Enabled = False
                cel_txt_radvoc.Enabled = False
                cel_txt_radvmp.Enabled = False
                cel_txt_radisc.Text = "1"
                cel_txt_radimp.Text = "1"
                cel_txt_radvoc.Text = "1"
                cel_txt_radvmp.Text = "1"
                cel_txt_eff.Text = Format(cell_eff * 100, "0.0")
            Case False ' EOL
                cel_txt_radisc.Enabled = True
                cel_txt_radimp.Enabled = True
                cel_txt_radvoc.Enabled = True
                cel_txt_radvmp.Enabled = True
                cel_txt_radisc.Text = CStr(cell_radisc)
                cel_txt_radimp.Text = CStr(cell_radimp)
                cel_txt_radvoc.Text = CStr(cell_radvoc)
                cel_txt_radvmp.Text = CStr(cell_radvmp)
                cel_txt_eff.Text = Format(cell_eff * 100, "0.0")
        End Select
        '
        Call graph_cellule()

    End Sub

    Private Sub cel_opt_beol_1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cel_opt_beol_1.Click
        '
        Select Case cel_opt_beol_0.Checked
            Case True ' BOL
                cel_txt_radisc.Enabled = False
                cel_txt_radimp.Enabled = False
                cel_txt_radvoc.Enabled = False
                cel_txt_radvmp.Enabled = False
                cel_txt_radisc.Text = "1"
                cel_txt_radimp.Text = "1"
                cel_txt_radvoc.Text = "1"
                cel_txt_radvmp.Text = "1"
                cel_txt_eff.Text = Format(cell_eff * 100, "0.0")
            Case False ' EOL
                cel_txt_radisc.Enabled = True
                cel_txt_radimp.Enabled = True
                cel_txt_radvoc.Enabled = True
                cel_txt_radvmp.Enabled = True
                cel_txt_radisc.Text = CStr(cell_radisc)
                cel_txt_radimp.Text = CStr(cell_radimp)
                cel_txt_radvoc.Text = CStr(cell_radvoc)
                cel_txt_radvmp.Text = CStr(cell_radvmp)
                cel_txt_eff.Text = Format(cell_eff * 100, "0.0")
        End Select
        '
        Call graph_cellule()

    End Sub

    Private Sub cel_txt_dimp_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_dimp.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_dimp.Text) Then
            X = CDbl(cel_txt_dimp.Text)
            X = value_limiter3(X, -0.05, 0.0499)
            cell_dimp = CSng(X)
            cel_txt_dimp.Text = Format(cell_dimp, "#0.000")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_dimp.Text = Format(cell_dimp, "#0.000")
            cel_txt_dimp.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_disc_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_disc.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_disc.Text) Then
            X = CDbl(cel_txt_disc.Text)
            X = value_limiter3(X, -0.05, 0.0499)
            cell_disc = CSng(X)
            cel_txt_disc.Text = Format(cell_disc, "#0.000")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_disc.Text = Format(cell_disc, "#0.000")
            cel_txt_disc.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_dvmp_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_dvmp.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_dvmp.Text) Then
            X = CDbl(cel_txt_dvmp.Text)
            X = value_limiter3(X, -0.05, 0.0499)
            cell_dvmp = CSng(X)
            cel_txt_dvmp.Text = Format(cell_dvmp, "#0.000")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_dvmp.Text = Format(cell_dvmp, "#0.000")
            cel_txt_dvmp.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_dvoc_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_dvoc.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_dvoc.Text) Then
            X = CDbl(cel_txt_dvoc.Text)
            X = value_limiter3(X, -0.05, 0.0499)
            cell_dvoc = CSng(X)
            cel_txt_dvoc.Text = Format(cell_dvoc, "#0.000")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_dvoc.Text = Format(cell_dvoc, "#0.000")
            cel_txt_dvoc.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_eff_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cel_txt_eff.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_eff.Text) Then
            X = CDbl(cel_txt_eff.Text)
            X = value_limiter3(X, 0.01, 100)
            cell_eff = CSng(X) / 100
            cel_txt_eff.Text = Format(cell_eff * 100, "#0.0")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_eff.Text = Format(cell_eff * 100, "#0.0")
            cel_txt_eff.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_imp_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_imp.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_imp.Text) Then
            X = CDbl(cel_txt_imp.Text)
            X = value_limiter3(X, 0, 9.999)
            cell_imp = CSng(X)
            If cell_imp >= cell_isc Then cell_imp = cell_isc - 0.02
            cel_txt_imp.Text = Format(cell_imp, "#0.00")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_imp.Text = Format(cell_imp, "#0.00")
            cel_txt_imp.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_isc_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_isc.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_isc.Text) Then
            X = CDbl(cel_txt_isc.Text)
            X = value_limiter3(X, 0, 9.999)
            cell_isc = CSng(X)
            If cell_isc <= cell_imp Then cell_isc = cell_imp + 0.02
            cel_txt_isc.Text = Format(cell_isc, "#0.00")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_isc.Text = Format(cell_isc, "#0.00")
            cel_txt_isc.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_ns_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_ns.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_ns.Text) And cel_txt_ns.Text = Int(cel_txt_ns.Text) Then
            X = CDbl(cel_txt_ns.Text)
            X = value_limiter3(X, 0, 99)
            cell_ns = CSng(X)
            cel_txt_ns.Text = Format(cell_ns, "#0")
            Call graph_cellule()
        Else
            MsgBox("Please enter an integer !")
            cel_txt_ns.Text = Format(cell_ns, "#0")
            cel_txt_ns.Focus()
        End If




        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_radimp_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_radimp.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_radimp.Text) Then
            X = CDbl(cel_txt_radimp.Text)
            X = value_limiter3(X, 0, 1.0199)
            cell_radimp = CSng(X)
            If cell_radisc * cell_isc <= cell_radimp * cell_imp Then cell_radisc = 1.02 * cell_radimp * cell_imp / cell_isc
            cel_txt_radimp.Text = Format(cell_radimp, "#0.000")
            cel_txt_radisc.Text = Format(cell_radisc, "#0.000")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_radimp.Text = Format(cell_radimp, "#0.000")
            cel_txt_radimp.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_radisc_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_radisc.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_radisc.Text) Then
            X = CDbl(cel_txt_radisc.Text)
            X = value_limiter3(X, 0, 1.0199)
            cell_radisc = CSng(X)
            If cell_radisc * cell_isc <= cell_radimp * cell_imp Then cell_radimp = 0.98 * cell_radisc * cell_isc / cell_imp
            cel_txt_radisc.Text = Format(cell_radisc, "#0.000")
            cel_txt_radimp.Text = Format(cell_radimp, "#0.000")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_radisc.Text = Format(cell_radisc, "#0.000")
            cel_txt_radisc.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_radvmp_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_radvmp.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_radvmp.Text) Then
            X = CDbl(cel_txt_radvmp.Text)
            X = value_limiter3(X, 0, 1.0199)
            cell_radvmp = CSng(X)
            If cell_radvoc * cell_voc <= cell_radvmp * cell_vmp Then cell_radvoc = 1.02 * cell_radvmp * cell_vmp / cell_voc
            cel_txt_radvmp.Text = Format(cell_radvmp, "#0.000")
            cel_txt_radvoc.Text = Format(cell_radvoc, "#0.000")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_radvmp.Text = Format(cell_radvmp, "#0.000")
            cel_txt_radvmp.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_radvoc_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_radvoc.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_radvoc.Text) Then
            X = CDbl(cel_txt_radvoc.Text)
            X = value_limiter3(X, 0, 1.0199)
            cell_radvoc = CSng(X)
            If cell_radvoc * cell_voc <= cell_radvmp * cell_vmp Then cell_radvmp = 0.98 * cell_radvoc * cell_voc / cell_vmp
            cel_txt_radvoc.Text = Format(cell_radvoc, "#0.000")
            cel_txt_radvmp.Text = Format(cell_radvmp, "#0.000")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_radvoc.Text = Format(cell_radvoc, "#0.000")
            cel_txt_radvoc.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_vmp_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_vmp.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_vmp.Text) Then
            X = CDbl(cel_txt_vmp.Text)
            X = value_limiter3(X, 0, 9.999)
            cell_vmp = CSng(X)
            If cell_vmp >= cell_voc Then cell_vmp = cell_voc - 0.02
            cel_txt_vmp.Text = Format(cell_vmp, "#0.00")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_vmp.Text = Format(cell_vmp, "#0.00")
            cel_txt_vmp.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_voc_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_voc.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_voc.Text) Then
            X = CDbl(cel_txt_voc.Text)
            X = value_limiter3(X, 0, 9.999)
            cell_voc = CSng(X)
            If cell_voc <= cell_vmp Then cell_voc = cell_vmp + 0.02
            cel_txt_voc.Text = Format(cell_voc, "#0.00")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_voc.Text = Format(cell_voc, "#0.00")
            cel_txt_voc.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_surf_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_surf.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_surf.Text) Then
            X = CDbl(cel_txt_surf.Text)
            X = value_limiter3(X, 0.001, 100)
            cell_surf = CSng(X)
            'cell_eff = cell_imp * cell_vmp * 10000 / 1368 / cell_surf ' Update efficiency according to cell surface
            cel_txt_surf.Text = Format(cell_surf, "#0.00")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_surf.Text = Format(cell_surf, "#0.00")
            cel_txt_surf.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_tetad_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_tetad.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_tetad.Text) Then
            X = CDbl(cel_txt_tetad.Text)
            X = value_limiter3(X, 0, 90)
            X = value_limiter3(X, 0, cell_tetam - 0.01)
            cell_tetad = CSng(X)
            cel_txt_tetad.Text = Format(cell_tetad, "#0.0")
            Call graph_cosaprox()
        Else
            MsgBox("Please enter a number !")
            cel_txt_tetad.Text = Format(cell_tetad, "#0.0")
            cel_txt_tetad.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_tetam_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_tetam.Leave
        '
        Dim X As Double
        '
        If IsNumeric(cel_txt_tetam.Text) Then
            X = CDbl(cel_txt_tetam.Text)
            X = value_limiter3(X, 0, 90)
            X = value_limiter3(X, cell_tetad + 0.01, 90)
            cell_tetam = CSng(X)
            cel_txt_tetam.Text = Format(cell_tetam, "#0.0")
            Call graph_cosaprox()
        Else
            MsgBox("Please enter a number !")
            cel_txt_tetam.Text = Format(cell_tetam, "#0.0")
            cel_txt_tetam.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub cel_txt_tref_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cel_txt_tref.Leave
        '
        If IsNumeric(cel_txt_tref.Text) Then
            cell_tref = CSng(cel_txt_tref.Text)
            cel_txt_tref.Text = Format(cell_tref, "#0.0")
            Call graph_cellule()
        Else
            MsgBox("Please enter a number !")
            cel_txt_tref.Text = Format(cell_tref, "#0.0")
            cel_txt_tref.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

#End Region

#End Region

#Region "Management TAB"

    Private Sub gst_opt_regul_0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles gst_opt_regul_0.Click
        '
        If gst_opt_regul_0.Checked = True Then
            gst_type = 1  ' Regulated case
            gst_txt_Vregul.Enabled = True
        Else
            gst_type = 0  ' Not regulated case
            gst_txt_Vregul.Enabled = False
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub gst_opt_regul_1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles gst_opt_regul_1.Click
        '
        If gst_opt_regul_1.Checked = True Then
            gst_type = 0 ' Not regulated case
            gst_txt_Vregul.Enabled = False
        Else
            gst_type = 1
            gst_txt_Vregul.Enabled = True
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

#Region "Reading values"

    Private Sub gst_txt_Vregul_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles gst_txt_Vregul.Leave
        '
        Dim X As Double
        '
        If IsNumeric(gst_txt_Vregul.Text) Then
            X = CDbl(gst_txt_Vregul.Text)
            X = value_limiter3(X, 0.1, 100)
            Vregul = CSng(X)
            gst_txt_Vregul.Text = Format(Vregul, "#0.0")
        Else
            MsgBox("Please enter a number !")
            gst_txt_Vregul.Text = Format(Vregul, "#0.0")
            gst_txt_Vregul.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub gst_txt_chini_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles gst_txt_chini.Leave
        '
        Dim X As Double
        '
        If IsNumeric(gst_txt_chini.Text) Then
            X = CDbl(gst_txt_chini.Text)
            X = value_limiter3(X, 0, 100)
            chini = CSng(X)
            gst_txt_chini.Text = Format(chini, "#0.0")
        Else
            MsgBox("Please enter a number !")
            gst_txt_chini.Text = Format(chini, "#0.0")
            gst_txt_chini.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub gst_txt_diss_0_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles gst_txt_diss_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(gst_txt_diss_0.Text) Then
            X = CDbl(gst_txt_diss_0.Text)
            X = value_limiter3(X, 0, 100)
            diss(0) = CSng(X)
            gst_txt_diss_0.Text = Format(diss(0), "#0.0")
        Else
            MsgBox("Please enter a number !")
            gst_txt_diss_0.Text = Format(diss(0), "#0.0")
            gst_txt_diss_0.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub gst_txt_diss_1_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles gst_txt_diss_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(gst_txt_diss_1.Text) Then
            X = CDbl(gst_txt_diss_1.Text)
            X = value_limiter3(X, 0, 100)
            diss(1) = CSng(X)
            gst_txt_diss_1.Text = Format(diss(1), "#0.0")
        Else
            MsgBox("Please enter a number !")
            gst_txt_diss_1.Text = Format(diss(1), "#0.0")
            gst_txt_diss_1.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub gst_txt_cour_ent_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles gst_txt_cour_ent.Leave
        '
        Dim X As Double
        '
        If IsNumeric(gst_txt_cour_ent.Text) Then
            X = CDbl(gst_txt_cour_ent.Text)
            X = value_limiter3(X, 0, 100)
            bat_courant_entretien = CSng(X)
            gst_txt_cour_ent.Text = Format(bat_courant_entretien, "#0.0")
        Else
            MsgBox("Please enter a number !")
            gst_txt_cour_ent.Text = Format(bat_courant_entretien, "#0.0")
            gst_txt_cour_ent.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub gst_txt_ichmax_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles gst_txt_ichmax.Leave
        '
        Dim X As Double
        '
        If IsNumeric(gst_txt_ichmax.Text) Then
            X = CDbl(gst_txt_ichmax.Text)
            X = value_down3(X, 0, False)
            ichmax = CSng(X)
            gst_txt_ichmax.Text = Format(ichmax, "#0.0")
        Else
            MsgBox("Please enter a number !")
            gst_txt_ichmax.Text = Format(ichmax, "#0.0")
            gst_txt_ichmax.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub gst_txt_kcharge_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles gst_txt_kcharge.Leave
        '
        Dim X As Double
        '
        If IsNumeric(gst_txt_kcharge.Text) Then
            X = CDbl(gst_txt_kcharge.Text)
            X = value_down3(X, 1, True)
            kcharge = CSng(X)
            gst_txt_kcharge.Text = Format(kcharge, "#0.0")
        Else
            MsgBox("Please enter a number !")
            gst_txt_kcharge.Text = Format(kcharge, "#0.0")
            gst_txt_kcharge.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub gst_txt_pdem_0_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles gst_txt_pdem_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(gst_txt_pdem_0.Text) Then
            X = CDbl(gst_txt_pdem_0.Text)
            X = value_down3(X, 0, True)
            pdem(0) = CSng(X)
            gst_txt_pdem_0.Text = Format(pdem(0), "#0.0")
        Else
            MsgBox("Please enter a number !")
            gst_txt_pdem_0.Text = Format(pdem(0), "#0.0")
            gst_txt_pdem_0.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub gst_txt_pdem_1_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles gst_txt_pdem_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(gst_txt_pdem_1.Text) Then
            X = CDbl(gst_txt_pdem_1.Text)
            X = value_down3(X, 0, True)
            pdem(1) = CSng(X)
            gst_txt_pdem_1.Text = Format(pdem(1), "#0.0")
        Else
            MsgBox("Please enter a number !")
            gst_txt_pdem_1.Text = Format(pdem(1), "#0.0")
            gst_txt_pdem_1.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub gst_txt_rendbat_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles gst_txt_rendbat.Leave
        '
        Dim X As Double
        '
        If IsNumeric(gst_txt_rendbat.Text) Then
            X = CDbl(gst_txt_rendbat.Text)
            X = value_limiter3(X, 0, 100)
            rendbat = CSng(X)
            gst_txt_rendbat.Text = Format(rendbat, "#0.0")
        Else
            MsgBox("Please enter a number !")
            gst_txt_rendbat.Text = Format(rendbat, "#0.0")
            gst_txt_rendbat.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub gst_txt_rendgs_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles gst_txt_rendgs.Leave
        '
        Dim X As Double
        '
        If IsNumeric(gst_txt_rendgs.Text) Then
            X = CDbl(gst_txt_rendgs.Text)
            X = value_limiter3(X, 0, 100)
            rendgs = CSng(X)
            gst_txt_rendgs.Text = Format(rendgs, "#0.0")
        Else
            MsgBox("Please enter a number !")
            gst_txt_rendgs.Text = Format(rendgs, "#0.0")
            gst_txt_rendgs.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub gst_txt_chvmax_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles gst_txt_chvmax.Leave
        '
        Dim X As Double
        '
        If IsNumeric(gst_txt_chvmax.Text) Then
            X = CDbl(gst_txt_chvmax.Text)
            X = value_down3(X, 1, True)
            chvmax = CSng(X)
            gst_txt_chvmax.Text = Format(chvmax, "#0.0")
        Else
            MsgBox("Please enter a number !")
            gst_txt_chvmax.Text = Format(chvmax, "#0.0")
            gst_txt_chvmax.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

#End Region

#End Region

#Region "Thermical Data"

#Region "Reading values"

    Private Sub thr_txtTBat_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles thr_txtTBat.Leave
        Dim X As Single
        Dim control As TextBox
        '
        control = thr_txtTBat
        If IsNumeric(control.Text) Then
            X = CSng(control.Text)
            X = value_limiter3(CDbl(X), -50, 50)
            control.Text = Format(X, "#0.00")
            T_battery = X
        Else
            MsgBox("Please enter a number !")
            control.Text = T_battery
            control.Focus()
        End If
        blnIsNewPrj = True
    End Sub

    Private Sub thr_txtInitTemp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles thr_txtInitTemp.Leave
        Dim X As Single
        Dim control As TextBox
        '
        control = thr_txtInitTemp
        If IsNumeric(control.Text) Then
            X = CSng(control.Text)
            X = value_limiter3(CDbl(X), -50, 50)
            control.Text = Format(X, "#0.00")
            T_init_panels = X
        Else
            MsgBox("Please enter a number !")
            control.Text = T_init_panels
            control.Focus()
        End If
        blnIsNewPrj = True
    End Sub

    Private Sub thr_txtInitBodyTemp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles thr_txtInitBodyTemp.Leave
        Dim X As Single
        Dim control As TextBox
        '
        control = thr_txtInitBodyTemp
        If IsNumeric(control.Text) Then
            X = CSng(control.Text)
            X = value_limiter3(CDbl(X), -50, 50)
            control.Text = Format(X, "#0.00")
            T_init_body = X
        Else
            MsgBox("Please enter a number !")
            control.Text = T_init_body
            control.Focus()
        End If
        blnIsNewPrj = True
    End Sub

    Private Sub thr_txtCond_1_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _
        thr_txtCond_1.Leave, thr_txtCond_2.Leave, thr_txtCond_3.Leave, thr_txtCond_4.Leave, thr_txtCond_5.Leave, thr_txtCond_6.Leave, thr_txtCond_7.Leave, thr_txtCond_8.Leave
        '
        Dim X As Single
        Dim index As Integer
        Dim control As TextBox
        '
        control = CType(sender, Control)
        index = CInt(Microsoft.VisualBasic.Right(control.Name, 1))
        '
        If IsNumeric(control.Text) Then
            X = CSng(control.Text)
            X = value_limiter3(CDbl(X), 0, 1000)
            control.Text = Format(X, "#0.00")
            SolarPanels.Item(index).PanelConduction = X
        Else
            MsgBox("Please enter a number !")
            control.Text = SolarPanels.Item(index).PanelConduction
            control.Focus()
        End If
        blnIsNewPrj = True
    End Sub

    Private Sub thr_txtBodyAlf_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
    thr_txtBodyAlf_px.Leave, thr_txtBodyAlf_mx.Leave, thr_txtBodyAlf_py.Leave, thr_txtBodyAlf_my.Leave, thr_txtBodyAlf_pz.Leave, thr_txtBodyAlf_mz.Leave
        '
        Dim X As Single
        Dim control As TextBox
        '
        control = CType(sender, Control)
        '
        If IsNumeric(control.Text) Then
            X = CSng(control.Text)
            X = value_limiter3(CDbl(X), 0, 1)
            control.Text = Format(X, "#0.00")
        Else
            MsgBox("Please enter a number !")
            control.Text = "0.00"
            control.Focus()
        End If
        Select Case Microsoft.VisualBasic.Right(control.Name, 2)
            Case "px" : BodyAlf(0) = X
            Case "mx" : BodyAlf(1) = X
            Case "py" : BodyAlf(2) = X
            Case "my" : BodyAlf(3) = X
            Case "pz" : BodyAlf(4) = X
            Case "mz" : BodyAlf(5) = X
        End Select
        blnIsNewPrj = True
    End Sub

    Private Sub thr_txtBodyEps_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
        thr_txtBodyEps_px.Leave, thr_txtBodyEps_mx.Leave, thr_txtBodyEps_py.Leave, thr_txtBodyEps_my.Leave, thr_txtBodyEps_pz.Leave, thr_txtBodyEps_mz.Leave
        '
        Dim X As Single
        Dim control As TextBox
        '
        control = CType(sender, Control)
        '
        If IsNumeric(control.Text) Then
            X = CSng(control.Text)
            X = value_limiter3(CDbl(X), 0, 1)
            control.Text = Format(X, "#0.00")
        Else
            MsgBox("Please enter a number !")
            control.Text = "0.00"
            control.Focus()
        End If
        Select Case Microsoft.VisualBasic.Right(control.Name, 2)
            Case "px" : BodyEps(0) = X
            Case "mx" : BodyEps(1) = X
            Case "py" : BodyEps(2) = X
            Case "my" : BodyEps(3) = X
            Case "pz" : BodyEps(4) = X
            Case "mz" : BodyEps(5) = X
        End Select
        blnIsNewPrj = True
    End Sub

    Private Sub thr_txtBodyCspec_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles thr_txtBodyCspec.Leave
        Dim X As Single
        Dim control As TextBox
        '
        control = thr_txtBodyCspec
        If IsNumeric(control.Text) Then
            X = CSng(control.Text)
            X = value_limiter3(CDbl(X), 1, 500000) 'limit to zero ???
            control.Text = Format(X, "#0.00")
            BodySpecHeat = X
        Else
            MsgBox("Please enter a valid number between 1 and 50000 !")
            control.Text = BodySpecHeat
            control.Focus()
        End If
        blnIsNewPrj = True
    End Sub

    Private Sub thr_txtCspec_1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
        thr_txtCspec_1.Leave, thr_txtCspec_2.Leave, thr_txtCspec_3.Leave, thr_txtCspec_4.Leave, thr_txtCspec_5.Leave, thr_txtCspec_6.Leave, thr_txtCspec_7.Leave, thr_txtCspec_8.Leave
        '
        Dim X As Single
        Dim index As Integer
        Dim control As TextBox
        '
        control = CType(sender, Control)
        index = CInt(Microsoft.VisualBasic.Right(control.Name, 1))
        '
        If IsNumeric(control.Text) Then
            X = CSng(control.Text)
            X = value_limiter3(CDbl(X), 0, 500000)
            control.Text = Format(X, "#0.00")
            SolarPanels.Item(index).PanelSpecHeat = X
        Else
            MsgBox("Please enter a number !")
            control.Text = SolarPanels.Item(index).PanelSpecHeat
            control.Focus()
        End If
        blnIsNewPrj = True
    End Sub

    Private Sub thr_txtBackAlf_1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
    thr_txtBackAlf_1.Leave, thr_txtBackAlf_2.Leave, thr_txtBackAlf_3.Leave, thr_txtBackAlf_4.Leave, thr_txtBackAlf_5.Leave, thr_txtBackAlf_6.Leave, thr_txtBackAlf_7.Leave, thr_txtBackAlf_8.Leave
        '
        Dim X As Single
        Dim index As Integer
        Dim control As TextBox
        '
        control = CType(sender, Control)
        index = CInt(Microsoft.VisualBasic.Right(control.Name, 1))
        '
        If IsNumeric(control.Text) Then
            X = CSng(control.Text)
            X = value_limiter3(CDbl(X), 0, 1)
            control.Text = Format(X, "#0.00")
            SolarPanels.Item(index).PanelBackAlf = X
        Else
            MsgBox("Please enter a number !")
            control.Text = SolarPanels.Item(index).PanelBackAlf
            control.Focus()
        End If
        blnIsNewPrj = True
    End Sub

    Private Sub thr_txtBackEps_1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
    thr_txtBackEps_1.Leave, thr_txtBackEps_2.Leave, thr_txtBackEps_3.Leave, thr_txtBackEps_4.Leave, thr_txtBackEps_5.Leave, thr_txtBackEps_6.Leave, thr_txtBackEps_7.Leave, thr_txtBackEps_8.Leave
        '
        Dim X As Single
        Dim index As Integer
        Dim control As TextBox
        '
        control = CType(sender, Control)
        index = CInt(Microsoft.VisualBasic.Right(control.Name, 1))
        If IsNumeric(control.Text) Then
            X = CSng(control.Text)
            X = value_limiter3(CDbl(X), 0, 1)
            control.Text = Format(X, "#0.00")
            SolarPanels.Item(index).PanelBackEps = X
        Else
            MsgBox("Please enter a number !")
            control.Text = SolarPanels.Item(index).PanelBackEps
            control.Focus()
        End If
        blnIsNewPrj = True
    End Sub

    Private Sub thr_txtCellAlf_1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
    thr_txtCellAlf_1.Leave, thr_txtCellAlf_2.Leave, thr_txtCellAlf_3.Leave, thr_txtCellAlf_4.Leave, thr_txtCellAlf_5.Leave, thr_txtCellAlf_6.Leave, thr_txtCellAlf_7.Leave, thr_txtCellAlf_8.Leave
        '
        Dim X As Single
        Dim index As Integer
        Dim control As TextBox
        '
        control = CType(sender, Control)
        index = CInt(Microsoft.VisualBasic.Right(control.Name, 1))
        If IsNumeric(control.Text) Then
            X = CSng(control.Text)
            X = value_limiter3(CDbl(X), 0, 1)
            control.Text = Format(X, "#0.00")
            SolarPanels.Item(index).PanelCellAlf = X
        Else
            MsgBox("Please enter a number !")
            control.Text = SolarPanels.Item(index).PanelCellAlf
            control.Focus()
        End If
        blnIsNewPrj = True
    End Sub

    Private Sub thr_txtCellEps_1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
    thr_txtCellEps_1.Leave, thr_txtCellEps_2.Leave, thr_txtCellEps_3.Leave, thr_txtCellEps_4.Leave, thr_txtCellEps_5.Leave, thr_txtCellEps_6.Leave, thr_txtCellEps_7.Leave, thr_txtCellEps_8.Leave
        '
        Dim X As Single
        Dim index As Integer
        Dim control As TextBox
        '
        control = CType(sender, Control)
        index = CInt(Microsoft.VisualBasic.Right(control.Name, 1))
        If IsNumeric(control.Text) Then
            X = CSng(control.Text)
            X = value_limiter3(CDbl(X), 0, 1)
            control.Text = Format(X, "#0.00")
            SolarPanels.Item(index).PanelCellEps = X
        Else
            MsgBox("Please enter a number !")
            control.Text = SolarPanels.Item(index).PanelCellEps
            control.Focus()
        End If
        blnIsNewPrj = True
        ''
    End Sub

#End Region

#End Region

    Private Sub cmd_quit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmd_quit.Click
        Me.Hide()
    End Sub

    Private Sub frmpowerconfclosed_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = True
        Me.Hide()
    End Sub

    Private Sub bat_onglet_batterie_TabPage3_Click(sender As Object, e As EventArgs) Handles bat_onglet_batterie_TabPage3.Click

    End Sub
End Class
