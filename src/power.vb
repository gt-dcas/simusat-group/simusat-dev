﻿Module power
    '
    Public Const C_Stefan As Double = 0.0000000567
    '
    ' Configuration parametres
    '
    ' Solar cells performance
    Public cell_type As Integer
    Public cell_eff As Single
    Public cell_surf As Single
    Public cell_tref As Single
    Public cell_isc As Single
    Public cell_imp As Single
    Public cell_voc As Single
    Public cell_vmp As Single
    Public cell_disc As Single
    Public cell_dimp As Single
    Public cell_dvoc As Single
    Public cell_dvmp As Single

    Public cell_radisc As Single
    Public cell_radimp As Single
    Public cell_radvoc As Single
    Public cell_radvmp As Single

    Public cell_ns As Single
    Public th_idat As Long
    Public cell_tetad As Single
    Public cell_tetam As Single
    '
    ' Operation parameters and distribution system performance
    ' 0 Sunshine, 1 Eclipse
    Public pdem(1) As Single
    Public diss(1) As Single
    Public rendgs As Single
    Public Vregul As Single
    Public rendbat As Single
    Public gst_type As Integer  ' 0 non régulé, 1 régulé
    '
    '
    ' SIMUSAT power simulation managing & simulation results
    'Public flux_solaire As Single
    'Public albedo As Single
    'Public alfa_optimal As Double
    'Public alfa_current As Double
    Public T_battery As Double      ' Temperature of batteries in °C
    Public panels_power As Double
    Public puseful As Double
    Public batch As Double
    Public batv As Double           ' Battery voltage (one cell)
    Public bati As Double           ' Battery current (one cell)
    Public pbat As Double
    Public pbat_diss_charge As Double
    Public pexcess As Double
    Public pdiss As Double
    Public charging As Boolean
    Public T_init_panels As Single         ' Initial temperature of panels
    Public Voc As Double
    '
    ' Globals added because of ' Data Exploitation' Window
    ' If not used, may be removed back to their module or Form
    '
    Public pbat_diss As Double
    Public pgs_diss As Double
    Public pcu_diss As Double
    Public bat_discharge_time As Long
    Public bat_charge_time As Long
    Public inDebugMode As Boolean



    Public Sub initialize_power()
        inDebugMode = False
        '
        ' Gives initial values to globals
        ' -------------------------------
        Dim i As Integer
        '
        ' Battery
        batch = chini / 100
        bati = 0.000001 * cnom ' To avoid Logarithm Error
        batv = charge_voltage(bati, T_battery, batch)
        '
        ' Panels temperature
        For i = 1 To SolarPanels.Count()
            SolarPanels.Item(i).PanelTemp = T_init_panels
        Next
        '
        ' Body temperature
        BodyTemp = T_init_body
        printOnConsoleIfBebugMode("BodyTemp T_init_body is:" + BodyTemp.ToString)

        '
        ' Initialise dissipated power
        pdiss = 0
        '
    End Sub

    Public Sub compute_power(ByVal Time As Double, ByVal TimeStep As Double)
        '
        Dim i%
        Dim cells_surface As Single
        Dim panel As CSolarPanel

        '
        ' Updating values for incoming radiations sun, albedo and IR
        '
        flux_calculation(Time)
        '
        ' Orientable panels (à réorganiser)
        ' -----------------
        ' In order to achieve current and optimal facing of solar panels
        ' Note that Azimuth of panels within SAT-centred celestial sphere is panel_azimut
        '**************** Call optimal_orientation()
        '**************** Call panels_rotation_fast(sim_data.step)
        '
        ' Thermal balance to obtain temperature of solar panels for efficiency calculation
        ' --------------------------------------------------------------------------------
        compute_thermal(Time, TimeStep)
        '
        ' Calculates output power available from solar panels
        ' ---------------------------------------------------
        ' Each panel's output power is evaluated separately, since each of it have
        ' different temperature and different efficiency
        panels_power = 0

        For i = 1 To SolarPanels.Count()
            panel = SolarPanels.Item(i)
            printOnConsoleIfBebugMode("panel is: " + panel.ToString)

            cells_surface = cell_ns * panel.PanelNbString * cell_surf / 10000   ' Total surface of cells for the panel
            printOnConsoleIfBebugMode("panel.PanelTemp is: " + panel.PanelTemp.ToString)

            printOnConsoleIfBebugMode("flux_incident_overall_power(panel, Time) is: " + flux_incident_overall_power(panel, Time).ToString)
            printOnConsoleIfBebugMode("cell_efficiency(panel.PanelTemp) is: " + cell_efficiency(panel.PanelTemp).ToString)
            printOnConsoleIfBebugMode("cells_surface is: " + cells_surface.ToString)
            'MsgBox(flux_incident_overall_power(panel, Time)) : MsgBox(cell_efficiency(panel.PanelTemp)) : MsgBox(cells_surface)

            panel.PanelPower = flux_incident_overall_power(panel, Time) * cell_efficiency(panel.PanelTemp) * cells_surface
            panels_power += panel.PanelPower
        Next
        '
        ' Power management
        ' ----------------
        power_manager(TimeStep)
        ''

    End Sub

    Public Function cell_efficiency(ByVal Tcell As Double) As Double
        '
        ' Returns cell efficiency according to temperature and radiations dose.
        ' It's here that the architecture is taking into account :
        ' - regulated: the voltage is the regulated voltage
        ' - not regulated: the voltage is the battery voltage
        '
        ' Input: Cells temperature (°C)
        '
        Dim Vpm, Ipm, Isc, i0, Vt, voltage, intensite As Single
        '
        ' Calculation of solar cells characteristics  
        Voc = cell_voc + cell_dvoc * (Tcell - cell_tref)
        Vpm = cell_vmp + cell_dvmp * (Tcell - cell_tref)
        Ipm = cell_imp + cell_dimp * (Tcell - cell_tref)
        Isc = cell_isc + cell_disc * (Tcell - cell_tref)
        If frmPowerConf.cel_opt_beol_1.Checked Then
            ' EOL case
            Voc = cell_radvoc * Voc
            Vpm = cell_radvmp * Vpm
            Ipm = cell_radimp * Ipm
            Isc = cell_radisc * Isc
        End If
        '
        If gst_type = 0 Then
            voltage = batv * bat_ns     ' Not regulated case
        Else
            voltage = Vregul            ' Regulated case
        End If

        If voltage > Voc * cell_ns Then

            intensite = 0
        Else
            Vt = (Voc - Vpm) / Math.Log(Isc / (Isc - Ipm))
            i0 = Isc / (Math.Exp(Voc / Vt) - 1)
            intensite = (Isc - i0 * (Math.Exp(voltage / Vt / cell_ns) - 1)) ' for one string
        End If

        cell_efficiency = voltage * intensite / (cell_imp * cell_vmp) * cell_eff / cell_ns
        'cell_efficiency = voltage * intensite * 10000 / 1368 / cell_surf / cell_ns
        ''
        'MsgBox("voltage " & voltage & vbCrLf & "intensite" & intensite & vbCrLf & "cell_imp" & cell_imp & vbCrLf & "cell_vmp" & cell_vmp & vbCrLf & "cell_eff" & cell_eff)
    End Function

    Public Sub power_manager(ByVal TimeStep As Double)
        '
        ' Takes global panels_power value (Solar panels output) and compares it to power demands
        ' Excess of power goes to dissipation devices while batteries confront any shortage.
        ' Remember that the eclipse status is given by the boolean blnEclipse
        '
        Dim eclipse As Integer
        Dim dod As Double
        Dim pch As Double
        Dim pde As Double
        Dim c1 As Double
        Dim c2 As Double
        Dim X As Double
        '
        ' Eclipse
        ' -------
        If blnEclipse Then
            eclipse = 1
        Else
            eclipse = 0
        End If
        '
        ' DOD
        ' ---
        dod = (1 - batch) * 100
        '
        ' Initialisations
        ' ---------------
        pbat_diss_charge = 0
        pbat_diss = 0


        printOnConsoleIfBebugMode("panels_power is: " + panels_power.ToString)
        printOnConsoleIfBebugMode("rendgs is: " + rendgs.ToString)
        pgs_diss = panels_power * (1 - rendgs / 100)
        charging = True
        '
        ' Gestion charge et décharge batterie
        ' -----------------------------------
        '
        If panels_power * rendgs / 100 >= pdem(eclipse) Then
            If charging = False Then
                pbat = 0
                batch = batch * self_discharge(T_battery, 1)
                bati = bat_courant_entretien
                bat_charge_time = 0
            Else
                ' Gestion de la charge batterie
                ' -----------------------------
                ' WARNING : The test "batv > chvmax" has been removed to avoid a bug
                ' in "current_charge_ctvoltage" (to be verified later)
                '
                pch = (panels_power * rendgs / 100 - pdem(eclipse)) / bat_ns / bat_np
                '
                ' Current state of charge
                'X = batch + bati * (dblTimeStep * ticks / 1000 * 24) / (kcharge * cnom)    ' The step is in hours as for Cnom (A.h)
                X = batch + bati * (TimeStep * 24) / (kcharge * cnom)    ' The step is in hours as for Cnom (A.h)

                bat_charge_time = bat_charge_time + TimeStep
                '
                ' Stopping charging process when full charge is reached
                If X >= 1 Then
                    X = 1
                    charging = False
                    bat_charge_time = 0
                    'bati = bat_courant_entretien
                End If
                batch = X
                '
                ' Updating Voltage and Current
                bati = cnom * current_charge_ctpower(batch, pch / cnom, bati / cnom, T_battery)
                If bati > ichmax Then bati = ichmax
                batv = charge_voltage(bati / cnom, T_battery, batch)
                If batv > chvmax Then
                    batv = chvmax
                    bati = current_charge_ctvoltage(batch, batv, bati / cnom, T_battery) * cnom
                End If
                pbat = -bat_ns * bat_np * bati * batv
                pbat_diss_charge = Math.Abs(pbat * (kcharge - 1) / kcharge)
                '
            End If
            bat_discharge_time = 0
        Else
            ' Discharge management
            ' --------------------
            '
            ' Current state of charge
            c1 = derating_temp(T_battery)
            c2 = derating_current(bati / cnom)
            batch = batch - bati * (TimeStep * 24) / (c1 * c2 * cnom)
            '
            ' Updating Voltage and Current
            pde = (pdem(eclipse) - panels_power * rendgs / 100) / bat_ns / (rendbat / 100) / bat_np
            bati = cnom * current_discharge_ctpower(batch, pde / cnom, bati / cnom, T_battery)
            batv = discharge_voltage(bati / cnom, T_battery, batch)
            pbat = bat_ns * bat_np * bati * batv
            bat_discharge_time = bat_discharge_time + TimeStep
            '
            ' In case of full discharge
            If batch <= 0 Then
                batch = 0
            End If
            pbat_diss = pbat * (1 - rendbat / 100)
        End If
        '
        ' Power budget
        ' ------------
        If pbat > 0 Then
            pexcess = panels_power * rendgs / 100 + pbat * rendbat / 100 - pdem(eclipse) ' discharge
        Else
            pexcess = panels_power * rendgs / 100 + pbat - pdem(eclipse) ' charge & standby
        End If
        '
        If pexcess < 0 Then
            puseful = pdem(eclipse) + pexcess
        Else
            puseful = pdem(eclipse)
        End If
        '
        pcu_diss = puseful * diss(eclipse) / 100
        '
        ' Power dissipation (excess and conversion)
        ' -----------------------------------------

        printOnConsoleIfBebugMode("pbat_diss is: " + pbat_diss.ToString)
        printOnConsoleIfBebugMode("pbat_diss_charge is: " + pbat_diss_charge.ToString)
        printOnConsoleIfBebugMode("pgs_diss is: " + pgs_diss.ToString)
        pdiss = pbat_diss + pbat_diss_charge + pgs_diss
        printOnConsoleIfBebugMode("pcu_diss is: " + pcu_diss.ToString)
        pdiss = pdiss + pcu_diss
        printOnConsoleIfBebugMode("pdiss is: " + pdiss.ToString)

        If pexcess > 0 Then pdiss = pdiss + pexcess
        ''
        'MsgBox(pdiss)
    End Sub

    Public Function printOnConsoleIfBebugMode(str As String) As String

        If inDebugMode Then
            Console.WriteLine(str)
        End If
        printOnConsoleIfBebugMode = str
    End Function


End Module
