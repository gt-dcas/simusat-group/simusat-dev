﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting



<TestClass()> Public Class ThermalUnitTest

    Dim epsilon As Double = 0.01
    Dim time As Double = 2451544.5 'Jan 01, 2000
    Dim timestep As Double = 300 / secday '5 minute

    ''' <summary>
    ''' To run before every test, data taken from BE
    ''' </summary>
    ''' <remarks></remarks>
    <TestInitialize()> Public Sub satelliteInitialization()
        '
        ' Creating satellite
        Satellite = New CSatellite
        Call Satellite.construct("New_Sat", 65535, 0, 0, True)
        With Satellite
            .satBodyHeight = 0.8
            .satBodyWidth = 0.8
            .primaryAxis = "-z"
            .primaryDir = "nadir"
            .secondaryAxis = "x"
            .secondaryDir = "velocity"
        End With
        '
        ' Creating orbit
        With SatOrbit0
            .Name = "Circular 820"
            .SemiMajor_Axis = 7198.137
            .Eccentricity = 0
            .Inclination = 0
            .Argument_Perigee = 0
            .RAAN = 0
            .Mean_Anomaly = 0
            .epoch = 2451544.5          ' January 1st 2000 at 0h
            .dec1 = 0
            .dec2 = 0
            .bstar = 0
        End With
        SatOrbit = SatOrbit0
        '
        ' Creating new collection of solar panels
        SolarPanels = New Collection
        ''
        '
        'Giving a time for simulation and a time step
        '
        '
        SunPow = 3.8E+26
        load_thermal_models()
        update_Sun(time, SunPos)
        blnEclipse = Eclipse(SatPos, SunPos)
        '
        ' ----------------------------------
        ' Default values (Energy Sub-System)
        ' ----------------------------------
        '
        ' Cells performance
        cell_type = 0
        cell_isc = 0.53
        cell_imp = 0.5
        cell_voc = 1
        cell_vmp = 0.86
        cell_disc = 0.0003
        cell_dimp = 0.00025
        cell_dvoc = -0.002
        cell_dvmp = -0.002
        cell_radisc = 1
        cell_radimp = 1
        cell_radvoc = 1
        cell_radvmp = 1
        cell_ns = 90          ' Value added for EPSUnitTest
        cell_surf = 16
        cell_tref = 28
        cell_tetad = 50
        cell_tetam = 75
        cell_eff = 0.18

        '
        ' Operation parameters and distribution system performance
        pdem(0) = 50    ' 0 Sunshine 1 Eclipse
        diss(0) = 15
        pdem(1) = 50
        diss(1) = 15
        rendgs = 90
        rendbat = 90
        '
        ' Variables related to batteries system with default values
        gst_type = 0         ' Value added for EPSUnitTest
        cnom = 10
        bat_ns = 10
        bat_np = 1
        bat_type = 0
        T_battery = 15       ' Value added for EPSUnitTest
        T_init_panels = 25   ' Value added for EPSUnitTest
        T_init_body = 25     ' Value added for EPSUnitTest
        '
        ' Operation parameters
        chvmax = 1.5
        ichmax = 100
        kcharge = 1.05
        chini = 100
        bat_courant_entretien = 0.001

        '
        ' Charge behaviour parameters
        ch0(0) = 0 : ch0(1) = 0 : ch0(2) = 0 : ch0(3) = 1.3 : ch0(4) = 0 : ch0(5) = 0
        ch1(0) = 15 : ch1(1) = 0.1 : ch1(2) = -5 : ch1(3) = 1.4 : ch1(4) = -0.001 : ch1(5) = 0.01
        ch2(0) = 65 : ch2(1) = 0.5 : ch2(2) = -5 : ch2(3) = 1.41 : ch2(4) = -0.001 : ch2(5) = 0.01
        och(0) = 120 : och(1) = 0.1 : och(2) = -5 : och(3) = 1.59 : och(4) = -0.002 : och(5) = 0.02
        '
        ' Discharge behaviour parameters
        de0(0) = 0 : de0(1) = 0 : de0(2) = 0 : de0(3) = 1.3 : de0(4) = 0 : de0(5) = 0
        de1(0) = 15 : de1(1) = 0.1 : de1(2) = -5 : de1(3) = 1.21 : de1(4) = 0.001 : de1(5) = -0.01
        de2(0) = 95 : de2(1) = 0.1 : de2(2) = -2 : de2(3) = 1.2 : de2(4) = 0.001 : de2(5) = -0.01
        de100(0) = 100 : de100(1) = 0 : de100(2) = 0 : de100(3) = 1.0# : de100(4) = 0 : de100(5) = 0
        cmax_cnom = 1.2
        c10_cnom = 0.6
        tbatmin = -50
        cmin100 = 30
        tbat99 = 50
        '
        ' Self-discharge behaviour parameters
        tc0 = 100
        tc50 = 15
    End Sub


#Region "Panel creation functions"

    ''' <summary>
    ''' Add an orientable panel (Type 1)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y" or "- Y"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type1(ByRef length As Single, ByRef width As Single, ByRef mass As Single, ByRef face As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type1"        ' Orientable Panel
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub

    ''' <summary>
    ''' Add a panel on a face (Type 2)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y","- Y", "+ Z" or "- Z"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type2(ByRef length As Single, ByRef width As Single, ByRef mass As Single, ByRef face As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type2"        ' Panel on a face
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub

    ''' <summary>
    ''' Add a panel fixed perpendicular to a face (Type 3)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y" or "- Y"</param>
    ''' <param name="dir">Direction : "+ Z" or "- Z"</param>
    ''' <param name="hpos">Position : "Upper", "Middle" or "Lower"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type3(ByRef length As Single, ByRef width As Single, ByRef mass As Single,
                                ByRef face As String, ByRef dir As String, ByRef hpos As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type3"        ' Panel fixed perpendicular to a face
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelDir = dir
            .PanelHpos = hpos
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & " : " & .PanelDir & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub


    ''' <summary>
    ''' it adds a type 1 panel; its dimensions can vary according to the different cases
    ''' minimum --> each dimension is set to its minimum
    ''' maximum --> each dimension is set to its maximum
    ''' randomUser --> each dimension is set to a random value written by the author
    ''' randomMachine --> each dimension is set to a random value obtained by using vbnet's random function. Therefore each time the values will differ from the previous ones
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub add_panel1(dimensions As String)

        Select Case dimensions
            Case "minimum"
                add_panel_type1(0.1, 0.1, 0.1, "- X")
            Case "maximum"
                add_panel_type1(20, 20, 50, "- X")
            Case "randomUser"
                add_panel_type1(5, 18, 21, "- X")
            Case "randomMachine"
                add_panel_type1(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(1))
        End Select


    End Sub

    ''' <summary>
    ''' it adds a type 2 panel; its dimensions can vary according to the different cases
    ''' minimum --> each dimension is set to its minimum
    ''' maximum --> each dimension is set to its maximum
    ''' randomUser --> each dimension is set to a random value written by the author
    ''' randomMachine --> each dimension is set to a random value obtained by using vbnet's random function. Therefore each time the values will differ from the previous ones
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub add_panel2(dimensions As String)


        Select Case dimensions
            Case "minimum"
                add_panel_type2(0.1, 0.1, 0.1, "- Y")
            Case "maximum"
                add_panel_type2(20, 20, 50, "- Y")
            Case "randomUser"
                add_panel_type2(6, 13, 12, "- Y")
            Case "randomMachine"
                add_panel_type2(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(2))
        End Select


    End Sub

    ''' <summary>
    ''' it adds a type 3 panel; its dimensions can vary according to the different cases
    ''' minimum --> each dimension is set to its minimum
    ''' maximum --> each dimension is set to its maximum
    ''' randomUser --> each dimension is set to a random value written by the author
    ''' randomMachine --> each dimension is set to a random value obtained by using vbnet's random function. Therefore each time the values will differ from the previous ones
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub add_panel3(dimensions As String)


        Select Case dimensions
            Case "minimum"
                add_panel_type3(0.1, 0.1, 0.1, "+ X", "+ Z", "Lower")
            Case "maximum"
                add_panel_type3(20, 20, 50, "+ X", "+ Z", "Lower")
            Case "randomUser"
                add_panel_type3(7, 16, 39, "+ X", "+ Z", "Lower")
            Case "randomMachine"
                add_panel_type3(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(3), select_random_orientation(), select_random_position())
        End Select


    End Sub


    ''' <summary>
    ''' it randomly chooses a face
    ''' </summary>
    ''' <param name="type"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function select_random_face(ByRef type As Integer) As String

        Select Case type
            Case 1
                Dim faces1() As String = {"+ X", "+ Y", "- X", "- Y"}
                Return faces1(Int(4 * Rnd()) + 0)
            Case 2
                Dim faces2() As String = {"+ X", "+ Y", "+ Z", "- X", "- Y", "- Z"}
                Return faces2(Int(6 * Rnd()) + 0)
            Case 3
                Dim faces3() As String = {"+ X", "+ Y", "- X", "- Y"}
                Return faces3(Int(4 * Rnd()) + 0)
        End Select

        Return "faces" & CStr(type)(Int(4 * Rnd()) + 0)

    End Function

    ''' <summary>
    ''' it randomly chooses an orientation
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function select_random_orientation() As String

        Dim orientation() As String = {"+ Z", "- Z"}
        Return orientation(Int(2 * Rnd()) + 0)

    End Function


    ''' <summary>
    ''' it randomly chooses a position for type 3 panels
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function select_random_position() As String

        Dim orientation() As String = {"Upper", "Middle", "Lower"}
        Return orientation(Int(3 * Rnd()) + 0)

    End Function


#End Region

#Region "CSV functions"
    'Friend Sub ReadCSV()
    '    'Whole file inserted in "orbit_verf_temp" variable
    '    Dim i As Integer = 1 'line 'Starts in one because 0 is the headers
    '    Dim j As Integer = 0 'column
    '    Dim orbit_verf_temp() As String = System.IO.File.ReadAllLines(My.Application.Info.DirectoryPath & EarthTempFile) 'ligne 0:header, 

    '    'We calculate the limits of the table
    '    Dim nlines As Integer = orbit_verf_temp.Length - 1
    '    'Dim ncol As Integer = ColumnNumber(orbit_verf_temp)

    'End Sub



    Private Function CsvVal(ByVal table As Array, ByVal i As Integer, ByVal j As Integer) As Double
        Dim line As String = table(i)
        Dim lineArray() As String = Split(line, ",")
        CsvVal = Val(lineArray(j))
    End Function
#End Region

    <TestMethod> Public Sub conf_factor_earth_sphere_test()

        ' ARRANGE

        Dim d As Double = 0 + RE
        Dim cf_expected As Double = 0.5

        ' ACT

        Dim cf_actual As Double = conf_factor_earth(d, RE)

        ' ASSERT 

        Assert.AreEqual(cf_expected, cf_actual, epsilon)
        '
        '
        'ARRANGE

        d = 1000000
        cf_expected = 0

        ' ACT

        cf_actual = conf_factor_earth(d, RE)

        ' ASSERT 

        Assert.AreEqual(cf_expected, cf_actual, epsilon)
        '
        '
        'ARRANGE

        d = 820 + RE
        cf_expected = 0.268235

        ' ACT

        cf_actual = conf_factor_earth(d, RE)

        ' ASSERT 

        Assert.AreEqual(cf_expected, cf_actual, epsilon)

    End Sub

    <TestMethod> Public Sub conf_factor_rectangle_oneFace_earth_test()

        ' ARRANGE
        Dim d As Double 'distance sat-earthCenter
        Dim eta As Double 'angle face-earthSurface
        Dim cfe_expected As Double
        Dim cfe_actual As Double

        ' ARRANGE (extreme case, face over Earth surface: interpolation to point outside the table)
        d = RE
        eta = 0
        cfe_expected = 1
        ' ACT
        cfe_actual = EarthIRConfFactor(eta, d)
        ' ASSERT
        Assert.AreEqual(cfe_expected, cfe_actual, epsilon)

        ' ARRANGE (case in between some points of the table)
        d = 8007.2264
        eta = 27.41192725
        cfe_expected = 0.563
        ' ACT
        cfe_actual = EarthIRConfFactor(eta, d)
        ' ASSERT
        Assert.AreEqual(cfe_expected, cfe_actual, epsilon)

        ' ARRANGE (extreme case, very long and not facin earth)
        d = 10000
        eta = 180
        cfe_expected = 0
        ' ACT
        cfe_actual = EarthIRConfFactor(eta, d)
        ' ASSERT
        Assert.AreEqual(cfe_expected, cfe_actual, epsilon)

        ' ARRANGE (case to see if it reachs well the table)
        d = RE + 1000
        eta = 60
        cfe_expected = 0.4216
        ' ACT
        cfe_actual = EarthIRConfFactor(eta, d)
        ' ASSERT
        Assert.AreEqual(cfe_expected, cfe_actual, epsilon)

        ' ARRANGE (case to see if interpolates well between angles)
        d = 500 + RE
        eta = 105
        cfe_expected = 0.18105
        ' ACT
        cfe_actual = EarthIRConfFactor(eta, d)
        ' ASSERT
        Assert.AreEqual(cfe_expected, cfe_actual, epsilon)

        ' ARRANGE (case to se if interpolates well between angles and distances)
        d = 750 + RE
        eta = 45
        cfe_expected = 0.5859
        ' ACT
        cfe_actual = EarthIRConfFactor(eta, d)
        ' ASSERT
        Assert.AreEqual(cfe_expected, cfe_actual, epsilon)

    End Sub

    <TestMethod> Public Sub albedo_ESA_test()

        ' ARRANGE
        Dim i As Double 'latitude
        Dim month As Double 'month
        Dim albedo_expected As Double
        Dim albedo_actual As Double

        ' ARRANGE (nominal case: point of the table)
        i = -35
        month = 7
        albedo_expected = 0.32
        ' ACT
        albedo_actual = AlbedoCoef(i, month)
        ' ASSERT 
        Assert.AreEqual(albedo_expected, albedo_actual, epsilon)

        ' ARRANGE (interpolation between table's point)
        i = 0
        month = 7
        albedo_expected = 0.235
        ' ACT
        albedo_actual = AlbedoCoef(i, month)
        ' ASSERT 
        Assert.AreEqual(albedo_expected, albedo_actual, epsilon)

        ' ARRANGE (extrapolation to point outside the table)
        i = 90
        month = 7
        albedo_expected = 0.535
        ' ACT
        albedo_actual = AlbedoCoef(i, month)
        ' ASSERT 
        Assert.AreEqual(albedo_expected, albedo_actual, epsilon)

        ' ARRANGE (extrapolation to point outside the table)
        i = -90
        month = 8
        albedo_expected = 0
        ' ACT
        albedo_actual = AlbedoCoef(i, month)
        ' ASSERT 
        Assert.AreEqual(albedo_expected, albedo_actual, epsilon)

    End Sub

    <TestMethod> Public Sub earth_temp_ESA_test()

        ' ARRANGE
        Dim i As Double
        Dim month As Double
        Dim temp_expected As Double
        Dim temp_actual As Double

        ' ARRANGE (Nominal point in table)
        i = -35
        month = 4
        temp_expected = 257
        ' ACT
        temp_actual = EarthTemp(i, month)
        ' ASSERT 
        Assert.AreEqual(temp_expected, temp_actual, epsilon)

        ' ARRANGE (Case interpolation inside the table)
        i = 50
        month = 7
        temp_expected = 256.5
        ' ACT
        temp_actual = EarthTemp(i, month)
        ' ASSERT 
        Assert.AreEqual(temp_expected, temp_actual, epsilon)

        ' ARRANGE (Extrapolation outside table)
        i = -90
        month = 9
        temp_expected = 215.5
        ' ACT
        temp_actual = EarthTemp(i, month)
        ' ASSERT 
        Assert.AreEqual(temp_expected, temp_actual, epsilon)

        ' ARRANGE (Extrapolation outside table)
        i = 90
        month = 1
        temp_expected = 232
        ' ACT
        temp_actual = EarthTemp(i, month)
        ' ASSERT 
        Assert.AreEqual(temp_expected, temp_actual, epsilon)

    End Sub
    <TestMethod> Public Sub Avg_ESA_test()

        'January Earth Temp
        Dim avg_expected As Double = 249.66666
        Dim avg_actual As Double = AvgCERES(EarthTempFile, 1)

        Assert.AreEqual(avg_expected, avg_actual, epsilon)

        'March albedo Coefficient

        avg_expected = 0.3961
        avg_actual = AvgCERES(AlbedoCoefFile, 3)

        Assert.AreEqual(avg_expected, avg_actual, epsilon)


    End Sub

    <TestMethod> Public Sub earth_Face_angle_test()
        '
        '0° inclination orbit with z axis pointing nadir and x axis pointing velocity (orientation of all faces should be constant)
        '
        Dim timesim As Double = time
        ' ARRANGE
        Dim angle_expected() As Double = New Double() {90, 90, 90, 90, 180, 0}
        'Dim Eps() As Double = New Double() {epsilon, epsilon, epsilon, epsilon, epsilon, epsilon}

        For i As Integer = 1 To 22
            ' ACT
            Call compute_thermal(timesim, timestep)

            ' ASSERT 
            For j As Integer = 0 To 5
                Assert.AreEqual(angle_expected(j), Face_EarthInc(j), epsilon)
            Next

            timesim += timestep
        Next
        '
        '
        '30° inclination case with inertial axis. All angles will change around the orbit
        '
        ' ARRANGE
        timesim = time
        epsilon = 0.001 'difference en degrees

        With Satellite
            .primaryAxis = "z"
            .primaryDir = "inertial z"
            .secondaryAxis = "x"
            .secondaryDir = "inertial x"
        End With
        With SatOrbit0
            .Inclination = 30
        End With
        SatOrbit = SatOrbit0

        'Data found doing the vectorial opperations in matlab by taking the position of the satellite at every point and the normal vector of each face. (earth_Face_angle_test_calculations.m)
        Dim angle_expected1(,) As Double = New Double(21, 5) {{179.940500362, 0.059499638, 89.948463596, 90.051536404, 89.970264407, 90.029735593}, {162.271781721, 17.728218279, 105.287759964, 74.712240036, 98.761385537, 81.238614463}, {144.478965358, 35.521034642, 120.205499165, 59.794500835, 106.894107717, 73.105892283}, {126.682050063, 53.317949937, 133.985570774, 46.014429226, 113.644202897, 66.355797103}, {108.882281087, 71.117718913, 145.028807002, 34.971192998, 118.23302723, 61.76697277}, {91.081026039, 88.918973961, 149.997240016, 30.002759984, 119.979206971, 60.020793029}, {73.279698146, 106.720301854, 146.066982214, 33.933017786, 118.579284464, 61.420715536}, {55.479714876, 124.520285124, 135.560841321, 44.439158679, 114.278715249, 65.721284751}, {37.682462689, 142.317537311, 122.006451516, 57.993548484, 107.731369933, 72.268630067}, {19.889259785, 160.110740215, 107.181200649, 72.818799351, 99.715794326, 80.284205674}, {2.103049894, 177.896950106, 91.868593532, 88.131406468, 90.964633013, 89.035366987}, {15.682269283, 164.317730717, 76.51365405, 103.48634595, 82.145612737, 97.854387263}, {33.458974965, 146.541025035, 61.53049397, 118.46950603, 73.917444346, 106.082555654}, {51.230530156, 128.769469844, 47.574889389, 132.425110611, 66.992273084, 113.007726916}, {68.998003765, 111.001996235, 36.080126069, 143.919873931, 62.140690073, 117.859309927}, {86.763081695, 93.236918305, 30.150207004, 149.849792996, 60.060538053, 119.939461947}, {104.527791567, 75.472208433, 32.984747105, 147.015252895, 61.106694561, 118.893305439}, {122.294206172, 57.705793828, 42.864730007, 137.135269993, 65.095892629, 114.904107371}, {140.064128506, 39.935871494, 56.137777085, 123.862222915, 71.412368047, 108.587631953}, {157.838744616, 22.161255384, 70.838695441, 109.161304559, 79.286717175, 100.713282825}, {175.615807562, 4.384192438, 86.109144722, 93.890855278, 87.982615023, 92.017384977}, {166.591760308, 13.408239692, 101.48319014, 78.51680986, 96.829040786, 83.170959214}}

        For i As Integer = 0 To 21
            ' ACT
            update_Sun(timesim, SunPos)
            SatOrbit = Satellite.getOrbit(timesim) ' Gives orbit parameters, SatPos, SatVel, SatGeoPos
            Call compute_power(timesim, timestep)
            ' ASSERT 
            For j As Integer = 0 To 5
                Assert.AreEqual(angle_expected1(i, j), Face_EarthInc(j), epsilon)
            Next

            timesim += timestep
        Next
    End Sub

    <TestMethod> Public Sub Type2_panel_orientation_Earth_Sun()
        Dim panel As CSolarPanel
        add_panel_type2(1, 1, 1, "+ X")
        add_panel_type2(1, 1, 1, "- X")
        add_panel_type2(1, 1, 1, "+ Y")
        add_panel_type2(1, 1, 1, "- Y")
        add_panel_type2(1, 1, 1, "+ Z")
        add_panel_type2(1, 1, 1, "- Z")
        With Satellite
            .primaryAxis = "z"
            .primaryDir = "inertial z"
            .secondaryAxis = "x"
            .secondaryDir = "inertial x"
        End With
        With SatOrbit0
            .Inclination = 30
        End With
        SatOrbit = SatOrbit0

        For k As Integer = 1 To 22
            For i As Integer = 1 To SolarPanels.Count()
                panel = SolarPanels.Item(i)
                Dim j As Integer
                Select Case panel.PanelFace
                    Case "+ X"
                        j = 0
                    Case "- X"
                        j = 1
                    Case "+ Y"
                        j = 2
                    Case "- Y"
                        j = 3
                    Case "+ Z"
                        j = 4
                    Case "- Z"
                        j = 5
                End Select

                ' ARRANGE
                update_Sun(time, SunPos)
                SatOrbit = Satellite.getOrbit(time)
                Call compute_thermal(time, timestep)

                ' ASSERT 
                Assert.AreEqual(panel.EarthIncidence(time) * 180 / pi, Face_EarthInc(j), epsilon)
                Assert.AreEqual(panel.SunIncidence(time), Face_SunInc(j), epsilon)
            Next
            time += timestep
        Next

    End Sub

    <TestMethod> Public Sub Type3_panel_orientation_Earth_Sun()
        Dim panel As CSolarPanel
        add_panel_type3(1, 1, 1, "+ X", "+ Z", "Upper")
        add_panel_type3(1, 1, 1, "+ Y", "+ Z", "Middle")
        add_panel_type3(1, 1, 1, "- X", "- Z", "Lower")

        With Satellite
            .primaryAxis = "z"
            .primaryDir = "inertial z"
            .secondaryAxis = "x"
            .secondaryDir = "inertial x"
        End With
        With SatOrbit0
            .Inclination = 30
        End With
        SatOrbit = SatOrbit0

        For k As Integer = 1 To 22

            For i As Integer = 1 To SolarPanels.Count()
                panel = SolarPanels.Item(i)
                Dim j As Integer
                Select Case panel.PanelDir
                    Case "+ Z"
                        j = 4
                    Case "- Z"
                        j = 5
                End Select

                ' ARRANGE
                update_Sun(time, SunPos)
                SatOrbit = Satellite.getOrbit(time)
                Call compute_thermal(time, timestep)

                ' ASSERT 
                Assert.AreEqual(panel.EarthIncidence(time) * 180 / pi, Face_EarthInc(j), epsilon)
                Assert.AreEqual(panel.SunIncidence(time), Face_SunInc(j), epsilon)
            Next
            time += timestep

        Next

    End Sub

    <TestMethod> Public Sub Type1_panel_orientation_Sun()

        Dim panel As CSolarPanel
        add_panel_type1(1, 1, 5, "+ X")
        Satellite.primaryAxis = "y"
        Satellite.primaryDir = "sun-pointing"

        For j As Integer = 1 To 22
            SatOrbit = Satellite.getOrbit(time)
            update_Sun(time, SunPos)

            For i As Integer = 1 To SolarPanels.Count()
                panel = SolarPanels.Item(i)
                ' ARRANGE
                Call compute_thermal(time, timestep)
                Dim angle_expected As Double = 0
                ' ASSERT 
                Assert.AreEqual(angle_expected, panel.SunIncidence(time), epsilon)
            Next

            time += timestep
        Next

        SolarPanels.Clear()  'Eliminate every solar panel

        add_panel_type1(1, 1, 5, "+ Y")
        For j As Integer = 1 To 22
            SatOrbit = Satellite.getOrbit(time)
            update_Sun(time, SunPos)

            For i As Integer = 1 To SolarPanels.Count()
                panel = SolarPanels.Item(i)
                ' ARRANGE
                Call compute_thermal(time, timestep)
                Dim angle_expected As Double = Math.PI / 2
                ' ASSERT 
                Assert.AreEqual(angle_expected, panel.SunIncidence(time), epsilon)
            Next

            time += timestep
        Next

    End Sub

    <TestMethod> Public Sub Eclipse_test()

        Dim Ecl As Boolean
        Dim SunDir, earthdir As vector
        Dim CosSunSat, CosEarthSat As Double


        'Point with no eclipse
        With SatOrbit0
            .RAAN = -90 'in front of Sun
        End With
        SatOrbit = SatOrbit0
        SatOrbit = Satellite.getOrbit(time)
        update_Sun(time, SunPos)
        blnEclipse = Eclipse(SatPos, SunPos)
        Ecl = False

        Assert.AreEqual(Ecl, blnEclipse)

        'Point with eclipse
        With SatOrbit0
            .RAAN = 90 'in front of Sun
        End With
        SatOrbit = SatOrbit0
        SatOrbit = Satellite.getOrbit(time)
        update_Sun(time, SunPos)
        Ecl = True

        blnEclipse = Eclipse(SatPos, SunPos)

        Assert.AreEqual(Ecl, blnEclipse)

        'Total Orbit
        For j As Integer = 1 To 22
            SatOrbit = Satellite.getOrbit(time)
            update_Sun(time, SunPos)
            blnEclipse = Eclipse(SatPos, SunPos)

            With Satellite.getSatCOSCoords(time, SunPos)
                SunDir.X = .X : SunDir.Y = .Y : SunDir.Z = .Z
            End With
            With earthdir
                .X = -SatPos.X : .Y = -SatPos.Y : .Z = -SatPos.Z
            End With

            CosSunSat = vctDot(SunPos, earthdir) / vctNorm(SunPos) / vctNorm(earthdir)
            CosEarthSat = Math.Cos(Math.Asin(RE / vctNorm(SatPos)))

            ' ARRANGE
            If CosEarthSat < CosSunSat Then
                Ecl = True
            Else
                Ecl = False
            End If
            ' ASSERT 

            Assert.AreEqual(Ecl, blnEclipse)

            time += timestep
        Next

    End Sub

    <TestMethod> Public Sub solar_intensity_test()
        Dim expected_solar_int As Double
        Dim actual_solar_int As Double
        '
        'Nominal case
        '
        ' ARRANGE 
        update_Sun(time, SunPos)
        expected_solar_int = SunPow / (4 * pi * Math.Pow(vctNorm(SunPos), 2) * 1000000)
        actual_solar_int = solar_intensity(vctNorm(SunPos))
        ' ASSERT
        Assert.AreEqual(expected_solar_int, actual_solar_int, epsilon)

        'Extreme case (satellite at Sun)
        '
        ' ARRANGE
        expected_solar_int = Double.PositiveInfinity
        actual_solar_int = solar_intensity(0)
        ' ASSERT
        Assert.AreEqual(expected_solar_int, actual_solar_int, epsilon)

        'Extreme case (satellite too far from sun)
        '
        ' ARRANGE
        expected_solar_int = 0
        actual_solar_int = solar_intensity(1.0E+25)
        ' ASSERT
        Assert.AreEqual(expected_solar_int, actual_solar_int, epsilon)

    End Sub

    <TestMethod> Public Sub Albedo_test()  'We test that the Albedo is 0 if the angle Sat-Earth-Sun is bigger than 100°, the albedo flux is 0
        Dim cosSunEarthSat As Double
        Dim albedo_expected As Double
        Dim albedo_actual As Double

        ' ARRANGE
        For j As Integer = 1 To 101 ' 1 Orbit to do a well average
            SatOrbit = Satellite.getOrbit(time)
            update_Sun(time, SunPos)
            flux_calculation(time)
            cosSunEarthSat = vctDot(SunPos, SatPos) / vctNorm(SunPos) / vctNorm(SatPos)
            If cosSunEarthSat <= -0.1736648 Then  'If the angle is bigger than 100°
                albedo_expected = 0
                albedo_actual = flux_albedo
                ' ASSERT
                Assert.AreEqual(albedo_expected, albedo_actual, epsilon)
            End If

            ' ASSERT
            time += timestep
        Next

    End Sub

    <TestMethod> Public Sub flux_rad_test()
        Dim Temperature As Double
        Dim flux_expected As Double
        Dim flux_actual As Double

        ' ARRANGE
        Temperature = -273.15
        flux_expected = 0
        flux_actual = flux_rad(Temperature)

        ' ASSERT
        Assert.AreEqual(flux_expected, flux_actual, epsilon)

        ' ARRANGE
        Temperature = -300
        flux_expected = 0
        flux_actual = flux_rad(Temperature)

        ' ASSERT
        Assert.AreEqual(flux_expected, flux_actual, epsilon)

        ' ARRANGE
        Temperature = 20
        flux_expected = 418.7382
        flux_actual = flux_rad(Temperature)

        ' ASSERT
        Assert.AreEqual(flux_expected, flux_actual, epsilon)

    End Sub

    <TestMethod> Public Sub Average_Temp_test()
        'Problem n°2 from CONTROLE THERMIQUE DES VEHICULES SPATIAUX, Richard BRIET pour le service thermique du CNES de Toulouse (p66). Comparison if simusat solution and Excel solution of thermal equations.
        ' The excel file with the calculations  can be found on BiblioAngel/Testcube.xlsx
        epsilon = 0.00001
        timestep = 60 / secday 'timestep set to 60s to avoid NaN in the temperature calculation

        ' INITIALIZATION (data from the source problem's statement)
        ' Creation of a satellite with an equivalent face surface of the proyected surface of an sphere of 0.5m diameter
        With Satellite
            .satBodyHeight = 0.443
            .satBodyWidth = 0.443
        End With
        'Polar Orbit
        With SatOrbit0
            .Inclination = 90
            .RAAN = -90 'To have an orbit with eclipses
            .Mean_Anomaly = 0
        End With
        SatOrbit = SatOrbit0

        pdiss = 150
        BodySpecHeat = 46000
        T_init_body = 36.6
        initialize_power()
        For i As Integer = 0 To 5
            BodyAlf(i) = 0.65
            BodyEps(i) = 0.8

        Next
        '
        ' ARRANGE
        Dim Temp_expected As Double = 30.9789206 'Excel's solution 
        Dim BodyTemptest As Double = 0

        For j As Integer = 1 To 101 ' 1 Orbit to do a well average
            update_Sun(time, SunPos)
            SatOrbit = Satellite.getOrbit(time)
            blnEclipse = Eclipse(SatPos, SunPos)
            pdiss = 150

            Call compute_power(time, timestep)

            BodyTemptest += BodyTemp
            time += timestep
        Next
        BodyTemptest = BodyTemptest / 101 'Average calculation
        '
        ' ASSERT
        Assert.AreEqual(Temp_expected, BodyTemptest, epsilon)
    End Sub

    <TestMethod> Public Sub Orbit_Temp_test()
        'Integration test. We will test if the program finds always the same solution.
        epsilon = 0.001
        timestep = 60 / secday 'timestep set to 60s to avoid NaN in the temperature calculation

        ' INITIALIZATION 
        ' Creation of a satellite with an equivalent face surface of the proyected surface of an sphere of 0.5m diameter

        'Aleatory Orbit
        With SatOrbit0
            .Inclination = 37
            .RAAN = 20 'To have an orbit with eclipses
            .Mean_Anomaly = 0
            .Eccentricity = 0.2
            .SemiMajor_Axis = 30000
        End With
        SatOrbit = SatOrbit0

        BodySpecHeat = 46000
        T_init_body = 27
        pdem(0) = 0
        pdem(1) = 0
        diss(0) = 0
        diss(1) = 0
        initialize_power()
        For i As Integer = 0 To 5
            BodyAlf(i) = 0.65
            BodyEps(i) = 0.8

        Next

        '
        ' ARRANGE
        'Read CSV with data
        'Whole file inserted in "orbit_verf_temp" variable
        Dim m As Integer = 1 'line 'Starts in one because 0 is the headers
        Dim n As Integer = 0 'column
        Dim orbit_verf_temp() As String = System.IO.File.ReadAllLines(My.Application.Info.DirectoryPath & "/resources/orbit_temp_test_data.csv") 'ligne 0:header, 

        'We calculate the limits of the table
        Dim nlines As Integer = orbit_verf_temp.Length - 1
        'Dim ncol As Integer = ColumnNumber(orbit_verf_temp)
        '
        Dim Temp_expected As Double


        For j As Integer = 0 To 100 ' 1 Orbit to do a well average
            update_Sun(time, SunPos)
            SatOrbit = Satellite.getOrbit(time)
            blnEclipse = Eclipse(SatPos, SunPos)

            Call compute_power(time, timestep)

            '
            ' ARRANGE
            Temp_expected = CsvVal(orbit_verf_temp, j + 1, 0) 'First line is the header
            '
            ' ASSERT
            Assert.AreEqual(Temp_expected, BodyTemp, epsilon)

            time += timestep
        Next

    End Sub

    ''' <summary>
    ''' To run at the end of every test
    ''' </summary>
    ''' <remarks></remarks>
    <TestCleanup()> Public Sub fin()
        '
        SolarPanels.Clear()  'Eliminate every solar panel
        '
    End Sub

End Class