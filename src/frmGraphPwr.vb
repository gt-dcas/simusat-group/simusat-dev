﻿Imports System.Windows.Forms.DataVisualization.Charting

Public Class frmGraphPwr

    Private Sub frmGraph_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        '
        'Dim i As Integer
        'Dim panel As CSolarPanel
        Dim date_temp As udtDate
        '

        ' Initialisation date début par défaut
        '
        convert_Julian_Gregorian(SatOrbit0.epoch, date_temp)
        With date_temp
            txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        '
        ' Initialisation date fin par défaut
        '
        convert_Julian_Gregorian(SatOrbit0.epoch + 1 / 24, date_temp)
        With date_temp
            txt_date_fin.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                    & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        '
        ' Initialisations diverses
        '
        cmbGeneral.Items.Add("Battery current")
        cmbGeneral.Items.Add("Battery charge")
        cmbGeneral.Items.Add("Battery voltage")
        cmbGeneral.Items.Add("Batterie power")
        cmbGeneral.Items.Add("Panels power")
        cmbGeneral.Items.Add("Available power")
        cmbGeneral.Items.Add("Excess power")
        cmbGeneral.Items.Add("Useful power")
        cmbGeneral.Items.Add("Body temperature")
        cmbGeneral.SelectedIndex = 0
        '
        cmbPanelData.Items.Add("Sun incidence")
        cmbPanelData.Items.Add("Albedo incidence")
        cmbPanelData.Items.Add("Solar flux")
        cmbPanelData.Items.Add("Albedo flux")
        cmbPanelData.Items.Add("IR flux")
        cmbPanelData.Items.Add("Total flux")
        cmbPanelData.Items.Add("Efficient flux")
        cmbPanelData.Items.Add("Power")
        cmbPanelData.Items.Add("Temperature")
        cmbPanelData.SelectedIndex = 0
        '
        cmbPanelName.Enabled = False
        cmbPanelData.Enabled = False
        cmbGeneral.Enabled = True
        optParGen.Checked = True
        '
        'If SolarPanels.Count = 0 Then Exit Sub
        'cmbPanelName.Items.Clear()
        'For i = 1 To SolarPanels.Count()
        '    panel = SolarPanels.Item(i)
        '    cmbPanelName.Items.Add(panel.PanelName)
        'Next
        'cmbPanelName.SelectedIndex = 0

        ''
    End Sub

    Private Sub frmGraph_Activated(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Activated
        Dim i As Integer
        Dim panel As CSolarPanel

        cmbPanelName.Items.Clear()
        If SolarPanels.Count = 0 Then Exit Sub
        For i = 1 To SolarPanels.Count()
            Panel = SolarPanels.Item(i)
            cmbPanelName.Items.Add(Panel.PanelName)
        Next
        cmbPanelName.SelectedIndex = 0

        ''
    End Sub

    ''' <summary>
    ''' Define shortcuts Save / Draw
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmGraphPwr_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyData = (Keys.Control Or Keys.S) Then
            cmdSave_Click(cmdSave, New System.EventArgs)
            e.SuppressKeyPress = True ' Pas besoin que l'évènement KeyPress le gère on vient de le faire
        ElseIf e.KeyData = (Keys.Control Or Keys.Enter) Then
            cmdDraw_Click(cmdDraw, New System.EventArgs)
            e.SuppressKeyPress = True ' Pas besoin que l'évènement KeyPress le gère on vient de le faire
        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        '
        Dim NewData As String
        Dim i As Integer
        '

        If lstData.Items.Count = 8 Then
            MsgBox("You cannot select more than 8 parameters") : Exit Sub
        End If
        '
        If optParGen.Checked Then
            NewData = cmbGeneral.Items(cmbGeneral.SelectedIndex)
            For i = 0 To lstData.Items.Count - 1
                If lstData.Items(i) = NewData Then Exit Sub ' If the parameter is already in the list don't put it anather time
            Next
            lstData.Items.Add(NewData)
        Else
            If SolarPanels.Count = 0 Then Exit Sub
            NewData = "P" & (cmbPanelName.SelectedIndex + 1) & "_" & cmbPanelData.Items(cmbPanelData.SelectedIndex)
            For i = 0 To lstData.Items.Count - 1
                If lstData.Items(i) = NewData Then Exit Sub ' If the parameter is already in the list no action is done
            Next
            lstData.Items.Add(NewData)
        End If
        ''
    End Sub

    Private Sub cmdDraw_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDraw.Click
        Graph.ChartAreas("ChartArea1").AxisX.StripLines.Clear() 'Delete Eclipse shadow to adjust to the timestep
        draw_curves()
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        If lstData.SelectedIndex <> -1 Then
            lstData.Items.Remove(lstData.Items(lstData.SelectedIndex))
        End If
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        '
        Dim filename As String
        '
        dlgGraphSave.FileName = "Sans titre"
        dlgGraphSave.Filter = "(*.bmp)|*.bmp"
        If dlgGraphSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
        '
        filename = dlgGraphSave.FileName
        Graph.SaveImage(filename, ChartImageFormat.Bmp)
        '
    End Sub

    Private Sub optParPan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optParPan.CheckedChanged
        If optParPan.Checked Then
            If SolarPanels.Count <> 0 Then
                cmbGeneral.Enabled = False
                cmbPanelData.Enabled = True
                cmbPanelName.Enabled = True
            Else                    'If no panel exists, don't allow to have it checked
                optParPan.Checked = False
                optParGen.Checked = True
                MsgBox("There aren't solar panels on your satellite.")
            End If
        Else
            cmbGeneral.Enabled = True
            cmbPanelData.Enabled = False
            cmbPanelName.Enabled = False
        End If

    End Sub


    Private Sub draw_curves()
        '
        Dim i As Integer, j As Integer
        Dim NbCurves As Integer             ' Number of curves to draw
        Dim ParameterName(7) As String
        Dim ParameterUnit(7) As String
        Dim ParameterAxis(7) As Boolean

        Dim dblReportStart, dblReportTime As Double, dblReportEnd As Double, dblReportStep As Double
        Dim date_temp As udtDate
        Dim panel As CSolarPanel
        Dim x$
        ' 
        ' Init parameters to draw
        ' --------------------------
        NbCurves = lstData.Items.Count
        For i = 0 To NbCurves - 1
            ParameterName(i) = lstData.Items(i)
            ParameterAxis(i) = False
        Next
        If chkAxis_1.Checked Then ParameterAxis(0) = True
        If chkAxis_2.Checked Then ParameterAxis(1) = True
        If chkAxis_3.Checked Then ParameterAxis(2) = True
        If chkAxis_4.Checked Then ParameterAxis(3) = True
        If chkAxis_5.Checked Then ParameterAxis(4) = True
        If chkAxis_6.Checked Then ParameterAxis(5) = True
        If chkAxis_7.Checked Then ParameterAxis(7) = True
        If chkAxis_8.Checked Then ParameterAxis(8) = True
        '
        ' Preparing arrays to draw charts
        ' -------------------------------
        Dim tabXM(lstData.Items.Count - 1) As ArrayList
        Dim tabYM(lstData.Items.Count - 1) As ArrayList
        '
        For i = 0 To NbCurves - 1
            tabXM(i) = New ArrayList
            tabYM(i) = New ArrayList
        Next
        '
        ' Power simulation
        ' ----------------

        ' Read start date, end date, step
        With date_temp
            .year = Year(txt_date_debut.Value)
            .month = Month(txt_date_debut.Value)
            .day = Microsoft.VisualBasic.DateAndTime.Day(txt_date_debut.Value)
            .hour = Hour(txt_date_debut.Value)
            .minute = Minute(txt_date_debut.Value)
            .second = Second(txt_date_debut.Value)
        End With
        dblReportStart = convert_Gregorian_Julian(date_temp)
        '
        With date_temp
            .year = Year(txt_date_fin.Value)
            .month = Month(txt_date_fin.Value)
            .day = Microsoft.VisualBasic.DateAndTime.Day(txt_date_fin.Value)
            .hour = Hour(txt_date_fin.Value)
            .minute = Minute(txt_date_fin.Value)
            .second = Second(txt_date_fin.Value)
        End With
        '
        dblReportEnd = convert_Gregorian_Julian(date_temp)
        If dblReportEnd < dblReportStart Then
            MsgBox("End date is earlier than start date", MsgBoxStyle.OkOnly, "")
            Exit Sub
        End If
        x$ = updwTimeStep.Text
        x$ = Microsoft.VisualBasic.Left(x$, Len(x$) - 2)
        dblReportStep = CDbl(x$) / secday
        '
        If (dblReportEnd - dblReportStart) / dblReportStep > 6000 Then
            If (MessageBox.Show("You have selected a long analysis which may need much time to compute. Do you want to continue ?", "Long analysis...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No) Then
                Exit Sub
            End If
        End If

        Me.Cursor = Cursors.WaitCursor
        '
        ' Initialisation
        dblReportTime = dblReportStart
        '
        ' Power initialisation
        initialize_power()
        '
        ' Calculate and write data in the arrays
        '
        Dim count As Integer = 0  'For the striplines
        '
        Do While dblReportTime <= dblReportEnd
            ' 

            ' Orbitography
            update_Sun(dblReportTime, SunPos)
            SatOrbit = Satellite.getOrbit(dblReportTime)
            blnEclipse = Eclipse(SatPos, SunPos)
            '
            ' Power simulation
            compute_power(dblReportTime, dblReportStep)
            '
            ' Write data
            convert_Julian_Gregorian(dblReportTime, date_temp)
            With date_temp
                x$ = Format(.day, "00") & "/" & Format(.month, "00") & "/" & Format(.year, "0000") & " "
                x$ += Format(.hour, "00") & ":" & Format(.minute, "00") & ":" & Format(.second, "00") & " "
            End With
            '
            For i = 0 To NbCurves - 1
                x$ = ParameterName(i)
                Select Case x$
                    Case "Battery current"
                        If pbat > 0 Then
                            tabYM(i).Add(-bati * bat_np) ' Assume that current is negative in case of discharge
                        Else
                            tabYM(i).Add(bati * bat_np)
                        End If
                        If Double.IsNaN(bati * bat_np) Then
                            MsgBox("Batery current returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        End If
                    Case "Battery charge" : tabYM(i).Add(batch * 100)
                        If Double.IsNaN(batch) Then
                            MsgBox("Batery charge returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        End If
                    Case "Battery voltage" : tabYM(i).Add(batv * bat_ns)
                        If Double.IsNaN(batv * bat_ns) Then
                            MsgBox("Battery voltage returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        End If
                    Case "Batterie power" : tabYM(i).Add(pbat)
                        If Double.IsNaN(pbat) Then
                            MsgBox("Battery power returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        End If
                    Case "Panels power" : tabYM(i).Add(panels_power)
                        If Double.IsNaN(panels_power) Then
                            MsgBox("Panels returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        End If
                    Case "Available power" : tabYM(i).Add(pexcess + puseful)
                        If Double.IsNaN(pexcess + puseful) Then
                            MsgBox("Available power returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        End If
                    Case "Excess power" : tabYM(i).Add(pexcess)
                        If Double.IsNaN(pexcess) Then
                            MsgBox("Excess power returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        End If
                    Case "Useful power" : tabYM(i).Add(puseful)
                        If Double.IsNaN(puseful) Then
                            MsgBox("Useful power returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        End If
                    Case "Body temperature" : tabYM(i).Add(BodyTemp)
                        If Double.IsNaN(BodyTemp) Then
                            MsgBox("Body temperature returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        End If
                        '
                    Case Else   ' Case of a solar panel parameter
                        '
                        j = CInt(Microsoft.VisualBasic.Mid(x$, 2, 1))
                        panel = SolarPanels.Item(j)
                        Select Case Microsoft.VisualBasic.Right(x$, Len(x$) - 3)
                            Case "Sun incidence" : tabYM(i).Add(panel.SunIncidence(dblReportTime) * deg)
                                If Double.IsNaN(panel.SunIncidence(dblReportTime)) Then
                                    MsgBox("Panel to Sun incidence returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                                    Me.Cursor = Cursors.Default
                                    Exit Sub
                                End If
                            Case "Albedo incidence" : tabYM(i).Add(panel.EarthIncidence(dblReportTime) * deg)
                                If Double.IsNaN(panel.EarthIncidence(dblReportTime)) Then
                                    MsgBox("Panel to Earth incidence returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                                    Me.Cursor = Cursors.Default
                                    Exit Sub
                                End If
                            Case "Solar flux" : tabYM(i).Add(panel.PanelSolarFlux)
                                If Double.IsNaN(panel.PanelSolarFlux) Then
                                    MsgBox("Panel solar flux temperature returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                                    Me.Cursor = Cursors.Default
                                    Exit Sub
                                End If
                            Case "Albedo flux" : tabYM(i).Add(panel.PanelAlbedoFlux)
                                If Double.IsNaN(panel.PanelAlbedoFlux) Then
                                    MsgBox("Panel Albedo flux returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                                    Me.Cursor = Cursors.Default
                                    Exit Sub
                                End If
                            Case "IR flux" : tabYM(i).Add(panel.PanelIRFlux)
                                If Double.IsNaN(panel.PanelIRFlux) Then
                                    MsgBox("Panel flux infrarred returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                                    Me.Cursor = Cursors.Default
                                    Exit Sub
                                End If
                            Case "Total flux" : tabYM(i).Add(panel.PanelIRFlux + panel.PanelAlbedoFlux + panel.PanelSolarFlux)
                                If Double.IsNaN(panel.PanelIRFlux + panel.PanelAlbedoFlux + panel.PanelSolarFlux) Then
                                    MsgBox("Panel total flux returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                                    Me.Cursor = Cursors.Default
                                    Exit Sub
                                End If
                            Case "Efficient flux" : tabYM(i).Add(panel.PanelEffFlux)
                                If Double.IsNaN(panel.PanelEffFlux) Then
                                    MsgBox("Panel Efficient flux returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                                    Me.Cursor = Cursors.Default
                                    Exit Sub
                                End If
                            Case "Power" : tabYM(i).Add(panel.PanelPower)
                                If Double.IsNaN(panel.PanelPower) Then
                                    MsgBox("Panel power returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                                    Me.Cursor = Cursors.Default
                                    Exit Sub
                                End If
                            Case "Temperature" : tabYM(i).Add(panel.PanelTemp)
                                If Double.IsNaN(panel.PanelTemp) Then
                                    MsgBox("Panel temperature returned NaN at some point." & vbCrLf & "Please, check the imput data and/or the time step chosen", MsgBoxStyle.Exclamation, "Warning!")
                                    Me.Cursor = Cursors.Default
                                    Exit Sub
                                End If
                        End Select
                        '
                End Select
            Next
            '
            '
            For i = 0 To NbCurves - 1
                tabXM(i).Add((dblReportTime - dblReportStart) * 1440)   ' X axis in minutes since beginning simulation
            Next
            dblReportTime = dblReportTime + dblReportStep

            If blnEclipse Then
                ' Adding grey shadow for eclipses
                AddStripline(dblReportStep, count)
            End If
            count += 1
            '
        Loop
        '
        ' Drawing curves on the graph
        ' ---------------------------
        '
        ' Parameters units
        For i = 0 To NbCurves - 1
            x$ = ParameterName(i)
            Select Case x$
                Case "Battery current" : x$ = x$ & " (A)"
                Case "Battery charge" : x$ = x$ & " (%)"
                Case "Battery voltage" : x$ = x$ & " (V)"
                Case "Batterie power", "Panels power", "Available power", "Excess power", "Useful power" : x$ = x$ & " (W)"
                Case "Body temperature" : x$ = x$ & " (°C)"
                    '
                Case Else   ' Case of a solar panel parameter
                    '
                    j = CInt(Microsoft.VisualBasic.Mid(x$, 2, 1))
                    panel = SolarPanels.Item(j)
                    Select Case Microsoft.VisualBasic.Right(x$, Len(x$) - 3)
                        Case "Sun incidence", "Albedo incidence" : x$ = x$ & " (°)"
                        Case "Solar flux", "Albedo flux", "IR flux", "Total flux", "Efficient flux" : x$ = x$ & " (W/m²)"
                        Case "Power" : x$ = x$ & " (W))"
                        Case "Temperature" : x$ = x$ & " (°C)"
                    End Select
                    '
            End Select
            ParameterName(i) = x$
        Next


        '

        With Graph
            .Titles("Title1").Text = "Simulation from " & Format(txt_date_debut.Value, "dd/MM/yyyy HH:mm:ss") & " to " & Format(txt_date_fin.Value, "dd/MM/yyyy HH:mm:ss")
            ' X and Y axis definition
            .ChartAreas("ChartArea1").AxisX.Title = "Time (minutes from the start of the simulation)"
            .ChartAreas("ChartArea1").AxisX.IntervalAutoMode = DataVisualization.Charting.IntervalAutoMode.VariableCount
            .ChartAreas("ChartArea1").AxisY.IntervalAutoMode = DataVisualization.Charting.IntervalAutoMode.VariableCount
            .ChartAreas("ChartArea1").AxisY2.IntervalAutoMode = DataVisualization.Charting.IntervalAutoMode.VariableCount
            '
            ' Drawing curves
            '
            .Series.Clear()
            For i = 0 To NbCurves - 1
                .Series.Add(ParameterName(i))
                .Series(ParameterName(i)).Points.DataBindXY(tabXM(i), tabYM(i))
                If ParameterAxis(i) = True Then
                    .Series(ParameterName(i)).YAxisType = AxisType.Secondary
                Else
                    .Series(ParameterName(i)).YAxisType = AxisType.Primary
                End If
                .Series(ParameterName(i)).ChartType = DataVisualization.Charting.SeriesChartType.Line
                .Series(ParameterName(i)).BorderWidth = 2
            Next
            '
        End With
        Me.Cursor = Cursors.Default

        ''
    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Hide()
    End Sub

    Private Sub AddStripline(dblReportStep As Double, count As Integer)
        Dim stripline1 As New StripLine
        stripline1.BackColor = Color.Gray
        stripline1.BorderColor = Color.Gray
        stripline1.Interval = 0
        stripline1.IntervalOffset = count * dblReportStep * secday / 60
        stripline1.StripWidth = dblReportStep * secday / 60

        'stripline1.StripWidth = dblReportStep * secday / 60
        Graph.ChartAreas("ChartArea1").AxisX.StripLines.Add(stripline1)
    End Sub
End Class