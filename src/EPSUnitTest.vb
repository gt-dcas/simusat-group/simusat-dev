﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting



<TestClass()> Public Class EPSUnitTest

    Dim cells_surface As Double
    Dim panel As CSolarPanel
    Dim epsilon As Double = 0.01
    Dim time As Double = 2451944.5          ' January 1st 2000 at 0h + 400 sec
    Dim timestep As Double = 300 / secday   '5 minute
    Dim PanelNbString As Single = 1

    ''' <summary>
    ''' To run before every test, data taken from BE
    ''' </summary>
    ''' <remarks></remarks>
    <TestInitialize()> Public Sub satelliteInitialization()
        '
        ' Creating satellite
        Satellite = New CSatellite
        Call Satellite.construct("New_Sat", 65535, 0, 0, True)
        With Satellite
            .satBodyHeight = 0.8
            .satBodyWidth = 0.8
            .primaryAxis = "-z"
            .primaryDir = "nadir"
            .secondaryAxis = "x"
            .secondaryDir = "velocity"
        End With
        '
        ' Creating orbit
        With SatOrbit0
            .Name = "Circular 820"
            .SemiMajor_Axis = 7198.137
            .Eccentricity = 0
            .Inclination = 0
            .Argument_Perigee = 0
            .RAAN = 0
            .Mean_Anomaly = 0
            .epoch = 2451944.5          ' January 1st 2000 at 0h
            .dec1 = 0
            .dec2 = 0
            .bstar = 0
        End With
        SatOrbit = SatOrbit0
        '
        ' Creating new collection of solar panels
        SolarPanels = New Collection
        ''
        '
        'Giving a time for simulation and a time step
        '
        '
        SunPow = 3.8E+26
        load_thermal_models()
        update_Sun(time, SunPos)
        blnEclipse = Eclipse(SatPos, SunPos)
        '
        ' ----------------------------------
        ' Default values (Energy Sub-System)
        ' ----------------------------------
        '
        ' Cells performance
        cell_type = 0
        cell_isc = 0.53
        cell_imp = 0.5
        cell_voc = 1
        cell_vmp = 0.86
        cell_disc = 0.0003
        cell_dimp = 0.00025
        cell_dvoc = -0.002
        cell_dvmp = -0.002
        cell_radisc = 1
        cell_radimp = 1
        cell_radvoc = 1
        cell_radvmp = 1
        cell_ns = 90          ' Value added for EPSUnitTest
        cell_surf = 16
        cell_tref = 28
        cell_tetad = 50
        cell_tetam = 75
        cell_eff = 0.18

        '
        ' Operation parameters and distribution system performance
        pdem(0) = 50    ' 0 Sunshine 1 Eclipse
        diss(0) = 15
        pdem(1) = 50
        diss(1) = 15
        rendgs = 90
        rendbat = 90
        '
        ' Variables related to batteries system with default values
        gst_type = 0         ' Value added for EPSUnitTest
        cnom = 10
        bat_ns = 10
        bat_np = 1
        bat_type = 0
        T_battery = 15       ' Value added for EPSUnitTest
        T_init_panels = 25   ' Value added for EPSUnitTest
        T_init_body = 25     ' Value added for EPSUnitTest
        '
        ' Operation parameters
        chvmax = 1.5
        ichmax = 100
        kcharge = 1.05
        chini = 100
        bat_courant_entretien = 0.001

        '
        ' Charge behaviour parameters
        ch0(0) = 0 : ch0(1) = 0 : ch0(2) = 0 : ch0(3) = 1.3 : ch0(4) = 0 : ch0(5) = 0
        ch1(0) = 15 : ch1(1) = 0.1 : ch1(2) = -5 : ch1(3) = 1.4 : ch1(4) = -0.001 : ch1(5) = 0.01
        ch2(0) = 65 : ch2(1) = 0.5 : ch2(2) = -5 : ch2(3) = 1.41 : ch2(4) = -0.001 : ch2(5) = 0.01
        och(0) = 120 : och(1) = 0.1 : och(2) = -5 : och(3) = 1.59 : och(4) = -0.002 : och(5) = 0.02
        '
        ' Discharge behaviour parameters
        de0(0) = 0 : de0(1) = 0 : de0(2) = 0 : de0(3) = 1.3 : de0(4) = 0 : de0(5) = 0
        de1(0) = 15 : de1(1) = 0.1 : de1(2) = -5 : de1(3) = 1.21 : de1(4) = 0.001 : de1(5) = -0.01
        de2(0) = 95 : de2(1) = 0.1 : de2(2) = -2 : de2(3) = 1.2 : de2(4) = 0.001 : de2(5) = -0.01
        de100(0) = 100 : de100(1) = 0 : de100(2) = 0 : de100(3) = 1.0# : de100(4) = 0 : de100(5) = 0
        cmax_cnom = 1.2
        c10_cnom = 0.6
        tbatmin = -50
        cmin100 = 30
        tbat99 = 50
        '
        ' Self-discharge behaviour parameters
        tc0 = 100
        tc50 = 15
    End Sub

#Region "Panel creation functions"

    ''' <summary>
    ''' Add an orientable panel (Type 1)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y" or "- Y"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type1(ByRef length As Single, ByRef width As Single, ByRef mass As Single, ByRef face As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type1"        ' Orientable Panel
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub

    ''' <summary>
    ''' Add a panel on a face (Type 2)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y","- Y", "+ Z" or "- Z"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type2(ByRef length As Single, ByRef width As Single, ByRef mass As Single, ByRef face As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type2"        ' Panel on a face
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            '.PanelTemp = temp
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub

    ''' <summary>
    ''' Add a panel fixed perpendicular to a face (Type 3)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y" or "- Y"</param>
    ''' <param name="dir">Direction : "+ Z" or "- Z"</param>
    ''' <param name="hpos">Position : "Upper", "Middle" or "Lower"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type3(ByRef length As Single, ByRef width As Single, ByRef mass As Single,
                                ByRef face As String, ByRef dir As String, ByRef hpos As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type3"        ' Panel fixed perpendicular to a face
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelDir = dir
            .PanelHpos = hpos
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & " : " & .PanelDir & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub


    ''' <summary>
    ''' it adds a type 1 panel; its dimensions can vary according to the different cases
    ''' minimum --> each dimension is set to its minimum
    ''' maximum --> each dimension is set to its maximum
    ''' randomUser --> each dimension is set to a random value written by the author
    ''' randomMachine --> each dimension is set to a random value obtained by using vbnet's random function. Therefore each time the values will differ from the previous ones
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub add_panel1(dimensions As String)

        Select Case dimensions
            Case "minimum"
                add_panel_type1(0.1, 0.1, 0.1, "- X")
            Case "maximum"
                add_panel_type1(20, 20, 50, "- X")
            Case "randomUser"
                add_panel_type1(5, 18, 21, "- X")
            Case "randomMachine"
                add_panel_type1(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(1))
        End Select


    End Sub

    ''' <summary>
    ''' it adds a type 2 panel; its dimensions can vary according to the different cases
    ''' minimum --> each dimension is set to its minimum
    ''' maximum --> each dimension is set to its maximum
    ''' randomUser --> each dimension is set to a random value written by the author
    ''' randomMachine --> each dimension is set to a random value obtained by using vbnet's random function. Therefore each time the values will differ from the previous ones
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub add_panel2(dimensions As String)


        Select Case dimensions
            Case "minimum"
                add_panel_type2(0.1, 0.1, 0.1, "- Y")
            Case "maximum"
                add_panel_type2(20, 20, 50, "- Y")
            Case "randomUser"
                add_panel_type2(6, 13, 12, "- Y")
            Case "randomMachine"
                add_panel_type2(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(2))
        End Select


    End Sub

    ''' <summary>
    ''' it adds a type 3 panel; its dimensions can vary according to the different cases
    ''' minimum --> each dimension is set to its minimum
    ''' maximum --> each dimension is set to its maximum
    ''' randomUser --> each dimension is set to a random value written by the author
    ''' randomMachine --> each dimension is set to a random value obtained by using vbnet's random function. Therefore each time the values will differ from the previous ones
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub add_panel3(dimensions As String)


        Select Case dimensions
            Case "minimum"
                add_panel_type3(0.1, 0.1, 0.1, "+ X", "+ Z", "Lower")
            Case "maximum"
                add_panel_type3(20, 20, 50, "+ X", "+ Z", "Lower")
            Case "randomUser"
                add_panel_type3(7, 16, 39, "+ X", "+ Z", "Lower")
            Case "randomMachine"
                add_panel_type3(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(3), select_random_orientation(), select_random_position())
        End Select


    End Sub


    ''' <summary>
    ''' it randomly chooses a face
    ''' </summary>
    ''' <param name="type"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function select_random_face(ByRef type As Integer) As String

        Select Case type
            Case 1
                Dim faces1() As String = {"+ X", "+ Y", "- X", "- Y"}
                Return faces1(Int(4 * Rnd()) + 0)
            Case 2
                Dim faces2() As String = {"+ X", "+ Y", "+ Z", "- X", "- Y", "- Z"}
                Return faces2(Int(6 * Rnd()) + 0)
            Case 3
                Dim faces3() As String = {"+ X", "+ Y", "- X", "- Y"}
                Return faces3(Int(4 * Rnd()) + 0)
        End Select

        Return "faces" & CStr(type)(Int(4 * Rnd()) + 0)

    End Function

    ''' <summary>
    ''' it randomly chooses an orientation
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function select_random_orientation() As String

        Dim orientation() As String = {"+ Z", "- Z"}
        Return orientation(Int(2 * Rnd()) + 0)

    End Function


    ''' <summary>
    ''' it randomly chooses a position for type 3 panels
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function select_random_position() As String

        Dim orientation() As String = {"Upper", "Middle", "Lower"}
        Return orientation(Int(3 * Rnd()) + 0)

    End Function


#End Region


    <TestMethod> Public Sub panelsPower()

        ' ARRANGE 

        Dim panel As CSolarPanel
        Dim power_actual As Double
        Dim power_expected As Double = 9.5
        Dim epsilon As Double = 0.15
        Dim panel_power As Double = 0
        Dim i As Integer


        add_panel_type2(0.8, 0.8, 1, "+ X")
        add_panel_type2(0.8, 0.8, 1, "- X")
        add_panel_type2(0.8, 0.8, 1, "+ Y")
        add_panel_type2(0.8, 0.8, 1, "- Y")
        add_panel_type2(0.8, 0.8, 1, "+ Z")
        add_panel_type2(0.8, 0.8, 1, "- Z")

        With Satellite
            .primaryAxis = "z"
            .primaryDir = "inertial z"
            .secondaryAxis = "x"
            .secondaryDir = "inertial x"
        End With
        With SatOrbit0
            .Inclination = 30
        End With
        SatOrbit = SatOrbit0

        ' Battery
        batch = chini / 100
        bati = 0.000001 * cmax_cnom ' To avoid Logarithm Error
        batv = charge_voltage(bati, T_battery, batch)

        ' Panels temperature
        For i = 1 To SolarPanels.Count()
            'SolarPanels.Item(i).PanelTemp = T_init_panels
            panel = SolarPanels.Item(i)
            cells_surface = cell_ns * PanelNbString * cell_surf / 10000   ' Total surface of cells for the panel
            panel.PanelPower = flux_incident_overall_power(panel, time) * cell_efficiency(panel.PanelTemp) * cells_surface
            panel_power += panel.PanelPower

        Next

        ' ACT

        power_actual = panel_power
        Call flux_calculation(time)
        Call compute_thermal(time, timestep)


        ' ASSERT 

        Assert.AreEqual(power_expected, power_actual, epsilon)

    End Sub

    <TestMethod> Public Sub batteryChargeVoltage()

        ' ARRANGE

        Dim epsilon As Double = 0.1
        Dim batv_expected As Double = 1.4


        ' Battery
        batch = chini / 100
        bati = 0.000001 * cmax_cnom ' To avoid Logarithm Error        '

        ' ACT
        batv = charge_voltage(bati, T_battery, batch)

        ' ASSERT 

        Assert.AreEqual(batv_expected, batv, epsilon)

    End Sub

    <TestMethod> Public Sub batteryDischargeVoltage()
        ' ARRANGE

        Dim epsilon As Double = 0.1
        Dim charge_voltage_expected As Double = 1.2

        ' Battery
        batch = chini / 100
        bati = 0.000001 * cmax_cnom ' To avoid Logarithm Error

        ' ACT
        batv = discharge_voltage(bati, T_battery, batch)

        ' ASSERT 

        Assert.AreEqual(charge_voltage_expected, batv, epsilon)

    End Sub




End Class