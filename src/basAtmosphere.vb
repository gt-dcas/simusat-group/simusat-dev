﻿Module basAtmosphere
    '
    ' Author: Lorenzo Donato
    ' Changed and transfered to SIMUSAT by Wolfgang Hausmann
    ' (26.11.99)
    '
    ' Included functions and procedures:
    '
    ' Public Function temp_exo(dj As Double, alt As Double)
    '    (Calcul de la température exosphérique à partir des bases de données)
    '
    ' Public Function densite(Tx As Double, ha As Double)
    '    (Calcul de la densité atmosphérique)
    '

    Public Function temp_exo(ByVal dj As Double, ByVal lng As Double, ByVal lat As Double, ByVal alt As Double) As Double
        '
        ' -------------------------------------------------------------------
        ' Calcul de la température exosphérique à partir des bases de données
        ' -------------------------------------------------------------------
        '
        Dim tc As Double, tl As Double
        Dim eta As Double, mu As Double
        Dim lat_sol As Double, lng_sol As Double
        Dim flux As Double, kp As Double
        Dim tau As Double
        Dim date_courante As udtDate
        Dim s$ = ""
        '
        '
        ' Données sur l'activité solaire
        ' ------------------------------
        ' Lecture des données de flux solaire à partir du fichier flux_solaire.txt
        ' Provisoirement on ne tiens pas compte de kp qui n'est pas dans le fichier en attendant
        ' plus de détails sur le calcul de la température
        '
        Call convert_Julian_Gregorian(dj, date_courante)
        '
        If date_courante.year < 1991 Or date_courante.year > 2007 Then
            ' Si la date est en dehors de la table : approximation sinusoïdale
            flux = 135 + 65 * Math.Sin((dj - 17712) / 36525 * 2 * pi)
        Else
            '
            ' Sinon on lit la valeur dans le fichier
            Dim i As Integer
            FileOpen(1, System.IO.Path.Combine(My.Application.Info.DirectoryPath, "resources\flux_solaire.txt"), OpenMode.Input)
            For i = 1 To (date_courante.year - 1991) * 12 + date_courante.month
                If EOF(1) Then Exit For
                s$ = LineInput(1)
            Next
            flux = CDbl(Mid$(s$, 9, 13))
            FileClose()
        End If
        '
        ' Latitude et longitude du Soleil
        ' -------------------------------
        With SunPos
            lat_sol = Math.Asin(.Z / Math.Sqrt(.X * .X + .Y * .Y))
            lng_sol = Actan(.Y / Math.Sqrt(.X * .X + .Y * .Y), .X / Math.Sqrt(.X * .X + .Y * .Y))
        End With
        '
        ' Calcul de la température
        ' ------------------------
        Const k As Double = 2.2
        Const beta As Double = -37 * rad
        Const ni As Double = 3
        Const p As Double = 6 * rad
        Const a1 As Double = 0.3
        Const gam As Double = 43 * rad
        '
        ' lng and lat are in degrees they have to be converted in radians
        lat = lat * rad
        lng = lng * rad
        eta = 0.5 * Math.Abs(lat - lat_sol)
        mu = 0.5 * Math.Abs(lat + lat_sol)
        tau = lng - lng_sol + beta + p * Math.Sin(lng * rad - lng_sol + gam)
        '
        tc = 379 + 3.24 * flux   ' + 1.3 * (flux - flux) Vérifier cette formule
        tl = tc * (1 + a1 * (Math.Sin(mu)) ^ k) * (1 + a1 * ((Math.Cos(eta)) ^ k - (Math.Sin(mu)) ^ k) / _
            (1 + a1 * (Math.Sin(mu)) ^ k) * (Math.Cos(tau / 2)) ^ ni)
        '
        If alt < 200 Then
            temp_exo = tl + 14 * kp + 0.02 * Math.Exp(kp)
        Else
            temp_exo = tl + 28 * kp + 0.03 * Math.Exp(kp)
        End If
        ''
    End Function

    Public Function densite(ByVal Tx As Double, ByVal ha As Double) As Double
        '
        ' Atmospheric density calculation
        '
        Dim ro0 As Double, ro1 As Double
        Dim ro11 As Double, ro12 As Double, ro13 As Double
        Dim ro21 As Double, ro22 As Double, ro23 As Double
        Dim h1 As Double, h2 As Double
        Dim s$
        '
        ' Lecture des valeurs du tableau de Jacchia dans le fichier "densité.txt"
        ' -----------------------------------------------------------------------

        FileOpen(1, System.IO.Path.Combine(My.Application.Info.DirectoryPath, "resources\densite.txt"), OpenMode.Input)
        '
        ' Format des lignes
        ' Alt  500°     1000°    2000°
        '" 980 8.83E-16 3.11E-15 1.88E-13"
        '"1000 8.39E-16 2.84E-15 1.64E-13"
        '"1234567890123456789012345678901"
        s$ = LineInput(1)
        h1 = CDbl(Left$(s$, 4))
        ro11 = CDbl(Mid$(s$, 6, 8))
        ro12 = CDbl(Mid$(s$, 15, 8))
        ro13 = CDbl(Mid$(s$, 24, 8))
        '
        densite = 0
        Select Case ha
            Case Is < 100
                densite = 0.00000343
            Case Is >= 2500
                densite = 0
            Case Else
                If Tx <= 1000 Then
                    Do While Not EOF(1)
                        s$ = LineInput(1)
                        h2 = CDbl(Left$(s$, 4))
                        ro21 = CDbl(Mid$(s$, 6, 8))
                        ro22 = CDbl(Mid$(s$, 15, 8))
                        If ha >= h1 And ha <= h2 Then Exit Do
                        h1 = h2 : ro11 = ro21 : ro12 = ro22
                    Loop
                    ro0 = (ro21 - ro11) / (h2 - h1) * (ha - h1) + ro11
                    ro1 = (ro22 - ro12) / (h2 - h1) * (ha - h1) + ro12
                    densite = (ro1 - ro0) * (Tx - 500) / 500 + ro0
                Else
                    Do While Not EOF(1)
                        s$ = LineInput(1)
                        h2 = CDbl(Left$(s$, 4))
                        ro22 = CDbl(Mid$(s$, 15, 8))
                        ro23 = CDbl(Mid$(s$, 24, 8))
                        If ha >= h1 And ha <= h2 Then Exit Do
                        h1 = h2 : ro12 = ro22 : ro13 = ro23
                    Loop
                    ro0 = (ro22 - ro12) / (h2 - h1) * (ha - h1) + ro12
                    ro1 = (ro23 - ro13) / (h2 - h1) * (ha - h1) + ro13
                    densite = (ro1 - ro0) * (Tx - 1000) / 1000 + ro0
                End If
        End Select
        '
        FileClose()
        'XX = densite
        ''
    End Function


End Module
