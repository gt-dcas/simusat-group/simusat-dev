﻿Option Strict Off
Option Explicit On
'Imports Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6
Imports Excel = Microsoft.Office.Interop.Excel

Friend Class frmReportPwr
    Inherits System.Windows.Forms.Form

    Private Sub frmReportPwr_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        Dim i As Integer
        Dim date_temp As udtDate
        ' Initialisation date début par défaut
        '
        convert_Julian_Gregorian(SatOrbit0.epoch, date_temp)
        With date_temp
            txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        '
        ' Initialisation date fin par défaut
        '
        convert_Julian_Gregorian(SatOrbit0.epoch + 1 / 24, date_temp)
        With date_temp
            txt_date_fin.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                    & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        '
        With grdReport
            .ColumnCount = 0
            .Columns.Add("", "Date")
            .Columns.Add("", "Battery current (A)")
            .Columns.Add("", "Battery charge (%)")
            .Columns.Add("", "Battery voltage (V)")
            .Columns.Add("", "Battery power (W)")
            .Columns.Add("", "Panels power (W)")
            .Columns.Add("", "Available power (W)")
            .Columns.Add("", "Excess power (W)")
            .Columns.Add("", "Useful power (W)")
            .Columns.Add("", "Body temperature (°C)")
            '
            ' For each panel

            For i = 1 To SolarPanels.Count()
                If chkPanelsData.Checked Then
                    .Columns.Add("", "P" & i & "_Sun incidence (°)")
                    .Columns.Add("", "P" & i & "_Albedo incidence (°)")
                    .Columns.Add("", "P" & i & "_Solar flux (W/m²)")
                    .Columns.Add("", "P" & i & "_Albedo flux (W/m²)")
                    .Columns.Add("", "P" & i & "_IR flux (W/m²)")
                    .Columns.Add("", "P" & i & "_Total flux (W/m²)")
                    .Columns.Add("", "P" & i & "_Efficient flux (W/m²)")
                    .Columns.Add("", "P" & i & "_Power (W)")
                    .Columns.Add("", "P" & i & "_Temp. (°C)")
                Else
                    .Columns.Add("", "P" & i & "_Power (W)")
                End If
            Next
            '
            ' Mise en forme
            .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            For i = 0 To .ColumnCount - 1
                .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.None
                .Columns(i).Width = 80
            Next
            .Columns(0).Width = 140
            .Columns(0).Frozen = True
        End With


    End Sub

    ''' <summary>
    ''' Define shortcuts Save / Report
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmReportPwr_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyData = (Keys.Control Or Keys.S) Then
            cmdSave_Click(cmdSave, New System.EventArgs)
            e.SuppressKeyPress = True ' Pas besoin que l'évènement KeyPress le gère on vient de le faire
        ElseIf e.KeyData = (Keys.Control Or Keys.Enter) Then
            cmdReport_Click(cmdReport, New System.EventArgs)
            e.SuppressKeyPress = True ' Pas besoin que l'évènement KeyPress le gère on vient de le faire
        End If
    End Sub

    Private Sub frmReportPwr_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        grdReport.Width = Me.Width - 50
        grdReport.Height = Me.Height - 151
    End Sub

    Private Sub cmdReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        '
        Dim CurrentRow As Integer, CurrentCol As Integer
        Dim dblReportStart, dblReportTime As Double, dblReportEnd As Double, dblReportStep As Double
        Dim date_temp As udtDate
        Dim panel As CSolarPanel
        Dim x$
        Dim i As Integer
        '
        ' Datagrid preparation
        With grdReport
            .ColumnCount = 0
            .Columns.Add("", "Date")
            .Columns.Add("", "Battery current (A)")
            .Columns.Add("", "Battery charge (%)")
            .Columns.Add("", "Battery voltage (V)")
            .Columns.Add("", "Battery power (W)")
            .Columns.Add("", "Panels power (W)")
            .Columns.Add("", "Available power (W)")
            .Columns.Add("", "Excess power (W)")
            .Columns.Add("", "Useful power (W)")
            .Columns.Add("", "Body temperature (°C)")
            '
            ' For each panel

            For i = 1 To SolarPanels.Count()
                If chkPanelsData.Checked Then
                    .Columns.Add("", "P" & i & "_Sun incidence (°)")
                    .Columns.Add("", "P" & i & "_Albedo incidence (°)")
                    .Columns.Add("", "P" & i & "_Solar flux (W/m²)")
                    .Columns.Add("", "P" & i & "_Albedo flux (W/m²)")
                    .Columns.Add("", "P" & i & "_IR flux (W/m²)")
                    .Columns.Add("", "P" & i & "_Total flux (W/m²)")
                    .Columns.Add("", "P" & i & "_Efficient flux (W/m²)")
                    .Columns.Add("", "P" & i & "_Power (W)")
                    .Columns.Add("", "P" & i & "_Temp. (°C)")
                Else
                    .Columns.Add("", "P" & i & "_Power (W)")
                End If
            Next
            '
            ' Mise en forme
            .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            For i = 0 To .ColumnCount - 1
                .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.None
                .Columns(i).Width = 80
                .Columns(i).DefaultCellStyle.Format = "#.000"
            Next
            .Columns(0).Width = 140
            .Columns(0).Frozen = True
        End With

        ' Read start date, end date, step
        With date_temp
            .year = Year(txt_date_debut.Value)
            .month = Month(txt_date_debut.Value)
            .day = Microsoft.VisualBasic.DateAndTime.Day(txt_date_debut.Value)
            .hour = Hour(txt_date_debut.Value)
            .minute = Minute(txt_date_debut.Value)
            .second = Second(txt_date_debut.Value)
        End With
        dblReportStart = convert_Gregorian_Julian(date_temp)
        '
        With date_temp
            .year = Year(txt_date_fin.Value)
            .month = Month(txt_date_fin.Value)
            .day = Microsoft.VisualBasic.DateAndTime.Day(txt_date_fin.Value)
            .hour = Hour(txt_date_fin.Value)
            .minute = Minute(txt_date_fin.Value)
            .second = Second(txt_date_fin.Value)
        End With
        '
        dblReportEnd = convert_Gregorian_Julian(date_temp)
        If dblReportEnd < dblReportStart Then
            MsgBox("End date is earlier than start date", MsgBoxStyle.OkOnly, "")
            Exit Sub
        End If
        x$ = updwTimeStep.Text
        x$ = Microsoft.VisualBasic.Left(x$, Len(x$) - 2)
        dblReportStep = CDbl(x$) / secday
        '
        If (dblReportEnd - dblReportStart) / dblReportStep > 6000 Then
            If (MessageBox.Show("You have selected a long analysis which may need much time to compute. Do you want to continue ?", "Long analysis...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No) Then
                Exit Sub
            End If
        End If
        '
        Me.Cursor = Cursors.WaitCursor
        '
        ' Grid initialisation
        grdReport.RowCount = CInt((dblReportEnd - dblReportStart) / dblReportStep + 1)
        dblReportTime = dblReportStart
        '
        ' Power initialisation
        initialize_power()
        '
        ' Calculate and write data in the grid
        CurrentRow = 0

        Do While dblReportTime <= dblReportEnd
            ' 
            ' Orbitography
            update_Sun(dblReportTime, SunPos)
            SatOrbit = Satellite.getOrbit(dblReportTime) ' Gives orbit parameters, SatPos, SatVel, SatGeoPos
            blnEclipse = Eclipse(SatPos, SunPos)
            '
            ' Power simulation
            compute_power(dblReportTime, dblReportStep)
            '
            ' Write data
            convert_Julian_Gregorian(dblReportTime, date_temp)
            With date_temp
                x$ = Format(.day, "00") & "/" & Format(.month, "00") & "/" & Format(.year, "0000") & " "
                x$ += Format(.hour, "00") & ":" & Format(.minute, "00") & ":" & Format(.second, "00.0000") & " "
            End With
            '
            With grdReport
                .Item(0, CurrentRow).Value = x$
                .Item(1, CurrentRow).Value = bati * bat_np
                .Item(2, CurrentRow).Value = batch * 100
                .Item(3, CurrentRow).Value = batv * bat_ns
                .Item(4, CurrentRow).Value = pbat
                .Item(5, CurrentRow).Value = panels_power
                .Item(6, CurrentRow).Value = pexcess + puseful
                .Item(7, CurrentRow).Value = pexcess
                .Item(8, CurrentRow).Value = puseful
                .Item(9, CurrentRow).Value = BodyTemp
                '
                CurrentCol = 10
                If chkPanelsData.Checked Then
                    For i = 1 To SolarPanels.Count()
                        panel = SolarPanels.Item(i)
                        .Item(CurrentCol, CurrentRow).Value = panel.SunIncidence(dblReportTime) * deg           ' Sun incidence (°)")
                        .Item(CurrentCol + 1, CurrentRow).Value = panel.EarthIncidence(dblReportTime) * deg     ' Albedo incidence (°)")
                        .Item(CurrentCol + 2, CurrentRow).Value = panel.PanelSolarFlux                          ' Solar flux (W/m²)")
                        .Item(CurrentCol + 3, CurrentRow).Value = panel.PanelAlbedoFlux                         ' Albedo flux (W/m²)")
                        .Item(CurrentCol + 4, CurrentRow).Value = panel.PanelIRFlux                             ' Infrared flux (W/m²)")
                        .Item(CurrentCol + 5, CurrentRow).Value = panel.PanelIRFlux + panel.PanelAlbedoFlux + panel.PanelSolarFlux  '.Total flux (W/m²)")
                        .Item(CurrentCol + 6, CurrentRow).Value = panel.PanelEffFlux                            ' Efficient flux (W/m²)")
                        .Item(CurrentCol + 7, CurrentRow).Value = panel.PanelPower                              ' Power (W)")
                        .Item(CurrentCol + 8, CurrentRow).Value = panel.PanelTemp                               ' Temperature (°C)")
                        CurrentCol += 9
                    Next
                Else
                    For i = 1 To SolarPanels.Count()
                        panel = SolarPanels.Item(i)
                        .Item(CurrentCol, CurrentRow).Value = panel.PanelPower  ' Power (W)
                        CurrentCol += 1
                    Next
                End If

                If blnEclipse Then .Item(0, CurrentRow).Style.BackColor = Color.LightGray Else .Item(0, CurrentRow).Style.BackColor = Color.LightGoldenrodYellow
            End With
            CurrentRow += 1

            dblReportTime = dblReportTime + dblReportStep
        Loop
        '
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        '
        Dim intFile As Short
        Dim col As Integer, row As Integer
        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet
        Dim i As Integer
        Dim sep As String
        '
        On Error GoTo err_SaveReport
        '
        If grdReport.RowCount < 1 Then Exit Sub ' Nothing to save
        '
        If optText.Checked Then
            dlgReportSave.FileName = "Sans titre"
            dlgReportSave.Filter = "(*.txt)|*.txt|(*.csv)|*.csv"
            If dlgReportSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
            '
            intFile = FreeFile()
            FileOpen(intFile, dlgReportSave.FileName, OpenMode.Output)

            sep = "	"       'ATTENTION!: here, there is a character to allow PrestoPlot@cnes format
            '
            Me.Cursor = Cursors.WaitCursor
            '
            With grdReport
                ' Labels line
                Print(1, "Time" & sep)
                Print(1, "Battery_Current" & sep)
                Print(1, "Battery_Charge" & sep)
                Print(1, "Battery_Voltage" & sep)
                Print(1, "Battery_Power" & sep)
                Print(1, "Panels_Power" & sep)
                Print(1, "Available_Power" & sep)
                Print(1, "Excess_Power" & sep)
                Print(1, "Useful_Power" & sep)
                Print(1, "Body_Temperature" & sep)
                '
                For i = 1 To SolarPanels.Count()
                    If chkPanelsData.Checked Then
                        Print(1, "P" & i & "_Sun_incidence" & sep)
                        Print(1, "P" & i & "_Albedo_incidence" & sep)
                        Print(1, "P" & i & "_Solar_flux" & sep)
                        Print(1, "P" & i & "_Albedo_flux" & sep)
                        Print(1, "P" & i & "_IR_flux" & sep)
                        Print(1, "P" & i & "_Total_flux" & sep)
                        Print(1, "P" & i & "_Efficient_flux" & sep)
                        Print(1, "P" & i & "_Power" & sep)
                        Print(1, "P" & i & "_Temperature")
                    Else
                        Print(1, "P" & i & "_Power" & sep)
                    End If
                    If i <> SolarPanels.Count() Then Print(1, sep)
                Next
                '
                Print(1, vbCrLf)
                ' Units line
                Print(1, "EUR_UTC" & sep)
                Print(1, "A" & sep)
                Print(1, "%" & sep)
                Print(1, "V" & sep)
                Print(1, "W" & sep)
                Print(1, "W" & sep)
                Print(1, "W" & sep)
                Print(1, "W" & sep)
                Print(1, "W" & sep)
                Print(1, "°C" & sep)
                '
                For i = 1 To SolarPanels.Count()
                    If chkPanelsData.Checked Then
                        Print(1, "deg" & sep)
                        Print(1, "deg" & sep)
                        Print(1, "W/m²" & sep)
                        Print(1, "W/m²" & sep)
                        Print(1, "W/m²" & sep)
                        Print(1, "W/m²" & sep)
                        Print(1, "W/m²" & sep)
                        Print(1, "W" & sep)
                        Print(1, "°C")
                    Else
                        Print(1, "W" & sep)
                    End If
                    If i = SolarPanels.Count() Then Print(1, vbCrLf) Else Print(1, sep)
                Next
                '
                Print(1, vbCrLf)
                ' Data
                For row = 0 To .RowCount - 1
                    Print(1, .Item(0, row).Value & sep)
                    For col = 1 To .ColumnCount - 1
                        Print(1, Format(.Item(col, row).Value, "#0.000000"))
                        If col = .ColumnCount - 1 Then Print(1, vbCrLf) Else Print(1, sep)
                    Next
                Next
                '
            End With
            '
            FileClose(intFile)
            '
        Else
            dlgReportSave.FileName = "Sans titre"
            dlgReportSave.Filter = "Excel Worksheets (*.xlsx)|*.xlsx|Excel Worksheets 97-2003 (*.xls)|*.xls"
            If dlgReportSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
            '
            xlApp = New Excel.Application
            xlWorkBook = xlApp.Workbooks.Add()
            xlWorkSheet = xlWorkBook.Sheets.Add(, , 1, )
            xlWorkSheet.Name = "Simusat power data"
            '
            Me.Cursor = Cursors.WaitCursor
            '
            ' Fill data
            With grdReport
                ' Labels line
                For col = 0 To .ColumnCount - 1
                    xlWorkSheet.Cells(1, col + 1) = .Columns(col).HeaderText
                Next
                ' Data
                For row = 0 To .RowCount - 1
                    xlWorkSheet.Cells(row + 2, 1) = .Item(0, row).Value
                    For col = 1 To .ColumnCount - 1
                        'xlWorkSheet.Cells(row + 2, col + 1) = Format(.Item(col, row).Value, "#0.000")
                        xlWorkSheet.Cells(row + 2, col + 1) = .Item(col, row).Value
                    Next
                Next
            End With
            '
            ' Formatting
            With xlWorkSheet
                With .Range(.Cells(1, 1), .Cells(1, grdReport.ColumnCount))
                    .EntireColumn.AutoFit()
                    .Interior.Color = Color.Cyan
                    .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                End With
                With .Range(.Cells(2, 2), .Cells(grdReport.RowCount + 1, grdReport.ColumnCount))
                    '.NumberFormat = "#0.00"
                    .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                End With
                With .Range(.Cells(2, 1), .Cells(grdReport.RowCount + 1, 1))
                    .Interior.Color = RGB(255, 255, 192)
                    '.NumberFormat = "jj/mm/aaaa hh:mm:ss"
                End With
            End With
            '
            xlWorkBook.SaveAs(dlgReportSave.FileName)
            xlApp.Quit()
            '
        End If
        '
        Me.Cursor = Cursors.Default
        '
        Exit Sub
err_SaveReport:
        MsgBox(Err.Description)
        MsgBox("File not saved")
        Me.Cursor = Cursors.Default
        '
    End Sub

    Private Sub cmdClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        '
        Me.Hide()
        System.Windows.Forms.Application.DoEvents()
        '
    End Sub

End Class