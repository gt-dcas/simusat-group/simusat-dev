﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class IntegrationTest

    'Giving a time for simulation time and a timestep
    Dim cells_surface As Double
    Dim panel As CSolarPanel
    Dim epsilon As Double = 0.08
    Dim time As Double = 2451955.50694          ' January 1st 2000 at 0h + 400
    Dim timestep As Double = 10
    Dim PanelNbString As Single = 1
    Dim TempPanel As New CSolarPanel

    ''' <summary>
    ''' To run before every test, data taken from BE
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    <TestInitialize()> Public Sub satelliteInitialization()
        '
        ' Creating new collection of solar panels
        SolarPanels = New Collection
        '
        '
        SunPow = 3.8E+26
        load_thermal_models()
        update_Sun(time, SunPos)
        blnEclipse = Eclipse(SatPos, SunPos)
        '
        '
        ' ----------------------------------
        ' Default values (Energy Sub-System)
        ' ----------------------------------

        ' Thermical Data
        TempPanel.PanelSpecHeat = 100000
        TempPanel.PanelCellAlf = 0.5
        TempPanel.PanelCellEps = 0.5
        TempPanel.PanelBackAlf = 0.5
        TempPanel.PanelBackEps = 0.5
        TempPanel.PanelConduction = 5
        '
        ' Cells performance
        cell_type = 0
        cell_isc = 0.53
        cell_imp = 0.5
        cell_voc = 1
        cell_vmp = 0.86
        cell_disc = 0.0003
        cell_dimp = 0.00025
        cell_dvoc = -0.002
        cell_dvmp = -0.002
        cell_radisc = 1
        cell_radimp = 1
        cell_radvoc = 1
        cell_radvmp = 1
        cell_ns = 15          ' Value added for EPSUnitTest
        cell_surf = 16
        cell_tref = 28
        cell_tetad = 50
        cell_tetam = 75
        cell_eff = 0.18

        '
        ' Operation parameters and distribution system performance
        pdem(0) = 50    ' 0 Sunshine 1 Eclipse
        diss(0) = 15
        pdem(1) = 50
        diss(1) = 15
        rendgs = 90
        rendbat = 90
        '
        ' Variables related to batteries system with default values
        gst_type = 0         ' Value added for EPSUnitTest
        cnom = 10
        bat_ns = 10
        bat_np = 1
        bat_type = 0
        T_battery = 15       ' Value added for EPSUnitTest
        T_init_panels = 25   ' Value added for EPSUnitTest
        T_init_body = 25     ' Value added for EPSUnitTest
        '
        ' Operation parameters
        chvmax = 1.5
        ichmax = 100
        kcharge = 1.05
        chini = 100
        bat_courant_entretien = 0.001

        '
        ' Charge behaviour parameters
        ch0(0) = 0 : ch0(1) = 0 : ch0(2) = 0 : ch0(3) = 1.3 : ch0(4) = 0 : ch0(5) = 0
        ch1(0) = 15 : ch1(1) = 0.1 : ch1(2) = -5 : ch1(3) = 1.4 : ch1(4) = -0.001 : ch1(5) = 0.01
        ch2(0) = 65 : ch2(1) = 0.5 : ch2(2) = -5 : ch2(3) = 1.41 : ch2(4) = -0.001 : ch2(5) = 0.01
        och(0) = 120 : och(1) = 0.1 : och(2) = -5 : och(3) = 1.59 : och(4) = -0.002 : och(5) = 0.02
        '
        ' Discharge behaviour parameters
        de0(0) = 0 : de0(1) = 0 : de0(2) = 0 : de0(3) = 1.3 : de0(4) = 0 : de0(5) = 0
        de1(0) = 15 : de1(1) = 0.1 : de1(2) = -5 : de1(3) = 1.21 : de1(4) = 0.001 : de1(5) = -0.01
        de2(0) = 95 : de2(1) = 0.1 : de2(2) = -2 : de2(3) = 1.2 : de2(4) = 0.001 : de2(5) = -0.01
        de100(0) = 100 : de100(1) = 0 : de100(2) = 0 : de100(3) = 1.0# : de100(4) = 0 : de100(5) = 0
        cmax_cnom = 1.2
        c10_cnom = 0.6
        tbatmin = -50
        cmin100 = 30
        tbat99 = 50
        '
        ' Self-discharge behaviour parameters
        tc0 = 100
        tc50 = 15
    End Sub

    ''' <summary>
    ''' Creating satellite, it adds a satellite type
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub sat_type(sat As String)

        Select Case sat
            Case "SAT1"
                Satellite = New CSatellite
                Call Satellite.construct("New_Sat", 65535, 0, 0, True)
                With Satellite
                    .satBodyHeight = 0.8
                    .satBodyWidth = 0.8
                    .primaryAxis = "z"
                    .primaryDir = "inertial z"
                    .secondaryAxis = "x"
                    .secondaryDir = "inertial x"
                End With

            Case "SAT2"
                Satellite = New CSatellite
                Call Satellite.construct("SENTINEL-1A", 65535, 0, 0, True)
                With Satellite
                    .satBodyHeight = 0.8
                    .satBodyWidth = 0.8
                    .primaryAxis = "-z"
                    .primaryDir = "nadir"
                    .secondaryAxis = "x"
                    .secondaryDir = "velocity"
                End With

            Case "SAT3"
                Satellite = New CSatellite
                Call Satellite.construct("SENTINEL-5P", 65535, 0, 0, True)
                With Satellite
                    .satBodyHeight = 1.6
                    .satBodyWidth = 1.2
                    .primaryAxis = "x"
                    .primaryDir = "nadir"
                    .secondaryAxis = "y"
                    .secondaryDir = "velocity"
                End With

        End Select


    End Sub

    ''' <summary>
    ''' Creating orbit, it adds a orbit type
    ''' Sun synchronus --> each paramters is set to get the satellite passes around the sun
    ''' Geostationary --> each paramters is set to get the satellite in a geostationary orbit
    ''' Circular --> each paramters is set to get the satellite in a circular orbit
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub orbit_type(orbit As String)


        Select Case orbit
            Case "Orbit 1"

                With SatOrbit0
                    .Name = "Sun synchronous 800"
                    .SemiMajor_Axis = 7198.137
                    .Eccentricity = 0
                    .Inclination = 30
                    .Argument_Perigee = 0
                    .RAAN = 0
                    .Mean_Anomaly = 0
                    .epoch = 2451544.5       ' January 1st 2000 at 0h
                    .dec1 = 0
                    .dec2 = 0
                    .bstar = 0
                End With
                SatOrbit = SatOrbit0

            Case "Orbit 2"
                With SatOrbit0
                    .Name = "Geostationary"
                    .SemiMajor_Axis = 42164
                    .Eccentricity = 0
                    .Inclination = 0
                    .Argument_Perigee = 0
                    .RAAN = 0
                    .Mean_Anomaly = 40
                    .epoch = 2451544.5       ' January 1st 2000 at 0h
                    .dec1 = 0
                    .dec2 = 0
                    .bstar = 0
                End With
                SatOrbit = SatOrbit0

            Case "Orbit 3"
                With SatOrbit0
                    .Name = "Circular"
                    .SemiMajor_Axis = 7378.137
                    .Eccentricity = 0
                    .Inclination = 0
                    .Argument_Perigee = 0
                    .RAAN = 0
                    .Mean_Anomaly = 0
                    .epoch = 2451544.5       ' January 1st 2000 at 0h
                    .dec1 = 0
                    .dec2 = 0
                    .bstar = 0
                End With
                SatOrbit = SatOrbit0

            Case "Orbit 4"
                With SatOrbit0
                    .Name = "Orbit SENTINEL-5P"
                    .SemiMajor_Axis = 7204.95
                    .Eccentricity = 0.000109
                    .Inclination = 98.7317
                    .Argument_Perigee = 8108726
                    .RAAN = 97.7403
                    .Mean_Anomaly = 278.2573
                    .epoch = 2451544.5       ' January 1st 2000 at 0h
                    .dec1 = 0
                    .dec2 = 0
                    .bstar = 0.000028
                End With
                SatOrbit = SatOrbit0

        End Select

    End Sub


    ''' <summary>
    ''' Add an orientable panel (Type 1)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y" or "- Y"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type1(ByRef length As Single, ByRef width As Single, ByRef mass As Single, ByRef face As String)
        '        
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type1"        ' Orientable Panel
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub

    ''' <summary>
    ''' Add a panel on a face (Type 2)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y","- Y", "+ Z" or "- Z"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type2(ByRef length As Single, ByRef width As Single, ByRef mass As Single, ByRef face As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type2"        ' Panel on a face
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub

    ''' <summary>
    ''' Add a panel fixed perpendicular to a face (Type 3)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y" or "- Y"</param>
    ''' <param name="dir">Direction : "+ Z" or "- Z"</param>
    ''' <param name="hpos">Position : "Upper", "Middle" or "Lower"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type3(ByRef length As Single, ByRef width As Single, ByRef mass As Single,
                                ByRef face As String, ByRef dir As String, ByRef hpos As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type3"        ' Panel fixed perpendicular to a face
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelDir = dir
            .PanelHpos = hpos
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & " : " & .PanelDir & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub


    Private Sub add_panel1_randomly()

        add_panel_type1(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(1))

    End Sub

    Private Sub add_panel2_randomly()

        add_panel_type2(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(2))

    End Sub


    Private Sub add_panel_randomly()

        add_panel_type3(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(3), select_random_orientation(), select_random_position())

    End Sub



    Private Function select_random_face(ByRef type As Integer) As String

        Select Case type
            Case 1
                Dim faces1() As String = {"+ X", "+ Y", "- X", "- Y"}
                Return faces1(Int(4 * Rnd()) + 0)
            Case 2
                Dim faces2() As String = {"+ X", "+ Y", "+ Z", "- X", "- Y", "- Z"}
                Return faces2(Int(6 * Rnd()) + 0)
            Case 3
                Dim faces3() As String = {"+ X", "+ Y", "- X", "- Y"}
                Return faces3(Int(4 * Rnd()) + 0)
        End Select

        Return "faces" & CStr(type)(Int(4 * Rnd()) + 0)

    End Function


    Private Function select_random_orientation() As String

        Dim orientation() As String = {"+ Z", "- Z"}
        Return orientation(Int(2 * Rnd()) + 0)

    End Function



    Private Function select_random_position() As String

        Dim orientation() As String = {"Upper", "Middle", "Lower"}
        Return orientation(Int(3 * Rnd()) + 0)

    End Function

    Private Sub panel_type(panelType As String)
        Select Case panelType

            Case "panel_type1"

                add_panel_type1(0.8, 0.8, 1, "+ X")
                add_panel_type1(0.8, 0.8, 1, "- X")
                add_panel_type1(0.8, 0.8, 1, "+ Y")
                add_panel_type1(0.8, 0.8, 1, "- Y")

            Case "panel_type2"

                add_panel_type2(0.8, 0.8, 1, "+ X")
                add_panel_type2(0.8, 0.8, 1, "- X")
                add_panel_type2(0.8, 0.8, 1, "+ Y")
                add_panel_type2(0.8, 0.8, 1, "- Y")
                add_panel_type2(0.8, 0.8, 1, "+ Z")
                add_panel_type2(0.8, 0.8, 1, "- Z")

            Case "panel_type3"

                add_panel_type3(0.8, 0.8, 1, "+ X", "- Z", "Lower")
                add_panel_type3(0.8, 0.8, 1, "- X", "- Z", "Lower")
                add_panel_type3(0.8, 0.8, 1, "+ Y", "- Z", "Lower")
                add_panel_type3(0.8, 0.8, 1, "- Y", "- Z", "Lower")

        End Select
    End Sub

    Private Sub runSimulation()
        Dim update As New frmMain
        Dim date_temp As udtDate

        ' January 1st 2000 at 0h 
        With date_temp
            .day = 1
            .month = 1
            .year = 2000
            .hour = 0
            .minute = 0
            .second = 0
        End With

        update.Sim_update()
        dblSimulationStart = SatOrbit.epoch
        dblSimulationTime = dblSimulationStart
        convert_Julian_Gregorian(dblSimulationStart, date_temp)
        update.simulationloop()

        If simloop = True Then
            '86400 = 24H
            While dblSimulationTime > (dblSimulationTime + 86400)
                simloop = False
                update.Sim_update()
                update.simulationloop()
                update.Sim_update()
            End While
        End If

    End Sub
    Private Function CsvVal(ByVal table As Array, ByVal i As Integer, ByVal j As Integer) As Double
        Dim line As String = table(i)
        Dim lineArray() As String = Split(line, ",")
        CsvVal = Val(lineArray(j))
    End Function

    ''' <summary>
    ''' Test the calculation of the new mass of the whole satellite
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()> Public Sub sat_global_mass_test()

        ' SETTING
        BodyMass = 20
        GravityMass = 10

        'add_panel1_randomly()
        'add_panel2_randomly()

        add_panel_type1(1, 1, 5, "+ Z")
        add_panel_type2(1, 1, 5, "+ Z")
        add_panel_type3(1, 1, 0.5, "- Y", "+ Z", "Lower")


        ' ARRANGE
        Dim PanelMass As Double
        Dim i As Integer
        Dim mass_expected As Double = BodyMass + GravityMass + DampersMass(0) + DampersMass(1) + DampersMass(2)
        For i = 1 To PanelsNumber
            With SolarPanels.Item(i)
                .PanelMass = 3
                PanelMass = CDbl(.PanelMass)
            End With
            mass_expected += PanelMass
        Next

        ' ACT
        frmAttConf.update_attitude_data_fortest()

        ' ASSERT
        Assert.AreEqual(mass_expected, SatMass)


    End Sub
    ''' <summary>
    ''' Test the calculation of the different scenarios and the panels power
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()> Public Sub sat_orbit_scenarios_test()

        ' ARRANGE
        Dim update As New frmMain
        Dim numberOfSatellite As Integer = 3
        Dim numberOfOrbitType As Integer = 4
        Dim numberOfPanelType As Integer = 3
        Dim time As Double = 2451544.5          ' January 1st 2000 at 0h 
        Dim timestep As Double = 10             ' Attention!. Timestep has an influence on subsequent calculations

        ' Values to make the simulation comparation
        Dim simu1Power As Object = {16.091, 7.404, 6.678}
        Dim simu2Power As Object = {12.062, 6.188, 0.823}
        Dim simu3Power As Object = {11.96, 6.188, 0.0}

        ' ACT
        For j As Integer = 1 To numberOfSatellite

            Select Case j
                Case j

                    ' Select the satellite type
                    Dim strSat As String = "SAT" & CStr(j)
                    sat_type(strSat)

                    For i As Integer = 1 To numberOfOrbitType
                        Select Case i

                            Case i
                                ' Select the orbit type
                                Dim strOrbit As String = "Orbit " & CStr(i)
                                orbit_type(strOrbit)

                                For m As Integer = 1 To numberOfPanelType
                                    Select Case m
                                        Case m

                                            Dim simuTime As Double
                                            Dim panel_power As Double = 0
                                            Dim power_actual As Double = 0
                                            Dim power_expected As Double
                                            Dim epsilon As Double = 0.9

                                            ' Select the panel type
                                            Dim strPanel As String = "panel_type" & CStr(m)
                                            panel_type(strPanel)

                                            ' Battery
                                            batch = chini / 100
                                            bati = 0.000001 * cmax_cnom ' To avoid Logarithm Error
                                            batv = charge_voltage(bati, T_battery, batch)


                                            simuTime = SatOrbit0.epoch
                                            update_Sun(simuTime, SunPos)
                                            blnEclipse = Eclipse(SatPos, SunPos)

                                            If strSat = "SAT1" Then
                                                power_expected = simu1Power(m - 1)
                                            ElseIf strSat = "SAT2" Then
                                                power_expected = simu2Power(m - 1)
                                            Else
                                                power_expected = simu3Power(m - 1)
                                            End If

                                            ' Panels temperature, surface and power
                                            For l As Integer = 1 To SolarPanels.Count()
                                                panel = SolarPanels.Item(l)
                                                cells_surface = cell_ns * PanelNbString * cell_surf / 10000 ' Total surface of cells for the panel
                                                panel.PanelPower = flux_incident_overall_power(panel, simuTime) * cell_efficiency(panel.PanelTemp) * cells_surface
                                                panel_power += panel.PanelPower
                                            Next

                                            ' ACT
                                            power_actual += panel_power
                                            Call flux_calculation(simuTime)
                                            Call compute_thermal(simuTime, timestep)

                                            ' ASSERT
                                            Assert.AreEqual(power_expected, power_actual, epsilon)


                                    End Select

                                    ' Update the solar panel and EPS configuration
                                    SolarPanels.Clear()
                                    initialize_power()

                                Next

                        End Select

                    Next

            End Select
        Next

    End Sub
    ''' <summary>
    ''' Test that allows start the time simulation and allows different scenarios to be run
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()> Public Sub simulation_time_scenarios()

        ' ARRANGE
        Dim update As New frmMain
        Dim numberOfSatellite As Integer = 3
        Dim numberOfOrbitType As Integer = 4
        Dim numberOfPanelType As Integer = 3
        'Dim panel1 As CSolarPanel


        ' ACT
        For j As Integer = 1 To numberOfSatellite
            Select Case j

                Case j
                    ' Select the satellite type
                    Dim strSat As String = "SAT" & CStr(j)
                    sat_type(strSat)
                    update.Sim_update()

                    For i As Integer = 1 To numberOfOrbitType
                        Select Case i

                            Case i
                                ' Select the orbit type
                                Dim strOrbit As String = "Orbit " & CStr(i)
                                orbit_type(strOrbit)

                                For m As Integer = 2 To numberOfPanelType
                                    Select Case m

                                        Case m
                                            Dim simuTime As Double
                                            Dim simuDuration As Integer = 2   'simulation time 

                                            ' Select the panel type
                                            Dim strPanel As String = "panel_type" & CStr(m)
                                            panel_type(strPanel)

                                            ' ASSERT
                                            For k As Integer = 1 To simuDuration

                                                simuTime = SatOrbit.epoch + k
                                                SatOrbit = Satellite.getOrbit(simuTime)
                                                runSimulation()

                                            Next

                                    End Select

                                Next

                        End Select

                    Next

            End Select

        Next

    End Sub
    ''' <summary>
    ''' Integration test that compares a full report with a CSV file with a know right report
    ''' Integration test. We will test if the program finds always the same solution.
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod> Public Sub Report_test()

        ' ARRANGE
        epsilon = 0.5
        Dim time As Double
        Dim timestep As Double
        Dim i_time As Integer = 0
        Dim i_column As Integer = 1  'first column is date and time
        Dim i_row As Integer = 3     'first row with data
        Dim panel As CSolarPanel


        ' Creating satellite
        Satellite = New CSatellite
        Call Satellite.construct("New_Sat", 65535, 0, 0, True)
        With Satellite
            .satBodyHeight = 0.8
            .satBodyWidth = 0.8
            .primaryAxis = "-z"
            .primaryDir = "nadir"
            .secondaryAxis = "x"
            .secondaryDir = "velocity"
        End With

        ' Creating orbit
        With SatOrbit0
            .Name = "Circular 820"
            .SemiMajor_Axis = 8000
            .Eccentricity = 0
            .Inclination = 0
            .Argument_Perigee = 0
            .RAAN = 0
            .Mean_Anomaly = 0
            .epoch = 2457754.5          'January 1st 2017 at 0h
            .dec1 = 0
            .dec2 = 0
            .bstar = 0
        End With
        SatOrbit = SatOrbit0

        Alb_Cer_bln = True
        EarthIR_Cer_bln = True

        'This line avoids an exception when opening a new file. The exception is due to the If sentence in the load_att_and_pwr_conf function to handle retrocompatibility.
        'The exception ocurred when a ancient .att file was opened before
        frmMain.dlgMainOpen.FilterIndex = 1

        load_test_files()
        load_att_and_pwr_conf(My.Application.Info.DirectoryPath & TestSat)
        timestep = 60 / secday
        time = SatOrbit0.epoch

        initialize_power()

        'Starting the CSV
        Dim table() As String = System.IO.File.ReadAllLines(My.Application.Info.DirectoryPath & TestReport) 'ligne 0:header, 

        For i_time = 0 To 120

            ' ACT

            ' Orbitography
            update_Sun(time, SunPos)
            SatOrbit = Satellite.getOrbit(time)       ' Gives orbit parameters, SatPos, SatVel, SatGeoPos
            blnEclipse = Eclipse(SatPos, SunPos)

            'Power simulation
            compute_power(time, timestep)

            ' ASSERT
            Assert.AreEqual(bati * bat_np, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
            Assert.AreEqual(batch * 100, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
            Assert.AreEqual(batv * bat_ns, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
            Assert.AreEqual(pbat, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
            Assert.AreEqual(panels_power, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
            Assert.AreEqual(pexcess + puseful, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
            Assert.AreEqual(pexcess, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
            Assert.AreEqual(puseful, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
            Assert.AreEqual(CsvVal(table, i_row, i_column), BodyTemp, epsilon) : i_column += 1
            For j As Integer = 1 To SolarPanels.Count()
                panel = SolarPanels.Item(j)
                Assert.AreEqual(panel.SunIncidence(time) * deg, CsvVal(table, i_row, i_column), epsilon) : i_column += 1 ' Sun incidence (°)")
                Assert.AreEqual(panel.EarthIncidence(time) * deg, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
                Assert.AreEqual(panel.PanelSolarFlux, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
                Assert.AreEqual(panel.PanelAlbedoFlux, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
                Assert.AreEqual(panel.PanelIRFlux, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
                Assert.AreEqual(panel.PanelIRFlux + panel.PanelAlbedoFlux + panel.PanelSolarFlux, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
                Assert.AreEqual(panel.PanelEffFlux, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
                Assert.AreEqual(panel.PanelPower, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
                Assert.AreEqual(panel.PanelTemp, CsvVal(table, i_row, i_column), epsilon) : i_column += 1
            Next

            time += timestep
            i_row += 1
            i_column = 1
        Next

    End Sub
    ''' <summary>
    ''' To run at the end of every test
    ''' </summary>
    ''' <remarks></remarks>
    <TestCleanup()> Public Sub fin()
        '
        SolarPanels.Clear()  'Eliminate every solar panel
        '
    End Sub


End Class

