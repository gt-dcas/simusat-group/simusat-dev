-----------------------------------------
SIMUSAT: ATTITUDE CONTROL, VERSION VB.NET
-----------------------------------------


Auteur : Melvin ALTRACH, avril-juillet 2017
--------


Etat du projet : En cours de developpement
----------------


Description : Logiciel de simulation satellite (contr�le d'attitude, etude des couples perturbateurs, etc.)
-------------


Documentation : ../DOC/...
---------------
> IGRFcoefficients.xls : coefficients du mod�le IGRF de champ magn�tique terrestre utilis�
> Installation_bibliotheque.pdf : mode d'emploi pour installer les biblioth�ques n�cessaires
> Notice.pdf : mode d'emploi pour tracer les courbes selon le BE
> Presentation_des_courbes.pdf : trac� des couples perturbateurs et comparaison avec la version VB6 et Orekit (Java)
> Video_demonstration_Simusat.avi : d�monstration de ce qui est possible avec le logiciel


Sauvegardes "types" : ../DemoFiles/...
---------------------
> configBE.att : sauvegarde du satellite tel qu'il doit �tre construit dans le BE


Fichiers de configuration : ../bin/...
---------------------------
> blank_file.att : fichier "vierge" utilis� lorsque l'on souhaite cr�er un nouveau fichier ("New")
> data_range.txt : (modifiable) valeurs minimales et maximales admises pour chaque grandeur
> default_values.txt : (modifiable) valeurs par d�faut pour chaque grandeur
> densite.txt : valeurs de densit� atmosph�rique de 100 � 2500 m
> flux_solaire.txt : valeurs de flux solaire de 1991 � 2007


Installation de Visual Studio :
-------------------------------
> T�l�charger "Microsoft Visual Studio Express 2013 pour Windows Desktop"
> Ajouter les biblioth�ques n�cessaires (voir : ../DOC/Installation_bibliotheque.pdf)
> Si la biblioth�que Microsoft Chart Controls n'apparait pas, l'installer gr�ce � ../DOC/Biblio/MSChart-Setup.exe
> Si les biblioth�que TaoFramework n'apparaissent pas, les installer gr�ce � ../DOC/Biblio/TaoFramework-2.1.0-Setup.exe

===================
INSTALL
===================
dependencies/taoframework-2.1.0-setup.exe
dependencies/MSChart.exe

=======================
RUN
=======================
bin/Simusat



===========================
Wiki
==========================
h1. git usage:

# get repository
git clone https://openforge.isae.fr/git/simusat

#update
cd simusat
git pull origin master

#change branch from master to versionChristianVBNet
git co versionChristianVBNet



h1. branches: 

4 branches "vivantes" : 
*  master -> Version VBNet "officielle" d�ploy�e tr�s prochainement les postes, avec le SCAO (en test)
* VB6 -> Version VB actuellement d�ploy�e (V03) sur les postes, avec le SCAO (git co ec22976)
*  simusatDeployedBefore2017 -> Version VB initiale, base de celle d�ploy�e sur les postes, avec le SCAO
*  versionChristianVBNet -> version VBNet de christian, avec un d�but de SCAO
*  versionVBNet -> version VBNet "officielle", sans le SCAO
SCAO

2 branches suppl�mentaires:
* stage de Melvin (2017)
* stage de Robin (2017)

en cas de doute, 
cat versionAuto.txt


==============================
TODO
==============================
 - add somewhere information about loaded file .att
 - solve the update of Body Heat Capacity Issue
 - solve the update of Heat Capacity per solar pannel Issue
 - remove hard coded values
 - treat case of infinite temperatures
 - save orbital configuration

