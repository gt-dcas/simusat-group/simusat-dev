﻿Imports System.Globalization
Imports System.IO
Imports System.Collections.Generic
Imports Newtonsoft.Json
'Imports YamlParserTest


Module Utilitaires


    Dim forceDotCulture As CultureInfo

    Public Sub ForceDotForCurrentApplication()

        forceDotCulture = Application.CurrentCulture.Clone()
        forceDotCulture.NumberFormat.NumberDecimalSeparator = "."
        Application.CurrentCulture = forceDotCulture

    End Sub

    ' Checking functions
    ' ------------------

    ''' <summary>
    ''' (for general window) Keeps the value within a valid range and send a warning message if the value is out of range
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="min"></param>
    ''' <param name="max"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function value_limiter_gen(ByVal value As Double, ByVal min As Double, ByVal max As Double) As Double
        '
        If value > max Then
            value = max
            'frmAttConf.mass_lblmsg.Text = "WARNING: the value must be between " + min.ToString + " and " + max.ToString
        ElseIf value < min Then
            value = min
            'frmAttConf.mass_lblmsg.Text = "WARNING: the value must be between " + min.ToString + " and " + max.ToString
        Else
            'frmAttConf.mass_lblmsg.Text = ""
        End If
        value_limiter_gen = value
        ''
    End Function

    ''' <summary>
    ''' (for frmAttConf) Keeps the value within a valid range and send a warning message if the value is out of range
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="min"></param>
    ''' <param name="max"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function value_limiter(ByVal value As Double, ByVal min As Double, ByVal max As Double) As Double
        '
        If value > max Then
            value = max
            frmAttConf.mass_lblmsg.Text = "WARNING: the value must be between " + min.ToString + " and " + max.ToString
        ElseIf value < min Then
            value = min
            frmAttConf.mass_lblmsg.Text = "WARNING: the value must be between " + min.ToString + " and " + max.ToString
        Else
            frmAttConf.mass_lblmsg.Text = ""
        End If
        value_limiter = value
        ''
    End Function

    ''' <summary>
    ''' (for frmOrbit) Keeps the value within a valid range and send a warning message if the value is out of range
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="min"></param>
    ''' <param name="max"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function value_limiter2(ByVal value As Double, ByVal min As Double, ByVal max As Double) As Double
        '
        If value > max Then
            value = max
            frmOrbit.orbit_lblmsg.Text = "WARNING: the value must be between " + min.ToString + " and " + max.ToString
        ElseIf value < min Then
            value = min
            frmOrbit.orbit_lblmsg.Text = "WARNING: the value must be between " + min.ToString + " and " + max.ToString
        Else
            frmOrbit.orbit_lblmsg.Text = ""
        End If
        value_limiter2 = value
        ''
    End Function

    ''' <summary>
    ''' (for frmPowerConf) Keeps the value within a valid range and send a warning message if the value is out of range
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="min"></param>
    ''' <param name="max"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function value_limiter3(ByVal value As Double, ByVal min As Double, ByVal max As Double) As Double
        '
        If value > max Then
            value = max
            frmPowerConf.pwr_lblmsg.Text = "WARNING: the value must be between " + min.ToString + " and " + max.ToString
        ElseIf value < min Then
            value = min
            frmPowerConf.pwr_lblmsg.Text = "WARNING: the value must be between " + min.ToString + " and " + max.ToString
        Else
            frmPowerConf.pwr_lblmsg.Text = ""
        End If
        value_limiter3 = value
        ''
    End Function

    ''' <summary>
    ''' (for frmOrbit) Keeps the value above a lower limit and send a warning message if the value is out of range
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="min"></param>
    ''' <param name="equal"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function value_down2(ByVal value As Double, ByVal min As Double, ByVal equal As Boolean) As Double
        '
        If equal AndAlso value < min Then
            value = min
            frmOrbit.orbit_lblmsg.Text = "WARNING: the value must be above " + min.ToString
        ElseIf (Not equal) AndAlso value <= min Then
            value = min + 0.0001
            frmOrbit.orbit_lblmsg.Text = "WARNING: the value must be strictly above " + min.ToString
        Else
            frmOrbit.orbit_lblmsg.Text = ""
        End If
        value_down2 = value
        ''
    End Function

    ''' <summary>
    ''' (for frmPowerConf) Keeps the value above a lower limit and send a warning message if the value is out of range
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="min"></param>
    ''' <param name="equal"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function value_down3(ByVal value As Double, ByVal min As Double, ByVal equal As Boolean) As Double
        '
        If equal AndAlso value < min Then
            value = min
            frmPowerConf.pwr_lblmsg.Text = "WARNING: the value must be above " + min.ToString
        ElseIf (Not equal) AndAlso value <= min Then
            value = min + 0.0001
            frmPowerConf.pwr_lblmsg.Text = "WARNING: the value must be strictly above " + min.ToString
        Else
            frmPowerConf.pwr_lblmsg.Text = ""
        End If
        value_down3 = value
        ''
    End Function

    ''' <summary>
    ''' (for frmOrbit) Keeps the value below an upper limit and send a warning message if the value is out of range
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="max"></param>
    ''' <param name="equal"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function value_up2(ByVal value As Double, ByVal max As Double, ByVal equal As Boolean) As Double
        '
        If equal AndAlso value > max Then
            value = max
            frmOrbit.orbit_lblmsg.Text = "WARNING: the value must be below " + max.ToString
        ElseIf (Not equal) AndAlso value >= max Then
            value = max - 0.0001
            frmOrbit.orbit_lblmsg.Text = "WARNING: the value must be strictly below " + max.ToString
        Else
            frmOrbit.orbit_lblmsg.Text = ""
        End If
        value_up2 = value
        ''
    End Function

    ''' <summary>
    ''' (for frmPowerConf) Keeps the value below an upper limit and send a warning message if the value is out of range
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="max"></param>
    ''' <param name="equal"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function value_up3(ByVal value As Double, ByVal max As Double, ByVal equal As Boolean) As Double
        '
        If equal AndAlso value > max Then
            value = max
            frmPowerConf.pwr_lblmsg.Text = "WARNING: the value must be below " + max.ToString
        ElseIf (Not equal) AndAlso value >= max Then
            value = max - 0.0001
            frmPowerConf.pwr_lblmsg.Text = "WARNING: the value must be strictly below " + max.ToString
        Else
            frmPowerConf.pwr_lblmsg.Text = ""
        End If
        value_up3 = value
        ''
    End Function

    ' Saving and loading functions
    ' ----------------------------

    Public Function read_line() As String
        '
        ' Lit une ligne d'un fichier
        '
        Dim X$ = ""
        '
        X$ = LineInput(1)
        If Left$(X$, 1) <> "!" Then
            read_line = Left$(X$, 20)
        Else
            read_line = ""
        End If
        ''
    End Function

    Public Function read_whole_line() As String
        '
        ' Lit une ligne d'un fichier (sans limitation de taille)
        '
        Dim X$ = ""
        '
        X$ = LineInput(1)
        If Left$(X$, 1) <> "!" Then
            read_whole_line = X
        Else
            read_whole_line = ""
        End If
        ''
    End Function

    Public Sub load_att_and_pwr_conf(ByVal file As String)
        Console.WriteLine("Loading file: " + file.ToString)
        '
        Dim i As Integer
        'Dim xs As String
        Dim s$ = ""
        '
        On Error Resume Next
        FileOpen(1, file, OpenMode.Input)
        On Error GoTo 0
        '
        s$ = (read_line())                              ' "! ---------"
        s$ = (read_line())                              ' "! Satellite"
        s$ = (read_line())                              ' "! ---------"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! Central body geometry"
        s$ = (read_line())                              ' "! ---------------------"
        BodyMass = Val((read_line()))                   ' "Body mass"
        Satellite.satBodyHeight = Val((read_line()))    ' "Body height"
        Satellite.satBodyWidth = Val((read_line()))     ' "Body width"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! Solar panels"
        s$ = (read_line())                              ' "! ------------"
        PanelsNumber = Val((read_line()))               ' "Panels number"
        SolarPanels.Clear()
        For i = 1 To PanelsNumber
            Dim TempPanel As New CSolarPanel
            With TempPanel
                .PanelType = Trim(read_line())          ' "Type"
                .PanelName = Trim(read_line())          ' "Name"
                .PanelFace = Trim(read_line())          ' "Mounting face"
                .PanelDir = Trim(read_line())           ' "Orientation"
                .PanelHpos = Trim(read_line())          ' "Height position"
                .PanelLength = Val((read_line()))       ' "Length"
                .PanelWidth = Val((read_line()))        ' "Width"
                .PanelMass = Val((read_line()))         ' "Mass"
                .PanelNbString = Val((read_line()))     ' "Strings number"
                .PanelCellAlf = Val((read_line()))      ' "Cells face absorbtivity"
                .PanelCellEps = Val((read_line()))      ' "Cells face emissivity"
                .PanelBackAlf = Val((read_line()))      ' "Back face absorbtivity"
                .PanelBackEps = Val((read_line()))      ' "Back face emissivity"
                .PanelSpecHeat = Val((read_line()))     ' "Specific heat"
                .PanelConduction = Val((read_line()))   ' "Panel conduction with central body"
                .PanelSurf = .PanelWidth * .PanelLength
            End With
            SolarPanels.Add(TempPanel)
        Next
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! Simulation data"
        s$ = (read_line())                              ' "! ---------------"
        dblSimulationStart = Val((read_line()))         ' "Simulation start date"
        dblTimeStep = Val((read_line()))                ' "Simulation time step"
        s$ = (read_line())                              ' "!"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! --------------------------"
        s$ = (read_line())                              ' "! Attitude control subsystem"
        s$ = (read_line())                              ' "! --------------------------"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                      '       "! Central body inertia"
        s$ = (read_line())                      '       "! --------------------"
        BodyCG.X = Val((read_line()))                   ' "Body CG.X"
        BodyCG.Y = Val((read_line()))                   ' "Body CG.Y"
        BodyCG.Z = Val((read_line()))                   ' "Body CG.Z"
        BodyInertia(0, 0) = Val((read_line()))          ' "Body Ixx"
        BodyInertia(1, 1) = Val((read_line()))          ' "Body Iyy"
        BodyInertia(2, 2) = Val((read_line()))          ' "Body Izz"
        BodyInertia(0, 1) = Val((read_line()))          ' "Body Ixy"
        BodyInertia(0, 2) = Val((read_line()))          ' "Body Ixz"
        BodyInertia(1, 2) = Val((read_line()))          ' "Body Iyz"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! Gravity gradient device"
        s$ = (read_line())                              ' "! -----------------------"
        GravityMass = Val((read_line()))                ' "Gradient gravity device mass"
        GravityDist.X = Val((read_line()))              ' "Gradient gravity device position X"
        GravityDist.Y = Val((read_line()))              ' "Gradient gravity device position Y"
        GravityDist.Z = Val((read_line()))              ' "Gradient gravity device position Z"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! Pressure coefficients"
        s$ = (read_line())                              ' "! ---------------------"
        Cn_aero(0) = Val((read_line()))                 ' "Normal CN +X"
        Cn_aero(1) = Val((read_line()))                 ' "Normal CN +Y"
        Cn_aero(2) = Val((read_line()))                 ' "Normal CN +Z"
        Cn_aero(3) = Val((read_line()))                 ' "Normal CN -X"
        Cn_aero(4) = Val((read_line()))                 ' "Normal CN -Y"
        Cn_aero(5) = Val((read_line()))                 ' "Normal CN -Z"
        Ct_aero(0) = Val((read_line()))                 ' "Tangential CT +X"
        Ct_aero(1) = Val((read_line()))                 ' "Tangential CT +Y"
        Ct_aero(2) = Val((read_line()))                 ' "Tangential CT +Z"
        Ct_aero(3) = Val((read_line()))                 ' "Tangential CT -X"
        Ct_aero(4) = Val((read_line()))                 ' "Tangential CT -Y"
        Ct_aero(5) = Val((read_line()))                 ' "Tangential CT -Z"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! Sun radiation coefficients"
        s$ = (read_line())                              ' "! --------------------------"
        Cr_solar(0) = Val((read_line()))                ' "Reflective CR +X"
        Cr_solar(1) = Val((read_line()))                ' "Reflective CR +Y"
        Cr_solar(2) = Val((read_line()))                ' "Reflective CR +Z"
        Cr_solar(3) = Val((read_line()))                ' "Reflective CR -X"
        Cr_solar(4) = Val((read_line()))                ' "Reflective CR -Y"
        Cr_solar(5) = Val((read_line()))                ' "Reflective CR -Z"
        Cd_solar(0) = Val((read_line()))                ' "Diffuse CD +X"
        Cd_solar(1) = Val((read_line()))                ' "Diffuse CD +Y"
        Cd_solar(2) = Val((read_line()))                ' "Diffuse CD +Z"
        Cd_solar(3) = Val((read_line()))                ' "Diffuse CD -X"
        Cd_solar(4) = Val((read_line()))                ' "Diffuse CD -Y"
        Cd_solar(5) = Val((read_line()))                ' "Diffuse CD -Z"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! Magnetic dipole (because of passive properties)"
        s$ = (read_line())                              ' "! -----------------------------------------------"
        dipsat_passive.X = Val((read_line()))           ' "Magnetic moment X"
        dipsat_passive.Y = Val((read_line()))           ' "Magnetic moment Y"
        dipsat_passive.Z = Val((read_line()))           ' "Magnetic moment Z"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! Flywheels"
        s$ = (read_line())                              ' "! ---------"
        JR(0) = Val((read_line()))                      ' "Moment of inertia X"
        JR(1) = Val((read_line()))                      ' "Moment of inertia Y"
        JR(2) = Val((read_line()))                      ' "Moment of inertia Z"
        om(0) = Val((read_line()))                      ' "Rotational speed X"
        om(1) = Val((read_line()))                      ' "Rotational speed Y"
        om(2) = Val((read_line()))                      ' "Rotational speed Z"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! Magnetic dipole (because of magnetic control system)"
        s$ = (read_line())                              ' "! ----------------------------------------------------"
        dipsat_active.X = Val((read_line()))            ' "Magnetic moment X"
        dipsat_active.Y = Val((read_line()))            ' "Magnetic moment Y"
        dipsat_active.Z = Val((read_line()))            ' "Magnetic moment Z"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! Dampers"
        s$ = (read_line())                              ' "! -------"
        DampersMass(0) = Val((read_line()))             ' "Mass X"
        DampersMass(1) = Val((read_line()))             ' "Mass Y"
        DampersMass(2) = Val((read_line()))             ' "Mass Z"
        DampersDist(0) = Val((read_line()))             ' "Position at rest X"
        DampersDist(1) = Val((read_line()))             ' "Position at rest Y"
        DampersDist(2) = Val((read_line()))             ' "Position at rest Z"
        c(0) = Val((read_line()))                       ' "Spring constant X"
        c(1) = Val((read_line()))                       ' "Spring constant Y"
        c(2) = Val((read_line()))                       ' "Spring constant Z"
        kd(0) = Val((read_line()))                      ' "Damper constant X"
        kd(1) = Val((read_line()))                      ' "Damper constant Y"
        kd(2) = Val((read_line()))                      ' "Damper constant Z"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! Nozzles"
        s$ = (read_line())                              ' "! -------"
        jet(0) = Val((read_line()))                     ' "Control moment X"
        jet(1) = Val((read_line()))                     ' "Control moment Y"
        jet(2) = Val((read_line()))                     ' "Control moment Z"
        s$ = (read_line())                              ' "!"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! ----------------"
        s$ = (read_line())                              ' "! Energy subsystem"
        s$ = (read_line())                              ' "! ----------------"
        '
        s$ = (read_line())                              ' "!"
        s$ = (read_line())                              ' "! Solar cells"
        s$ = (read_line())                              ' "! -----------"
        cell_type = Val((read_line()))                  ' "Cell type"
        cell_isc = Val((read_line()))                   ' "Isc"
        cell_imp = Val((read_line()))                   ' "Imp"
        cell_voc = Val((read_line()))                   ' "Voc"
        cell_vmp = Val((read_line()))                   ' "Vmp"
        cell_disc = Val((read_line()))                  ' "dIsc/dT"
        cell_dimp = Val((read_line()))                  ' "dImp/dT"
        cell_dvoc = Val((read_line()))                  ' "dVoc/dT"
        cell_dvmp = Val((read_line()))                  ' "dVmp/dT"
        cell_radisc = Val((read_line()))                ' "Isc/Isc0"
        cell_radimp = Val((read_line()))                ' "Imp/Imp0"
        cell_radvoc = Val((read_line()))                ' "Voc/Voc0"
        cell_radvmp = Val((read_line()))                ' "Vmp/Vmp0"
        cell_ns = Val((read_line()))                    ' "Number per string"
        cell_surf = Val((read_line()))                  ' "Cell area"
        cell_eff = Val((read_line()))                   ' "Cell efficiency"
        cell_tref = Val((read_line()))                  ' "Reference temperature"
        cell_tetad = Val((read_line()))                 ' "Divergence incidence"
        cell_tetam = Val((read_line()))                 ' "Max incidence"
        '
        If frmMain.dlgMainOpen.FilterIndex = 2 Then
            s$ = (read_line())                          ' "!"
            s$ = (read_line())                          ' "! Environnement"
            s$ = (read_line())                          '   "! -------------"
            s$ = (read_line())                          '   "! Albedo"
            For i = 0 To 11 : Albedo_month(i) = Val((read_line())) : Next
            s$ = (read_line())                          ' "! Earth infrared flux"
            For i = 0 To 11 : IR_month(i) = Val((read_line())) : Next
            SunPow = Val((read_line()))                 ' "Solar flux"
        End If
        '
        s$ = (read_line())                               ' "!"
        s$ = (read_line())                               ' "! Energy management"
        s$ = (read_line())                               ' "! -----------------"
        chini = Val((read_line()))                       ' "Initial battery charge"
        chvmax = Val((read_line()))                      ' "Max charge voltage"
        bat_courant_entretien = Val((read_line()))       ' "Holding current"
        ichmax = Val((read_line()))                      ' "Max charge current"
        kcharge = Val((read_line()))                     ' "Recharge coefficient"
        rendbat = Val((read_line()))                     ' "Battery/payload efficiency"
        rendgs = Val((read_line()))                      ' "Panels/payload efficiency"
        Vregul = Val((read_line()))                      ' "Regulated voltage"
        pdem(0) = Val((read_line()))                     ' "Day requested power"
        diss(0) = Val((read_line()))                     ' "Day dissipated power"
        pdem(1) = Val((read_line()))                     ' "Night requested power"
        diss(1) = Val((read_line()))                     ' "Night dissipated power"
        gst_type = Val((read_line()))                    ' "Regulated (1) or no regulated (0) voltage"
        '
        s$ = (read_line())                               ' "!"
        s$ = (read_line())                               ' "! Battery"
        s$ = (read_line())                               ' "! -------"
        bat_type = Val((read_line()))                    ' "Battery type"
        bat_ns = Val((read_line()))                      ' "Cells number serial"
        bat_np = Val((read_line()))                      ' "Cells number parallel"
        cnom = Val((read_line()))                        ' "Nominal capacity per cell"
        s$ = (read_line())                               ' "! Data for charge curve drawing"
        For i = 0 To 5
            ch0(i) = Val((read_line()))
            ch1(i) = Val((read_line()))
            ch2(i) = Val((read_line()))
            och(i) = Val((read_line()))
        Next
        s$ = (read_line())                               ' "! Data for discharge curve drawing"
        For i = 0 To 5
            de0(i) = Val((read_line()))
            de1(i) = Val((read_line()))
            de2(i) = Val((read_line()))
            de100(i) = Val((read_line()))
        Next
        cmax_cnom = Val((read_line()))                   ' "Influence of discharge current : Cmax/Cnom"
        c10_cnom = Val((read_line()))                    ' "Influence of discharge current : C10/Cnom à courant 10C"
        cmin100 = Val((read_line()))                     ' "Influence of temperature : Cmin % at Tref"
        tbat99 = Val((read_line()))                      ' "Influence of temperature : Temperature 99.9%"
        tbatmin = Val((read_line()))                     ' "Influence of temperature : Temperature min"
        tc0 = Val((read_line()))                         ' "Autodischarge : time constant at 0° days"
        tc50 = Val((read_line()))                        ' "Autodischarge : time constant at 50° days"
        '
        s$ = (read_line())                               ' "!"
        s$ = (read_line())                               ' "! Thermical data"
        s$ = (read_line())                               ' "! --------------"
        T_init_panels = Val((read_line()))               ' "Panels initial temperature"
        T_battery = Val((read_line()))                   ' "Battery temperature"
        BodyAlf(0) = Val((read_line()))                  ' "Absorptivity of body faces +X"
        BodyAlf(1) = Val((read_line()))                  ' "Absorptivity of body faces -X"
        BodyAlf(2) = Val((read_line()))                  ' "Absorptivity of body faces +Y"
        BodyAlf(3) = Val((read_line()))                  ' "Absorptivity of body faces -Y"
        BodyAlf(4) = Val((read_line()))                  ' "Absorptivity of body faces +Z"
        BodyAlf(5) = Val((read_line()))                  ' "Absorptivity of body faces -Z"
        BodyEps(0) = Val((read_line()))                  ' "Emissivity of body faces +X"
        BodyEps(1) = Val((read_line()))                  ' "Emissivity of body faces -X"
        BodyEps(2) = Val((read_line()))                  ' "Emissivity of body faces +Y"
        BodyEps(3) = Val((read_line()))                  ' "Emissivity of body faces -Y"
        BodyEps(4) = Val((read_line()))                  ' "Emissivity of body faces +Z"
        BodyEps(5) = Val((read_line()))                  ' "Emissivity of body faces -Z"
        BodySpecHeat = Val((read_line()))                ' "Body specific heat"
        T_init_body = Val((read_line()))                 ' "Body initial temperature"
        '
        FileClose(1)




        '
        'errorload:
        'xs = MsgBox("Writing error" & Chr(13) _
        '& " Loading data is not possible !!", vbCritical)
        ''
    End Sub

    Public Sub save_att_and_pwr_conf(ByVal file As String)
        '
        Dim i As Integer
        'Dim xs As String
        Dim TempPanel As New CSolarPanel
        '
        On Error GoTo 0
        If My.Computer.FileSystem.FileExists(file) Then Kill(file)
        On Error GoTo 0
        '
        FileOpen(1, file, OpenMode.Output)
        '
        PrintLine(1, "! ---------")
        PrintLine(1, "! Satellite")
        PrintLine(1, "! ---------")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Central body geometry")
        PrintLine(1, "! ---------------------")
        PrintLine(1, BodyMass, TAB(24), "Body mass")
        PrintLine(1, Satellite.satBodyHeight, TAB(24), "Body height")
        PrintLine(1, Satellite.satBodyWidth, TAB(24), "Body width")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Solar panels")
        PrintLine(1, "! ------------")
        PrintLine(1, PanelsNumber, TAB(24), "Panels number")
        For i = 1 To PanelsNumber
            TempPanel = SolarPanels.Item(i)
            With TempPanel
                PrintLine(1, .PanelType, TAB(24), "Type")
                PrintLine(1, .PanelName, TAB(24), "Name")
                PrintLine(1, .PanelFace, TAB(24), "Mounting face")
                PrintLine(1, .PanelDir, TAB(24), "Orientation")
                PrintLine(1, .PanelHpos, TAB(24), "Height position")
                PrintLine(1, .PanelLength, TAB(24), "Length")
                PrintLine(1, .PanelWidth, TAB(24), "Width")
                PrintLine(1, .PanelMass, TAB(24), "Mass")
                PrintLine(1, .PanelNbString, TAB(24), "Strings number")
                PrintLine(1, .PanelCellAlf, TAB(24), "Cells face absorbtivity")
                PrintLine(1, .PanelCellEps, TAB(24), "Cells face emissivity")
                PrintLine(1, .PanelBackAlf, TAB(24), "Back face absorbtivity")
                PrintLine(1, .PanelBackEps, TAB(24), "Back face emissivity")
                PrintLine(1, .PanelSpecHeat, TAB(24), "Specific heat")
                PrintLine(1, .PanelConduction, TAB(24), "Panel conduction with central body")
            End With
        Next
        '
        PrintLine(1, "!")
        PrintLine(1, "! Simulation data")
        PrintLine(1, "! ---------------")
        PrintLine(1, dblSimulationStart, TAB(24), "Simulation start date")
        PrintLine(1, dblTimeStep, TAB(24), "Simulation time step")
        PrintLine(1, "!")
        '
        PrintLine(1, "!")
        PrintLine(1, "! --------------------------")
        PrintLine(1, "! Attitude control subsystem")
        PrintLine(1, "! --------------------------")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Central body inertia")
        PrintLine(1, "! --------------------")
        PrintLine(1, BodyCG.X, TAB(24), "Body CG.X")
        PrintLine(1, BodyCG.Y, TAB(24), "Body CG.Y")
        PrintLine(1, BodyCG.Z, TAB(24), "Body CG.Z")
        PrintLine(1, BodyInertia(0, 0), TAB(24), "Body Ixx")
        PrintLine(1, BodyInertia(1, 1), TAB(24), "Body Iyy")
        PrintLine(1, BodyInertia(2, 2), TAB(24), "Body Izz")
        PrintLine(1, BodyInertia(0, 1), TAB(24), "Body Ixy")
        PrintLine(1, BodyInertia(0, 2), TAB(24), "Body Ixz")
        PrintLine(1, BodyInertia(1, 2), TAB(24), "Body Iyz")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Gravity gradient device")
        PrintLine(1, "! -----------------------")
        PrintLine(1, GravityMass, TAB(24), "Gradient gravity device mass")
        PrintLine(1, GravityDist.X, TAB(24), "Gradient gravity device position X")
        PrintLine(1, GravityDist.Y, TAB(24), "Gradient gravity device position Y")
        PrintLine(1, GravityDist.Z, TAB(24), "Gradient gravity device position Z")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Pressure coefficients")
        PrintLine(1, "! ---------------------")
        PrintLine(1, Cn_aero(0), TAB(24), "Normal CN +X")
        PrintLine(1, Cn_aero(1), TAB(24), "Normal CN +Y")
        PrintLine(1, Cn_aero(2), TAB(24), "Normal CN +Z")
        PrintLine(1, Cn_aero(3), TAB(24), "Normal CN -X")
        PrintLine(1, Cn_aero(4), TAB(24), "Normal CN -Y")
        PrintLine(1, Cn_aero(5), TAB(24), "Normal CN -Z")
        PrintLine(1, Ct_aero(0), TAB(24), "Tangential CT +X")
        PrintLine(1, Ct_aero(1), TAB(24), "Tangential CT +Y")
        PrintLine(1, Ct_aero(2), TAB(24), "Tangential CT +Z")
        PrintLine(1, Ct_aero(3), TAB(24), "Tangential CT -X")
        PrintLine(1, Ct_aero(4), TAB(24), "Tangential CT -Y")
        PrintLine(1, Ct_aero(5), TAB(24), "Tangential CT -Z")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Sun radiation coefficients")
        PrintLine(1, "! --------------------------")
        PrintLine(1, Cr_solar(0), TAB(24), "Reflective CR +X")
        PrintLine(1, Cr_solar(1), TAB(24), "Reflective CR +Y")
        PrintLine(1, Cr_solar(2), TAB(24), "Reflective CR +Z")
        PrintLine(1, Cr_solar(3), TAB(24), "Reflective CR -X")
        PrintLine(1, Cr_solar(4), TAB(24), "Reflective CR -Y")
        PrintLine(1, Cr_solar(5), TAB(24), "Reflective CR -Z")
        PrintLine(1, Cd_solar(0), TAB(24), "Diffuse CD +X")
        PrintLine(1, Cd_solar(1), TAB(24), "Diffuse CD +Y")
        PrintLine(1, Cd_solar(2), TAB(24), "Diffuse CD +Z")
        PrintLine(1, Cd_solar(3), TAB(24), "Diffuse CD -X")
        PrintLine(1, Cd_solar(4), TAB(24), "Diffuse CD -Y")
        PrintLine(1, Cd_solar(5), TAB(24), "Diffuse CD -Z")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Magnetic dipole (because of passive properties)")
        PrintLine(1, "! -----------------------------------------------")
        PrintLine(1, dipsat_passive.X, TAB(24), "Magnetic moment X")
        PrintLine(1, dipsat_passive.Y, TAB(24), "Magnetic moment Y")
        PrintLine(1, dipsat_passive.Z, TAB(24), "Magnetic moment Z")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Flywheels")
        PrintLine(1, "! ---------")
        PrintLine(1, JR(0), TAB(24), "Moment of inertia X")
        PrintLine(1, JR(1), TAB(24), "Moment of inertia Y")
        PrintLine(1, JR(2), TAB(24), "Moment of inertia Z")
        PrintLine(1, om(0), TAB(24), "Rotational speed X")
        PrintLine(1, om(1), TAB(24), "Rotational speed Y")
        PrintLine(1, om(2), TAB(24), "Rotational speed Z")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Magnetic dipole (because of magnetic control system)")
        PrintLine(1, "! ----------------------------------------------------")
        PrintLine(1, dipsat_active.X, TAB(24), "Magnetic moment X")
        PrintLine(1, dipsat_active.Y, TAB(24), "Magnetic moment Y")
        PrintLine(1, dipsat_active.Z, TAB(24), "Magnetic moment Z")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Dampers")
        PrintLine(1, "! -------")
        PrintLine(1, DampersMass(0), TAB(24), "Mass X")
        PrintLine(1, DampersMass(1), TAB(24), "Mass Y")
        PrintLine(1, DampersMass(2), TAB(24), "Mass Z")
        PrintLine(1, DampersDist(0), TAB(24), "Position at rest X")
        PrintLine(1, DampersDist(1), TAB(24), "Position at rest Y")
        PrintLine(1, DampersDist(2), TAB(24), "Position at rest Z")
        PrintLine(1, c(0), TAB(24), "Spring constant X")
        PrintLine(1, c(1), TAB(24), "Spring constant Y")
        PrintLine(1, c(2), TAB(24), "Spring constant Z")
        PrintLine(1, kd(0), TAB(24), "Damper constant X")
        PrintLine(1, kd(1), TAB(24), "Damper constant Y")
        PrintLine(1, kd(2), TAB(24), "Damper constant Z")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Nozzles")
        PrintLine(1, "! -------")
        PrintLine(1, jet(0), TAB(24), "Control moment X")
        PrintLine(1, jet(1), TAB(24), "Control moment Y")
        PrintLine(1, jet(2), TAB(24), "Control moment Z")
        PrintLine(1, "!")
        '
        PrintLine(1, "!")
        PrintLine(1, "! ----------------")
        PrintLine(1, "! Energy subsystem")
        PrintLine(1, "! ----------------")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Solar cells")
        PrintLine(1, "! -----------")
        PrintLine(1, cell_type, TAB(24), "Cell type")
        PrintLine(1, cell_isc, TAB(24), "Isc")
        PrintLine(1, cell_imp, TAB(24), "Imp")
        PrintLine(1, cell_voc, TAB(24), "Voc")
        PrintLine(1, cell_vmp, TAB(24), "Vmp")
        PrintLine(1, cell_disc, TAB(24), "dIsc/dT")
        PrintLine(1, cell_dimp, TAB(24), "dImp/dT")
        PrintLine(1, cell_dvoc, TAB(24), "dVoc/dT")
        PrintLine(1, cell_dvmp, TAB(24), "dVmp/dT")
        PrintLine(1, cell_radisc, TAB(24), "Isc/Isc0")
        PrintLine(1, cell_radimp, TAB(24), "Imp/Imp0")
        PrintLine(1, cell_radvoc, TAB(24), "Voc/Voc0")
        PrintLine(1, cell_radvmp, TAB(24), "Vmp/Vmp0")
        PrintLine(1, cell_ns, TAB(24), "Number per string")
        PrintLine(1, cell_surf, TAB(24), "Cell area")
        PrintLine(1, cell_eff, TAB(24), "Cell efficiency")
        PrintLine(1, cell_tref, TAB(24), "Reference temperature")
        PrintLine(1, cell_tetad, TAB(24), "Divergence incidence")
        PrintLine(1, cell_tetam, TAB(24), "Max incidence")
        '
        'PrintLine(1, "!")
        'PrintLine(1, "! Environnement")
        'PrintLine(1, "! -------------")
        'PrintLine(1, "! Albedo")
        'For i = 0 To 11 : PrintLine(1, Albedo_month(i)) : Next
        'PrintLine(1, "! Earth infrared flux")
        'For i = 0 To 11 : PrintLine(1, IR_month(i)) : Next
        'PrintLine(1, Format$(SunPow, "0.0000E+00"), TAB(24), "Solar flux")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Energy management")
        PrintLine(1, "! -----------------")
        PrintLine(1, chini, TAB(24), "Initial battery charge")
        PrintLine(1, chvmax, TAB(24), "Max charge voltage")
        PrintLine(1, bat_courant_entretien, TAB(24), "Holding current")
        PrintLine(1, ichmax, TAB(24), "Max charge current")
        PrintLine(1, kcharge, TAB(24), "Recharge coefficient")
        PrintLine(1, rendbat, TAB(24), "Battery/payload efficiency")
        PrintLine(1, rendgs, TAB(24), "Panels/payload efficiency")
        PrintLine(1, Vregul, TAB(24), "Regulated voltage")
        PrintLine(1, pdem(0), TAB(24), "Day requested power")
        PrintLine(1, diss(0), TAB(24), "Day dissipated power")
        PrintLine(1, pdem(1), TAB(24), "Night requested power")
        PrintLine(1, diss(1), TAB(24), "Night dissipated power")
        PrintLine(1, gst_type, TAB(24), "Regulated (1) or no regulated (0) voltage")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Battery")
        PrintLine(1, "! -------")
        PrintLine(1, bat_type, TAB(24), "Battery type")
        PrintLine(1, bat_ns, TAB(24), "Cells number serial")
        PrintLine(1, bat_np, TAB(24), "Cells number parallel")
        PrintLine(1, cnom, TAB(24), "Nominal capacity per cell")
        PrintLine(1, "! Data for charge curve drawing")
        For i = 0 To 5
            PrintLine(1, ch0(i))
            PrintLine(1, ch1(i))
            PrintLine(1, ch2(i))
            PrintLine(1, och(i))
        Next
        PrintLine(1, "! Data for discharge curve drawing")
        For i = 0 To 5
            PrintLine(1, de0(i))
            PrintLine(1, de1(i))
            PrintLine(1, de2(i))
            PrintLine(1, de100(i))
        Next
        PrintLine(1, cmax_cnom, TAB(24), "Influence of discharge current : Cmax/Cnom")
        PrintLine(1, c10_cnom, TAB(24), "Influence of discharge current : C10/Cnom à courant 10C")
        PrintLine(1, cmin100, TAB(24), "Influence of temperature : Cmin % at Tref")
        PrintLine(1, tbat99, TAB(24), "Influence of temperature : Temperature 99.9%")
        PrintLine(1, tbatmin, TAB(24), "Influence of temperature : Temperature min")
        PrintLine(1, tc0, TAB(24), "Autodischarge : time constant at 0° days")
        PrintLine(1, tc50, TAB(24), "Autodischarge : time constant at 50° days")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Thermical data")
        PrintLine(1, "! --------------")
        PrintLine(1, T_init_panels, TAB(24), "Panels initial temperature")
        PrintLine(1, T_battery, TAB(24), "Battery temperature")
        PrintLine(1, BodyAlf(0), TAB(24), "Absorptivity of body faces +X")
        PrintLine(1, BodyAlf(1), TAB(24), "Absorptivity of body faces -X")
        PrintLine(1, BodyAlf(2), TAB(24), "Absorptivity of body faces +Y")
        PrintLine(1, BodyAlf(3), TAB(24), "Absorptivity of body faces -Y")
        PrintLine(1, BodyAlf(4), TAB(24), "Absorptivity of body faces +Z")
        PrintLine(1, BodyAlf(5), TAB(24), "Absorptivity of body faces -Z")
        PrintLine(1, BodyEps(0), TAB(24), "Emissivity of body faces +X")
        PrintLine(1, BodyEps(1), TAB(24), "Emissivity of body faces -X")
        PrintLine(1, BodyEps(2), TAB(24), "Emissivity of body faces +Y")
        PrintLine(1, BodyEps(3), TAB(24), "Emissivity of body faces -Y")
        PrintLine(1, BodyEps(4), TAB(24), "Emissivity of body faces +Z")
        PrintLine(1, BodyEps(5), TAB(24), "Emissivity of body faces -Z")
        PrintLine(1, BodySpecHeat, TAB(24), "Body specific heat")
        PrintLine(1, T_init_body, TAB(24), "Body initial temperature")
        '
        FileClose(1)
        '
        Exit Sub
        '
        'errorsave:
        'xs = MsgBox("Writing error" & Chr(13) _
        '& " Saving data is not possible !!", vbCritical)
        ''
    End Sub

    Public Sub load_data_range()

        Dim json As String = File.ReadAllText(System.IO.Path.Combine(My.Application.Info.DirectoryPath, "resources\conf.json"))

        Dim dict As New Dictionary(Of String, Double)

        dict = JsonConvert.DeserializeObject(Of Dictionary(Of String, Double))(json)



        BodyMass_min = dict.Item("BodyMass_min")            ' "Body Mass (min)"
        BodyMass_max = dict.Item("BodyMass_max")            ' "Body Mass (max)"
        BodyHeigth_min = dict.Item("BodyHeigth_min")        ' "Body Heigth (min)"
        BodyHeigth_max = dict.Item("BodyHeigth_max")        ' "Body Heigth (max)"
        BodyWidth_min = dict.Item("BodyWidth_min")          ' "Body Width (min)"
        BodyWidth_max = dict.Item("BodyWidth_max")          ' "Body Width (max)"
        BodyInertia_min = dict.Item("BodyInertia_min")      ' "Body Inertia (min)"
        BodyInertia_max = dict.Item("BodyInertia_max")      ' "Body Inertia (max)"
        BoomMass_min = dict.Item("BoomMass_min")            ' "Gravity Boom Mass (min)"
        BoomMass_max = dict.Item("BoomMass_max")            ' "Gravity Boom Mass (max)"
        BoomDist_min = dict.Item("BoomDist_min")            ' "Gravity Boom Distance (min)"
        BoomDist_max = dict.Item("BoomDist_max")            ' "Gravity Boom Distance (max)"
        '
        PanelMass_min = dict.Item("PanelMass_min")          ' "Panel Mass (min)"
        PanelMass_max = dict.Item("PanelMass_max")          ' "Panel Mass (max)"
        PanelStrings_min = dict.Item("PanelStrings_min")    ' "Panel Number of Strings (min)"
        PanelStrings_max = dict.Item("PanelStrings_max")    ' "Panel Number of Strings (max)"
        PanelLength_min = dict.Item("PanelLength_min")      ' "Panel Length (min)"
        PanelLength_max = dict.Item("PanelLength_max")      ' "Panel Length (max)"
        PanelWidth_min = dict.Item("PanelWidth_min")        ' "Panel Width (min)"
        PanelWidth_max = dict.Item("PanelWidth_max")        ' "Panel Width (max)"
        '
        dipsat_passive_min = dict.Item("dipsat_passive_min")      ' "Magnetic moment (min)"
        dipsat_passive_max = dict.Item("dipsat_passive_max")      ' "Magnetic moment (max)"
        '
        dipsat_active_min = dict.Item("dipsat_active_min")        ' "Magnetic moment (min)"
        dipsat_active_max = dict.Item("dipsat_active_max")        ' "Magnetic moment (max)"
        '
        JR_min = dict.Item("JR_min")                ' "Moment of inertia (min)"
        JR_max = dict.Item("JR_max")                ' "Moment of inertia (max)"
        om_min = dict.Item("om_min")                ' "Rotational speed (min)"
        om_max = dict.Item("om_max")                ' "Rotational speed (max)"
        '
        DampersMass_min = dict.Item("DampersMass_min")         ' "Mass (min)"
        DampersMass_max = dict.Item("DampersMass_max")         ' "Mass (max)"
        DampersDist_min = dict.Item("DampersDist_min")         ' "Position at rest (min)"
        DampersDist_max = dict.Item("DampersDist_max")         ' "Position at rest (max)"
        c_min = dict.Item("c_min")                             ' "Spring constant (min)"
        c_max = dict.Item("c_max")                             ' "Spring constant (max)"
        kd_min = dict.Item("kd_min")                           ' "Damper constant (min)"
        kd_max = dict.Item("kd_max")                           ' "Damper constant (max)"
        '
        jet_min = dict.Item("jet_min")                ' "Control moment (min)"
        jet_max = dict.Item("jet_max")                ' "Control moment (max)"

    End Sub

    Public Sub load_default_att_values()
        '
        Dim i As Integer
        Dim s$ = ""
        Dim temp1, temp2, temp3, temp4 As Double
        '
        On Error Resume Next
        'FileOpen(1, My.Application.Info.DirectoryPath & "resources\default_values.txt", OpenMode.Input)
        FileOpen(1, System.IO.Path.Combine(My.Application.Info.DirectoryPath, "resources\default_values.txt"), OpenMode.Input)
        On Error GoTo 0
        '
        s$ = (read_line())                      ' "! --------------------------------"
        s$ = (read_line())                      ' "! Default values for each variable"
        s$ = (read_line())                      ' "! --------------------------------"
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! Central body"
        s$ = (read_line())                      ' "! ------------"
        BodyMass = (read_line())                ' "Body Mass
        Satellite.satBodyHeight = (read_line()) ' "Body Heigth
        Satellite.satBodyWidth = (read_line())  ' "Body Width
        '
        temp1 = (read_line())                    ' "Body CG
        BodyCG.X = temp1
        BodyCG.Y = temp1
        BodyCG.Z = temp1
        '
        temp1 = (read_line())                    ' "Body Inertia (Ixx, Iyy, Izz)
        BodyInertia(0, 0) = temp1
        BodyInertia(1, 1) = temp1
        BodyInertia(2, 2) = temp1
        '
        temp1 = (read_line())                    ' "Body Inertia (Iyz, Ixy, Ixz)
        BodyInertia(0, 1) = temp1
        BodyInertia(0, 2) = temp1
        BodyInertia(1, 0) = temp1
        BodyInertia(1, 2) = temp1
        BodyInertia(2, 0) = temp1
        BodyInertia(2, 1) = temp1
        '
        GravityMass = (read_line())             ' "Gravity Boom Mass
        GravityDist.X = (read_line())           ' "Gravity Boom Distance
        GravityDist.Y = 0
        GravityDist.Z = 0
        '
        s$ = (read_line())                                      ' "!"
        s$ = (read_line())                                      ' "! Solar panels"
        s$ = (read_line())                                      ' "! ------------"
        frmAttConf.mass_txtPanelMass.Text = CStr(read_line())   ' "Panel Mass
        frmAttConf.mass_txtPanelLength.Text = CStr(read_line()) ' "Panel Length
        frmAttConf.mass_txtPanelWidth.Text = CStr(read_line())  ' "Panel Width
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! Magnetic dipole (because of passive properties)"
        s$ = (read_line())                      ' "! -----------------------------------------------"
        temp1 = (read_line())                    ' "Magnetic moment
        dipsat_passive.X = temp1
        dipsat_passive.Y = temp1
        dipsat_passive.Z = temp1
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! Pressure coefficients (Normal)"
        s$ = (read_line())                      ' "! ------------------------------"
        For i = 0 To 5
            Cn_aero(i) = (read_line())
        Next
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! Pressure coefficients (Tangential)"
        s$ = (read_line())                      ' "! ----------------------------------"
        For i = 0 To 5
            Ct_aero(i) = (read_line())
        Next
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! Sun radiation coefficients (Reflective)"
        s$ = (read_line())                      ' "! ---------------------------------------"
        For i = 0 To 5
            Cr_solar(i) = (read_line())
        Next
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! Sun radiation coefficients (Diffuse)"
        s$ = (read_line())                      ' "! ------------------------------------"
        For i = 0 To 5
            Cd_solar(i) = (read_line())
        Next
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! Magnetic dipole (because of magnetic control system)"
        s$ = (read_line())                      ' "! ----------------------------------------------------"
        temp1 = (read_line())                    ' "Magnetic moment
        dipsat_active.X = temp1
        dipsat_active.Y = temp1
        dipsat_active.Z = temp1
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! Flywheels"
        s$ = (read_line())                      ' "! ---------"
        temp1 = (read_line())                   ' "Moment of inertia
        temp2 = (read_line())                   ' "Rotational speed
        For i = 0 To 2
            JR(i) = temp1
            om(i) = temp2
        Next
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! Dampers"
        s$ = (read_line())                      ' "! -------"
        temp1 = (read_line())                   ' "Mass
        temp2 = (read_line())                   ' "Position at rest
        temp3 = (read_line())                   ' "Spring constant
        temp4 = (read_line())                   ' "Damper constant
        For i = 0 To 2
            DampersMass(i) = temp1
            DampersDist(i) = temp2
            c(i) = temp3
            kd(i) = temp4
        Next
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! Nozzles"
        s$ = (read_line())                      ' "! -------"
        temp1 = (read_line())                   ' "Control moment
        For i = 0 To 2
            jet(i) = temp1
        Next
        '
        FileClose(1)
        ''
    End Sub

    Public Sub load_thermal_models()

        Dim json As String = File.ReadAllText(System.IO.Path.Combine(My.Application.Info.DirectoryPath, "resources\thermalfiles.json"))

        Dim dict As New Dictionary(Of String, String)

        dict = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(json)

        EarthConfFactorFile = dict.Item("Earth_Configuration_Factor")
        EarthTempFile = dict.Item("Earth_Temperature_(ESA_mesurements)")
        AlbedoCoefFile = dict.Item("Albedo_Coefficient_(ESA_mesurements)")
    End Sub


    Public Sub load_test_files()
        Dim json As String = File.ReadAllText(System.IO.Path.Combine(My.Application.Info.DirectoryPath, "resources\testfiles.json"))

        Dim dict As New Dictionary(Of String, String)

        dict = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(json)

        TestSat = dict.Item("Integration_Test_Sat")
        TestReport = dict.Item("Integration_Test_Report")
        TestIntegration = dict.Item("Integration_Test_ReportNew")
        TestSatIntegration = dict.Item("Integration_Test_Sat")
    End Sub

End Module
