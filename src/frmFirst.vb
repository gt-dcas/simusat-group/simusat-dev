Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmFirst
	Inherits System.Windows.Forms.Form

    Public Sub frmFirst_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        ForceDotForCurrentApplication()

        'disable events
        allow_event = False

        ' Show the window
        Show()
        Refresh()
        Activate()
        '
        ' Get the decimal separator
        decimalSeparator = "."
        Dim buffer As String = ""
        If 0 <> GetLocaleInfo(GetSystemDefaultLCID, LOCALE_SDECIMAL, buffer, 4) Then
            decimalSeparator = Cstr2Bstr(buffer)
        End If
        '
        ' Loading Textures
        ShowProgress((10))
        lblLoading.Text = "Loading in progress"
        Refresh()
        '
        blnIsNewPrj = False
        frmMain.Show()

        'enable events
        allow_event = True
        'Hide()
        '
    End Sub

	Private Function Cstr2Bstr(ByRef buffer As String) As String
		Dim Length As Integer
        Length = InStr(1, buffer, Chr(0))
        'experimental
        Cstr2Bstr = ""
		If Length > 0 Then
			Cstr2Bstr = VB.Left(buffer, Length - 1)
		End If
	End Function
	
    Public Sub ShowProgress(ByRef i As Short)
        '
        ProgressBar.Value = i
        '
    End Sub
	
	Function AppPath() As Object
		Dim X As String
		X = My.Application.Info.DirectoryPath
		If VB.Right(X, 1) <> "\" Then X = X & "\"
		AppPath = UCase(X)
	End Function
    
End Class