﻿Module thermal
    ' Central body thermical characteristics
    Public BodyAlf(5) As Single     ' Absorptivity of body faces  0: +x / 1: -x / 2: +y / 3: -y / 4: +z / 5: -z
    Public BodyEps(5) As Single     ' Emissivity of body faces
    Public BodySpecHeat As Single   ' Body specific heat
    Public BodyTemp As Single       ' Body temperature
    Public T_init_body As Single    ' Initial temperature of central body

    Public flux_solaire As Single
    Public albedo As Single
    Public cos_sun_earth_sat As Double
    Public flux_ir As Double
    Public flux_albedo As Double

    Public Function solar_intensity(ByVal d As Double) As Single
        '
        ' Direct solar radiation intensity for a given distance to Sun
        ' Input: d distance to Sun
        '
        solar_intensity = CSng(SunPow / 4 / pi / d / d / 1000000.0#)
        '
    End Function

    Public Function flux_incident_overall_power(ByVal SolarPanel As CSolarPanel, ByVal Time As Double) As Double 'Used only in power.vb
        '
        ' Returns the value of the normal-equivalent incident energie flux related to current
        ' sun & earth position, also taking into account the effect of sun incidence relative to the panel
        '
        ' Input : solar panel
        '
        Dim fs As Double
        Dim fa As Double
        Dim sun_flux_ratio As Double
        Dim albedo_flux_ratio As Double
        '
        sun_flux_ratio = flux_effective_ratio(SolarPanel.SunIncidence(Time))
        If blnEclipse Then
            sun_flux_ratio = 0
        End If
        If sun_flux_ratio > 0 Then
            fs = solar_intensity(vctNorm(SunPos)) * sun_flux_ratio
        Else
            fs = 0
        End If
        albedo_flux_ratio = flux_effective_ratio(SolarPanel.EarthIncidence(Time))
        If albedo_flux_ratio > 0 Then
            fa = flux_albedo * albedo_flux_ratio
        Else
            fa = 0
        End If
        flux_incident_overall_power = fa + fs
        SolarPanel.PanelEffFlux = flux_incident_overall_power
        ''
    End Function

    Public Function flux_effective_ratio(ByVal teta As Double) As Double
        '
        ' Returns portion of equivalent incident flux related to the
        ' sun angle over the cells plain taking account the curve which is entered in the solar cells definition
        ' in the energy subsystem data window.
        '
        ' Input:
        '   - Angle of incidence
        '
        Dim p(2) As Double
        Dim tm As Double
        Dim td As Double
        Dim X As Double
        tm = cell_tetam * rad
        td = cell_tetad * rad
        '
        ' Features of Parabolical section
        p(0) = (Math.Sin(td) * (tm - td) - Math.Cos(td)) / (tm - td) ^ 2
        p(1) = -Math.Sin(td) - 2 * p(0) * td
        p(2) = -p(0) * tm * tm - p(1) * tm
        If teta <= td Then
            X = Math.Cos(teta)
        ElseIf teta > td And teta < tm Then
            X = p(0) * teta * teta + p(1) * teta + p(2)
        Else
            X = 0
        End If
        If X < 0 Then
            X = 0
        End If
        flux_effective_ratio = X
        ''
    End Function

    Public Sub flux_calculation(ByVal j_current As Double)
        '
        ' Solar flux, albedo and earth IR calculation
        '
        ' Entrée : j_current : Current time in Julian days
        '
        Dim r_sat_earth As Double
        Dim r_sun_earth As Double
        Dim earth_angle_from_sat As Double
        Dim ka As Double
        Dim X As Double
        Dim udtGregorianNow As udtDate
        '
        '
        ' Solar flux calculation
        ' ----------------------
        ' The current value for the solar constant results from the Sat-Sun distance and
        ' decreases with is 2nd power
        flux_solaire = solar_intensity(vctNorm(SunPos))
        '
        ' Albedo calculation
        ' ------------------
        ' SatPos and SunPos are respectively Earth_Sat and Earth_Sun vectors
        ' Then Sat_Earth and Sat_Sun unitary vectors are obtained, as well as Earth's disc
        ' (by mean of its angular radius earth_angle_from_sat).
        ' The boolean blnEclipse (already obtained by orbitography updating) specifies if the satellite is in eclipse or not (True/False respectively).
        r_sat_earth = vctNorm(SatPos)
        r_sun_earth = vctNorm(SunPos)
        earth_angle_from_sat = Math.Asin(RE / r_sat_earth)
        '
        ' That is, flux_solaire * albedo * ka * cos (angle (sat/sun,sat/earth))
        ' with ka = 0.664 + 0.521 * earth_angle_from_sat - 0.203 * earth_angle_from_sat ^ 2
        ' and earth_angle_from_sat = earth's angular radius as seen from the satellite
        ' See LARSON-WERTZ
        ' We take the equatorial radius of earth since its value is the greatest
        ' Albedo is related to current sat/earth/sun angle through the factor 
        ' [cos(0.9*angle)]^1.5 (CNES) means that above 100° no albedo reaches the satellite

        ka = 0.664 + 0.521 * earth_angle_from_sat - 0.203 * earth_angle_from_sat ^ 2
        cos_sun_earth_sat = vctDot(SunPos, SatPos) / r_sun_earth / r_sat_earth
        X = Math.Acos(cos_sun_earth_sat)
        X = Math.Cos(0.9 * X)
        If X < 0 Then X = 0 ' If flux is negative (angle sun/sat/earth > 100°
        X = Math.Sqrt(X ^ 3)
        ' Current values for albedo and ir depend on which is the current month
        ' within the year.
        convert_Julian_Gregorian(j_current, udtGregorianNow)
        albedo = Albedo_month(udtGregorianNow.month - 1)
        flux_albedo = flux_solaire * albedo * ka * X * (RE / r_sat_earth) ^ 2
        '
        ' IR flux calculation
        ' -------------------
        flux_ir = IR_month(udtGregorianNow.month - 1) * (RE / r_sat_earth) ^ 2
        ''
    End Sub
    Public Sub compute_thermal(ByVal Time As Double, ByVal TimeStep As Double)
        '
        Dim panel As CSolarPanel
        Dim deltaT As Double
        Dim alf(1) As Double    ' Absorptivity
        Dim eps(1) As Double    ' Emissivity
        Dim fs(1) As Double, fs1(1) As Double, fs2(1) As Double     ' Solar + albedo flux
        Dim fir(1) As Double    ' Earth Infra Red flux
        Dim i As Integer
        Dim X As Single
        '
        ' ----------------------------
        ' Central body thermal balance 
        ' ----------------------------
        Dim SunDir As vector                ' Sun direction in the satellite's coordinates system 
        Dim EarthDir As vector              ' Earth direction in the satellite's coordinates system 
        Dim Face_dir(5) As vector           ' 0: +x / 1: -x / 2: +y / 3: -y / 4: +z / 5: -z
        Dim Face_SunInc(5) As Single        ' Sun incidence on faces         
        Dim Face_SolarFlux(5) As Single     ' Solar flux on faces   
        Dim Face_EarthInc(5) As Single
        Dim Face_AlbedoFlux(5) As Single    ' Albedo flux on faces   
        Dim Face_IRFlux(5) As Single        ' Infrared flux on faces 
        Dim Face_Surf(5) As Single
        Dim Face_conduct(5) As Single       ' Power due to conduction with solar panel
        Dim tmpVect As vector
        '
        ' Calculate flux for each face 
        ' ----------------------------
        ' Effective absorbed and emmited flux (Watts) taking account of surface, absortivity alf and emissivity eps of each face
        With Satellite.getSatCOSCoords(Time, SunPos)
            SunDir.X = .X : SunDir.Y = .Y : SunDir.Z = .Z
        End With
        '
        Face_dir(0).X = 1 : Face_dir(0).Y = 0 : Face_dir(0).Z = 0
        Face_dir(1).X = -1 : Face_dir(1).Y = 0 : Face_dir(1).Z = 0
        Face_dir(2).X = 0 : Face_dir(2).Y = 1 : Face_dir(2).Z = 0
        Face_dir(3).X = 0 : Face_dir(3).Y = -1 : Face_dir(3).Z = 0
        Face_dir(4).X = 0 : Face_dir(4).Y = 0 : Face_dir(4).Z = 1
        Face_dir(5).X = 0 : Face_dir(5).Y = 0 : Face_dir(5).Z = -1
        '
        For i = 0 To 5
            '
            ' Face surface
            Select Case i
                Case 0 To 4 : Face_Surf(i) = Satellite.satBodyWidth * Satellite.satBodyWidth    ' +X -X +Y -Y
                Case Else : Face_Surf(i) = Satellite.satBodyWidth * Satellite.satBodyHeight     ' +Z -Z
            End Select
            ' Solar flux
            Face_SunInc(i) = Math.Acos(vctDot(SunDir, Face_dir(i)) / vctNorm(SunDir) / vctNorm(Face_dir(i)))
            Face_SolarFlux(i) = flux_solaire * Math.Cos(Face_SunInc(i)) * Face_Surf(i) * BodyAlf(i)
            If blnEclipse Or Math.Cos(Face_SunInc(i)) <= 0 Then Face_SolarFlux(i) = 0
            '
            ' Albedo flux
            tmpVect.X = 0 : tmpVect.Y = 0 : tmpVect.Z = -1 'modif here for thermal try...
            With Satellite.getSatCOSCoords(Time, tmpVect)
                EarthDir.X = .X : EarthDir.Y = .Y : EarthDir.Z = .Z ' Earth direction in the satellite's COS
            End With
            'printOnConsoleIfBebugMode("EarthDir: X:" + EarthDir.X.ToString + " Y:" + EarthDir.Y.ToString + " Z:" + EarthDir.Z.ToString)

            'printOnConsoleIfBebugMode("vctNorme(EarthDir):" + vctNorme(EarthDir).ToString)
            'printOnConsoleIfBebugMode("vctNorme(Face_dir(" + i.ToString + ")) is:" + vctNorme(Face_dir(i)).ToString)

            Face_EarthInc(i) = Math.Acos(vctDot(EarthDir, Face_dir(i)) / vctNorm(EarthDir) / vctNorm(Face_dir(i)))
            Face_AlbedoFlux(i) = flux_albedo * facteur_vue(Face_EarthInc(i) * 180 / pi) * Face_Surf(i) * BodyAlf(i)
            'printOnConsoleIfBebugMode("flux_albedo is " + flux_albedo.ToString)
            'printOnConsoleIfBebugMode("Face_EarthInc(" + i.ToString + ") is:" + Face_EarthInc(i).ToString)
            'printOnConsoleIfBebugMode("Face_Surf(" + i.ToString + ") is:" + Face_Surf(i).ToString)
            'printOnConsoleIfBebugMode("Face_dir(" + i.ToString + ") is:" + Face_dir(i).ToString)


            ' 
            ' Received Infrared flux
            Face_IRFlux(i) = flux_ir * facteur_vue(Face_EarthInc(i) * 180 / pi) * Face_Surf(i) * BodyEps(i)
            '
            'printOnConsoleIfBebugMode("Face_AlbedoFlux(" + i.ToString + ") is:" + Face_AlbedoFlux(i).ToString)
            'printOnConsoleIfBebugMode("Face_IRFlux(" + i.ToString + ") is:" + Face_IRFlux(i).ToString)

        Next
        '
        ' Add interaction with solar panels
        ' ---------------------------------
        For i = 1 To SolarPanels.Count()
            panel = SolarPanels.Item(i)
            If panel.PanelType = "Type2" Then
                With panel
                    Select Case .PanelFace
                        '
                        Case "+ X"
                            '
                            ' - Remove external flux masked by the solar panel
                            X = Math.Max(1 - .PanelSurf / Face_Surf(0), 0)
                            Face_SolarFlux(0) *= X
                            Face_AlbedoFlux(0) *= X
                            Face_IRFlux(0) *= X
                            '
                            ' Add infrared flux coming from solar panel
                            Face_IRFlux(0) += .PanelSurf * .PanelBackEps * C_Stefan * (.PanelTemp + 273.15) ^ 4
                            '
                            ' Add conduction with solar panel
                            Face_conduct(0) = .PanelConduction * (.PanelTemp - BodyTemp) * .PanelSurf
                            '
                        Case "- X"
                            '
                            X = Math.Max(1 - .PanelSurf / Face_Surf(1), 0)
                            Face_SolarFlux(1) *= X
                            Face_AlbedoFlux(1) *= X
                            Face_IRFlux(1) *= X
                            '
                            Face_IRFlux(1) += .PanelSurf * .PanelBackEps * C_Stefan * (.PanelTemp + 273.15) ^ 4
                            '
                            Face_conduct(1) = .PanelConduction * (.PanelTemp - BodyTemp) * .PanelSurf
                            '
                        Case "+ Y"
                            '
                            X = Math.Max(1 - .PanelSurf / Face_Surf(2), 0)
                            Face_SolarFlux(2) *= X
                            Face_AlbedoFlux(2) *= X
                            Face_IRFlux(2) *= X
                            '
                            Face_IRFlux(2) += .PanelSurf * .PanelBackEps * C_Stefan * (.PanelTemp + 273.15) ^ 4
                            '
                            Face_conduct(2) = .PanelConduction * (.PanelTemp - BodyTemp) * .PanelSurf
                            '
                        Case "- Y"
                            '
                            X = Math.Max(1 - .PanelSurf / Face_Surf(3), 0)
                            Face_SolarFlux(3) *= X
                            Face_AlbedoFlux(3) *= X
                            Face_IRFlux(3) *= X
                            '
                            Face_IRFlux(3) += .PanelSurf * .PanelBackEps * C_Stefan * (.PanelTemp + 273.15) ^ 4
                            '
                            Face_conduct(3) = .PanelConduction * (.PanelTemp - BodyTemp) * .PanelSurf
                            '                        
                        Case "+ Z"
                            '
                            X = Math.Max(1 - .PanelSurf / Face_Surf(4), 0)
                            Face_SolarFlux(4) *= X
                            Face_AlbedoFlux(4) *= X
                            Face_IRFlux(4) *= X
                            '
                            Face_IRFlux(4) += .PanelSurf * .PanelBackEps * C_Stefan * (.PanelTemp + 273.15) ^ 4
                            '
                            Face_conduct(4) = .PanelConduction * (.PanelTemp - BodyTemp) * .PanelSurf
                            '  
                        Case "- Z"
                            '
                            X = Math.Max(1 - .PanelSurf / Face_Surf(5), 0)
                            Face_SolarFlux(5) *= X
                            Face_AlbedoFlux(5) *= X
                            Face_IRFlux(5) *= X
                            '
                            Face_IRFlux(5) += .PanelSurf * .PanelBackEps * C_Stefan * (.PanelTemp + 273.15) ^ 4
                            '
                            Face_conduct(5) = .PanelConduction * (.PanelTemp - BodyTemp) * .PanelSurf
                            '                      
                    End Select
                End With
            End If
        Next
        '
        ' Global thermal balance of body
        ' ------------------------------
        X = 0
        ' Add each face global power
        For i = 0 To 5
            printOnConsoleIfBebugMode("Face_SolarFlux(" + i.ToString + ") is:" + Face_SolarFlux(i).ToString)
            printOnConsoleIfBebugMode("Face_AlbedoFlux(" + i.ToString + ") is:" + Face_AlbedoFlux(i).ToString)
            printOnConsoleIfBebugMode("Face_IRFlux(" + i.ToString + ") is:" + Face_IRFlux(i).ToString)
            printOnConsoleIfBebugMode("Face_conduct(" + i.ToString + ") is:" + Face_conduct(i).ToString)

            X += Face_SolarFlux(i) + Face_AlbedoFlux(i) + Face_IRFlux(i) + Face_conduct(i)
            printOnConsoleIfBebugMode("X only add is:" + X.ToString)
            ' Remove emitted Infrared flux which has not yet been taken into account
            printOnConsoleIfBebugMode("BodyTemp is:" + BodyTemp.ToString)
            printOnConsoleIfBebugMode("Face_Surf(" + i.ToString + ") is:" + Face_Surf(i).ToString)
            printOnConsoleIfBebugMode("BodyEps(" + i.ToString + ") is:" + BodyEps(i).ToString)

            X -= C_Stefan * (BodyTemp + 273.15) ^ 4 * Face_Surf(i) * BodyEps(i)
            printOnConsoleIfBebugMode("X only neg is:" + X.ToString)
        Next
        ' Add dissipated power
        printOnConsoleIfBebugMode("pdiss is:" + pdiss.ToString)
        X += pdiss
        printOnConsoleIfBebugMode("X pdiss:" + X.ToString)
        printOnConsoleIfBebugMode("X is:" + X.ToString)
        ' Compute temperature
        deltaT = X / BodySpecHeat * TimeStep * secday
        ' 
        printOnConsoleIfBebugMode("BodySpecHeat is:" + BodySpecHeat.ToString)
        printOnConsoleIfBebugMode("deltaT is:" + deltaT.ToString)

        BodyTemp += deltaT
        '
        ' ----------------------------
        ' Solar panels thermal balance
        ' ----------------------------
        For i = 1 To SolarPanels.Count()
            panel = SolarPanels.Item(i)
            ' Solar panels flux
            ' Note:
            ' 0 Cells face
            ' 1 Back face
            '
            With panel
                ' Cells face flux
                alf(0) = .PanelCellAlf
                eps(0) = .PanelCellEps
                printOnConsoleIfBebugMode(" panel is:" + panel.ToString)
                printOnConsoleIfBebugMode(" Time is:" + Time.ToString)
                fs1(0) = flux_incident_solar(panel, 0, Time)
                fs2(0) = flux_incident_albedo(panel, 0, Time)

                printOnConsoleIfBebugMode(" fs1(0)  is:" + fs1(0).ToString)
                printOnConsoleIfBebugMode(" fs2(0) is:" + fs2(0).ToString)
                fs(0) = fs1(0) + fs2(0)
                fir(0) = flux_incident_ir(panel, 0, Time)


                '
                ' Back face flux
                alf(1) = .PanelBackAlf
                eps(1) = .PanelBackEps
                fs(1) = flux_incident_solar(panel, pi, Time) + flux_incident_albedo(panel, pi, Time)
                fir(1) = flux_incident_ir(panel, pi, Time)
                '
                If .PanelType = "Type2" Then  ' If type "Type2" no external flux on the back face
                    fs(1) = 0
                    fir(1) = 0
                End If
                '
                ' Thermal balance in panel
                printOnConsoleIfBebugMode("Thermal balance in panel C_Stefan is:" + C_Stefan.ToString)
                printOnConsoleIfBebugMode("Thermal balance in panel .PanelSurf is:" + .PanelSurf.ToString)
                printOnConsoleIfBebugMode("Thermal balance in panel .PanelTemp is:" + .PanelTemp.ToString)

                printOnConsoleIfBebugMode("Thermal balance in panel alf(0) is:" + alf(0).ToString)
                printOnConsoleIfBebugMode("Thermal balance in panel fs(0) is:" + fs(0).ToString)
                printOnConsoleIfBebugMode("Thermal balance in panel  eps(0) is:" + eps(0).ToString)
                printOnConsoleIfBebugMode("Thermal balance in panel  fir(0) is:" + fir(0).ToString)

                printOnConsoleIfBebugMode("Thermal balance in panel alf(1) is:" + alf(1).ToString)
                printOnConsoleIfBebugMode("Thermal balance in panel fs(1) is:" + fs(1).ToString)
                printOnConsoleIfBebugMode("Thermal balance in panel  eps(1) is:" + eps(1).ToString)
                printOnConsoleIfBebugMode("Thermal balance in panel  fir(1) is:" + fir(1).ToString)
                X = (alf(0) * fs(0) + eps(0) * fir(0) + alf(1) * fs(1) + eps(1) * fir(1) _
                     - (eps(0) + eps(1)) * C_Stefan * (.PanelTemp + 273.15) ^ 4) _
                     * .PanelSurf
                '
                printOnConsoleIfBebugMode("Thermal balance in panel X is:" + X.ToString)
                If .PanelType = "Type2" Then
                    '
                    ' Add infrared flux coming from body
                    Select Case .PanelFace
                        Case "+ X"
                            X += BodyEps(0) * C_Stefan * (BodyTemp + 273.15) ^ 4 * .PanelSurf
                        Case "- X"
                            X += BodyEps(1) * C_Stefan * (BodyTemp + 273.15) ^ 4 * .PanelSurf
                        Case "+ Y"
                            X += BodyEps(2) * C_Stefan * (BodyTemp + 273.15) ^ 4 * .PanelSurf
                        Case "- Y"
                            X += BodyEps(3) * C_Stefan * (BodyTemp + 273.15) ^ 4 * .PanelSurf
                        Case "+ Z"
                            X += BodyEps(4) * C_Stefan * (BodyTemp + 273.15) ^ 4 * .PanelSurf
                        Case "- Z"
                            X += BodyEps(5) * C_Stefan * (BodyTemp + 273.15) ^ 4 * .PanelSurf
                    End Select
                    '
                    ' Add conduction with body

                    printOnConsoleIfBebugMode("Add conduction with body .PanelConduction is:" + .PanelConduction.ToString)
                    printOnConsoleIfBebugMode("Add conduction with body BodyTemp is:" + BodyTemp.ToString)
                    printOnConsoleIfBebugMode("Add conduction with body .PanelTemp is:" + .PanelTemp.ToString)
                    printOnConsoleIfBebugMode("Add conduction with body .PanelSurf is:" + .PanelSurf.ToString)
                    X += .PanelConduction * (BodyTemp - .PanelTemp) * .PanelSurf

                    printOnConsoleIfBebugMode("Add conduction with body X is:" + X.ToString)
                    '
                End If
                '
                printOnConsoleIfBebugMode(".PanelSpecHeat for panelTmp is:" + .PanelSpecHeat.ToString)
                printOnConsoleIfBebugMode("X for panelTmp is:" + X.ToString)
                printOnConsoleIfBebugMode("TimeStep for panelTmp is:" + TimeStep.ToString)
                printOnConsoleIfBebugMode("secday for panelTmp is:" + secday.ToString)
                deltaT = X / .PanelSpecHeat * TimeStep * secday
                'deltaT = (X - .PanelPower) / .PanelSpecHeat * TimeStep * secday
                '
                ' Current temperature in panel
                printOnConsoleIfBebugMode("deltaT for panelTmp is:" + deltaT.ToString)
                .PanelTemp += deltaT
                '
                .PanelSolarFlux = fs1(0)
                .PanelAlbedoFlux = fs2(0)
                .PanelIRFlux = fir(0)
            End With
        Next
        ''
    End Sub

    Public Function flux_incident_solar(ByVal panel As CSolarPanel, ByVal angle As Single, ByVal Time As Double) As Double
        '
        ' Return values of equivalent incident energie flux related to current
        ' sun and earth position without taking care of effect of sun angle.
        '
        ' Input :
        '   - panel
        '   - angle = 0 : front face of the panel
        '   - angle = pi : back face of the panel
        '
        ' A correction of equivalent incident flux must be carried out in order
        ' to take into acount the effect of sun angle on cells efficiency
        ' (only with faces containing solar cells, see flux_incident_overall_power Function)
        '
        Dim costetasun As Double
        '
        costetasun = Math.Cos(panel.SunIncidence(Time) + angle)
        printOnConsoleIfBebugMode("costetasun for flux_incident_solar is:" + costetasun.ToString)
        If blnEclipse Then
            costetasun = 0
        End If
        If costetasun > 0 Then
            flux_incident_solar = flux_solaire * costetasun
        Else
            flux_incident_solar = 0
        End If
        printOnConsoleIfBebugMode("flux_incident_solar for flux_incident_solar is:" + flux_incident_solar.ToString)
    End Function
    Public Function flux_incident_albedo(ByVal panel As CSolarPanel, ByVal angle As Single, ByVal Time As Double) As Double
        '
        ' angle = 0 : front face of the panel
        ' angle = pi : back face of the panel
        '
        Dim tetaearth As Double

        '
        printOnConsoleIfBebugMode("Time for flux_incident_solar is:" + Time.ToString)
        printOnConsoleIfBebugMode("angle for flux_incident_solar is:" + angle.ToString)
        tetaearth = panel.EarthIncidence(Time) + angle
        printOnConsoleIfBebugMode("tetaearth for flux_incident_solar is:" + tetaearth.ToString)
        flux_incident_albedo = flux_albedo * facteur_vue(tetaearth * 180 / pi)
        ''
    End Function
    Public Function flux_incident_ir(ByVal panel As CSolarPanel, ByVal angle As Single, ByVal Time As Double) As Double
        '
        ' angle = 0 : front face of the panel
        ' angle = pi : back face of the panel
        '
        Dim tetaearth As Double
        '
        printOnConsoleIfBebugMode(" panel.EarthIncidence(Time) is:" + panel.EarthIncidence(Time).ToString)
        printOnConsoleIfBebugMode(" flux_ir is:" + flux_ir.ToString)
        printOnConsoleIfBebugMode(" facteur_vue(tetaearth * 180 / pi) is:" + facteur_vue(tetaearth * 180 / pi).ToString)
        tetaearth = panel.EarthIncidence(Time) + angle
        printOnConsoleIfBebugMode(" tetaearth is:" + tetaearth.ToString)
        flux_incident_ir = flux_ir * facteur_vue(tetaearth * 180 / pi)
        ''
        printOnConsoleIfBebugMode(" flux_incident_ir is:" + flux_incident_ir.ToString)
    End Function

#Region "Configuration Factors"

    Public Function facteur_vue(ByVal a As Single) As Single
        '
        ' a : angle de la face % terre en degrés
        ' h : altitude en km
        '
        ' Ce facteur de vue est approximé analytiquement par la fonction :
        ' Fp/Fp0 = 1 - 0.0031*a° - 0.0668/90*ln(h)    pour a < 90°
        ' Fp/Fp0 = ((0.00005367*a^2 - 0.02345*a + 2.395) - ((-0.000002833*a^2 - 0.000215*a3 + 0.1091))*ln(h)
        '
        Dim fp0 As Single, R As Single
        Dim earth_angle_from_sat As Double
        '
        R = vctNorm(SatPos)
        earth_angle_from_sat = Math.Asin(RE / R)

        printOnConsoleIfBebugMode(" R is:" + R.ToString)
        printOnConsoleIfBebugMode(" earth_angle_from_sat is:" + earth_angle_from_sat.ToString)

        fp0 = (0.664 + 0.521 * earth_angle_from_sat - 0.203 * earth_angle_from_sat * earth_angle_from_sat) * Math.Sin(earth_angle_from_sat) * Math.Sin(earth_angle_from_sat)
        '
        printOnConsoleIfBebugMode(" fp0 is:" + fp0.ToString)
        printOnConsoleIfBebugMode(" RE is:" + RE.ToString)
        printOnConsoleIfBebugMode(" a is:" + a.ToString)
        printOnConsoleIfBebugMode("  Math.Log(R - RE) is:" + Math.Log(R - RE).ToString)

        If a < 90 Then
            facteur_vue = fp0 * (1 - 0.0031 * a - 0.0668 / 90 * a * Math.Log(R - RE))
            printOnConsoleIfBebugMode(" facteur_vue IF is:" + facteur_vue.ToString)
            If facteur_vue < 0.001 Then facteur_vue = 0
        Else
            facteur_vue = fp0 * ((0.00005367 * a ^ 2 - 0.02345 * a + 2.395) - (0.1091 - 0.000002833 * a ^ 2 - 0.000215 * a) * Math.Log(R - RE))
            printOnConsoleIfBebugMode(" facteur_vue ELSE is:" + facteur_vue.ToString)
            If facteur_vue < 0.001 Then facteur_vue = 0
        End If
        ''
        printOnConsoleIfBebugMode(" facteur_vue is:" + facteur_vue.ToString)
    End Function
#End Region
End Module