Option Strict Off
Option Explicit On

Imports VB = Microsoft.VisualBasic
Imports System.Math
Imports System.Net
Imports System.Text
Imports System.IO

Friend Class frmOrbit
    Inherits System.Windows.Forms.Form
    '
    Private intIDData As Short
    Private intFwdTrackSize, intBckTrackSize As Short
    Private period As Double
    Dim fin_transfert As Boolean
    Dim data_norad As String
    Private RAAN, i, a, e, w, Mu As Double
    Private epoch As Double
    Dim listFiles() As String
    Dim listSat() As String
    Dim indexChange As Boolean = True
    ' Create a new WebClient instance.
    Dim webClient1 As New WebClient()
    Dim url As String
    Dim databuffer As Byte()
    '
    Dim strSatName, strDefaultSatName As String
    '
    ' New or old satellite
    Private tempSat As CSatellite
    '
    ' Attitude
    ' --------
    ' all available axis
    Dim axis(5) As String
    ' all available orientations
    Dim orientation(7) As String
    ' axis of the main direction
    Dim prinAxis As String
    ' orientation of the main direction
    Dim prinOrient As String
    ' axis of the optimized direction
    Dim optAxis As String
    ' orientation of the optimized direction
    Dim optOrient As String
    '
    ' Onglet environnement
    ' --------------------
    Dim dsun(11) As Double
    Dim dmax As Double
    Dim dmin As Double

    Private Property cmdFwd As Object

    '
    '
    ''' <summary>
    ''' Update of the data and searching for the chosen satellite and its first orbit0
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub frmOrbit_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        '
        Dim CalendarEpoch As udtDate
        Dim X As String
        ReDim listFiles(40)
        ReDim listSat(40)
        Dim i As Short

        'disable events
        allow_event = False
        '
        ' Setting orbit0 data
        ' ------------------
        ' filling of the combo norad type satellite
        Dim tempfile As Integer
        tempfile = FreeFile()

        FileOpen(tempfile, My.Application.Info.DirectoryPath & "\Data\Norad\lst_norad.txt", OpenMode.Input)
        i = 0
        While Not EOF(tempfile)
            X = LineInput(tempfile)
            If X.Substring(0, 1) = " " Then
                cmbType_Sat.Items.Add(RTrim(X.Substring(0, 49)))
                listFiles(i) = RTrim(X.Substring(49))
                listSat(i) = RTrim(X.Substring(0, 49))
                i = i + 1
            Else
                cmbType_Sat.Items.Add(RTrim(X.Substring(0)))
            End If
        End While
        FileClose(tempfile)


        Call Update_OrbitWindow()

        ' Modify satellite
        txtName.Text = SatOrbit0.Name
        ColorBox.Color = System.Drawing.ColorTranslator.FromOle(Satellite.satColor)
        previewColor.BackColor = ColorBox.Color
        cmbFwdTrack.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
        cmbBckTrack.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)
        cmbFwdTrackCO.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
        cmbBckTrackCO.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)
        cmbFwdTrack.Visible = True
        cmbBckTrack.Visible = True
        cmbFwdTrackCO.Visible = False
        cmbBckTrackCO.Visible = False

        Satellite.satFootprint = False  ' Footptint checkbox initialised in uncheked

        If Satellite.satFootprint Then
            chkFootprint.CheckState = System.Windows.Forms.CheckState.Checked
        Else
            chkFootprint.CheckState = System.Windows.Forms.CheckState.Unchecked
        End If

        period = 2 * pi * (SatOrbit0.SemiMajor_Axis ^ 3 / myEarth) ^ 0.5
        period = period / 60 'in minutes

        cmbTime.SelectedIndex = 0
        txtEpochDate.Visible = True
        txtEpochTime.Visible = True
        txtNoradEpoch.Visible = False

        Call convert_Julian_Gregorian(SatOrbit0.epoch, CalendarEpoch)

        With CalendarEpoch
            txtEpochDate.Text = Format(.day, "00") & "-" & Format(.month, "00") & "-" & Format(.year, "0000")
            txtEpochTime.Text = Format(.hour, "00") & ":" & Format(.minute, "00") & ":" & Format(.second, "00")
        End With
        With CalendarEpoch
            .day = "01"
            .month = "01"
            .hour = "00"
            .minute = "00"
            .second = "00"
        End With
        txtNoradEpoch.Text = VB.Right(CStr(CalendarEpoch.year), 2) & Format(SatOrbit0.epoch - convert_Gregorian_Julian(CalendarEpoch) + 1, "000.00000000")

        txtA.Text = CStr(SatOrbit0.SemiMajor_Axis)
        txtE.Text = CStr(SatOrbit0.Eccentricity)
        txtI.Text = CStr(SatOrbit0.Inclination)
        txtW.Text = CStr(SatOrbit0.Argument_Perigee)
        txtRAAN.Text = CStr(SatOrbit0.RAAN)
        txtMu.Text = CStr(SatOrbit0.Mean_Anomaly)
        txtBstar.Text = CStr(SatOrbit0.bstar)


        cmbSemiMajor.Visible = True
        cmbSemiMajor.SelectedIndex = 0
        cmbEccentricity.Visible = True
        cmbEccentricity.SelectedIndex = 0
        cmbRAAN.Visible = True
        cmbRAAN.SelectedIndex = 0 ' "RAAN"
        cmbAnomaly.Visible = True
        cmbAnomaly.SelectedIndex = 0 ' "Mean Anomaly"
        cmbType_Sat.SelectedIndex = 0

        ' -------------------------
        ' Setting data for attitude
        ' -------------------------
        '
        ' providing the available axes
        axis(0) = "x"
        axis(1) = "y"
        axis(2) = "z"
        axis(3) = "-x"
        axis(4) = "-y"
        axis(5) = "-z"
        ' providing the available orientations
        orientation(0) = "nadir"
        orientation(1) = "sun-pointing"
        orientation(2) = "moon-pointing"
        orientation(3) = "velocity"
        orientation(4) = "inertial x"
        orientation(5) = "inertial y"
        orientation(6) = "inertial z"
        orientation(7) = "ground speed"
        '
        ' allowing everything for the axis of the principle and optimized direction
        For i = 0 To UBound(axis)
            cmbPrinAxis.Items.Add(axis(i))
            cmbOptAxis.Items.Add(axis(i))
        Next i
        ' allowing everything for the orientation of the principle and optimized direction
        For i = 0 To UBound(orientation)
            cmbPrinOrient.Items.Add(orientation(i))
            cmbOptOrient.Items.Add(orientation(i))
        Next i

        cmbPrinAxis.SelectedIndex = 5
        cmbPrinOrient.SelectedIndex = 0
        cmbOptAxis.SelectedIndex = 0
        cmbOptOrient.SelectedIndex = 3

        ' setting the data from the satellite (if already setted)
        If Satellite.primaryAxis <> "" Then
            prinAxis = Satellite.primaryAxis
            Select Case prinAxis
                Case "x" : cmbPrinAxis.SelectedIndex = 0
                Case "y" : cmbPrinAxis.SelectedIndex = 1
                Case "z" : cmbPrinAxis.SelectedIndex = 2
                Case "-x" : cmbPrinAxis.SelectedIndex = 3
                Case "-y" : cmbPrinAxis.SelectedIndex = 4
                Case "-z" : cmbPrinAxis.SelectedIndex = 5
            End Select
        End If
        If Satellite.primaryDir <> "" Then
            prinOrient = Satellite.primaryDir
            Select Case prinOrient
                Case "nadir" : cmbPrinOrient.SelectedIndex = 0
                Case "sun-pointing" : cmbPrinOrient.SelectedIndex = 1
                Case "moon-pointing" : cmbPrinOrient.SelectedIndex = 2
                Case "velocity" : cmbPrinOrient.SelectedIndex = 3
                Case "inertial x" : cmbPrinOrient.SelectedIndex = 4
                Case "inertial y" : cmbPrinOrient.SelectedIndex = 5
                Case "inertial z" : cmbPrinOrient.SelectedIndex = 6
                Case "ground speed" : cmbPrinOrient.SelectedIndex = 7
            End Select
        End If
        If Satellite.secondaryAxis <> "" Then
            optAxis = Satellite.secondaryAxis
            Select Case optAxis
                Case "x" : cmbOptAxis.SelectedIndex = 0
                Case "y" : cmbOptAxis.SelectedIndex = 1
                Case "z" : cmbOptAxis.SelectedIndex = 2
                Case "-x" : cmbOptAxis.SelectedIndex = 3
                Case "-y" : cmbOptAxis.SelectedIndex = 4
                Case "-z" : cmbOptAxis.SelectedIndex = 5
            End Select
        End If
        '
        If Satellite.secondaryDir <> "" Then
            optOrient = Satellite.secondaryDir
            Select Case optOrient
                Case "nadir" : cmbOptOrient.SelectedIndex = 0
                Case "sun-pointing" : cmbOptOrient.SelectedIndex = 1
                Case "moon-pointing" : cmbOptOrient.SelectedIndex = 2
                Case "velocity" : cmbOptOrient.SelectedIndex = 3
                Case "inertial x" : cmbOptOrient.SelectedIndex = 4
                Case "inertial y" : cmbOptOrient.SelectedIndex = 5
                Case "inertial z" : cmbOptOrient.SelectedIndex = 6
                Case "ground speed" : cmbOptOrient.SelectedIndex = 7
            End Select
        End If
        '
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        '
        '
        ' --------------------
        ' Onglet environnement
        ' --------------------
        '
        ' Gives values to 12 monthly average values of the Sun-Earth distance
        dsun(0) = 147136383.585747
        dsun(1) = 147734283.137167
        dsun(2) = 148774603.47896
        dsun(3) = 150094009.236035
        dsun(4) = 151234136.704803
        dsun(5) = 151964678.668876
        dsun(6) = 152063368.430357
        dsun(7) = 151511718.826698
        dsun(8) = 150436237.556895
        dsun(9) = 149163195.894725
        dsun(10) = 147961188.654898
        dsun(11) = 147231119.143355
        dmax = 152101686.718472
        dmin = 147090267.044645
        '
        Call env_update_texts()
        Call trace_environment()
        Call trace_CERES()
        '
        'enable events
        allow_event = True
    End Sub

    Public Sub Update_OrbitWindow()
        Dim tleNorad As String

        If optMain_0.Checked Then
            fraType_Orbit.Visible = True
            fraDatabase.Visible = False
            orbit_checked_loading()
        Else
            fraType_Orbit.Visible = False
            fraDatabase.Visible = True
        End If
        txtName.Text = SatOrbit0.Name
        '
        If optMain_0.Checked Then
            If optType_Orbit_1.Checked Then
                SatOrbit0.Inclination = 63.4
                txtI.ReadOnly = True
                lblI.Enabled = False
                SatOrbit0.Argument_Perigee = 270.0#
                txtW.ReadOnly = True
                lblW.Enabled = False
                SatOrbit0.epoch = 18500
                txtA.Text = CStr(500)
                txtE.Text = CStr(0.74096938)
                txtRAAN.Text = CStr(100)
                txtMu.Text = CStr(0)
            End If
            If optType_Orbit_7.Checked Then
                lblI.Enabled = True
                lblW.Enabled = True
                cmbSemiMajor.Visible = True
                cmbSemiMajor.SelectedIndex = 0
                cmbEccentricity.Visible = True
                cmbEccentricity.SelectedIndex = 0
                cmbRAAN.Visible = True
                cmbRAAN.SelectedIndex = 0 ' "RAAN"
                cmbAnomaly.Visible = True
                cmbAnomaly.SelectedIndex = 0 ' "Mean Anomaly"
            End If
        ElseIf optMain_1.Checked Then
            fraType_Orbit.Visible = False
            fraDatabase.Visible = True
            strSatName = lstSatellites.SelectedIndex.ToString
            tleNorad = Mid(data_norad, InStr(data_norad, strSatName), 166)
            Call norad_to_Orbit(tleNorad, SatOrbit0)
            txtName.Text = SatOrbit0.Name
        End If
        '
        Dim CalendarEpoch As udtDate
        Call convert_Julian_Gregorian(SatOrbit0.epoch, CalendarEpoch)
        With CalendarEpoch
            txtEpochDate.Text = Format(.day, "00") & "-" & Format(.month, "00") & "-" & Format(.year, "0000")
            txtEpochTime.Text = Format(.hour, "00") & ":" & Format(.minute, "00") & ":" & Format(.second, "00")
        End With
        txtNoradEpoch.Text = VB.Right(CStr(CalendarEpoch.year), 2) & Format(SatOrbit0.epoch - convert_Gregorian_Julian(CalendarEpoch) + 1, "000.00000000")
        '
        With SatOrbit0
            txtA.Text = Format(.SemiMajor_Axis, "#0.000")
            txtE.Text = Format(.Eccentricity, "#0.00000")
            txtI.Text = Format(.Inclination, "#0.000")
            txtRAAN.Text = Format(.RAAN, "#0.000")
            txtW.Text = Format(.Argument_Perigee, "#0.0000")
            txtMu.Text = Format(.Mean_Anomaly, "#0.000")
            txtBstar.Text = Format(.bstar, "#0.0000")
        End With
        '
        period = 2 * pi * (SatOrbit0.SemiMajor_Axis ^ 3 / myEarth) ^ 0.5
        period = period / 60 'in minutes

        ' Track size command
        cmbFwdTrack.Enabled = True
        cmbBckTrack.Enabled = True
        cmbFwdTrack.Visible = True
        cmbBckTrack.Visible = True
        cmbFwdTrackCO.Enabled = False
        cmbBckTrackCO.Enabled = False
        cmbFwdTrackCO.Visible = False
        cmbBckTrackCO.Visible = False

    End Sub

    ''' <summary>
    ''' Define shortcuts Open / Save / Enter
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmOrbit_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If tabOrbit.SelectedIndex = 0 Then
            If e.KeyData = (Keys.Control Or Keys.O) Then
                cmdOrbitOpen_Click(cmdOrbitOpen, New System.EventArgs)
                e.SuppressKeyPress = True ' Pas besoin que l'�v�nement KeyPress le g�re on vient de le faire
            ElseIf e.KeyData = (Keys.Control Or Keys.S) Then
                cmdOrbitSave_Click(cmdOrbitSave, New System.EventArgs)
                e.SuppressKeyPress = True ' Pas besoin que l'�v�nement KeyPress le g�re on vient de le faire
            ElseIf e.KeyData = Keys.Enter Then
                orbit_lblmsg.Focus()
                e.SuppressKeyPress = True ' Pas besoin que l'�v�nement KeyPress le g�re on vient de le faire
            End If
        End If
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOk.Click

        Me.Hide()
        System.Windows.Forms.Application.DoEvents()

        ' These lines are added to avoid problems in the simulation, particularly in circular orbits
        If optType_Orbit_7.Checked = True Then
            Satellite.satBTrackSize = 0
            Satellite.satFTrackSize = 0

            If SatOrbit0.Eccentricity = 0 And SatOrbit0.Inclination = 0 Then
                'Choice of dblTrackStep(frmDisplay.vb)
                testOrbit = True
                'Case forward track size
                If cmbFwdTrackCO.Text = "1 Orbit" Or cmbFwdTrack.Text = "1 Orbit" Then
                    Satellite.satFTrackSize = 4
                ElseIf cmbFwdTrackCO.Text = "1/2 Orbit" Or cmbFwdTrack.Text = "1/2 Orbit" Then
                    Satellite.satFTrackSize = 2
                ElseIf cmbFwdTrackCO.Text = "1/4 Orbit" Or cmbFwdTrack.Text = "1/4 Orbit" Then
                    Satellite.satFTrackSize = 1
                ElseIf cmbFwdTrackCO.Text = "None" Or cmbFwdTrack.Text = "None" Then
                    Satellite.satFTrackSize = 0
                End If
                'Case backforward track size
                If cmbBckTrackCO.Text = "1 Orbit" Or cmbBckTrack.Text = "1 Orbit" Then
                    Satellite.satBTrackSize = 4
                ElseIf cmbBckTrackCO.Text = "1/2 Orbit" Or cmbBckTrack.Text = "1/2 Orbit" Then
                    Satellite.satBTrackSize = 2
                ElseIf cmbBckTrackCO.Text = "1/4 Orbit" Or cmbBckTrack.Text = "1/4 Orbit" Then
                    Satellite.satBTrackSize = 1
                ElseIf cmbBckTrackCO.Text = "None" Or cmbBckTrack.Text = "None" Then
                    Satellite.satBTrackSize = 0
                End If

            Else
                'Choice of dblTrackStep(frmDisplay.vb)
                testOrbit = False
                'Case forward track size
                If cmbFwdTrackCO.Text = "1 Orbit" Or cmbFwdTrack.Text = "1 Orbit" Then
                    Satellite.satFTrackSize = 400
                ElseIf cmbFwdTrackCO.Text = "1/2 Orbit" Or cmbFwdTrack.Text = "1/2 Orbit" Then
                    Satellite.satFTrackSize = 200
                ElseIf cmbFwdTrackCO.Text = "1/4 Orbit" Or cmbFwdTrack.Text = "1/4 Orbit" Then
                    Satellite.satFTrackSize = 100
                ElseIf cmbFwdTrackCO.Text = "None" Or cmbFwdTrack.Text = "None" Then
                    Satellite.satFTrackSize = 0
                End If
                'Case backforward track size
                If cmbBckTrackCO.Text = "1 Orbit" Or cmbBckTrack.Text = "1 Orbit" Then
                    Satellite.satBTrackSize = 400
                ElseIf cmbBckTrackCO.Text = "1/2 Orbit" Or cmbBckTrack.Text = "1/2 Orbit" Then
                    Satellite.satBTrackSize = 200
                ElseIf cmbBckTrackCO.Text = "1/4 Orbit" Or cmbBckTrack.Text = "1/4 Orbit" Then
                    Satellite.satBTrackSize = 100
                ElseIf cmbBckTrackCO.Text = "None" Or cmbBckTrack.Text = "None" Then
                    Satellite.satBTrackSize = 0
                End If
            End If
        End If

    End Sub
    Private Sub FermetureForm_closing2(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        e.Cancel = True
    End Sub


#Region "Orbit selection"

    ''' <summary>
    ''' Fill orbit_checked with the right values : 1 at the right index, 0 at the other indexes
    ''' </summary>
    ''' <param name="i">right index</param>
    ''' <remarks></remarks>
    Private Sub orbit_checked_saving(ByRef i As Integer)
        orbit_checked = {0, 0, 0, 0, 0, 0, 0, 0}
        orbit_checked(i) = 1
    End Sub


    ''' <summary>
    ''' Check the right orbit according to data saved in orbit_checked
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub orbit_checked_loading()
        If orbit_checked(0) = 1 Then
            optType_Orbit_0.Checked = True
        ElseIf orbit_checked(1) = 1 Then
            optType_Orbit_1.Checked = True
        ElseIf orbit_checked(2) = 1 Then
            optType_Orbit_2.Checked = True
        ElseIf orbit_checked(3) = 1 Then
            optType_Orbit_3.Checked = True
        ElseIf orbit_checked(4) = 1 Then
            optType_Orbit_4.Checked = True
        ElseIf orbit_checked(5) = 1 Then
            optType_Orbit_5.Checked = True
        ElseIf orbit_checked(6) = 1 Then
            optType_Orbit_6.Checked = True
        ElseIf orbit_checked(7) = 1 Then
            optType_Orbit_7.Checked = True
        End If
    End Sub


    Private Sub select_orbit_type()
        '
        'SatOrbit0.epoch = dblSimulationStart
        'cmbTime.SelectedIndex = -1
        strSatName = strDefaultSatName
        txtA.Enabled = True
        txtE.Enabled = True
        txtI.Enabled = True
        txtRAAN.Enabled = True
        txtW.Enabled = True
        txtMu.Enabled = True

        If Not optType_Orbit_2.Checked Then
            lblSSAlt.Visible = False
            lblSSIncl.Visible = False
            txtSSAltitude.Visible = False
            txtSSInclination.Visible = False
            lblSSAlt.Visible = False
            lblSSIncl.Visible = False
        End If

        If cmdOrbitOpen.Focus() = False Then

        ElseIf optType_Orbit_0.Checked Then         ' Geostationnary
            Satellite.satBTrackSize = 0
            Satellite.satFTrackSize = 0
            lblDescription.Text = "A satellite in geostationary orbit will remain fixed in the sky above the specified longitude."
            SatOrbit0.SemiMajor_Axis = 42164
            SatOrbit0.Eccentricity = 0
            SatOrbit0.Inclination = 0
            SatOrbit0.Argument_Perigee = 0
            SatOrbit0.RAAN = 0
            SatOrbit0.Mean_Anomaly = 40
            SatOrbit.Name = "Geostationnary"
            txtA.Enabled = False
            txtE.Enabled = False
            txtI.Enabled = False
            txtW.Enabled = False
            txtMu.Enabled = False
            cmbSemiMajor.SelectedIndex = 0       ' "Semimajor axis"
            cmbEccentricity.SelectedIndex = 0    ' "Eccentricity"
            cmbAnomaly.SelectedIndex = 0         ' "Mean anomaly"
            ' To choice the track size
            testOrbit = False
            cmbFwdTrack.Enabled = True
            cmbBckTrack.Enabled = True
            cmbFwdTrack.Visible = True
            cmbBckTrack.Visible = True
            cmbFwdTrackCO.Enabled = False
            cmbBckTrackCO.Enabled = False
            cmbFwdTrackCO.Visible = False
            cmbBckTrackCO.Visible = False
            cmbFwdTrack.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbBckTrack.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)

        ElseIf optType_Orbit_1.Checked Then     ' Molniya
            testOrbit = False
            Satellite.satBTrackSize = 0
            Satellite.satFTrackSize = 0
            lblDescription.Text = "Molniya orbits are highly eccentric, meaning there is a large difference between the altitude at the apogee and the altitude at the perigee. They are also critically inclined: this keeps the perigee of the orbit in the southern hemisphere."
            SatOrbit0.SemiMajor_Axis = 26562
            SatOrbit0.Eccentricity = 0.74
            SatOrbit0.Inclination = 63.4
            SatOrbit0.Argument_Perigee = 270
            SatOrbit0.RAAN = 0
            SatOrbit0.Mean_Anomaly = 0
            SatOrbit.Name = "Molnyia"
            txtA.Enabled = False
            txtE.Enabled = False
            txtI.Enabled = False
            txtW.Enabled = False
            cmbSemiMajor.SelectedIndex = 0      ' "Semimajor axis"
            cmbEccentricity.SelectedIndex = 0   ' "Eccentricity"
            cmbAnomaly.SelectedIndex = 0        ' "Mean anomaly"
            ' To choice the track size
            cmbFwdTrack.Enabled = True
            cmbBckTrack.Enabled = True
            cmbFwdTrack.Visible = True
            cmbBckTrack.Visible = True
            cmbFwdTrackCO.Enabled = False
            cmbBckTrackCO.Enabled = False
            cmbFwdTrackCO.Visible = False
            cmbBckTrackCO.Visible = False
            cmbFwdTrack.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbBckTrack.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)

        ElseIf optType_Orbit_2.Checked Then     ' Sun synchronous
            testOrbit = False
            Satellite.satBTrackSize = 0
            Satellite.satFTrackSize = 0
            lblDescription.Text = "The effect of the oblateness of the Earth is used to cause the orbit plane to rotate at the same rate at which the Earth moves in orbit around the Sun. Thus, at the equator, the satellite passes overhead at the same local time for each revolution."
            lblSSAlt.Visible = True
            txtSSAltitude.Text = CStr(800)
            txtSSAltitude.Visible = True
            lblSSIncl.Visible = True
            txtSSInclination.Visible = True
            Call txtSSAltitude_Leave(txtSSAltitude, New System.EventArgs())
            SatOrbit0.Eccentricity = 0
            SatOrbit0.RAAN = 0
            SatOrbit0.Argument_Perigee = 0
            SatOrbit0.Mean_Anomaly = 0
            SatOrbit.Name = "Sun synchronus"
            txtA.Enabled = False
            txtE.Enabled = False
            txtI.Enabled = False
            cmbSemiMajor.SelectedIndex = 0      ' "Semimajor axis"
            cmbEccentricity.SelectedIndex = 0   ' "Eccentricity"
            ' To choice the track size
            cmbFwdTrack.Enabled = True
            cmbBckTrack.Enabled = True
            cmbFwdTrack.Visible = True
            cmbBckTrack.Visible = True
            cmbFwdTrackCO.Enabled = False
            cmbBckTrackCO.Enabled = False
            cmbFwdTrackCO.Visible = False
            cmbBckTrackCO.Visible = False
            cmbFwdTrack.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbBckTrack.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)

        ElseIf optType_Orbit_3.Checked Then     ' Critically inclined
            testOrbit = False
            Satellite.satBTrackSize = 0
            Satellite.satFTrackSize = 0
            lblDescription.Text = "These orbits have a perigee that remains at a fixed latitude. The line of apsides does not change over time"
            SatOrbit0.SemiMajor_Axis = RE + 1000
            SatOrbit0.Inclination = 63.4
            SatOrbit0.Eccentricity = 0
            SatOrbit0.RAAN = 0
            SatOrbit0.Argument_Perigee = 0
            SatOrbit0.Mean_Anomaly = 0
            SatOrbit.Name = "Critically inclined"
            txtI.Enabled = False
            cmbSemiMajor.SelectedIndex = 0      ' "Semimajor axis"
            cmbEccentricity.SelectedIndex = 0   ' "Eccentricity"
            ' To choice the track size
            cmbFwdTrack.Enabled = True
            cmbBckTrack.Enabled = True
            cmbFwdTrack.Visible = True
            cmbBckTrack.Visible = True
            cmbFwdTrackCO.Enabled = False
            cmbBckTrackCO.Enabled = False
            cmbFwdTrackCO.Visible = False
            cmbBckTrackCO.Visible = False
            cmbFwdTrack.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbBckTrack.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)

        ElseIf optType_Orbit_4.Checked Then      ' Cr. inclined + sun synchronous
            testOrbit = False
            Satellite.satBTrackSize = 0
            Satellite.satFTrackSize = 0
            lblDescription.Text = "This orbit combines the properties of both orbits. It has a retrograde inclination of 116.565 deg"
            SatOrbit0.Inclination = 116.565
            SatOrbit0.Eccentricity = 0
            SatOrbit0.RAAN = 0
            SatOrbit0.Argument_Perigee = 0
            SatOrbit0.Mean_Anomaly = 0
            SatOrbit.Name = "Critically inclined & Sun synchronus"
            txtSSInclination.Text = SatOrbit0.Inclination
            Call txtSSInclination_Leave(txtSSInclination, New System.EventArgs())
            txtA.Enabled = False
            txtE.Enabled = False
            txtI.Enabled = False
            cmbSemiMajor.SelectedIndex = 0      ' "Semimajor axis"
            cmbEccentricity.SelectedIndex = 0   ' "Eccentricity"
            ' To choice the track size
            cmbFwdTrack.Enabled = True
            cmbBckTrack.Enabled = True
            cmbFwdTrack.Visible = True
            cmbBckTrack.Visible = True
            cmbFwdTrackCO.Enabled = False
            cmbBckTrackCO.Enabled = False
            cmbFwdTrackCO.Visible = False
            cmbBckTrackCO.Visible = False
            cmbFwdTrack.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbBckTrack.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)

        ElseIf optType_Orbit_5.Checked Then    ' Repeating ground trace
            testOrbit = False
            Satellite.satBTrackSize = 0
            Satellite.satFTrackSize = 0
            lblDescription.Text = "These orbits repeat the ground coverage cycle. They are useful when identical viewing conditions are desired at different times, to detect changes. The ground trace may be caused to repeat every day or to interweave from day to day before repeating."
            ' No code yet
            txtA.Enabled = False
            txtE.Enabled = False
            txtI.Enabled = False
            txtRAAN.Enabled = False
            txtW.Enabled = False
            txtMu.Enabled = False
            txtBstar.Enabled = False
            cmbSemiMajor.SelectedIndex = 0      ' "Semimajor axis"
            cmbEccentricity.SelectedIndex = 0   ' "Eccentricity"
            cmbRAAN.SelectedIndex = 0           ' "RAAN"
            cmbAnomaly.SelectedIndex = 0        ' "Mean anomaly"
            ' To choice the track size
            cmbFwdTrack.Enabled = True
            cmbBckTrack.Enabled = True
            cmbFwdTrack.Visible = True
            cmbBckTrack.Visible = True
            cmbFwdTrackCO.Enabled = False
            cmbBckTrackCO.Enabled = False
            cmbFwdTrackCO.Visible = False
            cmbBckTrackCO.Visible = False
            cmbFwdTrack.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbBckTrack.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)

        ElseIf optType_Orbit_6.Checked Then     ' Circular
            testOrbit = True
            Satellite.satBTrackSize = 0
            Satellite.satFTrackSize = 0
            lblDescription.Text = "Circular orbits have a constant radius"
            SatOrbit0.SemiMajor_Axis = RE + 1000
            SatOrbit0.Eccentricity = 0
            SatOrbit0.Inclination = 0
            SatOrbit0.RAAN = 0
            SatOrbit0.Argument_Perigee = 0
            SatOrbit0.Mean_Anomaly = 0
            SatOrbit.Name = "Circular"
            txtE.Enabled = False
            cmbSemiMajor.SelectedIndex = 0      ' "Semimajor axis"
            cmbEccentricity.SelectedIndex = 0   ' "Eccentricity"
            ' To choice the track size
            cmbFwdTrack.Enabled = False
            cmbBckTrack.Enabled = False
            cmbFwdTrack.Visible = False
            cmbBckTrack.Visible = False
            cmbFwdTrackCO.Enabled = True
            cmbBckTrackCO.Enabled = True
            cmbFwdTrackCO.Visible = True
            cmbBckTrackCO.Visible = True
            cmbFwdTrackCO.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbBckTrackCO.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)

        ElseIf optType_Orbit_7.Checked Then     ' Other
            Satellite.satBTrackSize = 0
            Satellite.satFTrackSize = 0
            lblDescription.Text = "Choose your own parameters"
            SatOrbit.Name = "Other"
            cmbSemiMajor.SelectedIndex = 0      ' "Semimajor axis"
            cmbEccentricity.SelectedIndex = 0   ' "Eccentricity"
            cmbRAAN.SelectedIndex = 0           ' "RAAN"
            cmbAnomaly.SelectedIndex = 0        ' "Mean anomaly"
            SatOrbit0.Inclination = 30
            ' To choice the track size
            cmbFwdTrack.Enabled = True
            cmbBckTrack.Enabled = True
            cmbFwdTrack.Visible = True
            cmbBckTrack.Visible = True
            cmbFwdTrackCO.Enabled = False
            cmbBckTrackCO.Enabled = False
            cmbFwdTrackCO.Visible = False
            cmbBckTrackCO.Visible = False
            cmbFwdTrack.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbBckTrack.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbFwdTrackCO.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbBckTrackCO.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)

        End If

        txtA.Text = Format(SatOrbit0.SemiMajor_Axis, "#0.000")
        txtE.Text = Format(SatOrbit0.Eccentricity, "#0.000")
        txtI.Text = Format(SatOrbit0.Inclination, "#0.0000")
        txtRAAN.Text = Format(SatOrbit0.RAAN, "#0.0000")
        txtW.Text = Format(SatOrbit0.Argument_Perigee, "#0.0000")
        txtMu.Text = Format(SatOrbit0.Mean_Anomaly, "#0.0000")



    End Sub

    Private Sub optType_Orbit_0_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_0.CheckedChanged
        If optType_Orbit_0.Checked Then orbit_checked_saving(0)
        Call select_orbit_type()
    End Sub
    Private Sub optType_Orbit_1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_1.CheckedChanged
        If optType_Orbit_1.Checked Then orbit_checked_saving(1)
        Call select_orbit_type()
    End Sub
    Private Sub optType_Orbit_2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_2.CheckedChanged
        If optType_Orbit_2.Checked Then orbit_checked_saving(2)
        Call select_orbit_type()
    End Sub
    Private Sub optType_Orbit_3_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_3.CheckedChanged
        If optType_Orbit_3.Checked Then orbit_checked_saving(3)
        Call select_orbit_type()
    End Sub
    Private Sub optType_Orbit_4_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_4.CheckedChanged
        If optType_Orbit_4.Checked Then orbit_checked_saving(4)
        Call select_orbit_type()
    End Sub
    Private Sub optType_Orbit_5_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_5.CheckedChanged
        If optType_Orbit_5.Checked Then orbit_checked_saving(5)
        Call select_orbit_type()
    End Sub
    Private Sub optType_Orbit_6_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_6.CheckedChanged
        If optType_Orbit_6.Checked Then orbit_checked_saving(6)
        Call select_orbit_type()
    End Sub
    Private Sub optType_Orbit_7_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_7.CheckedChanged
        If optType_Orbit_7.Checked Then orbit_checked_saving(7)
        Call select_orbit_type()
    End Sub

#End Region

#Region "Orbit buttons"

    Private Sub txtName_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtName.Leave
        SatOrbit0.Name = Trim(txtName.Text)
    End Sub


    Private Sub cmdOrbitSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOrbitSave.Click
        '
        Dim file As String
        Dim xs As String
        Dim j As Integer
        '
        On Error GoTo errorsave
        dlgOrbitSave.FileName = txtName.Text
        If dlgOrbitSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
        file = dlgOrbitSave.FileName
        If My.Computer.FileSystem.FileExists(file) Then Kill(file)
        On Error GoTo 0
        '
        txtName.Text = Path.GetFileNameWithoutExtension(file)
        SatOrbit0.Name = Path.GetFileNameWithoutExtension(file)
		
        FileOpen(1, file, OpenMode.Output)
        '
        With SatOrbit0
            PrintLine(1, "! ----------")
            PrintLine(1, "! Orbit Data")
            PrintLine(1, "! ----------")
            PrintLine(1, "!")
            PrintLine(1, .Name, TAB(24), "Orbit name")
            PrintLine(1, .epoch, TAB(24), "Epoch")
            PrintLine(1, .SemiMajor_Axis, TAB(24), "SemiMajor axis")
            PrintLine(1, .Eccentricity, TAB(24), "Eccentricity")
            PrintLine(1, .Inclination, TAB(24), "Inclination")
            PrintLine(1, .RAAN, TAB(24), "Right ascension of ascending node")
            PrintLine(1, .Argument_Perigee, TAB(24), "Perigee argument")
            PrintLine(1, .Mean_Anomaly, TAB(24), "Mean anomaly")
            PrintLine(1, .bstar, TAB(24), "Bstar drag term")
            PrintLine(1, .dec1, TAB(24), "First Time Derivative of Mean Motion")
            PrintLine(1, .dec2, TAB(24), "Second Time Derivative of Mean Motion")
            PrintLine(1, "!")
        End With
        '
        With Satellite
            PrintLine(1, "! -------------")
            PrintLine(1, "! Attitude data")
            PrintLine(1, "! -------------")
            PrintLine(1, "!")
            PrintLine(1, .primaryAxis, TAB(24), "Primary axis")
            PrintLine(1, .primaryDir, TAB(24), "Primary direction")
            PrintLine(1, .secondaryAxis, TAB(24), "Secondary axis axis")
            PrintLine(1, .secondaryDir, TAB(24), "Secondary direction")
        End With
        '
        PrintLine(1, "! ----------------")
        PrintLine(1, "! Environment data")
        PrintLine(1, "! ----------------")
        PrintLine(1, "!")
        PrintLine(1, "! Albedo")
        For j = 0 To 11 : PrintLine(1, Albedo_month(j)) : Next
        PrintLine(1, Alb_Ce_check.Checked, TAB(24), "CERES data used")
        PrintLine(1, "! Earth infrared flux")
        For j = 0 To 11 : PrintLine(1, IR_month(j)) : Next
        PrintLine(1, Earth_Ce_check.Checked, TAB(24), "CERES data used")
        PrintLine(1, "! Sun Power")
        PrintLine(1, Format$(SunPow, "0.0000E+00"), TAB(24), "Solar flux")

        FileClose(1)
        '
        Exit Sub
        '
errorsave:
        xs = MsgBox("Writing error" & Chr(13) _
        & " Saving data is not possible !!", vbCritical)
        ''
    End Sub

    Private Sub cmdOrbitOpen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOrbitOpen.Click

        '
        Dim file As String
        Dim s$ = ""
        Dim j As Integer
        '
        'On Error Resume Next
        If dlgOrbitOpen.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
        file = dlgOrbitOpen.FileName
        FileOpen(1, file, OpenMode.Input)
        '
        'On Error GoTo 0
        '
        s$ = (read_line())                      ' "! ----------"
        s$ = (read_line())                      ' "! Orbit Data"
        s$ = (read_line())                      ' "! ----------"
        s$ = (read_line())                      ' "!"
        With SatOrbit0
            .Name = Trim(read_line())
            .epoch = (read_line())
            .SemiMajor_Axis = (read_line())
            .Eccentricity = (read_line())
            .Inclination = (read_line())
            .RAAN = (read_line())
            .Argument_Perigee = (read_line())
            .Mean_Anomaly = (read_line())
            .bstar = (read_line())
            .dec1 = (read_line())
            .dec2 = (read_line())
        End With
        '
        '
        ' Update simulation start date
        ' ----------------------------
        Dim date_temp As udtDate
        convert_Julian_Gregorian(SatOrbit0.epoch, date_temp)
        With date_temp
            frmMain.txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                 & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        dblSimulationStart = SatOrbit0.epoch
        dblSimulationTime = SatOrbit0.epoch
        update_CurrentDate(dblSimulationTime)
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! -------------"
        s$ = (read_line())                      ' "! Attitude data"
        s$ = (read_line())                      ' "! -------------"
        s$ = (read_line())                      ' "!"
        With Satellite
            .primaryAxis = Trim(read_line())
            .primaryDir = Trim(read_line())
            .secondaryAxis = Trim(read_line())
            .secondaryDir = Trim(read_line())
            '
            ' Setting values for attitude
            ' ----------------------------
            ' If no attitude is set, set attitude default values
            If .primaryAxis = "" Then
                .primaryAxis = "-z"
                .primaryDir = "nadir"
                .secondaryAxis = "x"
                .secondaryDir = "velocity"
            End If
            '
            ' Principal direction
            cmbPrinAxis.Text = .primaryAxis
            cmbPrinOrient.Text = .primaryDir
            ' Optimized direction
            cmbOptAxis.Text = .secondaryAxis
            cmbOptOrient.Text = .secondaryDir
        End With
        '
        Try
            s$ = (read_line())                      ' "! -------------"
            s$ = (read_line())                      ' "! Environnement"
            s$ = (read_line())                      ' "! -------------"
            s$ = (read_line())                      ' "! "
            s$ = (read_line())                      ' "! Albedo"
            For j = 0 To 11 : Albedo_month(j) = (read_line()) : Next
            Alb_Ce_check.Checked = Trim(read_line())
            s$ = (read_line())                      ' "! Earth infrared flux"
            For j = 0 To 11 : IR_month(j) = (read_line()) : Next
            Earth_Ce_check.Checked = Trim(read_line())
            s$ = (read_line())                      ' "! Sun Power"
            SunPow = (read_line())                  ' "Solar flux"
            '
        Catch ex As EndOfStreamException
            MsgBox("You are opening an older Simusat version file." & vbCrLf & "Orbit data will be charged correctly but enviromental data will remain as it was before opening the file.", MsgBoxStyle.Exclamation, "Warning!")
        Finally
            FileClose(1)
        End Try
        '
        ' Update orbit
        Update_OrbitWindow()
        ' Update attitude
        If Satellite.primaryAxis <> "" Then
            prinAxis = Satellite.primaryAxis
            Select Case prinAxis
                Case "x" : cmbPrinAxis.SelectedIndex = 0
                Case "y" : cmbPrinAxis.SelectedIndex = 1
                Case "z" : cmbPrinAxis.SelectedIndex = 2
                Case "-x" : cmbPrinAxis.SelectedIndex = 3
                Case "-y" : cmbPrinAxis.SelectedIndex = 4
                Case "-z" : cmbPrinAxis.SelectedIndex = 5
            End Select
        End If
        If Satellite.primaryDir <> "" Then
            prinOrient = Satellite.primaryDir
            Select Case prinOrient
                Case "nadir" : cmbPrinOrient.SelectedIndex = 0
                Case "sun-pointing" : cmbPrinOrient.SelectedIndex = 1
                Case "moon-pointing" : cmbPrinOrient.SelectedIndex = 2
                Case "velocity" : cmbPrinOrient.SelectedIndex = 3
                Case "inertial x" : cmbPrinOrient.SelectedIndex = 4
                Case "inertial y" : cmbPrinOrient.SelectedIndex = 5
                Case "inertial z" : cmbPrinOrient.SelectedIndex = 6
                Case "ground speed" : cmbPrinOrient.SelectedIndex = 7
            End Select
        End If
        If Satellite.secondaryAxis <> "" Then
            optAxis = Satellite.secondaryAxis
            Select Case optAxis
                Case "x" : cmbOptAxis.SelectedIndex = 0
                Case "y" : cmbOptAxis.SelectedIndex = 1
                Case "z" : cmbOptAxis.SelectedIndex = 2
                Case "-x" : cmbOptAxis.SelectedIndex = 3
                Case "-y" : cmbOptAxis.SelectedIndex = 4
                Case "-z" : cmbOptAxis.SelectedIndex = 5
            End Select
        End If
        '
        If Satellite.secondaryDir <> "" Then
            optOrient = Satellite.secondaryDir
            Select Case optOrient
                Case "nadir" : cmbOptOrient.SelectedIndex = 0
                Case "sun-pointing" : cmbOptOrient.SelectedIndex = 1
                Case "moon-pointing" : cmbOptOrient.SelectedIndex = 2
                Case "velocity" : cmbOptOrient.SelectedIndex = 3
                Case "inertial x" : cmbOptOrient.SelectedIndex = 4
                Case "inertial y" : cmbOptOrient.SelectedIndex = 5
                Case "inertial z" : cmbOptOrient.SelectedIndex = 6
                Case "ground speed" : cmbOptOrient.SelectedIndex = 7
            End Select
        End If
        '
        'Update environment
        Call env_update_texts()
        Call trace_environment()
        Call trace_CERES()
        ''
    End Sub

    Private Sub optMain_0_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optMain_0.CheckedChanged

        If optMain_0.Checked Then
            fraType_Orbit.Visible = True
            fraDatabase.Visible = False
            Call select_orbit_type()
        ElseIf optMain_1.Checked Then
            Satellite.satBTrackSize = 0
            Satellite.satFTrackSize = 0
            fraDatabase.Visible = True
            fraType_Orbit.Visible = False
            strSatName = strDefaultSatName
            txtA.Enabled = True
            txtE.Enabled = True
            txtI.Enabled = True
            txtRAAN.Enabled = True
            txtW.Enabled = True
            txtMu.Enabled = True
            cmbFwdTrack.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbBckTrack.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbFwdTrackCO.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbBckTrackCO.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)
        End If
    End Sub

    Private Sub cmbType_Sat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbType_Sat.SelectedIndexChanged
        If allow_event Then
            Dim strType_Sat As String = ""
            Dim Line As String
            Dim strSat_Name As String 'VB6.FixedLengthString(24)
            Dim k As Short
            Dim j As Integer

            If cmbType_Sat.Text.Substring(0, 1) <> " " Then
                cmbType_Sat.SelectedIndex = cmbType_Sat.SelectedIndex + 1 ' cmbType_Sat.list(cmbType_Sat.listindex + 1)
            End If
            k = 0
            While listFiles(k) <> Nothing
                If listSat(k) = cmbType_Sat.Text Then
                    strType_Sat = listFiles(k)
                    Exit While
                End If
                k += 1
            End While
            data_norad = ""
            FileOpen(1, My.Application.Info.DirectoryPath & "\Data\Norad\" & strType_Sat, OpenMode.Input)
            Do While Not EOF(1)
                Line = LineInput(1)
                If Line.Substring(0, 1) = Chr(34) Then Line = Mid(Line, 2)
                data_norad = data_norad & Line & vbCrLf
            Loop
            FileClose(1)

            lstSatellites.Items.Clear()
            j = 1
            Do While j <= Len(data_norad) - 166
                strSat_Name = Mid(data_norad, j, 24)
                lstSatellites.Items.Add(strSat_Name)
                j = j + 168
            Loop
            lstSatellites.SetSelected(0, True)
        End If
    End Sub

    Private Sub cmdUpdateNORAD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdateNORAD.Click
        Dim strFile, strUrl As String
        Dim Index As Short

        strUrl = "http://www.celestrak.com/NORAD/elements/"
        ProgressBar1.Value = 0
        ProgressBar1.Maximum = UBound(listFiles)
        ProgressBar1.Visible = True
        etat_connex.Text = "Downloading..."
        For Index = 0 To UBound(listFiles)
            If listFiles(Index) <> Nothing Then
                strFile = listFiles(Index)
                url = strUrl & strFile 'Inet1.url = strUrl & strFile

                ' Download the datas from NORAD.
                databuffer = webClient1.DownloadData(url)
                ' Convert downloaded data into string
                data_norad = Encoding.ASCII.GetString(databuffer)

                fin_transfert = False
                FileOpen(1, My.Application.Info.DirectoryPath & "\Data\Norad\" & strFile, OpenMode.Output)
                WriteLine(1, data_norad)
                FileClose(1)
                ProgressBar1.Value = Index
            End If
        Next
        ProgressBar1.Visible = False
        etat_connex.Text = "Completed !"
        Call cmbType_Sat_SelectedIndexChanged(cmbType_Sat, New System.EventArgs())
    End Sub

    Private Sub lstSatellites_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSatellites.SelectedIndexChanged
        If allow_event Then
            Dim tleNorad As String

            strSatName = lstSatellites.SelectedItem
            txtName.Text = SatOrbit0.Name
            tleNorad = Mid(data_norad, InStr(data_norad, strSatName), 166)
            txtNORAD.Text = tleNorad
            Call norad_to_Orbit(tleNorad, SatOrbit0)
            ' Epoch display
            Dim CalendarEpoch As udtDate
            Call convert_Julian_Gregorian(SatOrbit0.epoch, CalendarEpoch)
            With CalendarEpoch
                txtEpochDate.Text = Format(.day, "00") & "-" & Format(.month, "00") & "-" & Format(.year, "0000")
                txtEpochTime.Text = Format(.hour, "00") & ":" & Format(.minute, "00") & ":" & Format(.second, "00")
            End With
            txtNoradEpoch.Text = VB.Right(CStr(CalendarEpoch.year), 2) & Format(SatOrbit0.epoch - convert_Gregorian_Julian(CalendarEpoch) + 1, "000.00000000")
            cmbSemiMajor.SelectedIndex = 4
            txtA.Text = Format(minday / get_period(SatOrbit0), "#0.00000000")
            cmbEccentricity.SelectedIndex = 0
            txtE.Text = Format(SatOrbit0.Eccentricity, "#0.000000")
            cmbRAAN.SelectedIndex = 0 '"RAAN"
            txtRAAN.Text = Format(SatOrbit0.RAAN, "#0.0000")
            cmbAnomaly.SelectedIndex = 0 '"Mean Anomaly"
            txtMu.Text = Format(SatOrbit0.Mean_Anomaly, "#0.0000")
            txtI.Text = Format(SatOrbit0.Inclination, "#0.0000")
            txtW.Text = Format(SatOrbit0.Argument_Perigee, "#0.0000")
            txtBstar.Text = Format(SatOrbit0.bstar, "#0.000000")
        End If
        Satellite.satBTrackSize = 0
        Satellite.satFTrackSize = 0

        If SatOrbit0.Eccentricity = 0 And SatOrbit0.Inclination = 0 Then
            cmbFwdTrack.Enabled = False
            cmbBckTrack.Enabled = False
            cmbFwdTrack.Visible = False
            cmbBckTrack.Visible = False
            cmbFwdTrackCO.Enabled = True
            cmbBckTrackCO.Enabled = True
            cmbFwdTrackCO.Visible = True
            cmbBckTrackCO.Visible = True
            cmbFwdTrackCO.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbBckTrackCO.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)
        Else
            cmbFwdTrack.Enabled = True
            cmbBckTrack.Enabled = True
            cmbFwdTrack.Visible = True
            cmbBckTrack.Visible = True
            cmbFwdTrackCO.Enabled = False
            cmbBckTrackCO.Enabled = False
            cmbFwdTrackCO.Visible = False
            cmbBckTrackCO.Visible = False
            cmbFwdTrack.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
            cmbBckTrack.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)
        End If


    End Sub

    Private Sub txtNORAD_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNORAD.Leave
        Call norad_to_Orbit(Trim(txtNORAD.Text), SatOrbit0)
        cmbSemiMajor.SelectedIndex = 4
        cmbEccentricity.SelectedIndex = 0
        cmbRAAN.SelectedIndex = 0 '"RAAN"
        cmbAnomaly.SelectedIndex = 0 '"Mean Anomaly"
        txtI.Text = Format(SatOrbit0.Inclination, "#0.0000")
        txtW.Text = Format(SatOrbit0.Argument_Perigee, "#0.0000")
        txtBstar.Text = Format(SatOrbit0.bstar, "#0.000000")
    End Sub

#End Region

#Region "Orbit parameters"

    Private Sub txtA_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtA.Leave
        Dim X As Double
        If IsNumeric(txtA.Text) Then
            X = CDbl(txtA.Text)
            With cmbSemiMajor
                If .Text = "Semimajor Axis" Then
                    X = value_limiter2(X, RE + 100, 1.0E+69)
                    SatOrbit0.SemiMajor_Axis = X
                ElseIf .Text = "Apogee Radius" Then
                    X = value_limiter2(X, RE + 100, 1.0E+69)
                    SatOrbit0.SemiMajor_Axis = (X + CDbl(txtE.Text)) / 2
                    SatOrbit0.Eccentricity = (X - CDbl(txtE.Text)) / 2 / SatOrbit0.SemiMajor_Axis
                ElseIf .Text = "Apogee Altitude" Then
                    X = value_limiter2(X, 100, 1.0E+69)
                    SatOrbit0.SemiMajor_Axis = (X + CDbl(txtE.Text) + 2 * RE) / 2
                    SatOrbit0.Eccentricity = (X - CDbl(txtE.Text)) / 2 / SatOrbit0.SemiMajor_Axis
                ElseIf .Text = "Period" Then
                    X = value_limiter2(X, 85, 1.0E+69)
                    SatOrbit0.SemiMajor_Axis = (myEarth * ((X * 60) / (2 * pi)) ^ 2) ^ (1 / 3)
                ElseIf .Text = "Mean Motion" Then
                    X = value_limiter2(X, 0, 16.93)
                    SatOrbit0.SemiMajor_Axis = (myEarth * ((86400 / X) / (2 * pi)) ^ 2) ^ (1 / 3)
                End If
                If .Text = "Mean Motion" Then
                    txtA.Text = Format(X, "#0.00000000")
                Else
                    txtA.Text = Format(X, "#0.000")
                End If
            End With
        Else
            MsgBox("Please enter a number !")
            txtA.Text = Format(SatOrbit0.SemiMajor_Axis, "#0.000")
            txtA.Focus()
        End If
    End Sub
    Private Sub txtE_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtE.Leave
        Dim X As Double
        If IsNumeric(txtE.Text) Then
            X = CDbl(txtE.Text)
            If X = 0 Then X = 0.0000001
            With cmbEccentricity
                Select Case .Text
                    Case "Eccentricity"
                        ' [ZB] changed upper limit, since a 1 makes "/ (1 - Orbit.Eccentricity)" crash, of course
                        X = value_limiter2(X, 0, 0.9)
                        txtE.Text = Format(X, "#0.0000000")
                        SatOrbit0.Eccentricity = X
                        If (SatOrbit0.SemiMajor_Axis * (1 - SatOrbit0.Eccentricity)) < RE Then
                            SatOrbit0.SemiMajor_Axis = (RE + 100) / (1 - SatOrbit0.Eccentricity)
                            txtA.Text = Format(SatOrbit0.SemiMajor_Axis, "#0.000")
                        End If
                    Case "Perigee Radius"
                        X = value_limiter2(X, RE + 100, 1.0E+69)
                        SatOrbit0.SemiMajor_Axis = (X + CDbl(txtA.Text)) / 2
                        SatOrbit0.Eccentricity = (CDbl(txtA.Text) - X) / 2 / SatOrbit0.SemiMajor_Axis

                    Case "Perigee Altitude"
                        X = value_limiter2(X, 100, 1.0E+69)
                        SatOrbit0.SemiMajor_Axis = (X + CDbl(txtA.Text) + 2 * RE) / 2
                        SatOrbit0.Eccentricity = (CDbl(txtA.Text) - X) / 2 / SatOrbit0.SemiMajor_Axis
                End Select
            End With
        Else
            MsgBox("Please enter a number !")
            txtE.Text = Format(SatOrbit0.Eccentricity, "#0.0000")
            txtE.Focus()
        End If
    End Sub
    Private Sub txtI_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtI.Leave
        Dim X As Double
        '
        If IsNumeric(txtI.Text) Then
            X = CDbl(txtI.Text)
            X = value_limiter2(X, 0, 180 - 0.0001)
            txtI.Text = Format(X, "#0.0000")
            SatOrbit0.Inclination = X
        Else
            MsgBox("Please enter a number !")
            txtI.Text = Format(SatOrbit0.Inclination, "#0.0000")
            txtI.Focus()
        End If
    End Sub
    Private Sub txtRAAN_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtRAAN.Leave
        Dim X As Double
        If IsNumeric(txtRAAN.Text) Then
            X = CDbl(txtRAAN.Text)
            Select Case cmbRAAN.Text
                Case "RAAN"
                    X = value_limiter2(X, -180, 360)
                    txtRAAN.Text = Format(X, "#0.0000")
                    SatOrbit0.RAAN = X
                Case "Longitude of Asc. Node"
                    X = value_limiter2(X, -180, 360)
                    txtRAAN.Text = Format(X, "#0.0000")
                    SatOrbit0.RAAN = X + temps_sideral(SatOrbit0.epoch) * deg
            End Select
        Else
            MsgBox("Please enter a number !")
            txtRAAN.Text = Format(SatOrbit0.RAAN, "#0.0000")
            txtRAAN.Focus()
        End If
    End Sub
    Private Sub txtW_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtW.Leave
        Dim X As Double
        '
        If IsNumeric(txtW.Text) Then
            X = CDbl(txtW.Text)
            X = value_limiter2(X, -180, 360)
            txtW.Text = Format(X, "#0.0000")
            SatOrbit0.Argument_Perigee = X
        Else
            MsgBox("Please enter a number !")
            txtW.Text = Format(SatOrbit0.Argument_Perigee, "#0.0000")
            txtW.Focus()
        End If
    End Sub
    Private Sub txtMu_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMu.Leave
        Dim Mu, X, ZZ As Double 'Y
        If IsNumeric(txtMu.Text) Then
            X = CDbl(txtMu.Text)
            Select Case cmbAnomaly.Text
                Case "Mean Anomaly"
                    X = value_limiter2(X, -180, 360)
                    SatOrbit0.Mean_Anomaly = X
                    txtMu.Text = Format(X, "#0.0000")
                Case "True Anomaly"
                    X = value_limiter2(X, -180, 360)
                    ZZ = trueAnom_to_eccentricAnom(X * rad, SatOrbit0.Eccentricity)
                    Mu = ZZ - SatOrbit0.Eccentricity * Sin(ZZ)
                    SatOrbit0.Mean_Anomaly = Mu * deg
                    txtMu.Text = Format(X, "#0.0000")
                Case "Eccentric Anomaly"
                    X = value_limiter2(X, -180, 360)
                    Mu = X * rad - SatOrbit0.Eccentricity * Sin(X * rad)
                    SatOrbit0.Mean_Anomaly = Mu * deg
                    txtMu.Text = Format(X, "#0.0000")
            End Select
        Else
            MsgBox("Please enter a number !")
            txtMu.Text = Format(SatOrbit0.Mean_Anomaly, "#0.0000")
            txtMu.Focus()
        End If
    End Sub
    Private Sub txtBstar_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBstar.Leave
        Dim X As Double
        '
        If IsNumeric(txtBstar.Text) Then
            X = CDbl(txtBstar.Text)
            X = value_limiter2(X, 0, 1)
            txtBstar.Text = Format(X, "#0.00000")
            SatOrbit0.bstar = X
        Else
            MsgBox("Please enter a number !")
            txtBstar.Text = Format(SatOrbit0.bstar, "#0.00000")
            txtBstar.Focus()
        End If
    End Sub

    Private Sub cmbTime_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTime.SelectedIndexChanged
        If allow_event Then
            Dim CalendarEpoch As udtDate
            Select Case cmbTime.Text
                Case "Calendar"
                    txtEpochDate.Visible = True
                    txtEpochTime.Visible = True
                    txtNoradEpoch.Visible = False
                    Call convert_Julian_Gregorian(SatOrbit0.epoch, CalendarEpoch)
                    With CalendarEpoch
                        txtEpochDate.Text = Format(.day, "00") & "-" & Format(.month, "00") & "-" & Format(.year, "0000")
                        txtEpochTime.Text = Format(.hour, "00") & ":" & Format(.minute, "00") & ":" & Format(.second, "00")
                    End With
                Case "NORAD epoch"
                    txtEpochDate.Visible = False
                    txtEpochTime.Visible = False
                    txtNoradEpoch.Visible = True
                    Call convert_Julian_Gregorian(SatOrbit0.epoch, CalendarEpoch)
                    With CalendarEpoch
                        .day = "01"
                        .month = "01"
                        .hour = "00"
                        .minute = "00"
                        .second = "00"
                    End With
                    txtNoradEpoch.Text = VB.Right(CStr(CalendarEpoch.year), 2) & Format(SatOrbit0.epoch - convert_Gregorian_Julian(CalendarEpoch) + 1, "000.00000000")
            End Select
        End If
    End Sub

    Private Sub txtEpochDate_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEpochDate.Leave
        Dim date_convertie As udtDate

        If Not IsDate(txtEpochDate.Text) Then txtEpochDate.Focus() : Exit Sub
        If Not IsDate(txtEpochTime.Text) Then txtEpochTime.Focus() : Exit Sub

        With date_convertie
            .day = Mid(CStr(DateValue(txtEpochDate.Text)), 1, 2)
            .month = Mid(CStr(DateValue(txtEpochDate.Text)), 4, 2)
            .year = Mid(CStr(DateValue(txtEpochDate.Text)), 7, 4)
            .hour = Mid(CStr(TimeValue(txtEpochTime.Text)), 1, 2)
            .minute = Mid(CStr(TimeValue(txtEpochTime.Text)), 4, 2)
            .second = Mid(CStr(TimeValue(txtEpochTime.Text)), 7, 2)
        End With
        SatOrbit0.epoch = convert_Gregorian_Julian(date_convertie)
        '
        ' Update simulation start date
        ' ----------------------------
        Dim date_temp As udtDate
        convert_Julian_Gregorian(SatOrbit0.epoch, date_temp)
        With date_temp
            frmMain.txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        dblSimulationStart = SatOrbit0.epoch
        dblSimulationTime = SatOrbit0.epoch
        update_CurrentDate(dblSimulationTime)
        update_frm_dates()
        '
    End Sub
    Private Sub txtEpochTime_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEpochTime.Leave
        Dim date_convertie As udtDate

        If Not IsDate(txtEpochDate.Text) Then txtEpochDate.Focus() : Exit Sub
        If Not IsDate(txtEpochTime.Text) Then txtEpochTime.Focus() : Exit Sub
        With date_convertie
            .day = Mid(CStr(DateValue(txtEpochDate.Text)), 1, 2)
            .month = Mid(CStr(DateValue(txtEpochDate.Text)), 4, 2)
            .year = Mid(CStr(DateValue(txtEpochDate.Text)), 7, 4)
            .hour = Mid(CStr(TimeValue(txtEpochTime.Text)), 1, 2)
            .minute = Mid(CStr(TimeValue(txtEpochTime.Text)), 4, 2)
            .second = Mid(CStr(TimeValue(txtEpochTime.Text)), 7, 2)
        End With
        SatOrbit0.epoch = convert_Gregorian_Julian(date_convertie)
        '
        ' Update simulation start date
        ' ----------------------------
        Dim date_temp As udtDate
        convert_Julian_Gregorian(SatOrbit0.epoch, date_temp)
        With date_temp
            frmMain.txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        dblSimulationStart = SatOrbit0.epoch
        dblSimulationTime = SatOrbit0.epoch
        update_CurrentDate(dblSimulationTime)
        update_frm_dates()
        '
    End Sub

    Private Sub txtNoradEpoch_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNoradEpoch.Leave
        '
        Dim X As String
        Dim an As Short
        X = Trim(txtNoradEpoch.Text)
        If IsNumeric(X) Then
            an = CShort(VB.Left(X, 2))
            If an > 50 Then
                an = 1900 + an
            Else
                an = 2000 + an
            End If
            '
            SatOrbit0.epoch = 365.25 * 4 * ((an - 1900) \ 4) - 18264
            If (an - 1900) Mod 4 <> 0 Then
                SatOrbit0.epoch = SatOrbit0.epoch + (365 * ((an - 1900) Mod 4)) + 1
            End If

            SatOrbit0.epoch = SatOrbit0.epoch + CDbl(VB.Right(X, Len(X) - 2)) + 2433282.5
        Else
            MsgBox("Please enter a number !")
            txtNoradEpoch.Text = Format(SatOrbit0.epoch, "#0.00000000")
            txtNoradEpoch.Focus()
        End If
        '
        ' Update simulation start date
        ' ----------------------------
        Dim date_temp As udtDate
        convert_Julian_Gregorian(SatOrbit0.epoch, date_temp)
        With date_temp
            frmMain.txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        dblSimulationStart = SatOrbit0.epoch
        dblSimulationTime = SatOrbit0.epoch
        update_CurrentDate(dblSimulationTime)
        update_frm_dates()
        '
    End Sub

    Private Sub cmbSemiMajor_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbSemiMajor.SelectedIndexChanged
        If allow_event Then
            Select Case cmbSemiMajor.Text
                Case "Apogee Radius"
                    If Not cmbEccentricity.Text = "Perigee Radius" Then
                        cmbEccentricity.SelectedIndex = 1
                        txtA.Text = Format(get_apogeeRadius(SatOrbit0), "#0.000")
                        lblAUnits.Text = "km"
                        txtE.Text = Format(get_perigeeRadius(SatOrbit0), "#0.000")
                        lblEUnits.Text = "km"
                    End If
                Case "Apogee Altitude"
                    If Not cmbEccentricity.Text = "Perigee Altitude" Then
                        cmbEccentricity.SelectedIndex = 2
                        txtA.Text = Format(get_apogeeAlt(SatOrbit0), "#0.000")
                        lblAUnits.Text = "km"
                        txtE.Text = Format(get_perigeeAlt(SatOrbit0), "#0.000")
                        lblEUnits.Text = "km"
                    End If
                Case "Period"
                    If Not cmbEccentricity.Text = "Eccentricity" Then
                        cmbEccentricity.SelectedIndex = 0
                        txtE.Text = Format(SatOrbit0.Eccentricity, "#0.000000")
                        lblEUnits.Text = ""
                    End If
                    txtA.Text = Format(get_period(SatOrbit0), "#0.00")
                    lblAUnits.Text = "min"
                Case "Mean Motion"
                    If Not cmbEccentricity.Text = "Eccentricity" Then
                        cmbEccentricity.SelectedIndex = 0
                        txtE.Text = Format(SatOrbit0.Eccentricity, "#0.000000")
                        lblEUnits.Text = ""
                    End If
                    txtA.Text = Format(minday / get_period(SatOrbit0), "#0.00000000")
                    lblAUnits.Text = "rev/day"
                Case Else
                    If Not cmbEccentricity.Text = "Eccentricity" Then
                        cmbEccentricity.SelectedIndex = 0
                        txtE.Text = Format(SatOrbit0.Eccentricity, "#0.000000")
                        lblEUnits.Text = ""
                    End If
                    txtA.Text = Format(SatOrbit0.SemiMajor_Axis, "#0.000")
                    lblAUnits.Text = "km"
            End Select
        End If
    End Sub

    Private Sub cmbEccentricity_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEccentricity.SelectedIndexChanged
        If allow_event Then
            Select Case cmbEccentricity.Text
                Case "Perigee Radius"
                    If Not cmbSemiMajor.Text = "Apogee Radius" Then
                        cmbSemiMajor.SelectedIndex = 1
                        txtA.Text = Format(get_apogeeRadius(SatOrbit0), "#0.000")
                        lblAUnits.Text = "km"
                        txtE.Text = Format(get_perigeeRadius(SatOrbit0), "#0.000")
                        lblEUnits.Text = "km"
                    End If
                Case "Perigee Altitude"
                    If Not cmbSemiMajor.Text = "Apogee Altitude" Then
                        cmbSemiMajor.SelectedIndex = 2
                        txtA.Text = Format(get_apogeeAlt(SatOrbit0), "#0.000")
                        lblAUnits.Text = "km"
                        txtE.Text = Format(get_perigeeAlt(SatOrbit0), "#0.000")
                        lblEUnits.Text = "km"
                    End If
                Case Else
                    If ((cmbSemiMajor.Text = "Apogee Radius") Or (cmbSemiMajor.Text = "Apogee Altitude")) Then
                        cmbSemiMajor.SelectedIndex = 0
                        txtA.Text = Format(SatOrbit0.SemiMajor_Axis, "#0.000")
                        lblAUnits.Text = "km"
                        txtE.Text = Format(SatOrbit0.Eccentricity, "#0.0000000")
                        lblEUnits.Text = ""
                    End If
                    txtE.Text = Format(SatOrbit0.Eccentricity, "#0.0000000")
                    lblEUnits.Text = ""
            End Select
        End If
    End Sub

    Private Sub cmbRAAN_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbRAAN.SelectedIndexChanged
        If allow_event Then
            Select Case cmbRAAN.Text
                Case "RAAN"
                    txtRAAN.Text = Format(SatOrbit0.RAAN, "#0.0000")
                Case "Longitude of Asc. Node"
                    txtRAAN.Text = Format(SatOrbit0.RAAN - temps_sideral(SatOrbit0.epoch) * deg, "#0.0000")
            End Select
        End If
    End Sub

    Private Sub cmbAnomaly_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbAnomaly.SelectedIndexChanged
        If allow_event Then
            Select Case cmbAnomaly.Text
                Case "Mean Anomaly"
                    txtMu.Text = Format(SatOrbit0.Mean_Anomaly, "#0.0000")
                Case "True Anomaly"
                    txtMu.Text = Format(get_trueAnom(SatOrbit0), "#0.0000")
                Case "Eccentric Anomaly"
                    txtMu.Text = Format(get_eccentricAnom(SatOrbit0), "#0.0000")
            End Select
        End If
    End Sub

    Private Sub txtSSAltitude_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSSAltitude.Leave
        Dim cosi, sem, inclin As Double
        '
        If IsNumeric(txtSSAltitude.Text) Then
            If CDbl(txtSSAltitude.Text) > 5970 Then
                MsgBox("5970 km is the maximum possible altitude for a circular sun-synchronous orbit", MsgBoxStyle.OkOnly, "Entry error")
                txtSSAltitude.Focus()
                Exit Sub
            End If
            '
            sem = RE + CDbl(txtSSAltitude.Text)
            cosi = -(sem ^ 3.5) * 0.000000000000004773624117289
            inclin = Math.Acos(cosi) * deg
            txtSSInclination.Text = Format(inclin, "#0.0000")
            SatOrbit0.SemiMajor_Axis = sem
            SatOrbit0.Inclination = inclin
            '
            txtA.Text = Format(SatOrbit0.SemiMajor_Axis, "#0.000")
            txtI.Text = Format(SatOrbit0.Inclination, "#0.0000")
        Else
            MsgBox("Please enter a number !")
            txtSSAltitude.Focus()
        End If
    End Sub
    Private Sub txtSSInclination_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSSInclination.Leave
        Dim cosi, sem, inclin As Double
        '
        If IsNumeric(txtSSInclination.Text) Then
            inclin = CDbl(txtSSInclination.Text)
            If inclin < 96 Then
                MsgBox("96 deg is the mininum possible inclination for a circular sun-synchronous orbit", MsgBoxStyle.OkOnly, "Entry error")
                txtSSInclination.Focus()
                Exit Sub
            End If
            '
            cosi = Cos(inclin * rad)
            sem = (-cosi * 209484445241100.0#) ^ (2 / 7)
            txtSSAltitude.Text = Format(sem - RE, "#0.000")
            SatOrbit0.SemiMajor_Axis = sem
            SatOrbit0.Inclination = inclin
            '
            txtA.Text = Format(SatOrbit0.SemiMajor_Axis, "#0.000")
            txtI.Text = Format(SatOrbit0.Inclination, "#0.0000")
        Else
            MsgBox("Please enter a number !")
            txtSSInclination.Focus()
        End If
    End Sub

#End Region

#Region "Attitude parameters"

    Private Sub cmbPrinAxis_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPrinAxis.SelectedIndexChanged
        If allow_event And indexChange Then
            prinAxis = cmbPrinAxis.Text
            ComboBoxAxisChanges(cmbPrinAxis, cmbOptAxis)
            indexChange = True
            Satellite.primaryAxis = prinAxis
            Satellite.secondaryAxis = cmbOptAxis.Text
        End If
    End Sub

    Private Sub cmbPrinOrient_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPrinOrient.SelectedIndexChanged
        If allow_event And indexChange Then
            ' The orientation of the primary direction has been changed
            prinOrient = cmbPrinOrient.Text
            ComboBoxOrientChanges(cmbPrinOrient, cmbOptOrient)
            indexChange = True
            Satellite.primaryDir = prinOrient
            Satellite.secondaryDir = cmbOptOrient.Text
        End If
    End Sub

    Private Sub cmbOptAxis_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbOptAxis.SelectedIndexChanged
        If allow_event And indexChange Then
            ' The axis of the optimized direction has been changed
            optAxis = cmbOptAxis.Text
            ComboBoxAxisChanges(cmbOptAxis, cmbPrinAxis)
            indexChange = True
            Satellite.secondaryAxis = optAxis
            Satellite.primaryAxis = cmbPrinAxis.Text
        End If
    End Sub

    Private Sub cmbOptOrient_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbOptOrient.SelectedIndexChanged
        If allow_event And indexChange Then
            '
            optOrient = cmbOptOrient.Text
            ' clearing the old value
            ComboBoxOrientChanges(cmbOptOrient, cmbPrinOrient)
            indexChange = True
            Satellite.secondaryDir = optOrient
            Satellite.primaryDir = cmbPrinOrient.Text
        End If
    End Sub

    Private Sub ComboBoxAxisChanges(ByRef cmbDepend As System.Windows.Forms.ComboBox, ByRef cmbChange As System.Windows.Forms.ComboBox)
        Dim i As Short
        Dim tmpaxis, index As Integer
        indexChange = False

        For i = 0 To UBound(axis)
            ' searching for the axis in the combobox
            If cmbDepend.Text = axis(i) Then
                If i >= 3 Then
                    tmpaxis = i - 3
                Else
                    tmpaxis = i
                End If
            End If
            If cmbChange.Text = axis(i) Then
                index = i
            End If
        Next i
        cmbChange.Items.Clear()
        For i = 0 To UBound(axis)
            If tmpaxis <> i And tmpaxis + 3 <> i Then
                cmbChange.Items.Add(axis(i))
            End If
        Next i
        If index <> tmpaxis And index <> tmpaxis + 3 Then
            cmbChange.Text = axis(index)
        Else
            cmbChange.SelectedIndex = 0
        End If
    End Sub

    Private Sub ComboBoxOrientChanges(ByRef cmbDepend As System.Windows.Forms.ComboBox, ByRef cmbChange As System.Windows.Forms.ComboBox)
        Dim i As Short
        Dim tmporient, index As Integer
        indexChange = False

        For i = 0 To UBound(orientation)
            ' searching for the axis in the combobox
            If cmbDepend.Text = orientation(i) Then
                tmporient = i
            End If
            If cmbChange.Text = orientation(i) Then
                index = i
            End If
        Next i
        cmbChange.Items.Clear()
        For i = 0 To UBound(orientation)
            If tmporient <> i Then
                cmbChange.Items.Add(orientation(i))
            End If
        Next i
        If index <> tmporient Then
            cmbChange.Text = orientation(index)
        Else
            cmbChange.SelectedIndex = 0
        End If
    End Sub

#End Region

#Region "Graphics parameters"

    Private Sub ChangeColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChangeColor.Click
        If ColorBox.ShowDialog() = DialogResult.OK Then
            previewColor.BackColor = ColorBox.Color
        End If
        ' Set the satellite color
        Satellite.satColor = System.Drawing.ColorTranslator.ToOle(ColorBox.Color)
    End Sub

    Private Sub cmbFwdTrack_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFwdTrack.SelectedIndexChanged, cmbBckTrack.SelectedIndexChanged

        ' determine the forward tracksize
        Select Case cmbFwdTrack.Text
            Case "1 Orbit" : Satellite.satFTrackSize = 400
            Case "1/2 Orbit" : Satellite.satFTrackSize = 200
            Case "1/4 Orbit" : Satellite.satFTrackSize = 100
            Case "None" : Satellite.satFTrackSize = 0
        End Select

        ' determine the backward tracksize
        Select Case cmbBckTrack.Text
            Case "1 Orbit" : Satellite.satBTrackSize = 400
            Case "1/2 Orbit" : Satellite.satBTrackSize = 200
            Case "1/4 Orbit" : Satellite.satBTrackSize = 100
            Case "None" : Satellite.satBTrackSize = 0
        End Select


    End Sub
    Private Sub cmbFwdTrackCO_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbFwdTrackCO.SelectedIndexChanged, cmbBckTrackCO.SelectedIndexChanged
        ' determine the forward tracksize
        Select Case cmbFwdTrackCO.Text
            Case "1 Orbit" : Satellite.satFTrackSize = 4
            Case "1/2 Orbit" : Satellite.satFTrackSize = 2
            Case "1/4 Orbit" : Satellite.satFTrackSize = 1
            Case "None" : Satellite.satFTrackSize = 0
        End Select

        ' determine the backward tracksize
        Select Case cmbBckTrackCO.Text
            Case "1 Orbit" : Satellite.satBTrackSize = 4
            Case "1/2 Orbit" : Satellite.satBTrackSize = 2
            Case "1/4 Orbit" : Satellite.satBTrackSize = 1
            Case "None" : Satellite.satBTrackSize = 0
        End Select
    End Sub


    Private Sub chkfootprint_change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFootprint.CheckStateChanged
        '
        ' Set footprint
        If chkFootprint.CheckState = 1 Then
            Satellite.satFootprint = True
        Else
            Satellite.satFootprint = False
        End If
        '
    End Sub

#End Region

#Region "Environment TAB"

    Private Sub env_txt_flux_0_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles env_txt_flux_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(env_txt_flux_0.Text) Then
            X = CDbl(env_txt_flux_0.Text)
            X = value_down2(X, 0, False)
            SunPow = X * 4 * pi * dmin * dmin * 1000000.0#
            env_txt_flux_0.Text = Format(X, "#0.00")
            env_txt_flux_1.Text = Format(solar_flux(dmax), "0.00")
            Call trace_environment()
            'If solar_flux(dsun(i)) > ChartArea1.AxisY.Maximum Then
            '    ChartArea1.AxisY.Maximum = Round(solar_flux(dmin))
            'End If
        Else
            MsgBox("Please enter a number !")
            X = SunPow / (4 * pi * dmin * dmin) / 1000000.0#
            env_txt_flux_0.Text = Format(X, "#0.00")
            env_txt_flux_0.Focus()
        End If
        ''
    End Sub
    Private Sub env_txt_flux_1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles env_txt_flux_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(env_txt_flux_1.Text) Then
            X = CDbl(env_txt_flux_1.Text)
            X = value_down2(X, 0, False)
            SunPow = X * 4 * pi * dmax * dmax * 1000000.0#
            env_txt_flux_1.Text = Format(X, "#0.00")
            env_txt_flux_0.Text = Format(solar_flux(dmin), "0.00")
            Call trace_environment()
            'If solar_flux(dsun(i)) > ChartArea1.AxisY.Maximum Then
            '    ChartArea1.AxisY.Maximum = Round(solar_flux(dmin))
            'End If
        Else
            MsgBox("Please enter a number !")
            X = SunPow / (4 * pi * dmax * dmax) / 1000000.0#
            env_txt_flux_1.Text = Format(X, "#0.00")
            env_txt_flux_1.Focus()
        End If
        ''
    End Sub

    Private Sub env_list_months_alb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles env_list_months_alb.SelectedIndexChanged
        env_txt_albedo.Text = Albedo_month(env_list_months_alb.SelectedIndex)
    End Sub
    Private Sub env_txt_albedo_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles env_txt_albedo.Leave
        '
        Dim i As Integer = env_list_months_alb.SelectedIndex
        Dim X As Double
        '
        If IsNumeric(env_txt_albedo.Text) Then
            X = CDbl(env_txt_albedo.Text)
            X = value_limiter2(X, 0, 1)
            Albedo_month(i) = CSng(X)
            env_txt_albedo.Text = Format(Albedo_month(i), "#0.00")
            Call trace_environment()
        Else
            MsgBox("Please enter a number !")
            env_txt_albedo.Text = Format(Albedo_month(i), "#0.00")
            env_txt_albedo.Focus()
        End If
        ''
    End Sub

    Private Sub env_cmd_sin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles env_cmd_sin.Click
        '
        ' Gives values to the monthly average values of albedo so as to trace a trigonometric law.
        '
        Dim i As Integer
        Dim i0 As Integer = env_list_months_alb.SelectedIndex
        Dim mean As Single
        Dim var As Single
        Dim X As Single
        '
        If IsNumeric(env_txt_var.Text) Then
            X = CDbl(env_txt_var.Text)
            mean = CDbl(env_txt_albedo.Text)
            If mean > 0.5 Then
                var = value_up2(Math.Abs(X), 1 - mean, True)
            Else
                var = value_up2(Math.Abs(X), mean, True)
            End If
            env_txt_var.Text = Format(var, "#0.00")
            For i = 0 To 11
                Albedo_month(i) = Format(mean + var * Sin(30 * rad * (i - i0)), "#0.00")
            Next i
            Call trace_environment()
        Else
            MsgBox("Please enter a number !")
            env_txt_var.Text = Format(0, "#0.00")
            env_txt_var.Focus()
        End If
        ''
    End Sub

    Private Sub Alb_Ce_check_CheckedChanged(sender As Object, e As EventArgs) Handles Alb_Ce_check.CheckedChanged
        If Alb_Ce_check.Checked Then
            env_list_months_alb.Enabled = False
            env_cmd_sin.Enabled = False
            env_txt_albedo.Enabled = False
            env_txt_var.Enabled = False
            Alb_Cer_bln = True
            Call trace_environment()
            Call trace_CERES()
        ElseIf Not Alb_Ce_check.Checked Then
            env_list_months_alb.Enabled = True
            env_cmd_sin.Enabled = True
            env_txt_albedo.Enabled = True
            env_txt_var.Enabled = True
            Alb_Cer_bln = False
            Call trace_environment()
            Call trace_CERES()
        End If

    End Sub

    Private Sub env_list_months_ir_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles env_list_months_ir.SelectedIndexChanged
        env_txt_ir.Text = IR_month(env_list_months_ir.SelectedIndex)
    End Sub
    Private Sub env_txt_ir_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles env_txt_ir.Leave
        '
        Dim i As Integer = env_list_months_ir.SelectedIndex
        Dim X As Double
        '
        If IsNumeric(env_txt_ir.Text) Then
            X = CDbl(env_txt_ir.Text)
            X = value_down2(X, 0, True)
            IR_month(i) = CSng(X)
            env_txt_ir.Text = Format(IR_month(i), "#0.00")
            Call trace_environment()
        Else
            MsgBox("Please enter a number !")
            env_txt_ir.Text = Format(IR_month(i), "#0.00")
            env_txt_ir.Focus()
        End If
        ''
    End Sub

    Private Sub env_cmd_isoterre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles env_cmd_isoterre.Click
        '
        ' This command button gives values to the monthly average values of intensity using
        ' the isothermal-sphere model for the Earth
        '
        Dim i As Integer
        '
        For i = 0 To 11
            If Alb_Cer_bln Then
                IR_month(i) = Format(solar_flux(dsun(i)) * (1 - AvgCERES(AlbedoCoefFile, i + 1)) / 4, "0.00")
            Else
                IR_month(i) = Format(solar_flux(dsun(i)) * (1 - Albedo_month(i)) / 4, "0.00")
            End If
        Next i
        env_txt_ir.Text = IR_month(env_list_months_ir.SelectedIndex)
        Call trace_environment()
        ''
    End Sub

    Private Sub Earth_Ce_check_CheckedChanged(sender As Object, e As EventArgs) Handles Earth_Ce_check.CheckedChanged
        If Earth_Ce_check.Checked Then
            env_list_months_ir.Enabled = False
            env_cmd_isoterre.Enabled = False
            env_txt_albedo.Enabled = False
            env_txt_ir.Enabled = False
            EarthIR_Cer_bln = True
            Call trace_environment()
            Call trace_CERES()
        ElseIf Not Earth_Ce_check.Checked Then
            env_list_months_ir.Enabled = True
            env_cmd_isoterre.Enabled = True
            env_txt_ir.Enabled = True
            EarthIR_Cer_bln = False
            Call trace_environment()
            Call trace_CERES()
        End If
    End Sub

    Private Sub CERES_month_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CERES_month.SelectedIndexChanged
        Call trace_CERES()
    End Sub

    Private Sub optMain_1_CheckedChanged(sender As Object, e As EventArgs) Handles optMain_1.CheckedChanged

    End Sub

#End Region

    Private Function TrackName(ByRef tracksize As Short, ByRef a As Double) As String
        'experimental
        TrackName = ""
        Select Case tracksize
            Case 0
                TrackName = "None"
            Case 12
                TrackName = "1/4 Orbit"
            Case 25
                TrackName = "1/2 Orbit"
            Case 50
                TrackName = "1 Orbit"
        End Select
    End Function

    Public Sub env_update_texts()
        '
        ' Updates text widgets with global variables
        env_list_months_alb.SelectedIndex = 0
        env_list_months_ir.SelectedIndex = 0
        env_txt_flux_0.Text = Format(solar_flux(dmin), "0.00")
        env_txt_flux_1.Text = Format(solar_flux(dmax), "0.00")
        env_txt_albedo.Text = Albedo_month(0)
        env_txt_ir.Text = IR_month(0)
        ''
    End Sub

    Private Sub trace_environment()
        ' Dessins des graphes
        Dim tabXM As ArrayList
        Dim tabYM1 As ArrayList
        Dim tabYM2 As ArrayList
        Dim tabYM3 As ArrayList
        Dim bCustomLabel As Boolean
        '
        Dim i As Integer
        Dim X As Double
        '
        '    Drawing graph
        '    -------------
        tabXM = New ArrayList
        tabYM1 = New ArrayList
        tabYM2 = New ArrayList
        tabYM3 = New ArrayList
        '
        ' Trac� des courbes
        '
        For i = 0 To 11
            X = i + 0.5
            '
            tabXM.Add(X)
            '
            tabYM1.Add(solar_flux(dsun(i)))
            '
            If Alb_Cer_bln Then
                tabYM2.Add(AvgCERES(AlbedoCoefFile, i + 1))
            Else
                tabYM2.Add(Albedo_month(i))
            End If
            '
            If EarthIR_Cer_bln Then
                tabYM3.Add(flux_rad(AvgCERES(EarthTempFile, i + 1) - 273.15))
            Else
                tabYM3.Add(IR_month(i))
            End If
            '

        Next i
        '
        With env_graph
            '
            .Series.Clear()
            .Series.Add("Solar")
            .Series("Solar").Points.DataBindXY(tabXM, tabYM1)
            .Series("Solar").ChartType = DataVisualization.Charting.SeriesChartType.Line
            .Series("Solar").YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary
            .Series("Solar").ShadowOffset = 1
            .Series("Solar").BorderWidth = 2
            .Series("Solar").Color = Color.Yellow
            .Series("Solar").ToolTip = "Solar = #VALY{N3}  W/m�"
            '
            .Series.Add("Albedo Coef.")
            .Series("Albedo Coef.").Points.DataBindXY(tabXM, tabYM2)
            .Series("Albedo Coef.").ChartType = DataVisualization.Charting.SeriesChartType.Line
            .Series("Albedo Coef.").YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary
            .Series("Albedo Coef.").ShadowOffset = 1
            .Series("Albedo Coef.").BorderWidth = 2
            .Series("Albedo Coef.").Color = Color.Orange
            .Series("Albedo Coef.").ToolTip = "Albedo coefficient = #VALY{N3}"
            '
            .Series.Add("Earth IR")
            .Series("Earth IR").Points.DataBindXY(tabXM, tabYM3)
            .Series("Earth IR").ChartType = DataVisualization.Charting.SeriesChartType.Line
            '.Series("Solar").YAxisType = "Primary"
            .Series("Earth IR").ShadowOffset = 1
            .Series("Earth IR").BorderWidth = 2
            .Series("Earth IR").Color = Color.Red
            .Series("Earth IR").ToolTip = "Earth IR = #VALY{N3}  W/m�"
            '
            If bCustomLabel = False Then
                '
                .ChartAreas(0).AxisX.CustomLabels.Add(0, 1, "J")
                .ChartAreas(0).AxisX.CustomLabels.Add(1, 2, "F")
                .ChartAreas(0).AxisX.CustomLabels.Add(2, 3, "M")
                .ChartAreas(0).AxisX.CustomLabels.Add(3, 4, "A")
                .ChartAreas(0).AxisX.CustomLabels.Add(4, 5, "M")
                .ChartAreas(0).AxisX.CustomLabels.Add(5, 6, "J")
                .ChartAreas(0).AxisX.CustomLabels.Add(6, 7, "J")
                .ChartAreas(0).AxisX.CustomLabels.Add(7, 8, "A")
                .ChartAreas(0).AxisX.CustomLabels.Add(8, 9, "S")
                .ChartAreas(0).AxisX.CustomLabels.Add(9, 10, "O")
                .ChartAreas(0).AxisX.CustomLabels.Add(10, 11, "N")
                .ChartAreas(0).AxisX.CustomLabels.Add(11, 12, "D")
                bCustomLabel = True
                '
            End If
            '
        End With
        ''
    End Sub
    Private Sub trace_CERES()
        ' Dessins des graphes
        Dim tabXM As ArrayList
        Dim tabYM1 As ArrayList
        Dim tabYM2 As ArrayList
        'Dim bCustomLabel As Boolean
        '
        Dim i As Integer
        Dim Y As Double = -95
        '
        '    Drawing graph
        '    -------------
        tabXM = New ArrayList
        tabYM1 = New ArrayList
        tabYM2 = New ArrayList
        '
        ' Trac� des courbes
        '
        For i = 0 To 17
            Y += 10
            '
            tabXM.Add(Y)
            '
            tabYM1.Add(flux_rad(EarthTemp(Y, CERES_month.SelectedIndex + 1) - 273.15))
            '
            tabYM2.Add(AlbedoCoef(Y, CERES_month.SelectedIndex + 1))
            '
        Next i

        '
        With CERES_graph
            '
            .Series.Clear()
            '
            '
            If EarthIR_Cer_bln Then
                .Series.Add("Earth IR")
                .Series("Earth IR").Points.DataBindXY(tabXM, tabYM1)
                .Series("Earth IR").ChartType = DataVisualization.Charting.SeriesChartType.Line
                .Series("Earth IR").YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary
                .Series("Earth IR").ShadowOffset = 1
                .Series("Earth IR").BorderWidth = 2
                .Series("Earth IR").Color = Color.Red
                .Series("Earth IR").ToolTip = "Latitude = #VALX" & vbCrLf & "Earth IR = #VALY  W/m�"
            End If
            '
            If Alb_Cer_bln Then
                .Series.Add("Albedo Coef.")
                .Series("Albedo Coef.").Points.DataBindXY(tabXM, tabYM2)
                .Series("Albedo Coef.").ChartType = DataVisualization.Charting.SeriesChartType.Line
                .Series("Albedo Coef.").YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary
                .Series("Albedo Coef.").ShadowOffset = 1
                .Series("Albedo Coef.").BorderWidth = 2
                .Series("Albedo Coef.").Color = Color.Orange
                .Series("Albedo Coef.").ToolTip = "Latitude = #VALX" & vbCrLf & "Albedo coefficient = #VALY"
            End If
            '
            'If bCustomLabel = False Then
            '    '
            '    .ChartAreas(0).AxisX.CustomLabels.Add(0, 1, "J")
            '    .ChartAreas(0).AxisX.CustomLabels.Add(1, 2, "F")
            '    .ChartAreas(0).AxisX.CustomLabels.Add(2, 3, "M")
            '    .ChartAreas(0).AxisX.CustomLabels.Add(3, 4, "A")
            '    .ChartAreas(0).AxisX.CustomLabels.Add(4, 5, "M")
            '    .ChartAreas(0).AxisX.CustomLabels.Add(5, 6, "J")
            '    .ChartAreas(0).AxisX.CustomLabels.Add(6, 7, "J")
            '    .ChartAreas(0).AxisX.CustomLabels.Add(7, 8, "A")
            '    .ChartAreas(0).AxisX.CustomLabels.Add(8, 9, "S")
            '    .ChartAreas(0).AxisX.CustomLabels.Add(9, 10, "O")
            '    .ChartAreas(0).AxisX.CustomLabels.Add(10, 11, "N")
            '    .ChartAreas(0).AxisX.CustomLabels.Add(11, 12, "D")
            '    bCustomLabel = True
            '    '
            'End If
            '
        End With
        ''
    End Sub

    'Private Sub trace_environment()
    '    ' Dessins des graphes
    '    Dim tabXM1 As ArrayList
    '    Dim tabYM1 As ArrayList
    '    Dim tabXM2 As ArrayList
    '    Dim tabYM2 As ArrayList
    '    Dim tabXM3 As ArrayList
    '    Dim tabYM3 As ArrayList
    '    Dim tabXM4 As ArrayList
    '    Dim tabYM4 As ArrayList
    '    Dim bCustomLabel As Boolean
    '    '
    '    Dim i As Integer
    '    Dim X As Double, Y As Double
    '    '
    '    '    Drawing graph
    '    '    -------------
    '    tabXM1 = New ArrayList
    '    tabYM1 = New ArrayList
    '    tabXM2 = New ArrayList
    '    tabYM2 = New ArrayList
    '    tabXM3 = New ArrayList
    '    tabYM3 = New ArrayList
    '    tabXM4 = New ArrayList
    '    tabYM4 = New ArrayList
    '    '
    '    ' Trac� des courbes
    '    '
    '    For i = 0 To 11
    '        X = i + 0.5
    '        '
    '        Y = solar_flux(dsun(i))
    '        tabXM1.Add(X)
    '        tabYM1.Add(Y)
    '        '
    '        Y = Albedo_month(i)
    '        tabXM2.Add(X)
    '        tabYM2.Add(Y)
    '        '
    '        Y = IR_month(i)
    '        tabXM3.Add(X)
    '        tabYM3.Add(Y)
    '        '
    '    Next i
    '    '
    '    With env_graph
    '        '
    '        .Series.Clear()
    '        .Series.Add("Solar")
    '        .Series("Solar").Points.DataBindXY(tabXM1, tabYM1)
    '        .Series("Solar").ChartType = DataVisualization.Charting.SeriesChartType.Line
    '        .Series("Solar").ShadowOffset = 1
    '        .Series("Solar").BorderWidth = 2
    '        .Series("Solar").Color = Color.Yellow
    '        .ChartAreas(0).RecalculateAxesScale()
    '        '
    '        .Series.Add("Albedo")
    '        .Series("Albedo").Points.DataBindXY(tabXM2, tabYM2)
    '        .Series("Albedo").ChartType = DataVisualization.Charting.SeriesChartType.Line
    '        .Series("Albedo").YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary
    '        .Series("Albedo").ShadowOffset = 1
    '        .Series("Albedo").BorderWidth = 2
    '        .Series("Albedo").Color = Color.Orange
    '        '
    '        .Series.Add("Earth IR")
    '        .Series("Earth IR").Points.DataBindXY(tabXM3, tabYM3)
    '        .Series("Earth IR").ChartType = DataVisualization.Charting.SeriesChartType.Line
    '        '.Series("Solar").YAxisType = "Primary"
    '        .Series("Earth IR").ShadowOffset = 1
    '        .Series("Earth IR").BorderWidth = 2
    '        .Series("Earth IR").Color = Color.Red
    '        '
    '        If bCustomLabel = False Then
    '            '
    '            .ChartAreas(0).AxisX.CustomLabels.Add(0, 1, "J")
    '            .ChartAreas(0).AxisX.CustomLabels.Add(1, 2, "F")
    '            .ChartAreas(0).AxisX.CustomLabels.Add(2, 3, "M")
    '            .ChartAreas(0).AxisX.CustomLabels.Add(3, 4, "A")
    '            .ChartAreas(0).AxisX.CustomLabels.Add(4, 5, "M")
    '            .ChartAreas(0).AxisX.CustomLabels.Add(5, 6, "J")
    '            .ChartAreas(0).AxisX.CustomLabels.Add(6, 7, "J")
    '            .ChartAreas(0).AxisX.CustomLabels.Add(7, 8, "A")
    '            .ChartAreas(0).AxisX.CustomLabels.Add(8, 9, "S")
    '            .ChartAreas(0).AxisX.CustomLabels.Add(9, 10, "O")
    '            .ChartAreas(0).AxisX.CustomLabels.Add(10, 11, "N")
    '            .ChartAreas(0).AxisX.CustomLabels.Add(11, 12, "D")
    '            bCustomLabel = True
    '            '
    '        End If
    '        '
    '    End With
    '    ''
    'End Sub

    Public Function solar_flux(ByVal d As Double) As Single
        '
        ' Direct solar radiation intensity for a given distance to Sun
        ' Input: d distance to Sun
        '
        solar_flux = CSng(SunPow / 4 / pi / d / d / 1000000.0#)
        '
    End Function

#Region "ONLY FOR TEST functions"

    ''' <summary>
    ''' Public copy of select_orbit_type()
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub select_orbit_type_fortest()
        select_orbit_type()
    End Sub

    ''' <summary>
    ''' Public copy of txtSSAltitude_Leave()
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Public Sub txtSSAltitude_Leave_fortest(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        txtSSAltitude_Leave(eventSender, eventArgs)
    End Sub

#End Region


End Class
