﻿Module basAttGlobal
    ' -------------------
    ' Module : att_global
    ' -------------------
    '
    ' Author: Lorenzo Donato
    ' Changed and transfered to SIMUSAT by Wolfgang Hausmann
    ' (26.11.99)
    '
    ' Included functions and procedures:
    ' Only declarations of public variables
    '
    '
    ' Variables du contrôle d'attitude
    ' --------------------------------
    Public SatMass As Double            ' Mass of the whole satellite
    Public BodyMass As Double           ' Mass of the central satellite body
    Public Cr_solar(5) As Double        ' Coefficients of reflection of solar light  0 To 5 : +X, +Y, +Z, -X, -Y, -Z
    Public Cd_solar(5) As Double        ' Coefficients of diffusion of solar light   0 To 5 : +X, +Y, +Z, -X, -Y, -Z
    Public Cn_aero(5) As Double         ' Coefficients for aerodynamic pressure normal to the surface   0 To 5 : +X, +Y, +Z, -X, -Y, -Z
    Public Ct_aero(5) As Double         ' Coefficients for aerodynamic pressure tangential to the surface   0 To 5 : +X, +Y, +Z, -X, -Y, -Z
    Public dipole_sat As vector         ' Total magnetic dipole of the satellite
    Public dipsat_passive As vector     ' Magnetic dipole because of passive properties
    Public dipsat_active As vector      ' Magnetic dipole because of magnetic control system
    Public dipsat_control As vector     ' Switch active magnetic dipole on and off
    Public jet(2) As Double             ' Control moment due to gas jets
    Public jet_control(2) As Integer    ' Switch jets on and off
    Public SatInertia(2, 2) As Double   ' Inertial mass moments of the whole satellite
    Public BodyInertia(2, 2) As Double  ' Inertial mass moments of the central satellite-body
    Public yd(5) As Double              ' EULER vector (angles and speeds)
    '                                     yd(0), yd(1), yd(2) are angles about x, z, y (!!!)
    '                                     yd(3), yd(4), yd(5) are speed about x, y, z
    '                                     angles are attitude angles (SPOT - SAT coordinate system)
    Public yi(5) As Double              ' Vector defining angles, speed and accelerations in
    '                                     the inertial coordinate system (gamma50) or between
    '                                     gamma50 and sat coordinate system
    '                                     yi(0), yi(1), yi(2): angles about x, y, z
    '                                     yi(3), yi(4), yi(5): speed about x, y, z
    '                                     yi(6), yi(7), yi(8): acceleration about x, y, z
    Public yi0(8) As Double             ' Start-vector for yi(8)
    Public omega(5) As Double           ' Angular speed and angular acceleration of the satellite
    Public omega0(5) As Double          ' Start-vector for omega(5)
    Public SatCG As vector              ' Position center mass for the whole satellite
    Public BodyCG As vector             ' Position center mass for the central satellite-body
    '
    Public vect_face(5) As vector       ' Array containing 6 vectors normal to the 6 surfaces   0 To 5 : +X, +Y, +Z, -X, -Y, -Z

    ' Solar presure
    Public Const PS As Double = 0.000004644 ' N/m²

    ' Gravity gradient stabilisation
    Public GravityMass As Double        ' Mass of the gravity gradient stabilisation system
    Public GravityDist As vector        ' Distance from centre of mass of main satellite

    ' Dampers
    Public DampersMass(2) As Double     ' Mass of the three damper masses
    Public zd(2) As Double              ' Deviation of damper masses from neutral position
    Public zdp(2) As Double             ' Speed of damper masses
    Public zdpp(2) As Double            ' Acceleration of damper masses
    Public DampersDist(2) As Double     ' Position of damper masses at rest
    Public c(2) As Double               ' Spring constants
    Public kd(2) As Double              ' Damper constants

    ' Wheels
    Public JR(2) As Double               ' Moments of inertia of the wheels (around their rotational axis)
    Public om(2) As Double               ' Rotational speed of wheels in the sat system (in deg/s)
    Public OMp(2) As Double              ' Rotational acceleration of wheels in the sat system
    Public OMin(2) As Double             ' Rotational speed of the wheels in the inertial system
    Public OMin0(2) As Double            ' Initialisation value

    ' Constants
    Public Const eps As Double = 1.0E-21

End Module
