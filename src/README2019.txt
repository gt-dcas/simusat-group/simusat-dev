--------------------
SIMUSAT: VERSION 6.3
--------------------

Description : Software for satellite simulation of ADCS, thermal and EPS
-------------


Documentation : ../DOC_Simusar/...
---------------
> IGRFcoefficients.xls : IGRF model of Earth magnetic field
> Installation_bibliotheque.pdf : how to install the necessaries libraries 
> Notice.pdf : mode d'emploi pour tracer les courbes selon le BE
> Presentation_des_courbes.pdf : trac� des couples perturbateurs et comparaison avec la version VB6 et Orekit (Java)
> Video_demonstration_Simusat.avi : d�monstration de ce qui est possible avec le logiciel


File examples : ../bin/DemoFiles/Quickstart&examples/...
---------------------
> sattest.sc : a simple satellite already configured ready to be opened by Simusat
> orbittest.orb : orbit and environment defined ready to be opened by Simusat
> Quickstart manual : Manual to create a satellite and its orbit in less than 10 minutes.

Fichiers de configuration : ../bin/resources/...
---------------------------
> blank_file.cs : fichier "vierge" utilis� lorsque l'on souhaite cr�er un nouveau fichier ("New")
> data_range.txt : (modifiable) valeurs minimales et maximales admises pour chaque grandeur
> default_values.txt : (modifiable) valeurs par d�faut pour chaque grandeur
> densite.txt : valeurs de densit� atmosph�rique de 100 � 2500 m
> flux_solaire.txt : valeurs de flux solaire de 1991 � 2007
> Albedo_iinc_mon_ESA.csv
> albedo_orbit_inclination.csv
> ConfigurationFactorEArthIR.csv
> Earth_Temp_ESA.csv
> orbit_temp_test_data.csv
> test_report.csv
> sattest2.sc
> testfiles.json
> thermalfiles.json
> conf.json


Installation of Visual Studio 2013:
-------------------------------
> Download "Microsoft Visual Studio Express 2013 pour Windows Desktop"
> Add the needed libraries (see : ../DOC_Simusat/Installation_bibliotheque.pdf)
> If the library Microsoft Chart Controls does not appear, install it thanks to ../DOC_Simusat/Biblio/MSChart-Setup.exe
> If the libraries TaoFramework does not appear, install them thanks to ../DOC_Simusat/Biblio/TaoFramework-2.1.0-Setup.exe



Installation of NewtonSoft.json in Visual Studio
------------------------------------------------
Make sure to have version 5 of Visual Studio 2013 (if not, update it). Inside Visual Studio:
1- Tools?Extensions then Updates and select Updates?Visual Studio Gallery
2- Once installed go to Tools?Nuget Package Manager --> Package Manager Console
3- Type Install-Package Newtonsoft.Json


===================
INSTALL
===================
dependencies/taoframework-2.1.0-setup.exe
dependencies/MSChart.exe

=======================
RUN
=======================
bin/Simusat



===========================
Wiki
==========================
h1. git usage:

# get repository
git clone https://openforge.isae.fr/git/simusat

#update
cd simusat
git pull origin master

#change branch from master to versionChristianVBNet
git co versionChristianVBNet



h1. branches: 

5 branches "vivantes" : 
*  master -> Version VBNet "officielle" d�ploy�e tr�s prochainement les postes, avec le SCAO (en test)
*  SimusatDeployedBefore2017-> ancienne version Version VB, fichiers manquants
*  VB6-> Version VB actuellement d�ploy�e sur les postes, avec le SCAO
*  versionChristianVBNet -> version VBNet de christian, avec un d�but de SCAO
*  versionVBNet -> version VBNet "officielle", sans le SCAO
SCAO

2 branches suppl�mentaires:
* stage de Melvin (2017)
* stage de Robin (2017)

en cas de doute, 
cat versionAuto.txt


==============================
TODO
==============================
 - add somewhere information about loaded file .att
 - solve the update of Body Heat Capacity Issue
 - solve the update of Heat Capacity per solar pannel Issue
 - remove hard coded values
 - treat case of infinite temperatures
 - save orbital configuration



===================
New stage - Angel 2019 - Engaged

