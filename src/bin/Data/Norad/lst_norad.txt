SPECIAL-INTEREST SATELLITE
 Last 30 Days' Launches                          tle-new.txt
 International Space Station                     stations.txt
 100 (or so) Brightest                           visual.txt
WEATHER & EARTH RESOURCES SATELLITES
 Weather                                         weather.txt
 NOAA                                            noaa.txt
 GOES                                            goes.txt
 Earth Resources                                 resource.txt
 Search & Rescue (SARSAT)                        sarsat.txt
 Tracking and Data Relay Satellite System        tdrss.txt
COMMUNICATION SATELLITES
 Geostationary                                   geo.txt
 Intelsat                                        intelsat.txt
 Gorizont                                        gorizont.txt
 Raduga                                          raduga.txt
 Molniya                                         molniya.txt
 Iridium                                         iridium.txt
 Orbcomm                                         orbcomm.txt
 Globalstar                                      globalstar.txt
 Amateur Radio                                   amateur.txt
 Experimental                                    x-comm.txt
 Other                                           other-comm.txt
NAVIGATION SATELLITES
 GPS Operational                                 gps-ops.txt
 Glonass Operational                             glo-ops.txt
 Navy Navigation Satellite System (NNSS)         nnss.txt
 Russian LEO Navigation                          musson.txt
SCIENTIFIC SATELLITES
 Space & Earth Science                           science.txt
 Geodetic                                        geodetic.txt
 Engineering                                     engineering.txt
 Education                                       education.txt
MISCELLANEOUS SATELLITES
 Miscellaneous Military                          military.txt
 Radar Calibration                               radar.txt
 Other                                           other.txt