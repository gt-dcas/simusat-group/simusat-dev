=====================
Content of the folder
=====================
Before running simusat, make sure that the folder where this file is has the following folders and files:

*Data
*resources
*Quickstart and examples
*Microsoft.Office.Interop.Excel.dll
*Microsoft.VisualStudio.QualityTools.UnitTestFramework.dll
*Newtonsoft.Json.dll
*Simusat
*Simusat.exe
*Simusat2011.ico
*Tao.OpenGl.dll
*Tao.Platform.Windows.dll

Ig any of the last is missing, please contact the provider if the software does not work well when running it.

==========
Quickstart
==========

To start using the software Simusat, just click on Simusat to start running the program.

If you find difficulties the first you open the software, open the Quickstart and examples folder. 
There you will find a pdf with a little guide that will take you through some of the software features to get started with it.

The examples files are made to be opened from the software. 
In the Satellite Design section you will work with .sc files and on the Mission Design section you will work with .orb files.

==================
Retrocompatibility
==================

If you have already worked with previous versions of Simusat, you may have .att and .orb files on your computer. 
These old files are still compatible with Simusat, although Environment data may not be correctly updated. 
We encourage you to update those files to .sc and .orb files.
 