﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAttConf
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAttConf))
        Me.tabPower = New System.Windows.Forms.TabControl()
        Me.Onglet_att_geom = New System.Windows.Forms.TabPage()
        Me.zfra_8 = New System.Windows.Forms.GroupBox()
        Me.zlbl_17 = New System.Windows.Forms.Label()
        Me.zlbl_18 = New System.Windows.Forms.Label()
        Me.mass_cmbBoomFace = New System.Windows.Forms.ComboBox()
        Me.mass_txtBoomMass = New System.Windows.Forms.TextBox()
        Me.mass_txtBoomDist = New System.Windows.Forms.TextBox()
        Me.zlbl_19 = New System.Windows.Forms.Label()
        Me.zlbl_16 = New System.Windows.Forms.Label()
        Me.zlbl_20 = New System.Windows.Forms.Label()
        Me.zfra_4 = New System.Windows.Forms.GroupBox()
        Me.mass_txtPanelNStrings = New System.Windows.Forms.TextBox()
        Me.lbl_nbstrings = New System.Windows.Forms.Label()
        Me.mass_lstPanels = New System.Windows.Forms.ListBox()
        Me.mass_cmdModify = New System.Windows.Forms.Button()
        Me.mass_txtPanelMass = New System.Windows.Forms.TextBox()
        Me.mass_cmdAddPanel = New System.Windows.Forms.Button()
        Me.zlbl_75 = New System.Windows.Forms.Label()
        Me.mass_cmdRemovePanel = New System.Windows.Forms.Button()
        Me.zlbl_64 = New System.Windows.Forms.Label()
        Me.zlbl_63 = New System.Windows.Forms.Label()
        Me.mass_cmbFace = New System.Windows.Forms.ComboBox()
        Me.mass_txtPanelLength = New System.Windows.Forms.TextBox()
        Me.mass_txtPanelWidth = New System.Windows.Forms.TextBox()
        Me.mass_cmbDirection = New System.Windows.Forms.ComboBox()
        Me.mass_cmbPosition = New System.Windows.Forms.ComboBox()
        Me.mass_optPanelType_0 = New System.Windows.Forms.RadioButton()
        Me.mass_optPanelType_1 = New System.Windows.Forms.RadioButton()
        Me.mass_optPanelType_2 = New System.Windows.Forms.RadioButton()
        Me.zlbl_69 = New System.Windows.Forms.Label()
        Me.zlbl_1 = New System.Windows.Forms.Label()
        Me.zlbl_5 = New System.Windows.Forms.Label()
        Me.zlbl_6 = New System.Windows.Forms.Label()
        Me.mass_lblPanelOrient = New System.Windows.Forms.Label()
        Me.mass_lblPanelPos = New System.Windows.Forms.Label()
        Me.zlbl_3 = New System.Windows.Forms.Label()
        Me.zpict_5 = New System.Windows.Forms.PictureBox()
        Me.zfra_3 = New System.Windows.Forms.GroupBox()
        Me.zlbl_11 = New System.Windows.Forms.Label()
        Me.mass_txtBodyMass = New System.Windows.Forms.TextBox()
        Me.zlbl_15 = New System.Windows.Forms.Label()
        Me.zfra_7 = New System.Windows.Forms.GroupBox()
        Me.mass_txtGy = New System.Windows.Forms.TextBox()
        Me.zlbl_74 = New System.Windows.Forms.Label()
        Me.zlbl_83 = New System.Windows.Forms.Label()
        Me.mass_txtGx = New System.Windows.Forms.TextBox()
        Me.zlbl_78 = New System.Windows.Forms.Label()
        Me.zlbl_82 = New System.Windows.Forms.Label()
        Me.mass_txtGz = New System.Windows.Forms.TextBox()
        Me.zlbl_79 = New System.Windows.Forms.Label()
        Me.zlbl_80 = New System.Windows.Forms.Label()
        Me.zfra_6 = New System.Windows.Forms.GroupBox()
        Me.mass_txtIyz = New System.Windows.Forms.TextBox()
        Me.zlbl_85 = New System.Windows.Forms.Label()
        Me.zlbl_86 = New System.Windows.Forms.Label()
        Me.zlbl_14 = New System.Windows.Forms.Label()
        Me.mass_txtIyy = New System.Windows.Forms.TextBox()
        Me.zlbl_13 = New System.Windows.Forms.Label()
        Me.zlbl_84 = New System.Windows.Forms.Label()
        Me.zlbl_12 = New System.Windows.Forms.Label()
        Me.mass_txtIzz = New System.Windows.Forms.TextBox()
        Me.mass_txtIxz = New System.Windows.Forms.TextBox()
        Me.mass_txtIxx = New System.Windows.Forms.TextBox()
        Me.mass_txtIxy = New System.Windows.Forms.TextBox()
        Me.zlbl_87 = New System.Windows.Forms.Label()
        Me.zlbl_10 = New System.Windows.Forms.Label()
        Me.zlbl_88 = New System.Windows.Forms.Label()
        Me.zfra_5 = New System.Windows.Forms.GroupBox()
        Me.zlbl_0 = New System.Windows.Forms.Label()
        Me.zlbl_2 = New System.Windows.Forms.Label()
        Me.mass_txtBodyWidth = New System.Windows.Forms.TextBox()
        Me.mass_txtBodyHeight = New System.Windows.Forms.TextBox()
        Me.zlbl_65 = New System.Windows.Forms.Label()
        Me.zlbl_66 = New System.Windows.Forms.Label()
        Me.Onglet_att_prop = New System.Windows.Forms.TabPage()
        Me.zfra_14 = New System.Windows.Forms.GroupBox()
        Me.zlbl102 = New System.Windows.Forms.Label()
        Me.zlbl101 = New System.Windows.Forms.Label()
        Me.zlbl100 = New System.Windows.Forms.Label()
        Me.prop_txtMG_X = New System.Windows.Forms.TextBox()
        Me.prop_txtMG_Y = New System.Windows.Forms.TextBox()
        Me.zlbl34 = New System.Windows.Forms.Label()
        Me.zlbl31 = New System.Windows.Forms.Label()
        Me.zlbl35 = New System.Windows.Forms.Label()
        Me.prop_txtMG_Z = New System.Windows.Forms.TextBox()
        Me.zfra_11 = New System.Windows.Forms.GroupBox()
        Me.zfra_12 = New System.Windows.Forms.GroupBox()
        Me.prop_txtCD_0 = New System.Windows.Forms.TextBox()
        Me.prop_txtCD_4 = New System.Windows.Forms.TextBox()
        Me.zlbl24 = New System.Windows.Forms.Label()
        Me.prop_txtCD_3 = New System.Windows.Forms.TextBox()
        Me.prop_txtCD_2 = New System.Windows.Forms.TextBox()
        Me.zlbl23 = New System.Windows.Forms.Label()
        Me.prop_txtCD_5 = New System.Windows.Forms.TextBox()
        Me.prop_txtCD_1 = New System.Windows.Forms.TextBox()
        Me.zlbl22 = New System.Windows.Forms.Label()
        Me.zlbl19 = New System.Windows.Forms.Label()
        Me.zlbl20 = New System.Windows.Forms.Label()
        Me.zlbl21 = New System.Windows.Forms.Label()
        Me.zfra_13 = New System.Windows.Forms.GroupBox()
        Me.prop_txtCR_0 = New System.Windows.Forms.TextBox()
        Me.prop_txtCR_4 = New System.Windows.Forms.TextBox()
        Me.zlbl18 = New System.Windows.Forms.Label()
        Me.prop_txtCR_3 = New System.Windows.Forms.TextBox()
        Me.zlbl17 = New System.Windows.Forms.Label()
        Me.prop_txtCR_2 = New System.Windows.Forms.TextBox()
        Me.prop_txtCR_5 = New System.Windows.Forms.TextBox()
        Me.zlbl16 = New System.Windows.Forms.Label()
        Me.prop_txtCR_1 = New System.Windows.Forms.TextBox()
        Me.zlbl15 = New System.Windows.Forms.Label()
        Me.zlbl13 = New System.Windows.Forms.Label()
        Me.zlbl14 = New System.Windows.Forms.Label()
        Me.zfra_1 = New System.Windows.Forms.GroupBox()
        Me.zfra_10 = New System.Windows.Forms.GroupBox()
        Me.prop_txtCT_0 = New System.Windows.Forms.TextBox()
        Me.prop_txtCT_4 = New System.Windows.Forms.TextBox()
        Me.zlbl12 = New System.Windows.Forms.Label()
        Me.zlbl11 = New System.Windows.Forms.Label()
        Me.prop_txtCT_3 = New System.Windows.Forms.TextBox()
        Me.zlbl10 = New System.Windows.Forms.Label()
        Me.prop_txtCT_2 = New System.Windows.Forms.TextBox()
        Me.zlbl6 = New System.Windows.Forms.Label()
        Me.prop_txtCT_5 = New System.Windows.Forms.TextBox()
        Me.zlbl4 = New System.Windows.Forms.Label()
        Me.prop_txtCT_1 = New System.Windows.Forms.TextBox()
        Me.zlbl3 = New System.Windows.Forms.Label()
        Me.zfra_2 = New System.Windows.Forms.GroupBox()
        Me.prop_txtCN_0 = New System.Windows.Forms.TextBox()
        Me.prop_txtCN_4 = New System.Windows.Forms.TextBox()
        Me.zlbl9 = New System.Windows.Forms.Label()
        Me.zlbl1 = New System.Windows.Forms.Label()
        Me.zlbl8 = New System.Windows.Forms.Label()
        Me.zlbl2 = New System.Windows.Forms.Label()
        Me.zlbl7 = New System.Windows.Forms.Label()
        Me.prop_txtCN_3 = New System.Windows.Forms.TextBox()
        Me.prop_txtCN_2 = New System.Windows.Forms.TextBox()
        Me.zlbl5 = New System.Windows.Forms.Label()
        Me.prop_txtCN_5 = New System.Windows.Forms.TextBox()
        Me.prop_txtCN_1 = New System.Windows.Forms.TextBox()
        Me.Onglet_att_act = New System.Windows.Forms.TabPage()
        Me.act_txtName = New System.Windows.Forms.TextBox()
        Me.zlbl154 = New System.Windows.Forms.Label()
        Me.act_cmdModify = New System.Windows.Forms.Button()
        Me.zfra_24 = New System.Windows.Forms.GroupBox()
        Me.zlbl153 = New System.Windows.Forms.Label()
        Me.zlbl152 = New System.Windows.Forms.Label()
        Me.zlbl151 = New System.Windows.Forms.Label()
        Me.act_txtMN_X = New System.Windows.Forms.TextBox()
        Me.act_txtMN_Y = New System.Windows.Forms.TextBox()
        Me.zlbl149 = New System.Windows.Forms.Label()
        Me.zlbl150 = New System.Windows.Forms.Label()
        Me.zlbl148 = New System.Windows.Forms.Label()
        Me.act_txtMN_Z = New System.Windows.Forms.TextBox()
        Me.zfra_23 = New System.Windows.Forms.GroupBox()
        Me.zlbl147 = New System.Windows.Forms.Label()
        Me.zlbl146 = New System.Windows.Forms.Label()
        Me.zlbl145 = New System.Windows.Forms.Label()
        Me.act_txtKX = New System.Windows.Forms.TextBox()
        Me.act_txtKY = New System.Windows.Forms.TextBox()
        Me.zlbl143 = New System.Windows.Forms.Label()
        Me.zlbl144 = New System.Windows.Forms.Label()
        Me.zlbl142 = New System.Windows.Forms.Label()
        Me.act_txtKZ = New System.Windows.Forms.TextBox()
        Me.zfra_232 = New System.Windows.Forms.GroupBox()
        Me.zlbl135 = New System.Windows.Forms.Label()
        Me.zlbl134 = New System.Windows.Forms.Label()
        Me.act_txtDX = New System.Windows.Forms.TextBox()
        Me.act_txtDZ = New System.Windows.Forms.TextBox()
        Me.zlbl133 = New System.Windows.Forms.Label()
        Me.zlbl130 = New System.Windows.Forms.Label()
        Me.zlbl132 = New System.Windows.Forms.Label()
        Me.act_txtDY = New System.Windows.Forms.TextBox()
        Me.zlbl131 = New System.Windows.Forms.Label()
        Me.zfra_231 = New System.Windows.Forms.GroupBox()
        Me.zlbl129 = New System.Windows.Forms.Label()
        Me.zlbl128 = New System.Windows.Forms.Label()
        Me.act_txtMX = New System.Windows.Forms.TextBox()
        Me.act_txtMZ = New System.Windows.Forms.TextBox()
        Me.zlbl127 = New System.Windows.Forms.Label()
        Me.zlbl124 = New System.Windows.Forms.Label()
        Me.zlbl126 = New System.Windows.Forms.Label()
        Me.act_txtMY = New System.Windows.Forms.TextBox()
        Me.zlbl125 = New System.Windows.Forms.Label()
        Me.zlbl141 = New System.Windows.Forms.Label()
        Me.zlbl140 = New System.Windows.Forms.Label()
        Me.zlbl139 = New System.Windows.Forms.Label()
        Me.act_txtCX = New System.Windows.Forms.TextBox()
        Me.act_txtCY = New System.Windows.Forms.TextBox()
        Me.zlbl137 = New System.Windows.Forms.Label()
        Me.zlbl138 = New System.Windows.Forms.Label()
        Me.zlbl136 = New System.Windows.Forms.Label()
        Me.act_txtCZ = New System.Windows.Forms.TextBox()
        Me.zfra_22 = New System.Windows.Forms.GroupBox()
        Me.zlbl123 = New System.Windows.Forms.Label()
        Me.zlbl122 = New System.Windows.Forms.Label()
        Me.zlbl121 = New System.Windows.Forms.Label()
        Me.act_txtMG_X = New System.Windows.Forms.TextBox()
        Me.act_txtMG_Y = New System.Windows.Forms.TextBox()
        Me.zlbl119 = New System.Windows.Forms.Label()
        Me.zlbl120 = New System.Windows.Forms.Label()
        Me.zlbl118 = New System.Windows.Forms.Label()
        Me.act_txtMG_Z = New System.Windows.Forms.TextBox()
        Me.zfra_21 = New System.Windows.Forms.GroupBox()
        Me.zlbl117 = New System.Windows.Forms.Label()
        Me.zlbl116 = New System.Windows.Forms.Label()
        Me.zlbl115 = New System.Windows.Forms.Label()
        Me.zlbl114 = New System.Windows.Forms.Label()
        Me.zlbl113 = New System.Windows.Forms.Label()
        Me.zlbl112 = New System.Windows.Forms.Label()
        Me.act_txtWRX = New System.Windows.Forms.TextBox()
        Me.act_txtWRY = New System.Windows.Forms.TextBox()
        Me.zlbl110 = New System.Windows.Forms.Label()
        Me.zlbl111 = New System.Windows.Forms.Label()
        Me.zlbl109 = New System.Windows.Forms.Label()
        Me.act_txtWRZ = New System.Windows.Forms.TextBox()
        Me.zlbl108 = New System.Windows.Forms.Label()
        Me.zlbl107 = New System.Windows.Forms.Label()
        Me.zlbl106 = New System.Windows.Forms.Label()
        Me.act_txtIRX = New System.Windows.Forms.TextBox()
        Me.act_txtIRY = New System.Windows.Forms.TextBox()
        Me.zlbl104 = New System.Windows.Forms.Label()
        Me.zlbl105 = New System.Windows.Forms.Label()
        Me.zlbl103 = New System.Windows.Forms.Label()
        Me.act_txtIRZ = New System.Windows.Forms.TextBox()
        Me.Onglet_att_manag = New System.Windows.Forms.TabPage()
        Me.zfra_25 = New System.Windows.Forms.GroupBox()
        Me.gest_cmdOpen = New System.Windows.Forms.Button()
        Me.gest_cmdSave = New System.Windows.Forms.Button()
        Me.gest_txtName = New System.Windows.Forms.TextBox()
        Me.zlbl202 = New System.Windows.Forms.Label()
        Me.gest_txtDescription = New System.Windows.Forms.TextBox()
        Me.zlbl200 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.gest_txtTime = New System.Windows.Forms.TextBox()
        Me.zlbl201 = New System.Windows.Forms.Label()
        Me.gest_cmdAdd = New System.Windows.Forms.Button()
        Me.gest_cmdRemove = New System.Windows.Forms.Button()
        Me.gest_cmdModify = New System.Windows.Forms.Button()
        Me.gest_lstEvents = New System.Windows.Forms.ListBox()
        Me.zfra_9 = New System.Windows.Forms.GroupBox()
        Me.mass_lblSatMass = New System.Windows.Forms.Label()
        Me.zlbl30 = New System.Windows.Forms.Label()
        Me.zlbl28 = New System.Windows.Forms.Label()
        Me.zfra_15 = New System.Windows.Forms.GroupBox()
        Me.mass_lblSatGz = New System.Windows.Forms.Label()
        Me.mass_lblSatGy = New System.Windows.Forms.Label()
        Me.mass_lblSatGx = New System.Windows.Forms.Label()
        Me.zlbl37 = New System.Windows.Forms.Label()
        Me.zlbl38 = New System.Windows.Forms.Label()
        Me.zlbl40 = New System.Windows.Forms.Label()
        Me.zfra_16 = New System.Windows.Forms.GroupBox()
        Me.mass_lblSatIxz = New System.Windows.Forms.Label()
        Me.mass_lblSatIxy = New System.Windows.Forms.Label()
        Me.mass_lblSatIyz = New System.Windows.Forms.Label()
        Me.mass_lblSatIxx = New System.Windows.Forms.Label()
        Me.mass_lblSatIyy = New System.Windows.Forms.Label()
        Me.mass_lblSatIzz = New System.Windows.Forms.Label()
        Me.zlbl25 = New System.Windows.Forms.Label()
        Me.zlbl26 = New System.Windows.Forms.Label()
        Me.zlbl29 = New System.Windows.Forms.Label()
        Me.zlbl32 = New System.Windows.Forms.Label()
        Me.zlbl33 = New System.Windows.Forms.Label()
        Me.zlbl36 = New System.Windows.Forms.Label()
        Me.mass_pictSatellite = New System.Windows.Forms.PictureBox()
        Me.cmd_quit = New System.Windows.Forms.Button()
        Me.tmrAnimate = New System.Windows.Forms.Timer(Me.components)
        Me.tmrTransition = New System.Windows.Forms.Timer(Me.components)
        Me.mass_lblmsg = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dlgScenarioOpen = New System.Windows.Forms.OpenFileDialog()
        Me.dlgScenarioSave = New System.Windows.Forms.SaveFileDialog()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tabPower.SuspendLayout()
        Me.Onglet_att_geom.SuspendLayout()
        Me.zfra_8.SuspendLayout()
        Me.zfra_4.SuspendLayout()
        CType(Me.zpict_5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.zfra_3.SuspendLayout()
        Me.zfra_7.SuspendLayout()
        Me.zfra_6.SuspendLayout()
        Me.zfra_5.SuspendLayout()
        Me.Onglet_att_prop.SuspendLayout()
        Me.zfra_14.SuspendLayout()
        Me.zfra_11.SuspendLayout()
        Me.zfra_12.SuspendLayout()
        Me.zfra_13.SuspendLayout()
        Me.zfra_1.SuspendLayout()
        Me.zfra_10.SuspendLayout()
        Me.zfra_2.SuspendLayout()
        Me.Onglet_att_act.SuspendLayout()
        Me.zfra_24.SuspendLayout()
        Me.zfra_23.SuspendLayout()
        Me.zfra_232.SuspendLayout()
        Me.zfra_231.SuspendLayout()
        Me.zfra_22.SuspendLayout()
        Me.zfra_21.SuspendLayout()
        Me.Onglet_att_manag.SuspendLayout()
        Me.zfra_25.SuspendLayout()
        Me.zfra_9.SuspendLayout()
        Me.zfra_15.SuspendLayout()
        Me.zfra_16.SuspendLayout()
        CType(Me.mass_pictSatellite, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tabPower
        '
        Me.tabPower.Controls.Add(Me.Onglet_att_geom)
        Me.tabPower.Controls.Add(Me.Onglet_att_prop)
        Me.tabPower.Controls.Add(Me.Onglet_att_act)
        Me.tabPower.Controls.Add(Me.Onglet_att_manag)
        Me.tabPower.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabPower.ItemSize = New System.Drawing.Size(100, 28)
        Me.tabPower.Location = New System.Drawing.Point(23, 23)
        Me.tabPower.Name = "tabPower"
        Me.tabPower.SelectedIndex = 0
        Me.tabPower.Size = New System.Drawing.Size(635, 556)
        Me.tabPower.TabIndex = 26
        '
        'Onglet_att_geom
        '
        Me.Onglet_att_geom.Controls.Add(Me.zfra_8)
        Me.Onglet_att_geom.Controls.Add(Me.zfra_4)
        Me.Onglet_att_geom.Controls.Add(Me.zfra_3)
        Me.Onglet_att_geom.Location = New System.Drawing.Point(4, 32)
        Me.Onglet_att_geom.Name = "Onglet_att_geom"
        Me.Onglet_att_geom.Size = New System.Drawing.Size(627, 520)
        Me.Onglet_att_geom.TabIndex = 6
        Me.Onglet_att_geom.Text = "Mass, geometry"
        Me.Onglet_att_geom.UseVisualStyleBackColor = True
        '
        'zfra_8
        '
        Me.zfra_8.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_8.Controls.Add(Me.zlbl_17)
        Me.zfra_8.Controls.Add(Me.zlbl_18)
        Me.zfra_8.Controls.Add(Me.mass_cmbBoomFace)
        Me.zfra_8.Controls.Add(Me.mass_txtBoomMass)
        Me.zfra_8.Controls.Add(Me.mass_txtBoomDist)
        Me.zfra_8.Controls.Add(Me.zlbl_19)
        Me.zfra_8.Controls.Add(Me.zlbl_16)
        Me.zfra_8.Controls.Add(Me.zlbl_20)
        Me.zfra_8.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_8.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_8.Location = New System.Drawing.Point(15, 443)
        Me.zfra_8.Name = "zfra_8"
        Me.zfra_8.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_8.Size = New System.Drawing.Size(592, 69)
        Me.zfra_8.TabIndex = 12
        Me.zfra_8.TabStop = False
        Me.zfra_8.Text = "Gravity gradient boom"
        '
        'zlbl_17
        '
        Me.zlbl_17.AutoSize = True
        Me.zlbl_17.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_17.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_17.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_17.Location = New System.Drawing.Point(285, 31)
        Me.zlbl_17.Name = "zlbl_17"
        Me.zlbl_17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_17.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_17.TabIndex = 25
        Me.zlbl_17.Text = "m"
        Me.zlbl_17.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_18
        '
        Me.zlbl_18.AutoSize = True
        Me.zlbl_18.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_18.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_18.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_18.Location = New System.Drawing.Point(119, 31)
        Me.zlbl_18.Name = "zlbl_18"
        Me.zlbl_18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_18.Size = New System.Drawing.Size(23, 16)
        Me.zlbl_18.TabIndex = 25
        Me.zlbl_18.Text = "kg"
        Me.zlbl_18.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_cmbBoomFace
        '
        Me.mass_cmbBoomFace.BackColor = System.Drawing.SystemColors.Window
        Me.mass_cmbBoomFace.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmbBoomFace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.mass_cmbBoomFace.Enabled = False
        Me.mass_cmbBoomFace.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmbBoomFace.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_cmbBoomFace.Location = New System.Drawing.Point(392, 28)
        Me.mass_cmbBoomFace.Name = "mass_cmbBoomFace"
        Me.mass_cmbBoomFace.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmbBoomFace.Size = New System.Drawing.Size(66, 24)
        Me.mass_cmbBoomFace.TabIndex = 25
        '
        'mass_txtBoomMass
        '
        Me.mass_txtBoomMass.AcceptsReturn = True
        Me.mass_txtBoomMass.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtBoomMass.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtBoomMass.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtBoomMass.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtBoomMass.Location = New System.Drawing.Point(64, 28)
        Me.mass_txtBoomMass.MaximumSize = New System.Drawing.Size(150, 50)
        Me.mass_txtBoomMass.MaxLength = 0
        Me.mass_txtBoomMass.Name = "mass_txtBoomMass"
        Me.mass_txtBoomMass.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtBoomMass.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtBoomMass.TabIndex = 23
        Me.mass_txtBoomMass.Text = "1"
        '
        'mass_txtBoomDist
        '
        Me.mass_txtBoomDist.AcceptsReturn = True
        Me.mass_txtBoomDist.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtBoomDist.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtBoomDist.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtBoomDist.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtBoomDist.Location = New System.Drawing.Point(230, 28)
        Me.mass_txtBoomDist.MaximumSize = New System.Drawing.Size(150, 50)
        Me.mass_txtBoomDist.MaxLength = 0
        Me.mass_txtBoomDist.Name = "mass_txtBoomDist"
        Me.mass_txtBoomDist.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtBoomDist.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtBoomDist.TabIndex = 24
        Me.mass_txtBoomDist.Text = "1"
        '
        'zlbl_19
        '
        Me.zlbl_19.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_19.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_19.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_19.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_19.Location = New System.Drawing.Point(297, 31)
        Me.zlbl_19.Name = "zlbl_19"
        Me.zlbl_19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_19.Size = New System.Drawing.Size(89, 19)
        Me.zlbl_19.TabIndex = 23
        Me.zlbl_19.Text = "On face"
        Me.zlbl_19.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_16
        '
        Me.zlbl_16.AutoSize = True
        Me.zlbl_16.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_16.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_16.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_16.Location = New System.Drawing.Point(18, 31)
        Me.zlbl_16.Name = "zlbl_16"
        Me.zlbl_16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_16.Size = New System.Drawing.Size(41, 16)
        Me.zlbl_16.TabIndex = 22
        Me.zlbl_16.Text = "Mass"
        Me.zlbl_16.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_20
        '
        Me.zlbl_20.AutoSize = True
        Me.zlbl_20.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_20.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_20.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_20.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_20.Location = New System.Drawing.Point(162, 31)
        Me.zlbl_20.Name = "zlbl_20"
        Me.zlbl_20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_20.Size = New System.Drawing.Size(65, 16)
        Me.zlbl_20.TabIndex = 21
        Me.zlbl_20.Text = "Distance"
        Me.zlbl_20.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zfra_4
        '
        Me.zfra_4.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_4.Controls.Add(Me.mass_txtPanelNStrings)
        Me.zfra_4.Controls.Add(Me.lbl_nbstrings)
        Me.zfra_4.Controls.Add(Me.mass_lstPanels)
        Me.zfra_4.Controls.Add(Me.mass_cmdModify)
        Me.zfra_4.Controls.Add(Me.mass_txtPanelMass)
        Me.zfra_4.Controls.Add(Me.mass_cmdAddPanel)
        Me.zfra_4.Controls.Add(Me.zlbl_75)
        Me.zfra_4.Controls.Add(Me.mass_cmdRemovePanel)
        Me.zfra_4.Controls.Add(Me.zlbl_64)
        Me.zfra_4.Controls.Add(Me.zlbl_63)
        Me.zfra_4.Controls.Add(Me.mass_cmbFace)
        Me.zfra_4.Controls.Add(Me.mass_txtPanelLength)
        Me.zfra_4.Controls.Add(Me.mass_txtPanelWidth)
        Me.zfra_4.Controls.Add(Me.mass_cmbDirection)
        Me.zfra_4.Controls.Add(Me.mass_cmbPosition)
        Me.zfra_4.Controls.Add(Me.mass_optPanelType_0)
        Me.zfra_4.Controls.Add(Me.mass_optPanelType_1)
        Me.zfra_4.Controls.Add(Me.mass_optPanelType_2)
        Me.zfra_4.Controls.Add(Me.zlbl_69)
        Me.zfra_4.Controls.Add(Me.zlbl_1)
        Me.zfra_4.Controls.Add(Me.zlbl_5)
        Me.zfra_4.Controls.Add(Me.zlbl_6)
        Me.zfra_4.Controls.Add(Me.mass_lblPanelOrient)
        Me.zfra_4.Controls.Add(Me.mass_lblPanelPos)
        Me.zfra_4.Controls.Add(Me.zlbl_3)
        Me.zfra_4.Controls.Add(Me.zpict_5)
        Me.zfra_4.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_4.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_4.Location = New System.Drawing.Point(16, 207)
        Me.zfra_4.Name = "zfra_4"
        Me.zfra_4.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_4.Size = New System.Drawing.Size(591, 230)
        Me.zfra_4.TabIndex = 12
        Me.zfra_4.TabStop = False
        Me.zfra_4.Text = "Panels"
        '
        'mass_txtPanelNStrings
        '
        Me.mass_txtPanelNStrings.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtPanelNStrings.Location = New System.Drawing.Point(226, 199)
        Me.mass_txtPanelNStrings.Name = "mass_txtPanelNStrings"
        Me.mass_txtPanelNStrings.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtPanelNStrings.TabIndex = 26
        Me.mass_txtPanelNStrings.Text = "1"
        '
        'lbl_nbstrings
        '
        Me.lbl_nbstrings.BackColor = System.Drawing.SystemColors.Control
        Me.lbl_nbstrings.Cursor = System.Windows.Forms.Cursors.Default
        Me.lbl_nbstrings.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nbstrings.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl_nbstrings.Location = New System.Drawing.Point(164, 199)
        Me.lbl_nbstrings.Name = "lbl_nbstrings"
        Me.lbl_nbstrings.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_nbstrings.Size = New System.Drawing.Size(57, 17)
        Me.lbl_nbstrings.TabIndex = 27
        Me.lbl_nbstrings.Text = "Strings"
        Me.lbl_nbstrings.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip1.SetToolTip(Me.lbl_nbstrings, "Number of Strings of solar cells that the Solar Panel contains")
        '
        'mass_lstPanels
        '
        Me.mass_lstPanels.BackColor = System.Drawing.SystemColors.Window
        Me.mass_lstPanels.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_lstPanels.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lstPanels.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_lstPanels.ItemHeight = 16
        Me.mass_lstPanels.Location = New System.Drawing.Point(428, 29)
        Me.mass_lstPanels.Name = "mass_lstPanels"
        Me.mass_lstPanels.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_lstPanels.Size = New System.Drawing.Size(150, 132)
        Me.mass_lstPanels.TabIndex = 21
        '
        'mass_cmdModify
        '
        Me.mass_cmdModify.BackColor = System.Drawing.SystemColors.Control
        Me.mass_cmdModify.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmdModify.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmdModify.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_cmdModify.Location = New System.Drawing.Point(462, 172)
        Me.mass_cmdModify.Name = "mass_cmdModify"
        Me.mass_cmdModify.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmdModify.Size = New System.Drawing.Size(67, 31)
        Me.mass_cmdModify.TabIndex = 22
        Me.mass_cmdModify.Text = "Modify"
        Me.mass_cmdModify.UseVisualStyleBackColor = False
        '
        'mass_txtPanelMass
        '
        Me.mass_txtPanelMass.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtPanelMass.Location = New System.Drawing.Point(226, 170)
        Me.mass_txtPanelMass.Name = "mass_txtPanelMass"
        Me.mass_txtPanelMass.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtPanelMass.TabIndex = 18
        Me.mass_txtPanelMass.Text = "1"
        '
        'mass_cmdAddPanel
        '
        Me.mass_cmdAddPanel.BackColor = System.Drawing.SystemColors.Control
        Me.mass_cmdAddPanel.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmdAddPanel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmdAddPanel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_cmdAddPanel.Location = New System.Drawing.Point(312, 54)
        Me.mass_cmdAddPanel.Name = "mass_cmdAddPanel"
        Me.mass_cmdAddPanel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmdAddPanel.Size = New System.Drawing.Size(103, 29)
        Me.mass_cmdAddPanel.TabIndex = 19
        Me.mass_cmdAddPanel.Text = "Add >>"
        Me.mass_cmdAddPanel.UseVisualStyleBackColor = False
        '
        'zlbl_75
        '
        Me.zlbl_75.AutoSize = True
        Me.zlbl_75.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_75.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_75.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_75.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_75.Location = New System.Drawing.Point(280, 172)
        Me.zlbl_75.Name = "zlbl_75"
        Me.zlbl_75.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_75.Size = New System.Drawing.Size(23, 16)
        Me.zlbl_75.TabIndex = 25
        Me.zlbl_75.Text = "kg"
        Me.zlbl_75.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_cmdRemovePanel
        '
        Me.mass_cmdRemovePanel.BackColor = System.Drawing.SystemColors.Control
        Me.mass_cmdRemovePanel.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmdRemovePanel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmdRemovePanel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_cmdRemovePanel.Location = New System.Drawing.Point(312, 95)
        Me.mass_cmdRemovePanel.Name = "mass_cmdRemovePanel"
        Me.mass_cmdRemovePanel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmdRemovePanel.Size = New System.Drawing.Size(103, 31)
        Me.mass_cmdRemovePanel.TabIndex = 20
        Me.mass_cmdRemovePanel.Text = "<< Remove"
        Me.mass_cmdRemovePanel.UseVisualStyleBackColor = False
        '
        'zlbl_64
        '
        Me.zlbl_64.AutoSize = True
        Me.zlbl_64.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_64.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_64.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_64.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_64.Location = New System.Drawing.Point(282, 143)
        Me.zlbl_64.Name = "zlbl_64"
        Me.zlbl_64.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_64.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_64.TabIndex = 25
        Me.zlbl_64.Text = "m"
        Me.zlbl_64.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_63
        '
        Me.zlbl_63.AutoSize = True
        Me.zlbl_63.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_63.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_63.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_63.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_63.Location = New System.Drawing.Point(282, 113)
        Me.zlbl_63.Name = "zlbl_63"
        Me.zlbl_63.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_63.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_63.TabIndex = 25
        Me.zlbl_63.Text = "m"
        Me.zlbl_63.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_cmbFace
        '
        Me.mass_cmbFace.BackColor = System.Drawing.SystemColors.Window
        Me.mass_cmbFace.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmbFace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.mass_cmbFace.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmbFace.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_cmbFace.Location = New System.Drawing.Point(226, 21)
        Me.mass_cmbFace.Name = "mass_cmbFace"
        Me.mass_cmbFace.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmbFace.Size = New System.Drawing.Size(66, 24)
        Me.mass_cmbFace.TabIndex = 13
        '
        'mass_txtPanelLength
        '
        Me.mass_txtPanelLength.AcceptsReturn = True
        Me.mass_txtPanelLength.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtPanelLength.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtPanelLength.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtPanelLength.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtPanelLength.Location = New System.Drawing.Point(226, 111)
        Me.mass_txtPanelLength.MaximumSize = New System.Drawing.Size(150, 50)
        Me.mass_txtPanelLength.MaxLength = 0
        Me.mass_txtPanelLength.Name = "mass_txtPanelLength"
        Me.mass_txtPanelLength.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtPanelLength.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtPanelLength.TabIndex = 16
        Me.mass_txtPanelLength.Text = "1"
        '
        'mass_txtPanelWidth
        '
        Me.mass_txtPanelWidth.AcceptsReturn = True
        Me.mass_txtPanelWidth.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtPanelWidth.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtPanelWidth.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtPanelWidth.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtPanelWidth.Location = New System.Drawing.Point(226, 141)
        Me.mass_txtPanelWidth.MaximumSize = New System.Drawing.Size(150, 50)
        Me.mass_txtPanelWidth.MaxLength = 0
        Me.mass_txtPanelWidth.Name = "mass_txtPanelWidth"
        Me.mass_txtPanelWidth.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtPanelWidth.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtPanelWidth.TabIndex = 17
        Me.mass_txtPanelWidth.Text = "1"
        '
        'mass_cmbDirection
        '
        Me.mass_cmbDirection.BackColor = System.Drawing.SystemColors.Window
        Me.mass_cmbDirection.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmbDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.mass_cmbDirection.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmbDirection.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_cmbDirection.Location = New System.Drawing.Point(226, 51)
        Me.mass_cmbDirection.Name = "mass_cmbDirection"
        Me.mass_cmbDirection.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmbDirection.Size = New System.Drawing.Size(66, 24)
        Me.mass_cmbDirection.TabIndex = 14
        '
        'mass_cmbPosition
        '
        Me.mass_cmbPosition.BackColor = System.Drawing.SystemColors.Window
        Me.mass_cmbPosition.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmbPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.mass_cmbPosition.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmbPosition.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_cmbPosition.Items.AddRange(New Object() {"Upper", "Middle", "Bottom"})
        Me.mass_cmbPosition.Location = New System.Drawing.Point(226, 81)
        Me.mass_cmbPosition.Name = "mass_cmbPosition"
        Me.mass_cmbPosition.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmbPosition.Size = New System.Drawing.Size(66, 24)
        Me.mass_cmbPosition.TabIndex = 15
        '
        'mass_optPanelType_0
        '
        Me.mass_optPanelType_0.BackColor = System.Drawing.SystemColors.Control
        Me.mass_optPanelType_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_optPanelType_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_optPanelType_0.Location = New System.Drawing.Point(55, 85)
        Me.mass_optPanelType_0.Name = "mass_optPanelType_0"
        Me.mass_optPanelType_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_optPanelType_0.Size = New System.Drawing.Size(14, 13)
        Me.mass_optPanelType_0.TabIndex = 12
        Me.mass_optPanelType_0.TabStop = True
        Me.mass_optPanelType_0.UseVisualStyleBackColor = False
        '
        'mass_optPanelType_1
        '
        Me.mass_optPanelType_1.BackColor = System.Drawing.SystemColors.Control
        Me.mass_optPanelType_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_optPanelType_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_optPanelType_1.Location = New System.Drawing.Point(83, 85)
        Me.mass_optPanelType_1.Name = "mass_optPanelType_1"
        Me.mass_optPanelType_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_optPanelType_1.Size = New System.Drawing.Size(14, 13)
        Me.mass_optPanelType_1.TabIndex = 11
        Me.mass_optPanelType_1.TabStop = True
        Me.mass_optPanelType_1.UseVisualStyleBackColor = False
        '
        'mass_optPanelType_2
        '
        Me.mass_optPanelType_2.BackColor = System.Drawing.SystemColors.Control
        Me.mass_optPanelType_2.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_optPanelType_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_optPanelType_2.Location = New System.Drawing.Point(112, 85)
        Me.mass_optPanelType_2.Name = "mass_optPanelType_2"
        Me.mass_optPanelType_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_optPanelType_2.Size = New System.Drawing.Size(14, 13)
        Me.mass_optPanelType_2.TabIndex = 10
        Me.mass_optPanelType_2.TabStop = True
        Me.mass_optPanelType_2.UseVisualStyleBackColor = False
        '
        'zlbl_69
        '
        Me.zlbl_69.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_69.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_69.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_69.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_69.Location = New System.Drawing.Point(180, 171)
        Me.zlbl_69.Name = "zlbl_69"
        Me.zlbl_69.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_69.Size = New System.Drawing.Size(41, 16)
        Me.zlbl_69.TabIndex = 22
        Me.zlbl_69.Text = "Mass"
        Me.zlbl_69.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_1
        '
        Me.zlbl_1.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_1.Location = New System.Drawing.Point(132, 26)
        Me.zlbl_1.Name = "zlbl_1"
        Me.zlbl_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_1.Size = New System.Drawing.Size(89, 19)
        Me.zlbl_1.TabIndex = 23
        Me.zlbl_1.Text = "Mount face"
        Me.zlbl_1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_5
        '
        Me.zlbl_5.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_5.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_5.Location = New System.Drawing.Point(168, 114)
        Me.zlbl_5.Name = "zlbl_5"
        Me.zlbl_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_5.Size = New System.Drawing.Size(53, 16)
        Me.zlbl_5.TabIndex = 22
        Me.zlbl_5.Text = "Length"
        Me.zlbl_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_6
        '
        Me.zlbl_6.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_6.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_6.Location = New System.Drawing.Point(175, 144)
        Me.zlbl_6.Name = "zlbl_6"
        Me.zlbl_6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_6.Size = New System.Drawing.Size(46, 16)
        Me.zlbl_6.TabIndex = 21
        Me.zlbl_6.Text = "Width"
        Me.zlbl_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'mass_lblPanelOrient
        '
        Me.mass_lblPanelOrient.BackColor = System.Drawing.SystemColors.Control
        Me.mass_lblPanelOrient.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_lblPanelOrient.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblPanelOrient.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_lblPanelOrient.Location = New System.Drawing.Point(132, 54)
        Me.mass_lblPanelOrient.Name = "mass_lblPanelOrient"
        Me.mass_lblPanelOrient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_lblPanelOrient.Size = New System.Drawing.Size(89, 19)
        Me.mass_lblPanelOrient.TabIndex = 20
        Me.mass_lblPanelOrient.Text = "Orientation"
        Me.mass_lblPanelOrient.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_lblPanelPos
        '
        Me.mass_lblPanelPos.BackColor = System.Drawing.SystemColors.Control
        Me.mass_lblPanelPos.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_lblPanelPos.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblPanelPos.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_lblPanelPos.Location = New System.Drawing.Point(147, 84)
        Me.mass_lblPanelPos.Name = "mass_lblPanelPos"
        Me.mass_lblPanelPos.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_lblPanelPos.Size = New System.Drawing.Size(73, 19)
        Me.mass_lblPanelPos.TabIndex = 19
        Me.mass_lblPanelPos.Text = "Position"
        Me.mass_lblPanelPos.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_3
        '
        Me.zlbl_3.AutoSize = True
        Me.zlbl_3.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_3.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_3.Location = New System.Drawing.Point(35, 67)
        Me.zlbl_3.Name = "zlbl_3"
        Me.zlbl_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_3.Size = New System.Drawing.Size(51, 16)
        Me.zlbl_3.TabIndex = 13
        Me.zlbl_3.Text = "Type :"
        '
        'zpict_5
        '
        Me.zpict_5.Cursor = System.Windows.Forms.Cursors.Default
        Me.zpict_5.Image = CType(resources.GetObject("zpict_5.Image"), System.Drawing.Image)
        Me.zpict_5.Location = New System.Drawing.Point(28, 95)
        Me.zpict_5.Name = "zpict_5"
        Me.zpict_5.Size = New System.Drawing.Size(108, 79)
        Me.zpict_5.TabIndex = 24
        Me.zpict_5.TabStop = False
        '
        'zfra_3
        '
        Me.zfra_3.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_3.Controls.Add(Me.zlbl_11)
        Me.zfra_3.Controls.Add(Me.mass_txtBodyMass)
        Me.zfra_3.Controls.Add(Me.zlbl_15)
        Me.zfra_3.Controls.Add(Me.zfra_7)
        Me.zfra_3.Controls.Add(Me.zfra_6)
        Me.zfra_3.Controls.Add(Me.zfra_5)
        Me.zfra_3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_3.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_3.Location = New System.Drawing.Point(15, 15)
        Me.zfra_3.Name = "zfra_3"
        Me.zfra_3.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_3.Size = New System.Drawing.Size(592, 182)
        Me.zfra_3.TabIndex = 11
        Me.zfra_3.TabStop = False
        Me.zfra_3.Text = "Central body"
        '
        'zlbl_11
        '
        Me.zlbl_11.AutoSize = True
        Me.zlbl_11.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_11.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_11.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_11.Location = New System.Drawing.Point(345, 25)
        Me.zlbl_11.Name = "zlbl_11"
        Me.zlbl_11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_11.Size = New System.Drawing.Size(23, 16)
        Me.zlbl_11.TabIndex = 39
        Me.zlbl_11.Text = "kg"
        Me.zlbl_11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_txtBodyMass
        '
        Me.mass_txtBodyMass.AcceptsReturn = True
        Me.mass_txtBodyMass.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtBodyMass.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtBodyMass.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtBodyMass.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtBodyMass.Location = New System.Drawing.Point(281, 22)
        Me.mass_txtBodyMass.MaxLength = 0
        Me.mass_txtBodyMass.Name = "mass_txtBodyMass"
        Me.mass_txtBodyMass.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtBodyMass.Size = New System.Drawing.Size(61, 23)
        Me.mass_txtBodyMass.TabIndex = 1
        Me.mass_txtBodyMass.Text = "50"
        '
        'zlbl_15
        '
        Me.zlbl_15.AutoSize = True
        Me.zlbl_15.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_15.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_15.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_15.Location = New System.Drawing.Point(236, 25)
        Me.zlbl_15.Name = "zlbl_15"
        Me.zlbl_15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_15.Size = New System.Drawing.Size(41, 16)
        Me.zlbl_15.TabIndex = 37
        Me.zlbl_15.Text = "Mass"
        Me.zlbl_15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_7
        '
        Me.zfra_7.Controls.Add(Me.mass_txtGy)
        Me.zfra_7.Controls.Add(Me.zlbl_74)
        Me.zfra_7.Controls.Add(Me.zlbl_83)
        Me.zfra_7.Controls.Add(Me.mass_txtGx)
        Me.zfra_7.Controls.Add(Me.zlbl_78)
        Me.zfra_7.Controls.Add(Me.zlbl_82)
        Me.zfra_7.Controls.Add(Me.mass_txtGz)
        Me.zfra_7.Controls.Add(Me.zlbl_79)
        Me.zfra_7.Controls.Add(Me.zlbl_80)
        Me.zfra_7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_7.ForeColor = System.Drawing.Color.Black
        Me.zfra_7.Location = New System.Drawing.Point(176, 55)
        Me.zfra_7.Name = "zfra_7"
        Me.zfra_7.Size = New System.Drawing.Size(128, 117)
        Me.zfra_7.TabIndex = 36
        Me.zfra_7.TabStop = False
        Me.zfra_7.Text = "C.G location"
        '
        'mass_txtGy
        '
        Me.mass_txtGy.AcceptsReturn = True
        Me.mass_txtGy.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtGy.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtGy.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtGy.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtGy.Location = New System.Drawing.Point(37, 54)
        Me.mass_txtGy.MaxLength = 0
        Me.mass_txtGy.Name = "mass_txtGy"
        Me.mass_txtGy.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtGy.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtGy.TabIndex = 5
        Me.mass_txtGy.Text = "0"
        '
        'zlbl_74
        '
        Me.zlbl_74.AutoSize = True
        Me.zlbl_74.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_74.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_74.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_74.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_74.Location = New System.Drawing.Point(7, 28)
        Me.zlbl_74.Name = "zlbl_74"
        Me.zlbl_74.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_74.Size = New System.Drawing.Size(24, 16)
        Me.zlbl_74.TabIndex = 27
        Me.zlbl_74.Text = "Gx"
        Me.zlbl_74.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_83
        '
        Me.zlbl_83.AutoSize = True
        Me.zlbl_83.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_83.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_83.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_83.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_83.Location = New System.Drawing.Point(7, 57)
        Me.zlbl_83.Name = "zlbl_83"
        Me.zlbl_83.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_83.Size = New System.Drawing.Size(25, 16)
        Me.zlbl_83.TabIndex = 29
        Me.zlbl_83.Text = "Gy"
        Me.zlbl_83.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'mass_txtGx
        '
        Me.mass_txtGx.AcceptsReturn = True
        Me.mass_txtGx.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtGx.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtGx.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtGx.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtGx.Location = New System.Drawing.Point(37, 25)
        Me.mass_txtGx.MaxLength = 0
        Me.mass_txtGx.Name = "mass_txtGx"
        Me.mass_txtGx.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtGx.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtGx.TabIndex = 4
        Me.mass_txtGx.Text = "0"
        '
        'zlbl_78
        '
        Me.zlbl_78.AutoSize = True
        Me.zlbl_78.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_78.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_78.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_78.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_78.Location = New System.Drawing.Point(92, 86)
        Me.zlbl_78.Name = "zlbl_78"
        Me.zlbl_78.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_78.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_78.TabIndex = 33
        Me.zlbl_78.Text = "m"
        Me.zlbl_78.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_82
        '
        Me.zlbl_82.AutoSize = True
        Me.zlbl_82.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_82.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_82.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_82.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_82.Location = New System.Drawing.Point(7, 86)
        Me.zlbl_82.Name = "zlbl_82"
        Me.zlbl_82.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_82.Size = New System.Drawing.Size(24, 16)
        Me.zlbl_82.TabIndex = 28
        Me.zlbl_82.Text = "Gz"
        Me.zlbl_82.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'mass_txtGz
        '
        Me.mass_txtGz.AcceptsReturn = True
        Me.mass_txtGz.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtGz.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtGz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtGz.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtGz.Location = New System.Drawing.Point(37, 83)
        Me.mass_txtGz.MaxLength = 0
        Me.mass_txtGz.Name = "mass_txtGz"
        Me.mass_txtGz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtGz.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtGz.TabIndex = 6
        Me.mass_txtGz.Text = "0"
        '
        'zlbl_79
        '
        Me.zlbl_79.AutoSize = True
        Me.zlbl_79.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_79.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_79.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_79.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_79.Location = New System.Drawing.Point(92, 57)
        Me.zlbl_79.Name = "zlbl_79"
        Me.zlbl_79.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_79.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_79.TabIndex = 34
        Me.zlbl_79.Text = "m"
        Me.zlbl_79.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_80
        '
        Me.zlbl_80.AutoSize = True
        Me.zlbl_80.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_80.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_80.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_80.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_80.Location = New System.Drawing.Point(92, 28)
        Me.zlbl_80.Name = "zlbl_80"
        Me.zlbl_80.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_80.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_80.TabIndex = 35
        Me.zlbl_80.Text = "m"
        Me.zlbl_80.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zfra_6
        '
        Me.zfra_6.Controls.Add(Me.mass_txtIyz)
        Me.zfra_6.Controls.Add(Me.zlbl_85)
        Me.zfra_6.Controls.Add(Me.zlbl_86)
        Me.zfra_6.Controls.Add(Me.zlbl_14)
        Me.zfra_6.Controls.Add(Me.mass_txtIyy)
        Me.zfra_6.Controls.Add(Me.zlbl_13)
        Me.zfra_6.Controls.Add(Me.zlbl_84)
        Me.zfra_6.Controls.Add(Me.zlbl_12)
        Me.zfra_6.Controls.Add(Me.mass_txtIzz)
        Me.zfra_6.Controls.Add(Me.mass_txtIxz)
        Me.zfra_6.Controls.Add(Me.mass_txtIxx)
        Me.zfra_6.Controls.Add(Me.mass_txtIxy)
        Me.zfra_6.Controls.Add(Me.zlbl_87)
        Me.zfra_6.Controls.Add(Me.zlbl_10)
        Me.zfra_6.Controls.Add(Me.zlbl_88)
        Me.zfra_6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_6.ForeColor = System.Drawing.Color.Black
        Me.zfra_6.Location = New System.Drawing.Point(324, 55)
        Me.zfra_6.Name = "zfra_6"
        Me.zfra_6.Size = New System.Drawing.Size(255, 117)
        Me.zfra_6.TabIndex = 36
        Me.zfra_6.TabStop = False
        Me.zfra_6.Text = "Inertias"
        '
        'mass_txtIyz
        '
        Me.mass_txtIyz.AcceptsReturn = True
        Me.mass_txtIyz.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtIyz.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtIyz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtIyz.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtIyz.Location = New System.Drawing.Point(142, 25)
        Me.mass_txtIyz.MaxLength = 0
        Me.mass_txtIyz.Name = "mass_txtIyz"
        Me.mass_txtIyz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtIyz.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtIyz.TabIndex = 10
        Me.mass_txtIyz.Text = "0"
        '
        'zlbl_85
        '
        Me.zlbl_85.AutoSize = True
        Me.zlbl_85.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_85.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_85.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_85.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_85.Location = New System.Drawing.Point(17, 57)
        Me.zlbl_85.Name = "zlbl_85"
        Me.zlbl_85.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_85.Size = New System.Drawing.Size(29, 16)
        Me.zlbl_85.TabIndex = 5
        Me.zlbl_85.Text = "Iyy"
        Me.zlbl_85.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_86
        '
        Me.zlbl_86.AutoSize = True
        Me.zlbl_86.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_86.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_86.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_86.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_86.Location = New System.Drawing.Point(17, 86)
        Me.zlbl_86.Name = "zlbl_86"
        Me.zlbl_86.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_86.Size = New System.Drawing.Size(27, 16)
        Me.zlbl_86.TabIndex = 6
        Me.zlbl_86.Text = "Izz"
        Me.zlbl_86.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_14
        '
        Me.zlbl_14.AutoSize = True
        Me.zlbl_14.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_14.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_14.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_14.Location = New System.Drawing.Point(199, 86)
        Me.zlbl_14.Name = "zlbl_14"
        Me.zlbl_14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_14.Size = New System.Drawing.Size(46, 16)
        Me.zlbl_14.TabIndex = 33
        Me.zlbl_14.Text = "kg.m²"
        Me.zlbl_14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_txtIyy
        '
        Me.mass_txtIyy.AcceptsReturn = True
        Me.mass_txtIyy.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtIyy.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtIyy.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtIyy.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtIyy.Location = New System.Drawing.Point(44, 54)
        Me.mass_txtIyy.MaxLength = 0
        Me.mass_txtIyy.Name = "mass_txtIyy"
        Me.mass_txtIyy.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtIyy.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtIyy.TabIndex = 8
        Me.mass_txtIyy.Text = "1"
        '
        'zlbl_13
        '
        Me.zlbl_13.AutoSize = True
        Me.zlbl_13.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_13.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_13.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_13.Location = New System.Drawing.Point(199, 57)
        Me.zlbl_13.Name = "zlbl_13"
        Me.zlbl_13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_13.Size = New System.Drawing.Size(46, 16)
        Me.zlbl_13.TabIndex = 34
        Me.zlbl_13.Text = "kg.m²"
        Me.zlbl_13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_84
        '
        Me.zlbl_84.AutoSize = True
        Me.zlbl_84.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_84.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_84.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_84.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_84.Location = New System.Drawing.Point(16, 28)
        Me.zlbl_84.Name = "zlbl_84"
        Me.zlbl_84.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_84.Size = New System.Drawing.Size(27, 16)
        Me.zlbl_84.TabIndex = 6
        Me.zlbl_84.Text = "Ixx"
        Me.zlbl_84.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_12
        '
        Me.zlbl_12.AutoSize = True
        Me.zlbl_12.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_12.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_12.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_12.Location = New System.Drawing.Point(199, 28)
        Me.zlbl_12.Name = "zlbl_12"
        Me.zlbl_12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_12.Size = New System.Drawing.Size(46, 16)
        Me.zlbl_12.TabIndex = 35
        Me.zlbl_12.Text = "kg.m²"
        Me.zlbl_12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_txtIzz
        '
        Me.mass_txtIzz.AcceptsReturn = True
        Me.mass_txtIzz.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtIzz.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtIzz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtIzz.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtIzz.Location = New System.Drawing.Point(44, 83)
        Me.mass_txtIzz.MaxLength = 0
        Me.mass_txtIzz.Name = "mass_txtIzz"
        Me.mass_txtIzz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtIzz.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtIzz.TabIndex = 9
        Me.mass_txtIzz.Text = "1"
        '
        'mass_txtIxz
        '
        Me.mass_txtIxz.AcceptsReturn = True
        Me.mass_txtIxz.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtIxz.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtIxz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtIxz.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtIxz.Location = New System.Drawing.Point(142, 83)
        Me.mass_txtIxz.MaxLength = 0
        Me.mass_txtIxz.Name = "mass_txtIxz"
        Me.mass_txtIxz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtIxz.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtIxz.TabIndex = 12
        Me.mass_txtIxz.Text = "0"
        '
        'mass_txtIxx
        '
        Me.mass_txtIxx.AcceptsReturn = True
        Me.mass_txtIxx.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtIxx.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtIxx.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtIxx.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtIxx.Location = New System.Drawing.Point(44, 25)
        Me.mass_txtIxx.MaxLength = 0
        Me.mass_txtIxx.Name = "mass_txtIxx"
        Me.mass_txtIxx.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtIxx.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtIxx.TabIndex = 7
        Me.mass_txtIxx.Text = "1"
        '
        'mass_txtIxy
        '
        Me.mass_txtIxy.AcceptsReturn = True
        Me.mass_txtIxy.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtIxy.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtIxy.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtIxy.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtIxy.Location = New System.Drawing.Point(142, 54)
        Me.mass_txtIxy.MaxLength = 0
        Me.mass_txtIxy.Name = "mass_txtIxy"
        Me.mass_txtIxy.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtIxy.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtIxy.TabIndex = 11
        Me.mass_txtIxy.Text = "0"
        '
        'zlbl_87
        '
        Me.zlbl_87.AutoSize = True
        Me.zlbl_87.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_87.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_87.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_87.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_87.Location = New System.Drawing.Point(112, 28)
        Me.zlbl_87.Name = "zlbl_87"
        Me.zlbl_87.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_87.Size = New System.Drawing.Size(28, 16)
        Me.zlbl_87.TabIndex = 27
        Me.zlbl_87.Text = "Iyz"
        Me.zlbl_87.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_10
        '
        Me.zlbl_10.AutoSize = True
        Me.zlbl_10.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_10.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_10.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_10.Location = New System.Drawing.Point(112, 86)
        Me.zlbl_10.Name = "zlbl_10"
        Me.zlbl_10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_10.Size = New System.Drawing.Size(27, 16)
        Me.zlbl_10.TabIndex = 28
        Me.zlbl_10.Text = "Ixz"
        Me.zlbl_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_88
        '
        Me.zlbl_88.AutoSize = True
        Me.zlbl_88.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_88.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_88.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_88.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_88.Location = New System.Drawing.Point(112, 57)
        Me.zlbl_88.Name = "zlbl_88"
        Me.zlbl_88.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_88.Size = New System.Drawing.Size(28, 16)
        Me.zlbl_88.TabIndex = 29
        Me.zlbl_88.Text = "Ixy"
        Me.zlbl_88.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_5
        '
        Me.zfra_5.Controls.Add(Me.zlbl_0)
        Me.zfra_5.Controls.Add(Me.zlbl_2)
        Me.zfra_5.Controls.Add(Me.mass_txtBodyWidth)
        Me.zfra_5.Controls.Add(Me.mass_txtBodyHeight)
        Me.zfra_5.Controls.Add(Me.zlbl_65)
        Me.zfra_5.Controls.Add(Me.zlbl_66)
        Me.zfra_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_5.ForeColor = System.Drawing.Color.Black
        Me.zfra_5.Location = New System.Drawing.Point(14, 55)
        Me.zfra_5.Name = "zfra_5"
        Me.zfra_5.Size = New System.Drawing.Size(143, 117)
        Me.zfra_5.TabIndex = 36
        Me.zfra_5.TabStop = False
        Me.zfra_5.Text = "Size"
        '
        'zlbl_0
        '
        Me.zlbl_0.AutoSize = True
        Me.zlbl_0.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_0.Location = New System.Drawing.Point(15, 40)
        Me.zlbl_0.Name = "zlbl_0"
        Me.zlbl_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_0.Size = New System.Drawing.Size(46, 16)
        Me.zlbl_0.TabIndex = 5
        Me.zlbl_0.Text = "Width"
        Me.zlbl_0.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_2
        '
        Me.zlbl_2.AutoSize = True
        Me.zlbl_2.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_2.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_2.Location = New System.Drawing.Point(11, 69)
        Me.zlbl_2.Name = "zlbl_2"
        Me.zlbl_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_2.Size = New System.Drawing.Size(50, 16)
        Me.zlbl_2.TabIndex = 6
        Me.zlbl_2.Text = "Height"
        Me.zlbl_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'mass_txtBodyWidth
        '
        Me.mass_txtBodyWidth.AcceptsReturn = True
        Me.mass_txtBodyWidth.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtBodyWidth.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtBodyWidth.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtBodyWidth.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtBodyWidth.Location = New System.Drawing.Point(61, 37)
        Me.mass_txtBodyWidth.MaxLength = 0
        Me.mass_txtBodyWidth.Name = "mass_txtBodyWidth"
        Me.mass_txtBodyWidth.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtBodyWidth.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtBodyWidth.TabIndex = 2
        Me.mass_txtBodyWidth.Text = "1"
        '
        'mass_txtBodyHeight
        '
        Me.mass_txtBodyHeight.AcceptsReturn = True
        Me.mass_txtBodyHeight.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtBodyHeight.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtBodyHeight.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtBodyHeight.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtBodyHeight.Location = New System.Drawing.Point(61, 66)
        Me.mass_txtBodyHeight.MaxLength = 0
        Me.mass_txtBodyHeight.Name = "mass_txtBodyHeight"
        Me.mass_txtBodyHeight.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtBodyHeight.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtBodyHeight.TabIndex = 3
        Me.mass_txtBodyHeight.Text = "1"
        '
        'zlbl_65
        '
        Me.zlbl_65.AutoSize = True
        Me.zlbl_65.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_65.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_65.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_65.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_65.Location = New System.Drawing.Point(114, 40)
        Me.zlbl_65.Name = "zlbl_65"
        Me.zlbl_65.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_65.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_65.TabIndex = 26
        Me.zlbl_65.Text = "m"
        Me.zlbl_65.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_66
        '
        Me.zlbl_66.AutoSize = True
        Me.zlbl_66.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_66.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_66.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_66.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_66.Location = New System.Drawing.Point(114, 69)
        Me.zlbl_66.Name = "zlbl_66"
        Me.zlbl_66.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_66.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_66.TabIndex = 26
        Me.zlbl_66.Text = "m"
        Me.zlbl_66.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Onglet_att_prop
        '
        Me.Onglet_att_prop.BackColor = System.Drawing.Color.Transparent
        Me.Onglet_att_prop.Controls.Add(Me.zfra_14)
        Me.Onglet_att_prop.Controls.Add(Me.zfra_11)
        Me.Onglet_att_prop.Controls.Add(Me.zfra_1)
        Me.Onglet_att_prop.Location = New System.Drawing.Point(4, 32)
        Me.Onglet_att_prop.Name = "Onglet_att_prop"
        Me.Onglet_att_prop.Size = New System.Drawing.Size(627, 520)
        Me.Onglet_att_prop.TabIndex = 1
        Me.Onglet_att_prop.Text = "Satellite properties"
        '
        'zfra_14
        '
        Me.zfra_14.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_14.Controls.Add(Me.zlbl102)
        Me.zfra_14.Controls.Add(Me.zlbl101)
        Me.zfra_14.Controls.Add(Me.zlbl100)
        Me.zfra_14.Controls.Add(Me.prop_txtMG_X)
        Me.zfra_14.Controls.Add(Me.prop_txtMG_Y)
        Me.zfra_14.Controls.Add(Me.zlbl34)
        Me.zfra_14.Controls.Add(Me.zlbl31)
        Me.zfra_14.Controls.Add(Me.zlbl35)
        Me.zfra_14.Controls.Add(Me.prop_txtMG_Z)
        Me.zfra_14.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_14.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_14.Location = New System.Drawing.Point(16, 328)
        Me.zfra_14.Name = "zfra_14"
        Me.zfra_14.Size = New System.Drawing.Size(192, 131)
        Me.zfra_14.TabIndex = 37
        Me.zfra_14.TabStop = False
        Me.zfra_14.Text = "Magnetic dipole"
        '
        'zlbl102
        '
        Me.zlbl102.AutoSize = True
        Me.zlbl102.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl102.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl102.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl102.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl102.Location = New System.Drawing.Point(124, 102)
        Me.zlbl102.Name = "zlbl102"
        Me.zlbl102.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl102.Size = New System.Drawing.Size(40, 16)
        Me.zlbl102.TabIndex = 38
        Me.zlbl102.Text = "A.m²"
        Me.zlbl102.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl101
        '
        Me.zlbl101.AutoSize = True
        Me.zlbl101.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl101.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl101.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl101.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl101.Location = New System.Drawing.Point(124, 66)
        Me.zlbl101.Name = "zlbl101"
        Me.zlbl101.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl101.Size = New System.Drawing.Size(40, 16)
        Me.zlbl101.TabIndex = 37
        Me.zlbl101.Text = "A.m²"
        Me.zlbl101.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl100
        '
        Me.zlbl100.AutoSize = True
        Me.zlbl100.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl100.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl100.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl100.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl100.Location = New System.Drawing.Point(124, 29)
        Me.zlbl100.Name = "zlbl100"
        Me.zlbl100.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl100.Size = New System.Drawing.Size(40, 16)
        Me.zlbl100.TabIndex = 36
        Me.zlbl100.Text = "A.m²"
        Me.zlbl100.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'prop_txtMG_X
        '
        Me.prop_txtMG_X.AcceptsReturn = True
        Me.prop_txtMG_X.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtMG_X.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtMG_X.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtMG_X.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtMG_X.Location = New System.Drawing.Point(68, 26)
        Me.prop_txtMG_X.MaxLength = 0
        Me.prop_txtMG_X.Name = "prop_txtMG_X"
        Me.prop_txtMG_X.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtMG_X.Size = New System.Drawing.Size(51, 23)
        Me.prop_txtMG_X.TabIndex = 51
        Me.prop_txtMG_X.Text = "1.732"
        '
        'prop_txtMG_Y
        '
        Me.prop_txtMG_Y.AcceptsReturn = True
        Me.prop_txtMG_Y.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtMG_Y.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtMG_Y.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtMG_Y.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtMG_Y.Location = New System.Drawing.Point(68, 63)
        Me.prop_txtMG_Y.MaxLength = 0
        Me.prop_txtMG_Y.Name = "prop_txtMG_Y"
        Me.prop_txtMG_Y.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtMG_Y.Size = New System.Drawing.Size(51, 23)
        Me.prop_txtMG_Y.TabIndex = 52
        Me.prop_txtMG_Y.Text = "1.732"
        '
        'zlbl34
        '
        Me.zlbl34.AutoSize = True
        Me.zlbl34.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl34.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl34.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl34.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl34.Location = New System.Drawing.Point(34, 66)
        Me.zlbl34.Name = "zlbl34"
        Me.zlbl34.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl34.Size = New System.Drawing.Size(26, 16)
        Me.zlbl34.TabIndex = 6
        Me.zlbl34.Text = "+Y"
        Me.zlbl34.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl31
        '
        Me.zlbl31.AutoSize = True
        Me.zlbl31.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl31.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl31.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl31.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl31.Location = New System.Drawing.Point(33, 102)
        Me.zlbl31.Name = "zlbl31"
        Me.zlbl31.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl31.Size = New System.Drawing.Size(26, 16)
        Me.zlbl31.TabIndex = 29
        Me.zlbl31.Text = "+Z"
        Me.zlbl31.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl35
        '
        Me.zlbl35.AutoSize = True
        Me.zlbl35.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl35.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl35.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl35.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl35.Location = New System.Drawing.Point(34, 29)
        Me.zlbl35.Name = "zlbl35"
        Me.zlbl35.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl35.Size = New System.Drawing.Size(26, 16)
        Me.zlbl35.TabIndex = 6
        Me.zlbl35.Text = "+X"
        Me.zlbl35.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtMG_Z
        '
        Me.prop_txtMG_Z.AcceptsReturn = True
        Me.prop_txtMG_Z.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtMG_Z.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtMG_Z.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtMG_Z.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtMG_Z.Location = New System.Drawing.Point(68, 99)
        Me.prop_txtMG_Z.MaxLength = 0
        Me.prop_txtMG_Z.Name = "prop_txtMG_Z"
        Me.prop_txtMG_Z.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtMG_Z.Size = New System.Drawing.Size(51, 23)
        Me.prop_txtMG_Z.TabIndex = 53
        Me.prop_txtMG_Z.Text = "1.732"
        '
        'zfra_11
        '
        Me.zfra_11.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_11.Controls.Add(Me.zfra_12)
        Me.zfra_11.Controls.Add(Me.zfra_13)
        Me.zfra_11.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_11.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_11.Location = New System.Drawing.Point(323, 29)
        Me.zfra_11.Name = "zfra_11"
        Me.zfra_11.Size = New System.Drawing.Size(288, 268)
        Me.zfra_11.TabIndex = 37
        Me.zfra_11.TabStop = False
        Me.zfra_11.Text = "Sun radiation coefficients"
        '
        'zfra_12
        '
        Me.zfra_12.Controls.Add(Me.prop_txtCD_0)
        Me.zfra_12.Controls.Add(Me.prop_txtCD_4)
        Me.zfra_12.Controls.Add(Me.zlbl24)
        Me.zfra_12.Controls.Add(Me.prop_txtCD_3)
        Me.zfra_12.Controls.Add(Me.prop_txtCD_2)
        Me.zfra_12.Controls.Add(Me.zlbl23)
        Me.zfra_12.Controls.Add(Me.prop_txtCD_5)
        Me.zfra_12.Controls.Add(Me.prop_txtCD_1)
        Me.zfra_12.Controls.Add(Me.zlbl22)
        Me.zfra_12.Controls.Add(Me.zlbl19)
        Me.zfra_12.Controls.Add(Me.zlbl20)
        Me.zfra_12.Controls.Add(Me.zlbl21)
        Me.zfra_12.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_12.ForeColor = System.Drawing.Color.Black
        Me.zfra_12.Location = New System.Drawing.Point(151, 34)
        Me.zfra_12.Name = "zfra_12"
        Me.zfra_12.Size = New System.Drawing.Size(119, 214)
        Me.zfra_12.TabIndex = 36
        Me.zfra_12.TabStop = False
        Me.zfra_12.Text = "Diffuse"
        '
        'prop_txtCD_0
        '
        Me.prop_txtCD_0.AcceptsReturn = True
        Me.prop_txtCD_0.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCD_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCD_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCD_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCD_0.Location = New System.Drawing.Point(52, 28)
        Me.prop_txtCD_0.MaxLength = 0
        Me.prop_txtCD_0.Name = "prop_txtCD_0"
        Me.prop_txtCD_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCD_0.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCD_0.TabIndex = 45
        Me.prop_txtCD_0.Text = "0.05"
        '
        'prop_txtCD_4
        '
        Me.prop_txtCD_4.AcceptsReturn = True
        Me.prop_txtCD_4.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCD_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCD_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCD_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCD_4.Location = New System.Drawing.Point(52, 115)
        Me.prop_txtCD_4.MaxLength = 0
        Me.prop_txtCD_4.Name = "prop_txtCD_4"
        Me.prop_txtCD_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCD_4.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCD_4.TabIndex = 48
        Me.prop_txtCD_4.Text = "0.18"
        '
        'zlbl24
        '
        Me.zlbl24.AutoSize = True
        Me.zlbl24.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl24.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl24.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl24.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl24.Location = New System.Drawing.Point(20, 147)
        Me.zlbl24.Name = "zlbl24"
        Me.zlbl24.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl24.Size = New System.Drawing.Size(26, 16)
        Me.zlbl24.TabIndex = 29
        Me.zlbl24.Text = "+Z"
        Me.zlbl24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCD_3
        '
        Me.prop_txtCD_3.AcceptsReturn = True
        Me.prop_txtCD_3.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCD_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCD_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCD_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCD_3.Location = New System.Drawing.Point(52, 57)
        Me.prop_txtCD_3.MaxLength = 0
        Me.prop_txtCD_3.Name = "prop_txtCD_3"
        Me.prop_txtCD_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCD_3.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCD_3.TabIndex = 46
        Me.prop_txtCD_3.Text = "0.04"
        '
        'prop_txtCD_2
        '
        Me.prop_txtCD_2.AcceptsReturn = True
        Me.prop_txtCD_2.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCD_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCD_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCD_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCD_2.Location = New System.Drawing.Point(52, 144)
        Me.prop_txtCD_2.MaxLength = 0
        Me.prop_txtCD_2.Name = "prop_txtCD_2"
        Me.prop_txtCD_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCD_2.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCD_2.TabIndex = 49
        Me.prop_txtCD_2.Text = "0.05"
        '
        'zlbl23
        '
        Me.zlbl23.AutoSize = True
        Me.zlbl23.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl23.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl23.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl23.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl23.Location = New System.Drawing.Point(19, 60)
        Me.zlbl23.Name = "zlbl23"
        Me.zlbl23.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl23.Size = New System.Drawing.Size(23, 16)
        Me.zlbl23.TabIndex = 5
        Me.zlbl23.Text = "-X"
        Me.zlbl23.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCD_5
        '
        Me.prop_txtCD_5.AcceptsReturn = True
        Me.prop_txtCD_5.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCD_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCD_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCD_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCD_5.Location = New System.Drawing.Point(52, 173)
        Me.prop_txtCD_5.MaxLength = 0
        Me.prop_txtCD_5.Name = "prop_txtCD_5"
        Me.prop_txtCD_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCD_5.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCD_5.TabIndex = 50
        Me.prop_txtCD_5.Text = "0.04"
        '
        'prop_txtCD_1
        '
        Me.prop_txtCD_1.AcceptsReturn = True
        Me.prop_txtCD_1.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCD_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCD_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCD_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCD_1.Location = New System.Drawing.Point(52, 86)
        Me.prop_txtCD_1.MaxLength = 0
        Me.prop_txtCD_1.Name = "prop_txtCD_1"
        Me.prop_txtCD_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCD_1.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCD_1.TabIndex = 47
        Me.prop_txtCD_1.Text = "0.2"
        '
        'zlbl22
        '
        Me.zlbl22.AutoSize = True
        Me.zlbl22.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl22.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl22.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl22.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl22.Location = New System.Drawing.Point(20, 176)
        Me.zlbl22.Name = "zlbl22"
        Me.zlbl22.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl22.Size = New System.Drawing.Size(24, 16)
        Me.zlbl22.TabIndex = 28
        Me.zlbl22.Text = "-Z"
        Me.zlbl22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl19
        '
        Me.zlbl19.AutoSize = True
        Me.zlbl19.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl19.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl19.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl19.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl19.Location = New System.Drawing.Point(20, 118)
        Me.zlbl19.Name = "zlbl19"
        Me.zlbl19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl19.Size = New System.Drawing.Size(23, 16)
        Me.zlbl19.TabIndex = 27
        Me.zlbl19.Text = "-Y"
        Me.zlbl19.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl20
        '
        Me.zlbl20.AutoSize = True
        Me.zlbl20.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl20.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl20.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl20.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl20.Location = New System.Drawing.Point(18, 31)
        Me.zlbl20.Name = "zlbl20"
        Me.zlbl20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl20.Size = New System.Drawing.Size(26, 16)
        Me.zlbl20.TabIndex = 6
        Me.zlbl20.Text = "+X"
        Me.zlbl20.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl21
        '
        Me.zlbl21.AutoSize = True
        Me.zlbl21.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl21.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl21.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl21.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl21.Location = New System.Drawing.Point(19, 89)
        Me.zlbl21.Name = "zlbl21"
        Me.zlbl21.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl21.Size = New System.Drawing.Size(26, 16)
        Me.zlbl21.TabIndex = 6
        Me.zlbl21.Text = "+Y"
        Me.zlbl21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_13
        '
        Me.zfra_13.Controls.Add(Me.prop_txtCR_0)
        Me.zfra_13.Controls.Add(Me.prop_txtCR_4)
        Me.zfra_13.Controls.Add(Me.zlbl18)
        Me.zfra_13.Controls.Add(Me.prop_txtCR_3)
        Me.zfra_13.Controls.Add(Me.zlbl17)
        Me.zfra_13.Controls.Add(Me.prop_txtCR_2)
        Me.zfra_13.Controls.Add(Me.prop_txtCR_5)
        Me.zfra_13.Controls.Add(Me.zlbl16)
        Me.zfra_13.Controls.Add(Me.prop_txtCR_1)
        Me.zfra_13.Controls.Add(Me.zlbl15)
        Me.zfra_13.Controls.Add(Me.zlbl13)
        Me.zfra_13.Controls.Add(Me.zlbl14)
        Me.zfra_13.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_13.ForeColor = System.Drawing.Color.Black
        Me.zfra_13.Location = New System.Drawing.Point(16, 34)
        Me.zfra_13.Name = "zfra_13"
        Me.zfra_13.Size = New System.Drawing.Size(119, 214)
        Me.zfra_13.TabIndex = 36
        Me.zfra_13.TabStop = False
        Me.zfra_13.Text = "Reflective"
        '
        'prop_txtCR_0
        '
        Me.prop_txtCR_0.AcceptsReturn = True
        Me.prop_txtCR_0.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCR_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCR_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCR_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCR_0.Location = New System.Drawing.Point(52, 28)
        Me.prop_txtCR_0.MaxLength = 0
        Me.prop_txtCR_0.Name = "prop_txtCR_0"
        Me.prop_txtCR_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCR_0.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCR_0.TabIndex = 39
        Me.prop_txtCR_0.Text = "0.9"
        '
        'prop_txtCR_4
        '
        Me.prop_txtCR_4.AcceptsReturn = True
        Me.prop_txtCR_4.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCR_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCR_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCR_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCR_4.Location = New System.Drawing.Point(52, 115)
        Me.prop_txtCR_4.MaxLength = 0
        Me.prop_txtCR_4.Name = "prop_txtCR_4"
        Me.prop_txtCR_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCR_4.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCR_4.TabIndex = 42
        Me.prop_txtCR_4.Text = "0.45"
        '
        'zlbl18
        '
        Me.zlbl18.AutoSize = True
        Me.zlbl18.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl18.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl18.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl18.Location = New System.Drawing.Point(20, 147)
        Me.zlbl18.Name = "zlbl18"
        Me.zlbl18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl18.Size = New System.Drawing.Size(26, 16)
        Me.zlbl18.TabIndex = 29
        Me.zlbl18.Text = "+Z"
        Me.zlbl18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCR_3
        '
        Me.prop_txtCR_3.AcceptsReturn = True
        Me.prop_txtCR_3.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCR_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCR_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCR_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCR_3.Location = New System.Drawing.Point(52, 57)
        Me.prop_txtCR_3.MaxLength = 0
        Me.prop_txtCR_3.Name = "prop_txtCR_3"
        Me.prop_txtCR_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCR_3.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCR_3.TabIndex = 4
        Me.prop_txtCR_3.Text = "0.81"
        '
        'zlbl17
        '
        Me.zlbl17.AutoSize = True
        Me.zlbl17.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl17.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl17.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl17.Location = New System.Drawing.Point(19, 60)
        Me.zlbl17.Name = "zlbl17"
        Me.zlbl17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl17.Size = New System.Drawing.Size(23, 16)
        Me.zlbl17.TabIndex = 5
        Me.zlbl17.Text = "-X"
        Me.zlbl17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCR_2
        '
        Me.prop_txtCR_2.AcceptsReturn = True
        Me.prop_txtCR_2.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCR_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCR_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCR_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCR_2.Location = New System.Drawing.Point(52, 144)
        Me.prop_txtCR_2.MaxLength = 0
        Me.prop_txtCR_2.Name = "prop_txtCR_2"
        Me.prop_txtCR_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCR_2.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCR_2.TabIndex = 43
        Me.prop_txtCR_2.Text = "0.9"
        '
        'prop_txtCR_5
        '
        Me.prop_txtCR_5.AcceptsReturn = True
        Me.prop_txtCR_5.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCR_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCR_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCR_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCR_5.Location = New System.Drawing.Point(52, 173)
        Me.prop_txtCR_5.MaxLength = 0
        Me.prop_txtCR_5.Name = "prop_txtCR_5"
        Me.prop_txtCR_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCR_5.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCR_5.TabIndex = 44
        Me.prop_txtCR_5.Text = "0.81"
        '
        'zlbl16
        '
        Me.zlbl16.AutoSize = True
        Me.zlbl16.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl16.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl16.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl16.Location = New System.Drawing.Point(20, 176)
        Me.zlbl16.Name = "zlbl16"
        Me.zlbl16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl16.Size = New System.Drawing.Size(24, 16)
        Me.zlbl16.TabIndex = 28
        Me.zlbl16.Text = "-Z"
        Me.zlbl16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCR_1
        '
        Me.prop_txtCR_1.AcceptsReturn = True
        Me.prop_txtCR_1.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCR_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCR_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCR_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCR_1.Location = New System.Drawing.Point(52, 86)
        Me.prop_txtCR_1.MaxLength = 0
        Me.prop_txtCR_1.Name = "prop_txtCR_1"
        Me.prop_txtCR_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCR_1.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCR_1.TabIndex = 41
        Me.prop_txtCR_1.Text = "0.5"
        '
        'zlbl15
        '
        Me.zlbl15.AutoSize = True
        Me.zlbl15.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl15.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl15.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl15.Location = New System.Drawing.Point(19, 89)
        Me.zlbl15.Name = "zlbl15"
        Me.zlbl15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl15.Size = New System.Drawing.Size(26, 16)
        Me.zlbl15.TabIndex = 6
        Me.zlbl15.Text = "+Y"
        Me.zlbl15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl13
        '
        Me.zlbl13.AutoSize = True
        Me.zlbl13.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl13.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl13.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl13.Location = New System.Drawing.Point(18, 31)
        Me.zlbl13.Name = "zlbl13"
        Me.zlbl13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl13.Size = New System.Drawing.Size(26, 16)
        Me.zlbl13.TabIndex = 6
        Me.zlbl13.Text = "+X"
        Me.zlbl13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl14
        '
        Me.zlbl14.AutoSize = True
        Me.zlbl14.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl14.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl14.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl14.Location = New System.Drawing.Point(20, 118)
        Me.zlbl14.Name = "zlbl14"
        Me.zlbl14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl14.Size = New System.Drawing.Size(23, 16)
        Me.zlbl14.TabIndex = 27
        Me.zlbl14.Text = "-Y"
        Me.zlbl14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_1
        '
        Me.zfra_1.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_1.Controls.Add(Me.zfra_10)
        Me.zfra_1.Controls.Add(Me.zfra_2)
        Me.zfra_1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_1.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_1.Location = New System.Drawing.Point(16, 29)
        Me.zfra_1.Name = "zfra_1"
        Me.zfra_1.Size = New System.Drawing.Size(288, 268)
        Me.zfra_1.TabIndex = 37
        Me.zfra_1.TabStop = False
        Me.zfra_1.Text = "Pressure coefficients"
        '
        'zfra_10
        '
        Me.zfra_10.Controls.Add(Me.prop_txtCT_0)
        Me.zfra_10.Controls.Add(Me.prop_txtCT_4)
        Me.zfra_10.Controls.Add(Me.zlbl12)
        Me.zfra_10.Controls.Add(Me.zlbl11)
        Me.zfra_10.Controls.Add(Me.prop_txtCT_3)
        Me.zfra_10.Controls.Add(Me.zlbl10)
        Me.zfra_10.Controls.Add(Me.prop_txtCT_2)
        Me.zfra_10.Controls.Add(Me.zlbl6)
        Me.zfra_10.Controls.Add(Me.prop_txtCT_5)
        Me.zfra_10.Controls.Add(Me.zlbl4)
        Me.zfra_10.Controls.Add(Me.prop_txtCT_1)
        Me.zfra_10.Controls.Add(Me.zlbl3)
        Me.zfra_10.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_10.ForeColor = System.Drawing.Color.Black
        Me.zfra_10.Location = New System.Drawing.Point(151, 34)
        Me.zfra_10.Name = "zfra_10"
        Me.zfra_10.Size = New System.Drawing.Size(119, 214)
        Me.zfra_10.TabIndex = 36
        Me.zfra_10.TabStop = False
        Me.zfra_10.Text = "Tangential"
        '
        'prop_txtCT_0
        '
        Me.prop_txtCT_0.AcceptsReturn = True
        Me.prop_txtCT_0.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCT_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCT_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCT_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCT_0.Location = New System.Drawing.Point(52, 28)
        Me.prop_txtCT_0.MaxLength = 0
        Me.prop_txtCT_0.Name = "prop_txtCT_0"
        Me.prop_txtCT_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCT_0.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCT_0.TabIndex = 33
        Me.prop_txtCT_0.Text = "0.85"
        '
        'prop_txtCT_4
        '
        Me.prop_txtCT_4.AcceptsReturn = True
        Me.prop_txtCT_4.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCT_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCT_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCT_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCT_4.Location = New System.Drawing.Point(52, 115)
        Me.prop_txtCT_4.MaxLength = 0
        Me.prop_txtCT_4.Name = "prop_txtCT_4"
        Me.prop_txtCT_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCT_4.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCT_4.TabIndex = 36
        Me.prop_txtCT_4.Text = "0.27"
        '
        'zlbl12
        '
        Me.zlbl12.AutoSize = True
        Me.zlbl12.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl12.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl12.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl12.Location = New System.Drawing.Point(18, 147)
        Me.zlbl12.Name = "zlbl12"
        Me.zlbl12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl12.Size = New System.Drawing.Size(26, 16)
        Me.zlbl12.TabIndex = 29
        Me.zlbl12.Text = "+Z"
        Me.zlbl12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl11
        '
        Me.zlbl11.AutoSize = True
        Me.zlbl11.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl11.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl11.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl11.Location = New System.Drawing.Point(17, 60)
        Me.zlbl11.Name = "zlbl11"
        Me.zlbl11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl11.Size = New System.Drawing.Size(23, 16)
        Me.zlbl11.TabIndex = 5
        Me.zlbl11.Text = "-X"
        Me.zlbl11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCT_3
        '
        Me.prop_txtCT_3.AcceptsReturn = True
        Me.prop_txtCT_3.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCT_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCT_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCT_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCT_3.Location = New System.Drawing.Point(52, 57)
        Me.prop_txtCT_3.MaxLength = 0
        Me.prop_txtCT_3.Name = "prop_txtCT_3"
        Me.prop_txtCT_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCT_3.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCT_3.TabIndex = 34
        Me.prop_txtCT_3.Text = "0.77"
        '
        'zlbl10
        '
        Me.zlbl10.AutoSize = True
        Me.zlbl10.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl10.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl10.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl10.Location = New System.Drawing.Point(18, 176)
        Me.zlbl10.Name = "zlbl10"
        Me.zlbl10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl10.Size = New System.Drawing.Size(24, 16)
        Me.zlbl10.TabIndex = 28
        Me.zlbl10.Text = "-Z"
        Me.zlbl10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCT_2
        '
        Me.prop_txtCT_2.AcceptsReturn = True
        Me.prop_txtCT_2.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCT_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCT_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCT_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCT_2.Location = New System.Drawing.Point(52, 144)
        Me.prop_txtCT_2.MaxLength = 0
        Me.prop_txtCT_2.Name = "prop_txtCT_2"
        Me.prop_txtCT_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCT_2.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCT_2.TabIndex = 37
        Me.prop_txtCT_2.Text = "0.85"
        '
        'zlbl6
        '
        Me.zlbl6.AutoSize = True
        Me.zlbl6.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl6.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl6.Location = New System.Drawing.Point(17, 89)
        Me.zlbl6.Name = "zlbl6"
        Me.zlbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl6.Size = New System.Drawing.Size(26, 16)
        Me.zlbl6.TabIndex = 6
        Me.zlbl6.Text = "+Y"
        Me.zlbl6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCT_5
        '
        Me.prop_txtCT_5.AcceptsReturn = True
        Me.prop_txtCT_5.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCT_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCT_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCT_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCT_5.Location = New System.Drawing.Point(52, 173)
        Me.prop_txtCT_5.MaxLength = 0
        Me.prop_txtCT_5.Name = "prop_txtCT_5"
        Me.prop_txtCT_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCT_5.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCT_5.TabIndex = 38
        Me.prop_txtCT_5.Text = "0.77"
        '
        'zlbl4
        '
        Me.zlbl4.AutoSize = True
        Me.zlbl4.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl4.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl4.Location = New System.Drawing.Point(18, 118)
        Me.zlbl4.Name = "zlbl4"
        Me.zlbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl4.Size = New System.Drawing.Size(23, 16)
        Me.zlbl4.TabIndex = 27
        Me.zlbl4.Text = "-Y"
        Me.zlbl4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCT_1
        '
        Me.prop_txtCT_1.AcceptsReturn = True
        Me.prop_txtCT_1.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCT_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCT_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCT_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCT_1.Location = New System.Drawing.Point(52, 86)
        Me.prop_txtCT_1.MaxLength = 0
        Me.prop_txtCT_1.Name = "prop_txtCT_1"
        Me.prop_txtCT_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCT_1.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCT_1.TabIndex = 35
        Me.prop_txtCT_1.Text = "0.3"
        '
        'zlbl3
        '
        Me.zlbl3.AutoSize = True
        Me.zlbl3.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl3.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl3.Location = New System.Drawing.Point(16, 31)
        Me.zlbl3.Name = "zlbl3"
        Me.zlbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl3.Size = New System.Drawing.Size(26, 16)
        Me.zlbl3.TabIndex = 6
        Me.zlbl3.Text = "+X"
        Me.zlbl3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_2
        '
        Me.zfra_2.Controls.Add(Me.prop_txtCN_0)
        Me.zfra_2.Controls.Add(Me.prop_txtCN_4)
        Me.zfra_2.Controls.Add(Me.zlbl9)
        Me.zfra_2.Controls.Add(Me.zlbl1)
        Me.zfra_2.Controls.Add(Me.zlbl8)
        Me.zfra_2.Controls.Add(Me.zlbl2)
        Me.zfra_2.Controls.Add(Me.zlbl7)
        Me.zfra_2.Controls.Add(Me.prop_txtCN_3)
        Me.zfra_2.Controls.Add(Me.prop_txtCN_2)
        Me.zfra_2.Controls.Add(Me.zlbl5)
        Me.zfra_2.Controls.Add(Me.prop_txtCN_5)
        Me.zfra_2.Controls.Add(Me.prop_txtCN_1)
        Me.zfra_2.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_2.ForeColor = System.Drawing.Color.Black
        Me.zfra_2.Location = New System.Drawing.Point(16, 34)
        Me.zfra_2.Name = "zfra_2"
        Me.zfra_2.Size = New System.Drawing.Size(119, 214)
        Me.zfra_2.TabIndex = 36
        Me.zfra_2.TabStop = False
        Me.zfra_2.Text = "Normal"
        '
        'prop_txtCN_0
        '
        Me.prop_txtCN_0.AcceptsReturn = True
        Me.prop_txtCN_0.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCN_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCN_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCN_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCN_0.Location = New System.Drawing.Point(52, 28)
        Me.prop_txtCN_0.MaxLength = 0
        Me.prop_txtCN_0.Name = "prop_txtCN_0"
        Me.prop_txtCN_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCN_0.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCN_0.TabIndex = 27
        Me.prop_txtCN_0.Text = "0.9"
        '
        'prop_txtCN_4
        '
        Me.prop_txtCN_4.AcceptsReturn = True
        Me.prop_txtCN_4.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCN_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCN_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCN_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCN_4.Location = New System.Drawing.Point(52, 115)
        Me.prop_txtCN_4.MaxLength = 0
        Me.prop_txtCN_4.Name = "prop_txtCN_4"
        Me.prop_txtCN_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCN_4.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCN_4.TabIndex = 30
        Me.prop_txtCN_4.Text = "0.81"
        '
        'zlbl9
        '
        Me.zlbl9.AutoSize = True
        Me.zlbl9.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl9.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl9.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl9.Location = New System.Drawing.Point(20, 147)
        Me.zlbl9.Name = "zlbl9"
        Me.zlbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl9.Size = New System.Drawing.Size(26, 16)
        Me.zlbl9.TabIndex = 29
        Me.zlbl9.Text = "+Z"
        Me.zlbl9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl1
        '
        Me.zlbl1.AutoSize = True
        Me.zlbl1.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl1.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl1.Location = New System.Drawing.Point(19, 60)
        Me.zlbl1.Name = "zlbl1"
        Me.zlbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl1.Size = New System.Drawing.Size(23, 16)
        Me.zlbl1.TabIndex = 5
        Me.zlbl1.Text = "-X"
        Me.zlbl1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl8
        '
        Me.zlbl8.AutoSize = True
        Me.zlbl8.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl8.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl8.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl8.Location = New System.Drawing.Point(20, 176)
        Me.zlbl8.Name = "zlbl8"
        Me.zlbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl8.Size = New System.Drawing.Size(24, 16)
        Me.zlbl8.TabIndex = 28
        Me.zlbl8.Text = "-Z"
        Me.zlbl8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl2
        '
        Me.zlbl2.AutoSize = True
        Me.zlbl2.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl2.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl2.Location = New System.Drawing.Point(19, 89)
        Me.zlbl2.Name = "zlbl2"
        Me.zlbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl2.Size = New System.Drawing.Size(26, 16)
        Me.zlbl2.TabIndex = 6
        Me.zlbl2.Text = "+Y"
        Me.zlbl2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl7
        '
        Me.zlbl7.AutoSize = True
        Me.zlbl7.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl7.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl7.Location = New System.Drawing.Point(20, 118)
        Me.zlbl7.Name = "zlbl7"
        Me.zlbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl7.Size = New System.Drawing.Size(23, 16)
        Me.zlbl7.TabIndex = 27
        Me.zlbl7.Text = "-Y"
        Me.zlbl7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCN_3
        '
        Me.prop_txtCN_3.AcceptsReturn = True
        Me.prop_txtCN_3.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCN_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCN_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCN_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCN_3.Location = New System.Drawing.Point(52, 57)
        Me.prop_txtCN_3.MaxLength = 0
        Me.prop_txtCN_3.Name = "prop_txtCN_3"
        Me.prop_txtCN_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCN_3.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCN_3.TabIndex = 28
        Me.prop_txtCN_3.Text = "0.81"
        '
        'prop_txtCN_2
        '
        Me.prop_txtCN_2.AcceptsReturn = True
        Me.prop_txtCN_2.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCN_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCN_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCN_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCN_2.Location = New System.Drawing.Point(52, 144)
        Me.prop_txtCN_2.MaxLength = 0
        Me.prop_txtCN_2.Name = "prop_txtCN_2"
        Me.prop_txtCN_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCN_2.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCN_2.TabIndex = 31
        Me.prop_txtCN_2.Text = "0.9"
        '
        'zlbl5
        '
        Me.zlbl5.AutoSize = True
        Me.zlbl5.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl5.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl5.Location = New System.Drawing.Point(18, 31)
        Me.zlbl5.Name = "zlbl5"
        Me.zlbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl5.Size = New System.Drawing.Size(26, 16)
        Me.zlbl5.TabIndex = 6
        Me.zlbl5.Text = "+X"
        Me.zlbl5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCN_5
        '
        Me.prop_txtCN_5.AcceptsReturn = True
        Me.prop_txtCN_5.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCN_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCN_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCN_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCN_5.Location = New System.Drawing.Point(52, 173)
        Me.prop_txtCN_5.MaxLength = 0
        Me.prop_txtCN_5.Name = "prop_txtCN_5"
        Me.prop_txtCN_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCN_5.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCN_5.TabIndex = 32
        Me.prop_txtCN_5.Text = "0.81"
        '
        'prop_txtCN_1
        '
        Me.prop_txtCN_1.AcceptsReturn = True
        Me.prop_txtCN_1.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCN_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCN_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCN_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCN_1.Location = New System.Drawing.Point(52, 86)
        Me.prop_txtCN_1.MaxLength = 0
        Me.prop_txtCN_1.Name = "prop_txtCN_1"
        Me.prop_txtCN_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCN_1.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCN_1.TabIndex = 29
        Me.prop_txtCN_1.Text = "0.9"
        '
        'Onglet_att_act
        '
        Me.Onglet_att_act.BackColor = System.Drawing.Color.Transparent
        Me.Onglet_att_act.Controls.Add(Me.act_txtName)
        Me.Onglet_att_act.Controls.Add(Me.zlbl154)
        Me.Onglet_att_act.Controls.Add(Me.act_cmdModify)
        Me.Onglet_att_act.Controls.Add(Me.zfra_24)
        Me.Onglet_att_act.Controls.Add(Me.zfra_23)
        Me.Onglet_att_act.Controls.Add(Me.zfra_22)
        Me.Onglet_att_act.Controls.Add(Me.zfra_21)
        Me.Onglet_att_act.Location = New System.Drawing.Point(4, 32)
        Me.Onglet_att_act.Name = "Onglet_att_act"
        Me.Onglet_att_act.Size = New System.Drawing.Size(627, 520)
        Me.Onglet_att_act.TabIndex = 5
        Me.Onglet_att_act.Text = "Actuactors"
        '
        'act_txtName
        '
        Me.act_txtName.AcceptsReturn = True
        Me.act_txtName.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtName.Enabled = False
        Me.act_txtName.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtName.Location = New System.Drawing.Point(407, 76)
        Me.act_txtName.MaxLength = 0
        Me.act_txtName.Name = "act_txtName"
        Me.act_txtName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtName.Size = New System.Drawing.Size(195, 23)
        Me.act_txtName.TabIndex = 88
        '
        'zlbl154
        '
        Me.zlbl154.AutoSize = True
        Me.zlbl154.BackColor = System.Drawing.Color.Transparent
        Me.zlbl154.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl154.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl154.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl154.Location = New System.Drawing.Point(404, 43)
        Me.zlbl154.Name = "zlbl154"
        Me.zlbl154.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl154.Size = New System.Drawing.Size(162, 18)
        Me.zlbl154.TabIndex = 89
        Me.zlbl154.Text = "Name of the event"
        '
        'act_cmdModify
        '
        Me.act_cmdModify.BackColor = System.Drawing.SystemColors.Control
        Me.act_cmdModify.Cursor = System.Windows.Forms.Cursors.Default
        Me.act_cmdModify.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_cmdModify.ForeColor = System.Drawing.SystemColors.ControlText
        Me.act_cmdModify.Location = New System.Drawing.Point(443, 129)
        Me.act_cmdModify.Name = "act_cmdModify"
        Me.act_cmdModify.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_cmdModify.Size = New System.Drawing.Size(114, 37)
        Me.act_cmdModify.TabIndex = 87
        Me.act_cmdModify.Text = "Modify"
        Me.act_cmdModify.UseVisualStyleBackColor = False
        '
        'zfra_24
        '
        Me.zfra_24.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_24.Controls.Add(Me.zlbl153)
        Me.zfra_24.Controls.Add(Me.zlbl152)
        Me.zfra_24.Controls.Add(Me.zlbl151)
        Me.zfra_24.Controls.Add(Me.act_txtMN_X)
        Me.zfra_24.Controls.Add(Me.act_txtMN_Y)
        Me.zfra_24.Controls.Add(Me.zlbl149)
        Me.zfra_24.Controls.Add(Me.zlbl150)
        Me.zfra_24.Controls.Add(Me.zlbl148)
        Me.zfra_24.Controls.Add(Me.act_txtMN_Z)
        Me.zfra_24.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_24.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_24.Location = New System.Drawing.Point(407, 342)
        Me.zfra_24.Name = "zfra_24"
        Me.zfra_24.Size = New System.Drawing.Size(195, 133)
        Me.zfra_24.TabIndex = 52
        Me.zfra_24.TabStop = False
        Me.zfra_24.Text = "Nozzle"
        '
        'zlbl153
        '
        Me.zlbl153.AutoSize = True
        Me.zlbl153.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl153.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl153.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl153.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl153.Location = New System.Drawing.Point(138, 102)
        Me.zlbl153.Name = "zlbl153"
        Me.zlbl153.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl153.Size = New System.Drawing.Size(33, 16)
        Me.zlbl153.TabIndex = 38
        Me.zlbl153.Text = "N.m"
        Me.zlbl153.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl152
        '
        Me.zlbl152.AutoSize = True
        Me.zlbl152.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl152.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl152.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl152.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl152.Location = New System.Drawing.Point(138, 66)
        Me.zlbl152.Name = "zlbl152"
        Me.zlbl152.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl152.Size = New System.Drawing.Size(33, 16)
        Me.zlbl152.TabIndex = 37
        Me.zlbl152.Text = "N.m"
        Me.zlbl152.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl151
        '
        Me.zlbl151.AutoSize = True
        Me.zlbl151.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl151.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl151.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl151.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl151.Location = New System.Drawing.Point(138, 29)
        Me.zlbl151.Name = "zlbl151"
        Me.zlbl151.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl151.Size = New System.Drawing.Size(33, 16)
        Me.zlbl151.TabIndex = 36
        Me.zlbl151.Text = "N.m"
        Me.zlbl151.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'act_txtMN_X
        '
        Me.act_txtMN_X.AcceptsReturn = True
        Me.act_txtMN_X.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtMN_X.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtMN_X.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtMN_X.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtMN_X.Location = New System.Drawing.Point(80, 26)
        Me.act_txtMN_X.MaxLength = 0
        Me.act_txtMN_X.Name = "act_txtMN_X"
        Me.act_txtMN_X.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtMN_X.Size = New System.Drawing.Size(51, 23)
        Me.act_txtMN_X.TabIndex = 76
        Me.act_txtMN_X.Text = "0"
        '
        'act_txtMN_Y
        '
        Me.act_txtMN_Y.AcceptsReturn = True
        Me.act_txtMN_Y.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtMN_Y.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtMN_Y.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtMN_Y.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtMN_Y.Location = New System.Drawing.Point(80, 63)
        Me.act_txtMN_Y.MaxLength = 0
        Me.act_txtMN_Y.Name = "act_txtMN_Y"
        Me.act_txtMN_Y.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtMN_Y.Size = New System.Drawing.Size(51, 23)
        Me.act_txtMN_Y.TabIndex = 77
        Me.act_txtMN_Y.Text = "0"
        '
        'zlbl149
        '
        Me.zlbl149.AutoSize = True
        Me.zlbl149.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl149.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl149.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl149.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl149.Location = New System.Drawing.Point(10, 66)
        Me.zlbl149.Name = "zlbl149"
        Me.zlbl149.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl149.Size = New System.Drawing.Size(66, 16)
        Me.zlbl149.TabIndex = 6
        Me.zlbl149.Text = "Torque y"
        Me.zlbl149.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl150
        '
        Me.zlbl150.AutoSize = True
        Me.zlbl150.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl150.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl150.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl150.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl150.Location = New System.Drawing.Point(10, 102)
        Me.zlbl150.Name = "zlbl150"
        Me.zlbl150.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl150.Size = New System.Drawing.Size(65, 16)
        Me.zlbl150.TabIndex = 29
        Me.zlbl150.Text = "Torque z"
        Me.zlbl150.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl148
        '
        Me.zlbl148.AutoSize = True
        Me.zlbl148.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl148.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl148.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl148.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl148.Location = New System.Drawing.Point(10, 29)
        Me.zlbl148.Name = "zlbl148"
        Me.zlbl148.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl148.Size = New System.Drawing.Size(65, 16)
        Me.zlbl148.TabIndex = 6
        Me.zlbl148.Text = "Torque x"
        Me.zlbl148.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'act_txtMN_Z
        '
        Me.act_txtMN_Z.AcceptsReturn = True
        Me.act_txtMN_Z.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtMN_Z.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtMN_Z.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtMN_Z.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtMN_Z.Location = New System.Drawing.Point(80, 99)
        Me.act_txtMN_Z.MaxLength = 0
        Me.act_txtMN_Z.Name = "act_txtMN_Z"
        Me.act_txtMN_Z.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtMN_Z.Size = New System.Drawing.Size(51, 23)
        Me.act_txtMN_Z.TabIndex = 78
        Me.act_txtMN_Z.Text = "0"
        '
        'zfra_23
        '
        Me.zfra_23.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_23.Controls.Add(Me.zlbl147)
        Me.zfra_23.Controls.Add(Me.zlbl146)
        Me.zfra_23.Controls.Add(Me.zlbl145)
        Me.zfra_23.Controls.Add(Me.act_txtKX)
        Me.zfra_23.Controls.Add(Me.act_txtKY)
        Me.zfra_23.Controls.Add(Me.zlbl143)
        Me.zfra_23.Controls.Add(Me.zlbl144)
        Me.zfra_23.Controls.Add(Me.zlbl142)
        Me.zfra_23.Controls.Add(Me.act_txtKZ)
        Me.zfra_23.Controls.Add(Me.zfra_232)
        Me.zfra_23.Controls.Add(Me.zfra_231)
        Me.zfra_23.Controls.Add(Me.zlbl141)
        Me.zfra_23.Controls.Add(Me.zlbl140)
        Me.zfra_23.Controls.Add(Me.zlbl139)
        Me.zfra_23.Controls.Add(Me.act_txtCX)
        Me.zfra_23.Controls.Add(Me.act_txtCY)
        Me.zfra_23.Controls.Add(Me.zlbl137)
        Me.zfra_23.Controls.Add(Me.zlbl138)
        Me.zfra_23.Controls.Add(Me.zlbl136)
        Me.zfra_23.Controls.Add(Me.act_txtCZ)
        Me.zfra_23.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_23.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_23.Location = New System.Drawing.Point(22, 199)
        Me.zfra_23.Name = "zfra_23"
        Me.zfra_23.Size = New System.Drawing.Size(362, 276)
        Me.zfra_23.TabIndex = 51
        Me.zfra_23.TabStop = False
        Me.zfra_23.Text = "Damper"
        '
        'zlbl147
        '
        Me.zlbl147.AutoSize = True
        Me.zlbl147.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl147.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl147.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl147.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl147.Location = New System.Drawing.Point(300, 244)
        Me.zlbl147.Name = "zlbl147"
        Me.zlbl147.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl147.Size = New System.Drawing.Size(46, 16)
        Me.zlbl147.TabIndex = 79
        Me.zlbl147.Text = "N.s/m"
        Me.zlbl147.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl146
        '
        Me.zlbl146.AutoSize = True
        Me.zlbl146.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl146.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl146.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl146.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl146.Location = New System.Drawing.Point(300, 208)
        Me.zlbl146.Name = "zlbl146"
        Me.zlbl146.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl146.Size = New System.Drawing.Size(46, 16)
        Me.zlbl146.TabIndex = 78
        Me.zlbl146.Text = "N.s/m"
        Me.zlbl146.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl145
        '
        Me.zlbl145.AutoSize = True
        Me.zlbl145.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl145.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl145.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl145.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl145.Location = New System.Drawing.Point(300, 171)
        Me.zlbl145.Name = "zlbl145"
        Me.zlbl145.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl145.Size = New System.Drawing.Size(46, 16)
        Me.zlbl145.TabIndex = 77
        Me.zlbl145.Text = "N.s/m"
        Me.zlbl145.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'act_txtKX
        '
        Me.act_txtKX.AcceptsReturn = True
        Me.act_txtKX.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtKX.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtKX.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtKX.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtKX.Location = New System.Drawing.Point(241, 168)
        Me.act_txtKX.MaxLength = 0
        Me.act_txtKX.Name = "act_txtKX"
        Me.act_txtKX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtKX.Size = New System.Drawing.Size(51, 23)
        Me.act_txtKX.TabIndex = 73
        Me.act_txtKX.Text = "0"
        '
        'act_txtKY
        '
        Me.act_txtKY.AcceptsReturn = True
        Me.act_txtKY.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtKY.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtKY.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtKY.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtKY.Location = New System.Drawing.Point(241, 205)
        Me.act_txtKY.MaxLength = 0
        Me.act_txtKY.Name = "act_txtKY"
        Me.act_txtKY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtKY.Size = New System.Drawing.Size(51, 23)
        Me.act_txtKY.TabIndex = 74
        Me.act_txtKY.Text = "0"
        '
        'zlbl143
        '
        Me.zlbl143.AutoSize = True
        Me.zlbl143.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl143.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl143.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl143.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl143.Location = New System.Drawing.Point(211, 208)
        Me.zlbl143.Name = "zlbl143"
        Me.zlbl143.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl143.Size = New System.Drawing.Size(23, 16)
        Me.zlbl143.TabIndex = 71
        Me.zlbl143.Text = "ky"
        Me.zlbl143.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl144
        '
        Me.zlbl144.AutoSize = True
        Me.zlbl144.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl144.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl144.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl144.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl144.Location = New System.Drawing.Point(211, 244)
        Me.zlbl144.Name = "zlbl144"
        Me.zlbl144.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl144.Size = New System.Drawing.Size(22, 16)
        Me.zlbl144.TabIndex = 75
        Me.zlbl144.Text = "kz"
        Me.zlbl144.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl142
        '
        Me.zlbl142.AutoSize = True
        Me.zlbl142.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl142.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl142.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl142.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl142.Location = New System.Drawing.Point(211, 171)
        Me.zlbl142.Name = "zlbl142"
        Me.zlbl142.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl142.Size = New System.Drawing.Size(22, 16)
        Me.zlbl142.TabIndex = 72
        Me.zlbl142.Text = "kx"
        Me.zlbl142.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'act_txtKZ
        '
        Me.act_txtKZ.AcceptsReturn = True
        Me.act_txtKZ.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtKZ.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtKZ.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtKZ.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtKZ.Location = New System.Drawing.Point(241, 241)
        Me.act_txtKZ.MaxLength = 0
        Me.act_txtKZ.Name = "act_txtKZ"
        Me.act_txtKZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtKZ.Size = New System.Drawing.Size(51, 23)
        Me.act_txtKZ.TabIndex = 75
        Me.act_txtKZ.Text = "0"
        '
        'zfra_232
        '
        Me.zfra_232.Controls.Add(Me.zlbl135)
        Me.zfra_232.Controls.Add(Me.zlbl134)
        Me.zfra_232.Controls.Add(Me.act_txtDX)
        Me.zfra_232.Controls.Add(Me.act_txtDZ)
        Me.zfra_232.Controls.Add(Me.zlbl133)
        Me.zfra_232.Controls.Add(Me.zlbl130)
        Me.zfra_232.Controls.Add(Me.zlbl132)
        Me.zfra_232.Controls.Add(Me.act_txtDY)
        Me.zfra_232.Controls.Add(Me.zlbl131)
        Me.zfra_232.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_232.Location = New System.Drawing.Point(184, 19)
        Me.zfra_232.Name = "zfra_232"
        Me.zfra_232.Size = New System.Drawing.Size(153, 133)
        Me.zfra_232.TabIndex = 70
        Me.zfra_232.TabStop = False
        Me.zfra_232.Text = "Equilibrium length"
        '
        'zlbl135
        '
        Me.zlbl135.AutoSize = True
        Me.zlbl135.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl135.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl135.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl135.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl135.Location = New System.Drawing.Point(115, 100)
        Me.zlbl135.Name = "zlbl135"
        Me.zlbl135.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl135.Size = New System.Drawing.Size(19, 16)
        Me.zlbl135.TabIndex = 79
        Me.zlbl135.Text = "m"
        Me.zlbl135.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl134
        '
        Me.zlbl134.AutoSize = True
        Me.zlbl134.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl134.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl134.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl134.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl134.Location = New System.Drawing.Point(115, 64)
        Me.zlbl134.Name = "zlbl134"
        Me.zlbl134.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl134.Size = New System.Drawing.Size(19, 16)
        Me.zlbl134.TabIndex = 78
        Me.zlbl134.Text = "m"
        Me.zlbl134.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'act_txtDX
        '
        Me.act_txtDX.AcceptsReturn = True
        Me.act_txtDX.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtDX.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtDX.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtDX.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtDX.Location = New System.Drawing.Point(57, 24)
        Me.act_txtDX.MaxLength = 0
        Me.act_txtDX.Name = "act_txtDX"
        Me.act_txtDX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtDX.Size = New System.Drawing.Size(51, 23)
        Me.act_txtDX.TabIndex = 67
        Me.act_txtDX.Text = "0"
        '
        'act_txtDZ
        '
        Me.act_txtDZ.AcceptsReturn = True
        Me.act_txtDZ.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtDZ.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtDZ.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtDZ.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtDZ.Location = New System.Drawing.Point(57, 97)
        Me.act_txtDZ.MaxLength = 0
        Me.act_txtDZ.Name = "act_txtDZ"
        Me.act_txtDZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtDZ.Size = New System.Drawing.Size(51, 23)
        Me.act_txtDZ.TabIndex = 69
        Me.act_txtDZ.Text = "0"
        '
        'zlbl133
        '
        Me.zlbl133.AutoSize = True
        Me.zlbl133.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl133.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl133.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl133.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl133.Location = New System.Drawing.Point(115, 27)
        Me.zlbl133.Name = "zlbl133"
        Me.zlbl133.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl133.Size = New System.Drawing.Size(19, 16)
        Me.zlbl133.TabIndex = 77
        Me.zlbl133.Text = "m"
        Me.zlbl133.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl130
        '
        Me.zlbl130.AutoSize = True
        Me.zlbl130.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl130.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl130.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl130.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl130.Location = New System.Drawing.Point(26, 27)
        Me.zlbl130.Name = "zlbl130"
        Me.zlbl130.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl130.Size = New System.Drawing.Size(24, 16)
        Me.zlbl130.TabIndex = 72
        Me.zlbl130.Text = "Dx"
        Me.zlbl130.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl132
        '
        Me.zlbl132.AutoSize = True
        Me.zlbl132.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl132.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl132.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl132.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl132.Location = New System.Drawing.Point(26, 100)
        Me.zlbl132.Name = "zlbl132"
        Me.zlbl132.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl132.Size = New System.Drawing.Size(24, 16)
        Me.zlbl132.TabIndex = 75
        Me.zlbl132.Text = "Dz"
        Me.zlbl132.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'act_txtDY
        '
        Me.act_txtDY.AcceptsReturn = True
        Me.act_txtDY.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtDY.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtDY.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtDY.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtDY.Location = New System.Drawing.Point(57, 61)
        Me.act_txtDY.MaxLength = 0
        Me.act_txtDY.Name = "act_txtDY"
        Me.act_txtDY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtDY.Size = New System.Drawing.Size(51, 23)
        Me.act_txtDY.TabIndex = 68
        Me.act_txtDY.Text = "0"
        '
        'zlbl131
        '
        Me.zlbl131.AutoSize = True
        Me.zlbl131.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl131.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl131.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl131.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl131.Location = New System.Drawing.Point(26, 64)
        Me.zlbl131.Name = "zlbl131"
        Me.zlbl131.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl131.Size = New System.Drawing.Size(25, 16)
        Me.zlbl131.TabIndex = 71
        Me.zlbl131.Text = "Dy"
        Me.zlbl131.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_231
        '
        Me.zfra_231.Controls.Add(Me.zlbl129)
        Me.zfra_231.Controls.Add(Me.zlbl128)
        Me.zfra_231.Controls.Add(Me.act_txtMX)
        Me.zfra_231.Controls.Add(Me.act_txtMZ)
        Me.zfra_231.Controls.Add(Me.zlbl127)
        Me.zfra_231.Controls.Add(Me.zlbl124)
        Me.zfra_231.Controls.Add(Me.zlbl126)
        Me.zfra_231.Controls.Add(Me.act_txtMY)
        Me.zfra_231.Controls.Add(Me.zlbl125)
        Me.zfra_231.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_231.Location = New System.Drawing.Point(25, 19)
        Me.zfra_231.Name = "zfra_231"
        Me.zfra_231.Size = New System.Drawing.Size(153, 133)
        Me.zfra_231.TabIndex = 69
        Me.zfra_231.TabStop = False
        Me.zfra_231.Text = "Mass"
        '
        'zlbl129
        '
        Me.zlbl129.AutoSize = True
        Me.zlbl129.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl129.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl129.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl129.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl129.Location = New System.Drawing.Point(106, 100)
        Me.zlbl129.Name = "zlbl129"
        Me.zlbl129.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl129.Size = New System.Drawing.Size(23, 16)
        Me.zlbl129.TabIndex = 79
        Me.zlbl129.Text = "kg"
        Me.zlbl129.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl128
        '
        Me.zlbl128.AutoSize = True
        Me.zlbl128.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl128.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl128.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl128.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl128.Location = New System.Drawing.Point(106, 64)
        Me.zlbl128.Name = "zlbl128"
        Me.zlbl128.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl128.Size = New System.Drawing.Size(23, 16)
        Me.zlbl128.TabIndex = 78
        Me.zlbl128.Text = "kg"
        Me.zlbl128.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'act_txtMX
        '
        Me.act_txtMX.AcceptsReturn = True
        Me.act_txtMX.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtMX.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtMX.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtMX.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtMX.Location = New System.Drawing.Point(48, 24)
        Me.act_txtMX.MaxLength = 0
        Me.act_txtMX.Name = "act_txtMX"
        Me.act_txtMX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtMX.Size = New System.Drawing.Size(51, 23)
        Me.act_txtMX.TabIndex = 63
        Me.act_txtMX.Text = "0"
        '
        'act_txtMZ
        '
        Me.act_txtMZ.AcceptsReturn = True
        Me.act_txtMZ.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtMZ.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtMZ.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtMZ.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtMZ.Location = New System.Drawing.Point(48, 97)
        Me.act_txtMZ.MaxLength = 0
        Me.act_txtMZ.Name = "act_txtMZ"
        Me.act_txtMZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtMZ.Size = New System.Drawing.Size(51, 23)
        Me.act_txtMZ.TabIndex = 66
        Me.act_txtMZ.Text = "0"
        '
        'zlbl127
        '
        Me.zlbl127.AutoSize = True
        Me.zlbl127.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl127.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl127.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl127.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl127.Location = New System.Drawing.Point(106, 27)
        Me.zlbl127.Name = "zlbl127"
        Me.zlbl127.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl127.Size = New System.Drawing.Size(23, 16)
        Me.zlbl127.TabIndex = 77
        Me.zlbl127.Text = "kg"
        Me.zlbl127.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl124
        '
        Me.zlbl124.AutoSize = True
        Me.zlbl124.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl124.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl124.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl124.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl124.Location = New System.Drawing.Point(16, 27)
        Me.zlbl124.Name = "zlbl124"
        Me.zlbl124.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl124.Size = New System.Drawing.Size(26, 16)
        Me.zlbl124.TabIndex = 72
        Me.zlbl124.Text = "Mx"
        Me.zlbl124.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl126
        '
        Me.zlbl126.AutoSize = True
        Me.zlbl126.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl126.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl126.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl126.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl126.Location = New System.Drawing.Point(15, 100)
        Me.zlbl126.Name = "zlbl126"
        Me.zlbl126.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl126.Size = New System.Drawing.Size(26, 16)
        Me.zlbl126.TabIndex = 75
        Me.zlbl126.Text = "Mz"
        Me.zlbl126.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'act_txtMY
        '
        Me.act_txtMY.AcceptsReturn = True
        Me.act_txtMY.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtMY.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtMY.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtMY.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtMY.Location = New System.Drawing.Point(48, 61)
        Me.act_txtMY.MaxLength = 0
        Me.act_txtMY.Name = "act_txtMY"
        Me.act_txtMY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtMY.Size = New System.Drawing.Size(51, 23)
        Me.act_txtMY.TabIndex = 64
        Me.act_txtMY.Text = "0"
        '
        'zlbl125
        '
        Me.zlbl125.AutoSize = True
        Me.zlbl125.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl125.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl125.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl125.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl125.Location = New System.Drawing.Point(16, 64)
        Me.zlbl125.Name = "zlbl125"
        Me.zlbl125.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl125.Size = New System.Drawing.Size(27, 16)
        Me.zlbl125.TabIndex = 71
        Me.zlbl125.Text = "My"
        Me.zlbl125.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl141
        '
        Me.zlbl141.AutoSize = True
        Me.zlbl141.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl141.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl141.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl141.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl141.Location = New System.Drawing.Point(131, 244)
        Me.zlbl141.Name = "zlbl141"
        Me.zlbl141.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl141.Size = New System.Drawing.Size(34, 16)
        Me.zlbl141.TabIndex = 59
        Me.zlbl141.Text = "N/m"
        Me.zlbl141.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl140
        '
        Me.zlbl140.AutoSize = True
        Me.zlbl140.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl140.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl140.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl140.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl140.Location = New System.Drawing.Point(131, 208)
        Me.zlbl140.Name = "zlbl140"
        Me.zlbl140.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl140.Size = New System.Drawing.Size(34, 16)
        Me.zlbl140.TabIndex = 58
        Me.zlbl140.Text = "N/m"
        Me.zlbl140.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl139
        '
        Me.zlbl139.AutoSize = True
        Me.zlbl139.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl139.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl139.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl139.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl139.Location = New System.Drawing.Point(131, 171)
        Me.zlbl139.Name = "zlbl139"
        Me.zlbl139.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl139.Size = New System.Drawing.Size(34, 16)
        Me.zlbl139.TabIndex = 57
        Me.zlbl139.Text = "N/m"
        Me.zlbl139.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'act_txtCX
        '
        Me.act_txtCX.AcceptsReturn = True
        Me.act_txtCX.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtCX.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtCX.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtCX.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtCX.Location = New System.Drawing.Point(73, 168)
        Me.act_txtCX.MaxLength = 0
        Me.act_txtCX.Name = "act_txtCX"
        Me.act_txtCX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtCX.Size = New System.Drawing.Size(51, 23)
        Me.act_txtCX.TabIndex = 70
        Me.act_txtCX.Text = "0"
        '
        'act_txtCY
        '
        Me.act_txtCY.AcceptsReturn = True
        Me.act_txtCY.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtCY.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtCY.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtCY.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtCY.Location = New System.Drawing.Point(73, 205)
        Me.act_txtCY.MaxLength = 0
        Me.act_txtCY.Name = "act_txtCY"
        Me.act_txtCY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtCY.Size = New System.Drawing.Size(51, 23)
        Me.act_txtCY.TabIndex = 71
        Me.act_txtCY.Text = "0"
        '
        'zlbl137
        '
        Me.zlbl137.AutoSize = True
        Me.zlbl137.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl137.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl137.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl137.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl137.Location = New System.Drawing.Point(40, 208)
        Me.zlbl137.Name = "zlbl137"
        Me.zlbl137.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl137.Size = New System.Drawing.Size(25, 16)
        Me.zlbl137.TabIndex = 51
        Me.zlbl137.Text = "Cy"
        Me.zlbl137.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl138
        '
        Me.zlbl138.AutoSize = True
        Me.zlbl138.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl138.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl138.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl138.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl138.Location = New System.Drawing.Point(39, 244)
        Me.zlbl138.Name = "zlbl138"
        Me.zlbl138.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl138.Size = New System.Drawing.Size(24, 16)
        Me.zlbl138.TabIndex = 55
        Me.zlbl138.Text = "Cz"
        Me.zlbl138.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl136
        '
        Me.zlbl136.AutoSize = True
        Me.zlbl136.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl136.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl136.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl136.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl136.Location = New System.Drawing.Point(40, 171)
        Me.zlbl136.Name = "zlbl136"
        Me.zlbl136.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl136.Size = New System.Drawing.Size(24, 16)
        Me.zlbl136.TabIndex = 52
        Me.zlbl136.Text = "Cx"
        Me.zlbl136.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'act_txtCZ
        '
        Me.act_txtCZ.AcceptsReturn = True
        Me.act_txtCZ.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtCZ.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtCZ.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtCZ.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtCZ.Location = New System.Drawing.Point(73, 241)
        Me.act_txtCZ.MaxLength = 0
        Me.act_txtCZ.Name = "act_txtCZ"
        Me.act_txtCZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtCZ.Size = New System.Drawing.Size(51, 23)
        Me.act_txtCZ.TabIndex = 72
        Me.act_txtCZ.Text = "0"
        '
        'zfra_22
        '
        Me.zfra_22.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_22.Controls.Add(Me.zlbl123)
        Me.zfra_22.Controls.Add(Me.zlbl122)
        Me.zfra_22.Controls.Add(Me.zlbl121)
        Me.zfra_22.Controls.Add(Me.act_txtMG_X)
        Me.zfra_22.Controls.Add(Me.act_txtMG_Y)
        Me.zfra_22.Controls.Add(Me.zlbl119)
        Me.zfra_22.Controls.Add(Me.zlbl120)
        Me.zfra_22.Controls.Add(Me.zlbl118)
        Me.zfra_22.Controls.Add(Me.act_txtMG_Z)
        Me.zfra_22.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_22.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_22.Location = New System.Drawing.Point(407, 199)
        Me.zfra_22.Name = "zfra_22"
        Me.zfra_22.Size = New System.Drawing.Size(195, 133)
        Me.zfra_22.TabIndex = 39
        Me.zfra_22.TabStop = False
        Me.zfra_22.Text = "Magnetorquer"
        '
        'zlbl123
        '
        Me.zlbl123.AutoSize = True
        Me.zlbl123.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl123.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl123.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl123.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl123.Location = New System.Drawing.Point(138, 100)
        Me.zlbl123.Name = "zlbl123"
        Me.zlbl123.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl123.Size = New System.Drawing.Size(40, 16)
        Me.zlbl123.TabIndex = 38
        Me.zlbl123.Text = "A.m²"
        Me.zlbl123.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl122
        '
        Me.zlbl122.AutoSize = True
        Me.zlbl122.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl122.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl122.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl122.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl122.Location = New System.Drawing.Point(138, 64)
        Me.zlbl122.Name = "zlbl122"
        Me.zlbl122.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl122.Size = New System.Drawing.Size(40, 16)
        Me.zlbl122.TabIndex = 37
        Me.zlbl122.Text = "A.m²"
        Me.zlbl122.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl121
        '
        Me.zlbl121.AutoSize = True
        Me.zlbl121.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl121.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl121.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl121.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl121.Location = New System.Drawing.Point(138, 27)
        Me.zlbl121.Name = "zlbl121"
        Me.zlbl121.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl121.Size = New System.Drawing.Size(40, 16)
        Me.zlbl121.TabIndex = 36
        Me.zlbl121.Text = "A.m²"
        Me.zlbl121.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'act_txtMG_X
        '
        Me.act_txtMG_X.AcceptsReturn = True
        Me.act_txtMG_X.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtMG_X.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtMG_X.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtMG_X.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtMG_X.Location = New System.Drawing.Point(80, 24)
        Me.act_txtMG_X.MaxLength = 0
        Me.act_txtMG_X.Name = "act_txtMG_X"
        Me.act_txtMG_X.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtMG_X.Size = New System.Drawing.Size(51, 23)
        Me.act_txtMG_X.TabIndex = 60
        Me.act_txtMG_X.Text = "0"
        '
        'act_txtMG_Y
        '
        Me.act_txtMG_Y.AcceptsReturn = True
        Me.act_txtMG_Y.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtMG_Y.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtMG_Y.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtMG_Y.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtMG_Y.Location = New System.Drawing.Point(80, 61)
        Me.act_txtMG_Y.MaxLength = 0
        Me.act_txtMG_Y.Name = "act_txtMG_Y"
        Me.act_txtMG_Y.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtMG_Y.Size = New System.Drawing.Size(51, 23)
        Me.act_txtMG_Y.TabIndex = 61
        Me.act_txtMG_Y.Text = "0"
        '
        'zlbl119
        '
        Me.zlbl119.AutoSize = True
        Me.zlbl119.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl119.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl119.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl119.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl119.Location = New System.Drawing.Point(47, 64)
        Me.zlbl119.Name = "zlbl119"
        Me.zlbl119.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl119.Size = New System.Drawing.Size(26, 16)
        Me.zlbl119.TabIndex = 6
        Me.zlbl119.Text = "+Y"
        Me.zlbl119.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl120
        '
        Me.zlbl120.AutoSize = True
        Me.zlbl120.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl120.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl120.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl120.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl120.Location = New System.Drawing.Point(47, 100)
        Me.zlbl120.Name = "zlbl120"
        Me.zlbl120.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl120.Size = New System.Drawing.Size(26, 16)
        Me.zlbl120.TabIndex = 29
        Me.zlbl120.Text = "+Z"
        Me.zlbl120.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl118
        '
        Me.zlbl118.AutoSize = True
        Me.zlbl118.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl118.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl118.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl118.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl118.Location = New System.Drawing.Point(47, 27)
        Me.zlbl118.Name = "zlbl118"
        Me.zlbl118.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl118.Size = New System.Drawing.Size(26, 16)
        Me.zlbl118.TabIndex = 6
        Me.zlbl118.Text = "+X"
        Me.zlbl118.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'act_txtMG_Z
        '
        Me.act_txtMG_Z.AcceptsReturn = True
        Me.act_txtMG_Z.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtMG_Z.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtMG_Z.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtMG_Z.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtMG_Z.Location = New System.Drawing.Point(80, 97)
        Me.act_txtMG_Z.MaxLength = 0
        Me.act_txtMG_Z.Name = "act_txtMG_Z"
        Me.act_txtMG_Z.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtMG_Z.Size = New System.Drawing.Size(51, 23)
        Me.act_txtMG_Z.TabIndex = 62
        Me.act_txtMG_Z.Text = "0"
        '
        'zfra_21
        '
        Me.zfra_21.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_21.Controls.Add(Me.zlbl117)
        Me.zfra_21.Controls.Add(Me.zlbl116)
        Me.zfra_21.Controls.Add(Me.zlbl115)
        Me.zfra_21.Controls.Add(Me.zlbl114)
        Me.zfra_21.Controls.Add(Me.zlbl113)
        Me.zfra_21.Controls.Add(Me.zlbl112)
        Me.zfra_21.Controls.Add(Me.act_txtWRX)
        Me.zfra_21.Controls.Add(Me.act_txtWRY)
        Me.zfra_21.Controls.Add(Me.zlbl110)
        Me.zfra_21.Controls.Add(Me.zlbl111)
        Me.zfra_21.Controls.Add(Me.zlbl109)
        Me.zfra_21.Controls.Add(Me.act_txtWRZ)
        Me.zfra_21.Controls.Add(Me.zlbl108)
        Me.zfra_21.Controls.Add(Me.zlbl107)
        Me.zfra_21.Controls.Add(Me.zlbl106)
        Me.zfra_21.Controls.Add(Me.act_txtIRX)
        Me.zfra_21.Controls.Add(Me.act_txtIRY)
        Me.zfra_21.Controls.Add(Me.zlbl104)
        Me.zfra_21.Controls.Add(Me.zlbl105)
        Me.zfra_21.Controls.Add(Me.zlbl103)
        Me.zfra_21.Controls.Add(Me.act_txtIRZ)
        Me.zfra_21.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_21.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_21.Location = New System.Drawing.Point(22, 33)
        Me.zfra_21.Name = "zfra_21"
        Me.zfra_21.Size = New System.Drawing.Size(362, 133)
        Me.zfra_21.TabIndex = 38
        Me.zfra_21.TabStop = False
        Me.zfra_21.Text = "Flywheel"
        '
        'zlbl117
        '
        Me.zlbl117.AutoSize = True
        Me.zlbl117.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl117.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl117.Font = New System.Drawing.Font("Symbol", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.zlbl117.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl117.Location = New System.Drawing.Point(198, 100)
        Me.zlbl117.Name = "zlbl117"
        Me.zlbl117.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl117.Size = New System.Drawing.Size(19, 16)
        Me.zlbl117.TabIndex = 50
        Me.zlbl117.Text = "W"
        Me.zlbl117.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl116
        '
        Me.zlbl116.AutoSize = True
        Me.zlbl116.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl116.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl116.Font = New System.Drawing.Font("Symbol", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.zlbl116.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl116.Location = New System.Drawing.Point(198, 64)
        Me.zlbl116.Name = "zlbl116"
        Me.zlbl116.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl116.Size = New System.Drawing.Size(19, 16)
        Me.zlbl116.TabIndex = 49
        Me.zlbl116.Text = "W"
        Me.zlbl116.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl115
        '
        Me.zlbl115.AutoSize = True
        Me.zlbl115.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl115.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl115.Font = New System.Drawing.Font("Symbol", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.zlbl115.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl115.Location = New System.Drawing.Point(198, 27)
        Me.zlbl115.Name = "zlbl115"
        Me.zlbl115.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl115.Size = New System.Drawing.Size(19, 16)
        Me.zlbl115.TabIndex = 48
        Me.zlbl115.Text = "W"
        Me.zlbl115.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl114
        '
        Me.zlbl114.AutoSize = True
        Me.zlbl114.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl114.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl114.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl114.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl114.Location = New System.Drawing.Point(298, 100)
        Me.zlbl114.Name = "zlbl114"
        Me.zlbl114.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl114.Size = New System.Drawing.Size(45, 16)
        Me.zlbl114.TabIndex = 47
        Me.zlbl114.Text = "deg/s"
        Me.zlbl114.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl113
        '
        Me.zlbl113.AutoSize = True
        Me.zlbl113.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl113.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl113.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl113.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl113.Location = New System.Drawing.Point(298, 64)
        Me.zlbl113.Name = "zlbl113"
        Me.zlbl113.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl113.Size = New System.Drawing.Size(45, 16)
        Me.zlbl113.TabIndex = 46
        Me.zlbl113.Text = "deg/s"
        Me.zlbl113.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl112
        '
        Me.zlbl112.AutoSize = True
        Me.zlbl112.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl112.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl112.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl112.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl112.Location = New System.Drawing.Point(298, 27)
        Me.zlbl112.Name = "zlbl112"
        Me.zlbl112.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl112.Size = New System.Drawing.Size(45, 16)
        Me.zlbl112.TabIndex = 45
        Me.zlbl112.Text = "deg/s"
        Me.zlbl112.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'act_txtWRX
        '
        Me.act_txtWRX.AcceptsReturn = True
        Me.act_txtWRX.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtWRX.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtWRX.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtWRX.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtWRX.Location = New System.Drawing.Point(241, 24)
        Me.act_txtWRX.MaxLength = 0
        Me.act_txtWRX.Name = "act_txtWRX"
        Me.act_txtWRX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtWRX.Size = New System.Drawing.Size(51, 23)
        Me.act_txtWRX.TabIndex = 57
        Me.act_txtWRX.Text = "0"
        '
        'act_txtWRY
        '
        Me.act_txtWRY.AcceptsReturn = True
        Me.act_txtWRY.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtWRY.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtWRY.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtWRY.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtWRY.Location = New System.Drawing.Point(241, 61)
        Me.act_txtWRY.MaxLength = 0
        Me.act_txtWRY.Name = "act_txtWRY"
        Me.act_txtWRY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtWRY.Size = New System.Drawing.Size(51, 23)
        Me.act_txtWRY.TabIndex = 58
        Me.act_txtWRY.Text = "0"
        '
        'zlbl110
        '
        Me.zlbl110.AutoSize = True
        Me.zlbl110.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl110.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl110.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl110.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl110.Location = New System.Drawing.Point(214, 64)
        Me.zlbl110.Name = "zlbl110"
        Me.zlbl110.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl110.Size = New System.Drawing.Size(21, 16)
        Me.zlbl110.TabIndex = 39
        Me.zlbl110.Text = "ry"
        Me.zlbl110.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl111
        '
        Me.zlbl111.AutoSize = True
        Me.zlbl111.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl111.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl111.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl111.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl111.Location = New System.Drawing.Point(214, 100)
        Me.zlbl111.Name = "zlbl111"
        Me.zlbl111.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl111.Size = New System.Drawing.Size(20, 16)
        Me.zlbl111.TabIndex = 43
        Me.zlbl111.Text = "rz"
        Me.zlbl111.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl109
        '
        Me.zlbl109.AutoSize = True
        Me.zlbl109.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl109.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl109.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl109.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl109.Location = New System.Drawing.Point(214, 27)
        Me.zlbl109.Name = "zlbl109"
        Me.zlbl109.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl109.Size = New System.Drawing.Size(20, 16)
        Me.zlbl109.TabIndex = 40
        Me.zlbl109.Text = "rx"
        Me.zlbl109.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'act_txtWRZ
        '
        Me.act_txtWRZ.AcceptsReturn = True
        Me.act_txtWRZ.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtWRZ.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtWRZ.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtWRZ.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtWRZ.Location = New System.Drawing.Point(241, 97)
        Me.act_txtWRZ.MaxLength = 0
        Me.act_txtWRZ.Name = "act_txtWRZ"
        Me.act_txtWRZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtWRZ.Size = New System.Drawing.Size(51, 23)
        Me.act_txtWRZ.TabIndex = 59
        Me.act_txtWRZ.Text = "0"
        '
        'zlbl108
        '
        Me.zlbl108.AutoSize = True
        Me.zlbl108.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl108.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl108.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl108.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl108.Location = New System.Drawing.Point(130, 100)
        Me.zlbl108.Name = "zlbl108"
        Me.zlbl108.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl108.Size = New System.Drawing.Size(46, 16)
        Me.zlbl108.TabIndex = 38
        Me.zlbl108.Text = "kg.m²"
        Me.zlbl108.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl107
        '
        Me.zlbl107.AutoSize = True
        Me.zlbl107.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl107.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl107.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl107.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl107.Location = New System.Drawing.Point(130, 64)
        Me.zlbl107.Name = "zlbl107"
        Me.zlbl107.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl107.Size = New System.Drawing.Size(46, 16)
        Me.zlbl107.TabIndex = 37
        Me.zlbl107.Text = "kg.m²"
        Me.zlbl107.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl106
        '
        Me.zlbl106.AutoSize = True
        Me.zlbl106.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl106.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl106.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl106.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl106.Location = New System.Drawing.Point(130, 27)
        Me.zlbl106.Name = "zlbl106"
        Me.zlbl106.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl106.Size = New System.Drawing.Size(46, 16)
        Me.zlbl106.TabIndex = 36
        Me.zlbl106.Text = "kg.m²"
        Me.zlbl106.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'act_txtIRX
        '
        Me.act_txtIRX.AcceptsReturn = True
        Me.act_txtIRX.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtIRX.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtIRX.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtIRX.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtIRX.Location = New System.Drawing.Point(73, 24)
        Me.act_txtIRX.MaxLength = 0
        Me.act_txtIRX.Name = "act_txtIRX"
        Me.act_txtIRX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtIRX.Size = New System.Drawing.Size(51, 23)
        Me.act_txtIRX.TabIndex = 54
        Me.act_txtIRX.Text = "0"
        '
        'act_txtIRY
        '
        Me.act_txtIRY.AcceptsReturn = True
        Me.act_txtIRY.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtIRY.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtIRY.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtIRY.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtIRY.Location = New System.Drawing.Point(73, 61)
        Me.act_txtIRY.MaxLength = 0
        Me.act_txtIRY.Name = "act_txtIRY"
        Me.act_txtIRY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtIRY.Size = New System.Drawing.Size(51, 23)
        Me.act_txtIRY.TabIndex = 55
        Me.act_txtIRY.Text = "0"
        '
        'zlbl104
        '
        Me.zlbl104.AutoSize = True
        Me.zlbl104.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl104.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl104.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl104.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl104.Location = New System.Drawing.Point(42, 64)
        Me.zlbl104.Name = "zlbl104"
        Me.zlbl104.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl104.Size = New System.Drawing.Size(26, 16)
        Me.zlbl104.TabIndex = 6
        Me.zlbl104.Text = "Iry"
        Me.zlbl104.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl105
        '
        Me.zlbl105.AutoSize = True
        Me.zlbl105.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl105.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl105.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl105.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl105.Location = New System.Drawing.Point(42, 100)
        Me.zlbl105.Name = "zlbl105"
        Me.zlbl105.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl105.Size = New System.Drawing.Size(25, 16)
        Me.zlbl105.TabIndex = 29
        Me.zlbl105.Text = "Irz"
        Me.zlbl105.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl103
        '
        Me.zlbl103.AutoSize = True
        Me.zlbl103.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl103.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl103.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl103.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl103.Location = New System.Drawing.Point(42, 27)
        Me.zlbl103.Name = "zlbl103"
        Me.zlbl103.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl103.Size = New System.Drawing.Size(25, 16)
        Me.zlbl103.TabIndex = 6
        Me.zlbl103.Text = "Irx"
        Me.zlbl103.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'act_txtIRZ
        '
        Me.act_txtIRZ.AcceptsReturn = True
        Me.act_txtIRZ.BackColor = System.Drawing.SystemColors.Window
        Me.act_txtIRZ.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.act_txtIRZ.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_txtIRZ.ForeColor = System.Drawing.SystemColors.WindowText
        Me.act_txtIRZ.Location = New System.Drawing.Point(73, 97)
        Me.act_txtIRZ.MaxLength = 0
        Me.act_txtIRZ.Name = "act_txtIRZ"
        Me.act_txtIRZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.act_txtIRZ.Size = New System.Drawing.Size(51, 23)
        Me.act_txtIRZ.TabIndex = 56
        Me.act_txtIRZ.Text = "0"
        '
        'Onglet_att_manag
        '
        Me.Onglet_att_manag.BackColor = System.Drawing.Color.Transparent
        Me.Onglet_att_manag.Controls.Add(Me.zfra_25)
        Me.Onglet_att_manag.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.Onglet_att_manag.Location = New System.Drawing.Point(4, 32)
        Me.Onglet_att_manag.Name = "Onglet_att_manag"
        Me.Onglet_att_manag.Size = New System.Drawing.Size(627, 520)
        Me.Onglet_att_manag.TabIndex = 4
        Me.Onglet_att_manag.Text = "Management"
        '
        'zfra_25
        '
        Me.zfra_25.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_25.Controls.Add(Me.gest_cmdOpen)
        Me.zfra_25.Controls.Add(Me.gest_cmdSave)
        Me.zfra_25.Controls.Add(Me.gest_txtName)
        Me.zfra_25.Controls.Add(Me.zlbl202)
        Me.zfra_25.Controls.Add(Me.gest_txtDescription)
        Me.zfra_25.Controls.Add(Me.zlbl200)
        Me.zfra_25.Controls.Add(Me.Label3)
        Me.zfra_25.Controls.Add(Me.gest_txtTime)
        Me.zfra_25.Controls.Add(Me.zlbl201)
        Me.zfra_25.Controls.Add(Me.gest_cmdAdd)
        Me.zfra_25.Controls.Add(Me.gest_cmdRemove)
        Me.zfra_25.Controls.Add(Me.gest_cmdModify)
        Me.zfra_25.Controls.Add(Me.gest_lstEvents)
        Me.zfra_25.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_25.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_25.Location = New System.Drawing.Point(33, 31)
        Me.zfra_25.Name = "zfra_25"
        Me.zfra_25.Size = New System.Drawing.Size(561, 460)
        Me.zfra_25.TabIndex = 40
        Me.zfra_25.TabStop = False
        Me.zfra_25.Text = "List of events"
        '
        'gest_cmdOpen
        '
        Me.gest_cmdOpen.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gest_cmdOpen.ForeColor = System.Drawing.Color.Black
        Me.gest_cmdOpen.Location = New System.Drawing.Point(447, 68)
        Me.gest_cmdOpen.Name = "gest_cmdOpen"
        Me.gest_cmdOpen.Size = New System.Drawing.Size(92, 33)
        Me.gest_cmdOpen.TabIndex = 86
        Me.gest_cmdOpen.Text = "Open"
        Me.gest_cmdOpen.UseVisualStyleBackColor = True
        '
        'gest_cmdSave
        '
        Me.gest_cmdSave.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gest_cmdSave.ForeColor = System.Drawing.Color.Black
        Me.gest_cmdSave.Location = New System.Drawing.Point(447, 29)
        Me.gest_cmdSave.Name = "gest_cmdSave"
        Me.gest_cmdSave.Size = New System.Drawing.Size(92, 33)
        Me.gest_cmdSave.TabIndex = 85
        Me.gest_cmdSave.Text = "Export"
        Me.gest_cmdSave.UseVisualStyleBackColor = True
        '
        'gest_txtName
        '
        Me.gest_txtName.AcceptsReturn = True
        Me.gest_txtName.BackColor = System.Drawing.SystemColors.Window
        Me.gest_txtName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gest_txtName.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gest_txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gest_txtName.Location = New System.Drawing.Point(262, 31)
        Me.gest_txtName.MaxLength = 0
        Me.gest_txtName.Name = "gest_txtName"
        Me.gest_txtName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gest_txtName.Size = New System.Drawing.Size(178, 27)
        Me.gest_txtName.TabIndex = 83
        Me.gest_txtName.Text = "Scenario_1"
        '
        'zlbl202
        '
        Me.zlbl202.AutoSize = True
        Me.zlbl202.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl202.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl202.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl202.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl202.Location = New System.Drawing.Point(202, 34)
        Me.zlbl202.Name = "zlbl202"
        Me.zlbl202.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl202.Size = New System.Drawing.Size(55, 18)
        Me.zlbl202.TabIndex = 84
        Me.zlbl202.Text = "Name"
        '
        'gest_txtDescription
        '
        Me.gest_txtDescription.AcceptsReturn = True
        Me.gest_txtDescription.BackColor = System.Drawing.SystemColors.Window
        Me.gest_txtDescription.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gest_txtDescription.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gest_txtDescription.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gest_txtDescription.Location = New System.Drawing.Point(207, 190)
        Me.gest_txtDescription.MaxLength = 0
        Me.gest_txtDescription.Multiline = True
        Me.gest_txtDescription.Name = "gest_txtDescription"
        Me.gest_txtDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gest_txtDescription.Size = New System.Drawing.Size(332, 195)
        Me.gest_txtDescription.TabIndex = 82
        '
        'zlbl200
        '
        Me.zlbl200.AutoSize = True
        Me.zlbl200.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl200.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl200.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl200.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl200.Location = New System.Drawing.Point(204, 160)
        Me.zlbl200.Name = "zlbl200"
        Me.zlbl200.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl200.Size = New System.Drawing.Size(87, 16)
        Me.zlbl200.TabIndex = 81
        Me.zlbl200.Text = "Description:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(519, 125)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(15, 16)
        Me.Label3.TabIndex = 77
        Me.Label3.Text = "s"
        '
        'gest_txtTime
        '
        Me.gest_txtTime.AcceptsReturn = True
        Me.gest_txtTime.BackColor = System.Drawing.SystemColors.Window
        Me.gest_txtTime.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gest_txtTime.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gest_txtTime.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gest_txtTime.Location = New System.Drawing.Point(438, 122)
        Me.gest_txtTime.MaxLength = 0
        Me.gest_txtTime.Name = "gest_txtTime"
        Me.gest_txtTime.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gest_txtTime.Size = New System.Drawing.Size(75, 23)
        Me.gest_txtTime.TabIndex = 75
        Me.gest_txtTime.Text = "0"
        '
        'zlbl201
        '
        Me.zlbl201.AutoSize = True
        Me.zlbl201.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl201.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl201.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl201.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl201.Location = New System.Drawing.Point(204, 125)
        Me.zlbl201.Name = "zlbl201"
        Me.zlbl201.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl201.Size = New System.Drawing.Size(222, 16)
        Me.zlbl201.TabIndex = 76
        Me.zlbl201.Text = "Time of occurrence (after start)"
        '
        'gest_cmdAdd
        '
        Me.gest_cmdAdd.BackColor = System.Drawing.SystemColors.Control
        Me.gest_cmdAdd.Cursor = System.Windows.Forms.Cursors.Default
        Me.gest_cmdAdd.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gest_cmdAdd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gest_cmdAdd.Location = New System.Drawing.Point(95, 404)
        Me.gest_cmdAdd.Name = "gest_cmdAdd"
        Me.gest_cmdAdd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gest_cmdAdd.Size = New System.Drawing.Size(114, 37)
        Me.gest_cmdAdd.TabIndex = 25
        Me.gest_cmdAdd.Text = "Add"
        Me.gest_cmdAdd.UseVisualStyleBackColor = False
        '
        'gest_cmdRemove
        '
        Me.gest_cmdRemove.BackColor = System.Drawing.SystemColors.Control
        Me.gest_cmdRemove.Cursor = System.Windows.Forms.Cursors.Default
        Me.gest_cmdRemove.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gest_cmdRemove.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gest_cmdRemove.Location = New System.Drawing.Point(225, 404)
        Me.gest_cmdRemove.Name = "gest_cmdRemove"
        Me.gest_cmdRemove.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gest_cmdRemove.Size = New System.Drawing.Size(114, 37)
        Me.gest_cmdRemove.TabIndex = 24
        Me.gest_cmdRemove.Text = "Remove"
        Me.gest_cmdRemove.UseVisualStyleBackColor = False
        '
        'gest_cmdModify
        '
        Me.gest_cmdModify.BackColor = System.Drawing.SystemColors.Control
        Me.gest_cmdModify.Cursor = System.Windows.Forms.Cursors.Default
        Me.gest_cmdModify.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gest_cmdModify.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gest_cmdModify.Location = New System.Drawing.Point(355, 404)
        Me.gest_cmdModify.Name = "gest_cmdModify"
        Me.gest_cmdModify.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gest_cmdModify.Size = New System.Drawing.Size(114, 37)
        Me.gest_cmdModify.TabIndex = 23
        Me.gest_cmdModify.Text = "Modify"
        Me.gest_cmdModify.UseVisualStyleBackColor = False
        '
        'gest_lstEvents
        '
        Me.gest_lstEvents.BackColor = System.Drawing.SystemColors.Window
        Me.gest_lstEvents.Cursor = System.Windows.Forms.Cursors.Default
        Me.gest_lstEvents.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gest_lstEvents.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gest_lstEvents.ItemHeight = 16
        Me.gest_lstEvents.Location = New System.Drawing.Point(22, 29)
        Me.gest_lstEvents.Name = "gest_lstEvents"
        Me.gest_lstEvents.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gest_lstEvents.Size = New System.Drawing.Size(163, 356)
        Me.gest_lstEvents.TabIndex = 22
        '
        'zfra_9
        '
        Me.zfra_9.BackColor = System.Drawing.Color.LightGray
        Me.zfra_9.Controls.Add(Me.mass_lblSatMass)
        Me.zfra_9.Controls.Add(Me.zlbl30)
        Me.zfra_9.Controls.Add(Me.zlbl28)
        Me.zfra_9.Controls.Add(Me.zfra_15)
        Me.zfra_9.Controls.Add(Me.zfra_16)
        Me.zfra_9.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_9.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_9.Location = New System.Drawing.Point(678, 427)
        Me.zfra_9.Name = "zfra_9"
        Me.zfra_9.Size = New System.Drawing.Size(350, 152)
        Me.zfra_9.TabIndex = 38
        Me.zfra_9.TabStop = False
        Me.zfra_9.Text = "Synthesis"
        '
        'mass_lblSatMass
        '
        Me.mass_lblSatMass.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatMass.ForeColor = System.Drawing.Color.Black
        Me.mass_lblSatMass.Location = New System.Drawing.Point(153, 27)
        Me.mass_lblSatMass.Name = "mass_lblSatMass"
        Me.mass_lblSatMass.Size = New System.Drawing.Size(43, 17)
        Me.mass_lblSatMass.TabIndex = 41
        Me.mass_lblSatMass.Text = "0"
        '
        'zlbl30
        '
        Me.zlbl30.AutoSize = True
        Me.zlbl30.BackColor = System.Drawing.Color.Transparent
        Me.zlbl30.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl30.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl30.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl30.Location = New System.Drawing.Point(197, 25)
        Me.zlbl30.Name = "zlbl30"
        Me.zlbl30.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl30.Size = New System.Drawing.Size(23, 16)
        Me.zlbl30.TabIndex = 40
        Me.zlbl30.Text = "kg"
        Me.zlbl30.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl28
        '
        Me.zlbl28.AutoSize = True
        Me.zlbl28.BackColor = System.Drawing.Color.Transparent
        Me.zlbl28.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl28.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl28.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl28.Location = New System.Drawing.Point(110, 25)
        Me.zlbl28.Name = "zlbl28"
        Me.zlbl28.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl28.Size = New System.Drawing.Size(41, 16)
        Me.zlbl28.TabIndex = 40
        Me.zlbl28.Text = "Mass"
        Me.zlbl28.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_15
        '
        Me.zfra_15.Controls.Add(Me.mass_lblSatGz)
        Me.zfra_15.Controls.Add(Me.mass_lblSatGy)
        Me.zfra_15.Controls.Add(Me.mass_lblSatGx)
        Me.zfra_15.Controls.Add(Me.zlbl37)
        Me.zfra_15.Controls.Add(Me.zlbl38)
        Me.zfra_15.Controls.Add(Me.zlbl40)
        Me.zfra_15.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_15.ForeColor = System.Drawing.Color.Black
        Me.zfra_15.Location = New System.Drawing.Point(16, 49)
        Me.zfra_15.Name = "zfra_15"
        Me.zfra_15.Size = New System.Drawing.Size(105, 91)
        Me.zfra_15.TabIndex = 36
        Me.zfra_15.TabStop = False
        Me.zfra_15.Text = "C.G (m)"
        '
        'mass_lblSatGz
        '
        Me.mass_lblSatGz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatGz.Location = New System.Drawing.Point(45, 65)
        Me.mass_lblSatGz.Name = "mass_lblSatGz"
        Me.mass_lblSatGz.Size = New System.Drawing.Size(50, 16)
        Me.mass_lblSatGz.TabIndex = 39
        Me.mass_lblSatGz.Text = "0"
        Me.mass_lblSatGz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatGy
        '
        Me.mass_lblSatGy.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatGy.Location = New System.Drawing.Point(45, 45)
        Me.mass_lblSatGy.Name = "mass_lblSatGy"
        Me.mass_lblSatGy.Size = New System.Drawing.Size(50, 16)
        Me.mass_lblSatGy.TabIndex = 39
        Me.mass_lblSatGy.Text = "0"
        Me.mass_lblSatGy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatGx
        '
        Me.mass_lblSatGx.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatGx.Location = New System.Drawing.Point(45, 25)
        Me.mass_lblSatGx.Name = "mass_lblSatGx"
        Me.mass_lblSatGx.Size = New System.Drawing.Size(50, 16)
        Me.mass_lblSatGx.TabIndex = 39
        Me.mass_lblSatGx.Text = "0"
        Me.mass_lblSatGx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl37
        '
        Me.zlbl37.BackColor = System.Drawing.Color.Transparent
        Me.zlbl37.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl37.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl37.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl37.Location = New System.Drawing.Point(15, 25)
        Me.zlbl37.Name = "zlbl37"
        Me.zlbl37.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl37.Size = New System.Drawing.Size(24, 16)
        Me.zlbl37.TabIndex = 27
        Me.zlbl37.Text = "Gx"
        Me.zlbl37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl38
        '
        Me.zlbl38.BackColor = System.Drawing.Color.Transparent
        Me.zlbl38.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl38.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl38.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl38.Location = New System.Drawing.Point(15, 45)
        Me.zlbl38.Name = "zlbl38"
        Me.zlbl38.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl38.Size = New System.Drawing.Size(25, 16)
        Me.zlbl38.TabIndex = 29
        Me.zlbl38.Text = "Gy"
        Me.zlbl38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl40
        '
        Me.zlbl40.BackColor = System.Drawing.Color.Transparent
        Me.zlbl40.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl40.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl40.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl40.Location = New System.Drawing.Point(15, 65)
        Me.zlbl40.Name = "zlbl40"
        Me.zlbl40.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl40.Size = New System.Drawing.Size(24, 16)
        Me.zlbl40.TabIndex = 28
        Me.zlbl40.Text = "Gz"
        Me.zlbl40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zfra_16
        '
        Me.zfra_16.Controls.Add(Me.mass_lblSatIxz)
        Me.zfra_16.Controls.Add(Me.mass_lblSatIxy)
        Me.zfra_16.Controls.Add(Me.mass_lblSatIyz)
        Me.zfra_16.Controls.Add(Me.mass_lblSatIxx)
        Me.zfra_16.Controls.Add(Me.mass_lblSatIyy)
        Me.zfra_16.Controls.Add(Me.mass_lblSatIzz)
        Me.zfra_16.Controls.Add(Me.zlbl25)
        Me.zfra_16.Controls.Add(Me.zlbl26)
        Me.zfra_16.Controls.Add(Me.zlbl29)
        Me.zfra_16.Controls.Add(Me.zlbl32)
        Me.zfra_16.Controls.Add(Me.zlbl33)
        Me.zfra_16.Controls.Add(Me.zlbl36)
        Me.zfra_16.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_16.ForeColor = System.Drawing.Color.Black
        Me.zfra_16.Location = New System.Drawing.Point(133, 49)
        Me.zfra_16.Name = "zfra_16"
        Me.zfra_16.Size = New System.Drawing.Size(203, 91)
        Me.zfra_16.TabIndex = 37
        Me.zfra_16.TabStop = False
        Me.zfra_16.Text = "Inertias (kg.m²)"
        '
        'mass_lblSatIxz
        '
        Me.mass_lblSatIxz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatIxz.Location = New System.Drawing.Point(136, 46)
        Me.mass_lblSatIxz.Name = "mass_lblSatIxz"
        Me.mass_lblSatIxz.Size = New System.Drawing.Size(60, 16)
        Me.mass_lblSatIxz.TabIndex = 39
        Me.mass_lblSatIxz.Text = "0"
        Me.mass_lblSatIxz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatIxy
        '
        Me.mass_lblSatIxy.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatIxy.Location = New System.Drawing.Point(136, 26)
        Me.mass_lblSatIxy.Name = "mass_lblSatIxy"
        Me.mass_lblSatIxy.Size = New System.Drawing.Size(60, 16)
        Me.mass_lblSatIxy.TabIndex = 39
        Me.mass_lblSatIxy.Text = "0"
        Me.mass_lblSatIxy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatIyz
        '
        Me.mass_lblSatIyz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatIyz.Location = New System.Drawing.Point(136, 65)
        Me.mass_lblSatIyz.Name = "mass_lblSatIyz"
        Me.mass_lblSatIyz.Size = New System.Drawing.Size(60, 16)
        Me.mass_lblSatIyz.TabIndex = 39
        Me.mass_lblSatIyz.Text = "0"
        Me.mass_lblSatIyz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatIxx
        '
        Me.mass_lblSatIxx.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatIxx.Location = New System.Drawing.Point(37, 25)
        Me.mass_lblSatIxx.Name = "mass_lblSatIxx"
        Me.mass_lblSatIxx.Size = New System.Drawing.Size(60, 16)
        Me.mass_lblSatIxx.TabIndex = 39
        Me.mass_lblSatIxx.Text = "0"
        Me.mass_lblSatIxx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatIyy
        '
        Me.mass_lblSatIyy.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatIyy.Location = New System.Drawing.Point(37, 45)
        Me.mass_lblSatIyy.Name = "mass_lblSatIyy"
        Me.mass_lblSatIyy.Size = New System.Drawing.Size(60, 16)
        Me.mass_lblSatIyy.TabIndex = 39
        Me.mass_lblSatIyy.Text = "0"
        Me.mass_lblSatIyy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatIzz
        '
        Me.mass_lblSatIzz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatIzz.Location = New System.Drawing.Point(37, 65)
        Me.mass_lblSatIzz.Name = "mass_lblSatIzz"
        Me.mass_lblSatIzz.Size = New System.Drawing.Size(60, 16)
        Me.mass_lblSatIzz.TabIndex = 39
        Me.mass_lblSatIzz.Text = "0"
        Me.mass_lblSatIzz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl25
        '
        Me.zlbl25.BackColor = System.Drawing.Color.Transparent
        Me.zlbl25.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl25.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl25.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl25.Location = New System.Drawing.Point(7, 45)
        Me.zlbl25.Name = "zlbl25"
        Me.zlbl25.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl25.Size = New System.Drawing.Size(29, 16)
        Me.zlbl25.TabIndex = 5
        Me.zlbl25.Text = "Iyy"
        Me.zlbl25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl26
        '
        Me.zlbl26.BackColor = System.Drawing.Color.Transparent
        Me.zlbl26.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl26.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl26.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl26.Location = New System.Drawing.Point(7, 65)
        Me.zlbl26.Name = "zlbl26"
        Me.zlbl26.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl26.Size = New System.Drawing.Size(27, 16)
        Me.zlbl26.TabIndex = 6
        Me.zlbl26.Text = "Izz"
        Me.zlbl26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl29
        '
        Me.zlbl29.BackColor = System.Drawing.Color.Transparent
        Me.zlbl29.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl29.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl29.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl29.Location = New System.Drawing.Point(7, 25)
        Me.zlbl29.Name = "zlbl29"
        Me.zlbl29.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl29.Size = New System.Drawing.Size(27, 16)
        Me.zlbl29.TabIndex = 6
        Me.zlbl29.Text = "Ixx"
        Me.zlbl29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl32
        '
        Me.zlbl32.BackColor = System.Drawing.Color.Transparent
        Me.zlbl32.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl32.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl32.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl32.Location = New System.Drawing.Point(108, 64)
        Me.zlbl32.Name = "zlbl32"
        Me.zlbl32.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl32.Size = New System.Drawing.Size(28, 16)
        Me.zlbl32.TabIndex = 27
        Me.zlbl32.Text = "Iyz"
        Me.zlbl32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl33
        '
        Me.zlbl33.BackColor = System.Drawing.Color.Transparent
        Me.zlbl33.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl33.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl33.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl33.Location = New System.Drawing.Point(108, 45)
        Me.zlbl33.Name = "zlbl33"
        Me.zlbl33.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl33.Size = New System.Drawing.Size(27, 16)
        Me.zlbl33.TabIndex = 28
        Me.zlbl33.Text = "Ixz"
        Me.zlbl33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl36
        '
        Me.zlbl36.BackColor = System.Drawing.Color.Transparent
        Me.zlbl36.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl36.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl36.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl36.Location = New System.Drawing.Point(108, 25)
        Me.zlbl36.Name = "zlbl36"
        Me.zlbl36.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl36.Size = New System.Drawing.Size(28, 16)
        Me.zlbl36.TabIndex = 29
        Me.zlbl36.Text = "Ixy"
        Me.zlbl36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_pictSatellite
        '
        Me.mass_pictSatellite.BackColor = System.Drawing.SystemColors.Control
        Me.mass_pictSatellite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mass_pictSatellite.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_pictSatellite.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_pictSatellite.Location = New System.Drawing.Point(678, 55)
        Me.mass_pictSatellite.Name = "mass_pictSatellite"
        Me.mass_pictSatellite.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_pictSatellite.Size = New System.Drawing.Size(350, 350)
        Me.mass_pictSatellite.TabIndex = 10
        Me.mass_pictSatellite.TabStop = False
        '
        'cmd_quit
        '
        Me.cmd_quit.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_quit.Location = New System.Drawing.Point(550, 585)
        Me.cmd_quit.Name = "cmd_quit"
        Me.cmd_quit.Size = New System.Drawing.Size(108, 36)
        Me.cmd_quit.TabIndex = 3
        Me.cmd_quit.Text = "Close"
        Me.cmd_quit.UseVisualStyleBackColor = True
        '
        'tmrAnimate
        '
        Me.tmrAnimate.Enabled = True
        '
        'tmrTransition
        '
        Me.tmrTransition.Interval = 10
        '
        'mass_lblmsg
        '
        Me.mass_lblmsg.AutoSize = True
        Me.mass_lblmsg.BackColor = System.Drawing.Color.Red
        Me.mass_lblmsg.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblmsg.Location = New System.Drawing.Point(26, 596)
        Me.mass_lblmsg.Name = "mass_lblmsg"
        Me.mass_lblmsg.Size = New System.Drawing.Size(0, 16)
        Me.mass_lblmsg.TabIndex = 39
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(682, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(343, 14)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "Mouse Control: Rotation (Left Click) / Zoom (Right Click)"
        '
        'dlgScenarioOpen
        '
        Me.dlgScenarioOpen.FileName = "OpenFileDialog1"
        Me.dlgScenarioOpen.Filter = "(*.scn)|*.scn"
        '
        'dlgScenarioSave
        '
        Me.dlgScenarioSave.Filter = "(*.scn)|*.scn"
        '
        'frmAttConf
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1049, 632)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.mass_lblmsg)
        Me.Controls.Add(Me.zfra_9)
        Me.Controls.Add(Me.cmd_quit)
        Me.Controls.Add(Me.tabPower)
        Me.Controls.Add(Me.mass_pictSatellite)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmAttConf"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Attitude Control Data"
        Me.tabPower.ResumeLayout(False)
        Me.Onglet_att_geom.ResumeLayout(False)
        Me.zfra_8.ResumeLayout(False)
        Me.zfra_8.PerformLayout()
        Me.zfra_4.ResumeLayout(False)
        Me.zfra_4.PerformLayout()
        CType(Me.zpict_5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.zfra_3.ResumeLayout(False)
        Me.zfra_3.PerformLayout()
        Me.zfra_7.ResumeLayout(False)
        Me.zfra_7.PerformLayout()
        Me.zfra_6.ResumeLayout(False)
        Me.zfra_6.PerformLayout()
        Me.zfra_5.ResumeLayout(False)
        Me.zfra_5.PerformLayout()
        Me.Onglet_att_prop.ResumeLayout(False)
        Me.zfra_14.ResumeLayout(False)
        Me.zfra_14.PerformLayout()
        Me.zfra_11.ResumeLayout(False)
        Me.zfra_12.ResumeLayout(False)
        Me.zfra_12.PerformLayout()
        Me.zfra_13.ResumeLayout(False)
        Me.zfra_13.PerformLayout()
        Me.zfra_1.ResumeLayout(False)
        Me.zfra_10.ResumeLayout(False)
        Me.zfra_10.PerformLayout()
        Me.zfra_2.ResumeLayout(False)
        Me.zfra_2.PerformLayout()
        Me.Onglet_att_act.ResumeLayout(False)
        Me.Onglet_att_act.PerformLayout()
        Me.zfra_24.ResumeLayout(False)
        Me.zfra_24.PerformLayout()
        Me.zfra_23.ResumeLayout(False)
        Me.zfra_23.PerformLayout()
        Me.zfra_232.ResumeLayout(False)
        Me.zfra_232.PerformLayout()
        Me.zfra_231.ResumeLayout(False)
        Me.zfra_231.PerformLayout()
        Me.zfra_22.ResumeLayout(False)
        Me.zfra_22.PerformLayout()
        Me.zfra_21.ResumeLayout(False)
        Me.zfra_21.PerformLayout()
        Me.Onglet_att_manag.ResumeLayout(False)
        Me.zfra_25.ResumeLayout(False)
        Me.zfra_25.PerformLayout()
        Me.zfra_9.ResumeLayout(False)
        Me.zfra_9.PerformLayout()
        Me.zfra_15.ResumeLayout(False)
        Me.zfra_16.ResumeLayout(False)
        CType(Me.mass_pictSatellite, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents tabPower As System.Windows.Forms.TabControl
    Public WithEvents Onglet_att_prop As System.Windows.Forms.TabPage
    Public WithEvents Onglet_att_manag As System.Windows.Forms.TabPage
    Friend WithEvents cmd_quit As System.Windows.Forms.Button
    Public WithEvents tmrAnimate As System.Windows.Forms.Timer
    Public WithEvents tmrTransition As System.Windows.Forms.Timer
    Public WithEvents zfra_4 As System.Windows.Forms.GroupBox
    Public WithEvents mass_lstPanels As System.Windows.Forms.ListBox
    Public WithEvents mass_cmdModify As System.Windows.Forms.Button
    Friend WithEvents mass_txtPanelMass As System.Windows.Forms.TextBox
    Public WithEvents mass_cmdAddPanel As System.Windows.Forms.Button
    Public WithEvents zlbl_75 As System.Windows.Forms.Label
    Public WithEvents mass_cmdRemovePanel As System.Windows.Forms.Button
    Public WithEvents zlbl_64 As System.Windows.Forms.Label
    Public WithEvents zlbl_63 As System.Windows.Forms.Label
    Public WithEvents mass_cmbFace As System.Windows.Forms.ComboBox
    Public WithEvents mass_txtPanelLength As System.Windows.Forms.TextBox
    Public WithEvents mass_txtPanelWidth As System.Windows.Forms.TextBox
    Public WithEvents mass_cmbDirection As System.Windows.Forms.ComboBox
    Public WithEvents mass_cmbPosition As System.Windows.Forms.ComboBox
    Public WithEvents mass_optPanelType_0 As System.Windows.Forms.RadioButton
    Public WithEvents mass_optPanelType_1 As System.Windows.Forms.RadioButton
    Public WithEvents mass_optPanelType_2 As System.Windows.Forms.RadioButton
    Public WithEvents zlbl_69 As System.Windows.Forms.Label
    Public WithEvents zlbl_1 As System.Windows.Forms.Label
    Public WithEvents zlbl_5 As System.Windows.Forms.Label
    Public WithEvents zlbl_6 As System.Windows.Forms.Label
    Public WithEvents mass_lblPanelOrient As System.Windows.Forms.Label
    Public WithEvents mass_lblPanelPos As System.Windows.Forms.Label
    Public WithEvents zlbl_3 As System.Windows.Forms.Label
    Public WithEvents zpict_5 As System.Windows.Forms.PictureBox
    Public WithEvents zfra_3 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_11 As System.Windows.Forms.Label
    Public WithEvents mass_txtBodyMass As System.Windows.Forms.TextBox
    Public WithEvents zlbl_15 As System.Windows.Forms.Label
    Friend WithEvents zfra_7 As System.Windows.Forms.GroupBox
    Public WithEvents mass_txtGy As System.Windows.Forms.TextBox
    Public WithEvents zlbl_74 As System.Windows.Forms.Label
    Public WithEvents zlbl_83 As System.Windows.Forms.Label
    Public WithEvents mass_txtGx As System.Windows.Forms.TextBox
    Public WithEvents zlbl_78 As System.Windows.Forms.Label
    Public WithEvents zlbl_82 As System.Windows.Forms.Label
    Public WithEvents mass_txtGz As System.Windows.Forms.TextBox
    Public WithEvents zlbl_79 As System.Windows.Forms.Label
    Public WithEvents zlbl_80 As System.Windows.Forms.Label
    Friend WithEvents zfra_6 As System.Windows.Forms.GroupBox
    Public WithEvents mass_txtIyz As System.Windows.Forms.TextBox
    Public WithEvents zlbl_85 As System.Windows.Forms.Label
    Public WithEvents zlbl_86 As System.Windows.Forms.Label
    Public WithEvents zlbl_14 As System.Windows.Forms.Label
    Public WithEvents mass_txtIyy As System.Windows.Forms.TextBox
    Public WithEvents zlbl_13 As System.Windows.Forms.Label
    Public WithEvents zlbl_84 As System.Windows.Forms.Label
    Public WithEvents zlbl_12 As System.Windows.Forms.Label
    Public WithEvents mass_txtIzz As System.Windows.Forms.TextBox
    Public WithEvents mass_txtIxz As System.Windows.Forms.TextBox
    Public WithEvents mass_txtIxx As System.Windows.Forms.TextBox
    Public WithEvents mass_txtIxy As System.Windows.Forms.TextBox
    Public WithEvents zlbl_87 As System.Windows.Forms.Label
    Public WithEvents zlbl_10 As System.Windows.Forms.Label
    Public WithEvents zlbl_88 As System.Windows.Forms.Label
    Friend WithEvents zfra_5 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_0 As System.Windows.Forms.Label
    Public WithEvents zlbl_2 As System.Windows.Forms.Label
    Public WithEvents mass_txtBodyWidth As System.Windows.Forms.TextBox
    Public WithEvents mass_txtBodyHeight As System.Windows.Forms.TextBox
    Public WithEvents zlbl_65 As System.Windows.Forms.Label
    Public WithEvents zlbl_66 As System.Windows.Forms.Label
    Public WithEvents mass_pictSatellite As System.Windows.Forms.PictureBox
    Public WithEvents zfra_8 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_17 As System.Windows.Forms.Label
    Public WithEvents zlbl_18 As System.Windows.Forms.Label
    Public WithEvents mass_cmbBoomFace As System.Windows.Forms.ComboBox
    Public WithEvents mass_txtBoomMass As System.Windows.Forms.TextBox
    Public WithEvents mass_txtBoomDist As System.Windows.Forms.TextBox
    Public WithEvents zlbl_19 As System.Windows.Forms.Label
    Public WithEvents zlbl_16 As System.Windows.Forms.Label
    Public WithEvents zlbl_20 As System.Windows.Forms.Label
    Friend WithEvents zfra_1 As System.Windows.Forms.GroupBox
    Public WithEvents prop_txtCN_4 As System.Windows.Forms.TextBox
    Public WithEvents zlbl1 As System.Windows.Forms.Label
    Public WithEvents zlbl2 As System.Windows.Forms.Label
    Public WithEvents prop_txtCN_3 As System.Windows.Forms.TextBox
    Public WithEvents zlbl5 As System.Windows.Forms.Label
    Public WithEvents prop_txtCN_1 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCN_5 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCN_0 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCN_2 As System.Windows.Forms.TextBox
    Public WithEvents zlbl7 As System.Windows.Forms.Label
    Public WithEvents zlbl8 As System.Windows.Forms.Label
    Public WithEvents zlbl9 As System.Windows.Forms.Label
    Friend WithEvents zfra_2 As System.Windows.Forms.GroupBox
    Friend WithEvents zfra_11 As System.Windows.Forms.GroupBox
    Friend WithEvents zfra_12 As System.Windows.Forms.GroupBox
    Public WithEvents prop_txtCD_0 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCD_4 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCD_3 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCD_2 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCD_5 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCD_1 As System.Windows.Forms.TextBox
    Friend WithEvents zfra_13 As System.Windows.Forms.GroupBox
    Public WithEvents prop_txtCR_0 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCR_4 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCR_3 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCR_2 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCR_5 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCR_1 As System.Windows.Forms.TextBox
    Friend WithEvents zfra_10 As System.Windows.Forms.GroupBox
    Public WithEvents prop_txtCT_0 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCT_4 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCT_3 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCT_2 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCT_5 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCT_1 As System.Windows.Forms.TextBox
    Public WithEvents zlbl31 As System.Windows.Forms.Label
    Public WithEvents zlbl34 As System.Windows.Forms.Label
    Public WithEvents zlbl35 As System.Windows.Forms.Label
    Friend WithEvents zfra_14 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl24 As System.Windows.Forms.Label
    Public WithEvents zlbl23 As System.Windows.Forms.Label
    Public WithEvents zlbl22 As System.Windows.Forms.Label
    Public WithEvents zlbl19 As System.Windows.Forms.Label
    Public WithEvents zlbl20 As System.Windows.Forms.Label
    Public WithEvents zlbl21 As System.Windows.Forms.Label
    Public WithEvents zlbl18 As System.Windows.Forms.Label
    Public WithEvents zlbl17 As System.Windows.Forms.Label
    Public WithEvents zlbl16 As System.Windows.Forms.Label
    Public WithEvents zlbl15 As System.Windows.Forms.Label
    Public WithEvents zlbl13 As System.Windows.Forms.Label
    Public WithEvents zlbl14 As System.Windows.Forms.Label
    Public WithEvents zlbl12 As System.Windows.Forms.Label
    Public WithEvents zlbl11 As System.Windows.Forms.Label
    Public WithEvents zlbl10 As System.Windows.Forms.Label
    Public WithEvents zlbl6 As System.Windows.Forms.Label
    Public WithEvents zlbl4 As System.Windows.Forms.Label
    Public WithEvents zlbl3 As System.Windows.Forms.Label
    Friend WithEvents zfra_16 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl25 As System.Windows.Forms.Label
    Public WithEvents zlbl26 As System.Windows.Forms.Label
    Public WithEvents zlbl29 As System.Windows.Forms.Label
    Public WithEvents zlbl32 As System.Windows.Forms.Label
    Public WithEvents zlbl33 As System.Windows.Forms.Label
    Public WithEvents zlbl36 As System.Windows.Forms.Label
    Friend WithEvents zfra_9 As System.Windows.Forms.GroupBox
    Friend WithEvents zfra_15 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl37 As System.Windows.Forms.Label
    Public WithEvents zlbl38 As System.Windows.Forms.Label
    Public WithEvents zlbl40 As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatGx As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatGz As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatGy As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatIxz As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatIxy As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatIyz As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatIxx As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatIyy As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatIzz As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatMass As System.Windows.Forms.Label
    Public WithEvents zlbl30 As System.Windows.Forms.Label
    Public WithEvents zlbl28 As System.Windows.Forms.Label
    Public WithEvents zlbl102 As System.Windows.Forms.Label
    Public WithEvents zlbl101 As System.Windows.Forms.Label
    Public WithEvents zlbl100 As System.Windows.Forms.Label
    Friend WithEvents zfra_21 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl108 As System.Windows.Forms.Label
    Public WithEvents zlbl107 As System.Windows.Forms.Label
    Public WithEvents zlbl106 As System.Windows.Forms.Label
    Public WithEvents act_txtIRX As System.Windows.Forms.TextBox
    Public WithEvents act_txtIRY As System.Windows.Forms.TextBox
    Public WithEvents zlbl104 As System.Windows.Forms.Label
    Public WithEvents zlbl105 As System.Windows.Forms.Label
    Public WithEvents zlbl103 As System.Windows.Forms.Label
    Public WithEvents act_txtIRZ As System.Windows.Forms.TextBox
    Public WithEvents zlbl114 As System.Windows.Forms.Label
    Public WithEvents zlbl113 As System.Windows.Forms.Label
    Public WithEvents zlbl112 As System.Windows.Forms.Label
    Public WithEvents act_txtWRX As System.Windows.Forms.TextBox
    Public WithEvents act_txtWRY As System.Windows.Forms.TextBox
    Public WithEvents zlbl110 As System.Windows.Forms.Label
    Public WithEvents zlbl111 As System.Windows.Forms.Label
    Public WithEvents zlbl109 As System.Windows.Forms.Label
    Public WithEvents act_txtWRZ As System.Windows.Forms.TextBox
    Friend WithEvents zfra_24 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl153 As System.Windows.Forms.Label
    Public WithEvents zlbl152 As System.Windows.Forms.Label
    Public WithEvents zlbl151 As System.Windows.Forms.Label
    Public WithEvents act_txtMN_X As System.Windows.Forms.TextBox
    Public WithEvents act_txtMN_Y As System.Windows.Forms.TextBox
    Public WithEvents zlbl149 As System.Windows.Forms.Label
    Public WithEvents zlbl150 As System.Windows.Forms.Label
    Public WithEvents zlbl148 As System.Windows.Forms.Label
    Public WithEvents act_txtMN_Z As System.Windows.Forms.TextBox
    Friend WithEvents zfra_23 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl147 As System.Windows.Forms.Label
    Public WithEvents zlbl146 As System.Windows.Forms.Label
    Public WithEvents zlbl145 As System.Windows.Forms.Label
    Public WithEvents act_txtKX As System.Windows.Forms.TextBox
    Public WithEvents act_txtKY As System.Windows.Forms.TextBox
    Public WithEvents zlbl143 As System.Windows.Forms.Label
    Public WithEvents zlbl144 As System.Windows.Forms.Label
    Public WithEvents zlbl142 As System.Windows.Forms.Label
    Public WithEvents act_txtKZ As System.Windows.Forms.TextBox
    Friend WithEvents zfra_232 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl135 As System.Windows.Forms.Label
    Public WithEvents zlbl134 As System.Windows.Forms.Label
    Public WithEvents act_txtDX As System.Windows.Forms.TextBox
    Public WithEvents act_txtDZ As System.Windows.Forms.TextBox
    Public WithEvents zlbl133 As System.Windows.Forms.Label
    Public WithEvents zlbl130 As System.Windows.Forms.Label
    Public WithEvents zlbl132 As System.Windows.Forms.Label
    Public WithEvents act_txtDY As System.Windows.Forms.TextBox
    Public WithEvents zlbl131 As System.Windows.Forms.Label
    Friend WithEvents zfra_231 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl129 As System.Windows.Forms.Label
    Public WithEvents zlbl128 As System.Windows.Forms.Label
    Public WithEvents act_txtMX As System.Windows.Forms.TextBox
    Public WithEvents act_txtMZ As System.Windows.Forms.TextBox
    Public WithEvents zlbl127 As System.Windows.Forms.Label
    Public WithEvents zlbl124 As System.Windows.Forms.Label
    Public WithEvents zlbl126 As System.Windows.Forms.Label
    Public WithEvents act_txtMY As System.Windows.Forms.TextBox
    Public WithEvents zlbl125 As System.Windows.Forms.Label
    Public WithEvents zlbl141 As System.Windows.Forms.Label
    Public WithEvents zlbl140 As System.Windows.Forms.Label
    Public WithEvents zlbl139 As System.Windows.Forms.Label
    Public WithEvents act_txtCX As System.Windows.Forms.TextBox
    Public WithEvents act_txtCY As System.Windows.Forms.TextBox
    Public WithEvents zlbl137 As System.Windows.Forms.Label
    Public WithEvents zlbl138 As System.Windows.Forms.Label
    Public WithEvents zlbl136 As System.Windows.Forms.Label
    Public WithEvents act_txtCZ As System.Windows.Forms.TextBox
    Friend WithEvents zfra_22 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl123 As System.Windows.Forms.Label
    Public WithEvents zlbl122 As System.Windows.Forms.Label
    Public WithEvents zlbl121 As System.Windows.Forms.Label
    Public WithEvents act_txtMG_X As System.Windows.Forms.TextBox
    Public WithEvents act_txtMG_Y As System.Windows.Forms.TextBox
    Public WithEvents zlbl119 As System.Windows.Forms.Label
    Public WithEvents zlbl120 As System.Windows.Forms.Label
    Public WithEvents zlbl118 As System.Windows.Forms.Label
    Public WithEvents act_txtMG_Z As System.Windows.Forms.TextBox
    Public WithEvents zlbl117 As System.Windows.Forms.Label
    Public WithEvents zlbl116 As System.Windows.Forms.Label
    Public WithEvents zlbl115 As System.Windows.Forms.Label
    Friend WithEvents zfra_25 As System.Windows.Forms.GroupBox
    Public WithEvents Onglet_att_act As System.Windows.Forms.TabPage
    Public WithEvents Onglet_att_geom As System.Windows.Forms.TabPage
    Friend WithEvents mass_lblmsg As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents prop_txtMG_X As System.Windows.Forms.TextBox
    Public WithEvents prop_txtMG_Y As System.Windows.Forms.TextBox
    Public WithEvents prop_txtMG_Z As System.Windows.Forms.TextBox
    Public WithEvents gest_cmdAdd As System.Windows.Forms.Button
    Public WithEvents gest_cmdRemove As System.Windows.Forms.Button
    Public WithEvents gest_cmdModify As System.Windows.Forms.Button
    Public WithEvents gest_lstEvents As System.Windows.Forms.ListBox
    Public WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents gest_txtTime As System.Windows.Forms.TextBox
    Public WithEvents zlbl201 As System.Windows.Forms.Label
    Public WithEvents zlbl200 As System.Windows.Forms.Label
    Public WithEvents gest_txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents gest_cmdOpen As System.Windows.Forms.Button
    Friend WithEvents gest_cmdSave As System.Windows.Forms.Button
    Public WithEvents gest_txtName As System.Windows.Forms.TextBox
    Public WithEvents zlbl202 As System.Windows.Forms.Label
    Friend WithEvents dlgScenarioOpen As System.Windows.Forms.OpenFileDialog
    Friend WithEvents dlgScenarioSave As System.Windows.Forms.SaveFileDialog
    Public WithEvents act_txtName As System.Windows.Forms.TextBox
    Public WithEvents zlbl154 As System.Windows.Forms.Label
    Public WithEvents act_cmdModify As System.Windows.Forms.Button
    Friend WithEvents mass_txtPanelNStrings As System.Windows.Forms.TextBox
    Public WithEvents lbl_nbstrings As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip

End Class
