Option Strict Off
Option Explicit On
Friend Class CSatellite
    '
    ' This class contains all needed information about the satellite.
    '
    ' It provides function to calculate its position and vectors to other "objects"
    ' The y-vector looks forward, the x-vector to the right and the z-vector upward.
    '
    '
    ' Data that belongs in the DB
    ' ---------------------------
    Private name As String
    Private color As Integer                ' Color of the satellite
    Private fTrackSize, bTrackSize As Short ' Forward and backward tracksize of the satellite (ForwardTrackSize, BackTrackSize)
    Private footprint As Boolean            ' Display of the satellite is active? (blnFootprint)
    '
    ' New data for the DB
    ' -------------------
    Private primDir As String               ' Primary direction in words
    Private primAxis As String              ' Primary axis ("x", "y", "z", "-x", "-y", "-z")
    Private primConToSat As CSatellite      ' Reference to the satellite for the primary direction
    '
    ' Central body size
    Private BodyHeight As Double
    Private BodyWidth As Single
    Private BodyDepth As Single             ' (NEW: 10.05.17) I don't think it's necessary because apparently BodyWidth = BodyDepth
    '
    Private secDir As String                ' Secondary direction in words
    Private secAxis As String               ' Secondary axis ("x", "y", "z", "-x", "-y", "-z")
    Private secConToSat As CSatellite       ' Reference to the satellite for the secondary direction
    '
    ' Calculated data
    ' ---------------
    Private simTime As Double               ' Time of the data
    Private position_calculated As Boolean  ' Are the satPos, satVel, satOldOrbit, satNewOrbit and geodesicPos up to date ?
    Private X As vector                     ' Calculated direction of the x-axis
    Private Y As vector                     ' Calculated direction of the y-axis
    Private Z As vector                     ' Calculated direction of the z-axis
    Private axis_calculated As Boolean      ' Are x, y and z up to date ?


    ''' <summary>
    ''' Check whether the simulation time has changed and so the older results are no longer correct
    ''' </summary>
    ''' <param name="time">Time actual time.</param>
    ''' <remarks></remarks>
    Private Sub checkForChanges(ByRef time As Double)
        If time <> simTime Then
            simTime = time
            Call clearResults()
        End If
    End Sub

    ''' <summary>
    ''' Invalidate old results
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub clearResults()
        position_calculated = False
        axis_calculated = False
        '
    End Sub

    ''' <summary>
    ''' Pseudo constructor to set the important data
    ''' </summary>
    ''' <param name="SatName">Name of the satellite</param>
    ''' <param name="satColor">Color of the satellite</param>
    ''' <param name="satFTrackSize">Size of the forward track of the satellite</param>
    ''' <param name="satBTrackSize">Size of the backward track of the satellite</param>
    ''' <param name="satFootprint">whether the satellite has a footprint or not.</param>
    ''' <remarks></remarks>
    Public Sub construct(ByRef SatName As String, ByRef satColor As Integer, ByRef satFTrackSize As Short, ByRef satBTrackSize As Short, ByRef satFootprint As Boolean)
        name = SatName
        color = satColor
        fTrackSize = satFTrackSize
        bTrackSize = satBTrackSize
        footprint = satFootprint

        'Temporary axis
        X.X = 1
        X.Y = 0
        X.Z = 0
        Y.X = 0
        Y.Y = 1
        Y.Z = 0
        Z.X = 0
        Z.Y = 0
        Z.Z = 1
    End Sub

    '

    ''' <summary>
    ''' Return the position of the satellite
    ''' </summary>
    ''' <param name="time">time when position should be calculated</param>
    ''' <returns>the position of the satellite</returns>
    ''' <remarks></remarks>
    Friend Function getPos(ByRef time As Double) As vector

        position_calculation(time)
        getPos = SatPos
        '
    End Function

    ''' <summary>
    ''' Return the velocity of the satellite
    ''' </summary>
    ''' <param name="time">time time when velocity should be calculated</param>
    ''' <returns>vector containing the velocity of the satellite</returns>
    ''' <remarks></remarks>
    Friend Function getVel(ByRef time As Double) As vector
        checkForChanges(time)
        If Not position_calculated Then Call position_calculation(time)
        getVel = SatVel
    End Function

    ''' <summary>
    ''' Return the new orbit of the satellite
    ''' </summary>
    ''' <param name="time">time time when new orbit should be calculated</param>
    ''' <returns>the new orbit of the satellite</returns>
    ''' <remarks></remarks>
    Friend Function getOrbit(ByRef time As Double) As orbit
        Call position_calculation(time)
        getOrbit = SatOrbit
    End Function

    ''' <summary>
    ''' Return the X vector of the satellite
    ''' </summary>
    ''' <param name="time">time when x-vector should be calculated</param>
    ''' <returns>X vector of the satellite</returns>
    ''' <remarks></remarks>
    Friend Function getX(ByRef time As Double) As vector
        checkForChanges((time))
        If Not axis_calculated Then Call axis_calculation(time)
        getX = X
        ''
    End Function
    ''' <summary>
    ''' Return the Y vector of the satellite
    ''' </summary>
    ''' <param name="time">time when Y vector should be calculated</param>
    ''' <returns>Y vector of the satellite</returns>
    ''' <remarks></remarks>
    Friend Function getY(ByRef time As Double) As vector
        checkForChanges((time))
        If Not axis_calculated Then Call axis_calculation(time)
        getY = Y
        ''
    End Function
    ''' <summary>
    ''' Return the Z vector of the satellite
    ''' </summary>
    ''' <param name="time">time time when z-vector should be calculated</param>
    ''' <returns>z-vector of the satellite</returns>
    ''' <remarks></remarks>
    Friend Function getZ(ByRef time As Double) As vector
        checkForChanges((time))
        If Not axis_calculated Then Call axis_calculation(time)
        getZ = Z
        ''
    End Function

    ''' <summary>
    ''' Return normalized vector to predefined target
    ''' </summary>
    ''' <param name="time">time when vector should be calculated</param>
    ''' <param name="target">target of the vector; must be one of the following :</param>
    ''' <param name="id"></param>
    ''' <returns>normalized vector pointing to the target</returns>
    ''' <remarks></remarks>
    Friend Function getVectTo(ByRef time As Double, ByRef target As String, Optional ByRef id As Short = 0) As vector
        Dim temp As vector
        '
        Select Case target
            Case "nadir"
                temp.X = 0
                temp.Y = 0
                temp.Z = 0  'Earth is the origin of coordinates
                getVectTo = getVectToPos(time, temp)
            Case "sun-pointing"
                getVectTo = getVectToPos(time, SunPos)
            Case "moon-pointing"
                getVectTo = getVectToPos(time, MoonPos)
            Case "velocity"
                getVectTo = vctToNorm(getVel(time))
            Case "inertial x"
                temp.X = 1
                temp.Y = 0
                temp.Z = 0
                getVectTo = temp
            Case "inertial y"
                temp.X = 0
                temp.Y = 1
                temp.Z = 0
                getVectTo = temp
            Case "inertial z"
                temp.X = 0
                temp.Y = 0
                temp.Z = 1
                getVectTo = temp
            Case "ground speed"
                getVectTo = vctToNorm(getGroundSpeed(time))
            Case Else
                Debug.Print("No available target.")
        End Select
    End Function

    ''' <summary>
    ''' Return normalized vector to given coordinate
    ''' </summary>
    ''' <param name="time">time when vector should be calculated</param>
    ''' <param name="Pos">position to calculate vector to</param>
    ''' <returns>return normalized vector pointing to Pos</returns>
    ''' <remarks></remarks>
    Friend Function getVectToPos(ByRef time As Double, ByRef Pos As vector) As vector
        getVectToPos.X = Pos.X - getPos(time).X
        getVectToPos.Y = Pos.Y - getPos(time).Y
        getVectToPos.Z = Pos.Z - getPos(time).Z
        getVectToPos = vctToNorm(getVectToPos)
    End Function

    ''' <summary>
    ''' Return normalized vector of the groundspeed of the satellite
    ''' </summary>
    ''' <param name="time">time when vector should be calculated</param>
    ''' <returns>normalized vector corresponding to groundspeed-direction</returns>
    ''' <remarks></remarks>
    Friend Function getGroundSpeed(ByRef time As Double) As vector
        Const omega_earth As Double = 360 / 86164 ' deg/s
        Dim earthVel, velDepOnEarth, vctToEarth As vector
        vctToEarth = vctSub(vct(0, 0, 0), getPos(time))
        velDepOnEarth = vctCross(getVel(time), vctToNorm(vctToEarth))
        velDepOnEarth = vctCross(vctToNorm(vctToEarth), velDepOnEarth)
        earthVel = vctCross(vctToEarth, vct(0, 0, -omega_earth * rad))
        getGroundSpeed = vctAdd(velDepOnEarth, earthVel)
    End Function

    '

    ''' <summary>
    ''' Check whether a given target is visible
    ''' </summary>
    ''' <param name="target">position of the target</param>
    ''' <param name="time">time when line of sight should be calculated</param>
    ''' <returns>True if they can see each other, False otherwise</returns>
    ''' <remarks></remarks>
    Friend Function canSatSeePos(ByRef target As vector, ByRef time As Double) As Boolean
        Dim thispos As vector
        If target.X = 0 And target.Y = 0 And target.Z = 0 Then
            canSatSeePos = True
            Exit Function
        End If
        thispos = getPos(time)
        If vctAngle(vctMultScalar(thispos, -1), vctSub(target, thispos)) > Math.Asin(RE / vctNorm(thispos)) Then
            canSatSeePos = True
        ElseIf vctAngle(vctSub(thispos, target), vctMultScalar(target, -1)) > pi / 2 Then
            canSatSeePos = True
        Else
            canSatSeePos = False
        End If

    End Function

    ''' <summary>
    ''' Calculate the position of the satellite and by the way the old and new orbit and the velocity
    ''' </summary>
    ''' <param name="time">used for calculations</param>
    ''' <remarks></remarks>
    Private Sub position_calculation(ByRef time As Double)
        update_Orbit(time, SatOrbit)
        Kepler_to_PosVel(SatOrbit, SatPos, SatVel)
        coord_gamma_geodetic(time, SatPos, SatGeoPos)
    End Sub

    ''' <summary>
    ''' Calculate the normalized X, X and Z vectors of the satellite
    ''' </summary>
    ''' <param name="time">time used for calculations</param>
    ''' <remarks></remarks>
    Private Sub axis_calculation(ByRef time As Double)
        Dim last, prim, sec, temp As vector
        '
        ' If no attitude is set
        If (primAxis = "") Or (secAxis = "") Then
            X.X = 1
            X.Y = 0
            X.Z = 0
            Y.X = 0
            Y.Y = 1
            Y.Z = 0
            Z.X = 0
            Z.Y = 0
            Z.Z = 1
            Exit Sub
        End If

        ' vector of the primary axis
        prim = getVectTo(time, primaryDir)
        ' correct vector?
        If (prim.X = 0 And prim.Y = 0 And prim.Z = 0) Then prim.X = 1
        ' vector to the target of the secondary direction
        sec = getVectTo(time, secondaryDir)
        ' correct vector?
        If (sec.X = 0 And sec.Y = 0 And sec.Z = 0) Then sec.X = 1
        ' vector (or inverse of it) of the last axis
        last = vctCross(prim, sec)
        ' vector of the secondary axis
        sec = vctCross(last, prim)

        ' set the primary vector
        If primAxis = "x" Then X = prim
        If primAxis = "y" Then Y = prim
        If primAxis = "z" Then Z = prim
        If primAxis = "-x" Then X = vctInverse(prim)
        If primAxis = "-y" Then Y = vctInverse(prim)
        If primAxis = "-z" Then Z = vctInverse(prim)
        ' set the secondary vector
        If secAxis = "x" Then X = sec
        If secAxis = "y" Then Y = sec
        If secAxis = "z" Then Z = sec
        If secAxis = "-x" Then X = vctInverse(sec)
        If secAxis = "-y" Then Y = vctInverse(sec)
        If secAxis = "-z" Then Z = vctInverse(sec)
        ' set the third vector
        If (primAxis <> "x" And primAxis <> "-x" And secAxis <> "x" And secAxis <> "-x") Then X = last
        If (primAxis <> "y" And primAxis <> "-y" And secAxis <> "y" And secAxis <> "-y") Then Y = last
        If (primAxis <> "z" And primAxis <> "-z" And secAxis <> "z" And secAxis <> "-z") Then Z = last

        temp = vctCross(X, Y)

        If vctNorm(vctAdd(Z, temp)) < vctNorm(vctAdd(sec, vctInverse(temp))) Then
            last = vctInverse(last)
            If (primAxis <> "x" And primAxis <> "-x" And secAxis <> "x" And secAxis <> "-x") Then X = last
            If (primAxis <> "y" And primAxis <> "-y" And secAxis <> "y" And secAxis <> "-y") Then Y = last
            If (primAxis <> "z" And primAxis <> "-z" And secAxis <> "z" And secAxis <> "-z") Then Z = last
        End If

        ' Norm the axes
        X = vctToNorm(X)
        Y = vctToNorm(Y)
        Z = vctToNorm(Z)
        '
        axis_calculated = True
        ''
    End Sub

#Region "Get or set properties"

    ''' <summary>
    ''' Set or get name of the satellite
    ''' </summary>
    ''' <value>Name of the satellite</value>
    ''' <returns>name of the satellite</returns>
    ''' <remarks></remarks>
    Public Property SatName() As String
        Get
            SatName = name
        End Get
        Set(ByVal Value As String)
            name = Value
            Call clearResults()
        End Set
    End Property

    ''' <summary>
    ''' Set or get color of the satellite
    ''' </summary>
    ''' <value>Color of the satellite</value>
    ''' <returns>color of the satellite</returns>
    ''' <remarks></remarks>
    Public Property satColor() As Integer
        Get
            satColor = color
        End Get
        Set(ByVal Value As Integer)
            color = Value
            Call clearResults()
        End Set
    End Property

    ''' <summary>
    ''' Set or get height of the satellite
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property satBodyHeight() As Single
        Get
            satBodyHeight = BodyHeight
        End Get
        Set(ByVal Value As Single)
            BodyHeight = Value
            Call clearResults()
        End Set
    End Property

    ''' <summary>
    ''' Set or get width of the satellite
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property satBodyWidth() As Single
        Get
            satBodyWidth = BodyWidth
        End Get
        Set(ByVal Value As Single)
            BodyWidth = Value
            Call clearResults()
        End Set
    End Property

    ''' <summary>
    ''' Set or get forward tracksize of the satellite
    ''' </summary>
    ''' <value>satFTrackSize Length of the forward tracksize</value>
    ''' <returns>forward tracksize</returns>
    ''' <remarks></remarks>
    Public Property satFTrackSize() As Short
        Get
            satFTrackSize = fTrackSize
        End Get
        Set(ByVal Value As Short)
            fTrackSize = Value
            Call clearResults()
        End Set
    End Property

    ''' <summary>
    ''' Set or get backward tracksize of the satellite
    ''' </summary>
    ''' <value>satBTrackSize Length of the backward tracksize</value>
    ''' <returns>backward tracksize of the satellite</returns>
    ''' <remarks></remarks>
    Public Property satBTrackSize() As Short
        Get
            satBTrackSize = bTrackSize
        End Get
        Set(ByVal Value As Short)
            bTrackSize = Value
            Call clearResults()
        End Set
    End Property

    ''' <summary>
    ''' Set or get whether the footprint of the satellite should be visible
    ''' </summary>
    ''' <value>satFootprint True to activate, false otherwise</value>
    ''' <returns>True if visible, false otherwise</returns>
    ''' <remarks></remarks>
    Public Property satFootprint() As Short
        Get
            satFootprint = footprint
        End Get
        Set(ByVal Value As Short)
            footprint = Value
            Call clearResults()
        End Set
    End Property

    ''' <summary>
    ''' Set or get primary direction
    ''' </summary>
    ''' <value>Primary direction</value>
    ''' <returns>primary direction</returns>
    ''' <remarks></remarks>
    Public Property primaryDir() As String
        Get
            primaryDir = primDir
        End Get
        Set(ByVal Value As String)
            primDir = Value
            Call clearResults()
        End Set
    End Property

    ''' <summary>
    ''' Set or get secondary direction
    ''' </summary>
    ''' <value>secondary direction</value>
    ''' <returns>secondary direction</returns>
    ''' <remarks></remarks>
    Public Property secondaryDir() As String
        Get
            secondaryDir = secDir
        End Get
        Set(ByVal Value As String)
            secDir = Value
            Call clearResults()
        End Set
    End Property

    ''' <summary>
    ''' Set or get primary axis
    ''' </summary>
    ''' <value>primary axis</value>
    ''' <returns>primary axis</returns>
    ''' <remarks></remarks>
    Public Property primaryAxis() As String
        Get
            primaryAxis = primAxis
        End Get
        Set(ByVal Value As String)
            primAxis = Value
            Call clearResults()
        End Set
    End Property

    ''' <summary>
    ''' Set or get secondary axis
    ''' </summary>
    ''' <value>secondary axis</value>
    ''' <returns>secondary axis</returns>
    ''' <remarks></remarks>
    Public Property secondaryAxis() As String
        Get
            secondaryAxis = secAxis
        End Get
        Set(ByVal Value As String)
            secAxis = Value
            Call clearResults()
        End Set
    End Property

    ''' <summary>
    ''' Set or get satellite for primary direction
    ''' </summary>
    ''' <value>primary satellite used for primary direction</value>
    ''' <returns>satellite used for primary direction</returns>
    ''' <remarks></remarks>
    Public Property primarySat() As CSatellite
        Get
            primarySat = primConToSat
        End Get
        Set(ByVal Value As CSatellite)
            primConToSat = Value
            Call clearResults()
        End Set
    End Property

    ''' <summary>
    ''' Set or get satellite for secondary direction
    ''' </summary>
    ''' <value>secondary direction</value>
    ''' <returns>secondary direction</returns>
    ''' <remarks></remarks>
    Public Property secondarySat() As CSatellite
        Get
            secondarySat = secConToSat
        End Get
        Set(ByVal Value As CSatellite)
            secConToSat = Value
            Call clearResults()
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Get OpenGL position
    ''' </summary>
    ''' <param name="time">time used for calculations</param>
    ''' <returns>vector determining satellite's position</returns>
    ''' <remarks></remarks>
    Friend Function getOGLPos(ByRef time As Double) As vector
        checkForChanges((time))
        If Not position_calculated Then Call position_calculation(time)
        getOGLPos.X = SatPos.Y
        getOGLPos.Y = SatPos.Z
        getOGLPos.Z = SatPos.X
    End Function

    ''' <summary>
    ''' Get OpenGL vector to other satellite
    ''' </summary>
    ''' <param name="time">time used for calculations</param>
    ''' <param name="satid">ID of the other satellite</param>
    ''' <returns>vector pointing to the other satellite</returns>
    ''' <remarks></remarks>
    Friend Function getOGLVectToSat(ByRef time As Double, ByRef satid As Short) As vector
        Dim thispos As vector
        Dim otherpos As vector
        ' gets the Position of the other satellite
        otherpos = Satellite.getOGLPos(time)
        thispos = getOGLPos(time)
        ' calculates the vector to the other satellite
        getOGLVectToSat.X = otherpos.X - thispos.X
        getOGLVectToSat.Y = otherpos.Y - thispos.Y
        getOGLVectToSat.Z = otherpos.Z - thispos.Z
        'getVectToSat = vctToNorm(getVectToSat)
    End Function

    ''' <summary>
    ''' Return normalized OpenGL vector to the other coordinate
    ''' </summary>
    ''' <param name="time">time used for calculations</param>
    ''' <param name="Pos">other coordinate</param>
    ''' <returns>vector pointing to Pos</returns>
    ''' <remarks></remarks>
    Friend Function getOGLVectToPos(ByRef time As Double, ByRef Pos As vector) As vector
        Dim thispos As vector
        thispos = getOGLPos(time)
        getOGLVectToPos.X = Pos.Y - thispos.X
        getOGLVectToPos.Y = Pos.Z - thispos.Y
        getOGLVectToPos.Z = Pos.X - thispos.Z

    End Function

    ''' <summary>
    ''' Transform coordinates from the inertial COS into coordinates in the COS of the satellite
    ''' </summary>
    ''' <param name="time">time used for calculations</param>
    ''' <param name="vector">vector Coordinates to transform</param>
    ''' <returns>transformed coordinates</returns>
    ''' <remarks></remarks>
    Friend Function getSatCOSCoords(ByRef time As Double, ByRef vector As vector) As vector

        Dim temp As vector
        '
        ' Translation
        temp = vector 'vctSub(vector, getPos(time))
        '
        ' Rotation
        getSatCOSCoords.X = getX(time).X * temp.X + getX(time).Y * temp.Y + getX(time).Z * temp.Z
        getSatCOSCoords.Y = getY(time).X * temp.X + getY(time).Y * temp.Y + getY(time).Z * temp.Z
        getSatCOSCoords.Z = getZ(time).X * temp.X + getZ(time).Y * temp.Y + getZ(time).Z * temp.Z
        '
    End Function

End Class