﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGraphAtt
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title1 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGraphAtt))
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdDraw = New System.Windows.Forms.Button()
        Me.cmdClose = New System.Windows.Forms.Button()
        Me.updwTimeStep = New System.Windows.Forms.DomainUpDown()
        Me.zlbl_2 = New System.Windows.Forms.Label()
        Me.txt_date_fin = New System.Windows.Forms.DateTimePicker()
        Me.txt_date_debut = New System.Windows.Forms.DateTimePicker()
        Me.zlbl_3 = New System.Windows.Forms.Label()
        Me.zlbl_0 = New System.Windows.Forms.Label()
        Me.cmbGeneral = New System.Windows.Forms.ComboBox()
        Me.cmbTorqueAxis = New System.Windows.Forms.ComboBox()
        Me.cmbTorqueData = New System.Windows.Forms.ComboBox()
        Me.zfra_0 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.optParPan = New System.Windows.Forms.RadioButton()
        Me.optParGen = New System.Windows.Forms.RadioButton()
        Me.lstData = New System.Windows.Forms.ListBox()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.cmdRemove = New System.Windows.Forms.Button()
        Me.zfra_1 = New System.Windows.Forms.GroupBox()
        Me.Graph = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.chkAxis_1 = New System.Windows.Forms.CheckBox()
        Me.chkAxis_2 = New System.Windows.Forms.CheckBox()
        Me.chkAxis_3 = New System.Windows.Forms.CheckBox()
        Me.chkAxis_4 = New System.Windows.Forms.CheckBox()
        Me.chkAxis_5 = New System.Windows.Forms.CheckBox()
        Me.chkAxis_6 = New System.Windows.Forms.CheckBox()
        Me.chkAxis_7 = New System.Windows.Forms.CheckBox()
        Me.chkAxis_8 = New System.Windows.Forms.CheckBox()
        Me.zlbl_4 = New System.Windows.Forms.Label()
        Me.dlgGraphSave = New System.Windows.Forms.SaveFileDialog()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.zfra_0.SuspendLayout()
        Me.zfra_1.SuspendLayout()
        CType(Me.Graph, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave.Location = New System.Drawing.Point(854, 19)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(133, 64)
        Me.cmdSave.TabIndex = 43
        Me.cmdSave.Text = "Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdDraw
        '
        Me.cmdDraw.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDraw.Location = New System.Drawing.Point(970, 156)
        Me.cmdDraw.Name = "cmdDraw"
        Me.cmdDraw.Size = New System.Drawing.Size(157, 71)
        Me.cmdDraw.TabIndex = 44
        Me.cmdDraw.Text = "Draw"
        Me.cmdDraw.UseVisualStyleBackColor = True
        '
        'cmdClose
        '
        Me.cmdClose.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdClose.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClose.Location = New System.Drawing.Point(993, 19)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(134, 64)
        Me.cmdClose.TabIndex = 42
        Me.cmdClose.Text = "Close"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'updwTimeStep
        '
        Me.updwTimeStep.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.updwTimeStep.Items.Add("3600 s")
        Me.updwTimeStep.Items.Add("1800 s")
        Me.updwTimeStep.Items.Add("600 s")
        Me.updwTimeStep.Items.Add("300 s")
        Me.updwTimeStep.Items.Add("180 s")
        Me.updwTimeStep.Items.Add("60 s")
        Me.updwTimeStep.Items.Add("10 s")
        Me.updwTimeStep.Items.Add("5 s")
        Me.updwTimeStep.Items.Add("1 s")
        Me.updwTimeStep.Location = New System.Drawing.Point(499, 30)
        Me.updwTimeStep.Name = "updwTimeStep"
        Me.updwTimeStep.ReadOnly = True
        Me.updwTimeStep.Size = New System.Drawing.Size(74, 23)
        Me.updwTimeStep.TabIndex = 41
        Me.updwTimeStep.Text = "60 s"
        '
        'zlbl_2
        '
        Me.zlbl_2.AutoSize = True
        Me.zlbl_2.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_2.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_2.Location = New System.Drawing.Point(232, 33)
        Me.zlbl_2.Name = "zlbl_2"
        Me.zlbl_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_2.Size = New System.Drawing.Size(22, 16)
        Me.zlbl_2.TabIndex = 38
        Me.zlbl_2.Text = "to"
        Me.zlbl_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txt_date_fin
        '
        Me.txt_date_fin.CalendarFont = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_date_fin.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        Me.txt_date_fin.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_date_fin.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txt_date_fin.Location = New System.Drawing.Point(260, 30)
        Me.txt_date_fin.Name = "txt_date_fin"
        Me.txt_date_fin.Size = New System.Drawing.Size(173, 23)
        Me.txt_date_fin.TabIndex = 40
        '
        'txt_date_debut
        '
        Me.txt_date_debut.CalendarFont = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_date_debut.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        Me.txt_date_debut.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_date_debut.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txt_date_debut.Location = New System.Drawing.Point(57, 30)
        Me.txt_date_debut.Name = "txt_date_debut"
        Me.txt_date_debut.Size = New System.Drawing.Size(169, 23)
        Me.txt_date_debut.TabIndex = 39
        '
        'zlbl_3
        '
        Me.zlbl_3.AutoSize = True
        Me.zlbl_3.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_3.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_3.Location = New System.Drawing.Point(454, 31)
        Me.zlbl_3.Name = "zlbl_3"
        Me.zlbl_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_3.Size = New System.Drawing.Size(39, 16)
        Me.zlbl_3.TabIndex = 36
        Me.zlbl_3.Text = "Step"
        Me.zlbl_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_0
        '
        Me.zlbl_0.AutoSize = True
        Me.zlbl_0.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_0.Location = New System.Drawing.Point(12, 33)
        Me.zlbl_0.Name = "zlbl_0"
        Me.zlbl_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_0.Size = New System.Drawing.Size(40, 16)
        Me.zlbl_0.TabIndex = 37
        Me.zlbl_0.Text = "From"
        Me.zlbl_0.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmbGeneral
        '
        Me.cmbGeneral.Enabled = False
        Me.cmbGeneral.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbGeneral.FormattingEnabled = True
        Me.cmbGeneral.Location = New System.Drawing.Point(181, 32)
        Me.cmbGeneral.Name = "cmbGeneral"
        Me.cmbGeneral.Size = New System.Drawing.Size(144, 24)
        Me.cmbGeneral.TabIndex = 45
        '
        'cmbTorqueAxis
        '
        Me.cmbTorqueAxis.AccessibleDescription = ""
        Me.cmbTorqueAxis.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTorqueAxis.FormattingEnabled = True
        Me.cmbTorqueAxis.Location = New System.Drawing.Point(412, 69)
        Me.cmbTorqueAxis.Name = "cmbTorqueAxis"
        Me.cmbTorqueAxis.Size = New System.Drawing.Size(70, 24)
        Me.cmbTorqueAxis.TabIndex = 45
        '
        'cmbTorqueData
        '
        Me.cmbTorqueData.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTorqueData.FormattingEnabled = True
        Me.cmbTorqueData.Location = New System.Drawing.Point(181, 69)
        Me.cmbTorqueData.Name = "cmbTorqueData"
        Me.cmbTorqueData.Size = New System.Drawing.Size(144, 24)
        Me.cmbTorqueData.TabIndex = 45
        '
        'zfra_0
        '
        Me.zfra_0.Controls.Add(Me.Label1)
        Me.zfra_0.Controls.Add(Me.optParPan)
        Me.zfra_0.Controls.Add(Me.cmbGeneral)
        Me.zfra_0.Controls.Add(Me.optParGen)
        Me.zfra_0.Controls.Add(Me.cmbTorqueData)
        Me.zfra_0.Controls.Add(Me.cmbTorqueAxis)
        Me.zfra_0.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_0.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_0.Location = New System.Drawing.Point(24, 99)
        Me.zfra_0.Name = "zfra_0"
        Me.zfra_0.Size = New System.Drawing.Size(510, 110)
        Me.zfra_0.TabIndex = 46
        Me.zfra_0.TabStop = False
        Me.zfra_0.Text = "Select parameters"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(352, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 16)
        Me.Label1.TabIndex = 51
        Me.Label1.Text = "on axis"
        '
        'optParPan
        '
        Me.optParPan.AutoSize = True
        Me.optParPan.Checked = True
        Me.optParPan.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optParPan.ForeColor = System.Drawing.Color.Black
        Me.optParPan.Location = New System.Drawing.Point(20, 70)
        Me.optParPan.Name = "optParPan"
        Me.optParPan.Size = New System.Drawing.Size(79, 20)
        Me.optParPan.TabIndex = 50
        Me.optParPan.TabStop = True
        Me.optParPan.Text = "Torques"
        Me.optParPan.UseVisualStyleBackColor = True
        '
        'optParGen
        '
        Me.optParGen.AutoSize = True
        Me.optParGen.Enabled = False
        Me.optParGen.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optParGen.ForeColor = System.Drawing.Color.Black
        Me.optParGen.Location = New System.Drawing.Point(20, 33)
        Me.optParGen.Name = "optParGen"
        Me.optParGen.Size = New System.Drawing.Size(154, 20)
        Me.optParGen.TabIndex = 50
        Me.optParGen.Text = "General parameters"
        Me.optParGen.UseVisualStyleBackColor = True
        '
        'lstData
        '
        Me.lstData.BackColor = System.Drawing.SystemColors.Window
        Me.lstData.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstData.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstData.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstData.ItemHeight = 16
        Me.lstData.Location = New System.Drawing.Point(695, 99)
        Me.lstData.Name = "lstData"
        Me.lstData.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstData.Size = New System.Drawing.Size(204, 132)
        Me.lstData.TabIndex = 49
        '
        'cmdAdd
        '
        Me.cmdAdd.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAdd.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAdd.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAdd.Location = New System.Drawing.Point(550, 125)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAdd.Size = New System.Drawing.Size(129, 29)
        Me.cmdAdd.TabIndex = 48
        Me.cmdAdd.Text = "Add to graph >>"
        Me.cmdAdd.UseVisualStyleBackColor = False
        '
        'cmdRemove
        '
        Me.cmdRemove.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemove.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRemove.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdRemove.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRemove.Location = New System.Drawing.Point(550, 164)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRemove.Size = New System.Drawing.Size(129, 31)
        Me.cmdRemove.TabIndex = 47
        Me.cmdRemove.Text = "<< Remove"
        Me.cmdRemove.UseVisualStyleBackColor = False
        '
        'zfra_1
        '
        Me.zfra_1.Controls.Add(Me.updwTimeStep)
        Me.zfra_1.Controls.Add(Me.zlbl_0)
        Me.zfra_1.Controls.Add(Me.zlbl_3)
        Me.zfra_1.Controls.Add(Me.txt_date_debut)
        Me.zfra_1.Controls.Add(Me.txt_date_fin)
        Me.zfra_1.Controls.Add(Me.zlbl_2)
        Me.zfra_1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_1.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_1.Location = New System.Drawing.Point(24, 19)
        Me.zfra_1.Name = "zfra_1"
        Me.zfra_1.Size = New System.Drawing.Size(590, 74)
        Me.zfra_1.TabIndex = 46
        Me.zfra_1.TabStop = False
        Me.zfra_1.Text = "Choose period"
        '
        'Graph
        '
        Me.Graph.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Graph.BorderlineColor = System.Drawing.Color.DarkCyan
        Me.Graph.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.Graph.BorderlineWidth = 2
        ChartArea1.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount
        ChartArea1.AxisX.IsLabelAutoFit = False
        ChartArea1.AxisX.LabelStyle.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea1.AxisX.Minimum = 0.0R
        ChartArea1.AxisY.IsStartedFromZero = False
        ChartArea1.AxisY.MinorGrid.LineColor = System.Drawing.Color.Red
        ChartArea1.AxisY2.IsStartedFromZero = False
        ChartArea1.BackColor = System.Drawing.Color.Transparent
        ChartArea1.Name = "ChartArea1"
        Me.Graph.ChartAreas.Add(ChartArea1)
        Legend1.BackColor = System.Drawing.Color.White
        Legend1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Legend1.IsTextAutoFit = False
        Legend1.Name = "Legend1"
        Me.Graph.Legends.Add(Legend1)
        Me.Graph.Location = New System.Drawing.Point(24, 247)
        Me.Graph.Margin = New System.Windows.Forms.Padding(2)
        Me.Graph.Name = "Graph"
        Me.Graph.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None
        Me.Graph.PaletteCustomColors = New System.Drawing.Color() {System.Drawing.Color.Blue, System.Drawing.Color.Red, System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer)), System.Drawing.Color.Gray, System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer)), System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer)), System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))}
        Series1.ChartArea = "ChartArea1"
        Series1.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Me.Graph.Series.Add(Series1)
        Me.Graph.Size = New System.Drawing.Size(1103, 438)
        Me.Graph.TabIndex = 90
        Title1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Title1.Name = "Title1"
        Me.Graph.Titles.Add(Title1)
        '
        'chkAxis_1
        '
        Me.chkAxis_1.AutoSize = True
        Me.chkAxis_1.Location = New System.Drawing.Point(906, 99)
        Me.chkAxis_1.Name = "chkAxis_1"
        Me.chkAxis_1.Size = New System.Drawing.Size(15, 14)
        Me.chkAxis_1.TabIndex = 91
        Me.chkAxis_1.UseVisualStyleBackColor = True
        '
        'chkAxis_2
        '
        Me.chkAxis_2.AutoSize = True
        Me.chkAxis_2.Location = New System.Drawing.Point(906, 116)
        Me.chkAxis_2.Name = "chkAxis_2"
        Me.chkAxis_2.Size = New System.Drawing.Size(15, 14)
        Me.chkAxis_2.TabIndex = 91
        Me.chkAxis_2.UseVisualStyleBackColor = True
        '
        'chkAxis_3
        '
        Me.chkAxis_3.AutoSize = True
        Me.chkAxis_3.Location = New System.Drawing.Point(906, 133)
        Me.chkAxis_3.Name = "chkAxis_3"
        Me.chkAxis_3.Size = New System.Drawing.Size(15, 14)
        Me.chkAxis_3.TabIndex = 91
        Me.chkAxis_3.UseVisualStyleBackColor = True
        '
        'chkAxis_4
        '
        Me.chkAxis_4.AutoSize = True
        Me.chkAxis_4.Location = New System.Drawing.Point(906, 150)
        Me.chkAxis_4.Name = "chkAxis_4"
        Me.chkAxis_4.Size = New System.Drawing.Size(15, 14)
        Me.chkAxis_4.TabIndex = 91
        Me.chkAxis_4.UseVisualStyleBackColor = True
        '
        'chkAxis_5
        '
        Me.chkAxis_5.AutoSize = True
        Me.chkAxis_5.Location = New System.Drawing.Point(906, 167)
        Me.chkAxis_5.Name = "chkAxis_5"
        Me.chkAxis_5.Size = New System.Drawing.Size(15, 14)
        Me.chkAxis_5.TabIndex = 91
        Me.chkAxis_5.UseVisualStyleBackColor = True
        '
        'chkAxis_6
        '
        Me.chkAxis_6.AutoSize = True
        Me.chkAxis_6.Location = New System.Drawing.Point(906, 184)
        Me.chkAxis_6.Name = "chkAxis_6"
        Me.chkAxis_6.Size = New System.Drawing.Size(15, 14)
        Me.chkAxis_6.TabIndex = 91
        Me.chkAxis_6.UseVisualStyleBackColor = True
        '
        'chkAxis_7
        '
        Me.chkAxis_7.AutoSize = True
        Me.chkAxis_7.Location = New System.Drawing.Point(906, 201)
        Me.chkAxis_7.Name = "chkAxis_7"
        Me.chkAxis_7.Size = New System.Drawing.Size(15, 14)
        Me.chkAxis_7.TabIndex = 91
        Me.chkAxis_7.UseVisualStyleBackColor = True
        '
        'chkAxis_8
        '
        Me.chkAxis_8.AutoSize = True
        Me.chkAxis_8.Location = New System.Drawing.Point(906, 218)
        Me.chkAxis_8.Name = "chkAxis_8"
        Me.chkAxis_8.Size = New System.Drawing.Size(15, 14)
        Me.chkAxis_8.TabIndex = 91
        Me.chkAxis_8.UseVisualStyleBackColor = True
        '
        'zlbl_4
        '
        Me.zlbl_4.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_4.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_4.Location = New System.Drawing.Point(927, 95)
        Me.zlbl_4.Name = "zlbl_4"
        Me.zlbl_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_4.Size = New System.Drawing.Size(205, 20)
        Me.zlbl_4.TabIndex = 92
        Me.zlbl_4.Text = "If checked, secondary axis is used"
        Me.zlbl_4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dlgGraphSave
        '
        Me.dlgGraphSave.Filter = "(*.bmp)|*.bmp"
        Me.dlgGraphSave.Title = "Save report as ..."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Location = New System.Drawing.Point(1069, 208)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 93
        Me.Label2.Text = "Ctrl+Enter"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Location = New System.Drawing.Point(949, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 13)
        Me.Label3.TabIndex = 94
        Me.Label3.Text = "Ctrl+S"
        '
        'frmGraphAtt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1149, 705)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.zlbl_4)
        Me.Controls.Add(Me.chkAxis_8)
        Me.Controls.Add(Me.chkAxis_7)
        Me.Controls.Add(Me.chkAxis_6)
        Me.Controls.Add(Me.chkAxis_5)
        Me.Controls.Add(Me.chkAxis_4)
        Me.Controls.Add(Me.chkAxis_3)
        Me.Controls.Add(Me.chkAxis_2)
        Me.Controls.Add(Me.chkAxis_1)
        Me.Controls.Add(Me.Graph)
        Me.Controls.Add(Me.lstData)
        Me.Controls.Add(Me.cmdAdd)
        Me.Controls.Add(Me.cmdRemove)
        Me.Controls.Add(Me.zfra_1)
        Me.Controls.Add(Me.zfra_0)
        Me.Controls.Add(Me.cmdDraw)
        Me.Controls.Add(Me.cmdClose)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmGraphAtt"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Curves : Attitude Control"
        Me.zfra_0.ResumeLayout(False)
        Me.zfra_0.PerformLayout()
        Me.zfra_1.ResumeLayout(False)
        Me.zfra_1.PerformLayout()
        CType(Me.Graph, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdDraw As System.Windows.Forms.Button
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents updwTimeStep As System.Windows.Forms.DomainUpDown
    Public WithEvents zlbl_2 As System.Windows.Forms.Label
    Friend WithEvents txt_date_fin As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_date_debut As System.Windows.Forms.DateTimePicker
    Public WithEvents zlbl_3 As System.Windows.Forms.Label
    Public WithEvents zlbl_0 As System.Windows.Forms.Label
    Friend WithEvents cmbGeneral As System.Windows.Forms.ComboBox
    Friend WithEvents cmbTorqueAxis As System.Windows.Forms.ComboBox
    Friend WithEvents cmbTorqueData As System.Windows.Forms.ComboBox
    Friend WithEvents zfra_0 As System.Windows.Forms.GroupBox
    Public WithEvents lstData As System.Windows.Forms.ListBox
    Public WithEvents cmdAdd As System.Windows.Forms.Button
    Public WithEvents cmdRemove As System.Windows.Forms.Button
    Friend WithEvents zfra_1 As System.Windows.Forms.GroupBox
    Friend WithEvents optParGen As System.Windows.Forms.RadioButton
    Friend WithEvents optParPan As System.Windows.Forms.RadioButton
    Friend WithEvents Graph As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents chkAxis_1 As System.Windows.Forms.CheckBox
    Friend WithEvents chkAxis_2 As System.Windows.Forms.CheckBox
    Friend WithEvents chkAxis_3 As System.Windows.Forms.CheckBox
    Friend WithEvents chkAxis_4 As System.Windows.Forms.CheckBox
    Friend WithEvents chkAxis_5 As System.Windows.Forms.CheckBox
    Friend WithEvents chkAxis_6 As System.Windows.Forms.CheckBox
    Friend WithEvents chkAxis_7 As System.Windows.Forms.CheckBox
    Friend WithEvents chkAxis_8 As System.Windows.Forms.CheckBox
    Public WithEvents zlbl_4 As System.Windows.Forms.Label
    Public WithEvents dlgGraphSave As System.Windows.Forms.SaveFileDialog
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
