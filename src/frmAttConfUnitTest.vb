﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting



<TestClass()> Public Class frmAttConfUnitTest

    Dim epsilon As Double = 0.01


    ''' <summary>
    ''' To run before every test, data taken from BE
    ''' </summary>
    ''' <remarks></remarks>
    <TestInitialize()> Public Sub satelliteInitialization()
        '
        ' Creating satellite
        Satellite = New CSatellite
        Call Satellite.construct("New_Sat", 65535, 0, 0, True)
        With Satellite
            .satBodyHeight = 0.8
            .satBodyWidth = 0.8
            .primaryAxis = "-z"
            .primaryDir = "nadir"
            .secondaryAxis = "x"
            .secondaryDir = "velocity"
        End With
        '
        ' Creating orbit
        With SatOrbit0
            .Name = "Sun synchronous 800"
            .SemiMajor_Axis = 7278.137
            .Eccentricity = 0
            .Inclination = 98.602
            .Argument_Perigee = 0
            .RAAN = 0
            .Mean_Anomaly = 0
            .epoch = 2451544.5          ' January 1st 2000 at 0h
            .dec1 = 0
            .dec2 = 0
            .bstar = 0
        End With
        SatOrbit = SatOrbit0
        '
        ' Creating new collection of solar panels
        SolarPanels = New Collection
        ''
    End Sub


#Region "Useful functions"

    ''' <summary>
    ''' initialization of sat values
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub initialize_sat_values()

        BodyMass = 10
        BodyCG = vct(0, 0, 0)
        GravityMass = 2
        GravityDist = vct(5, 0, 0)

    End Sub


    ''' <summary>
    ''' initialization of sat values as well as inertia matrix
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub initialize_sat_values_inertia()

        BodyMass = 10
        BodyCG = vct(0, 0, 0)
        GravityMass = 2
        GravityDist = vct(5, 0, 0)
        BodyInertia = {{15, 1, 1}, {1, 15, 1}, {1, 1, 15}}

    End Sub


    ''' <summary>
    ''' initialization of sat values as well as inertia matrix - second version
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub initialize_sat_values_inertia2()

        BodyMass = 10
        BodyCG = vct(0, 0, 0)
        GravityMass = 2
        GravityDist = vct(0, 0, 7)
        BodyInertia = {{15, 1, 1}, {1, 15, 1}, {1, 1, 15}}

    End Sub


    ''' <summary>
    ''' Add an orientable panel (Type 1)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y" or "- Y"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type1(ByRef length As Single, ByRef width As Single, ByRef mass As Single, ByRef face As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type1"        ' Orientable Panel
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub

    ''' <summary>
    ''' Add a panel on a face (Type 2)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y","- Y", "+ Z" or "- Z"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type2(ByRef length As Single, ByRef width As Single, ByRef mass As Single, ByRef face As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type2"        ' Panel on a face
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub

    ''' <summary>
    ''' Add a panel fixed perpendicular to a face (Type 3)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y" or "- Y"</param>
    ''' <param name="dir">Direction : "+ Z" or "- Z"</param>
    ''' <param name="hpos">Position : "Upper", "Middle" or "Lower"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type3(ByRef length As Single, ByRef width As Single, ByRef mass As Single, _
                                ByRef face As String, ByRef dir As String, ByRef hpos As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type3"        ' Panel fixed perpendicular to a face
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelDir = dir
            .PanelHpos = hpos
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & " : " & .PanelDir & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub


    ''' <summary>
    ''' it adds a type 1 panel; its dimensions can vary according to the different cases
    ''' minimum --> each dimension is set to its minimum
    ''' maximum --> each dimension is set to its maximum
    ''' randomUser --> each dimension is set to a random value written by the author
    ''' randomMachine --> each dimension is set to a random value obtained by using vbnet's random function. Therefore each time the values will differ from the previous ones
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub add_panel1(dimensions As String)

        Select Case dimensions
            Case "minimum"
                add_panel_type1(0.1, 0.1, 0.1, "- X")
            Case "maximum"
                add_panel_type1(20, 20, 50, "- X")
            Case "randomUser"
                add_panel_type1(5, 18, 21, "- X")
            Case "randomMachine"
                add_panel_type1(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(1))
        End Select


    End Sub

    ''' <summary>
    ''' it adds a type 2 panel; its dimensions can vary according to the different cases
    ''' minimum --> each dimension is set to its minimum
    ''' maximum --> each dimension is set to its maximum
    ''' randomUser --> each dimension is set to a random value written by the author
    ''' randomMachine --> each dimension is set to a random value obtained by using vbnet's random function. Therefore each time the values will differ from the previous ones
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub add_panel2(dimensions As String)


        Select Case dimensions
            Case "minimum"
                add_panel_type2(0.1, 0.1, 0.1, "- Y")
            Case "maximum"
                add_panel_type2(20, 20, 50, "- Y")
            Case "randomUser"
                add_panel_type2(6, 13, 12, "- Y")
            Case "randomMachine"
                add_panel_type2(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(2))
        End Select


    End Sub

    ''' <summary>
    ''' it adds a type 3 panel; its dimensions can vary according to the different cases
    ''' minimum --> each dimension is set to its minimum
    ''' maximum --> each dimension is set to its maximum
    ''' randomUser --> each dimension is set to a random value written by the author
    ''' randomMachine --> each dimension is set to a random value obtained by using vbnet's random function. Therefore each time the values will differ from the previous ones
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub add_panel3(dimensions As String)


        Select Case dimensions
            Case "minimum"
                add_panel_type3(0.1, 0.1, 0.1, "+ X", "+ Z", "Lower")
            Case "maximum"
                add_panel_type3(20, 20, 50, "+ X", "+ Z", "Lower")
            Case "randomUser"
                add_panel_type3(7, 16, 39, "+ X", "+ Z", "Lower")
            Case "randomMachine"
                add_panel_type3(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(3), select_random_orientation(), select_random_position())
        End Select


    End Sub


    ''' <summary>
    ''' it randomly chooses a face
    ''' </summary>
    ''' <param name="type"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function select_random_face(ByRef type As Integer) As String

        Select Case type
            Case 1
                Dim faces1() As String = {"+ X", "+ Y", "- X", "- Y"}
                Return faces1(Int(4 * Rnd()) + 0)
            Case 2
                Dim faces2() As String = {"+ X", "+ Y", "+ Z", "- X", "- Y", "- Z"}
                Return faces2(Int(6 * Rnd()) + 0)
            Case 3
                Dim faces3() As String = {"+ X", "+ Y", "- X", "- Y"}
                Return faces3(Int(4 * Rnd()) + 0)
        End Select

        Return "faces" & CStr(type)(Int(4 * Rnd()) + 0)

    End Function

    ''' <summary>
    ''' it randomly chooses an orientation
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function select_random_orientation() As String

        Dim orientation() As String = {"+ Z", "- Z"}
        Return orientation(Int(2 * Rnd()) + 0)

    End Function


    ''' <summary>
    ''' it randomly chooses a position for type 3 panels
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function select_random_position() As String

        Dim orientation() As String = {"Upper", "Middle", "Lower"}
        Return orientation(Int(3 * Rnd()) + 0)

    End Function


#End Region


#Region "Global Mass"

    ''' <summary>
    ''' Test the calculation of the new mass of the whole satellite
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()> Public Sub sat_global_mass_test()

        ' SETTINGS
        initialize_sat_values()
        Dim numberOfTests As Integer = 4

        For j As Integer = 1 To numberOfTests


            Select Case j
                Case 1
                    add_panel_type2(1, 1, 5, "+ Z")
                    add_panel_type3(1, 1, 0.5, "- Y", "+ Z", "Lower")
                Case 2
                    add_panel1("minimum")
                    add_panel2("minimum")
                    add_panel3("minimum")
                Case 3
                    add_panel1("maximum")
                    add_panel2("maximum")
                    add_panel3("maximum")
                Case 4
                    add_panel1("randomUser")
                    add_panel2("randomUser")
                    add_panel3("randomUser")
            End Select


            ' ARRANGE
            Dim PanelMass As Double
            Dim i As Integer
            Dim mass_expected As Double = BodyMass + GravityMass + DampersMass(0) + DampersMass(1) + DampersMass(2)
            For i = 1 To PanelsNumber
                With SolarPanels.Item(i)
                    PanelMass = CDbl(.PanelMass)
                End With
                mass_expected += PanelMass
            Next

            ' ACT
            frmAttConf.update_attitude_data_fortest()

            ' ASSERT
            Assert.AreEqual(mass_expected, SatMass, epsilon)

        Next


    End Sub


    ''' <summary>
    ''' Test the calculation of the new mass of the whole satellite using machine random values. to test robusteness of the software it ised a loop of 50 iterations
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()> Public Sub sat_global_mass_random_test()

        ' SETTINGS
        initialize_sat_values()

        For j As Integer = 1 To 50

            add_panel1("randomMachine")
            add_panel2("randomMachine")
            add_panel3("randomMachine")

            ' ARRANGE
            Dim PanelMass As Double
            Dim i As Integer
            Dim mass_expected As Double = BodyMass + GravityMass + DampersMass(0) + DampersMass(1) + DampersMass(2)
            For i = 1 To PanelsNumber
                With SolarPanels.Item(i)
                    PanelMass = CDbl(.PanelMass)
                End With
                mass_expected += PanelMass
            Next

            ' ACT
            frmAttConf.update_attitude_data_fortest()

            ' ASSERT
            Assert.AreEqual(mass_expected, SatMass, epsilon)

        Next

    End Sub

#End Region


#Region "Global Center of gravity"



    ''' <summary>
    ''' center of gravity unit test, BE data considering type 1 panels
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()> Public Sub sat_global_CG_type1_test()

        ' SETTINGS

        initialize_sat_values()

        Dim CG_exp As vector


        ' ARRANGE
        add_panel_type1(0.8, 0.8, 1, "+ X")
        add_panel_type1(0.8, 0.8, 1, "- X")
        add_panel_type1(0.8, 0.8, 1, "- Y")
        add_panel_type1(0.8, 0.8, 1, "+ Y")
        CG_exp = vct(0.625, 0, 0)
        ' ACT
        frmAttConf.update_attitude_data_fortest()
        ' ASSERT
        Assert.AreEqual(CG_exp.X, SatCG.X, epsilon)
        Assert.AreEqual(CG_exp.Y, SatCG.Y, epsilon)
        Assert.AreEqual(CG_exp.Z, SatCG.Z, epsilon)


    End Sub


    ''' <summary>
    ''' center of gravity unit test, BE data considering type 2 panels
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()> Public Sub sat_global_CG_type2_test()

        ' SETTINGS

        initialize_sat_values()

        Dim CG_exp As vector

        ' ARRANGE
        add_panel_type2(0.8, 0.8, 1, "+ X")
        add_panel_type2(0.8, 0.8, 1, "- X")
        add_panel_type2(0.8, 0.8, 1, "- Y")
        add_panel_type2(0.8, 0.8, 1, "+ Y")

        CG_exp = vct(0.625, 0, 0)

        ' ACT
        frmAttConf.update_attitude_data_fortest()

        ' ASSERT
        Assert.AreEqual(CG_exp.X, SatCG.X, epsilon)
        Assert.AreEqual(CG_exp.Y, SatCG.Y, epsilon)
        Assert.AreEqual(CG_exp.Z, SatCG.Z, epsilon)

    End Sub

    ''' <summary>
    ''' center of gravity unit test, BE data considering type 3 panels with different positions
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()> Public Sub sat_global_CG_type3_test()

        ' SETTINGS

        initialize_sat_values()

        Dim CG_exp As vector

        ' ARRANGE
        add_panel_type3(0.8, 0.8, 1, "+ X", "+ Z", "Lower")
        add_panel_type3(0.8, 0.8, 1, "- X", "+ Z", "Upper")
        add_panel_type3(0.8, 0.8, 1, "+ Y", "- Z", "Middle")
        add_panel_type3(0.8, 0.8, 1, "- Y", "+ Z", "Lower")
        CG_exp = vct(0.625, 0, -0.025)
        ' ACT
        frmAttConf.update_attitude_data_fortest()
        ' ASSERT
        Assert.AreEqual(CG_exp.X, SatCG.X, epsilon)
        Assert.AreEqual(CG_exp.Y, SatCG.Y, epsilon)
        Assert.AreEqual(CG_exp.Z, SatCG.Z, epsilon)

    End Sub


    ''' <summary>
    ''' center of gravity unit test, different types of panels with different positions
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()> Public Sub sat_global_CG_mix_test()

        ' SETTINGS

        initialize_sat_values()

        Dim CG_exp As vector



        ' ARRANGE
        add_panel_type1(0.8, 0.8, 1, "+ X")
        add_panel_type1(0.8, 0.8, 1, "- X")
        add_panel_type2(0.8, 0.8, 1, "+ Z")
        add_panel_type3(0.8, 0.8, 1, "+ Y", "+ Z", "Upper")
        add_panel_type3(0.8, 0.8, 1, "- Y", "+ Z", "Middle")
        CG_exp = vct(0.588, 0, 0.047)
        ' ACT
        frmAttConf.update_attitude_data_fortest()
        ' ASSERT
        Assert.AreEqual(CG_exp.X, SatCG.X, epsilon)
        Assert.AreEqual(CG_exp.Y, SatCG.Y, epsilon)
        Assert.AreEqual(CG_exp.Z, SatCG.Z, epsilon)


    End Sub

    ''' <summary>
    ''' center of gravity unit test, different types of panels and minimum values
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()> Public Sub sat_global_CG_minimum_test()

        ' SETTINGS

        initialize_sat_values()

        Dim CG_exp As vector


        ' ARRANGE
        add_panel_type1(0.1, 0.1, 0.1, "+ X")
        add_panel_type2(0.1, 0.1, 0.1, "+ Z")
        add_panel_type3(0.1, 0.1, 0.1, "+ Y", "+ Z", "Upper")
        CG_exp = vct(0.816, 0.003, 0.006)
        ' ACT
        frmAttConf.update_attitude_data_fortest()
        ' ASSERT
        Assert.AreEqual(CG_exp.X, SatCG.X, epsilon)
        Assert.AreEqual(CG_exp.Y, SatCG.Y, epsilon)
        Assert.AreEqual(CG_exp.Z, SatCG.Z, epsilon)


    End Sub

#End Region


#Region "Global Inertia"

    '' In this section some functions of the code will be used to calculate values whose correctenss has already been tested 
    '' perviously: such as the calcuation of satCG (by calling frmAttConf.update_attitude_data_fortest()), of PanelCG by 
    '' creating a function that uses the already existing and tested code, Huygens function and others...


    <TestMethod()> Public Sub sat_global_Inertia_type1_test()

        

        ' ARRANGE
        initialize_sat_values_inertia()

        Dim SatInertia_expected As Double(,) = {{16.4933, 1, 1}, {1, 60.243, 1}, {1, 1, 61.736}}

        add_panel_type1(0.8, 0.8, 1, "+ X")
        add_panel_type1(0.8, 0.8, 1, "- X")
        add_panel_type1(0.8, 0.8, 1, "- Y")
        add_panel_type1(0.8, 0.8, 1, "+ Y")


        ' ACT
        frmAttConf.update_attitude_data_fortest()

        ' ASSERT

        'CollectionAssert.AreEqual(SatInertia_expected, SatInertia)
        Assert.AreEqual(SatInertia_expected(0, 0), SatInertia(0, 0), epsilon)
        Assert.AreEqual(SatInertia_expected(0, 1), SatInertia(0, 1), epsilon)
        Assert.AreEqual(SatInertia_expected(0, 2), SatInertia(0, 2), epsilon)
        Assert.AreEqual(SatInertia_expected(1, 1), SatInertia(1, 1), epsilon)
        Assert.AreEqual(SatInertia_expected(1, 2), SatInertia(1, 2), epsilon)
        Assert.AreEqual(SatInertia_expected(2, 2), SatInertia(2, 2), epsilon)


    End Sub


    <TestMethod()> Public Sub sat_global_Inertia_type2_test()

        ' ARRANGE

        initialize_sat_values_inertia()

        Dim SatInertia_expected As Double(,) = {{15.64, 1, 1}, {1, 59.39, 1}, {1, 1, 59.603}}

        add_panel_type2(0.8, 0.8, 1, "+ X")
        add_panel_type2(0.8, 0.8, 1, "+ Y")
        add_panel_type2(0.8, 0.8, 1, "- X")
        add_panel_type2(0.8, 0.8, 1, "- Y")

        ' ACT
        frmAttConf.update_attitude_data_fortest()


        ' ASSERT

        'CollectionAssert.AreEqual(SatInertia_expected, SatInertia)
        Assert.AreEqual(SatInertia_expected(0, 0), SatInertia(0, 0), epsilon)
        Assert.AreEqual(SatInertia_expected(0, 1), SatInertia(0, 1), epsilon)
        Assert.AreEqual(SatInertia_expected(0, 2), SatInertia(0, 2), epsilon)
        Assert.AreEqual(SatInertia_expected(1, 1), SatInertia(1, 1), epsilon)
        Assert.AreEqual(SatInertia_expected(1, 2), SatInertia(1, 2), epsilon)
        Assert.AreEqual(SatInertia_expected(2, 2), SatInertia(2, 2), epsilon)


    End Sub


    <TestMethod()> Public Sub sat_global_Inertia_type3_test()

        ' ARRANGE

        initialize_sat_values_inertia()

        Dim SatInertia_expected As Double(,) = {{16.9633, 1, 0.43}, {1, 60.7133, 1}, {0.43, 1, 61.7366}}

        add_panel_type3(0.8, 0.8, 1, "+ X", "+ Z", "Upper")
        add_panel_type3(0.8, 0.8, 1, "- X", "+ Z", "Middle")
        add_panel_type3(0.8, 0.8, 1, "+ Y", "+ Z", "Lower")
        add_panel_type3(0.8, 0.8, 1, "- Y", "+ Z", "Lower")
 
        ' ACT
        frmAttConf.update_attitude_data_fortest()


        ' ASSERT

        'CollectionAssert.AreEqual(SatInertia_expected, SatInertia)
        Assert.AreEqual(SatInertia_expected(0, 0), SatInertia(0, 0), epsilon)
        Assert.AreEqual(SatInertia_expected(0, 1), SatInertia(0, 1), epsilon)
        Assert.AreEqual(SatInertia_expected(0, 2), SatInertia(0, 2), epsilon)
        Assert.AreEqual(SatInertia_expected(1, 1), SatInertia(1, 1), epsilon)
        Assert.AreEqual(SatInertia_expected(1, 2), SatInertia(1, 2), epsilon)
        Assert.AreEqual(SatInertia_expected(2, 2), SatInertia(2, 2), epsilon)


    End Sub

    <TestMethod()> Public Sub sat_global_Inertia_type3Low_test()

        ' ARRANGE

        initialize_sat_values_inertia()

        Dim SatInertia_expected As Double(,) = {{20.1733, 1, 0}, {1, 63.9233, 1}, {0, 1, 68.1366}}


        add_panel_type3(2, 0.8, 1, "+ X", "+ Z", "Lower")
        add_panel_type3(2, 0.8, 1, "+ Y", "+ Z", "Lower")
        add_panel_type3(2, 0.8, 1, "- X", "+ Z", "Lower")
        add_panel_type3(2, 0.8, 1, "- Y", "+ Z", "Lower")


        ' ACT
        frmAttConf.update_attitude_data_fortest()


        ' ASSERT

        'CollectionAssert.AreEqual(SatInertia_expected, SatInertia)
        Assert.AreEqual(SatInertia_expected(0, 0), SatInertia(0, 0), epsilon)
        Assert.AreEqual(SatInertia_expected(0, 1), SatInertia(0, 1), epsilon)
        Assert.AreEqual(SatInertia_expected(0, 2), SatInertia(0, 2), epsilon)
        Assert.AreEqual(SatInertia_expected(1, 1), SatInertia(1, 1), epsilon)
        Assert.AreEqual(SatInertia_expected(1, 2), SatInertia(1, 2), epsilon)
        Assert.AreEqual(SatInertia_expected(2, 2), SatInertia(2, 2), epsilon)


    End Sub


    <TestMethod()> Public Sub sat_global_Inertia_typeMix_test()

        ' ARRANGE

        initialize_sat_values_inertia()

        Dim SatInertia_expected As Double(,) = {{23.20463, 1.26782, -1.92869}, {1.26782, 79.38144, 1.34086}, {-1.92869, 1.34086, 85.61971}}


        add_panel_type3(2, 0.8, 1, "+ X", "+ Z", "Lower")
        add_panel_type3(2, 0.8, 2, "+ Y", "+ Z", "Lower")
        add_panel_type3(2, 0.8, 5, "- X", "+ Z", "Lower")
        add_panel_type3(2, 0.8, 1, "- Y", "+ Z", "Lower")
        add_panel_type2(0.8, 0.8, 1, "+ X")
        add_panel_type2(0.8, 0.8, 1, "- X")



        ' ACT
        frmAttConf.update_attitude_data_fortest()


        ' ASSERT

        'CollectionAssert.AreEqual(SatInertia_expected, SatInertia)
        Assert.AreEqual(SatInertia_expected(0, 0), SatInertia(0, 0), epsilon)
        Assert.AreEqual(SatInertia_expected(0, 1), SatInertia(0, 1), epsilon)
        Assert.AreEqual(SatInertia_expected(0, 2), SatInertia(0, 2), epsilon)
        Assert.AreEqual(SatInertia_expected(1, 1), SatInertia(1, 1), epsilon)
        Assert.AreEqual(SatInertia_expected(1, 2), SatInertia(1, 2), epsilon)
        Assert.AreEqual(SatInertia_expected(2, 2), SatInertia(2, 2), epsilon)


    End Sub


    <TestMethod()> Public Sub sat_global_Inertia_typeMix2_test()

        ' ARRANGE

        initialize_sat_values_inertia2()

        Dim SatInertia_expected As Double(,) = {{203.7461, 4.861, 2.5326}, {4.861, 125.515, -6.4294}, {2.5326, -6.4294, 121.2338}}


        add_panel_type3(2, 1, 1, "+ X", "+ Z", "Lower")
        add_panel_type3(2, 0.8, 2, "+ Y", "+ Z", "Lower")
        add_panel_type3(2, 0.8, 5, "- X", "+ Z", "Upper")
        add_panel_type1(5, 0.8, 10, "- Y")
        add_panel_type2(0.8, 0.8, 1, "+ Z")
        add_panel_type2(0.8, 0.8, 7, "- Z")



        ' ACT
        frmAttConf.update_attitude_data_fortest()


        ' ASSERT

        'CollectionAssert.AreEqual(SatInertia_expected, SatInertia)
        Assert.AreEqual(SatInertia_expected(0, 0), SatInertia(0, 0), epsilon)
        Assert.AreEqual(SatInertia_expected(0, 1), SatInertia(0, 1), epsilon)
        Assert.AreEqual(SatInertia_expected(0, 2), SatInertia(0, 2), epsilon)
        Assert.AreEqual(SatInertia_expected(1, 1), SatInertia(1, 1), epsilon)
        Assert.AreEqual(SatInertia_expected(1, 2), SatInertia(1, 2), epsilon)
        Assert.AreEqual(SatInertia_expected(2, 2), SatInertia(2, 2), epsilon)


    End Sub



#End Region


#Region "Range of values"


    <TestMethod()> Public Sub exceeded_value_centralBody_test()

        Dim limit_value As Double = -1000000000
        Dim i, j As Integer

        load_data_range()

        ' ARRANGE

        Satellite.satBodyHeight = CSng(limit_value)
        Satellite.satBodyWidth = CSng(limit_value)
        BodyMass = limit_value

        For i = 0 To 2
            For j = 0 To 2
                BodyInertia(i, j) = limit_value
            Next
        Next

        GravityMass = limit_value
        GravityDist = vct(Math.Max(0.001, limit_value), 0, 0)

        ' EXPECTED VALUES
        Dim BodyMass_expected As Double
        Dim BodyHeight_expected As Single
        Dim BodyWidth_expected As Single
        Dim BodyInertia_expected(2, 2) As Double
        Dim GravityMass_expected As Double
        Dim GravityDist_X_expected As Double

        If limit_value > 0 Then             ' limit_value must be very positive (very big), otherwise this test has no interest
            '
            BodyHeight_expected = 10
            BodyWidth_expected = 10
            BodyMass_expected = 100
            '
            For i = 0 To 2
                For j = 0 To 2
                    BodyInertia_expected(i, j) = 100
                Next
            Next

            GravityMass_expected = 50
            GravityDist_X_expected = 10
            '

        Else
            BodyHeight_expected = 0.4
            BodyWidth_expected = 0.4
            BodyMass_expected = 0.1
            '
            For i = 0 To 2
                For j = 0 To 2
                    BodyInertia_expected(i, j) = 0
                Next
            Next

            GravityMass_expected = 0
            GravityDist_X_expected = 0.4

        End If

        ' ACT 

        frmAttConf.update_attitude_conf()
        frmAttConf.update_window_data_fortest()

        Dim BodyHeight_actual As Single = CSng(frmAttConf.mass_txtBodyHeight.Text)
        Dim BodyWidth_actual As Single = CSng(frmAttConf.mass_txtBodyWidth.Text)
        Dim BodyMass_actual As Double = CDbl(frmAttConf.mass_txtBodyMass.Text)
        '
        Dim BodyInertia_actual(2, 2) As Double
        BodyInertia_actual(0, 0) = CDbl(frmAttConf.mass_txtIxx.Text)
        BodyInertia_actual(1, 1) = CDbl(frmAttConf.mass_txtIyy.Text)
        BodyInertia_actual(2, 2) = CDbl(frmAttConf.mass_txtIzz.Text)
        BodyInertia_actual(0, 1) = CDbl(frmAttConf.mass_txtIxy.Text) : BodyInertia_actual(1, 0) = CDbl(frmAttConf.mass_txtIxy.Text)
        BodyInertia_actual(0, 2) = CDbl(frmAttConf.mass_txtIxz.Text) : BodyInertia_actual(2, 0) = CDbl(frmAttConf.mass_txtIxz.Text)
        BodyInertia_actual(1, 2) = CDbl(frmAttConf.mass_txtIyz.Text) : BodyInertia_actual(2, 1) = CDbl(frmAttConf.mass_txtIyz.Text)
        '
        Dim GravityMass_actual As Double = CDbl(frmAttConf.mass_txtBoomMass.Text)
        Dim GravityDist_X_actual As Double = CDbl(frmAttConf.mass_txtBoomDist.Text)

        ' ASSERT

        Assert.AreEqual(BodyMass_expected, BodyMass_actual)
        Assert.AreEqual(BodyHeight_expected, BodyHeight_actual)
        Assert.AreEqual(BodyWidth_expected, BodyWidth_actual)
        CollectionAssert.AreEqual(BodyInertia_expected, BodyInertia_actual)
        Assert.AreEqual(GravityMass_expected, GravityMass_actual)
        Assert.AreEqual(GravityDist_X_expected, GravityDist_X_actual)
    End Sub


    <TestMethod()> Public Sub exceeded_value_solarPanels_test()

        Dim limit_value As Double = -1000000000

        add_panel_type1(0.8, 0.8, 1, "+ X")
        add_panel_type2(0.8, 0.8, 1, "- X")
        add_panel_type1(0.8, 0.8, 1, "+ Y")
        add_panel_type3(0.8, 0.8, 1, "- X", "+ Z", "Upper")

        load_data_range()

        ' ARRANGE

        With SolarPanels.Item(PanelsNumber)
            .PanelLength = limit_value
            .PanelWidth = limit_value
            .PanelMass = limit_value
        End With


        ' EXPECTED VALUES
        Dim PanelLength_expected As Double
        Dim PanelWidth_expected As Double
        Dim PanelMass_expected As Double


        If limit_value > 0 Then             ' limit_value must be very positive (very big), otherwise this test has no interest
            '
            PanelLength_expected = 20
            PanelWidth_expected = 20
            PanelMass_expected = 50
            '
        Else
            PanelLength_expected = 0.1
            PanelWidth_expected = 0.1
            PanelMass_expected = 0.1
            '
        End If

        ' ACT 

        frmAttConf.update_attitude_conf()
        frmAttConf.update_window_data_fortest()

        Dim PanelLength_actual As Double = CDbl(frmAttConf.mass_txtPanelLength.Text)
        Dim PanelWidth_actual As Double = CDbl(frmAttConf.mass_txtPanelWidth.Text)
        Dim PanelMass_actual As Double = CDbl(frmAttConf.mass_txtPanelMass.Text)

        ' ASSERT

        Assert.AreEqual(PanelLength_expected, PanelLength_actual)
        Assert.AreEqual(PanelWidth_expected, PanelWidth_actual)
        Assert.AreEqual(PanelMass_expected, PanelMass_actual)
        
    End Sub


    <TestMethod()> Public Sub exceeded_value_magneticDipole_test()

        Dim limit_value As Double = 1000000000

        load_data_range()

        ' ARRANGE

        dipsat_passive = vct(limit_value, limit_value, limit_value)
        dipsat_active = vct(limit_value, limit_value, limit_value)


        ' EXPECTED VALUES
        Dim dipsat_passive_expected As vector
        Dim dipsat_active_expected As vector


        If limit_value > 0 Then             ' limit_value must be very positive (very big), otherwise this test has no interest
            '
            dipsat_passive_expected = vct(10, 10, 10)
            dipsat_active_expected = vct(10, 10, 10)
            '
        Else
            dipsat_passive_expected = vct(-10, -10, -10)
            dipsat_active_expected = vct(-10, -10, -10)
            '
        End If

        ' ACT 

        frmAttConf.update_attitude_conf()
        frmAttConf.update_window_data_fortest()

        Dim dipsat_passive_actual As vector = vct(CDbl(frmAttConf.prop_txtMG_X.Text), CDbl(frmAttConf.prop_txtMG_Y.Text), CDbl(frmAttConf.prop_txtMG_Z.Text))
        Dim dipsat_active_actual As vector = vct(CDbl(frmAttConf.act_txtMG_X.Text), CDbl(frmAttConf.act_txtMG_Y.Text), CDbl(frmAttConf.act_txtMG_Z.Text))


        ' ASSERT

        Assert.AreEqual(dipsat_passive_expected, dipsat_passive_actual)
        Assert.AreEqual(dipsat_active_expected, dipsat_active_actual)


    End Sub



    <TestMethod()> Public Sub exceeded_value_flywheels_damper_nozzle_test()

        Dim limit_value As Double = 1000000000
        Dim i As Integer

        load_data_range()

        ' ARRANGE

        For i = 0 To 2
            JR(i) = limit_value
            om(i) = limit_value
            DampersMass(i) = limit_value
            DampersDist(i) = limit_value
            c(i) = limit_value
            kd(i) = limit_value
            jet(i) = limit_value
        Next


        ' EXPECTED VALUES
        Dim JR_expected(2) As Double
        Dim om_expected(2) As Double
        Dim DampersMass_expected(2) As Double
        Dim DampersDist_expected(2) As Double
        Dim c_expected(2) As Double
        Dim kd_expected(2) As Double
        Dim jet_expected(2) As Double


        If limit_value > 0 Then             ' limit_value must be very positive (very big), otherwise this test has no interest
            '
            For i = 0 To 2
                JR_expected(i) = 50
                om_expected(i) = 1000
                DampersMass_expected(i) = 20
                DampersDist_expected(i) = 10
                c_expected(i) = 100000
                kd_expected(i) = 100000
                jet_expected(i) = 10
            Next
            '
        Else
            For i = 0 To 2
                JR_expected(i) = 0
                om_expected(i) = 0
                DampersMass_expected(i) = 0
                DampersDist_expected(i) = 0
                c_expected(i) = 0
                kd_expected(i) = 0
                jet_expected(i) = -10
            Next
            '
        End If

        ' ACT 

        frmAttConf.update_attitude_conf()
        frmAttConf.update_window_data_fortest()

        Dim JR_actual() As Double = {CDbl(frmAttConf.act_txtIRX.Text), CDbl(frmAttConf.act_txtIRY.Text), CDbl(frmAttConf.act_txtIRZ.Text)}
        Dim om_actual() As Double = {CDbl(frmAttConf.act_txtWRX.Text), CDbl(frmAttConf.act_txtWRY.Text), CDbl(frmAttConf.act_txtWRZ.Text)}
        Dim DampersMass_actual() As Double = {CDbl(frmAttConf.act_txtMX.Text), CDbl(frmAttConf.act_txtMY.Text), CDbl(frmAttConf.act_txtMZ.Text)}
        Dim DampersDist_actual() As Double = {CDbl(frmAttConf.act_txtDX.Text), CDbl(frmAttConf.act_txtDY.Text), CDbl(frmAttConf.act_txtDZ.Text)}
        Dim c_actual() As Double = {CDbl(frmAttConf.act_txtCX.Text), CDbl(frmAttConf.act_txtCY.Text), CDbl(frmAttConf.act_txtCZ.Text)}
        Dim kd_actual() As Double = {CDbl(frmAttConf.act_txtKX.Text), CDbl(frmAttConf.act_txtKY.Text), CDbl(frmAttConf.act_txtKZ.Text)}
        Dim jet_actual() As Double = {CDbl(frmAttConf.act_txtMN_X.Text), CDbl(frmAttConf.act_txtMN_Y.Text), CDbl(frmAttConf.act_txtMN_Z.Text)}


        ' ASSERT

        CollectionAssert.AreEqual(JR_expected, JR_actual)
        CollectionAssert.AreEqual(om_expected, om_actual)
        CollectionAssert.AreEqual(DampersMass_expected, DampersMass_actual)
        CollectionAssert.AreEqual(DampersDist_expected, DampersDist_actual)
        CollectionAssert.AreEqual(c_expected, c_actual)
        CollectionAssert.AreEqual(kd_expected, kd_actual)
        CollectionAssert.AreEqual(jet_expected, jet_actual)


    End Sub






#End Region


    ''' <summary>
    ''' To run at the end of every test
    ''' </summary>
    ''' <remarks></remarks>
    <TestCleanup()> Public Sub fin()
        '
        ' Resetting dampers
        DampersMass(0) = 0
        DampersMass(1) = 0
        DampersMass(2) = 0
        '
        ' Resetting gravity mass
        GravityMass = 0
        GravityDist = vct(0, 0, 0)
        '
    End Sub




End Class