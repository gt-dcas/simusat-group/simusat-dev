﻿Module power_bat
    '
    ' Operation parameters related to batteries
    '
    Public dodre As Single         ' Attention : n'est plus utilisé
    Public chvmax As Single
    Public ichmax As Single
    Public kcharge As Single
    Public chini As Single
    Public bat_courant_entretien As Single
    '
    ' Batteries configuration parameters
    '
    Public cnom As Single           ' Capacité nominale d'un accu en Ampère.Heures
    Public bat_ns As Byte           ' Nombre d'accus série
    Public bat_np As Byte           ' Nombre d'accus parallèle
    Public bat_type As Byte         ' Type 0:NiCd 1:NiH 2:LiIon 3:Générique
    '
    ' Charge behaviour parameters
    '
    Public ch0(5) As Single
    Public ch1(5) As Single
    Public ch2(5) As Single
    Public och(5) As Single
    '
    ' Discharge behaviour parameters
    '
    Public de0(5) As Single
    Public de1(5) As Single
    Public de2(5) As Single
    Public de100(5) As Single
    Public cmax_cnom As Single      ' Capacité maximum disponible (Cmax/Cnom)
    Public c10_cnom As Single       ' Capacité disponible à 10C de décharge (C_10C/Cnom)
    Public tbatmin As Single        ' Température minimum (°)
    Public cmin100 As Single        ' Capacité disponible à température minimum (%)
    Public tbat99 As Single         ' Température (°) à 99% de la charge
    '
    ' Self-discharge behaviour parameters
    '
    Public tc50 As Single
    Public tc0 As Single

    ''' <summary>
    ''' Definition of end part of charge curve by means of a 3rd order polynomial
    ''' </summary>
    ''' <param name="m">Linear zone slope</param>
    ''' <param name="x2">% divergence</param>
    ''' <param name="xoc">% overcharge</param>
    ''' <param name="v2">divergence voltage (V)</param>
    ''' <param name="Voc">overcharge voltage (V)</param>
    ''' <param name="pol3">(result) 4 parameters of 3rd order polynomial which represents the end zone</param>
    ''' <remarks>Be careful to give values corrected with temperature and current</remarks>
    Public Sub define_p3(ByVal m As Double, ByVal x2 As Double, ByVal xoc As Double, _
        ByVal v2 As Double, ByVal Voc As Double, ByVal pol3() As Double)

        Dim m11 As Double
        Dim m12 As Double
        Dim m22 As Double
        Dim m21 As Double
        Dim detm As Double

        m11 = 3 * (x2 * x2 - xoc * xoc)
        m12 = 2 * (x2 - xoc)
        m21 = x2 ^ 3 + 2 * xoc ^ 3 - 3 * xoc * xoc * x2
        m22 = x2 * x2 + xoc * xoc - 2 * xoc * x2
        detm = m11 * m22 - m12 * m21

        pol3(0) = (m * m22 - (v2 - Voc) * m12) / detm
        pol3(1) = (m11 * (v2 - Voc) - m * m21) / detm
        pol3(2) = -3 * xoc * xoc * pol3(0) - 2 * xoc * pol3(1)
        pol3(3) = Voc - xoc ^ 3 * pol3(0) - xoc * xoc * pol3(1) - xoc * pol3(2)

    End Sub

    Public Function derating_current(ByVal current As Double) As Double
        '
        ' Obtains capacity derating due to discharge current rate
        '
        ' Inputs:
        '   current (current/cnom)
        '
        Dim m As Double
        If current <= 0.1 Then
            derating_current = cmax_cnom
        Else
            m = 1 - c10_cnom
            m = 1 - m * Math.Log(current) / Math.Log(10)
            If m > cmax_cnom Then
                derating_current = cmax_cnom
            Else
                derating_current = m
            End If
        End If
    End Function

    Public Function derating_temp(ByVal t As Double) As Double
        '
        ' Returns capacity derating due to temperature
        '
        ' Inputs: t temperature (°C)
        '
        Dim landa As Double
        Dim X As Double
        '
        landa = Math.Log(1000 * (1 - cmin100 / 100)) / (tbat99 - tbatmin)
        X = -(t - tbatmin) * landa
        X = cmin100 / 100 + (1 - cmin100 / 100) * (1 - Math.Exp(X))
        derating_temp = X
        ''
    End Function

    Public Function discharge_voltage(ByVal cur As Double, ByVal t As Double, ByVal ch As Double) As Double
        '
        ' Returns discharge voltage due to given current, temperature and state of charge
        '
        ' Inputs:
        '   - current (current/cnom)
        '   - temperature (°C)
        '   - state of charge
        '
        Dim q(2) As Double
        Dim m As Double
        Dim x1 As Double
        Dim x2 As Double
        Dim v0 As Single
        Dim v1 As Single
        Dim v2 As Single
        Dim v100 As Single
        Dim X As Double
        '
        On Error GoTo errorbats
        '
        If ch <= 0 Then
            discharge_voltage = 0
        Else
            X = 1 - ch 'State of discharge
            '
            ' Correction with temperature and current
            ' Takes care of the reasonable limits of values
            '
            x1 = (de1(0) + de1(1) * (t - 25) + de1(2) * Math.Log(cur) / Math.Log(10)) / 100
            x2 = (de2(0) + de2(1) * (t - 25) + de2(2) * Math.Log(cur) / Math.Log(10)) / 100
            x1 = value_limiter_gen(x1, 0, 1)
            x2 = value_limiter_gen(x2, 0, 1)
            v1 = de1(3) + de1(4) * (t - 25) + de1(5) * Math.Log(cur) / Math.Log(10)
            v2 = de2(3) + de2(4) * (t - 25) + de2(5) * Math.Log(cur) / Math.Log(10)
            m = (v2 - v1) / (x2 - x1)
            '
            If X < x1 Then
                v0 = de0(3) + de0(4) * (t - 25) + de0(5) * Math.Log(cur) / Math.Log(10)
                q(0) = (m * x1 + v0 - v1) / x1 / x1
                q(1) = m - 2 * x1 * q(0)
                q(2) = v0
                discharge_voltage = q(0) * X * X + q(1) * X + q(2)
            ElseIf X >= x1 And X <= x2 Then
                discharge_voltage = m * (X - x1) + v1
            Else
                v100 = de100(3) + de100(4) * (t - 25) + de100(5) * Math.Log(cur) / Math.Log(10)
                q(0) = (v2 - v100 + m * (1 - x2)) / (2 * x2 - 1 - x2 * x2)
                q(1) = m - 2 * x2 * q(0)
                q(2) = q(0) * (2 * x2 - 1) - m + v100
                discharge_voltage = q(0) * X * X + q(1) * X + q(2)
            End If
            '
        End If
        '
        Exit Function
        '
errorbats:
        MsgBox("Incompatibility between battery discharge paramters !", vbCritical)
        frmPowerConf.tabPower.TabIndex = 3
        frmPowerConf.Show()
        ''
    End Function

    ''' <summary>
    ''' Returns charge voltage due to given current, temperature and state of charge
    ''' </summary>
    ''' <param name="cur">current (current/cnom)</param>
    ''' <param name="t">temperature (°C)</param>
    ''' <param name="ch">state of charge (%)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function charge_voltage(ByVal cur As Double, ByVal t As Double, ByVal ch As Double) As Double

        Dim q(2) As Double
        Dim p(3) As Double
        Dim m As Double
        Dim x1 As Double
        Dim x2 As Double
        Dim xoc As Double
        Dim v0 As Single
        Dim v1 As Single
        Dim v2 As Single
        Dim Voc As Single
        Dim X As Double
        Dim xs As String

        On Error GoTo errorbats

        X = ch
        '
        ' Correction with temperature and current
        ' Takes care of the reasonable limits of values
        '
        x1 = (ch1(0) + ch1(1) * (t - 25) + ch1(2) * Math.Log(cur) / Math.Log(10)) / 100
        x2 = (ch2(0) + ch2(1) * (t - 25) + ch2(2) * Math.Log(cur) / Math.Log(10)) / 100
        x1 = value_limiter_gen(x1, 0, 1)
        x2 = value_limiter_gen(x2, 0, 1)
        v1 = ch1(3) + ch1(4) * (t - 25) + ch1(5) * Math.Log(cur) / Math.Log(10)
        v2 = ch2(3) + ch2(4) * (t - 25) + ch2(5) * Math.Log(cur) / Math.Log(10)
        m = (v2 - v1) / (x2 - x1)
        '
        If X < x1 Then
            v0 = ch0(3) + ch0(4) * (t - 25) + ch0(5) * Math.Log(cur) / Math.Log(10)
            q(0) = (m * x1 + v0 - v1) / x1 / x1
            q(1) = m - 2 * x1 * q(0)
            q(2) = v0
            charge_voltage = q(0) * X * X + q(1) * X + q(2)
        ElseIf X >= x1 And X <= x2 Then
            charge_voltage = m * (X - x1) + v1
        Else
            Voc = och(3) + och(4) * (t - 25) + och(5) * Math.Log(cur) / Math.Log(10)
            xoc = (och(0) + och(1) * (t - 25) + och(2) * Math.Log(cur) / Math.Log(10)) / 100
            Call define_p3(m, x2, xoc, v2, Voc, p)
            charge_voltage = p(0) * X ^ 3 + p(1) * X * X + p(2) * X + p(3)
        End If
        '
        Exit Function

errorbats:
        xs = MsgBox("Incompatibilité entre les paramètres de charge des batteries !!", vbCritical)
        frmPowerConf.tabPower.TabIndex = 3
        frmPowerConf.Show()
        frmPowerConf.tabPower.TabIndex = 3
        frmPowerConf.bat_onglet_batterie.TabIndex = 0
        ''
    End Function

    Public Function current_charge_ctvoltage(ByVal ch As Double, ByVal vdef As Double, ByVal cur As Double, ByVal t As Double) As Double
        '
        ' Returns current related to current state of charge of battery and pre-defined voltage.
        ' Uses Newton's method.
        '
        ' Inputs:
        '   - current state of charge (%)
        '   - Pre-defined voltage (V)
        '   - value of current to start the iteration (Current/Cnom)
        '   - temperature (°C)
        '
        Dim v0 As Double
        Dim v1 As Double
        Dim y0 As Double
        Dim y1 As Double
        Dim Y As Double

        Y = cur
        y0 = cur
        v0 = charge_voltage(y0, t, ch)
        y1 = y0 * 0.99
        v1 = charge_voltage(y1, t, ch)

        Do While Math.Abs(v1 - vdef) > 0.00001
            Y = (vdef - v0) * (y1 - y0) / (v1 - v0) + y0
            y0 = y1
            v0 = v1
            '
            ' In case of no curve featuring our power requirements the Newton converges
            ' towards the y < 0
            ' (e.g. charging maximum voltage supposes almost negligible current )
            '
            If Y <= 0 Then
                Y = 0.000001
            End If
            y1 = Y
            v1 = charge_voltage(y1, t, ch)
        Loop
        current_charge_ctvoltage = Y
        ''
    End Function

    ''' <summary>
    ''' Returns current related to current state of charge of battery and demanded power during charge phase (Newton's method)
    ''' </summary>
    ''' <param name="ch">current state of charge (%)</param>
    ''' <param name="pow">demand of power (Power/Cnom V/h)</param>
    ''' <param name="cur">value of current to start the iteration (Current/Cnom)</param>
    ''' <param name="t">temperature (°C)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function current_charge_ctpower(ByVal ch As Double, ByVal pow As Double, ByVal cur As Double, ByVal t As Double) As Double

        Dim p0 As Double
        Dim P1 As Double
        Dim y0 As Double
        Dim y1 As Double
        Dim Y As Double

        Y = cur
        y0 = cur
        p0 = charge_voltage(y0, t, ch) * y0
        y1 = y0 * 1.1

        Do While Math.Abs(P1 - pow) > 0.00001
            P1 = charge_voltage(y1, t, ch) * y1
            Y = (pow - p0) * (y1 - y0) / (P1 - p0) + y0
            y0 = y1
            p0 = P1

            '
            'In case of no curve featuring our power requirements
            '(e.g. power demanded from batteries almost negligible)
            '

            If Y <= 0 Then
                Y = 0.000001
            End If
            y1 = Y
        Loop
        current_charge_ctpower = Y

    End Function

    Public Function current_discharge_ctpower(ByVal ch As Double, ByVal pow As Double, ByVal cur As Double, ByVal t As Double) As Double
        '
        ' Returns current related to current state of charge of battery and demanded power
        ' during discharge phase.
        ' Uses Newton's method.
        '
        ' Inputs:
        '   - current state of charge (%)
        '   - demand of power (Power/Cnom V/h)
        '   - value of current to start the iteration (Current/Cnom)
        '   - temperature (°C)
        '
        Dim p0 As Double
        Dim P1 As Double
        Dim y0 As Double
        Dim y1 As Double
        Dim Y As Double
        '
        If ch <= 0 Then
            current_discharge_ctpower = 0
        Else
            Y = cur
            y0 = cur
            p0 = discharge_voltage(y0, t, ch) * y0
            y1 = y0 * 1.1

            Do While Math.Abs(P1 - pow) > 0.00001
                P1 = discharge_voltage(y1, t, ch) * y1
                Y = (pow - p0) * (y1 - y0) / (P1 - p0) + y0
                y0 = y1
                p0 = P1
                '
                'In case of no curve featuring our power requirements
                '(e.g. power demanded to batteries almost negligible)
                If Y <= 0 Then
                    Y = 0.000001
                End If
                y1 = Y
            Loop
            current_discharge_ctpower = Y
        End If
        ''
    End Function

    ''' <summary>
    ''' Computes self_discharge coefficient which gives the relation resulting %charge / initial %charge
    ''' </summary>
    ''' <param name="t">temperature (°C)</param>
    ''' <param name="pas">step, time in storage since last correction</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function self_discharge(ByVal t As Double, ByVal pas As Long) As Double
        '
        Dim tc As Single    ' Time constant in seconds, as well as pas
        '
        tc = tc0 * Math.Exp(Math.Log(tc50 / tc0) * t / 50) * 86400
        self_discharge = Math.Exp(-pas / tc)
        '
    End Function
    
End Module
