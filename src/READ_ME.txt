Here are the instructions on how tu run the satellite rotation simulation on Unity



1/ Download Unity 2017.1.1f1 Personal

2/Download the project on Openforge which is located on the branch stage_2017Octobre_Robin,
named Unity_test
		Make sure you have all the components

3/Run Unity and on the opening screen, clisk on open

4/Browse to the project doc and select it

5/In the window Hierarchy, click on the arrow pointing to Objects and select Satellite

6/In the window Inspector, you can change either A_km (semi big axe in kilometers),
E (the excentricity), or Delta_T (the delta of time)

7/To run the simulation, click on the icon play, on the top of the screen,
click on it again to quit the simulation running.

8/To edit the scripts of each piece of material, click on the gearing nexwt to
the name of the script in the window Inspector and select Edit script.