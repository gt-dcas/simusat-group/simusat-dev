﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class SynthesisUnitTest

    ''' <summary>
    ''' To run before every test, data taken from BE
    ''' </summary>
    ''' <remarks></remarks>
    <TestInitialize()> Public Sub satelliteInitialization()
        '
        ' Creating satellite
        Satellite = New CSatellite
        Call Satellite.construct("New_Sat", 65535, 0, 0, True)
        With Satellite
            .satBodyHeight = 0.8
            .satBodyWidth = 0.8
            .primaryAxis = "-z"
            .primaryDir = "nadir"
            .secondaryAxis = "x"
            .secondaryDir = "velocity"
        End With
        '
        ' Creating orbit
        With SatOrbit0
            .Name = "Sun synchronous 800"
            .SemiMajor_Axis = 7278.137
            .Eccentricity = 0
            .Inclination = 98.602
            .Argument_Perigee = 0
            .RAAN = 0
            .Mean_Anomaly = 0
            .epoch = 2451544.5          ' January 1st 2000 at 0h
            .dec1 = 0
            .dec2 = 0
            .bstar = 0
        End With
        SatOrbit = SatOrbit0
        '
        ' Creating new collection of solar panels
        SolarPanels = New Collection
        ''
    End Sub



    ''' <summary>
    ''' Add an orientable panel (Type 1)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y" or "- Y"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type1(ByRef length As Single, ByRef width As Single, ByRef mass As Single, ByRef face As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type1"        ' Orientable Panel
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub

    ''' <summary>
    ''' Add a panel on a face (Type 2)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y","- Y", "+ Z" or "- Z"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type2(ByRef length As Single, ByRef width As Single, ByRef mass As Single, ByRef face As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type3"        ' Panel on a face
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub

    ''' <summary>
    ''' Add a panel fixed perpendicular to a face (Type 3)
    ''' </summary>
    ''' <param name="length">Length</param>
    ''' <param name="width">Width</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="face">Face : "+ X", "- X", "+ Y" or "- Y"</param>
    ''' <param name="dir">Direction : "+ Z" or "- Z"</param>
    ''' <param name="hpos">Position : "Upper", "Middle" or "Lower"</param>
    ''' <remarks></remarks>
    Private Sub add_panel_type3(ByRef length As Single, ByRef width As Single, ByRef mass As Single, _
                                ByRef face As String, ByRef dir As String, ByRef hpos As String)
        '
        Dim TempPanel As New CSolarPanel
        With TempPanel
            .PanelType = "Type3"        ' Panel fixed perpendicular to a face
            .PanelLength = length
            .PanelWidth = width
            .PanelMass = mass
            .PanelFace = face
            .PanelDir = dir
            .PanelHpos = hpos
            .PanelName = frmAttConf.mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & " : " & .PanelDir & "]"
            frmAttConf.mass_lstPanels.Items.Add((.PanelName))
        End With
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count()
        ''
    End Sub


    Private Sub add_panel1_randomly()

        add_panel_type1(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(1))
            
    End Sub

    Private Sub add_panel2_randomly()

        add_panel_type2(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(2))
           
    End Sub


    Private Sub add_panel_randomly()

        add_panel_type3(19.9 * Rnd() + 0.1, 19.9 * Rnd() + 0.1, 49.9 * Rnd() + 0.1, select_random_face(3), select_random_orientation(), select_random_position())

    End Sub



    Private Function select_random_face(ByRef type As Integer) As String

        Select Case type
            Case 1
                Dim faces1() As String = {"+ X", "+ Y", "- X", "- Y"}
                Return faces1(Int(4 * Rnd()) + 0)
            Case 2
                Dim faces2() As String = {"+ X", "+ Y", "+ Z", "- X", "- Y", "- Z"}
                Return faces2(Int(6 * Rnd()) + 0)
            Case 3
                Dim faces3() As String = {"+ X", "+ Y", "- X", "- Y"}
                Return faces3(Int(4 * Rnd()) + 0)
        End Select


    End Function


    Private Function select_random_orientation() As String

        Dim orientation() As String = {"+ Z", "- Z"}
        Return orientation(Int(2 * Rnd()) + 0)

    End Function



    Private Function select_random_position() As String

        Dim orientation() As String = {"Upper", "Middle", "Lower"}
        Return orientation(Int(3 * Rnd()) + 0)

    End Function







    ''' <summary>
    ''' Test the calculation of the new mass of the whole satellite
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()> Public Sub sat_global_mass_test()

        ' SETTINGS
        BodyMass = 20
        GravityMass = 10

        'add_panel1_randomly()
        'add_panel2_randomly()

        add_panel_type2(1, 1, 5, "+ Z")
        add_panel_type3(1, 1, 0.5, "- Y", "+ Z", "Lower")


        ' ARRANGE
        Dim PanelMass As Double
        Dim i As Integer
        Dim mass_expected As Double = BodyMass + GravityMass + DampersMass(0) + DampersMass(1) + DampersMass(2)
        For i = 1 To PanelsNumber
            With SolarPanels.Item(i)
                .PanelMass = 3
                PanelMass = CDbl(.PanelMass)
            End With
            mass_expected += PanelMass
        Next

        ' ACT
        frmAttConf.update_attitude_data_fortest()

        ' ASSERT
        Assert.AreEqual(mass_expected, SatMass)

    End Sub


End Class