


#############################
# Check computer installation 
#############################
1- access to Simusat OpenForge
[https://openforge.isae.fr/projects/simusat](https://openforge.isae.fr/projects/simusat)

==================================================================
2- git installed and working
- A git client  (already installed, or https://git-scm.com/download/win for windows)


===========
GIT interactive
===========
https://www.katacoda.com/courses/git

===========
GIT tutorial at ISAE
===========
https://openforge.isae.fr/projects/nimph/repository/revisions/master/raw/repoPIE_2017-2018/2-WorkingDirectory/ProactiveWork/TutoGitForNimph.pdf

===========
GIT (branching)
===========
https://learngitbranching.js.org/


===========
smartGIT (windows)
===========
Tuto de Melvin sur Smartgit (https://www.syntevo.com/smartgit/download/)
COMMENT CLONER LE REPERTOIRE
1) Lance SmartGit
2) Clone le répertoire, pour cela :
2.1) Clique sur "Remote Git or SVN repository"
2.2) Entre "https://openforge.isae.fr/git/jsatorb" dans "Repository URL"
2.3) "Select the type of repository", clique sur Git
3) Connecte toi (entre les mêmes identifiants que sur Openforge ou iCampus)
4) "Selection, customize how and what to clone", selectionne tout et clique sur Next
5) Local directory, entre un dossier de destination et clique sur Finish

COMMENT UTILISER SMARTGIT
1) Dès que tu veux sauvegarder ce que tu as fait, clique sur "Commit" (en haut à gauche)
Remarque : avant de faire un "commit", essaie de supprimer (à la main) les fichiers binaires inutiles (fichiers cache, etc)
2) Ecrit un message qui permet de savoir ce que tu as modifié par rapport à ta dernière sauvegarde
3) Clique sur "Push" (en haut à gauche) pour que ton commit soit partagé sur Openforge

---------------------------------------



==================================================================
3- Visual Studio2013 installed and working
- TAO framework (https://openforge.isae.fr/projects/simusat/repository/revisions/master/show/dependencies)
- MSChart.exe (https://openforge.isae.fr/projects/simusat/repository/revisions/master/show/dependencies) 

- Do some tutorials (create a button, etc...) Explore the code
- Do an example of UnitTesting and Integration testing specifically with VBNET 
- Look at Melvin work on that (but not too deep, just to avoid to redoo already done stuff)




==================================================================
4- Validation Process
You can work on parallel on three aspects, A, B, and C (even if C part will be easier with A working !)

A-Unit testing
A.1- Attitude and control subsystem : -> Unit tests for all values
--> Proove that Synthesis is printing the right values
-- Sat Properties (same)
-- actuators (same)
-- Management: for the moment, remove it (comment the tab in the code, we'll go back on it later)

A.2 - Same idea with Energy subsystem

A.3 - Comparison against Orekit/celestlab curves

B - Ergonomic restructuration. 
- Thermal is not belonging to energy sub system. It should be nice to update the whole tree organisation to make it more logical.
(strucutre with thermal properties of the materials... etc)


C-Integration Testing
- based on BE scenario (earth observation), create 2 or 3 scenarios (and many variants of each) for full validation process
(automatically... of course ! option -t for all unti test + integration tests)
possibly perfomance tests..







