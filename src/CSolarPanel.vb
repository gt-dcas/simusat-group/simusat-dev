Option Strict Off
Option Explicit On
Public Class CSolarPanel
    '
    Public PanelType As String      ' Type of panel : "Type1", "Type2", "Type3" (1 is orientable, 2 is on a face, 3 is perpendicular to a face)
    Public PanelName As String      ' Name of panel Exemple : "2_Type3 [+ X : + Z]" (+X and +Z are panel face and panel direction, only panel face for type 1 and 2)
    Public PanelFace As String      ' Face on which the panel is mounted on the satellite (+x, -x, +y, -y, +z, -z)
    Public PanelDir As String       ' Direction which the panel is pointing to (+X, -X, +Y, -Y, +Z, -Z)
    Public PanelHpos As String      ' Height position of panel type 3 "Upper", "Lower", "Middle"
    Public PanelLength As Single    ' Length of panel
    Public PanelWidth As Single     ' Width of panel
    Public PanelMass As Single      ' Mass of the panel
    Public PanelCellAlf As Single   ' Absorptivity of panel cell face
    Public PanelCellEps As Single   ' Emissivity of panel cell face
    Public PanelBackAlf As Single   ' Absorptivity of panel back face
    Public PanelBackEps As Single   ' Emissivity of panel back face
    Public PanelSpecHeat As Single  ' Specific heat of panel
    Public PanelTemp As Single      ' Current temperature (�C)
    Public PanelSolarFlux As Single ' Flux incident solar
    Public PanelAlbedoFlux As Single ' Flux incident albedo
    Public PanelIRFlux As Single    ' Flux incident IR
    Public PanelEffFlux As Single   ' Efficient flux
    Public PanelPower As Single     ' Power
    Public PanelRollAngle As Double ' Angle (rad) by which the panel is rotated around its axe (-pi,+pi)
    Public PanelSurf As Single      ' Panel surface
    Public PanelNbString As Integer ' Number of solar cells strings
    Public PanelConduction As Single ' Panel conduction with central body
    '
    ''' <summary>
    ''' Calculate angle of incidence of the earth on the solar panel
    ''' ----------------------------------------------------------
    ''' Use this to calculate the earth angle of incidence on panels that are mounted on the satellite in a fixed direction
    ''' relative to the satellite's orientation
    ''' </summary>
    ''' <param name="time">used to calculate position of the satellite</param>
    ''' <remarks>This function returns the earth incidence in radians</remarks>
    Friend Function EarthIncidence(ByRef time As Double) As Single
        '
        Dim TargetDir As vector
        Dim panel_dir As vector
        Dim SunDir As vector            ' Sun direction in the satellite's coordinates system
        '
        ' Calculate sun direction
        With Satellite.getSatCOSCoords(time, SunPos)
            SunDir.X = .X : SunDir.Y = .Y : SunDir.Z = .Z
        End With
        '
        If Me.PanelType = "Type1" Then
            '
            ' In this case we compute the optimal orientation of the panel, taking account the sun direction only.
            ' The criteria is to minimize the sun incidence on the panel. The roll angle varies between -pi and +pi
            Select Case Me.PanelFace
                Case "+ X", "- X"
                    panel_dir.X = 0 : panel_dir.Y = SunDir.Y : panel_dir.Z = SunDir.Z
                    Me.PanelRollAngle = Math.Atan2(panel_dir.Z, -panel_dir.Y)    ' Rotation positive de Y vers z
                Case "+ Y", "- Y"
                    panel_dir.X = SunDir.X : panel_dir.Y = 0 : panel_dir.Z = SunDir.Z
                    Me.PanelRollAngle = Math.Atan2(panel_dir.Z, panel_dir.X)    ' Rotation positive de Z vers X
            End Select
            '
            ' Panel orientation in the satellite's COS
        ElseIf Me.PanelType = "Type2" Then
            Select Case Me.PanelFace
                Case "+ X" : panel_dir.X = 1 : panel_dir.Y = 0 : panel_dir.Z = 0
                Case "- X" : panel_dir.X = -1 : panel_dir.Y = 0 : panel_dir.Z = 0
                Case "+ Y" : panel_dir.X = 0 : panel_dir.Y = 1 : panel_dir.Z = 0
                Case "- Y" : panel_dir.X = 0 : panel_dir.Y = -1 : panel_dir.Z = 0
                Case "+ Z" : panel_dir.X = 0 : panel_dir.Y = 0 : panel_dir.Z = 1
                Case "- Z" : panel_dir.X = 0 : panel_dir.Y = 0 : panel_dir.Z = -1
            End Select
        Else
            Select Case Me.PanelDir
                Case "+ Z" : panel_dir.X = 0 : panel_dir.Y = 0 : panel_dir.Z = 1
                Case "- Z" : panel_dir.X = 0 : panel_dir.Y = 0 : panel_dir.Z = -1
            End Select
        End If

        '
        ' Calculates the earth incidence
        Dim tmpVect As vector
        tmpVect.X = -SatPos.X : tmpVect.Y = -SatPos.Y : tmpVect.Z = -SatPos.Z  'here modif to avoid NaN... que a non 0 norm...
        With Satellite.getSatCOSCoords(time, tmpVect)
            TargetDir.X = .X : TargetDir.Y = .Y : TargetDir.Z = .Z ' Earth direction in the satellite's COS
        End With

        EarthIncidence = Math.Acos(vctDot(TargetDir, panel_dir) / vctNorm(TargetDir) / vctNorm(panel_dir))
        printOnConsoleIfBebugMode("EarthIncidence for EarthIncidence is:" + EarthIncidence.ToString)
        '
    End Function

    ''' <summary>
    ''' Calculate angle of incidence of the sun on the solar panel
    ''' </summary>
    ''' <param name="time">used to calculate position of the satellite</param>
    ''' <remarks>This function returns the sun incidence in radians</remarks>
    Friend Function SunIncidence(ByRef time As Double) As Single
        '
        Dim SunDir As vector            ' Sun direction in the satellite's coordinates system 
        Dim Panel_dir As vector         ' Panel orientation in the satellite's coordinates system
        '
        ' Calculate sun direction
        With Satellite.getSatCOSCoords(time, SunPos)
            SunDir.X = .X : SunDir.Y = .Y : SunDir.Z = .Z
        End With
        '
        If Me.PanelType = "Type1" Then
            '
            ' In this case we compute the optimal orientation of the panel, taking account the sun direction only.
            ' The criteria is to minimize the sun incidence on the panel. The roll angle varies between -pi and +pi
            Select Case Me.PanelFace
                Case "+ X", "- X"
                    Panel_dir.X = 0 : Panel_dir.Y = SunDir.Y : Panel_dir.Z = SunDir.Z
                    Me.PanelRollAngle = Math.Atan2(Panel_dir.Z, -Panel_dir.Y)    ' Rotation positive de Y vers z
                Case "+ Y", "- Y"
                    Panel_dir.X = SunDir.X : Panel_dir.Y = 0 : Panel_dir.Z = SunDir.Z
                    Me.PanelRollAngle = Math.Atan2(Panel_dir.Z, Panel_dir.X)    ' Rotation positive de Z vers X
            End Select
            '
        ElseIf Me.PanelType = "Type2" Then
            Select Case Me.PanelFace
                Case "+ X" : Panel_dir.X = 1 : Panel_dir.Y = 0 : Panel_dir.Z = 0
                Case "- X" : Panel_dir.X = -1 : Panel_dir.Y = 0 : Panel_dir.Z = 0
                Case "+ Y" : Panel_dir.X = 0 : Panel_dir.Y = 1 : Panel_dir.Z = 0
                Case "- Y" : Panel_dir.X = 0 : Panel_dir.Y = -1 : Panel_dir.Z = 0
                Case "+ Z" : Panel_dir.X = 0 : Panel_dir.Y = 0 : Panel_dir.Z = 1
                Case "- Z" : Panel_dir.X = 0 : Panel_dir.Y = 0 : Panel_dir.Z = -1
            End Select
            '
        Else
            Select Case Me.PanelDir
                Case "+ Z" : Panel_dir.X = 0 : Panel_dir.Y = 0 : Panel_dir.Z = 1
                Case "- Z" : Panel_dir.X = 0 : Panel_dir.Y = 0 : Panel_dir.Z = -1
            End Select
            '
        End If
        '
        ' Calculate the sun incidence
        SunIncidence = Math.Acos(vctDot(SunDir, Panel_dir) / vctNorm(SunDir) / vctNorm(Panel_dir))
        ''
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class