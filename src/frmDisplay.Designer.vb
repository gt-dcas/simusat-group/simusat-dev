﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisplay
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisplay))
        Me.picMapView = New System.Windows.Forms.PictureBox()
        Me.mnuShowSphere = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuShowBigModels = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuShowAttSettings = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDayNight = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmbOptOrient = New System.Windows.Forms.ComboBox()
        Me.cmbOptAxis = New System.Windows.Forms.ComboBox()
        Me.cmbPrinOrient = New System.Windows.Forms.ComboBox()
        Me.cmbPrinAxis = New System.Windows.Forms.ComboBox()
        Me.chkDayNight = New System.Windows.Forms.CheckBox()
        Me.chkSphere = New System.Windows.Forms.CheckBox()
        Me.lblSphereOrient = New System.Windows.Forms.Label()
        Me.cmdClose = New System.Windows.Forms.Button()
        Me.Timerrefresh3d = New System.Windows.Forms.Timer(Me.components)
        Me.chkCouples = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LBLDoubleClick = New System.Windows.Forms.Label()
        CType(Me.picMapView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picMapView
        '
        Me.picMapView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picMapView.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.picMapView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.picMapView.Cursor = System.Windows.Forms.Cursors.Default
        Me.picMapView.ForeColor = System.Drawing.SystemColors.ControlText
        Me.picMapView.Location = New System.Drawing.Point(12, 56)
        Me.picMapView.Name = "picMapView"
        Me.picMapView.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.picMapView.Size = New System.Drawing.Size(1000, 500)
        Me.picMapView.TabIndex = 23
        Me.picMapView.TabStop = False
        '
        'mnuShowSphere
        '
        Me.mnuShowSphere.Checked = True
        Me.mnuShowSphere.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuShowSphere.Name = "mnuShowSphere"
        Me.mnuShowSphere.Size = New System.Drawing.Size(202, 22)
        Me.mnuShowSphere.Text = "Show Sphere"
        '
        'mnuShowBigModels
        '
        Me.mnuShowBigModels.Name = "mnuShowBigModels"
        Me.mnuShowBigModels.Size = New System.Drawing.Size(202, 22)
        Me.mnuShowBigModels.Text = "Fit Satellite to Sphere"
        '
        'mnuShowAttSettings
        '
        Me.mnuShowAttSettings.Name = "mnuShowAttSettings"
        Me.mnuShowAttSettings.Size = New System.Drawing.Size(202, 22)
        Me.mnuShowAttSettings.Text = "Attitude Sphere settings"
        '
        'mnuDayNight
        '
        Me.mnuDayNight.Checked = True
        Me.mnuDayNight.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuDayNight.Name = "mnuDayNight"
        Me.mnuDayNight.Size = New System.Drawing.Size(202, 22)
        Me.mnuDayNight.Text = "&Day/Night"
        '
        'cmbOptOrient
        '
        Me.cmbOptOrient.BackColor = System.Drawing.SystemColors.Window
        Me.cmbOptOrient.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbOptOrient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOptOrient.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbOptOrient.Location = New System.Drawing.Point(775, 15)
        Me.cmbOptOrient.Name = "cmbOptOrient"
        Me.cmbOptOrient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbOptOrient.Size = New System.Drawing.Size(97, 21)
        Me.cmbOptOrient.TabIndex = 28
        '
        'cmbOptAxis
        '
        Me.cmbOptAxis.BackColor = System.Drawing.SystemColors.Window
        Me.cmbOptAxis.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbOptAxis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOptAxis.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbOptAxis.Location = New System.Drawing.Point(720, 15)
        Me.cmbOptAxis.Name = "cmbOptAxis"
        Me.cmbOptAxis.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbOptAxis.Size = New System.Drawing.Size(49, 21)
        Me.cmbOptAxis.TabIndex = 27
        '
        'cmbPrinOrient
        '
        Me.cmbPrinOrient.BackColor = System.Drawing.SystemColors.Window
        Me.cmbPrinOrient.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbPrinOrient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPrinOrient.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbPrinOrient.Location = New System.Drawing.Point(606, 15)
        Me.cmbPrinOrient.Name = "cmbPrinOrient"
        Me.cmbPrinOrient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbPrinOrient.Size = New System.Drawing.Size(97, 21)
        Me.cmbPrinOrient.TabIndex = 26
        '
        'cmbPrinAxis
        '
        Me.cmbPrinAxis.BackColor = System.Drawing.SystemColors.Window
        Me.cmbPrinAxis.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbPrinAxis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPrinAxis.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbPrinAxis.Location = New System.Drawing.Point(551, 15)
        Me.cmbPrinAxis.Name = "cmbPrinAxis"
        Me.cmbPrinAxis.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbPrinAxis.Size = New System.Drawing.Size(49, 21)
        Me.cmbPrinAxis.TabIndex = 25
        '
        'chkDayNight
        '
        Me.chkDayNight.AutoSize = True
        Me.chkDayNight.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDayNight.Location = New System.Drawing.Point(34, 4)
        Me.chkDayNight.Name = "chkDayNight"
        Me.chkDayNight.Size = New System.Drawing.Size(92, 20)
        Me.chkDayNight.TabIndex = 29
        Me.chkDayNight.Text = "Day/Night"
        Me.chkDayNight.UseVisualStyleBackColor = True
        '
        'chkSphere
        '
        Me.chkSphere.AutoSize = True
        Me.chkSphere.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSphere.Location = New System.Drawing.Point(34, 27)
        Me.chkSphere.Name = "chkSphere"
        Me.chkSphere.Size = New System.Drawing.Size(170, 20)
        Me.chkSphere.TabIndex = 29
        Me.chkSphere.Text = "Show attitude sphere"
        Me.chkSphere.UseVisualStyleBackColor = True
        '
        'lblSphereOrient
        '
        Me.lblSphereOrient.AutoSize = True
        Me.lblSphereOrient.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSphereOrient.Location = New System.Drawing.Point(406, 18)
        Me.lblSphereOrient.Name = "lblSphereOrient"
        Me.lblSphereOrient.Size = New System.Drawing.Size(130, 16)
        Me.lblSphereOrient.TabIndex = 30
        Me.lblSphereOrient.Text = "Sphere orientation"
        '
        'cmdClose
        '
        Me.cmdClose.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdClose.Location = New System.Drawing.Point(902, 9)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(110, 36)
        Me.cmdClose.TabIndex = 31
        Me.cmdClose.Text = "Close"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'Timerrefresh3d
        '
        Me.Timerrefresh3d.Interval = 10
        '
        'chkCouples
        '
        Me.chkCouples.AutoSize = True
        Me.chkCouples.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCouples.Location = New System.Drawing.Point(217, 8)
        Me.chkCouples.Name = "chkCouples"
        Me.chkCouples.Size = New System.Drawing.Size(133, 20)
        Me.chkCouples.TabIndex = 32
        Me.chkCouples.Text = "Show effects of"
        Me.chkCouples.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(233, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(127, 16)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "disturbing torques"
        '
        'LBLDoubleClick
        '
        Me.LBLDoubleClick.AutoSize = True
        Me.LBLDoubleClick.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.LBLDoubleClick.Enabled = False
        Me.LBLDoubleClick.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBLDoubleClick.Location = New System.Drawing.Point(12, 50)
        Me.LBLDoubleClick.Name = "LBLDoubleClick"
        Me.LBLDoubleClick.Size = New System.Drawing.Size(160, 16)
        Me.LBLDoubleClick.TabIndex = 35
        Me.LBLDoubleClick.Text = "Double click for fullscreen"
        '
        'frmDisplay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.ClientSize = New System.Drawing.Size(1024, 590)
        Me.ControlBox = False
        Me.Controls.Add(Me.LBLDoubleClick)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.chkCouples)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.lblSphereOrient)
        Me.Controls.Add(Me.chkSphere)
        Me.Controls.Add(Me.chkDayNight)
        Me.Controls.Add(Me.cmbOptOrient)
        Me.Controls.Add(Me.cmbOptAxis)
        Me.Controls.Add(Me.cmbPrinOrient)
        Me.Controls.Add(Me.cmbPrinAxis)
        Me.Controls.Add(Me.picMapView)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1040, 606)
        Me.Name = "frmDisplay"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " "
        CType(Me.picMapView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents picMapView As System.Windows.Forms.PictureBox
    Public WithEvents mnuShowSphere As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuShowBigModels As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuShowAttSettings As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuDayNight As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents cmbOptOrient As System.Windows.Forms.ComboBox
    Public WithEvents cmbOptAxis As System.Windows.Forms.ComboBox
    Public WithEvents cmbPrinOrient As System.Windows.Forms.ComboBox
    Public WithEvents cmbPrinAxis As System.Windows.Forms.ComboBox
    Friend WithEvents chkDayNight As System.Windows.Forms.CheckBox
    Friend WithEvents chkSphere As System.Windows.Forms.CheckBox
    Friend WithEvents lblSphereOrient As System.Windows.Forms.Label
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents Timerrefresh3d As System.Windows.Forms.Timer
    Friend WithEvents chkCouples As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents LBLDoubleClick As System.Windows.Forms.Label
End Class
