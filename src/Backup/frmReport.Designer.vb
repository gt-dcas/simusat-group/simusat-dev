<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmReport
#Region "Code g�n�r� par le Concepteur Windows Form "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Cet appel est requis par le Concepteur Windows Form.
		InitializeComponent()
	End Sub
	'Form remplace la m�thode Dispose pour nettoyer la liste des composants.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requise par le Concepteur Windows Form
	Private components As System.ComponentModel.IContainer
    Public dlgReportSave As System.Windows.Forms.SaveFileDialog
	'REMARQUE�: la proc�dure suivante est requise par le Concepteur Windows Form
	'Elle peut �tre modifi�e � l'aide du Concepteur Windows Form.
	'Ne la modifiez pas � l'aide de l'�diteur de code.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReport))
        Me.dlgReportSave = New System.Windows.Forms.SaveFileDialog
        Me.updwTimeStep = New System.Windows.Forms.DomainUpDown
        Me.zlbl_2 = New System.Windows.Forms.Label
        Me.txt_date_fin = New System.Windows.Forms.DateTimePicker
        Me.txt_date_debut = New System.Windows.Forms.DateTimePicker
        Me.zlbl_3 = New System.Windows.Forms.Label
        Me.zlbl_0 = New System.Windows.Forms.Label
        Me.cmdClose = New System.Windows.Forms.Button
        Me.cmdReport = New System.Windows.Forms.Button
        Me.cmdSave = New System.Windows.Forms.Button
        Me.grdReport = New System.Windows.Forms.DataGridView
        Me.optExcel = New System.Windows.Forms.RadioButton
        Me.optText = New System.Windows.Forms.RadioButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        CType(Me.grdReport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dlgReportSave
        '
        Me.dlgReportSave.Filter = "(*.txt)|*.txt"
        Me.dlgReportSave.Title = "Save report as ..."
        '
        'updwTimeStep
        '
        Me.updwTimeStep.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.updwTimeStep.Items.Add("3600 s")
        Me.updwTimeStep.Items.Add("1800 s")
        Me.updwTimeStep.Items.Add("600 s")
        Me.updwTimeStep.Items.Add("300 s")
        Me.updwTimeStep.Items.Add("180 s")
        Me.updwTimeStep.Items.Add("60 s")
        Me.updwTimeStep.Items.Add("10 s")
        Me.updwTimeStep.Items.Add("5 s")
        Me.updwTimeStep.Items.Add("1 s")
        Me.updwTimeStep.Location = New System.Drawing.Point(519, 27)
        Me.updwTimeStep.Name = "updwTimeStep"
        Me.updwTimeStep.Size = New System.Drawing.Size(74, 23)
        Me.updwTimeStep.TabIndex = 33
        Me.updwTimeStep.Text = "60 s"
        '
        'zlbl_2
        '
        Me.zlbl_2.AutoSize = True
        Me.zlbl_2.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_2.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_2.Location = New System.Drawing.Point(247, 29)
        Me.zlbl_2.Name = "zlbl_2"
        Me.zlbl_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_2.Size = New System.Drawing.Size(22, 16)
        Me.zlbl_2.TabIndex = 30
        Me.zlbl_2.Text = "to"
        Me.zlbl_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txt_date_fin
        '
        Me.txt_date_fin.CalendarFont = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_date_fin.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        Me.txt_date_fin.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_date_fin.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txt_date_fin.Location = New System.Drawing.Point(275, 26)
        Me.txt_date_fin.Name = "txt_date_fin"
        Me.txt_date_fin.Size = New System.Drawing.Size(186, 23)
        Me.txt_date_fin.TabIndex = 32
        '
        'txt_date_debut
        '
        Me.txt_date_debut.CalendarFont = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_date_debut.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        Me.txt_date_debut.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_date_debut.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txt_date_debut.Location = New System.Drawing.Point(55, 26)
        Me.txt_date_debut.Name = "txt_date_debut"
        Me.txt_date_debut.Size = New System.Drawing.Size(186, 23)
        Me.txt_date_debut.TabIndex = 31
        '
        'zlbl_3
        '
        Me.zlbl_3.AutoSize = True
        Me.zlbl_3.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_3.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_3.Location = New System.Drawing.Point(474, 28)
        Me.zlbl_3.Name = "zlbl_3"
        Me.zlbl_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_3.Size = New System.Drawing.Size(39, 16)
        Me.zlbl_3.TabIndex = 28
        Me.zlbl_3.Text = "Step"
        Me.zlbl_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_0
        '
        Me.zlbl_0.AutoSize = True
        Me.zlbl_0.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_0.Location = New System.Drawing.Point(10, 29)
        Me.zlbl_0.Name = "zlbl_0"
        Me.zlbl_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_0.Size = New System.Drawing.Size(40, 16)
        Me.zlbl_0.TabIndex = 29
        Me.zlbl_0.Text = "From"
        Me.zlbl_0.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdClose
        '
        Me.cmdClose.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdClose.Location = New System.Drawing.Point(931, 22)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(107, 31)
        Me.cmdClose.TabIndex = 34
        Me.cmdClose.Text = "Close"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'cmdReport
        '
        Me.cmdReport.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdReport.Location = New System.Drawing.Point(635, 23)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(144, 30)
        Me.cmdReport.TabIndex = 35
        Me.cmdReport.Text = "Report"
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Location = New System.Drawing.Point(798, 22)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(112, 31)
        Me.cmdSave.TabIndex = 35
        Me.cmdSave.Text = "Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'grdReport
        '
        Me.grdReport.AllowUserToAddRows = False
        Me.grdReport.AllowUserToDeleteRows = False
        Me.grdReport.AllowUserToResizeColumns = False
        Me.grdReport.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure
        Me.grdReport.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.grdReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.grdReport.BackgroundColor = System.Drawing.Color.LightCyan
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdReport.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdReport.DefaultCellStyle = DataGridViewCellStyle3
        Me.grdReport.Location = New System.Drawing.Point(15, 94)
        Me.grdReport.Name = "grdReport"
        Me.grdReport.ReadOnly = True
        Me.grdReport.RowHeadersVisible = False
        Me.grdReport.Size = New System.Drawing.Size(1022, 495)
        Me.grdReport.TabIndex = 36
        '
        'optExcel
        '
        Me.optExcel.AutoSize = True
        Me.optExcel.Location = New System.Drawing.Point(807, 61)
        Me.optExcel.Name = "optExcel"
        Me.optExcel.Size = New System.Drawing.Size(51, 17)
        Me.optExcel.TabIndex = 37
        Me.optExcel.Text = "Excel"
        Me.optExcel.UseVisualStyleBackColor = True
        '
        'optText
        '
        Me.optText.AutoSize = True
        Me.optText.Checked = True
        Me.optText.Location = New System.Drawing.Point(864, 61)
        Me.optText.Name = "optText"
        Me.optText.Size = New System.Drawing.Size(46, 17)
        Me.optText.TabIndex = 38
        Me.optText.TabStop = True
        Me.optText.Text = "Text"
        Me.optText.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.updwTimeStep)
        Me.GroupBox1.Controls.Add(Me.zlbl_2)
        Me.GroupBox1.Controls.Add(Me.txt_date_fin)
        Me.GroupBox1.Controls.Add(Me.txt_date_debut)
        Me.GroupBox1.Controls.Add(Me.zlbl_3)
        Me.GroupBox1.Controls.Add(Me.zlbl_0)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Maroon
        Me.GroupBox1.Location = New System.Drawing.Point(15, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(603, 67)
        Me.GroupBox1.TabIndex = 40
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Choose period"
        '
        'frmReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1056, 623)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.optText)
        Me.Controls.Add(Me.optExcel)
        Me.Controls.Add(Me.grdReport)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.cmdReport)
        Me.Controls.Add(Me.cmdClose)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(114, 173)
        Me.Name = "frmReport"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Simusat report"
        CType(Me.grdReport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents updwTimeStep As System.Windows.Forms.DomainUpDown
    Public WithEvents zlbl_2 As System.Windows.Forms.Label
    Friend WithEvents txt_date_fin As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_date_debut As System.Windows.Forms.DateTimePicker
    Public WithEvents zlbl_3 As System.Windows.Forms.Label
    Public WithEvents zlbl_0 As System.Windows.Forms.Label
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Protected WithEvents grdReport As System.Windows.Forms.DataGridView
    Friend WithEvents optExcel As System.Windows.Forms.RadioButton
    Friend WithEvents optText As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
#End Region
End Class