Option Strict Off
Option Explicit On

Imports Tao.OpenGl
Imports Tao.Platform.Windows
Imports System.Drawing.Imaging



Module basGraphics

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="hDC"></param>
    ''' <remarks></remarks>
    Sub setupPixelFormat(ByVal hDC As Integer)
        Dim pfd As Gdi.PIXELFORMATDESCRIPTOR
        Dim PixelFormat As Short

        pfd.nSize = Len(pfd)
        pfd.nVersion = 1
        pfd.dwFlags = Gdi.PFD_SUPPORT_OPENGL Or Gdi.PFD_DRAW_TO_WINDOW Or Gdi.PFD_DOUBLEBUFFER Or Gdi.PFD_TYPE_RGBA
        pfd.iPixelType = Gdi.PFD_TYPE_RGBA
        pfd.cColorBits = 24
        pfd.cDepthBits = 24
        pfd.iLayerType = Gdi.PFD_MAIN_PLANE
        PixelFormat = Gdi.ChoosePixelFormat(hDC, pfd)
        Gdi.SetPixelFormat(hDC, PixelFormat, pfd)
    End Sub

    ''' <summary>
    ''' Initializes OpenGl-Window in PictureBox
    '''
    ''' This function Initialize a PictureBox-Control for acting as OpenGL Display Context
    ''' </summary>
    ''' <param name="PictureBox">the PictureBoxControl</param>
    ''' <param name="hGLRC">OpenGL-Context</param>
    ''' <returns>return true if successfull, false otherwise</returns>
    ''' <remarks>
    ''' note hGLRC is needed for later activating the DisplayContext
    ''' note this is used in frmAttitudeSphere (only)
    ''' </remarks>
    Public Function gl_InitializeWindow(ByRef PictureBox As System.Windows.Forms.PictureBox, ByRef hGLRC As Integer) As Boolean

        Dim pfd As Gdi.PIXELFORMATDESCRIPTOR
        Dim r As Integer ', Pos!(0 To 3)
        '
        'Set standard parameters
        '
        pfd.nSize = Len(pfd)
        pfd.nVersion = 1
        pfd.dwFlags = Gdi.PFD_SUPPORT_OPENGL Or Gdi.PFD_DRAW_TO_WINDOW Or Gdi.PFD_DOUBLEBUFFER Or Gdi.PFD_TYPE_RGBA
        pfd.iPixelType = Gdi.PFD_TYPE_RGBA
        pfd.cColorBits = 24
        pfd.cDepthBits = 24
        pfd.iLayerType = Gdi.PFD_MAIN_PLANE
        r = Gdi.ChoosePixelFormat(PictureBox.CreateGraphics.GetHdc, pfd)
        If r = 0 Then
            MsgBox("ChoosePixelFormat failed")
            Exit Function
        End If
        r = Gdi.SetPixelFormat(PictureBox.CreateGraphics.GetHdc, r, pfd)
        ' hGLRC = Wgl.wglCreateContext(PictureBox.CreateGraphics.GetHdc)
        ' Wgl.wglMakeCurrent(User.GetDC(PictureBox.Handle), hGLRC)
        'Wgl.wglMakeCurrent(ptr, hGLRC)
        Gl.glEnable(Gl.GL_DEPTH_TEST)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glLoadIdentity()
        Gl.glEnable(Gl.GL_CULL_FACE)
        Gl.glEnable(Gl.GL_POLYGON_SMOOTH)
        Gl.glShadeModel(Gl.GL_SMOOTH)
        Gl.glHint(Gl.GL_PERSPECTIVE_CORRECTION_HINT, Gl.GL_NICEST)

        Gl.glClearColor(0, 0, 0, 0)
        Gl.glEnable(Gl.GL_DEPTH_TEST)
        Gl.glEnable(Gl.GL_LIGHTING)

        Gl.glMatrixMode(Gl.GL_PROJECTION)
        Gl.glLoadIdentity()
        Gl.glViewport(0, 0, PictureBox.ClientRectangle.Width, PictureBox.ClientRectangle.Height)
        Glu.gluPerspective(30, PictureBox.ClientRectangle.Width / PictureBox.ClientRectangle.Height, 0.5, 300000)
        Glu.gluLookAt(0, 0, 20, 0, 0, 0, 0, 1, 0)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        gl_InitializeWindow = True

    End Function

    ''' <summary>
    ''' generates fonts for a specified PicureBox
    '''
    ''' This function generates fonts for a specified DisplayContext and activates
    ''' it so that all text drawn afterwards will be rendered in this font.
    ''' </summary>
    ''' <param name="PictureBox">the PictureBoxControl</param>
    ''' <param name="fontListBase1">GLDisplayListBaseNumber of a Arial type set</param>
    ''' <param name="fontListBase2">GLDisplayListBaseNumber of a underlined Arial type set</param>
    ''' <remarks>
    ''' note used in frmAttitudeSphere, frmMain and CSatelliteRenderer
    ''' sa gl_DrawText for drawing a text
    ''' </remarks>
    Public Sub gl_BuildFont2D(ByRef PictureBox As System.Windows.Forms.PictureBox, ByRef fontListBase1 As Short, ByRef fontListBase2 As Short)

        Dim font As Integer

        'Generate the GLDisplaylists
        fontListBase1 = Gl.glGenLists(256)
        fontListBase2 = Gl.glGenLists(256)
        font = Gdi.CreateFont(-11, 0, 0, 0, 400, 0, 0, 0, Gdi.ANSI_CHARSET, Gdi.CLIP_DEFAULT_PRECIS, Gdi.CLIP_DEFAULT_PRECIS, Gdi.DEFAULT_QUALITY, Gdi.DEFAULT_PITCH, "Verdana")
        Gdi.SelectObject(PictureBox.CreateGraphics.GetHdc, font)
        Wgl.wglUseFontBitmapsA(PictureBox.CreateGraphics.GetHdc, 0, 255, fontListBase1)
        '
        font = Gdi.CreateFont(-11, 0, 0, 0, 700, 0, 0, 0, Gdi.ANSI_CHARSET, Gdi.CLIP_DEFAULT_PRECIS, Gdi.CLIP_DEFAULT_PRECIS, Gdi.DEFAULT_QUALITY, Gdi.DEFAULT_PITCH, "Verdana")
        Gdi.SelectObject(PictureBox.CreateGraphics.GetHdc, font)
        Wgl.wglUseFontBitmapsA(PictureBox.CreateGraphics.GetHdc, 0, 255, fontListBase2)
        '
    End Sub

    ''' <summary>
    ''' Draw text into the current OpenGL display context
    '''
    ''' This function is used to draw text in currentlty active font
    ''' into the current OpenGL display context.
    ''' </summary>
    ''' <param name="text">the text to be rendered</param>
    ''' <param name="X">position x of the text</param>
    ''' <param name="Y">position y of the text</param>
    ''' <param name="Z">position z of the text</param>
    ''' <param name="fontListBase">basenumber of the desired font</param>
    ''' <remarks>
    ''' note textcolor can be specified by previous call of glColor
    ''' note used in frmAttitudeSphere only
    ''' sa gl_BuildFont2D for choosing a font for this text
    ''' </remarks>
    Public Sub gl_DrawText(ByRef text As String, ByRef X As Double, ByRef Y As Double, ByRef Z As Double, ByRef fontListBase As Short)
        Dim a() As Byte
        Dim i As Short

        Gl.glRasterPos3d(X, Y, Z)

        Gl.glListBase(fontListBase)
        ReDim a(Len(text))
        For i = 1 To Len(text)
            a(i - 1) = Asc(Mid(text, i, 1))
        Next
        a(i - 1) = 0
        Gl.glCallLists(Len(text), Gl.GL_UNSIGNED_BYTE, a)
    End Sub

    ''' <summary>
    ''' Draw colored text into the current OpenGL display
    '''
    ''' This function allows you to draw colored text into the current OpenGL display
    ''' </summary>
    ''' <param name="text">the text to be rendered</param>
    ''' <param name="X">position x of the text</param>
    ''' <param name="Y">position y of the text</param>
    ''' <param name="Z">position z of the text</param>
    ''' <param name="color">RGB-Value of text color</param>
    ''' <param name="fontListBase">basenumber of the desired font</param>
    ''' <remarks>
    ''' sa gl_BuildFont2D for fontListBase
    ''' </remarks>
    Public Sub gl_DrawTextColored(ByRef text As String, ByRef X As Double, ByRef Y As Double, ByRef Z As Double, ByRef color As Integer, ByVal fontListBase As Short)
        Dim a() As Byte
        Dim i As Short

        Gl.glDisable(Gl.GL_TEXTURE_2D)
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, 0)
        Gl.glColor3f(CShort(color And &HFF) / 255, CShort(color \ &H100 And &HFF) / 255, CShort(color \ &H10000 And &HFF) / 255)
        Gl.glRasterPos3d(X, Y, Z)

        Gl.glListBase(fontListBase)
        ReDim a(Len(text))
        For i = 1 To Len(text)
            a(i - 1) = Asc(Mid(text, i, 1))
        Next
        a(i - 1) = 0
        'For i = 0 To Len(text) - 1
        '    Gl.glCallLists(1, Gl.GL_UNSIGNED_BYTE, a(i))
        'Next
        Gl.glCallLists(Len(text), Gl.GL_UNSIGNED_BYTE, a)
        Gl.glEnable(Gl.GL_TEXTURE_2D)
    End Sub



    ''' <summary>
    ''' set active color to specified color
    '''
    ''' After calling this function, everything is rendered in the specified color.
    ''' </summary>
    ''' <param name="color">color in RGB-Long-Format</param>
    ''' <remarks></remarks>
    Public Sub gl_Color3f(ByRef color As Integer)
        Gl.glColor3f(CShort(color And &HFF) / 255, CShort(color \ &H100 And &HFF) / 255, CShort(color \ &H10000 And &HFF) / 255)
    End Sub



    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="width"></param>
    ''' <param name="height"></param>
    ''' <remarks></remarks>
    Public Sub resizeGLWindow(ByRef width As Single, ByRef height As Single)

        Gl.glPushMatrix()
        Gl.glMatrixMode(Gl.GL_PROJECTION)
        Gl.glLoadIdentity()
        Gl.glViewport(0, 0, width, height)
        Glu.gluPerspective(30, width / height, 0.5, 30000)
        Glu.gluLookAt(0, 0, 20, 0, 0, 0, 0, 1, 0)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glPopMatrix()

    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="filename"></param>
    ''' <param name="textureid"></param>
    ''' <remarks></remarks>
    Public Sub LoadTextureFromFile(ByRef filename As String, ByRef textureid As Integer)

        Dim bmp As Bitmap
        Dim data As BitmapData

        Gl.glGenTextures(1, textureid) 'G�n�re un n� de texture
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, textureid) 'S�lectionne ce n� de texture
        bmp = New Bitmap(filename)
        'Remplace le noir pur (0, 0, 0) par du transparent,
        'Utiliser des .bmp si vous voulez que le noir soit transparent, sinon utiliser de .jpg
        bmp.MakeTransparent(Color.Black)
        'Miroir vertial sur l'image
        bmp.RotateFlip(RotateFlipType.RotateNoneFlipY)
        data = bmp.LockBits(New Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
        Gl.glTexImage2D(Gl.GL_TEXTURE_2D, 0, Gl.GL_RGBA, data.Width, data.Height, 0, Gl.GL_BGRA, Gl.GL_UNSIGNED_BYTE, data.Scan0)
        bmp.UnlockBits(data)
        Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_MIN_FILTER, Gl.GL_LINEAR)
        Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_MAG_FILTER, Gl.GL_LINEAR)
    End Sub

End Module




