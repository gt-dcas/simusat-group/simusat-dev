Option Strict Off
Option Explicit On

Imports System.IO
Imports Tao.OpenGl
Imports Tao.Platform.Windows
Imports System.Math
Imports System.Threading

''' <summary>
''' ***************************************************************************
''' *  Program update in VB.NET - 17 june 2011                                *
''' *  Developpers : Benjamin and Florent - DUT computer science Internship   *
''' ***************************************************************************
''' </summary>
''' <remarks>Form main</remarks>
Friend Class frmMain
	Inherits System.Windows.Forms.Form
    '
    ''' <summary>
    ''' Forward
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Public Sub cmdFwd_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuPlay.Click
        '
        If Not simloop Then
            TickLast = GetTickCount
            simloop = True
            simulationloop()
        End If
    End Sub

    ''' <summary>
    ''' step to step
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub cmdOneBack_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOneBack.Click
        'Enable Timer
        simloop = False
        dblSimulationTime = dblSimulationTime - dblTimeStep

        Sim_update()
    End Sub
    ''' <summary>
    ''' step to step
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub cmdOneFwd_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOneFwd.Click
        'Enable Timer
        simloop = False
        dblSimulationTime = dblSimulationTime + dblTimeStep
        Sim_update()
    End Sub
    ''' <summary>
    ''' Pause
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Public Sub cmdPause_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdPause.Click
        simloop = False
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    ''' 
    Private Sub frmMain_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.GotFocus
        Sim_update()
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Form_Initialize_Renamed()
        'Needed for OpenGL Repaint
        blnSimulationInitialized = False
    End Sub
    ''' <summary>
    ''' Load the form AttitudeSphereSettings
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadAttSphere()

        Dim i As Short
        Dim sender As System.Object = 0
        Dim e As System.EventArgs = Nothing


        ' Sphere Attitude Settings
        With frmDisplay
            ' allowing everything for the axis of the principal and optimized direction
            For i = 0 To attsph.getAxisUbound
                .cmbPrinAxis.Items.Add(attsph.getAxis(i))
                .cmbOptAxis.Items.Add(attsph.getAxis(i))
            Next i
            ' allowing everything for the orientation of the principle and optimized direction
            For i = 0 To attsph.getOrientationUbound
                .cmbPrinOrient.Items.Add(attsph.getOrientation(i))
                .cmbOptOrient.Items.Add(attsph.getOrientation(i))
            Next i

            'set default values for the sphere attitude
            attsph.prinAxis = attsph.getAxis(5)
            ' .cmbPrinAxis_TextChanged(sender, e)
            attsph.prinOrient = attsph.getOrientation(0)
            '.cmbPrinOrient_TextChanged(sender, e)

            attsph.optAxis = attsph.getAxis(0)
            '.cmbOptAxis_TextChanged(sender, e)
            attsph.optOrient = attsph.getOrientation(3)
            '.cmbOptOrient_TextChanged(sender, e)

            'Update entries of the attitude comboboxes
            .cmbPrinAxis_SelectedIndexChanged(sender, e)
            .cmbPrinOrient_SelectedIndexChanged(sender, e)
            .cmbOptAxis_SelectedIndexChanged(sender, e)
            .cmbOptOrient_SelectedIndexChanged(sender, e)
        End With
        ''
    End Sub




    ''' <summary>
    ''' Load the form Main
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Public Sub frmMain_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        '
        Dim i As Integer
        Dim date_temp As udtDate
        '
        '
        '
        gl_initialize() 'on initialise une seule fois
        '
        '
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        '
        ' --------------------------
        ' Lecture du fichier �toiles
        ' --------------------------
        readStars("Data\Stars\star.txt")
        '
        ' ------------------------------------
        ' Set defaults for satellite and orbit
        ' ------------------------------------
        '
        Satellite = New CSatellite
        '
        ' Set default
        Call Satellite.construct("New_Sat", 65535, 0, 0, True)
        With Satellite
            .satBodyHeight = 1
            .satBodyWidth = 1
            .primaryAxis = "-z"
            .primaryDir = "nadir"
            .secondaryAxis = "x"
            .secondaryDir = "velocity"
        End With
        '
        With SatOrbit0
            .SemiMajor_Axis = 8000
            .Eccentricity = 0
            .Inclination = 0
            .Argument_Perigee = 0
            .RAAN = 0
            .Mean_Anomaly = 0
            .epoch = 2451544.5 ' january 1st 2000 0h
            .dec1 = 0
            .dec2 = 0
            .bstar = 0
        End With
        '
        ' Set attitude sphere
        attsph = New CAttitudeSphere
        LoadAttSphere()
        '
        '
        ' ------------------------------------------
        ' Default values (Attitude Control Subsystem)
        ' ------------------------------------------
        '
        ' Centre of mass for the whole satellite
        SatCG.X = 0 : SatCG.Y = 0 : SatCG.Z = 0
        '
        ' Centre of mass for the basic satellite
        BodyCG.X = 0 : BodyCG.Y = 0 : BodyCG.Z = 0
        ' 
        ' Gravity gradient stabilisation
        GravityMass = 0
        GravityDist.X = 0
        GravityDist.Y = 0
        GravityDist.Z = 0
        '
        ' Damper properties
        DampersMass(0) = 0 : DampersMass(1) = 0 : DampersMass(2) = 0
        zd(0) = 0 : zd(1) = 0 : zd(2) = 0
        zdp(0) = 0 : zdp(1) = 0 : zdp(2) = 0
        zdpp(0) = 0 : zdpp(1) = 0 : zdpp(2) = 0
        DampersDist(0) = 0 : DampersDist(1) = 0 : DampersDist(2) = 0
        c(0) = 0 : c(1) = 0 : c(2) = 0
        kd(0) = 0 : kd(1) = 0 : kd(2) = 0
        '
        ' Wheels properties
        JR(0) = 0 : JR(1) = 0 : JR(2) = 0
        om(0) = 0 : om(1) = 0 : om(2) = 0
        OMp(0) = 0 : OMp(1) = 0 : OMp(2) = 0
        OMin(0) = 0 : OMin(1) = 0 : OMin(2) = 0
        OMin0(0) = 0 : OMin0(1) = 0 : OMin0(1) = 0
        '
        ' Vectors normal to the six surfaces of the satellite
        ' 0 : face +x
        ' 1 : face +y
        ' 2 : face +z
        ' 3 : face -x
        ' 4 : face -y
        ' 5 : face -z
        For i = 0 To 5
            vect_face(i).X = 0 : vect_face(i).Y = 0 : vect_face(i).Z = 0
        Next
        vect_face(0).X = 1 : vect_face(1).Y = 1 : vect_face(2).Z = 1
        vect_face(3).X = -1 : vect_face(4).Y = -1 : vect_face(5).Z = -1
        '
        ' Initialisation of the inertial vector and the appropriate start-vector
        For i = 0 To 5
            yi0(i) = 0
            yi(i) = 0
        Next

        '
        ' Mass moment matrix of satellite
        'SatInertia(0, 0) = 1 : SatInertia(0, 1) = 0 : SatInertia(0, 2) = 0
        'SatInertia(1, 0) = 0 : SatInertia(1, 1) = 1 : SatInertia(1, 2) = 0
        'SatInertia(2, 0) = 0 : SatInertia(2, 1) = 0 : SatInertia(2, 2) = 1
        '
        ' Mass moment matrix of basic satellite
        BodyInertia(0, 0) = 1 : BodyInertia(0, 1) = 0 : BodyInertia(0, 2) = 0
        BodyInertia(1, 0) = 0 : BodyInertia(1, 1) = 1 : BodyInertia(1, 2) = 0
        BodyInertia(2, 0) = 0 : BodyInertia(2, 1) = 0 : BodyInertia(2, 2) = 1
        '
        ' constant properties of satellite
        SatMass = 50
        BodyMass = 50
        '
        ' Coefficients of reflexion of light
        Cr_solar(0) = 0.9 : Cr_solar(1) = 0.5 : Cr_solar(2) = 0.9
        Cr_solar(3) = 0.81 : Cr_solar(4) = 0.45 : Cr_solar(5) = 0.81
        '
        ' Coefficients of diffusion of light
        Cd_solar(0) = 0.05 : Cd_solar(1) = 0.2 : Cd_solar(2) = 0.05
        Cd_solar(3) = 0.04 : Cd_solar(4) = 0.18 : Cd_solar(5) = 0.04
        '
        ' Coefficients of aerodynamic pressure normal to the surface
        For i = 0 To 2
            Cn_aero(i) = 0.9
        Next
        For i = 3 To 5
            Cn_aero(i) = 0.81
        Next
        '
        ' coefficients of aerodynamic pressure tengential to the surface
        Ct_aero(0) = 0.85 : Ct_aero(1) = 0.3 : Ct_aero(2) = 0.85
        Ct_aero(3) = 0.77 : Ct_aero(4) = 0.27 : Ct_aero(5) = 0.77
        '
        ' magnetic dipole of the satellite
        dipole_sat.X = Math.Sqrt(3) : dipole_sat.Y = Math.Sqrt(3) : dipole_sat.Z = Math.Sqrt(3)
        dipsat_passive.X = Math.Sqrt(3) : dipsat_passive.Y = Math.Sqrt(3) : dipsat_passive.Z = Math.Sqrt(3)
        dipsat_active.X = 0 : dipsat_active.Y = 0 : dipsat_active.Z = 0
        dipsat_control.X = 0 : dipsat_control.Y = 0 : dipsat_control.Z = 0
        ' Gas jets
        jet(0) = 0 : jet(1) = 0 : jet(2) = 0
        jet_control(0) = 0 : jet_control(1) = 0 : jet_control(2) = 0
        '
        '
        ' Init values for radiation
        ' -------------------------
        SunPow = 3.8E+26
        For i = 0 To 11
            Albedo_month(i) = 0.38
            IR_month(i) = 237
        Next i
        '

        ' 
        ' All the Initializing is done
        Me.Show()
        cmdStop_Click(cmdStop, New System.EventArgs())
        View_Change() ' Select 2D view 
        blnSimulationInitialized = True
        displayOpened = False
        '
        dblTimeStep = 60 / secday
        updwTimeStep.SelectedIndex = 5
        '
        dblSimulationStart = SatOrbit0.epoch
        convert_Julian_Gregorian(dblSimulationStart, date_temp)
        With date_temp
            txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        dblSimulationTime = dblSimulationStart
        Sim_update()
        ''
    End Sub


    ''' <summary>
    ''' Load star file
    ''' </summary>
    ''' <param name="sPath">Path containing the star file</param>
    ''' <remarks></remarks>
    Public Sub readStars(ByVal sPath As String)

        Dim compteur_stars As Integer = 0
        Dim streamStars As New System.IO.StreamReader(sPath)
        Dim i As Integer = 0
        If System.IO.File.Exists(sPath) Then
            While Not streamStars.EndOfStream
                Dim sLine As String
                sLine = streamStars.ReadLine()
                Dim uneStar As New CStar
                uneStar.initStarFromLine(sLine)
                stars(compteur_stars) = uneStar
                compteur_stars = compteur_stars + 1
                i = i + 1
            End While
        End If

    End Sub

   
    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '
        Dim answer As Short
        '
        answer = MsgBox("You will loose your current data file." & vbCrLf & "Do you want to save it first?", MsgBoxStyle.YesNoCancel Or MsgBoxStyle.Exclamation, "Warning!")
        If answer = MsgBoxResult.Yes Then
            If dlgMainSave.ShowDialog = Windows.Forms.DialogResult.OK Then
                save_att_conf(dlgMainSave.FileName)
            End If
            End
        ElseIf answer = MsgBoxResult.Cancel Then
            e.Cancel = True
            Exit Sub
        Else
            End
        End If
        ''
    End Sub       


    ''' <summary>
    ''' Check the view
    ''' </summary>
    ''' <remarks></remarks>
    ''' 

    Private Sub View_Change() ' ByVal eventArgs As System.EventArgs
        If allow_event Then
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
            If Me.optView_0.Checked Then  ' si vue 2D

                mnu2DView.Checked = True
                mnu3DView.Checked = False
                mnuAttitudeSphere.Checked = False
                '
                frmDisplay.chkSphere.Enabled = False
                frmDisplay.chkDayNight.Enabled = True
                frmDisplay.lblSphereOrient.Enabled = False
                frmDisplay.cmbOptAxis.Enabled = False
                frmDisplay.cmbOptOrient.Enabled = False
                frmDisplay.cmbPrinOrient.Enabled = False
                frmDisplay.cmbPrinAxis.Enabled = False
                '
                selectedView = VIEW_2D
                '
            ElseIf Me.optView_1.Checked Then 'vue 3D espace 
                mnu2DView.Checked = False
                mnu3DView.Checked = True
                mnuAttitudeSphere.Checked = False
                '
                frmDisplay.chkSphere.Enabled = False
                frmDisplay.chkDayNight.Enabled = True
                frmDisplay.lblSphereOrient.Enabled = False
                frmDisplay.cmbOptAxis.Enabled = False
                frmDisplay.cmbOptOrient.Enabled = False
                frmDisplay.cmbPrinOrient.Enabled = False
                frmDisplay.cmbPrinAxis.Enabled = False
                '
                selectedView = VIEW_3D
                '
            ElseIf Me.optView_3.Checked Then 'vue 3D satellite
                mnu2DView.Checked = False
                mnu3DView.Checked = False
                mnuAttitudeSphere.Checked = True
                '
                frmDisplay.chkSphere.Enabled = True
                frmDisplay.chkDayNight.Enabled = False
                frmDisplay.lblSphereOrient.Enabled = True
                frmDisplay.cmbOptAxis.Enabled = True
                frmDisplay.cmbOptOrient.Enabled = True
                frmDisplay.cmbPrinOrient.Enabled = True
                frmDisplay.cmbPrinAxis.Enabled = True
                '
                selectedView = VIEW_ATTITUDE
                '
            End If
            '
            Sim_update()
            resizeGLWindow(frmDisplay.picMapView.Width, frmDisplay.picMapView.Height)
            Sim_update()
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            Sim_update()
        End If
    End Sub


    ''' <summary>
    ''' Update the simulation
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Sim_update()
        '
        ' Allows only one call to Sim_Update
        Static blnUpdateActive As Boolean
        '
        If blnUpdateActive Then Exit Sub
        blnUpdateActive = True
        '
        ' ------------
        ' Orbitography
        ' ------------
        update_Date(dblSimulationTime)
        update_Sun(dblSimulationTime, SunPos)
        update_Moon(dblSimulationTime, MoonPos)
        '
        If Not SatOrbit.Name Is Nothing Then
            SatOrbit = Satellite.getOrbit(dblSimulationTime) ' Gives orbit parameters, SatPos, SatVel, SatGeoPos
            blnEclipse = Eclipse(SatPos, SunPos) ' Eclipses
        End If
        '
        ' ---------------
        ' Graphic display
        ' ---------------
        If displayOpened Then
            Select Case selectedView
                Case VIEW_ATTITUDE
                    attsph.gl_Paint()
                Case VIEW_2D
                    frmDisplay.gl_draw_2D()
                Case VIEW_3D
                    frmDisplay.gl_draw_3D()
            End Select
        End If
        '
        '
        blnUpdateActive = False
        ''
    End Sub

    ''' <summary>
    ''' Refresh simulation
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub simulationloop()
        '
        cmdStop.Focus()
        '
        While simloop
            TickNow = GetTickCount
            ticks = TickNow - TickLast
            dblSimulationTime = dblSimulationTime + dblTimeStep * ticks / 1000
            Sim_update()
            TickLast = TickNow
            System.Windows.Forms.Application.DoEvents()
        End While
    End Sub

    Private Sub frmMain_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        frmFirst.Hide()
    End Sub



    Private Sub updwTimeStep_SelectedItemChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x$
        x$ = updwTimeStep.Text
        x$ = Microsoft.VisualBasic.Left(x$, Len(x$) - 2)
        dblTimeStep = CDbl(x$) / secday
    End Sub


    '
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Y"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function extractProjectName(ByRef Y As String) As String
        '
        Dim X As String
        Dim i As Short
        '
        X = Trim(Y)
        If X = "" Then extractProjectName = "" : Exit Function
        i = 1
        Do  ' remove path
            If Mid(X, i, 1) = "\" Then
                X = Microsoft.VisualBasic.Right(X, Len(X) - i)
                i = 1
            Else
                i = i + 1
            End If
        Loop While i < Len(X)
        '
        i = Len(X)
        Do  ' remove file extension
            If Mid(X, i, 1) = "." Then
                X = Microsoft.VisualBasic.Left(X, i - 1)
            Else
                i = i - 1
            End If
        Loop While i > 1
        '
        extractProjectName = X
        ''
    End Function


    Private Sub cmdOrbit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOrbit.Click
        frmOrbit.tabOrbit.SelectedIndex = 0
        frmOrbit.Show()
    End Sub

    Private Sub cmdEnvir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEnvir.Click
        frmOrbit.tabOrbit.SelectedIndex = 1
        frmOrbit.Show()
    End Sub

    Private Sub optView_0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optView_0.Click
        frmDisplay.Show()
        View_Change()
    End Sub

    Private Sub optView_1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optView_1.Click
        frmDisplay.Show()
        View_Change()
    End Sub

    Private Sub optView_3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optView_3.Click
        frmDisplay.Show()
        View_Change()
    End Sub

    Private Sub cmdMassGeo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdMassGeo.Click
        frmAttConf.tabPower.SelectedIndex = 0
        frmAttConf.Show()
    End Sub

    Private Sub cmdProperties_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdProperties.Click
        frmAttConf.tabPower.SelectedIndex = 1
        frmAttConf.Show()
    End Sub


    Private Sub cmdActuactors_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdActuactors.Click
        frmAttConf.tabPower.SelectedIndex = 2
        frmAttConf.Show()
    End Sub

    Private Sub cmdGest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGest.Click
        frmAttConf.tabPower.SelectedIndex = 3
        frmAttConf.Show()
    End Sub

    Private Sub cmdQuit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdQuit.Click
        '
        Dim answer As Short
        '
        answer = MsgBox("You will loose your current data file." & vbCrLf & "Do you want to save it first?", MsgBoxStyle.YesNoCancel Or MsgBoxStyle.Exclamation, "Warning!")
        If answer = MsgBoxResult.Yes Then
            If dlgMainSave.ShowDialog = Windows.Forms.DialogResult.OK Then
                save_att_conf(dlgMainSave.FileName)
            End If
            End
        ElseIf answer = MsgBoxResult.Cancel Then
            Exit Sub
        Else
            End
        End If
    End Sub

    '
    Private Sub cmdOpen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOpen.Click
        '
        If dlgMainOpen.ShowDialog = Windows.Forms.DialogResult.Cancel Then Exit Sub
        load_att_conf(dlgMainOpen.FileName)
        frmAttConf.update_attitude_conf()
        ''
    End Sub

    Private Sub txt_date_debut_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_date_debut.Leave
        '
        Dim date_temp As udtDate
        '
        ' Calcul du jour julien de d�but de simulation
        With date_temp
            .year = Year(txt_date_debut.Value)
            .month = Month(txt_date_debut.Value)
            .day = Microsoft.VisualBasic.DateAndTime.Day(txt_date_debut.Value)
            .hour = Hour(txt_date_debut.Value)
            .minute = Minute(txt_date_debut.Value)
            .second = Second(txt_date_debut.Value)
        End With
        dblSimulationStart = convert_Gregorian_Julian(date_temp)
        '
        dblSimulationTime = dblSimulationStart
        update_Date((dblSimulationTime))
        ''
    End Sub


    Private Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        '
        If dlgMainSave.ShowDialog = Windows.Forms.DialogResult.OK Then
            save_att_conf(dlgMainSave.FileName)
        End If
    End Sub


    ''' <summary>
    ''' Initialize OpenGL
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub gl_initialize()
        '
        ' Initialisation OPENGL
        ' --------------
        m_intptrHdc = User.GetDC(frmDisplay.picMapView.Handle)
        '
        setupPixelFormat(frmDisplay.picMapView.CreateGraphics.GetHdc)
        hGLRC = Wgl.wglCreateContext(m_intptrHdc)
        Wgl.wglMakeCurrent(m_intptrHdc, hGLRC)
        '
        ' Param�trage
        ' -----------
        Gl.glEnable(Gl.GL_DEPTH_TEST) ' Teste les faces cach�es
        Gl.glEnable(Gl.GL_TEXTURE_2D)
        Gl.glMatrixMode(Gl.GL_PROJECTION) ' Matrice transfo 3D � 2D
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glEnable(Gl.GL_BLEND)
        Gl.glBlendFunc(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA)
        Gl.glEnable(Gl.GL_LINE_SMOOTH)
        '
        ' G�n�ration des textures
        ' -----------------------
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Maps\earth_natural_2048.jpg", earthtexture) ' terre 3D
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\soleil.bmp", tx_soleil) ' Soleil 2D
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\sat_jour.bmp", sat_jour) ' Sat_jour
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\sat_nuit.bmp", sat_nuit) ' Sat en �clipse
        Gl.glClearColor(0, 0, 0.2, 0) ' met une transparence en fond la couleur de fond
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\lune.bmp", tx_lune) ' Lune vue 2D
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\moon.jpg", tx_moon) ' Texture lune vue 3D
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\halo.jpg", tx_halo) ' Texture etoile proche
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\soleil_3D.jpg", tx_soleil3D) ' Soleil 3D
        '
        ' -------------
        ' Listes openGL
        ' -------------
        '
        ' Sphere with n% facettes
        Dim lt0, lg0, n As Short

        Gl.glNewList(glSphere, Gl.GL_COMPILE)
        n = 200
        For lg0 = 0 To n - 1
            For lt0 = -n / 4 To n / 4 - 1
                Gl.glBegin((Gl.GL_QUADS))
                Gl.glNormal3f(Cos(lt0 * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin(lt0 * pi / n * 2))
                Gl.glTexCoord2f(lg0 / n, 0.5 + lt0 / n * 2)
                Gl.glVertex3f(Cos(lt0 * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin(lt0 * pi / n * 2))
                Gl.glNormal3f(Cos((lt0 + 1) * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                Gl.glTexCoord2f(lg0 / n, 0.5 + (lt0 + 1) / n * 2)
                Gl.glVertex3f(Cos((lt0 + 1) * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                Gl.glNormal3f(Cos((lt0 + 1) * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                Gl.glTexCoord2f((lg0 + 1) / n, 0.5 + (lt0 + 1) / n * 2)
                Gl.glVertex3f(Cos((lt0 + 1) * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                Gl.glNormal3f(Cos(lt0 * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin(lt0 * pi / n * 2))
                Gl.glTexCoord2f((lg0 + 1) / n, 0.5 + lt0 / n * 2)
                Gl.glVertex3f(Cos(lt0 * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin(lt0 * pi / n * 2))
                Gl.glEnd()
            Next
        Next

        Gl.glEndList()
        Dim r As Double

        Gl.glNewList(glStation, Gl.GL_COMPILE)
        n = 8
        r = 0.02
        For lg0 = 0 To n - 1
            For lt0 = 0 To n / 4 - 1
                Gl.glBegin((Gl.GL_QUADS))
                Gl.glNormal3f(Cos(lt0 * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin(lt0 * pi / n * 2))
                Gl.glTexCoord2f(lg0 / n, 0.5 + lt0 / n * 2)
                Gl.glVertex3f(r * Cos(lt0 * pi / n * 2) * Cos(lg0 * pi / n * 2), r * Cos(lt0 * pi / n * 2) * Sin(lg0 * pi / n * 2), r * Sin(lt0 * pi / n * 2))
                Gl.glNormal3f(Cos((lt0 + 1) * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                Gl.glTexCoord2f(lg0 / n, 0.5 + (lt0 + 1) / n * 2)
                Gl.glVertex3f(r * Cos((lt0 + 1) * pi / n * 2) * Cos(lg0 * pi / n * 2), r * Cos((lt0 + 1) * pi / n * 2) * Sin(lg0 * pi / n * 2), r * Sin((lt0 + 1) * pi / n * 2))
                Gl.glNormal3f(Cos((lt0 + 1) * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                Gl.glTexCoord2f((lg0 + 1) / n, 0.5 + (lt0 + 1) / n * 2)
                Gl.glVertex3f(r * Cos((lt0 + 1) * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), r * Cos((lt0 + 1) * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), r * Sin((lt0 + 1) * pi / n * 2))
                Gl.glNormal3f(Cos(lt0 * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin(lt0 * pi / n * 2))
                Gl.glTexCoord2f((lg0 + 1) / n, 0.5 + lt0 / n * 2)
                Gl.glVertex3f(r * Cos(lt0 * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), r * Cos(lt0 * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), r * Sin(lt0 * pi / n * 2))
                Gl.glEnd()
            Next
        Next

        Gl.glEndList()

        ' Fonts
        ' -----
        gl_BuildFont2D(frmDisplay.picMapView, font1, font2)
        '
        ' Other initialisations
        ' ---------------------
        Gl.glClearColor(0, 0, 0, 0) ' D�finit la couleur de fond
        distance_oeil = 5
        psi = pi / 2
        phi = 0
        OeilX = distance_oeil * Cos(phi) * Cos(psi)
        OeilY = distance_oeil * Sin(phi)
        OeilZ = distance_oeil * Cos(phi) * Sin(psi)
        ''
    End Sub

    Private Sub cmdReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        frmReport.Show()
    End Sub


    Private Sub cmdStop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStop.Click
        '
        simloop = False
        '
        dblSimulationTime = dblSimulationStart
        Sim_update()
    End Sub


    Private Sub cmdCurves_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCurves.Click
        frmGraph.Show()
    End Sub


    Private Sub cmdFwd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFwd.Click
        '
        If Not simloop Then
            TickLast = GetTickCount
            simloop = True
            simulationloop()
        End If
    End Sub

    Private Sub updwTimeStep_SelectedItemChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles updwTimeStep.SelectedItemChanged
        Dim x$
        x$ = updwTimeStep.Text
        x$ = Microsoft.VisualBasic.Left(x$, Len(x$) - 2)
        dblTimeStep = CDbl(x$) / secday
    End Sub


End Class