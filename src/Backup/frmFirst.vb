Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmFirst
	Inherits System.Windows.Forms.Form
	
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="buffer"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Private Function Cstr2Bstr(ByRef buffer As String) As String
		Dim Length As Integer
        Length = InStr(1, buffer, Chr(0))
        'experimental
        Cstr2Bstr = ""
		If Length > 0 Then
			Cstr2Bstr = VB.Left(buffer, Length - 1)
		End If
	End Function
	
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
	Public Sub frmFirst_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        'disable events
        allow_event = False

		' Show the window
		Show()
        Refresh()
		Activate()
		'
		' Get the decimal separator
		decimalSeparator = "."
        Dim buffer As String = ""
        If 0 <> GetLocaleInfo(GetSystemDefaultLCID, LOCALE_SDECIMAL, buffer, 4) Then
            decimalSeparator = Cstr2Bstr(buffer)
        End If
		'
		'
		'
		' Loading Textures
		ShowProgress((10))
        lblLoading.Text = "Loading in progress"
        Refresh()
		'
		blnIsNewPrj = True
        frmMain.Show()

        'enable events
        allow_event = True
        'Close()
		'
	End Sub
	
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="i"></param>
    ''' <remarks></remarks>
	Public Sub ShowProgress(ByRef i As Short)
		'
		ProgressBar.Value = i
		'
	End Sub
	
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Function AppPath() As Object
		Dim X As String
		X = My.Application.Info.DirectoryPath
		If VB.Right(X, 1) <> "\" Then X = X & "\"
		AppPath = UCase(X)
	End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub zimg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles zimg.Click

    End Sub
End Class