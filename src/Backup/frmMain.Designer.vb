<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMain
#Region "Code g�n�r� par le Concepteur Windows Form "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Cet appel est requis par le Concepteur Windows Form.
		InitializeComponent()
		Form_Initialize_renamed()
	End Sub
	'Form remplace la m�thode Dispose pour nettoyer la liste des composants.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requise par le Concepteur Windows Form
	Private components As System.ComponentModel.IContainer
    Public WithEvents mnuOpenProject As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileLine1 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuSaveAs As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileLine3 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuQuit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSatellite As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnu_MountPanels As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnu2DView As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnu3DView As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuAttitudeSphere As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSimulationLine1 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuIncrease As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuDecrease As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuSimulationLine2 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuPlay As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuPause As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuStop As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSimulationLine4 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuOneStepFwd As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuOneStepBack As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSimulationLine3 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents mnuSimulation As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    ' vue 3D satellite
    ' vue 2D planisphere
    ' vue 3D espace
    Public dlgMainOpen As System.Windows.Forms.OpenFileDialog
    Public dlgMainSave As System.Windows.Forms.SaveFileDialog
    'REMARQUE�: la proc�dure suivante est requise par le Concepteur Windows Form
    'Elle peut �tre modifi�e � l'aide du Concepteur Windows Form.
    'Ne la modifiez pas � l'aide de l'�diteur de code.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuOpenProject = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileLine1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuSaveAs = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileLine3 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuQuit = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSatellite = New System.Windows.Forms.ToolStripMenuItem
        Me.mnu_MountPanels = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSimulation = New System.Windows.Forms.ToolStripMenuItem
        Me.mnu2DView = New System.Windows.Forms.ToolStripMenuItem
        Me.mnu3DView = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAttitudeSphere = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSimulationLine1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuIncrease = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDecrease = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSimulationLine2 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuPlay = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPause = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuStop = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSimulationLine4 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuOneStepFwd = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuOneStepBack = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSimulationLine3 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem
        Me.dlgMainOpen = New System.Windows.Forms.OpenFileDialog
        Me.dlgMainSave = New System.Windows.Forms.SaveFileDialog
        Me.cmdCells = New System.Windows.Forms.RadioButton
        Me.cmdActuactors = New System.Windows.Forms.RadioButton
        Me.cmd = New System.Windows.Forms.RadioButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.zlbl_8 = New System.Windows.Forms.Label
        Me.zlbl_11 = New System.Windows.Forms.Label
        Me.zlbl_10 = New System.Windows.Forms.Label
        Me.zlbl_7 = New System.Windows.Forms.Label
        Me.cmdGest = New System.Windows.Forms.RadioButton
        Me.cmdAct = New System.Windows.Forms.RadioButton
        Me.cmdMassGeo = New System.Windows.Forms.RadioButton
        Me.cmdProperties = New System.Windows.Forms.RadioButton
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.zlbl_15 = New System.Windows.Forms.Label
        Me.zlbl_14 = New System.Windows.Forms.Label
        Me.cmdCurves = New System.Windows.Forms.RadioButton
        Me.cmdReport = New System.Windows.Forms.RadioButton
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.zlbl_13 = New System.Windows.Forms.Label
        Me.zlbl_12 = New System.Windows.Forms.Label
        Me.cmdOrbit = New System.Windows.Forms.RadioButton
        Me.cmdEnvir = New System.Windows.Forms.RadioButton
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.zlb_18 = New System.Windows.Forms.Label
        Me.zlbl_17 = New System.Windows.Forms.Label
        Me.zlbl_16 = New System.Windows.Forms.Label
        Me.cmdQuit = New System.Windows.Forms.RadioButton
        Me.cmdOpen = New System.Windows.Forms.RadioButton
        Me.cmdSave = New System.Windows.Forms.RadioButton
        Me.fraDisplay = New System.Windows.Forms.GroupBox
        Me.zlbl_6 = New System.Windows.Forms.Label
        Me.zlbl_5 = New System.Windows.Forms.Label
        Me.zlbl_4 = New System.Windows.Forms.Label
        Me.optView_3 = New System.Windows.Forms.RadioButton
        Me.optView_0 = New System.Windows.Forms.RadioButton
        Me.optView_1 = New System.Windows.Forms.RadioButton
        Me.fraSimulation = New System.Windows.Forms.GroupBox
        Me.updwTimeStep = New System.Windows.Forms.DomainUpDown
        Me.lblDate = New System.Windows.Forms.Label
        Me.zlbl_1 = New System.Windows.Forms.Label
        Me.txt_date_debut = New System.Windows.Forms.DateTimePicker
        Me.zlbl_3 = New System.Windows.Forms.Label
        Me.zlbl_0 = New System.Windows.Forms.Label
        Me.cmdOneFwd = New System.Windows.Forms.Button
        Me.cmdFwd = New System.Windows.Forms.Button
        Me.cmdOneBack = New System.Windows.Forms.Button
        Me.cmdStop = New System.Windows.Forms.Button
        Me.cmdPause = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.fraDisplay.SuspendLayout()
        Me.fraSimulation.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuOpenProject, Me.mnuFileLine1, Me.mnuSaveAs, Me.mnuFileLine3, Me.mnuQuit})
        Me.mnuFile.Name = "mnuFile"
        resources.ApplyResources(Me.mnuFile, "mnuFile")
        '
        'mnuOpenProject
        '
        Me.mnuOpenProject.Name = "mnuOpenProject"
        resources.ApplyResources(Me.mnuOpenProject, "mnuOpenProject")
        '
        'mnuFileLine1
        '
        Me.mnuFileLine1.Name = "mnuFileLine1"
        resources.ApplyResources(Me.mnuFileLine1, "mnuFileLine1")
        '
        'mnuSaveAs
        '
        Me.mnuSaveAs.Name = "mnuSaveAs"
        resources.ApplyResources(Me.mnuSaveAs, "mnuSaveAs")
        '
        'mnuFileLine3
        '
        Me.mnuFileLine3.Name = "mnuFileLine3"
        resources.ApplyResources(Me.mnuFileLine3, "mnuFileLine3")
        '
        'mnuQuit
        '
        Me.mnuQuit.Name = "mnuQuit"
        resources.ApplyResources(Me.mnuQuit, "mnuQuit")
        '
        'mnuSatellite
        '
        Me.mnuSatellite.Name = "mnuSatellite"
        resources.ApplyResources(Me.mnuSatellite, "mnuSatellite")
        '
        'mnu_MountPanels
        '
        Me.mnu_MountPanels.Name = "mnu_MountPanels"
        resources.ApplyResources(Me.mnu_MountPanels, "mnu_MountPanels")
        '
        'mnuSimulation
        '
        Me.mnuSimulation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnu2DView, Me.mnu3DView, Me.mnuAttitudeSphere, Me.mnuSimulationLine1, Me.mnuIncrease, Me.mnuDecrease, Me.mnuSimulationLine2, Me.mnuPlay, Me.mnuPause, Me.mnuStop, Me.mnuSimulationLine4, Me.mnuOneStepFwd, Me.mnuOneStepBack, Me.mnuSimulationLine3})
        Me.mnuSimulation.Name = "mnuSimulation"
        resources.ApplyResources(Me.mnuSimulation, "mnuSimulation")
        '
        'mnu2DView
        '
        Me.mnu2DView.Name = "mnu2DView"
        resources.ApplyResources(Me.mnu2DView, "mnu2DView")
        '
        'mnu3DView
        '
        Me.mnu3DView.Name = "mnu3DView"
        resources.ApplyResources(Me.mnu3DView, "mnu3DView")
        '
        'mnuAttitudeSphere
        '
        Me.mnuAttitudeSphere.Name = "mnuAttitudeSphere"
        resources.ApplyResources(Me.mnuAttitudeSphere, "mnuAttitudeSphere")
        '
        'mnuSimulationLine1
        '
        Me.mnuSimulationLine1.Name = "mnuSimulationLine1"
        resources.ApplyResources(Me.mnuSimulationLine1, "mnuSimulationLine1")
        '
        'mnuIncrease
        '
        Me.mnuIncrease.Name = "mnuIncrease"
        resources.ApplyResources(Me.mnuIncrease, "mnuIncrease")
        '
        'mnuDecrease
        '
        Me.mnuDecrease.Name = "mnuDecrease"
        resources.ApplyResources(Me.mnuDecrease, "mnuDecrease")
        '
        'mnuSimulationLine2
        '
        Me.mnuSimulationLine2.Name = "mnuSimulationLine2"
        resources.ApplyResources(Me.mnuSimulationLine2, "mnuSimulationLine2")
        '
        'mnuPlay
        '
        Me.mnuPlay.Name = "mnuPlay"
        resources.ApplyResources(Me.mnuPlay, "mnuPlay")
        '
        'mnuPause
        '
        Me.mnuPause.Name = "mnuPause"
        resources.ApplyResources(Me.mnuPause, "mnuPause")
        '
        'mnuStop
        '
        Me.mnuStop.Name = "mnuStop"
        resources.ApplyResources(Me.mnuStop, "mnuStop")
        '
        'mnuSimulationLine4
        '
        Me.mnuSimulationLine4.Name = "mnuSimulationLine4"
        resources.ApplyResources(Me.mnuSimulationLine4, "mnuSimulationLine4")
        '
        'mnuOneStepFwd
        '
        Me.mnuOneStepFwd.Name = "mnuOneStepFwd"
        resources.ApplyResources(Me.mnuOneStepFwd, "mnuOneStepFwd")
        '
        'mnuOneStepBack
        '
        Me.mnuOneStepBack.Name = "mnuOneStepBack"
        resources.ApplyResources(Me.mnuOneStepBack, "mnuOneStepBack")
        '
        'mnuSimulationLine3
        '
        Me.mnuSimulationLine3.Name = "mnuSimulationLine3"
        resources.ApplyResources(Me.mnuSimulationLine3, "mnuSimulationLine3")
        '
        'mnuHelp
        '
        Me.mnuHelp.Name = "mnuHelp"
        resources.ApplyResources(Me.mnuHelp, "mnuHelp")
        '
        'dlgMainOpen
        '
        resources.ApplyResources(Me.dlgMainOpen, "dlgMainOpen")
        '
        'dlgMainSave
        '
        resources.ApplyResources(Me.dlgMainSave, "dlgMainSave")
        '
        'cmdCells
        '
        resources.ApplyResources(Me.cmdCells, "cmdCells")
        Me.cmdCells.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCells.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCells.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCells.Name = "cmdCells"
        Me.cmdCells.UseVisualStyleBackColor = False
        '
        'cmdActuactors
        '
        resources.ApplyResources(Me.cmdActuactors, "cmdActuactors")
        Me.cmdActuactors.BackColor = System.Drawing.SystemColors.Control
        Me.cmdActuactors.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdActuactors.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdActuactors.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdActuactors.Name = "cmdActuactors"
        Me.cmdActuactors.UseVisualStyleBackColor = False
        '
        'cmd
        '
        Me.cmd.BackColor = System.Drawing.Color.Red
        Me.cmd.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmd.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmd.ForeColor = System.Drawing.SystemColors.ControlText
        resources.ApplyResources(Me.cmd, "cmd")
        Me.cmd.MaximumSize = New System.Drawing.Size(200, 200)
        Me.cmd.Name = "cmd"
        Me.cmd.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.zlbl_8)
        Me.GroupBox1.Controls.Add(Me.zlbl_11)
        Me.GroupBox1.Controls.Add(Me.zlbl_10)
        Me.GroupBox1.Controls.Add(Me.zlbl_7)
        Me.GroupBox1.Controls.Add(Me.cmdGest)
        Me.GroupBox1.Controls.Add(Me.cmdAct)
        Me.GroupBox1.Controls.Add(Me.cmdMassGeo)
        Me.GroupBox1.Controls.Add(Me.cmdProperties)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.ForeColor = System.Drawing.Color.Brown
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'zlbl_8
        '
        resources.ApplyResources(Me.zlbl_8, "zlbl_8")
        Me.zlbl_8.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_8.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_8.Name = "zlbl_8"
        '
        'zlbl_11
        '
        resources.ApplyResources(Me.zlbl_11, "zlbl_11")
        Me.zlbl_11.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_11.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_11.Name = "zlbl_11"
        '
        'zlbl_10
        '
        resources.ApplyResources(Me.zlbl_10, "zlbl_10")
        Me.zlbl_10.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_10.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_10.Name = "zlbl_10"
        '
        'zlbl_7
        '
        resources.ApplyResources(Me.zlbl_7, "zlbl_7")
        Me.zlbl_7.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_7.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_7.Name = "zlbl_7"
        '
        'cmdGest
        '
        resources.ApplyResources(Me.cmdGest, "cmdGest")
        Me.cmdGest.BackColor = System.Drawing.SystemColors.Control
        Me.cmdGest.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdGest.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdGest.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdGest.Name = "cmdGest"
        Me.cmdGest.UseVisualStyleBackColor = False
        '
        'cmdAct
        '
        resources.ApplyResources(Me.cmdAct, "cmdAct")
        Me.cmdAct.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAct.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAct.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdAct.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAct.Name = "cmdAct"
        Me.cmdAct.UseVisualStyleBackColor = False
        '
        'cmdMassGeo
        '
        resources.ApplyResources(Me.cmdMassGeo, "cmdMassGeo")
        Me.cmdMassGeo.BackColor = System.Drawing.SystemColors.Control
        Me.cmdMassGeo.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdMassGeo.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdMassGeo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdMassGeo.Name = "cmdMassGeo"
        Me.cmdMassGeo.UseVisualStyleBackColor = False
        '
        'cmdProperties
        '
        resources.ApplyResources(Me.cmdProperties, "cmdProperties")
        Me.cmdProperties.BackColor = System.Drawing.SystemColors.Control
        Me.cmdProperties.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdProperties.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdProperties.Name = "cmdProperties"
        Me.cmdProperties.UseVisualStyleBackColor = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.zlbl_15)
        Me.GroupBox3.Controls.Add(Me.zlbl_14)
        Me.GroupBox3.Controls.Add(Me.cmdCurves)
        Me.GroupBox3.Controls.Add(Me.cmdReport)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.ForeColor = System.Drawing.Color.Brown
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'zlbl_15
        '
        resources.ApplyResources(Me.zlbl_15, "zlbl_15")
        Me.zlbl_15.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_15.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_15.Name = "zlbl_15"
        '
        'zlbl_14
        '
        resources.ApplyResources(Me.zlbl_14, "zlbl_14")
        Me.zlbl_14.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_14.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_14.Name = "zlbl_14"
        '
        'cmdCurves
        '
        resources.ApplyResources(Me.cmdCurves, "cmdCurves")
        Me.cmdCurves.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCurves.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCurves.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdCurves.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCurves.Name = "cmdCurves"
        Me.cmdCurves.UseVisualStyleBackColor = False
        '
        'cmdReport
        '
        resources.ApplyResources(Me.cmdReport, "cmdReport")
        Me.cmdReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdReport.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdReport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.Control
        Me.GroupBox2.Controls.Add(Me.zlbl_13)
        Me.GroupBox2.Controls.Add(Me.zlbl_12)
        Me.GroupBox2.Controls.Add(Me.cmdOrbit)
        Me.GroupBox2.Controls.Add(Me.cmdEnvir)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.ForeColor = System.Drawing.Color.Brown
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'zlbl_13
        '
        resources.ApplyResources(Me.zlbl_13, "zlbl_13")
        Me.zlbl_13.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_13.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_13.Name = "zlbl_13"
        '
        'zlbl_12
        '
        resources.ApplyResources(Me.zlbl_12, "zlbl_12")
        Me.zlbl_12.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_12.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_12.Name = "zlbl_12"
        '
        'cmdOrbit
        '
        resources.ApplyResources(Me.cmdOrbit, "cmdOrbit")
        Me.cmdOrbit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOrbit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOrbit.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdOrbit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOrbit.Name = "cmdOrbit"
        Me.cmdOrbit.UseVisualStyleBackColor = False
        '
        'cmdEnvir
        '
        resources.ApplyResources(Me.cmdEnvir, "cmdEnvir")
        Me.cmdEnvir.BackColor = System.Drawing.SystemColors.Control
        Me.cmdEnvir.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdEnvir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdEnvir.Name = "cmdEnvir"
        Me.cmdEnvir.UseVisualStyleBackColor = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.zlb_18)
        Me.GroupBox4.Controls.Add(Me.zlbl_17)
        Me.GroupBox4.Controls.Add(Me.zlbl_16)
        Me.GroupBox4.Controls.Add(Me.cmdQuit)
        Me.GroupBox4.Controls.Add(Me.cmdOpen)
        Me.GroupBox4.Controls.Add(Me.cmdSave)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.ForeColor = System.Drawing.Color.Brown
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'zlb_18
        '
        resources.ApplyResources(Me.zlb_18, "zlb_18")
        Me.zlb_18.BackColor = System.Drawing.SystemColors.Control
        Me.zlb_18.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlb_18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlb_18.Name = "zlb_18"
        '
        'zlbl_17
        '
        resources.ApplyResources(Me.zlbl_17, "zlbl_17")
        Me.zlbl_17.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_17.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_17.Name = "zlbl_17"
        '
        'zlbl_16
        '
        resources.ApplyResources(Me.zlbl_16, "zlbl_16")
        Me.zlbl_16.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_16.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_16.Name = "zlbl_16"
        '
        'cmdQuit
        '
        resources.ApplyResources(Me.cmdQuit, "cmdQuit")
        Me.cmdQuit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdQuit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdQuit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdQuit.Name = "cmdQuit"
        Me.cmdQuit.UseVisualStyleBackColor = False
        '
        'cmdOpen
        '
        resources.ApplyResources(Me.cmdOpen, "cmdOpen")
        Me.cmdOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOpen.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOpen.Name = "cmdOpen"
        Me.cmdOpen.UseVisualStyleBackColor = False
        '
        'cmdSave
        '
        resources.ApplyResources(Me.cmdSave, "cmdSave")
        Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.UseVisualStyleBackColor = False
        '
        'fraDisplay
        '
        Me.fraDisplay.Controls.Add(Me.zlbl_6)
        Me.fraDisplay.Controls.Add(Me.zlbl_5)
        Me.fraDisplay.Controls.Add(Me.zlbl_4)
        Me.fraDisplay.Controls.Add(Me.optView_3)
        Me.fraDisplay.Controls.Add(Me.optView_0)
        Me.fraDisplay.Controls.Add(Me.optView_1)
        resources.ApplyResources(Me.fraDisplay, "fraDisplay")
        Me.fraDisplay.ForeColor = System.Drawing.Color.Brown
        Me.fraDisplay.Name = "fraDisplay"
        Me.fraDisplay.TabStop = False
        '
        'zlbl_6
        '
        resources.ApplyResources(Me.zlbl_6, "zlbl_6")
        Me.zlbl_6.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_6.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_6.Name = "zlbl_6"
        '
        'zlbl_5
        '
        resources.ApplyResources(Me.zlbl_5, "zlbl_5")
        Me.zlbl_5.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_5.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_5.Name = "zlbl_5"
        '
        'zlbl_4
        '
        resources.ApplyResources(Me.zlbl_4, "zlbl_4")
        Me.zlbl_4.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_4.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_4.Name = "zlbl_4"
        '
        'optView_3
        '
        resources.ApplyResources(Me.optView_3, "optView_3")
        Me.optView_3.BackColor = System.Drawing.SystemColors.Control
        Me.optView_3.Cursor = System.Windows.Forms.Cursors.Default
        Me.optView_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optView_3.Name = "optView_3"
        Me.optView_3.UseVisualStyleBackColor = False
        '
        'optView_0
        '
        resources.ApplyResources(Me.optView_0, "optView_0")
        Me.optView_0.BackColor = System.Drawing.SystemColors.Control
        Me.optView_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.optView_0.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.optView_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optView_0.Name = "optView_0"
        Me.optView_0.UseVisualStyleBackColor = False
        '
        'optView_1
        '
        resources.ApplyResources(Me.optView_1, "optView_1")
        Me.optView_1.BackColor = System.Drawing.SystemColors.Control
        Me.optView_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.optView_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optView_1.Name = "optView_1"
        Me.optView_1.UseVisualStyleBackColor = False
        '
        'fraSimulation
        '
        Me.fraSimulation.BackColor = System.Drawing.SystemColors.Control
        Me.fraSimulation.Controls.Add(Me.updwTimeStep)
        Me.fraSimulation.Controls.Add(Me.lblDate)
        Me.fraSimulation.Controls.Add(Me.zlbl_1)
        Me.fraSimulation.Controls.Add(Me.txt_date_debut)
        Me.fraSimulation.Controls.Add(Me.zlbl_3)
        Me.fraSimulation.Controls.Add(Me.zlbl_0)
        Me.fraSimulation.Controls.Add(Me.cmdOneFwd)
        Me.fraSimulation.Controls.Add(Me.cmdFwd)
        Me.fraSimulation.Controls.Add(Me.cmdOneBack)
        Me.fraSimulation.Controls.Add(Me.cmdStop)
        Me.fraSimulation.Controls.Add(Me.cmdPause)
        resources.ApplyResources(Me.fraSimulation, "fraSimulation")
        Me.fraSimulation.ForeColor = System.Drawing.Color.Brown
        Me.fraSimulation.MinimumSize = New System.Drawing.Size(0, 10)
        Me.fraSimulation.Name = "fraSimulation"
        Me.fraSimulation.TabStop = False
        '
        'updwTimeStep
        '
        resources.ApplyResources(Me.updwTimeStep, "updwTimeStep")
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items1"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items2"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items3"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items4"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items5"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items6"))
        Me.updwTimeStep.Name = "updwTimeStep"
        '
        'lblDate
        '
        Me.lblDate.BackColor = System.Drawing.Color.White
        Me.lblDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDate.Cursor = System.Windows.Forms.Cursors.Default
        resources.ApplyResources(Me.lblDate, "lblDate")
        Me.lblDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDate.Name = "lblDate"
        '
        'zlbl_1
        '
        resources.ApplyResources(Me.zlbl_1, "zlbl_1")
        Me.zlbl_1.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_1.Name = "zlbl_1"
        '
        'txt_date_debut
        '
        resources.ApplyResources(Me.txt_date_debut, "txt_date_debut")
        Me.txt_date_debut.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txt_date_debut.Name = "txt_date_debut"
        Me.txt_date_debut.Value = New Date(2000, 1, 1, 0, 0, 0, 0)
        '
        'zlbl_3
        '
        resources.ApplyResources(Me.zlbl_3, "zlbl_3")
        Me.zlbl_3.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_3.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_3.Name = "zlbl_3"
        '
        'zlbl_0
        '
        resources.ApplyResources(Me.zlbl_0, "zlbl_0")
        Me.zlbl_0.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_0.Name = "zlbl_0"
        '
        'cmdOneFwd
        '
        resources.ApplyResources(Me.cmdOneFwd, "cmdOneFwd")
        Me.cmdOneFwd.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOneFwd.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOneFwd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOneFwd.Name = "cmdOneFwd"
        Me.cmdOneFwd.UseVisualStyleBackColor = False
        '
        'cmdFwd
        '
        resources.ApplyResources(Me.cmdFwd, "cmdFwd")
        Me.cmdFwd.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFwd.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFwd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFwd.Name = "cmdFwd"
        Me.cmdFwd.UseVisualStyleBackColor = False
        '
        'cmdOneBack
        '
        resources.ApplyResources(Me.cmdOneBack, "cmdOneBack")
        Me.cmdOneBack.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOneBack.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOneBack.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOneBack.Name = "cmdOneBack"
        Me.cmdOneBack.UseVisualStyleBackColor = False
        '
        'cmdStop
        '
        resources.ApplyResources(Me.cmdStop, "cmdStop")
        Me.cmdStop.BackColor = System.Drawing.Color.White
        Me.cmdStop.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdStop.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdStop.Name = "cmdStop"
        Me.cmdStop.UseVisualStyleBackColor = False
        '
        'cmdPause
        '
        resources.ApplyResources(Me.cmdPause, "cmdPause")
        Me.cmdPause.BackColor = System.Drawing.Color.White
        Me.cmdPause.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdPause.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdPause.Name = "cmdPause"
        Me.cmdPause.UseVisualStyleBackColor = False
        '
        'frmMain
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.fraSimulation)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.fraDisplay)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox4)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Name = "frmMain"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.fraDisplay.ResumeLayout(False)
        Me.fraDisplay.PerformLayout()
        Me.fraSimulation.ResumeLayout(False)
        Me.fraSimulation.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents cmdCells As System.Windows.Forms.RadioButton
    Public WithEvents cmdActuactors As System.Windows.Forms.RadioButton
    Public WithEvents cmd As System.Windows.Forms.RadioButton
    Public WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_8 As System.Windows.Forms.Label
    Public WithEvents zlbl_11 As System.Windows.Forms.Label
    Public WithEvents zlbl_10 As System.Windows.Forms.Label
    Public WithEvents zlbl_7 As System.Windows.Forms.Label
    Public WithEvents cmdGest As System.Windows.Forms.RadioButton
    Public WithEvents cmdAct As System.Windows.Forms.RadioButton
    Public WithEvents cmdMassGeo As System.Windows.Forms.RadioButton
    Public WithEvents cmdProperties As System.Windows.Forms.RadioButton
    Public WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_15 As System.Windows.Forms.Label
    Public WithEvents zlbl_14 As System.Windows.Forms.Label
    Public WithEvents cmdCurves As System.Windows.Forms.RadioButton
    Public WithEvents cmdReport As System.Windows.Forms.RadioButton
    Public WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_13 As System.Windows.Forms.Label
    Public WithEvents zlbl_12 As System.Windows.Forms.Label
    Public WithEvents cmdOrbit As System.Windows.Forms.RadioButton
    Public WithEvents cmdEnvir As System.Windows.Forms.RadioButton
    Public WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Public WithEvents zlb_18 As System.Windows.Forms.Label
    Public WithEvents zlbl_17 As System.Windows.Forms.Label
    Public WithEvents zlbl_16 As System.Windows.Forms.Label
    Public WithEvents cmdQuit As System.Windows.Forms.RadioButton
    Public WithEvents cmdOpen As System.Windows.Forms.RadioButton
    Public WithEvents cmdSave As System.Windows.Forms.RadioButton
    Public WithEvents fraDisplay As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_6 As System.Windows.Forms.Label
    Public WithEvents zlbl_5 As System.Windows.Forms.Label
    Public WithEvents zlbl_4 As System.Windows.Forms.Label
    Public WithEvents optView_3 As System.Windows.Forms.RadioButton
    Public WithEvents optView_0 As System.Windows.Forms.RadioButton
    Public WithEvents optView_1 As System.Windows.Forms.RadioButton
    Public WithEvents fraSimulation As System.Windows.Forms.GroupBox
    Friend WithEvents updwTimeStep As System.Windows.Forms.DomainUpDown
    Public WithEvents lblDate As System.Windows.Forms.Label
    Public WithEvents zlbl_1 As System.Windows.Forms.Label
    Friend WithEvents txt_date_debut As System.Windows.Forms.DateTimePicker
    Public WithEvents zlbl_3 As System.Windows.Forms.Label
    Public WithEvents zlbl_0 As System.Windows.Forms.Label
    Public WithEvents cmdOneFwd As System.Windows.Forms.Button
    Public WithEvents cmdFwd As System.Windows.Forms.Button
    Public WithEvents cmdOneBack As System.Windows.Forms.Button
    Public WithEvents cmdStop As System.Windows.Forms.Button
    Public WithEvents cmdPause As System.Windows.Forms.Button
#End Region
End Class