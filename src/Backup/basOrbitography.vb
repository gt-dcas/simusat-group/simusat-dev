Option Strict Off
Option Explicit On
Imports System.Math
Module basOrbitography

    ''' <summary>
    ''' Checks if a satellite is seeing the sun or not
    ''' </summary>
    ''' <param name="satPos">ECI position of the satellite [km]</param>
    ''' <param name="SunPos">ECI position of the sun [km]</param>
    ''' <returns>return true if satellite is not seeing sun, false otherwise</returns>
    ''' <remarks></remarks>
    Public Function Eclipse(ByRef satPos As vector, ByRef SunPos As vector) As Boolean

        Dim modr2, norme_r As Double
        Dim sin_gamma, cos_gamma, Gamma As Double
        Dim zss0, xss0, yss0, dss0 As Double
        Dim yy, xx, ZZ As Double
        Dim e3, e1, e2, cos_ro As Double
        Dim s2, s1, s3 As Double
        '
        modr2 = Pow(satPos.X, 2) + Pow(satPos.Y, 2) + Pow(satPos.Z, 2)
        norme_r = Sqrt(modr2)
        '
        ' Coordonn�es du vecteur unitaire satellite/terre dans le rep�re gamma50
        ' Coordinates of the unit vector satellite/Earth in the coordination system gamma50 (ECI)
        e1 = -satPos.X / norme_r
        e2 = -satPos.Y / norme_r
        e3 = -satPos.Z / norme_r
        cos_ro = Sqrt(modr2 - Pow(RE, 2)) / Sqrt(modr2)
        '
        ' Coordonn�es du vecteur unitaire satellite/soleil dans gamma50
        ' Coordinates of the unit vector satellite/Sun in the coordination system gamma50 (ECI)
        xss0 = SunPos.X - satPos.X
        yss0 = SunPos.Y - satPos.Y
        zss0 = SunPos.Z - satPos.Z
        dss0 = Sqrt(Pow(xss0, 2) + Pow(yss0, 2) + Pow(zss0, 2))
        s1 = xss0 / dss0
        s2 = yss0 / dss0
        s3 = zss0 / dss0
        cos_gamma = e1 * s1 + e2 * s2 + e3 * s3
        xx = e2 * s3 - e3 * s2
        yy = e3 * s1 - e1 * s3
        ZZ = e1 * s2 - e2 * s1
        sin_gamma = Sqrt(Pow(xx, 2) + Pow(yy, 2) + Pow(ZZ, 2))
        Gamma = Math.Acos(cos_gamma)
        If cos_ro < cos_gamma Then
            Eclipse = True
        Else
            Eclipse = False
        End If
        ''
    End Function

    ''' <summary>
    ''' Transforms Kepler elements to a Position and Velocity (in ECI)
    ''' </summary>
    ''' <param name="Kepler">Orbit Elements (semi-major axis, eccentricity, inclination, argument of perigee, RAAN, Mean Anomaly)</param>
    ''' <param name="Pos">Position of the Satellite in ECI [km]</param>
    ''' <param name="Vel">Velocity of Satellite in ECI [km/s]</param>
    ''' <remarks>
    ''' Passage des �l�ments osculateurs de l'orbite aux �l�ments
    ''' rectangulaires correspondants dans le rep�re g�ocentrique
    ''' </remarks>
    Public Sub Kepler_to_PosVel(ByRef Kepler As orbit, ByRef Pos As vector, ByRef Vel As vector)

        Dim com, i, a, e, som, ma As Double
        Dim sini, cosi As Double
        Dim sinsom, cossom As Double
        Dim sincom, coscom As Double
        Dim anomalie_vrai As Double
        Dim xle, rayon As Double
        Dim sinxle, cosxle As Double
        Dim prod_aDE As Double
        Dim qy, qx, qz As Double
        Dim py, px, pz As Double

        With Kepler
            a = .SemiMajor_Axis
            e = .Eccentricity
            i = .Inclination
            som = .Argument_Perigee
            com = .RAAN
            ma = .Mean_Anomaly
        End With

        i = i * rad
        som = som * rad
        com = com * rad
        ma = ma * rad

        cosi = Cos(i)
        sini = Sin(i)
        cossom = Cos(som)
        sinsom = Sin(som)
        coscom = Cos(com)
        sincom = Sin(com)

        'r�solution de l'�quation de Kepler pour d�terminer xle : anomalie excentrique
        'Solution of Kepler equation for determination of xle : eccentric anomaly
        'xle : anomalie excentrique
        'xle : eccentric anomaly
        ma = Mod2pi(ma)
        xle = solveKepler(ma, e)
        cosxle = Cos(xle)
        sinxle = Sin(xle)
        anomalie_vrai = eccentricAnom_to_trueAnom(xle, e) * rad
        rayon = a * (1 - e * cosxle)
        '
        'Calculation of the rectangular Coordinates X,Y,Z
        With Pos
            .X = rayon * (Cos(som + anomalie_vrai) * coscom - Sin(som + anomalie_vrai) * sincom * cosi)
            .Y = rayon * (Cos(som + anomalie_vrai) * sincom + Sin(som + anomalie_vrai) * coscom * cosi)
            .Z = rayon * Sin(som + anomalie_vrai) * Sqrt(1 - (cosi) ^ 2)
        End With

        'Calculation of the velocity of the satellite
        prod_aDE = a * Sqrt(myEarth / (a * a * a)) / (1 - e * cosxle)

        qx = -sinsom * coscom - cosi * sincom * cossom
        qy = -sinsom * sincom + cosi * coscom * cossom
        qz = sini * cossom

        px = cossom * coscom - cosi * sincom * sinsom
        py = cossom * sincom + cosi * coscom * sinsom
        pz = sini * sinsom

        With Vel
            .X = prod_aDE * (qx * cosxle * Sqrt(1 - e ^ 2) - px * sinxle)
            .Y = prod_aDE * (qy * cosxle * Sqrt(1 - e ^ 2) - py * sinxle)
            .Z = prod_aDE * (qz * cosxle * Sqrt(1 - e ^ 2) - pz * sinxle)
        End With

    End Sub

    ''' <summary>
    ''' Transforms Position and Velocity (in ECI) to Kepler Orbit Elements
    ''' </summary>
    ''' <param name="r">Position of the Satellite in ECI [km]</param>
    ''' <param name="v">Velocity of Satellite in ECI [km/s]</param>
    ''' <param name="orbit">Kepler Orbit Elements (semi-major axis, eccentricity, inclination, argument of perigee, RAAN, Mean Anomaly)</param>
    ''' <remarks></remarks>
    Public Sub PosVel_to_Kepler(ByRef r As vector, ByRef v As vector, ByRef orbit As orbit)

        Dim ta, epsilon, ea As Double
        Dim n, h, e As vector
        Dim Z As vector
        '
        Z.X = 0
        Z.Y = 0
        Z.Z = 1
        '
        h = vctCross(r, v)
        n = vctCross(Z, h)
        e = vctMultScalar(r, (vctNorme(v) * vctNorme(v)) - (myEarth / vctNorme(r)))
        e = vctSub(e, vctMultScalar(v, vctDot(r, v)))
        e = vctMultScalar(e, 1 / myEarth)
        epsilon = ((vctNorme(v) * vctNorme(v)) / 2) - (myEarth / vctNorme(r))
        '
        With orbit
            .SemiMajor_Axis = -myEarth / (2 * epsilon)
            .Eccentricity = vctNorme(e)
            .Inclination = Math.Acos(h.Z / vctNorme(h)) * deg
            If vctNorme(n) = 0 Then
                .RAAN = 0
            Else
                .RAAN = Math.Acos(n.X / vctNorme(n)) * deg
            End If
            If Not (n.Y > 0) Then .RAAN = 360 - .RAAN

            If vctNorme(n) = 0 Or vctNorme(e) = 0 Then
                .Argument_Perigee = 0
            Else
                .Argument_Perigee = Math.Acos(vctDot(n, e) / (vctNorme(n) * vctNorme(e))) * deg
            End If
            If Not (e.Z > 0) Then .Argument_Perigee = 360 - .Argument_Perigee
            ta = Math.Acos(vctDot(e, r) / (vctNorme(e) * vctNorme(r)))
            If Not (vctDot(r, v) > 0) Then ta = 2 * pi - ta
            ea = trueAnom_to_eccentricAnom(ta, vctNorme(e)) * rad
            .Mean_Anomaly = (ea - (vctNorme(e) * Sin(ea))) * deg
        End With

    End Sub

    ''' <summary>
    ''' Solve Kepler's equation with a Newton approach
    ''' r�solution de l'�quation de K�pler par la m�thode de NEWTON
    ''' </summary>
    ''' <param name="am">average anomaly [rad] / anomalie moyenne(rad)</param>
    ''' <param name="ex">excentricity / excentricit�</param>
    ''' <returns>return eccentric anomaly / anomalie excentrique</returns>
    ''' <remarks>
    ''' todo refactoring: variables need types! => OK
    ''' </remarks>
    Public Function solveKepler(ByVal am As Double, ByVal ex As Double) As Double

        Const EPS As Double = 0.00000000001
        Const AMI As Double = 0.00000000001
        '
        '
        Dim EI, co, si, Z, ES As Double
        Dim DELTAE As Double
        Dim i As Short
        '
        si = Sin(am)
        '
        If ((ex < EPS) Or (Abs(si) < AMI)) Then
            solveKepler = am
        Else
            co = Cos(am)
            Z = ex * si / Sqrt(ex * ex + 1.0# - 2.0# * ex * co)
            EI = am + Z - Z * Z * Z * Z * co / (si * 6.0#) * (1.0# - ex * ex)
            i = 0
            Do
                ES = EI + (am + ex * Sin(EI) - EI) / (1.0# - ex * Cos(EI))
                DELTAE = Abs(ES - EI)
                EI = ES
                i = i + 1
            Loop Until ((i = 10) Or (DELTAE < EPS))
            solveKepler = ES
        End If
        ''
    End Function

    ''' <summary>
    ''' Update an Orbit for a timestep
    ''' </summary>
    ''' <param name="dblJulianNow">Julian Date</param>
    ''' <param name="orbit">Orbit as Kepler Orbit Elements</param>
    ''' <remarks></remarks>
    Public Sub update_Orbit(ByRef dblJulianNow As Double, ByRef orbit As orbit)

        Dim gom_moy, i_moy, a_moy, e_moy, pom_moy, am_moy As Double
        Dim dec2, dec1, bstar As Double
        Dim gom_corrige, i_corrige, a_corrige, e_corrige, pom_corrige, am_corrige As Double
        Dim date_bulletin, djules As Double
        '
        With SatOrbit0
            a_moy = .SemiMajor_Axis
            e_moy = .Eccentricity
            i_moy = .Inclination
            pom_moy = .Argument_Perigee
            gom_moy = .RAAN
            am_moy = .Mean_Anomaly
            date_bulletin = .epoch
            dec1 = .dec1
            dec2 = .dec2
            bstar = .bstar
        End With
        '
        If e_moy = 0 Then e_moy = 0.0000001 ' To avoid division with 0 in the propagators
        If i_moy = 0 Then i_moy = 0.00000005
        ' convert angles in radians
        i_moy = i_moy * rad
        gom_moy = gom_moy * rad
        pom_moy = pom_moy * rad
        am_moy = am_moy * rad

        djules = dblJulianNow
        ' Use SGP, SDP4 propagator
        Call norad_SGP(a_moy, e_moy, i_moy, pom_moy, gom_moy, am_moy, dec1, dec2, bstar, date_bulletin, djules, a_corrige, e_corrige, i_corrige, pom_corrige, gom_corrige, am_corrige)
        '
        ' Convert angles in degrees
        i_corrige = i_corrige * deg
        pom_corrige = pom_corrige * deg
        gom_corrige = gom_corrige * deg
        am_corrige = am_corrige * deg
        '
        ' Update the orbit
        With orbit
            .SemiMajor_Axis = a_corrige
            .Eccentricity = e_corrige
            .Inclination = i_corrige
            .Argument_Perigee = pom_corrige
            .RAAN = gom_corrige
            .Mean_Anomaly = am_corrige
            .epoch = dblJulianNow
        End With
        ''
    End Sub

    ''' <summary>
    ''' get altitude of apogee of given orbit
    ''' </summary>
    ''' <param name="orbit">Kepler Orbit Elements</param>
    ''' <returns>return altitude of the apogee of the orbit [km]</returns>
    ''' <remarks>
    ''' note: The altitude is measured from earth surface for a spherical Earth
    ''' </remarks>
    Public Function get_apogeeAlt(ByRef orbit As orbit) As Double
        With orbit
            get_apogeeAlt = .SemiMajor_Axis * (1 + .Eccentricity) - RE
        End With
    End Function

    ''' <summary>
    ''' get altitude of perigee of given orbit
    ''' </summary>
    ''' <param name="orbit">Kepler Orbit Elements</param>
    ''' <returns>return altitude of the perigee of the orbit [km]</returns>
    ''' <remarks>
    ''' note: The altitude is measured from earth surface for a spherical Earth
    ''' </remarks>
    Public Function get_perigeeAlt(ByRef orbit As orbit) As Double
        With orbit
            get_perigeeAlt = .SemiMajor_Axis * (1 - .Eccentricity) - RE
        End With
    End Function

    ''' <summary>
    ''' get period of the orbit
    ''' </summary>
    ''' <param name="orbit">Orbit Kepler Elements</param>
    ''' <returns>return period of the orbit [min]</returns>
    ''' <remarks>
    ''' todo going to base-units, this function and its calling functions must be changed, to
    ''' </remarks>
    Public Function get_period(ByRef orbit As orbit) As Double
        Dim periodsec As Double
        periodsec = 2 * pi * (orbit.SemiMajor_Axis ^ 3 / myEarth) ^ 0.5
        get_period = periodsec / 60
    End Function

    ''' <summary>
    ''' get radius of apogee of the orbit
    ''' </summary>
    ''' <param name="orbit">Orbit Kepler Elements</param>
    ''' <returns>return radius of the apogee of the orbit [km]</returns>
    ''' <remarks></remarks>
    Public Function get_apogeeRadius(ByRef orbit As orbit) As Double
        With orbit
            get_apogeeRadius = .SemiMajor_Axis * (1 + .Eccentricity)
        End With
    End Function

    ''' <summary>
    ''' get radius of perigee of the orbit
    ''' </summary>
    ''' <param name="orbit">Orbit Kepler Elements</param>
    ''' <returns>return radius of the perigee of the orbit [km]</returns>
    ''' <remarks></remarks>
    Public Function get_perigeeRadius(ByRef orbit As orbit) As Double
        With orbit
            get_perigeeRadius = .SemiMajor_Axis * (1 - .Eccentricity)
        End With
    End Function

    ''' <summary>
    ''' get eccentric anomaly of the orbit
    ''' </summary>
    ''' <param name="orbit">Orbit Kepler Elements</param>
    ''' <returns>return eccentric anomaly of the orbit</returns>
    ''' <remarks></remarks>
    Public Function get_eccentricAnom(ByRef orbit As orbit) As Double
        Dim ma, ea As Double
        With orbit
            ma = .Mean_Anomaly * rad
            ea = solveKepler(ma, .Eccentricity)
            get_eccentricAnom = ea * deg
        End With
    End Function

    ''' <summary>
    ''' get true anomaly of the orbit
    ''' </summary>
    ''' <param name="orbit"></param>
    ''' <returns>Orbit Kepler Elements</returns>
    ''' <remarks>return true anomaly of the orbit</remarks>
    Public Function get_trueAnom(ByRef orbit As orbit) As Double
        Dim ma, ea As Double
        With orbit
            ma = .Mean_Anomaly * rad
            ea = solveKepler(ma, .Eccentricity)
            get_trueAnom = eccentricAnom_to_trueAnom(ea, .Eccentricity)
        End With
    End Function

    ''' <summary>
    ''' Create an orbit from a NORAD TLE String
    ''' </summary>
    ''' <param name="tle">String with NORAD Two-Line Elements</param>
    ''' <param name="orb">Kepler Orbit Elements</param>
    ''' <remarks>
    ''' todo should be a function, shouldn't it?
    ''' </remarks>
    Public Sub norad_to_Orbit(ByRef tle As String, ByRef orb As orbit)

        Dim Year, Day As Double
        Dim xno As Double
        'Dim ao, aodp, xnodp, betao, eosq, betao2, del1, delo, theta2, A1, xno, xke, cosio, go, x3thm1As Double
        Dim J2, ck2 As Double
        '
        J2 = 0.0010826269
        ck2 = J2 / 2
        Year = 1900 + Val(Mid(tle, 45, 2))

        If Year < 1950 Then Year = Year + 100

        Day = Val(Mid(tle, 47, 12))
        orb.dec1 = Val(Mid(tle, 60, 10))
        orb.dec2 = Val("0." & Mid(tle, 71, 6) & "e" & Mid(tle, 77, 2))
        orb.bstar = Val("0." & Mid(tle, 80, 6) & "e" & Mid(tle, 86, 2))
        xno = Val(Mid(tle, 150, 11)) * ((2 * pi) / secday)
        orb.Eccentricity = Val("0." & Mid(tle, 124, 7))
        orb.Inclination = Val(Mid(tle, 106, 8))
        orb.RAAN = Val(Mid(tle, 115, 8))
        orb.Argument_Perigee = Val(Mid(tle, 132, 8))
        orb.Mean_Anomaly = Val(Mid(tle, 141, 8))
        orb.SemiMajor_Axis = (myEarth / (xno * xno)) ^ (1 / 3)
        '
        ' Correction of report
        ' ----------------------
        ' 1 is removed at report day
        Day = Day - 1
        '
        If (Modulo((Year - 1900), 4) > 0) Then
            ' In case of a leap year
            orb.epoch = (365.25 * (((Year - 1900) \ 4) * 4) + 365 * Modulo((Year - 1900), 4)) + 1 + Day - 18263
            ' the +1 corresponds to the 29 february of a past leap year
            ' le +1 correspond to 29 f�vrier of previous leap year
            ' 18263 corresponds to the number of days between the first january 1900
            ' and the first january 1950 origin of the CNES Julian Days; beware that the
            ' first january 1950 is the day 0 and not 1
            ' 18263 correspond to the days number 1er january 1900
            ' and 1er january 1950 is the origin of the CNES julian days, knowing that
            ' 1st january 1950 is day 0 and not 1
        Else
            'In case of not a leap year
            orb.epoch = 365.25 * (((Year - 1900) \ 4) * 4) + Day - 18263
        End If
        ' Finally the CNES Julian day is converted into Julian day
        orb.epoch = orb.epoch + 2433282.5
    End Sub

    ''' <summary>
    ''' Convert a true anomaly to an eccentric anomaly
    ''' </summary>
    ''' <param name="trueAnom">true anomaly</param>
    ''' <param name="Eccentricity">eccentricity</param>
    ''' <returns>return eccentric anomaly</returns>
    ''' <remarks>
    ''' This function is used in here and in some frames. -> stays public
    ''' </remarks>
    Public Function trueAnom_to_eccentricAnom(ByRef trueAnom As Double, ByRef Eccentricity As Double) As Double

        Dim cosea, sinea, ea As Double
        Dim sinta, costa As Double
        '
        sinta = Sin(trueAnom)
        costa = Cos(trueAnom)
        sinea = (Sqrt(1 - Pow(Eccentricity, 2)) * sinta) / (1 + Eccentricity * costa)
        cosea = (Eccentricity + costa) / (1 + Eccentricity * costa)
        ea = Atan2(sinea, cosea)
        ea = Mod2pi(ea)
        trueAnom_to_eccentricAnom = ea * deg
        ''
    End Function

    ''' <summary>
    ''' Convert an eccentric anomaly to a true anomaly
    ''' </summary>
    ''' <param name="eccentricAnom">eccentric anomaly</param>
    ''' <param name="Eccentricity">eccentricity</param>
    ''' <returns>return true anomaly</returns>
    ''' <remarks></remarks>
    Private Function eccentricAnom_to_trueAnom(ByRef eccentricAnom As Double, ByRef Eccentricity As Double) As Double

        Dim sinea, cosea As Double
        Dim costa, sinta, ta As Double
        '
        sinea = Sin(eccentricAnom)
        cosea = Cos(eccentricAnom)
        sinta = (Sqrt(1 - Pow(Eccentricity, 2)) * sinea) / (1 - Eccentricity * cosea)
        costa = (cosea - Eccentricity) / (1 - Eccentricity * cosea)
        ta = Atan2(sinta, costa)
        ta = Mod2pi(ta)
        eccentricAnom_to_trueAnom = ta * deg
        ''
    End Function

    ''' <summary>
    ''' Converts date format from Julian to Gregorian
    ''' </summary>
    ''' <param name="dblJulian">Julian Date [days]</param>
    ''' <param name="udtGregorian">time in Gregorian [Calender Format]</param>
    ''' <remarks>
    ''' note : CNES Julian 0 = Julian 2433282.5 = 01.01.1950 0h
    ''' </remarks>
    Public Sub convert_Julian_Gregorian(ByRef dblJulian As Double, ByRef udtGregorian As udtDate)

        Dim days_total As Double
        Dim days_rest As Double
        Dim CNESJulian As Double
        '
        Dim days_cumulative(13) As Short

        days_cumulative(1) = 0
        days_cumulative(2) = 31
        days_cumulative(3) = 59
        days_cumulative(4) = 90
        days_cumulative(5) = 120
        days_cumulative(6) = 151
        days_cumulative(7) = 181
        days_cumulative(8) = 212
        days_cumulative(9) = 243
        days_cumulative(10) = 273
        days_cumulative(11) = 304
        days_cumulative(12) = 334
        days_cumulative(13) = 365

        ' convert in a day CNES Julian
        CNESJulian = dblJulian - 2433282.5 '***** Attention � la d�finition
        days_total = Int(CNESJulian) + 18263 + 1
        '
        ' The fraction part of dblCNESJulian represents hours, minutes and seconds
        udtGregorian.hour = Int(Frac(CNESJulian) * 24)
        udtGregorian.minute = Int(Frac(Frac(CNESJulian) * 24) * 60)
        udtGregorian.second = Round(Frac(Frac(Frac(CNESJulian) * 24) * 60) * 60)
        If udtGregorian.second = 60 Then
            udtGregorian.second = 0
            udtGregorian.minute = udtGregorian.minute + 1
            If udtGregorian.minute = 60 Then
                udtGregorian.minute = 0
                udtGregorian.hour = udtGregorian.hour + 1
            End If
        End If
        'the integer part of dblCNESJulian represents the number of days passed since 01/01/1950
        udtGregorian.year = Int((days_total - 1) / 365.25) + 1900

        If ((udtGregorian.year - 1900) Mod 4 <> 0) Then
            days_rest = days_total - (((udtGregorian.year - 1900) \ 4) * 4 * 365.25) - (((udtGregorian.year - 1900) Mod 4) * 365) - 1
        Else
            days_rest = days_total - (((udtGregorian.year - 1900) \ 4) * 4 * 365.25)
        End If

        udtGregorian.month = 1

        Do
            udtGregorian.month = udtGregorian.month + 1
        Loop Until (((days_rest - days_cumulative(udtGregorian.month)) <= 0) Or (udtGregorian.month = 13))

        udtGregorian.month = udtGregorian.month - 1
        udtGregorian.day = Int(days_rest - days_cumulative(udtGregorian.month))

        'case of leap year and month >= march
        If ((((udtGregorian.year - 1900) Mod 4) = 0) And (udtGregorian.month >= 3)) Then
            '29 february
            udtGregorian.day = udtGregorian.day - 1

            If (udtGregorian.day = 0) Then
                udtGregorian.month = udtGregorian.month - 1

                If (udtGregorian.month = 2) Then
                    '29 february created
                    udtGregorian.day = Int(days_rest - days_cumulative(udtGregorian.month))
                Else
                    '29 february removed
                    udtGregorian.day = Int(days_rest - days_cumulative(udtGregorian.month)) - 1
                End If
            End If
        End If
        udtGregorian.dayofweek = Int(dblJulian) Mod 7
    End Sub
    ''' <summary>
    ''' Converts date format from Gregorian to Julian
    ''' </summary>
    ''' <param name="udtGregorian">time in Gregorian</param>
    ''' <returns>Julian Date [days]</returns>
    ''' <remarks></remarks>
    Public Function convert_Gregorian_Julian(ByRef udtGregorian As udtDate) As Double
        Dim temp As Double
        Dim days_cumulative(13) As Short

        days_cumulative(1) = 0
        days_cumulative(2) = 31
        days_cumulative(3) = 59
        days_cumulative(4) = 90
        days_cumulative(5) = 120
        days_cumulative(6) = 151
        days_cumulative(7) = 181
        days_cumulative(8) = 212
        days_cumulative(9) = 243
        days_cumulative(10) = 273
        days_cumulative(11) = 304
        days_cumulative(12) = 334
        days_cumulative(13) = 365

        temp = 365.25 * 4 * ((udtGregorian.year - 1900) \ 4)
        temp = temp + days_cumulative(udtGregorian.month)
        temp = temp + udtGregorian.day - 1 - 18263 'removes one day

        If (udtGregorian.year - 1900) Mod 4 <> 0 Then
            temp = temp + (365 * ((udtGregorian.year - 1900) Mod 4)) + 1
        Else
            If udtGregorian.month >= 3 Then temp = temp + 1
        End If

        convert_Gregorian_Julian = ((((udtGregorian.second / 60) + udtGregorian.minute) / 60 + udtGregorian.hour) / 24) + temp + 2433282.5

    End Function
    '
    ''' <summary>
    ''' Update time and date display in frmMain
    ''' </summary>
    ''' <param name="dblJulianNow">Julian Date [days]</param>
    ''' <remarks></remarks>
    Public Sub update_Date(ByRef dblJulianNow As Double)
        '
        Dim udtGregorianNow As udtDate
        '
        Call convert_Julian_Gregorian(dblSimulationTime, udtGregorianNow)
        '
        With udtGregorianNow
            frmMain.lblDate.Text = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
            & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        ''
    End Sub
    ''' <summary>
    ''' Update the sun's position
    ''' </summary>
    ''' <param name="dblJD">Julian Date [days]</param>
    ''' <param name="sun_pos">Position of the sun in ECI</param>
    ''' <remarks></remarks>
    Public Sub update_Sun(ByRef dblJD As Double, ByRef sun_pos As vector)
        Dim xeps, xlm As Double
        Dim dblDistEarthSun As Double 'distance earth / sun
        '
        Dim djulian_2000, djulian_1900 As Double
        Dim xgsrad As Double
        Dim xlmp, xms, xe As Double

        ' Convert julian date to julian date of (01/01/2000 12h)
        djulian_2000 = dblJD - 2451545
        '
        ' Calculate sun distance
        xgsrad = 6.239994691 + 0.01720196965 * djulian_2000
        dblDistEarthSun = (1.00014 - 0.01675 * Cos(xgsrad) - 0.00014 * Cos(2 * xgsrad)) * 149596000.0#
        '
        ' Calculate position of the sun in ecliptic
        ' -----------------------------------------
        ' xeps inclination of the ecliptic in the equatorial plane at given time
        ' xlm true longitude in ecliptic after passing the equinox
        '
        Const A1 As Double = 0.40931974744572
        Const B1 As Double = -0.0000000062179594501235
        Const c1 As Double = -2.14556050619E-17
        Const D1 As Double = 1.8016676426329E-22
        Const a2 As Double = 0.0167104
        Const B2 As Double = -0.0000000011444216290212
        Const c2 As Double = -9.4447419350758E-17
        Const A3 As Double = 4.881627972897
        Const B3 As Double = 0.017202791266239
        Const c3 As Double = 0.0000000000000039640538620461
        Const a4 As Double = 6.2565835223195
        Const B4 As Double = 0.017201969767685
        Const c4 As Double = -0.000000000000001962402902003
        Const d4 As Double = -1.0745532659839E-21
        '
        ' Convert dblJD jours juliens � djulian_1900 jours juliens (1/01/1900 12 heures)
        djulian_1900 = dblJD - 2415020
        '
        ' Inclination de l'�cliptique sur l'�quateur
        xeps = (((D1 * djulian_1900) + c1) * djulian_1900 + B1) * djulian_1900 + A1
        '
        ' Excentricit� de la terre sur son orbite
        xe = ((c2 * djulian_1900) + B2) * djulian_1900 + a2
        '
        ' Longitude moyenne
        xlmp = ((c3 * djulian_1900) + B3) * djulian_1900 + A3
        '
        ' Anomalie moyenne
        xms = (((d4 * djulian_1900) + c4) * djulian_1900 + B4) * djulian_1900 + a4
        '
        ' Longitude vraie
        xlm = xlmp + 2 * xe * Sin(xms) + 1.25 * xe * xe * Sin(2 * xms)
        If xlm >= (2 * pi) Then xlm = xlm - (2 * pi)
        If xlm < 0 Then xlm = xlm + (2 * pi)
        '
        '
        sun_pos.X = dblDistEarthSun * Cos(xlm)
        sun_pos.Y = dblDistEarthSun * Sin(xlm) * Cos(xeps)
        sun_pos.Z = dblDistEarthSun * Sin(xlm) * Sin(xeps)
        ''
    End Sub
    ''' <summary>
    ''' Update the earth's moon's position ECI
    ''' </summary>
    ''' <param name="dblJD">Julian Date  [days]</param>
    ''' <param name="moon_pos">Position of the sun in ECI [m]</param>
    ''' <remarks></remarks>
    Public Sub update_Moon(ByRef dblJD As Double, ByRef moon_pos As vector)

        ' D�clarations
        ' ------------
        Dim xms, j1900, xlmp As Double ' Relatives au calcul du soleil
        Dim xe, xeps, xlm As Double
        '
        Dim d As Double ' Relatives au calcul de la lune
        Dim n, L, m As Double
        Dim xA4, xA3, Ev, ae, Ec, v As Double
        Dim X, Y As Double
        Dim LBm, BTm As Double
        Dim lg As Double
        'Dim lt As double
        'Dim y_lune, x_lune, z_lune As Double
        '
        ' Constantes
        ' ----------
        Const A1 As Double = 0.40931974744572 ' Pour le calcul du soleil
        Const B1 As Double = -0.0000000062179594501235
        Const c1 As Double = -2.14556050619E-17
        Const D1 As Double = 1.8016676426329E-22
        Const a2 As Double = 0.0167104
        Const B2 As Double = -0.0000000011444216290212
        Const c2 As Double = -9.4447419350758E-17
        Const A3 As Double = 4.881627972897
        Const B3 As Double = 0.017202791266239
        Const c3 As Double = 0.0000000000000039640538620461
        Const a4 As Double = 6.2565835223195
        Const B4 As Double = 0.017201969767685
        Const c4 As Double = -0.000000000000001962402902003
        Const d4 As Double = -1.0745532659839E-21
        '
        Const L0 As Double = 318.351648 ' Pour le calcul de la lune
        Const P0 As Double = 36.34041
        Const N0 As Double = 318.510107
        Const i As Double = 5.145396
        Const Al As Double = 384401
        Const El As Double = 0.0549
        '
        '
        ' Calcul de la position du soleil sur l'�cliptique
        ' ------------------------------------------------
        ' Les constantes utilis�es sont celles de "Astronomical ephemeris 1969"
        ' page 496, apr�s conversion en radian/jour
        '
        ' Param�tres :
        '   dblJD : date en jours et fraction de jours juliens
        '   xeps : inclinaison de l'�cliptique sur l'�quateur � la date
        '   xe   : excentricit� de la terre sur son orbite
        '   xlm  : longitude vraie sur l'�cliptique � partir de l'�quinoxe
        '
        '
        'j1900 jours juliens (1/01/1900 12 heures)
        j1900 = dblJD - 2415020
        '
        ' inclinaison de l'�cliptique sur l'�quateur
        xeps = (((D1 * j1900) + c1) * j1900 + B1) * j1900 + A1
        ' excentricit�
        xe = ((c2 * j1900) + B2) * j1900 + a2
        ' longitude moyenne
        xlmp = ((c3 * j1900) + B3) * j1900 + A3
        ' anomalie moyenne du soleil (en radians)
        xms = (((d4 * j1900) + c4) * j1900 + B4) * j1900 + a4 ' M
        ' longitude vraie du soleil
        xlm = xlmp + 2 * xe * Sin(xms) + 1.25 * xe * xe * Sin(2 * xms) ' Lambda
        '
        xms = (xms / 2 / pi - Int(xms / 2 / pi)) * 360 ' Anomalie moyenne en degr�s
        xlm = (xlm / 2 / pi - Int(xlm / 2 / pi)) * 360 ' Longitude vraie en degr�s
        '
        ' Calcul de la position de la lune
        ' --------------------------------
        ' R�f�rence : Practical astronomy with your calculator
        ' Peter Duffet-Smith Cambridge university press
        ' Chapitre 65
        '
        ' Nombre de jour depuis le 31/12/1989 0h (Epoch 1990.0)
        d = dblJD - 2447891.5
        ' Correction pour passer en TDT : +60s en 2000 (j50=18262), +32s en 84 (j50=12418))
        d = d + (32 + (dblJD - 2445700.5) * 28 / 5744) / 86400
        '
        ' Calcul des coordonn�es ecliptiques (beta, lambda)
        L = 13.1763966 * d + L0
        If L >= 360 Then L = (L / 360 - Int(L / 360)) * 360
        If L < 0 Then L = 360 - (-L / 360 - Int(-L / 360)) * 360
        '
        m = L - 0.111404 * d - P0
        If m >= 360 Then m = (m / 360 - Int(m / 360)) * 360
        If m < 0 Then m = 360 - (-m / 360 - Int(-m / 360)) * 360
        '
        n = N0 - 0.0529539 * d
        If n >= 360 Then n = (n / 360 - Int(n / 360)) * 360
        If n < 0 Then n = 360 - (-n / 360 - Int(-n / 360)) * 360
        '
        Ev = 1.2739 * Sin((2 * (L - xlm) - m) * pi / 180)
        '
        ae = 0.1858 * Sin(xms * pi / 180)
        xA3 = 0.37 * Sin(xms * pi / 180)
        '
        m = m + Ev - ae - xA3
        '
        Ec = 6.2886 * Sin(m * pi / 180)
        '
        xA4 = 0.214 * Sin(2 * m * pi / 180)
        '
        L = L + Ev + Ec - ae + xA4
        '
        v = 0.6583 * Sin(2 * (L - xlm) * pi / 180)
        '
        L = L + v
        '
        n = n - 0.16 * Sin(xms * pi / 180)
        '
        Y = Sin((L - n) * pi / 180) * Cos(i * pi / 180)
        ' 17
        X = Cos((L - n) * pi / 180)
        ' 18/19
        LBm = Atan2(Y, X) * 180 / pi + n ' Longitude �cliptique en degr�s
        BTm = ArcSin(Sin(i * pi / 180) * Sin((L - n) * pi / 180)) * 180 / pi ' Latitude �cliptique en degr�s
        '
        ' Passage en coordonn�es �quatoriales
        Dim slt, clt As Double
        slt = Sin(BTm * pi / 180) * Cos(xeps) + Cos(BTm * pi / 180) * Sin(xeps) * Sin(LBm * pi / 180)
        clt = Sqrt(1 - slt * slt)
        Y = Sin(LBm * pi / 180) * Cos(xeps) - Tan(BTm * pi / 180) * Sin(xeps)
        X = Cos(LBm * pi / 180)
        lg = Atan2(Y, X) ' en radians
        '
        ' Calcul de la distance lune
        Dim d_lune As Double
        d_lune = Al * (1 - El * El) / (1 + El * Cos((m + Ec) * pi / 180))
        '
        ' Coordonn�es cart�siennes dans gamma
        moon_pos.X = d_lune * clt * Cos(lg)
        moon_pos.Y = d_lune * clt * Sin(lg)
        moon_pos.Z = d_lune * slt
        ''
    End Sub
    ''' <summary>
    ''' Converts coordinates from gamma 50 to geodetic
    '''
    ''' author Colongo
    ''' Procedure to convert the gamma coordinates in longitude, latitude
    ''' and altitude on the greenwich Earth ellipsoid
    ''' </summary>
    ''' <param name="dblJulianNow">Julian date [days]</param>
    ''' <param name="Gamma">coordinates in gamma [km]</param>
    ''' <param name="Geodetic">geodetic coordinates longitude, latitude[rad], altitude[km]</param>
    ''' <remarks></remarks>
    Sub coord_gamma_geodetic(ByRef dblJulianNow As Double, ByRef Gamma As vector, ByRef Geodetic As Lon_Lat)

        Dim ygr, xgr, zgr As Double
        '
        ' Convert from gamma to Greenwich
        With Gamma
            xgr = Cos(temps_sideral(dblJulianNow)) * .X + Sin(temps_sideral(dblJulianNow)) * .Y
            ygr = -Sin(temps_sideral(dblJulianNow)) * .X + Cos(temps_sideral(dblJulianNow)) * .Y
            zgr = .Z
        End With
        ' Convert from Greenwich to geodetic
        Call coord_greenwich_geodetic(xgr, ygr, zgr, Geodetic.lon, Geodetic.lat, Geodetic.alt)
        ''
    End Sub


    ''' <summary>
    ''' converts a julian date to sidereal time
    '''
    ''' author Colongo
    ''' date 2003-01-01
    ''' </summary>
    ''' <param name="julian_epoch">Julian Date [days]</param>
    ''' <returns>return sideral time corresponding to julian epoche [rad]</returns>
    ''' <remarks></remarks>
    Function temps_sideral(ByRef julian_epoch As Double) As Double
        Dim TU, UT, GMST As Double
        '
        UT = Frac(julian_epoch - 0.5)
        TU = (Int(julian_epoch - 0.5) + 0.5 - 2451545.0#) / 36525
        GMST = 24110.54841 + TU * (8640184.812866 + TU * (0.093104 - TU * 0.0000062))
        GMST = Modulo(GMST + secday * omega_E * UT, secday)
        Temps_sideral = 2 * pi * GMST / secday
        ''
    End Function

    ''' <summary>
    ''' Converts greenwich rectangular coordinates to geodetic coordinates
    '''
    ''' author Colongo
    '''
    ''' Procedure to convert ECEF Greenwich coordinates in ECEF Geodetic
    ''' Greenwich coordinates (longitude, latitude, altitude on Earth Ellipsoid)
    ''' </summary>
    ''' <param name="xgr">greenwich rectangular coordinate [km]</param>
    ''' <param name="ygr">greenwich rectangular coordinate [km]</param>
    ''' <param name="zgr">greenwich rectangular coordinate [km]</param>
    ''' <param name="lng">longitude [-180� to 180�]</param>
    ''' <param name="lat">latitude [-90� to 90�]</param>
    ''' <param name="alt">altitude [km]</param>
    ''' <remarks></remarks>
    Sub coord_greenwich_geodetic(ByVal xgr As Double, ByVal ygr As Double, ByVal zgr As Double, ByRef lng As Double, ByRef lat As Double, ByRef alt As Double)
        Const precision As Double = 0.0000000001
        Const PolarRadius As Double = 6356.7523 ' km
        Const aplat As Double = 1 / 298.257223563 ' Flattering factor f of the Earth
        '
        Dim alt0, e2, p, lat0 As Double
        Dim n As Double
        '
        'Calculation of longitude for the differents cases
        '-------------------------------------------------
        If (xgr = 0) Then
            If (ygr > 0) Then lng = pi / 2 Else lng = -pi / 2
        Else
            If (ygr = 0) Then
                If (xgr > 0) Then lng = 0 Else lng = pi
            Else
                lng = Atan(ygr / xgr) '  -pi < longitude < pi
                If (xgr < 0) Then
                    If (ygr < 0) Then lng = lng - pi Else lng = lng + pi
                End If
            End If
        End If
        ' Calculation of latitude and altitude
        ' ------------------------------------
        If ((xgr ^ 2 + ygr ^ 2) < (RE ^ 2 * 1.0E-18)) Then ' Case vertical to the poles
            If (zgr > 0) Then
                alt = zgr - PolarRadius
                lat = pi / 2
            End If
            If (zgr < 0) Then
                alt = -zgr - PolarRadius
                lat = -pi / 2
            End If
            '
        Else ' Outside poles
            If (zgr = 0) Then ' in the equatorial plane
                lat = 0
                alt = Sqrt(xgr ^ 2 + ygr ^ 2) - RE
            Else
                e2 = aplat * (2 - aplat)
                p = Sqrt(xgr ^ 2 + ygr ^ 2)
                n = RE
                alt = Sqrt(xgr ^ 2 + ygr ^ 2 + zgr ^ 2) - RE * Sqrt(1 - aplat)
                lat = Atan((zgr / p) / (1 - (e2 * n) / (n + alt)))

                Do
                    alt0 = alt
                    lat0 = lat
                    n = RE / Sqrt(1 - e2 * Sin(lat0) ^ 2)
                    alt = (p / Cos(lat0)) - n
                    lat = Atan((zgr / p) / (1 - (e2 * n) / (n + alt)))
                Loop Until ((Abs(alt - alt0) < (RE * precision)) And (Abs(lat - lat0) < precision))
            End If
        End If
        '
        ' Conversion into degrees
        ' -----------------------
        lng = lng * deg
        lat = lat * deg
        ''
    End Sub
    ' This type can be used whenever an object has both position and speed.
    Structure pos_vit
        Dim pos_x As Double ' x part of position
        Dim pos_y As Double ' y part of position
        Dim pos_z As Double ' z part of position
        Dim vit_x As Double ' x part of motion
        Dim vit_y As Double ' y part of motion
        Dim vit_z As Double ' z part of motion
    End Structure
    '
    Public Const half_sec As Double = 0.5 / 86400.0#
    Public Const omega_E As Double = 1.00273790934 'Earth rotations per sideral day (non-constant)
    Public Const omega_ER As Double = omega_E * 2 * pi 'Earth rotation, radians per sideral day
    Public Const omega_ERs As Double = 0.000072921158553 'Earth rotation, radians per solar second
    Dim tle_norad(1) As String
    '
    Dim eo, xnodeo, xmo, omegao, xincl As Double
    Dim xndd6o, xno, xndt2o, bstar As Double
    Dim epoch, julian_epoch, ds50 As Double
    Dim ideep, iflag As Short


    Dim arg_per, excent, asc_droite, th As Double
    Dim gom_corrige, i_corrige, a_corrige, e_corrige, pom_corrige, am_corrige As Double
    Dim vy_sat, z_sat, x_sat, y_sat, vx_sat, vz_sat As Double

    '
    Dim xke As Double
    Dim ae As Double
    Dim xkmper As Double 'Earth equatorial radius - kilometers (WGS '72)
    Dim f As Double 'Earth flattening (WGS '72)
    Dim ge As Double 'Earth gravitational constant (WGS '72)
    Dim J2 As Double 'J2 harmonic (WGS '72)
    Dim J3 As Double 'J3 harmonic (WGS '72)
    Dim J4 As Double 'J4 harmonic (WGS '72)
    Dim ck2 As Double
    Dim ck4 As Double
    Dim xj3 As Double
    Dim qo As Double
    Dim s As Double
    Dim e6a As Double
    Dim dpinit As Double 'Deep-space initialization code}
    Dim dpsec As Double 'Deep-space secular code}
    Dim dpper As Double 'Deep-space periodic code}
    '
    '
    '
    'dpinit
    Dim xnodot, xlldot, cosomo, sinomo, ao, cosiq, eqsq, siniq, rteqsq, cosq2, bsq, omgdt, xnodp As Double
    'dpsec/dpper
    Dim t, xinc, xnodes, xll, omgasm, x_em, xn, qoms2t As Double

    ''' <summary>
    ''' Calculate New Orbit with SGP4/SDP4 Propagator
    '''
    ''' author : Christian Colongo (Christian.Colongo@supaero.fr)
    ''' author : Carlos Corral van Damme
    '''
    ''' date : 2000
    '''
    ''' This Propagator is called SPG4/SDP4. During the 1970's, the North American
    ''' Aerospace Defense Command (NORAD) developed a fully-analytical orbital model
    ''' known as Simplified General Pertuabtion, abbreviated SPG. With this model, a
    ''' user can calculate a satellite position and velocity at aparticular time directly,
    ''' without the need for numerical integration. The result is a considerable reduction
    ''' in computational burden. Obviously, certain simplifications had to be made. The
    ''' mass of the satellite is assumed to be negligible compared with the mass of the
    ''' Earth and the satellites are in a low-eccentricity, near-Earth orbit. These
    ''' conditions were true for large percentage of the satellites at the time SPG was
    ''' developed and allowed a model to be formulated accounting only for the primary
    ''' pertubations on these satellites due to the Earth nonuniform mass distribution and
    ''' the atmospheric drag. Subsequent refinements to SPG extended this theory to include
    ''' a semi-analytic treatment of orbits with periods greater than 225 minutes. While
    ''' this theory does depend to some extent upon numerical integration, the computational
    ''' burden is still considerably reduced. The break at 225 minuts is the result of a
    ''' somewhat natural gap which has resulted from historical choices of orbits for
    ''' various satellite missions and a crossover in significant perturbing effects.
    ''' Above the gap the primary pertubing forces are no longer atmospheric drag, but
    ''' orbital resonances with the Earth nonuniform gravitational field, solar and lunar
    ''' gravitational forces and solar pressure. This new model is referred as SGP4/SDP4
    ''' and is the model currently in use by NORAD. The two-line orbital element sets are
    ''' mean Keplerian orbital element sets. The mean values for each element are generated
    ''' using the SPG4/SDP4 orbital model. The effects of the major perturbing forces are
    ''' incorporated into these mean values in a very specific way and SGP4/SDP4 must be
    ''' used to generate accurate predictions. Hence, inputting the NORAD two-line element
    ''' sets into a different moedl will result in degraded predictions, even if the model
    ''' is more accurate. These errors are typically manifested as large time errors in
    ''' satellite signal or visual acquisition [Corral van Damme, 2000]
    ''' </summary>
    ''' <param name="a_moy">Semi-Major Axis of Old Orbit [km]</param>
    ''' <param name="e_moy">Eccentricity of Old Orbit</param>
    ''' <param name="i_moy">Inclination of Old Orbit [rad]</param>
    ''' <param name="pom_moy">Argument of Perige of Old Orbit [rad]</param>
    ''' <param name="gom_moy">RAAN of Old Orbit [rad]</param>
    ''' <param name="am_moy">Mean Anomaly of Old Orbit [rad]</param>
    ''' <param name="dec1">First Time Derivative of the Mean Motion [rad/min]</param>
    ''' <param name="dec2">Second Time Derivative of the Mean Motion[rad/min�]</param>
    ''' <param name="bst">NORAD Drag Coefficient</param>
    ''' <param name="date_bulletin">Epoche in  Julian Date of Old Orbit [days]</param>
    ''' <param name="djules">Julian Date in  Format of New Orbit [days]</param>
    ''' <param name="a_corrige">Semi-Major Axis of New Orbit [km]</param>
    ''' <param name="e_corrige">Eccentricity of NewOrbit</param>
    ''' <param name="i_corrige">Inclination of New Orbit [rad]</param>
    ''' <param name="pom_corrige">Argument of Perige of New Orbit [rad]</param>
    ''' <param name="gom_corrige">RAAN of New Orbit [rad]</param>
    ''' <param name="am_corrige">Mean Anomaly of New Orbit [rad]</param>
    ''' <remarks>todo Change input and output variables to for example type orbit, declaration of variables!</remarks>
    Sub norad_SGP(ByRef a_moy As Double, ByRef e_moy As Double, ByRef i_moy As Double, ByRef pom_moy As Double, ByRef gom_moy As Double, ByRef am_moy As Double, ByRef dec1 As Double, ByRef dec2 As Double, ByRef bst As Double, ByRef date_bulletin As Double, ByRef djules As Double, ByRef a_corrige As Double, ByRef e_corrige As Double, ByRef i_corrige As Double, ByRef pom_corrige As Double, ByRef gom_corrige As Double, ByRef am_corrige As Double)
        '
        'Dim interval As Short
        Dim time As Double
        'Dim delta, 
        Dim tsince As Double
        Dim pos_vel As pos_vit
        Dim xnodp, del1, A1, ao, delo, temp As Double
        '	Dim x50, dg, y50 As Double
        Dim r, v As vector
        Dim SatOrbit As orbit
        '
        '
        '   ------------------
        '   D�but du programme
        '   ------------------
        '
        ' Initialisation des constantes
        ' -----------------------------
        ae = 1

        xkmper = 6378.135 'Earth equatorial radius - [km] (WGS '72)
        f = 1 / 298.26 'Earth flattening (WGS '72)
        ge = 398600.8 'Earth gravitational constant (WGS '72)
        J2 = 0.0010826158 'J2 harmonic (WGS '72)
        J3 = -0.00000253881 'J3 harmonic (WGS '72)
        J4 = -0.00000165597 'J4 harmonic (WGS '72)
        ck2 = J2 / 2
        ck4 = -3 * J4 / 8
        xj3 = J3
        qo = ae + 120 / xkmper
        s = ae + 78 / xkmper
        e6a = 0.000001
        dpinit = 1 'Deep-space initialization code
        dpsec = 2 'Deep-space secular code
        dpper = 3 'Deep-space periodic code
        '
        xke = Sqrt(3600 * ge / Pow(xkmper, 3)) 'Sqr(ge) ER^3/min^2
        qoms2t = (qo - s) * (qo - s) * (qo - s) * (qo - s) '(qo-s)^4 ER^4
        '
        '
        ' Conversion donn�es orbitales en unit�s SGP4/SDP4
        ' ------------------------------------------------

        ' Repasse Grand Omega (exprim� dans gamma50 ) dans Gamma de la date
        ' dans ce cas, les coordonn�es en sortie de SGP4/SDP4 sont exprim�es
        ' dans le rep�re gamma de la date
        '
        ' Sinon, les coordonn�es en sortie de SGP4/SDP4 sont exprim�es
        ' dans le rep�re gamma50
        xnodeo = gom_moy
        '
        eo = e_moy
        omegao = pom_moy ' petit omega (rad)
        xmo = am_moy ' anomalie moyenne (rad)
        xincl = i_moy ' inclinaison (rad)
        xno = Pow(myEarth / Pow(a_moy, 3), 0.5) ' xno en rad/sec
        xno = xno * 60 ' moyen mouvement (rad/mn)
        xndt2o = dec1 * 2 * pi / Pow(minday, 2) ' decay 1 (rad/mn/mn)
        xndd6o = dec2 * 2 * pi / Pow(minday, 3) ' decay 2 (rad/mn/mn/mn)
        bstar = bst
        ' Dates :
        time = djules ' time est en jours juliens
        julian_epoch = date_bulletin ' idem pour la date bulletin
        '
        ' Recherche du propagateur � utiliser (SGP4 ou SDP4)
        ' --------------------------------------------------
        A1 = Pow(xke / xno, 2 / 3)
        temp = 1.5 * ck2 * (3 * Pow(Cos(xincl), 2) - 1) / Pow(1 - eo * eo, 1.5)
        del1 = temp / (A1 * A1)
        ao = A1 * (1 - del1 * (0.5 * 2 / 3 + del1 * (1 + 134 / 81 * del1)))
        delo = temp / (ao * ao)
        xnodp = xno / (1 + delo)
        If (2 * pi / xnodp >= 225) Then
            ideep = 1
        Else
            ideep = 0
        End If
        '
        ' Lancement du calcul
        ' -------------------
        iflag = 1
        '
        tsince = (time - julian_epoch) * minday
        If ideep = 0 Then
            Call norad_SGP4(tsince, iflag, a_corrige, e_corrige, i_corrige, pom_corrige, gom_corrige, am_corrige, pos_vel)
        Else
            Call norad_SDP4(tsince, iflag, a_corrige, e_corrige, i_corrige, pom_corrige, gom_corrige, am_corrige, pos_vel)
        End If
        '
        ' Conversion des r�sultats en unit�s standard
        ' -------------------------------------------
        ' ( Les sorties des propagateurs sont en rayons equatoriaux pour les positions
        ' et en rayons equatoriaux par minutes pour les vitesses. )
        x_sat = pos_vel.pos_x * xkmper ' km
        y_sat = pos_vel.pos_y * xkmper
        z_sat = pos_vel.pos_z * xkmper
        vx_sat = pos_vel.vit_x * xkmper / 60 ' km/s
        vy_sat = pos_vel.vit_y * xkmper / 60
        vz_sat = pos_vel.vit_z * xkmper / 60
        r.X = x_sat
        r.Y = y_sat
        r.Z = z_sat
        v.X = vx_sat
        v.Y = vy_sat
        v.Z = vz_sat
        SatOrbit = SatOrbit0
        Call PosVel_to_Kepler(r, v, SatOrbit)
        With SatOrbit
            a_corrige = .SemiMajor_Axis
            e_corrige = .Eccentricity
            i_corrige = .Inclination * rad
            gom_corrige = .RAAN * rad
            pom_corrige = .Argument_Perigee * rad
            am_corrige = .Mean_Anomaly * rad
        End With
    End Sub

    ''' <summary>
    ''' Sub-Procedure of SGP4/SDP4 Orbital Model for Orbits with altitude over 225 km
    '''
    ''' author : Carlos Corral van Damme
    '''
    ''' date : 2000
    ''' </summary>
    ''' <param name="tsince">ulian days since the Julian Epoche [days]</param>
    ''' <param name="iflag">Flag is set to 1 ?</param>
    ''' <param name="a_corrige">Semi-Major Axis of New Orbit [km]</param>
    ''' <param name="e_corrige">Eccentricity of NewOrbit</param>
    ''' <param name="i_corrige">Inclination of New Orbit [rad]</param>
    ''' <param name="pom_corrige">Argument of Perige of New Orbit [rad]</param>
    ''' <param name="gom_corrige">RAAN of New Orbit [rad]</param>
    ''' <param name="am_corrige">Mean Anomaly of New Orbit [rad]</param>
    ''' <param name="pos_vel">New Position and Velocity [Earth Radii WGS72][(Earth Radii WGS72)/min]</param>
    ''' <remarks></remarks>
    Sub norad_SGP4(ByRef tsince As Double, ByRef iflag As Short, ByRef a_corrige As Double, ByRef e_corrige As Double, ByRef i_corrige As Double, ByRef pom_corrige As Double, ByRef gom_corrige As Double, ByRef am_corrige As Double, ByRef pos_vel As pos_vit)
        '
        ' D�claration des variables
        ' -------------------------

        Static eta, eeta, delmo, d4, d2, coef1, c5, c3, c1sq, betao2, aycof, ao, A1, a3ovk2, aodp, betao, c1, c2, c4, coef, cosio, d3, del1, delo, eosq, etasq As Double
        Static isimp As Short
        Static xnodot, xmdot, xlcof, x7thm1, x1mth2, tsi, theta2, temp2, temp, t4cof, t2cof, sinio, qoms24, pinvsq, omgdot, omgcof, perige, psisq, s4, sinmo, t3cof, t5cof, temp1, temp3, theta4, x1m5th, x3thm1, xhdot1, xmcof, xnodcf, xnodp As Double
        '
        Dim i As Short
        Dim sinean, ean, ydot, Z, X, omgadf, omega, tsq, tempa, templ, delm, tfour, e, beta, axn, aynl, ayn, sinepw, temp4, temp6, ecose, elsq, r, rfdot, cosu, u, cos2u, uk, xinck, sinik, sinnok, xmx, uz, ux, vy, rfdotk, cosuk, sinuk, vx, vz, uy, xmy, cosnok, cosik, rdotk, xnodek, rk, sin2u, sinu, betal, rdot, pl, esine, epw, temp5, cosepw, capu, xlt, xll, xn, xl, a, tcube, delomg, tempe, xnode, xmp, xnoddf, xmdf, Y, xdot, zdot, cosean, man As Double 'ADD by CC


        '
        ' Recover original mean motion (xnodp) and semimajor axis (aodp)
        ' from input elements.
        If (iflag <> 0) Then
            A1 = Pow(xke / xno, 2 / 3)
            cosio = Cos(xincl)
            theta2 = cosio * cosio
            x3thm1 = 3 * theta2 - 1
            eosq = eo * eo
            betao2 = 1 - eosq
            betao = Sqrt(betao2)
            del1 = 1.5 * ck2 * x3thm1 / (A1 * A1 * betao * betao2)
            ao = A1 * (1 - del1 * (0.5 * 2 / 3 + del1 * (1 + 134 / 81 * del1)))
            delo = 1.5 * ck2 * x3thm1 / (ao * ao * betao * betao2)
            xnodp = xno / (1 + delo)
            aodp = ao / (1 - delo)
            '
            ' Initialization
            ' For perigee less than 220 kilometers, the isimp flag is set and
            ' the equations are truncated to linear variation in sqrt a and
            ' quadratic variation in mean anomaly.  Also, the c3 term, the
            ' delta omega term, and the delta m term are dropped.
            isimp = 0
            If (aodp * (1 - eo) / ae) < (220 / xkmper + ae) Then isimp = 1
            ' For perigee below 156 km, the values of s and qoms2t are altered.
            s4 = s
            qoms24 = qoms2t
            perige = (aodp * (1 - eo) - ae) * xkmper
            If (perige < 156) Then
                s4 = perige - 78
                If (perige <= 98) Then s4 = 20
                qoms24 = Pow((120 - s4) * ae / xkmper, 4)
                s4 = s4 / xkmper + ae
            End If
            pinvsq = 1 / (aodp * aodp * betao2 * betao2)
            tsi = 1 / (aodp - s4)
            eta = aodp * eo * tsi
            etasq = eta * eta
            eeta = eo * eta
            psisq = Abs(1 - etasq)
            coef = qoms24 * Pow(tsi, 4)
            coef1 = coef / Pow(psisq, 3.5)
            c2 = coef1 * xnodp * (aodp * (1 + 1.5 * etasq + eeta * (4 + etasq)) + 0.75 * ck2 * tsi / psisq * x3thm1 * (8 + 3 * etasq * (8 + etasq)))
            c1 = bstar * c2
            sinio = Sin(xincl)
            a3ovk2 = -xj3 / ck2 * Pow(ae, 3)
            c3 = coef * tsi * a3ovk2 * xnodp * ae * sinio / eo
            x1mth2 = 1 - theta2
            temp2 = (-3 * x3thm1 * (1 - 2 * eeta + etasq * (1.5 - 0.5 * eeta)) + 0.75 * x1mth2 * (2 * etasq - eeta * (1 + etasq)) * Cos(2 * omegao))
            c4 = 2 * xnodp * coef1 * aodp * betao2 * (eta * (2 + 0.5 * etasq) + eo * (0.5 + 2 * etasq) - 2 * ck2 * tsi / (aodp * psisq) * temp2)
            c5 = 2 * coef1 * aodp * betao2 * (1 + 2.75 * (etasq + eeta) + eeta * etasq)
            theta4 = theta2 * theta2
            temp1 = 3 * ck2 * pinvsq * xnodp
            temp2 = temp1 * ck2 * pinvsq
            temp3 = 1.25 * ck4 * pinvsq * pinvsq * xnodp
            xmdot = xnodp + 0.5 * temp1 * betao * x3thm1 + 0.0625 * temp2 * betao * (13 - 78 * theta2 + 137 * theta4)
            x1m5th = 1 - 5 * theta2
            omgdot = -0.5 * temp1 * x1m5th + 0.0625 * temp2 * (7 - 114 * theta2 + 395 * theta4) + temp3 * (3 - 36 * theta2 + 49 * theta4)
            xhdot1 = -temp1 * cosio
            xnodot = xhdot1 + (0.5 * temp2 * (4 - 19 * theta2) + 2 * temp3 * (3 - 7 * theta2)) * cosio
            omgcof = bstar * c3 * Cos(omegao)
            xmcof = -2 / 3 * coef * bstar * ae / eeta
            xnodcf = 3.5 * betao2 * xhdot1 * c1
            t2cof = 1.5 * c1
            xlcof = 0.125 * a3ovk2 * sinio * (3 + 5 * cosio) / (1 + cosio)
            aycof = 0.25 * a3ovk2 * sinio
            delmo = Pow(1 + eta * Cos(xmo), 3)
            sinmo = Sin(xmo)
            x7thm1 = 7 * theta2 - 1
            If (isimp <> 1) Then
                c1sq = c1 * c1
                d2 = 4 * aodp * tsi * c1sq
                temp = d2 * tsi * c1 / 3
                d3 = (17 * aodp + s4) * temp
                d4 = 0.5 * temp * aodp * tsi * (221 * aodp + 31 * s4) * c1
                t3cof = d2 + 2 * c1sq
                t4cof = 0.25 * (3 * d3 + c1 * (12 * d2 + 10 * c1sq))
                t5cof = 0.2 * (3 * d4 + 12 * c1 * d3 + 6 * d2 * d2 + 15 * c1sq * (2 * d2 + c1sq))
            End If
            iflag = 0
        End If
        ' Update for secular gravity and atmospheric drag.
        xmdf = xmo + xmdot * tsince
        omgadf = omegao + omgdot * tsince
        xnoddf = xnodeo + xnodot * tsince
        omega = omgadf
        xmp = xmdf
        tsq = tsince * tsince
        xnode = xnoddf + xnodcf * tsq
        tempa = 1 - c1 * tsince
        tempe = bstar * c4 * tsince
        templ = t2cof * tsq
        If (isimp <> 1) Then
            delomg = omgcof * tsince
            delm = xmcof * (Pow(1 + eta * Cos(xmdf), 3) - delmo)
            temp = delomg + delm
            xmp = xmdf + temp
            omega = omgadf - temp : arg_per = omega 'argument du perig�e
            If (arg_per < 0) Then
                Do While (arg_per < 0)
                    arg_per = arg_per + (2 * pi)
                Loop
            End If
            arg_per = arg_per - Int(arg_per / (2 * pi)) * (2 * pi) ' modulo 2*pi

            tcube = tsq * tsince
            tfour = tsince * tcube
            tempa = tempa - d2 * tsq - d3 * tcube - d4 * tfour
            tempe = tempe + bstar * c5 * (Sin(xmp) - sinmo)
            templ = templ + t3cof * tcube + tfour * (t4cof + tsince * t5cof)
        End If
        'OJO ESTO ES UNA PRUEBA ####################################
        'aodp = A1
        'HABRA QUE SUPRIMIRLA #######################################
        a = aodp * tempa * tempa : a_corrige = xkmper * a 'Demi axe majeur
        e = eo - tempe : excent = e 'Excentricit�
        xl = xmp + omega + xnode + xnodp * templ
        beta = Sqrt(1 - e * e)
        xn = xke / Pow(a, 1.5)
        ' Long period periodics
        axn = e * Cos(omega)
        temp = 1 / (a * beta * beta)
        xll = temp * xlcof * axn
        aynl = temp * aycof
        xlt = xl + xll
        ayn = e * Sin(omega) + aynl
        ' Solve Kepler's Equation
        capu = Mod2pi(xlt - xnode)
        temp2 = capu
        For i = 1 To 10
            sinepw = Sin(temp2)
            cosepw = Cos(temp2)
            temp3 = axn * sinepw
            temp4 = ayn * cosepw
            temp5 = axn * cosepw
            temp6 = ayn * sinepw
            epw = (capu - temp4 + temp3 - temp2) / (1 - temp5 - temp6) + temp2
            If (Abs(epw - temp2) <= e6a) Then Exit For
            temp2 = epw
        Next i
        ' Short period preliminary quantities
        ecose = temp5 + temp6
        esine = temp3 - temp4
        elsq = axn * axn + ayn * ayn
        temp = 1 - elsq
        pl = a * temp
        r = a * (1 - ecose)
        temp1 = 1 / r
        rdot = xke * Sqrt(a) * esine * temp1
        rfdot = xke * Sqrt(pl) * temp1
        temp2 = a * temp1
        betal = Sqrt(temp)
        temp3 = 1 / (1 + betal)
        cosu = temp2 * (cosepw - axn + ayn * esine * temp3)
        sinu = temp2 * (sinepw - ayn - axn * esine * temp3)
        u = Atan2(sinu, cosu)
        sin2u = 2 * sinu * cosu
        cos2u = 2 * cosu * cosu - 1
        temp = 1 / pl
        temp1 = ck2 * temp
        temp2 = temp1 * temp
        ' Update for short periodics
        rk = r * (1 - 1.5 * temp2 * betal * x3thm1) + 0.5 * temp1 * x1mth2 * cos2u
        uk = u - 0.25 * temp2 * x7thm1 * sin2u : th = uk - arg_per 'anomalie vraie
        'If th > 2 * pi Then th = th - 2 * pi
        'If th < 0 Then th = 2 * pi + th
        xnodek = xnode + 1.5 * temp2 * cosio * sin2u : asc_droite = xnodek 'asc_droite
        If (asc_droite < 0) Then
            Do While (asc_droite < 0)
                asc_droite = asc_droite + (2 * pi)
            Loop
        End If
        asc_droite = asc_droite - Int(asc_droite / (2 * pi)) * (2 * pi) ' modulo 2*pi
        ' --------------------------------------------
        cosean = (excent + Cos(th)) / (1 + (excent * Cos(th)))
        sinean = (Sin(th) * Sqrt(1 - excent * excent)) / (1 + excent * Cos(th))
        ean = Atan2(sinean, cosean)
        man = ean - excent * sinean

        If man > 2 * pi Then man = man - 2 * pi
        If man < 0 Then man = 2 * pi + man

        e_corrige = excent
        pom_corrige = arg_per
        gom_corrige = asc_droite
        am_corrige = man



        xinck = xincl + 1.5 * temp2 * cosio * sinio * cos2u : i_corrige = xinck 'inclinaison
        rdotk = rdot - xn * temp1 * x1mth2 * sin2u
        rfdotk = rfdot + xn * temp1 * (x1mth2 * cos2u + 1.5 * x3thm1)
        ' Orientation vectors
        sinuk = Sin(uk)
        cosuk = Cos(uk)
        sinik = Sin(xinck)
        cosik = Cos(xinck)
        sinnok = Sin(xnodek)
        cosnok = Cos(xnodek)
        xmx = -sinnok * cosik
        xmy = cosnok * cosik
        ux = xmx * sinuk + cosnok * cosuk
        uy = xmy * sinuk + sinnok * cosuk
        uz = sinik * sinuk
        vx = xmx * cosuk - cosnok * sinuk
        vy = xmy * cosuk - sinnok * sinuk
        vz = sinik * cosuk
        ' Position and velocity
        X = rk * ux : pos_vel.pos_x = X
        Y = rk * uy : pos_vel.pos_y = Y
        Z = rk * uz : pos_vel.pos_z = Z
        xdot = rdotk * ux + rfdotk * vx : pos_vel.vit_x = xdot
        ydot = rdotk * uy + rfdotk * vy : pos_vel.vit_y = ydot
        zdot = rdotk * uz + rfdotk * vz : pos_vel.vit_z = zdot


    End Sub

    ''' <summary>
    ''' Updates the Kepler Orbit Elements
    '''
    ''' author : Carlos Corral van Damme
    '''
    ''' date : 2000
    ''' </summary>
    ''' <param name="ideep">0 : Orbit below 225 km; 1 : above 225 km</param>
    ''' <remarks></remarks>
    Sub norad_deep(ByRef ideep As Short)
        '
        Const zns As Double = 0.0000119459 : Const c1ss As Double = 0.0000029864797 : Const zes As Double = 0.01675
        Const znl As Double = 0.00015835218 : Const c1l As Double = 0.00000047968065 : Const zel As Double = 0.0549
        Const zcosis As Double = 0.91744867 : Const zsinis As Double = 0.39785416 : Const zsings As Double = -0.98088458
        Const zcosgs As Double = 0.1945905 'Const zcoshs As Double = 1 'Const zsinhs As Double = 0
        Const q22 As Double = 0.0000017891679 : Const q31 As Double = 0.0000021460748 : Const q33 As Double = 0.00000022123015
        Const g22 As Double = 5.7686396 : Const g32 As Double = 0.95240898 : Const g44 As Double = 1.8014998
        Const g52 As Double = 1.050833 : Const g54 As Double = 4.4108898 : Const root22 As Double = 0.0000017891679
        Const root32 As Double = 0.00000037393792 : Const root44 As Double = 0.0000000073636953 : Const root52 As Double = 0.00000011428639
        Const root54 As Double = 0.0000000021765803 : Const thdt As Double = 0.0043752691
        '
        Static iretn, isynfl, iresfl, iret, ls As Short
        Static g520, g410, g310, g211, g200, fasx6, fasx2, f542, f522, f441, f322, f311, f221, f2, eoc, e3, delt, del2, dbet, dalf, d5421, d5220, d4410, d3210, d2201, cosq, cosis, c, betdp, aqnv, ainv2, a9, a7, a5, A3, A1, a2, a4, a6, a8, a10, alfdp, atime, bfact, cc, cosok, ctem, d2211, d3222, d4422, d5232, d5433, Day, del1, del3, dls, ee2, eq, f220, f3, f321, f330, f442, f523, f543, fasx4, ft, g201, g300, g322, g422, g521 As Double
        Static x7, X5, X3, x2li, x1, temp1, stepp, step2, ssl, ssh, sse, sll, sl3, sl, sinzf, sinok, sini2, si3, si, sh1, sh2, sghs, sgh4, sgh2, ses, se3, se, s7, s5, s3, s1, pl, ph, pe, gam, g532, g533, omegaq, pgh, pinc, preep, s2, s4, s6, savtsn, se2, sel, sgh, sgh3, sghl, sh, sh3, shs, si2, sil, sinis, sinq, sis, sl2, sl4, sls, ssg, ssi, stem, stepn, temp, thgr, x2, x2omi, x4, x6, x8 As Double
        Static zx, zsini, zsinh, zsing, zmos, zmo, zf, zcosil, zcoshl, zcosgl, z33, z31, z23, z21, z13, z11, xqncl, xomi, xnoi, xno2, xndot, xmao, xli, xlamo, xl3, xl, xi2, xh2, xgh3, xfact, xgh2, xgh4, xh3, xi3, xl2, xl4, xldot, xls, xnddt, xni, xnodce, xnq, xpidot, z1, z12, z2, z22, z3, z32, zcosg, zcosh, zcosi, ze, zm, zmol, zn, zsingl, zsinhl, zsinil, zy As Double
        '
        '
        '
        Select Case ideep
            '
            Case dpinit
                ' Entrance for deep space initialization
                ' --------------------------------------
                thgr = temps_sideral(julian_epoch)
                eq = eo
                xnq = xnodp
                aqnv = 1 / ao
                xqncl = xincl
                xmao = xmo
                xpidot = omgdt + xnodot
                sinq = Sin(xnodeo)
                cosq = Cos(xnodeo)
                omegaq = omegao
                ' Initialize lunar solar terms
                Day = ds50 + 18261.5 'Days since 1900 Jan 0.5
                If (Day <> preep) Then
                    preep = Day
                    xnodce = 4.523602 - 0.00092422029 * Day
                    stem = Sin(xnodce)
                    ctem = Cos(xnodce)
                    zcosil = 0.91375164 - 0.03568096 * ctem
                    zsinil = Sqrt(1 - zcosil * zcosil)
                    zsinhl = 0.089683511 * stem / zsinil
                    zcoshl = Sqrt(1 - zsinhl * zsinhl)
                    c = 4.7199672 + 0.2299715 * Day
                    gam = 5.8351514 + 0.001944368 * Day
                    zmol = Mod2pi(c - gam)
                    zx = 0.39785416 * stem / zsinil
                    zy = zcoshl * ctem + 0.91744867 * zsinhl * stem
                    zx = Atan2(zx, zy)
                    zx = gam + zx - xnodce
                    zcosgl = Cos(zx)
                    zsingl = Sin(zx)
                    zmos = 6.2565837 + 0.017201977 * Day
                    zmos = Mod2pi(zmos)
                End If
                ' Do solar terms
                savtsn = 1.0E+20
                zcosg = zcosgs
                zsing = zsings
                zcosi = zcosis
                zsini = zsinis
                zcosh = cosq
                zsinh = sinq
                cc = c1ss
                zn = zns
                ze = zes
                zmo = zmos
                xnoi = 1 / xnq
                ls = 30 'assign 30 to ls
                '
                Do
                    A1 = zcosg * zcosh + zsing * zcosi * zsinh
                    A3 = -zsing * zcosh + zcosg * zcosi * zsinh
                    a7 = -zcosg * zsinh + zsing * zcosi * zcosh
                    a8 = zsing * zsini
                    a9 = zsing * zsinh + zcosg * zcosi * zcosh
                    a10 = zcosg * zsini
                    a2 = cosiq * a7 + siniq * a8
                    a4 = cosiq * a9 + siniq * a10
                    a5 = -siniq * a7 + cosiq * a8
                    a6 = -siniq * a9 + cosiq * a10
                    x1 = A1 * cosomo + a2 * sinomo
                    x2 = A3 * cosomo + a4 * sinomo
                    X3 = -A1 * sinomo + a2 * cosomo
                    x4 = -A3 * sinomo + a4 * cosomo
                    X5 = a5 * sinomo
                    x6 = a6 * sinomo
                    x7 = a5 * cosomo
                    x8 = a6 * cosomo
                    z31 = 12 * x1 * x1 - 3 * X3 * X3
                    z32 = 24 * x1 * x2 - 6 * X3 * x4
                    z33 = 12 * x2 * x2 - 3 * x4 * x4
                    z1 = 3 * (A1 * A1 + a2 * a2) + z31 * eqsq
                    z2 = 6 * (A1 * A3 + a2 * a4) + z32 * eqsq
                    z3 = 3 * (A3 * A3 + a4 * a4) + z33 * eqsq
                    z11 = -6 * A1 * a5 + eqsq * (-24 * x1 * x7 - 6 * X3 * X5)
                    z12 = -6 * (A1 * a6 + A3 * a5) + eqsq * (-24 * (x2 * x7 + x1 * x8) - 6 * (X3 * x6 + x4 * X5))
                    z13 = -6 * A3 * a6 + eqsq * (-24 * x2 * x8 - 6 * x4 * x6)
                    z21 = 6 * a2 * a5 + eqsq * (24 * x1 * X5 - 6 * X3 * x7)
                    z22 = 6 * (a4 * a5 + a2 * a6) + eqsq * (24 * (x2 * X5 + x1 * x6) - 6 * (x4 * x7 + X3 * x8))
                    z23 = 6 * a4 * a6 + eqsq * (24 * x2 * x6 - 6 * x4 * x8)
                    z1 = z1 + z1 + bsq * z31
                    z2 = z2 + z2 + bsq * z32
                    z3 = z3 + z3 + bsq * z33
                    s3 = cc * xnoi
                    s2 = -0.5 * s3 / rteqsq
                    s4 = s3 * rteqsq
                    s1 = -15 * eq * s4
                    s5 = x1 * X3 + x2 * x4
                    s6 = x2 * X3 + x1 * x4
                    s7 = x2 * x4 - x1 * X3
                    se = s1 * zn * s5
                    si = s2 * zn * (z11 + z13)
                    sl = -zn * s3 * (z1 + z3 - 14 - 6 * eqsq)
                    sgh = s4 * zn * (z31 + z33 - 6)
                    sh = -zn * s2 * (z21 + z23)
                    If (xqncl < 0.052359877) Then sh = 0
                    ee2 = 2 * s1 * s6
                    e3 = 2 * s1 * s7
                    xi2 = 2 * s2 * z12
                    xi3 = 2 * s2 * (z13 - z11)
                    xl2 = -2 * s3 * z2
                    xl3 = -2 * s3 * (z3 - z1)
                    xl4 = -2 * s3 * (-21 - 9 * eqsq) * ze
                    xgh2 = 2 * s4 * z32
                    xgh3 = 2 * s4 * (z33 - z31)
                    xgh4 = -18 * s4 * ze
                    xh2 = -2 * s2 * z22
                    xh3 = -2 * s2 * (z23 - z21)
                    Select Case ls
                        Case 40
                            Exit Do
                        Case 30
                            ' Do lunar terms
                            sse = se
                            ssi = si
                            ssl = sl
                            ssh = sh / siniq
                            ssg = sgh - cosiq * ssh
                            se2 = ee2
                            si2 = xi2
                            sl2 = xl2
                            sgh2 = xgh2
                            sh2 = xh2
                            se3 = e3
                            si3 = xi3
                            sl3 = xl3
                            sgh3 = xgh3
                            sh3 = xh3
                            sl4 = xl4
                            sgh4 = xgh4
                            zcosg = zcosgl
                            zsing = zsingl
                            zcosi = zcosil
                            zsini = zsinil
                            zcosh = zcoshl * cosq + zsinhl * sinq
                            zsinh = sinq * zcoshl - cosq * zsinhl
                            zn = znl
                            cc = c1l
                            ze = zel
                            zmo = zmol
                            ls = 40 'assign 40 to ls
                        Case Else
                            End
                    End Select ' Select case ls
                    '
                Loop
                '
                sse = sse + se
                ssi = ssi + si
                ssl = ssl + sl
                ssg = ssg + sgh - cosiq / siniq * sh
                ssh = ssh + sh / siniq
                '
                ' Geopotential resonance initialization for 12 hour orbits
                iresfl = 0
                isynfl = 0
                If (xnq < 0.0052359877) And (xnq > 0.0034906585) Then
                    ' Synchronous resonance terms initialization
                    iresfl = 1
                    isynfl = 1
                    g200 = 1 + eqsq * (-2.5 + 0.8125 * eqsq)
                    g310 = 1 + 2 * eqsq
                    g300 = 1 + eqsq * (-6 + 6.60937 * eqsq)
                    f220 = 0.75 * (1 + cosiq) * (1 + cosiq)
                    f311 = 0.9375 * siniq * siniq * (1 + 3 * cosiq) - 0.75 * (1 + cosiq)
                    f330 = 1 + cosiq
                    f330 = 1.875 * f330 * f330 * f330
                    del1 = 3 * xnq * xnq * aqnv * aqnv
                    del2 = 2 * del1 * f220 * g200 * q22
                    del3 = 3 * del1 * f330 * g300 * q33 * aqnv
                    del1 = del1 * f311 * g310 * q31 * aqnv
                    fasx2 = 0.13130908
                    fasx4 = 2.8843198
                    fasx6 = 0.37448087
                    xlamo = xmao + xnodeo + omegao - thgr
                    bfact = xlldot + xpidot - thdt
                    bfact = bfact + ssl + ssg + ssh
                Else
                    If (xnq < 0.00826) Or (xnq > 0.00924) Then Exit Sub
                    If (eq < 0.5) Then Exit Sub
                    iresfl = 1
                    eoc = eq * eqsq
                    g201 = -0.306 - (eq - 0.64) * 0.44
                    '
                    If (eq > 0.65) Then
                        g211 = -72.099 + 331.819 * eq - 508.738 * eqsq + 266.724 * eoc
                        g310 = -346.844 + 1582.851 * eq - 2415.925 * eqsq + 1246.113 * eoc
                        g322 = -342.585 + 1554.908 * eq - 2366.899 * eqsq + 1215.972 * eoc
                        g410 = -1052.797 + 4758.686 * eq - 7193.992 * eqsq + 3651.957 * eoc
                        g422 = -3581.69 + 16178.11 * eq - 24462.77 * eqsq + 12422.52 * eoc
                        If (eq > 0.715) Then
                            g520 = -5149.66 + 29936.92 * eq - 54087.36 * eqsq + 31324.56 * eoc
                        Else
                            g520 = 1464.74 - 4664.75 * eq + 3763.64 * eqsq
                        End If
                    Else
                        g211 = 3.616 - 13.247 * eq + 16.29 * eqsq
                        g310 = -19.302 + 117.39 * eq - 228.419 * eqsq + 156.591 * eoc
                        g322 = -18.9068 + 109.7927 * eq - 214.6334 * eqsq + 146.5816 * eoc
                        g410 = -41.122 + 242.694 * eq - 471.094 * eqsq + 313.953 * eoc
                        g422 = -146.407 + 841.88 * eq - 1629.014 * eqsq + 1083.435 * eoc
                        g520 = -532.114 + 3017.977 * eq - 5740 * eqsq + 3708.276 * eoc
                    End If
                    '
                    If (eq >= (0.7)) Then
                        g533 = -37995.78 + 161616.52 * eq - 229838.2 * eqsq + 109377.94 * eoc
                        g521 = -51752.104 + 218913.95 * eq - 309468.16 * eqsq + 146349.42 * eoc
                        g532 = -40023.88 + 170470.89 * eq - 242699.48 * eqsq + 115605.82 * eoc
                    Else
                        g533 = -919.2277 + 4988.61 * eq - 9064.77 * eqsq + 5542.21 * eoc
                        g521 = -822.71072 + 4568.6173 * eq - 8491.4146 * eqsq + 5337.524 * eoc
                        g532 = -853.666 + 4690.25 * eq - 8624.77 * eqsq + 5341.4 * eoc
                    End If
                    '
                    sini2 = siniq * siniq
                    f220 = 0.75 * (1 + 2 * cosiq + cosq2)
                    f221 = 1.5 * sini2
                    f321 = 1.875 * siniq * (1 - 2 * cosiq - 3 * cosq2)
                    f322 = -1.875 * siniq * (1 + 2 * cosiq - 3 * cosq2)
                    f441 = 35 * sini2 * f220
                    f442 = 39.375 * sini2 * sini2
                    f522 = 9.84375 * siniq * (sini2 * (1 - 2 * cosiq - 5 * cosq2) + 0.33333333 * (-2 + 4 * cosiq + 6 * cosq2))
                    f523 = siniq * (4.92187512 * sini2 * (-2 - 4 * cosiq + 10 * cosq2) + 6.56250012 * (1 + 2 * cosiq - 3 * cosq2))
                    f542 = 29.53125 * siniq * (2 - 8 * cosiq + cosq2 * (-12 + 8 * cosiq + 10 * cosq2))
                    f543 = 29.53125 * siniq * (-2 - 8 * cosiq + cosq2 * (12 + 8 * cosiq - 10 * cosq2))
                    xno2 = xnq * xnq
                    ainv2 = aqnv * aqnv
                    temp1 = 3 * xno2 * ainv2
                    temp = temp1 * root22
                    d2201 = temp * f220 * g201
                    d2211 = temp * f221 * g211
                    temp1 = temp1 * aqnv
                    temp = temp1 * root32
                    d3210 = temp * f321 * g310
                    d3222 = temp * f322 * g322
                    temp1 = temp1 * aqnv
                    temp = 2 * temp1 * root44
                    d4410 = temp * f441 * g410
                    d4422 = temp * f442 * g422
                    temp1 = temp1 * aqnv
                    temp = temp1 * root52
                    d5220 = temp * f522 * g520
                    d5232 = temp * f523 * g532
                    temp = 2 * temp1 * root54
                    d5421 = temp * f542 * g521
                    d5433 = temp * f543 * g533
                    xlamo = xmao + xnodeo + xnodeo - thgr - thgr
                    bfact = xlldot + xnodot + xnodot - thdt - thdt
                    bfact = bfact + ssl + ssh + ssh
                End If
                '
                xfact = bfact - xnq
                ' Initialize integrator
                xli = xlamo
                xni = xnq
                atime = 0
                stepp = 720
                stepn = -720
                step2 = 259200
                ' Fin de case dpinit
                '

            Case dpsec
                ' Entrance for deep space secular effects
                xll = xll + ssl * t
                omgasm = omgasm + ssg * t
                xnodes = xnodes + ssh * t
                x_em = eo + sse * t
                xinc = xincl + ssi * t
                '
                If (xinc < 0) Then
                    xinc = -xinc
                    xnodes = xnodes + pi
                    omgasm = omgasm - pi
                End If
                '
                If (iresfl = 0) Then Exit Sub
                '
100:            If (atime = 0) Or ((t >= 0) And (atime < 0)) Or ((t < 0) And (atime >= 0)) Then
                    GoTo 170
                End If
                If (Abs(t) >= Abs(atime)) Then
                    GoTo 120
                End If
                delt = stepp
                If (t >= 0) Then delt = stepn
                iret = 100 'assign 100 to iret
                GoTo 160
120:            delt = stepn
                If (t > 0) Then delt = stepp
125:            If (Abs(t - atime) >= stepp) Then
                    iret = 125 'assign 125 to iret
                    GoTo 160
                End If
                ft = t - atime
                iretn = 140 'assign 140 to iretn
                GoTo 150
140:            xn = xni + xndot * ft + xnddt * ft * ft * 0.5
                xl = xli + xldot * ft + xndot * ft * ft * 0.5
                temp = -xnodes + thgr + t * thdt
                xll = xl - omgasm + temp
                If (isynfl = 0) Then xll = xl + temp + temp
                Exit Sub

                ' Dot terms calculated
150:            If (isynfl = 0) Then
                    xomi = omegaq + omgdt * atime
                    x2omi = xomi + xomi
                    x2li = xli + xli
                    xndot = d2201 * Sin(x2omi + xli - g22) + d2211 * Sin(xli - g22) + d3210 * Sin(xomi + xli - g32) + d3222 * Sin(-xomi + xli - g32) + d4410 * Sin(x2omi + x2li - g44) + d4422 * Sin(x2li - g44) + d5220 * Sin(xomi + xli - g52) + d5232 * Sin(-xomi + xli - g52) + d5421 * Sin(xomi + x2li - g54) + d5433 * Sin(-xomi + x2li - g54)
                    xnddt = d2201 * Cos(x2omi + xli - g22) + d2211 * Cos(xli - g22) + d3210 * Cos(xomi + xli - g32) + d3222 * Cos(-xomi + xli - g32) + d5220 * Cos(xomi + xli - g52) + d5232 * Cos(-xomi + xli - g52) + 2 * (d4410 * Cos(x2omi + x2li - g44) + d4422 * Cos(x2li - g44) + d5421 * Cos(xomi + x2li - g54) + d5433 * Cos(-xomi + x2li - g54))
                Else
                    xndot = del1 * Sin(xli - fasx2) + del2 * Sin(2 * (xli - fasx4)) + del3 * Sin(3 * (xli - fasx6))
                    xnddt = del1 * Cos(xli - fasx2) + 2 * del2 * Cos(2 * (xli - fasx4)) + 3 * del3 * Cos(3 * (xli - fasx6))
                End If
                '
                xldot = xni + xfact
                xnddt = xnddt * xldot
                Select Case iretn
                    Case 140 : GoTo 140
                    Case 165 : GoTo 165
                    Case Else : End
                End Select



                ' Integrator
160:            iretn = 165 'assign 165 to iretn
                GoTo 150
165:            xli = xli + xldot * delt + xndot * step2
                xni = xni + xndot * delt + xnddt * step2
                atime = atime + delt
                Select Case iret
                    Case 100 : GoTo 100
                    Case 125 : GoTo 125
                    Case Else : End
                End Select 'case
                ' Epoch restart
170:            If (t >= 0) Then
                    delt = stepp
                Else
                    delt = stepn
                End If
                atime = 0
                xni = xnq
                xli = xlamo
                GoTo 125
                ' Fin de case dpsec
                '
                '
            Case dpper
                ' Entrance for lunar-solar periodics
                sinis = Sin(xinc)
                cosis = Cos(xinc)
                If (Abs(savtsn - t) >= 30) Then
                    savtsn = t
                    zm = zmos + zns * t
                    zf = zm + 2 * zes * Sin(zm)
                    sinzf = Sin(zf)
                    f2 = 0.5 * sinzf * sinzf - 0.25
                    f3 = -0.5 * sinzf * Cos(zf)
                    ses = se2 * f2 + se3 * f3
                    sis = si2 * f2 + si3 * f3
                    sls = sl2 * f2 + sl3 * f3 + sl4 * sinzf
                    sghs = sgh2 * f2 + sgh3 * f3 + sgh4 * sinzf
                    shs = sh2 * f2 + sh3 * f3
                    zm = zmol + znl * t
                    zf = zm + 2 * zel * Sin(zm)
                    sinzf = Sin(zf)
                    f2 = 0.5 * sinzf * sinzf - 0.25
                    f3 = -0.5 * sinzf * Cos(zf)
                    sel = ee2 * f2 + e3 * f3
                    sil = xi2 * f2 + xi3 * f3
                    sll = xl2 * f2 + xl3 * f3 + xl4 * sinzf
                    sghl = xgh2 * f2 + xgh3 * f3 + xgh4 * sinzf
                    sh1 = xh2 * f2 + xh3 * f3
                    pe = ses + sel
                    pinc = sis + sil
                    pl = sls + sll
                End If
                pgh = sghs + sghl
                ph = shs + sh1
                xinc = xinc + pinc
                x_em = x_em + pe
                If (xqncl < 0.2) Then
                    ' Apply periodics with Lyddane modification
                    sinok = Sin(xnodes)
                    cosok = Cos(xnodes)
                    alfdp = sinis * sinok
                    betdp = sinis * cosok
                    dalf = ph * cosok + pinc * cosis * sinok
                    dbet = -ph * sinok + pinc * cosis * cosok
                    alfdp = alfdp + dalf
                    betdp = betdp + dbet
                    xls = xll + omgasm + cosis * xnodes
                    dls = pl + pgh - pinc * xnodes * sinis
                    xls = xls + dls
                    xnodes = Atan2(alfdp, betdp)
                    xll = xll + pl
                    omgasm = xls - xll - Cos(xinc) * xnodes
                Else
                    ' Apply periodics directly
                    ph = ph / siniq
                    pgh = pgh - cosiq * ph
                    omgasm = omgasm + pgh
                    xnodes = xnodes + ph
                    xll = xll + pl
                End If
                '
                ' Fin de case dpper
        End Select ' ideep
        ''
    End Sub

    ''' <summary>
    ''' Deep Space Initialization
    '''
    ''' author : Carlos Corral van Damme
    ''' date : 2000
    ''' </summary>
    ''' <param name="eosq">Square of Eccentricity of Old Orbit</param>
    ''' <param name="sinio">Sinus of Inclination of Old Orbit</param>
    ''' <param name="cosio">Cosinus of Inclination of Old Orbit</param>
    ''' <param name="betao">Squareroot of (1-Square(Eccentricity of Old Orbit))</param>
    ''' <param name="aodp">Recovered original Semi-major Axis from input elements [km?]</param>
    ''' <param name="theta2">Square of Cosinus of Invlination of Old Orbit</param>
    ''' <param name="sing">Sinus of RAAN of Old Orbit</param>
    ''' <param name="cosg">Cosinus of RAAN of Old Orbit]</param>
    ''' <param name="betao2">(1-Square(Eccentricity of Old Orbit))</param>
    ''' <param name="xmdot">Derivative of Semimajor Axis [km/min]</param>
    ''' <param name="omgdot">Derivative of Argument of Perigee [rad/min]</param>
    ''' <param name="xnodott">Derivative of RAAN [rad/min]</param>
    ''' <param name="xnodpp">Recovered original Mean Motion [?]</param>
    ''' <remarks></remarks>
    Sub norad_dpinit(ByRef eosq As Double, ByRef sinio As Double, ByRef cosio As Double, ByRef betao As Double, ByRef aodp As Double, ByRef theta2 As Double, ByRef sing As Double, ByRef cosg As Double, ByRef betao2 As Double, ByRef xmdot As Double, ByRef omgdot As Double, ByRef xnodott As Double, ByRef xnodpp As Double)
        '
        eqsq = eosq
        siniq = sinio
        cosiq = cosio
        rteqsq = betao
        ao = aodp
        cosq2 = theta2
        sinomo = sing
        cosomo = cosg
        bsq = betao2
        xlldot = xmdot
        omgdt = omgdot
        xnodot = xnodott
        xnodp = xnodpp
        Call norad_deep(1)
        eosq = eqsq
        sinio = siniq
        cosio = cosiq
        betao = rteqsq
        aodp = ao
        theta2 = cosq2
        sing = sinomo
        cosg = cosomo
        betao2 = bsq
        xmdot = xlldot
        omgdot = omgdt
        xnodott = xnodot
        xnodpp = xnodp
        ''
    End Sub

    ''' <summary>
    ''' Deep Space Secular ?
    '''
    ''' author : Carlos Corral van Damme
    '''
    '''
    ''' date : 2000
    ''' </summary>
    ''' <param name="xmdf">Updated Semimajor Axis [km] (Secular Gravity and Drag)</param>
    ''' <param name="omgadf">Updated Argument of Perigee [rad] (Secular Gravity and Drag)</param>
    ''' <param name="xnode">Updated RAAN [rad] (Secular Gravity and Drag)</param>
    ''' <param name="emm">Updated Eccentricity</param>
    ''' <param name="xincc">updated inclination [rad]</param>
    ''' <param name="xnn">Updated Mean motion [rad/min]</param>
    ''' <param name="tsince">Time since Reference Julian Date [min]</param>
    ''' <remarks></remarks>
    Sub norad_dpsec(ByRef xmdf As Double, ByRef omgadf As Double, ByRef xnode As Double, ByRef emm As Double, ByRef xincc As Double, ByRef xnn As Double, ByRef tsince As Double)
        '
        xll = xmdf
        omgasm = omgadf
        xnodes = xnode
        xn = xnn
        t = tsince
        Call norad_deep(2)
        xmdf = xll
        omgadf = omgasm
        xnode = xnodes
        emm = x_em
        xincc = xinc
        xnn = xn
        tsince = t
        ''
    End Sub

    ''' <summary>
    ''' Deep Space periodic ?
    '''
    ''' author : Carlos Corral van Damme
    ''' date : 2000
    ''' </summary>
    ''' <param name="e">Eccentricity</param>
    ''' <param name="xincc">Inclination [rad]</param>
    ''' <param name="omgadf">Argument of perigee [rad]</param>
    ''' <param name="xnode">RAAN [rad]</param>
    ''' <param name="xmam">Semi-major Axis [km]</param>
    ''' <remarks></remarks>
    Sub norad_dpper(ByRef e As Double, ByRef xincc As Double, ByRef omgadf As Double, ByRef xnode As Double, ByRef xmam As Double)
        '
        x_em = e
        xinc = xincc
        omgasm = omgadf
        xnodes = xnode
        xll = xmam
        Call norad_deep(3)
        e = x_em
        xincc = xinc
        omgadf = omgasm
        xnode = xnodes
        xmam = xll
        ''
    End Sub

    ''' <summary>
    ''' Sub-Procedure of SGP4/SDP4 Orbital Model for Orbits with altitude below 225 km
    '''
    ''' author : Carlos Corral van Damme
    '''
    ''' date : 2000
    ''' </summary>
    ''' <param name="tsince">Time since the Julian Epoche [min]</param>
    ''' <param name="iflag">Flag is set to 1 ?</param>
    ''' <param name="a_corrige">Semi-Major Axis of New Orbit [km]</param>
    ''' <param name="e_corrige">Eccentricity of NewOrbit</param>
    ''' <param name="i_corrige">Inclination of New Orbit [rad]</param>
    ''' <param name="pom_corrige">Argument of Perige of New Orbit [rad]</param>
    ''' <param name="gom_corrige">RAAN of New Orbit [rad]</param>
    ''' <param name="am_corrige">Mean Anomaly of New Orbit [rad]</param>
    ''' <param name="pos_vel">New Position and Velocity [Earth Radii WGS72] [(Earth Radii WGS72)/min]</param>
    ''' <remarks></remarks>
    Sub norad_SDP4(ByRef tsince As Double, ByRef iflag As Short, ByRef a_corrige As Double, ByRef e_corrige As Double, ByRef i_corrige As Double, ByRef pom_corrige As Double, ByRef gom_corrige As Double, ByRef am_corrige As Double, ByRef pos_vel As pos_vit)
        '
        Static xnodot, xmdot, xhdot1, x3thm1, x1m5th, theta4, temp3, temp1, sinio, s4, psisq, perige, etasq, eosq, delo, cosio, coef1, c4, c1, betao, aodp, a3ovk2, A1, ao, aycof, betao2, c2, coef, cosg, del1, eeta, eta, omgdot, pinvsq, qoms24, sing, t2cof, temp2, theta2, tsi, x1mth2, x7thm1, xlcof, xnodcf, xnodp As Double
        '
        Dim i As Short
        '
        Dim sinean, ean, ydot, Z, X, xnode, xn, xmx, xmam, xll, xinck, vz, vx, uy, uk, tsq, tempe, temp6, temp4, sinuk, sinnok, sinepw, rk, rfdot, rdot, pl, esine, em, ecose, cosuk, cosnok, cosepw, capu, beta, ayn, a, axn, aynl, betal, cos2u, cosik, cosu, e, elsq, epw, omgadf, r, rdotk, rfdotk, sin2u, sinik, sinu, temp, temp5, tempa, templ, u, ux, uz, vy, xinc, xl, xlt, xmdf, xmy, xnoddf, xnodek, Y, xdot, zdot, cosean, man As Double
        '
        If iflag <> 0 Then
            ' Recover original mean motion (xnodp) and semimajor axis (aodp) from input elements.
            A1 = Pow(xke / xno, 2 / 3)
            cosio = Cos(xincl)
            theta2 = cosio * cosio
            x3thm1 = 3 * theta2 - 1
            eosq = eo * eo
            betao2 = 1 - eosq
            betao = Sqrt(betao2)
            del1 = 1.5 * ck2 * x3thm1 / (A1 * A1 * betao * betao2)
            ao = A1 * (1 - del1 * (0.5 * 2 / 3 + del1 * (1 + 134 / 81 * del1)))
            delo = 1.5 * ck2 * x3thm1 / (ao * ao * betao * betao2)
            xnodp = xno / (1 + delo)
            aodp = ao / (1 - delo)
            ' Initialization
            ' For perigee below 156 km, the values of s and qoms2t are altered.
            s4 = s
            qoms24 = qoms2t
            perige = (aodp * (1 - eo) - ae) * xkmper

            If (perige < 156) Then
                s4 = perige - 78
                If (perige <= 98) Then s4 = 20
                qoms24 = Pow((120 - s4) * ae / xkmper, 4)
                s4 = s4 / xkmper + ae
            End If
            pinvsq = 1 / (aodp * aodp * betao2 * betao2)
            sing = Sin(omegao)
            cosg = Cos(omegao)
            tsi = 1 / (aodp - s4)
            eta = aodp * eo * tsi
            etasq = eta * eta
            eeta = eo * eta
            psisq = Abs(1 - etasq)
            coef = qoms24 * Pow(tsi, 4)
            coef1 = coef / Pow(psisq, 3.5)
            c2 = coef1 * xnodp * (aodp * (1 + 1.5 * etasq + eeta * (4 + etasq)) + 0.75 * ck2 * tsi / psisq * x3thm1 * (8 + 3 * etasq * (8 + etasq)))
            c1 = bstar * c2
            sinio = Sin(xincl)
            a3ovk2 = -xj3 / ck2 * Pow(ae, 3)
            x1mth2 = 1 - theta2
            temp2 = (-3 * x3thm1 * (1 - 2 * eeta + etasq * (1.5 - 0.5 * eeta)) + 0.75 * x1mth2 * (2 * etasq - eeta * (1 + etasq)) * Cos(2 * omegao))
            c4 = 2 * xnodp * coef1 * aodp * betao2 * (eta * (2 + 0.5 * etasq) + eo * (0.5 + 2 * etasq) - 2 * ck2 * tsi / (aodp * psisq) * temp2)
            theta4 = theta2 * theta2
            temp1 = 3 * ck2 * pinvsq * xnodp
            temp2 = temp1 * ck2 * pinvsq
            temp3 = 1.25 * ck4 * pinvsq * pinvsq * xnodp
            xmdot = xnodp + 0.5 * temp1 * betao * x3thm1 + 0.0625 * temp2 * betao * (13 - 78 * theta2 + 137 * theta4)
            x1m5th = 1 - 5 * theta2
            omgdot = -0.5 * temp1 * x1m5th + 0.0625 * temp2 * (7 - 114 * theta2 + 395 * theta4) + temp3 * (3 - 36 * theta2 + 49 * theta4)
            xhdot1 = -temp1 * cosio
            xnodot = xhdot1 + (0.5 * temp2 * (4 - 19 * theta2) + 2 * temp3 * (3 - 7 * theta2)) * cosio
            xnodcf = 3.5 * betao2 * xhdot1 * c1
            t2cof = 1.5 * c1
            xlcof = 0.125 * a3ovk2 * sinio * (3 + 5 * cosio) / (1 + cosio)
            aycof = 0.25 * a3ovk2 * sinio
            x7thm1 = 7 * theta2 - 1
            iflag = 0
            Call norad_dpinit(eosq, sinio, cosio, betao, aodp, theta2, sing, cosg, betao2, xmdot, omgdot, xnodot, xnodp)
        End If
        ' Update for secular gravity and atmospheric drag
        xmdf = xmo + xmdot * tsince
        omgadf = omegao + omgdot * tsince : arg_per = omgadf 'Mod (2 * pi)   'argument du perig�e
        If (arg_per < 0) Then
            Do While (arg_per < 0)
                arg_per = arg_per + (2 * pi)
            Loop
        End If
        arg_per = arg_per - Int(arg_per / (2 * pi)) * (2 * pi) ' modulo 2*pi

        xnoddf = xnodeo + xnodot * tsince
        tsq = tsince * tsince
        xnode = xnoddf + xnodcf * tsq
        tempa = 1 - c1 * tsince
        tempe = bstar * c4 * tsince
        templ = t2cof * tsq
        xn = xnodp
        Call norad_dpsec(xmdf, omgadf, xnode, em, xinc, xn, tsince)
        a = Pow(xke / xn, 2 / 3) * tempa * tempa : a_corrige = xkmper * a 'demi axe majeur
        e = em - tempe : excent = e 'excentricit�
        xmam = xmdf + xnodp * templ
        Call norad_dpper(e, xinc, omgadf, xnode, xmam)
        xl = xmam + omgadf + xnode
        beta = Sqrt(1 - e * e)
        xn = xke / Pow(a, 1.5)
        ' Long period periodics
        axn = e * Cos(omgadf)
        temp = 1 / (a * beta * beta)
        xll = temp * xlcof * axn
        aynl = temp * aycof
        xlt = xl + xll
        ayn = e * Sin(omgadf) + aynl
        ' Solve Kepler's Equation
        capu = Mod2pi(xlt - xnode)
        temp2 = capu
        For i = 1 To 10
            sinepw = Sin(temp2)
            cosepw = Cos(temp2)
            temp3 = axn * sinepw
            temp4 = ayn * cosepw
            temp5 = axn * cosepw
            temp6 = ayn * sinepw
            epw = (capu - temp4 + temp3 - temp2) / (1 - temp5 - temp6) + temp2
            If (Abs(epw - temp2) <= e6a) Then Exit For
            temp2 = epw
        Next i
        ' Short period preliminary quantities
        ecose = temp5 + temp6
        esine = temp3 - temp4
        elsq = axn * axn + ayn * ayn
        temp = 1 - elsq
        pl = a * temp
        r = a * (1 - ecose)
        temp1 = 1 / r
        rdot = xke * Sqrt(a) * esine * temp1
        rfdot = xke * Sqrt(pl) * temp1
        temp2 = a * temp1
        betal = Sqrt(temp)
        temp3 = 1 / (1 + betal)
        cosu = temp2 * (cosepw - axn + ayn * esine * temp3)
        sinu = temp2 * (sinepw - ayn - axn * esine * temp3)
        u = Atan2(sinu, cosu)
        sin2u = 2 * sinu * cosu
        cos2u = 2 * cosu * cosu - 1
        temp = 1 / pl
        temp1 = ck2 * temp
        temp2 = temp1 * temp
        ' Update for short periodics
        rk = r * (1 - 1.5 * temp2 * betal * x3thm1) + 0.5 * temp1 * x1mth2 * cos2u
        uk = u - 0.25 * temp2 * x7thm1 * sin2u : th = uk - arg_per 'Anomalie vraie
        'If th > 2 * pi Then th = th - 2 * pi
        'If th < 0 Then th = 2 * pi + th
        xnodek = xnode + 1.5 * temp2 * cosio * sin2u : asc_droite = xnodek 'Asc. droite
        If (asc_droite < 0) Then
            Do While (asc_droite < 0)
                asc_droite = asc_droite + (2 * pi)
            Loop
        End If
        asc_droite = asc_droite - Int(asc_droite / (2 * pi)) * (2 * pi) ' modulo 2*pi
        xinck = xinc + 1.5 * temp2 * cosio * sinio * cos2u : i_corrige = xinck 'Inclinaison

        cosean = (excent + Cos(th)) / (1 + (excent * Cos(th)))
        sinean = (Sin(th) * Sqrt(1 - excent * excent)) / (1 + excent * Cos(th))

        ean = Atan2(sinean, cosean)
        man = ean - excent * sinean

        If man > 2 * pi Then man = man - 2 * pi
        If man < 0 Then man = 2 * pi + man

        e_corrige = excent
        pom_corrige = arg_per
        gom_corrige = asc_droite
        am_corrige = man



        rdotk = rdot - xn * temp1 * x1mth2 * sin2u
        rfdotk = rfdot + xn * temp1 * (x1mth2 * cos2u + 1.5 * x3thm1)
        ' Orientation vectors
        sinuk = Sin(uk)
        cosuk = Cos(uk)
        sinik = Sin(xinck)
        cosik = Cos(xinck)
        sinnok = Sin(xnodek)
        cosnok = Cos(xnodek)
        xmx = -sinnok * cosik
        xmy = cosnok * cosik
        ux = xmx * sinuk + cosnok * cosuk
        uy = xmy * sinuk + sinnok * cosuk
        uz = sinik * sinuk
        vx = xmx * cosuk - cosnok * sinuk
        vy = xmy * cosuk - sinnok * sinuk
        vz = sinik * cosuk
        ' Position and velocity
        X = rk * ux : pos_vel.pos_x = X
        Y = rk * uy : pos_vel.pos_y = Y
        Z = rk * uz : pos_vel.pos_z = Z
        xdot = rdotk * ux + rfdotk * vx : pos_vel.vit_x = xdot
        ydot = rdotk * uy + rfdotk * vy : pos_vel.vit_y = ydot
        zdot = rdotk * uz + rfdotk * vz : pos_vel.vit_z = zdot
        ''
    End Sub
    '
    '* @brief converts greenwich rectangular coordinates to gamma 50 coordinates
    '*
    '* @author Colongo
    '* @date 2003-01-01
    '*
    '* Procedure to convert ECEF Greenwich coordinates in ECI gamm50 coordinates
    '* at the Julian Date in CNES Format
    '*
    '* @param[in] dblCNESJulianNow Julian date in CNES Format [days]
    '* @param[in] xgr, ygr, zgr greenwich coordinates [km]
    '* @param[out] x50, y50, z50 gamma 50 coordinates [km]
    '*

    Sub coord_greenwich_gamma(ByVal dblJulianNow As Double, _
                          ByVal xgr As Double, _
                          ByVal ygr As Double, _
                          ByVal zgr As Double, _
                          ByRef x50 As Double, _
                          ByRef y50 As Double, _
                          ByRef z50 As Double)
        '
        Dim X As Double
        Dim Y As Double
        Dim Z As Double
        '
        ' Calculate coordinates in gamma
        X = System.Math.Cos(temps_sideral(dblJulianNow + 2433282.5)) * xgr - System.Math.Sin(temps_sideral(dblJulianNow + 2433282.5)) * ygr
        Y = System.Math.Sin(temps_sideral(dblJulianNow + 2433282.5)) * xgr + System.Math.Cos(temps_sideral(dblJulianNow + 2433282.5)) * ygr
        Z = zgr
        ''
    End Sub
End Module