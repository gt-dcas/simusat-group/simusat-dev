﻿
Imports Tao.OpenGl
Imports Tao.Platform.Windows
Imports System.Math
Imports Microsoft.VisualBasic


Public Class frmAttConf

    Inherits System.Windows.Forms.Form
    '
    Private Sub cmd_quit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmd_quit.Click
        '
        Me.Hide()
        ''
    End Sub

    Private Sub frmpowerconfclosed_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '
        e.Cancel = True
        Me.Hide()
        ''
    End Sub

    Public Sub power_conf_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        '
        ' Fill combo gradient gravity boom
        With mass_cmbBoomFace
            .Enabled = True
            .Items.Clear()
            .Items.Add("+ X")
            .Items.Add("- X")
            .Items.Add("+ Y")
            .Items.Add("- Y")
            .Items.Add("+ Z")
            .Items.Add("- Z")
            .Text = "+ X"
        End With
        update_attitude_conf()
        update_attitude_data()
        ''
    End Sub


    Public Sub update_attitude_conf()
        '
        Dim i As Short
        Dim X As String
        On Error GoTo err_2
        '
        ' ----------------
        ' Onglets panneaux
        ' ----------------
        ' Central body
        mass_txtBodyHeigth.Text = Satellite.satBodyHeight
        mass_txtBodyWidth.Text = Satellite.satBodyWidth
        mass_txtBodyMass.Text = BodyMass
        mass_txtGx.Text = Format(BodyCG.X, "#0.000")
        mass_txtGy.Text = Format(BodyCG.Y, "#0.000")
        mass_txtGz.Text = Format(BodyCG.Z, "#0.000")
        mass_txtIxx.Text = Format(BodyInertia(0, 0), "#0.000")
        mass_txtIyy.Text = Format(BodyInertia(1, 1), "#0.000")
        mass_txtIzz.Text = Format(BodyInertia(2, 2), "#0.000")
        mass_txtIxy.Text = Format(BodyInertia(0, 1), "#0.000")
        mass_txtIxz.Text = Format(BodyInertia(0, 2), "#0.000")
        mass_txtIyz.Text = Format(BodyInertia(1, 2), "#0.000")
        mass_txtBoomMass.Text = Format(GravityMass, "#0.000")
        mass_txtBoomDist.Text = Format(GravityDist, "#0.000")
        '
        ' Panels data 
        mass_lstPanels.Items.Clear()
        PanelsNumber = SolarPanels.Count() ' Update PanelsNumber
        For i = 1 To PanelsNumber
            With SolarPanels.Item(i)
                mass_lstPanels.Items.Add(.PanelName)
            End With
        Next
        '
        ' Select the last Panel in the ListBox
        mass_lstPanels.SelectedIndex = mass_lstPanels.Items.Count - 1
        '
        ' Redraw the satellite
        If mass_lstPanels.Items.Count <> 0 Then
            CameraTransition(SolarPanels.Item(mass_lstPanels.Items.Count).PanelFace)
        End If
        '

        '
        Exit Sub
        '
err_2:
        ''
    End Sub



    '
    Dim eyePos As vector ' Eye position
    Dim eyeUp As vector ' Eye up vector
    Dim eye_distance As Double ' Distance from (0,0,0) to the eye
    Dim eyeVertAngle As Double ' Angles that correspond to the eye's position
    Dim eyeHorAngle As Double
    Dim old_x As Double ' Old mouse coordinates for calculating relative changes
    Dim old_y As Double
    '
    '
    Dim transitionHorAngle As Double ' Variables used for camera transitions
    Dim transitionVertAngle As Double
    Dim transitionHorStart As Double
    Dim transitionVertStart As Double
    Dim transitionStartTime As Integer
    Dim transitionDuration As Integer
    '
    ' Font for axis labels
    Dim font1 As Short
    Dim font2 As Short

    '
    ' Id textures
    '
    Dim tx_panel As Integer ' panneau solaire
    Dim tx_sat As Integer ' satellite
    '
    Dim panel1 As Short
    Dim panel2 As Short
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Pos"></param>
    ''' <param name="horAngle"></param>
    ''' <param name="vertAngle"></param>
    ''' <remarks></remarks>
    Private Sub calcAnglesFromPosition(ByRef Pos As vector, ByRef horAngle As Double, ByRef vertAngle As Double)
        '
        Dim xz, xzOriginal As vector
        '
        vertAngle = Math.Asin(Pos.Y / vctNorme(Pos)) ' calculate the elevation angle (simple trigonometry)
        xz.X = Pos.X : xz.Y = 0 : xz.Z = Pos.Z ' project the vector into the xz plane
        If xz.X = 0 And xz.Y = 0 And xz.Z = 0 Then
            horAngle = 0 ' the horizontal angle is arbitrary for a 90° vertical rotation
        Else
            xzOriginal.X = 0 : xzOriginal.Y = 0 : xzOriginal.Z = 1 ' original position (horAngle and vertAngle = 0) is on the positive z-axis
            ' calculate the rotation angle in the xz plane relative to the z-axis
            horAngle = Math.Acos(vctDot(xzOriginal, xz) / vctNorme(xzOriginal) / vctNorme(xz))
            If Pos.X < 0 Then horAngle = -horAngle ' ArcCos returns an angle between 0 and pi; for angles greater than pi, the x coordinate is negative and we need to use the negative angle
        End If
        ''
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="PanelFace"></param>
    ''' <remarks></remarks>
    Private Sub CameraTransition(ByRef PanelFace As String)
        '
        ' Sets all variables for a camera transition from the current eye position to
        ' a position facing the solar panel
        '
        Dim targetPos As vector
        Dim horEnd, vertEnd As Double
        Dim temp As Double
        '
        ' Calculate the angles that correspond to the current eye position
        Call calcAnglesFromPosition(eyePos, transitionHorStart, transitionVertStart)
        '
        ' Get the target position in the OpenGL coordinate system
        With targetPos
            Select Case PanelFace
                Case "+ X" : .X = 1 : .Y = 0.4 : .Z = 0.4
                Case "+ Y" : .X = -0.4 : .Y = 1 : .Z = 0.4
                Case "- X" : .X = -1 : .Y = -0.4 : .Z = 0.4
                Case "- Y" : .X = 0.4 : .Y = -1 : .Z = 0.4
                Case "+ Z" : .X = 0.4 : .Y = 0.4 : .Z = 1
                Case "- Z" : .X = 0.4 : .Y = 0.4 : .Z = -1
            End Select
            ' This just "rotates" the values of the vector
            temp = .X
            .X = .Y
            .Y = .Z
            .Z = temp

            '.ConvertFromEngineeringCOS
        End With
        '
        ' Calculate the angles that correspond to the target position
        Call calcAnglesFromPosition(targetPos, horEnd, vertEnd)
        '
        ' Calculate the transition angles
        transitionHorAngle = horEnd - transitionHorStart
        If transitionHorAngle > pi Then transitionHorAngle = transitionHorAngle - 2 * pi ' -pi < angle < pi
        transitionVertAngle = vertEnd - transitionVertStart
        If transitionVertAngle > pi Then transitionVertAngle = transitionVertAngle - 2 * pi ' -pi < angle < pi
        '
        ' If there is a rotation to be performed, set the time variables
        If transitionHorAngle <> 0 Or transitionVertAngle <> 0 Then
            transitionDuration = 1500
            transitionStartTime = GetTickCount
            ' Start the transition: enable the timer, which will envoke tmrTransition_Timer()
            tmrTransition.Enabled = True
        End If
        ''
    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_cmbFace_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_cmbFace.SelectedIndexChanged
        '
        '   Change orientations in the orientation combo according to the face on which the panel is mounted
        '   (Only for the orientable panels)
        '

        If mass_optPanelType_0.Checked Then
            Select Case mass_cmbFace.Text
                '
                Case "+ X", "- X"
                    With mass_cmbDirection
                        .Enabled = True
                        .Items.Clear()
                        .Items.Add("+ Y")
                        .Items.Add("+ Z")
                        .Items.Add("- Y")
                        .Items.Add("- Z")
                        .SelectedIndex = 0
                    End With
                    '
                Case "+ Y", "- Y"
                    With mass_cmbDirection
                        .Enabled = True
                        .Items.Clear()
                        .Items.Add("+ X")
                        .Items.Add("+ Z")
                        .Items.Add("- X")
                        .Items.Add("- Z")
                        .SelectedIndex = 0
                    End With
                    '
            End Select
            '
        End If
        ''
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_cmdAddPanel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_cmdAddPanel.Click
        '
        ' Create the new panel and set its characteristics
        '
        Dim TempPanel As New CSolarPanel
        '
        If mass_lstPanels.Items.Count = 8 Then
            MsgBox("You cannot mount more than 8 panels") : Exit Sub
        End If
        '
        With TempPanel
            If mass_optPanelType_0.Checked = True Then .PanelType = "Type1"
            If mass_optPanelType_1.Checked = True Then .PanelType = "Type2"
            If mass_optPanelType_2.Checked = True Then .PanelType = "Type3"
            .PanelLength = CSng(mass_txtPanelLength.Text)
            .PanelWidth = CSng(mass_txtPanelWidth.Text)
            .PanelMass = CSng(mass_txtPanelMass.Text)
            .PanelFace = mass_cmbFace.Text
            .PanelDir = mass_cmbDirection.Text
            .PanelHpos = mass_cmbPosition.Text
            If .PanelType = "Type3" Then
                .PanelName = mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & " : " & .PanelDir & "]"
            Else
                .PanelName = mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
            End If
            ' Add it to the ListBox
            mass_lstPanels.Items.Add((.PanelName))
        End With

        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count() ' Update PanelsNumber
        '
        '
        ' Select the added Panel in the ListBox
        mass_lstPanels.SelectedIndex = mass_lstPanels.Items.Count - 1
        '
        update_attitude_data()
        '
        ' Redraw the satellite
        CameraTransition(SolarPanels.Item(mass_lstPanels.Items.Count).PanelFace)
        ''
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_cmdModify_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_cmdModify.Click
        '
        ' Modify the selected panel in listpanels
        '
        If SolarPanels.Count() = 0 Then Exit Sub
        With SolarPanels.Item(mass_lstPanels.SelectedIndex + 1)
            If mass_optPanelType_0.Checked = True Then .PanelType = "Type1"
            If mass_optPanelType_1.Checked = True Then .PanelType = "Type2"
            If mass_optPanelType_2.Checked = True Then .PanelType = "Type3"
            .PanelLength = mass_txtPanelLength.Text
            .PanelWidth = mass_txtPanelWidth.Text
            .panelMass = CInt(mass_txtPanelMass.Text)
            .PanelFace = mass_cmbFace.Text
            .PanelDir = mass_cmbDirection.Text
            .PanelHpos = mass_cmbPosition.Text
            If .PanelType = "Type3" Then
                .PanelName = mass_lstPanels.SelectedIndex + 1 & "_" & .PanelType & " [" & .PanelFace & " : " & .PanelDir & "]"
                mass_lstPanels.Items(mass_lstPanels.SelectedIndex) = .PanelName
            Else
                .PanelName = mass_lstPanels.SelectedIndex + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
                mass_lstPanels.Items(mass_lstPanels.SelectedIndex) = .PanelName
            End If
        End With
        '
        update_attitude_data()
        '
        ' Redraw the satellite
        CameraTransition(SolarPanels.Item(mass_lstPanels.SelectedIndex + 1).PanelFace)
        ''
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_cmdRemovePanel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_cmdRemovePanel.Click
        '
        Dim Index, i As Short
        '
        If (mass_lstPanels.SelectedIndex <> -1) Then
            '
            Index = mass_lstPanels.SelectedIndex + 1
            SolarPanels.Remove((Index))
            '
            ' Change the name of panels (to have numbers from 1 to panelsnumber)
            mass_lstPanels.Items.Clear()
            PanelsNumber = SolarPanels.Count() ' Update PanelsNumber
            For i = 1 To PanelsNumber
                With SolarPanels.Item(i)
                    If .PanelType = "Type3" Then
                        .PanelName = i & "_" & .PanelType & " [" & .PanelFace & " : " & .paneldir & "]"
                    Else
                        .PanelName = i & "_" & .PanelType & " [" & .PanelFace & "]"
                    End If
                    mass_lstPanels.Items.Add(.PanelName)
                End With
            Next
            '
        End If
        '
        ' Select the last Panel in the ListBox
        mass_lstPanels.SelectedIndex = mass_lstPanels.Items.Count - 1
        '
        update_attitude_data()
        '
        ' Redraw the satellite
        If mass_lstPanels.Items.Count <> 0 Then
            CameraTransition(SolarPanels.Item(mass_lstPanels.Items.Count).PanelFace)
        End If
        ''
    End Sub



    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_lstPanels_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_lstPanels.SelectedIndexChanged
        '
        Dim TempPanel As CSolarPanel
        '
        If (mass_lstPanels.SelectedIndex >= 0) Then
            TempPanel = SolarPanels.Item(mass_lstPanels.SelectedIndex + 1)
        Else
            Exit Sub
        End If
        '
        mass_optPanelType_0.Checked = True
        ' Update and fill combo boxes and data with TempPanel characteristics
        '
        Select Case TempPanel.PanelType
            '
            Case "Type1" ' Orientable panel
                mass_optPanelType_0.Checked = True
                mass_optPanelType_1.Checked = False
                mass_optPanelType_2.Checked = False
                With mass_cmbFace
                    .Items.Clear()
                    .Items.Add("+ X")
                    .Items.Add("+ Y")
                    .Items.Add("- X")
                    .Items.Add("- Y")
                    .Text = TempPanel.PanelFace
                End With
                '
                With mass_cmbDirection
                    .Items.Clear()
                    .Enabled = False
                End With
                '
                With mass_cmbPosition
                    .Items.Clear()
                    .Enabled = False
                End With
                '
            Case "Type2" ' Panel on a face
                mass_optPanelType_0.Checked = False
                mass_optPanelType_1.Checked = True
                mass_optPanelType_2.Checked = False
                With mass_cmbFace
                    .Enabled = True
                    .Items.Clear()
                    .Items.Add("+ X")
                    .Items.Add("- X")
                    .Items.Add("+ Y")
                    .Items.Add("- Y")
                    .Items.Add("+ Z")
                    .Items.Add("- Z")
                    .Text = TempPanel.PanelFace
                End With
                '
                With mass_cmbDirection
                    .Items.Clear()
                    .Enabled = False
                End With
                '
                With mass_cmbPosition
                    .Items.Clear()
                    .Enabled = False
                End With
                '
            Case "Type3" ' Fixed perpendicular to a face
                mass_optPanelType_0.Checked = False
                mass_optPanelType_1.Checked = False
                mass_optPanelType_2.Checked = True
                With mass_cmbFace
                    .Enabled = True
                    .Items.Clear()
                    .Items.Add("+ X")
                    .Items.Add("- X")
                    .Items.Add("+ Y")
                    .Items.Add("- Y")
                    .Text = TempPanel.PanelFace
                End With
                '
                With mass_cmbDirection
                    .Items.Clear()
                    .Enabled = True
                    .Items.Add("+ Z")
                    .Items.Add("- Z")
                    .Text = TempPanel.PanelDir
                End With
                '
                With mass_cmbPosition
                    .Items.Clear()
                    .Enabled = True
                    .Items.Add("Upper")
                    .Items.Add("Middle")
                    .Items.Add("Lower")
                    .Text = TempPanel.PanelHpos
                End With
                '
                '
        End Select
        mass_txtPanelWidth.Text = CStr(TempPanel.PanelWidth)
        mass_txtPanelLength.Text = CStr(TempPanel.PanelLength)
        mass_txtPanelMass.Text = CStr(TempPanel.PanelMass)
        '
        ' Redraw the satellite
        CameraTransition((TempPanel.PanelFace))
        ''
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>

    Private Sub tmrAnimate_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tmrAnimate.Tick
        '
        gl_DrawSatellite()
        ''
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub tmrTransition_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tmrTransition.Tick
        '
        Dim progress, progressF As Double
        '
        ' Calculate the current progress
        progress = (GetTickCount - transitionStartTime) / transitionDuration
        If progress > 1.0# Then progress = 1.0#

        ' We don't want a linear progression, so apply a function to the value
        ' (start fast and slow down gradually)
        progressF = ((progress - 1) ^ 3 + 1)
        '
        ' Calculate the eye position for this step of the transition
        eyeHorAngle = transitionHorStart + transitionHorAngle * progressF
        eyeVertAngle = transitionVertStart + transitionVertAngle * progressF
        eyePos.X = eye_distance * Cos(eyeVertAngle) * Sin(eyeHorAngle)
        eyePos.Y = eye_distance * Sin(eyeVertAngle)
        eyePos.Z = eye_distance * Cos(eyeVertAngle) * Cos(eyeHorAngle)

        ' If we're done, disable the timer
        If progress = 1.0# Then
            tmrTransition.Enabled = False
        End If
        '
        ' Redraw the satellite
        gl_DrawSatellite()
        ''
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub frmMountPanels_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        '
        ' Initializations
        '
        Dim i As Short
        Dim bmp As Bitmap
        '
        ' Initialize image
        bmp = New Bitmap(My.Application.Info.DirectoryPath & "\Data\Textures\Pan1.bmp")
        bmp.MakeTransparent(Color.Red)
        zpict_5.Image = bmp

        ' Fill body geometry
        mass_txtBodyHeigth.Text = Format(Satellite.satBodyHeight, "0.0#")
        mass_txtBodyWidth.Text = Format(Satellite.satBodyWidth, "0.0#")
        '
        ' Set default for panel type
        mass_optPanelType_0.Checked = True
        With mass_cmbFace
            .Enabled = True
            .Items.Clear()
            .Items.Add("+ X")
            .Items.Add("+ Y")
            .Items.Add("- X")
            .Items.Add("- Y")
            .SelectedIndex = 0
        End With
        '
        With mass_cmbDirection ' Initially looking +Z
            .Items.Clear()
            .Enabled = False
        End With
        mass_lblPanelOrient.Enabled = False
        '
        With mass_cmbPosition
            .Items.Clear()
            .Enabled = False
        End With
        mass_lblPanelPos.Enabled = False
        ' optPanelType_CheckedChanged(optPanelType.Item(0), New System.EventArgs())
        '
        ' Fill List with the satellite panels
        mass_lstPanels.Items.Clear()
        Dim panelTmp As CSolarPanel
        If SolarPanels.Count() > 0 Then
            '
            For i = 1 To SolarPanels.Count()
                ' Add the panel to the array and the ListBox
                panelTmp = SolarPanels.Item(i)
                mass_lstPanels.Items.Add(panelTmp.PanelName)
            Next
            '
            mass_lstPanels.SelectedIndex = 0
            '
        End If
        '
        ' Initialize the OpenGL window
        glInitialize()
        '
        ' Move the camera to face the panel
        CameraTransition(("+ X"))
        gl_DrawSatellite()
        ''
    End Sub

   
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_pictSatellite_MouseDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles mass_pictSatellite.MouseDown
        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim X As Single = eventArgs.X
        Dim Y As Single = eventArgs.Y
        old_x = X
        old_y = Y
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub pictSatellite_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles mass_pictSatellite.MouseMove
        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim X As Single = eventArgs.X
        Dim Y As Single = eventArgs.Y
        If Button = 1 Or Button = 2 Then
            ' Disable the timer in case the user is attempting to control the view while
            ' a transition is in progress
            Me.tmrTransition.Enabled = False
        End If

        If Button = 1 Then
            ' rotation
            eyeVertAngle = eyeVertAngle - (old_y - Y) / 200
            eyeHorAngle = eyeHorAngle - (X - old_x) / 200
            If eyeVertAngle > pi / 2 Then eyeVertAngle = pi / 2
            If eyeVertAngle < -pi / 2 Then eyeVertAngle = -pi / 2
            old_x = X
            old_y = Y
        ElseIf Button = 2 Then
            ' Zoom
            eye_distance = eye_distance + (Y - old_y) / 4
            If eye_distance < 3 Then eye_distance = 3
            old_y = Y
        Else
            Exit Sub
        End If
        '
        ' Calculate new eye position
        eyePos.X = eye_distance * Cos(eyeVertAngle) * Sin(eyeHorAngle)
        eyePos.Y = eye_distance * Sin(eyeVertAngle)
        eyePos.Z = eye_distance * Cos(eyeVertAngle) * Cos(eyeHorAngle)
        '
        ' Redraw
        gl_DrawSatellite()
        ''
    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub glInitialize()


        m_intptrHdc_sat = User.GetDC(mass_pictSatellite.Handle)

        setupPixelFormat(mass_pictSatellite.CreateGraphics.GetHdc)
        hGLRC_sat = Wgl.wglCreateContext(m_intptrHdc_sat)
        Wgl.wglMakeCurrent(m_intptrHdc_sat, hGLRC_sat)



        ' Set up openGL states

        Gl.glEnable(Gl.GL_DEPTH_TEST)

        Gl.glMatrixMode(Gl.GL_MODELVIEW)

        Gl.glLoadIdentity()

        Gl.glEnable(Gl.GL_CULL_FACE)

        Gl.glEnable(Gl.GL_LINE_SMOOTH)

        Gl.glEnable(Gl.GL_POLYGON_SMOOTH)

        Gl.glShadeModel(Gl.GL_SMOOTH)

        Gl.glHint(Gl.GL_PERSPECTIVE_CORRECTION_HINT, Gl.GL_NICEST)

        Gl.glEnable(Gl.GL_TEXTURE_2D)

        ' Lighting

        Gl.glEnable(Gl.GL_LIGHTING)
        Dim Pos(3) As Single

        Gl.glEnable(Gl.GL_LIGHT0)
        Pos(0) = 1
        Pos(1) = 1
        Pos(2) = 1
        Pos(3) = 1

        Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_AMBIENT, Pos(0))
        Pos(0) = 1
        Pos(1) = 1
        Pos(2) = 1
        Pos(3) = 1

        Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_DIFFUSE, Pos(0))
        '
        ' Build fonts
        gl_BuildFont2D(mass_pictSatellite, font1, font2)
        '
        ' Build textures

        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\" & "Data\Textures\goldfoil.jpg", tx_sat)
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\" & "Data\Textures\SolarPanel.jpg", tx_panel)
        '
        ' Setup Display Lists
        GLPanelList()
        '
        ' Set background color
        Gl.glClearColor(0, 0, 0, 0)
        '
        Gl.glPolygonMode(Gl.GL_FRONT_AND_BACK, Gl.GL_FILL)
        Gl.glMatrixMode((Gl.GL_PROJECTION))
        Gl.glLoadIdentity()

        'ScaleWidth et ScaleHeight remplacés pas Width et Height
        Gl.glViewport(0, 0, Me.mass_pictSatellite.Width, Me.mass_pictSatellite.Height)
        Glu.gluPerspective(45, Me.mass_pictSatellite.Width / Me.mass_pictSatellite.Height, 1, 1000)


        Gl.glClear(Gl.GL_COLOR_BUFFER_BIT Or Gl.GL_DEPTH_BUFFER_BIT)
        '
        ' Initialize the eye position and angles
        eyeVertAngle = pi / 4
        eyeHorAngle = pi / 4
        eye_distance = 5
        eyePos.X = eye_distance * Cos(eyeVertAngle) * Sin(eyeHorAngle)
        eyePos.Y = eye_distance * Sin(eyeVertAngle)
        eyePos.Z = eye_distance * Cos(eyeVertAngle) * Cos(eyeHorAngle)
        eyeUp.X = 0 : eyeUp.Y = 1 : eyeUp.Z = 0

    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub gl_DrawSatellite()
        '
        Dim rotMatrixSat(16) As Double
        Dim w, h, rmax As Single ' Width, height of central body and radius of bounding sphere
        Dim lp, wp As Double ' Length, width of solar panel
        Dim alf As Double ' Angle to rotate solar panel on the right face
        Static teta As Object ' Rotation angle of type 1 solar panel to animate 3D display
        Dim satscale As Double ' Global scal of satellite (to fill well the sattitude sphere
        Dim m$


        ' ScaleWidth et ScaleHeight remplacés pas Width et Height
        ' Il faut recalculer le point de vue avec viewport et perpesctive a chaque itération
        Gl.glPushMatrix()
        Gl.glMatrixMode(Gl.GL_PROJECTION)
        Gl.glLoadIdentity()
        Gl.glViewport(0, 0, Me.mass_pictSatellite.Width, Me.mass_pictSatellite.Height)
        Glu.gluPerspective(45, Me.mass_pictSatellite.Width / Me.mass_pictSatellite.Height, 0.5, 100)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glPopMatrix()

        Gl.glClear(Gl.GL_COLOR_BUFFER_BIT Or Gl.GL_DEPTH_BUFFER_BIT)

        Gl.glDisable(Gl.GL_CULL_FACE)
        '
        Wgl.wglMakeCurrent(m_intptrHdc_sat, hGLRC_sat)

        Gl.glClear(Gl.GL_COLOR_BUFFER_BIT Or Gl.GL_DEPTH_BUFFER_BIT)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glLoadIdentity()
        '
        ' Set the camera parameters & render the scene
        Glu.gluLookAt(eyePos.X, eyePos.Y, eyePos.Z, 0.0#, 0.0#, 0.0#, eyeUp.X, eyeUp.Y, eyeUp.Z)
        '
        Gl.glLineWidth(1)
        Gl.glEnable(Gl.GL_LIGHTING)
        Gl.glColor3f(1, 1, 1)
        Gl.glEnable(Gl.GL_LIGHTING)
        Gl.glEnable(Gl.GL_TEXTURE_2D)
        Gl.glEnable(Gl.GL_COLOR_MATERIAL)
        Gl.glEnable(Gl.GL_POLYGON_SMOOTH)
        Gl.glEnable(Gl.GL_NORMALIZE)
        '
        Dim i As Short
        '
        Gl.glDisable(Gl.GL_COLOR_MATERIAL)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glPushMatrix()
        Gl.glEnable((Gl.GL_TEXTURE_2D))
        '
        Gl.glRotated(-90, 1, 0, 0)
        Gl.glRotated(-90, 0, 0, 1)
        '
        ' Draw satellite body
        ' -------------------
        ' Preliminary calculation for scaling
        h = Satellite.satBodyHeight
        w = Satellite.satBodyWidth
        rmax = Sqrt(2 * w * w / 4 + h * h / 4) ' Bounding sphere radius
        satscale = 1 '0.5 * 4 / rmax       ' So that rmax corresponds to 50% of the sphere radius
        '
        Gl.glEnable(Gl.GL_LIGHT1)
        Gl.glPushMatrix()
        Gl.glScalef(w * satscale, w * satscale, h * satscale) ' Fill at 50% of the attitude sphere, the radius of which is 4
        '
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, tx_sat) ' Goldfoil cover
        Gl.glColor4f(1, 1, 1, 1)
        For i = 0 To 3 ' Side faces
            Gl.glRotatef(90, 0, 0, 1)
            Gl.glBegin((Gl.GL_QUADS))
            Gl.glTexCoord2f(0, 0)
            Gl.glVertex3f(0.5, 0.5, -0.5)
            Gl.glTexCoord2f(0, 1)
            Gl.glVertex3f(0.5, 0.5, 0.5)
            Gl.glTexCoord2f(1, 1)
            Gl.glVertex3f(0.5, -0.5, 0.5)
            Gl.glTexCoord2f(1, 0)
            Gl.glVertex3f(0.5, -0.5, -0.5)
            Gl.glEnd()
        Next
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Upper face
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(0.5, 0.5, 0.5)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(-0.5, 0.5, 0.5)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(-0.5, -0.5, 0.5)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(0.5, -0.5, 0.5)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Lower face
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(0.5, 0.5, -0.5)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(-0.5, 0.5, -0.5)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(-0.5, -0.5, -0.5)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(0.5, -0.5, -0.5)
        Gl.glEnd()
        Gl.glPopMatrix()
        '
        ' Draw panels
        ' -----------
        For i = 1 To SolarPanels.Count()
            ' DEBUT DU TRACE PANNEAU
            lp = SolarPanels.Item(i).PanelLength
            wp = SolarPanels.Item(i).PanelWidth
            '
            Gl.glPushMatrix()
            '
            If SolarPanels.Item(i).paneltype = "Type1" Then
                '
                teta = teta + 0.25
                If teta >= 180 Then teta = teta - 360
                m$ = SolarPanels.Item(i).PanelFace
                Select Case m$
                    Case "+ X" : alf = 0
                    Case "+ Y" : alf = 90
                    Case "- X" : alf = 180
                    Case "- Y" : alf = 270
                End Select
                '
                Gl.glRotatef(alf, 0, 0, 1)
                Gl.glPushMatrix()
                Gl.glTranslatef(w / 2 * satscale, 0, -0.01) ' -0.01 to keep the axis visible (out of the panel)
                Gl.glScalef(wp / 2 * satscale, wp / 2 * satscale, wp / 2 * satscale)
                Gl.glRotatef(teta, 1, 0, 0)
                Gl.glCallList(panel2)
                Gl.glPopMatrix()
                Gl.glTranslatef((w / 2 + lp / 2 + wp / 2) * satscale, 0, -0.01) ' -0.01 to keep the axis visible (out of the panel)
                Gl.glScalef(lp / 2 * satscale, wp / 2 * satscale, wp / 2 * satscale)
                Gl.glRotatef(teta, 1, 0, 0)
                Gl.glCallList(panel1)
                '
            ElseIf SolarPanels.Item(i).paneltype = "Type2" Then
                m$ = SolarPanels.Item(i).PanelFace
                Select Case m$
                    Case "+ X"
                        Gl.glRotatef(90, 0, 1, 0)       ' This rotation to have the length of the panel along z axis
                        Gl.glTranslatef(0, 0, (1.025 * w / 2 + 0.025 * w) * satscale)
                    Case "+ Y"
                        Gl.glRotatef(90, 0, 0, 1)       ' This rotation to have the length of the panel along z axis
                        Gl.glRotatef(90, 0, 1, 0)
                        Gl.glTranslatef(0, 0, (w / 2 + 0.025 * w) * satscale)
                    Case "- X"
                        Gl.glRotatef(-90, 0, 1, 0)
                        Gl.glTranslatef(0, 0, (w / 2 + 0.025 * w) * satscale)
                    Case "- Y"
                        Gl.glRotatef(90, 0, 0, 1)       ' This rotation to have the length of the panel along z axis
                        Gl.glRotatef(-90, 0, 1, 0)
                        Gl.glTranslatef(0, 0, (w / 2 + 0.025 * w) * satscale)
                    Case "+ Z"
                        Gl.glTranslatef(0, 0, (h / 2 + 0.025 * w) * satscale)
                    Case "- Z"
                        Gl.glRotatef(180, 1, 0, 0)
                        Gl.glTranslatef(0, 0, (h / 2 + 0.025 * w) * satscale)
                End Select
                '
                Gl.glScalef(lp / 2 * satscale, wp / 2 * satscale, wp / 2 * satscale)
                Gl.glCallList(panel1)
                '
            Else
                m$ = SolarPanels.Item(i).PanelFace
                Select Case m$
                    Case "+ X" : alf = 0
                    Case "+ Y" : alf = 90
                    Case "- X" : alf = 180
                    Case "- Y" : alf = 270
                End Select
                '
                Gl.glRotatef(alf, 0, 0, 1)
                Gl.glTranslatef((w / 2 + lp / 2) * satscale, 0, 0)
                '
                m$ = SolarPanels.Item(i).PanelHpos
                Select Case m$
                    Case "Upper"
                        Gl.glTranslatef(0, 0, (h / 2 - 0.025 * h) * satscale)
                    Case "Lower"
                        Gl.glTranslatef(0, 0, -(h / 2 - 0.025 * h) * satscale)
                    Case "Middle"
                        Gl.glTranslatef(0, 0, -0.01 * satscale) ' to keep the axis visible (out of the panel)
                End Select
                '
                Gl.glScalef(lp / 2 * satscale, wp / 2 * satscale, wp / 2 * satscale)
                If SolarPanels.Item(i).PanelDir = "- Z" Then
                    Gl.glRotatef(180, 1, 0, 0)
                End If
                '
                Gl.glCallList(panel1)
                '
            End If
            '
            Gl.glPopMatrix()
            ' FIN TRACE PANNEAU
        Next
        '
        ' Draw x,y,z axis
        ' ---------------
        '
        Gl.glDisable(Gl.GL_LIGHTING) ' Axis aren't textured nor lighted
        Gl.glDisable(Gl.GL_TEXTURE_2D)
        Gl.glLineWidth(2)
        '
        Gl.glBegin(Gl.GL_LINES)
        Gl.glColor3f(1, 0, 0) 'x-axis in red
        Gl.glVertex3f(0, 0, 0)
        Gl.glVertex3f(1.5 * Max(w, h), 0, 0)
        Gl.glColor3f(0, 1, 0) 'y-axis in green
        Gl.glVertex3f(0, 0, 0)
        Gl.glVertex3f(0, 1.5 * Max(w, h), 0)
        Gl.glColor3f(0, 1, 1) 'z-axis in cyan
        Gl.glVertex3f(0, 0, 0)
        Gl.glVertex3f(0, 0, 1.5 * Max(w, h))
        Gl.glEnd()
        '
        ' Draw labels (same colors as the axis)
        gl_DrawTextColored("X", 1.7 * Max(w, h), 0, 0, &HFF, font2)
        gl_DrawTextColored("Y", 0, 1.7 * Max(w, h), 0, &HFF00, font2)
        gl_DrawTextColored("Z", 0, 0, 1.7 * Max(w, h), &HFFFF00, font2)
        '
        ' Reactive lighting and texturing
        Gl.glLineWidth(1)
        Gl.glEnable(Gl.GL_LIGHTING)
        Gl.glEnable((Gl.GL_TEXTURE_2D))
        '
        ' Restore OpenGl Modelview Matrix
        Gl.glPopMatrix()
        '
        Gl.glDisable(Gl.GL_LIGHTING)
        Gl.glDisable(Gl.GL_TEXTURE_2D)
        Gl.glDisable(Gl.GL_NORMALIZE)
        Gl.glDisable(Gl.GL_POLYGON_SMOOTH)
        Gl.glDisable(Gl.GL_COLOR_MATERIAL)
        Gl.glDisable(Gl.GL_TEXTURE_2D)
        Gl.glDisable(Gl.GL_LIGHTING)
        '
        Gl.glFlush()
        Gdi.SwapBuffers(m_intptrHdc_sat)
        'Gdi.SwapBuffers(User.GetDC(pan_pictSatellite.Handle))
        GC.Collect()
        ''
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Sub GLPanelList()
        '
        Dim i As Short
        '
        ' Solar panel
        ' -----------
        panel1 = Gl.glGenLists(1)
        Gl.glNewList(panel1, Gl.GL_COMPILE)
        '
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, tx_panel) ' Solarpanel
        Gl.glColor4f(1, 1, 1, 1)
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Upper face
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(1, 1, 0.03)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(-1, 1, 0.03)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(-1, -1, 0.03)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(1, -1, 0.03)
        Gl.glEnd()
        '
        Gl.glDisable(Gl.GL_LIGHTING)
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, 0) ' Solarpanel
        Gl.glColor4f(0.2, 0.2, 0.2, 1)
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Lower face
        Gl.glVertex3f(1, 1, -0.03)
        Gl.glVertex3f(-1, 1, -0.03)
        Gl.glVertex3f(-1, -1, -0.03)
        Gl.glVertex3f(1, -1, -0.03)
        Gl.glEnd()
        '
        Gl.glColor4f(0.5, 0.5, 0.5, 1)
        For i = 0 To 3 ' Side faces
            Gl.glRotated(90 * i, 0, 0, 1)
            Gl.glBegin((Gl.GL_QUADS))
            Gl.glVertex3f(1, 1, -0.03)
            Gl.glVertex3f(1, 1, 0.03)
            Gl.glVertex3f(1, -1, 0.03)
            Gl.glVertex3f(1, -1, -0.03)
            Gl.glEnd()
        Next
        '
        Gl.glEnable(Gl.GL_LIGHTING)
        '
        Gl.glEndList()
        '
        ' Mechanism
        ' ---------
        panel2 = Gl.glGenLists(1)
        Gl.glNewList(panel2, Gl.GL_COMPILE)
        '
        Gl.glDisable(Gl.GL_LIGHTING)
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, 0)
        Gl.glColor4f(0.4, 0.4, 0.4, 1)
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Upper face
        Gl.glVertex3f(0, 0.1, 0.03)
        Gl.glVertex3f(0, -0.1, 0.03)
        Gl.glVertex3f(0.9, -0.9, 0.03)
        Gl.glVertex3f(0.9, -0.7, 0.03)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0, 0.1, 0.03)
        Gl.glVertex3f(0, -0.1, 0.03)
        Gl.glVertex3f(0.9, 0.7, 0.03)
        Gl.glVertex3f(0.9, 0.9, 0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, -0.9, 0.03)
        Gl.glVertex3f(0.99, -0.9, 0.03)
        Gl.glVertex3f(0.99, 0.9, 0.03)
        Gl.glVertex3f(0.9, 0.9, 0.03)
        Gl.glEnd()
        '
        Gl.glColor4f(0.4, 0.4, 0.4, 1)
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Upper face
        Gl.glVertex3f(0, 0.1, -0.03)
        Gl.glVertex3f(0, -0.1, -0.03)
        Gl.glVertex3f(0.9, -0.9, -0.03)
        Gl.glVertex3f(0.9, -0.7, -0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0, 0.1, -0.03)
        Gl.glVertex3f(0, -0.1, -0.03)
        Gl.glVertex3f(0.9, 0.7, -0.03)
        Gl.glVertex3f(0.9, 0.9, -0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, -0.9, -0.03)
        Gl.glVertex3f(0.99, -0.9, -0.03)
        Gl.glVertex3f(0.99, 0.9, -0.03)
        Gl.glVertex3f(0.9, 0.9, -0.03)
        Gl.glEnd()
        '
        Gl.glColor4f(0.3, 0.3, 0.3, 1)
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Sides
        Gl.glVertex3f(0.9, -0.9, -0.03)
        Gl.glVertex3f(0.9, -0.9, 0.03)
        Gl.glVertex3f(0, -0.1, 0.03)
        Gl.glVertex3f(0, -0.1, -0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, 0.9, -0.03)
        Gl.glVertex3f(0.9, 0.9, 0.03)
        Gl.glVertex3f(0, 0.1, 0.03)
        Gl.glVertex3f(0, 0.1, -0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, -0.7, -0.03)
        Gl.glVertex3f(0.9, -0.7, 0.03)
        Gl.glVertex3f(0, 0.1, 0.03)
        Gl.glVertex3f(0, 0.1, -0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, 0.7, -0.03)
        Gl.glVertex3f(0.9, 0.7, 0.03)
        Gl.glVertex3f(0, -0.1, 0.03)
        Gl.glVertex3f(0, -0.1, -0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, -0.9, -0.03)
        Gl.glVertex3f(0.9, -0.9, 0.03)
        Gl.glVertex3f(0.9, 0.9, 0.03)
        Gl.glVertex3f(0.9, 0.9, -0.03)
        Gl.glEnd()
        '
        Gl.glEnable(Gl.GL_LIGHTING)
        '
        Gl.glEndList()
        ''
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_txtBodyHeigth_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_txtBodyHeigth.Leave
        '
        Dim X As Double
        ' the entries must be a number
        If IsNumeric(mass_txtBodyHeigth.Text) Then
            X = CDbl(mass_txtBodyHeigth.Text)
            If WithinLimits(X, 0.4, 10.0#) = False Then
                mass_txtBodyHeigth.Focus()
                mass_txtBodyHeigth.Text = Format(Satellite.satBodyHeight, "#0.000")
                Exit Sub
            End If
            mass_txtBodyHeigth.Text = Format(X, "#0.000")
            Satellite.satBodyHeight = X
        Else
            MsgBox("Please enter a number !")
            mass_txtBodyHeigth.Text = Format(Satellite.satBodyHeight, "#0.000")
            mass_txtBodyHeigth.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_txtBodyWidth_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_txtBodyWidth.Leave
        '
        Dim X As Double
        ' the entries must be a number
        If IsNumeric(mass_txtBodyWidth.Text) Then
            X = CDbl(mass_txtBodyWidth.Text)
            If WithinLimits(X, 0.4, 10.0#) = False Then
                mass_txtBodyWidth.Focus()
                mass_txtBodyHeigth.Text = Format(Satellite.satBodyHeight, "#0.000")
                Exit Sub
            End If
            mass_txtBodyWidth.Text = Format(X, "#0.000")
            Satellite.satBodyWidth = X
        Else
            MsgBox("Please enter a number !")
            mass_txtBodyWidth.Text = Format(Satellite.satBodyWidth, "#0.000")
            mass_txtBodyWidth.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>    
    ''' 
    Private Sub mass_txtPanelLength_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_txtPanelLength.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtPanelLength.Text) Then
            X = CDbl(mass_txtPanelLength.Text)
            '
            If WithinLimits(X, 0.4, 20.0#) = False Then
                mass_txtPanelLength.Focus()
                Exit Sub
            End If
            mass_txtPanelLength.Text = Format(X, "#0.000")
        Else
            MsgBox("Please enter a number !")
            mass_txtPanelLength.Text = CStr(1)
            mass_txtPanelLength.Focus()
        End If
        ''
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_txtPanelWidth_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_txtPanelWidth.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtPanelWidth.Text) Then
            X = CDbl(mass_txtPanelWidth.Text)
            If WithinLimits(X, 0.1, 20.0#) = False Then
                mass_txtPanelWidth.Focus()
                Exit Sub
            End If
            mass_txtPanelWidth.Text = Format(X, "#0.000")
        Else
            MsgBox("Please enter a number !")
            mass_txtPanelWidth.Text = CStr(1)
            mass_txtPanelWidth.Focus()
        End If
        ''
    End Sub


    Private Sub mass_optPanelType_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles mass_optPanelType_0.CheckedChanged, mass_optPanelType_1.CheckedChanged, mass_optPanelType_2.CheckedChanged
        '
        ' Update and fill combo boxes and data with TempPanel characteristics
        '
        If mass_optPanelType_0.Checked = True Then ' Orientable panel
            mass_optPanelType_0.Checked = True
            mass_optPanelType_1.Checked = False
            mass_optPanelType_2.Checked = False
            With mass_cmbFace
                .Items.Clear()
                .Items.Add("+ X")
                .Items.Add("+ Y")
                .Items.Add("- X")
                .Items.Add("- Y")
                .Text = "+ X"
            End With
            '
            With mass_cmbDirection
                .Items.Clear()
                .Enabled = False
            End With
            '
            With mass_cmbPosition
                .Items.Clear()
                .Enabled = False
            End With
            mass_lblPanelOrient.Enabled = False
            mass_lblPanelPos.Enabled = False
            '
        ElseIf mass_optPanelType_1.Checked = True Then  ' Panel on a face
            mass_optPanelType_0.Checked = False
            mass_optPanelType_1.Checked = True
            mass_optPanelType_2.Checked = False
            With mass_cmbFace
                .Enabled = True
                .Items.Clear()
                .Items.Add("+ X")
                .Items.Add("- X")
                .Items.Add("+ Y")
                .Items.Add("- Y")
                .Items.Add("+ Z")
                .Items.Add("- Z")
                .Text = "+ X"
            End With
            '
            With mass_cmbDirection
                .Items.Clear()
                .Enabled = False
            End With
            '
            With mass_cmbPosition
                .Items.Clear()
                .Enabled = False
            End With
            mass_lblPanelOrient.Enabled = False
            mass_lblPanelPos.Enabled = False
            '
        Else ' Fixed perpendicular to a face
            mass_optPanelType_0.Checked = False
            mass_optPanelType_1.Checked = False
            mass_optPanelType_2.Checked = True
            With mass_cmbFace
                .Enabled = True
                .Items.Clear()
                .Items.Add("+ X")
                .Items.Add("- X")
                .Items.Add("+ Y")
                .Items.Add("- Y")
                .Text = "+ X"
            End With
            '
            With mass_cmbDirection
                .Items.Clear()
                .Enabled = True
                .Items.Add("+ Z")
                .Items.Add("- Z")
                .Text = "+ Z"
            End With
            mass_lblPanelOrient.Enabled = True
            mass_lblPanelPos.Enabled = True
            '
            With mass_cmbPosition
                .Items.Clear()
                .Enabled = True
                .Items.Add("Upper")
                .Items.Add("Middle")
                .Items.Add("Lower")
                .Text = "Upper"
            End With
            '
            '
        End If
        ''
    End Sub

    Private Sub mass_txtGx_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtGx.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtGx.Text) Then
            X = CDbl(mass_txtGx.Text)
            mass_txtGx.Text = Format(X, "#0.000")
            BodyCG.X = X
        Else
            MsgBox("Please enter a number !")
            mass_txtGx.Text = Format(BodyCG.X, "#0.000")
            mass_txtGx.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub mass_txtGy_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtGy.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtGy.Text) Then
            X = CDbl(mass_txtGy.Text)
            mass_txtGy.Text = Format(X, "#0.000")
            BodyCG.Y = X
        Else
            MsgBox("Please enter a number !")
            mass_txtGy.Text = Format(BodyCG.Y, "#0.000")
            mass_txtGy.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub mass_txtGz_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtGz.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtGz.Text) Then
            X = CDbl(mass_txtGz.Text)
            mass_txtGz.Text = Format(X, "#0.000")
            BodyCG.Z = X
        Else
            MsgBox("Please enter a number !")
            mass_txtGz.Text = Format(BodyCG.Z, "#0.000")
            mass_txtGz.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub mass_txtIxx_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtIxx.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtIxx.Text) Then
            X = CDbl(mass_txtIxx.Text)
            mass_txtIxx.Text = Format(X, "#0.000")
            BodyInertia(0, 0) = X
        Else
            MsgBox("Please enter a number !")
            mass_txtIxx.Text = Format(BodyInertia(0, 0), "#0.000")
            mass_txtIxx.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub mass_txtIxy_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtIxy.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtIxy.Text) Then
            X = CDbl(mass_txtIxy.Text)
            mass_txtIxy.Text = Format(X, "#0.000")
            BodyInertia(0, 1) = X
            BodyInertia(1, 0) = X
        Else
            MsgBox("Please enter a number !")
            mass_txtIxy.Text = Format(BodyInertia(0, 0), "#0.000")
            mass_txtIxy.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub mass_txtBodyMass_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtBodyMass.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtBodyMass.Text) Then
            X = CDbl(mass_txtBodyMass.Text)
            mass_txtBodyMass.Text = Format(X, "#0.000")
            BodyMass = X
        Else
            MsgBox("Please enter a number !")
            mass_txtBodyMass.Text = Format(BodyMass, "#0.000")
            mass_txtBodyMass.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub mass_txtIxz_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtIxz.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtIxz.Text) Then
            X = CDbl(mass_txtIxz.Text)
            mass_txtIxz.Text = Format(X, "#0.000")
            BodyInertia(0, 2) = X
            BodyInertia(2, 0) = X
        Else
            MsgBox("Please enter a number !")
            mass_txtIxz.Text = Format(BodyInertia(0, 2), "#0.000")
            mass_txtIxz.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub mass_txtIyy_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtIyy.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtIyy.Text) Then
            X = CDbl(mass_txtIyy.Text)
            mass_txtIyy.Text = Format(X, "#0.000")
            BodyInertia(1, 1) = X
        Else
            MsgBox("Please enter a number !")
            mass_txtIyy.Text = Format(BodyInertia(1, 1), "#0.000")
            mass_txtIyy.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub mass_txtIyz_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtIyz.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtIyz.Text) Then
            X = CDbl(mass_txtIyz.Text)
            mass_txtIyz.Text = Format(X, "#0.000")
            BodyInertia(1, 2) = X
            BodyInertia(2, 1) = X
        Else
            MsgBox("Please enter a number !")
            mass_txtIyz.Text = Format(BodyInertia(1, 2), "#0.000")
            mass_txtIyz.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub mass_txtIzz_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtIzz.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtIzz.Text) Then
            X = CDbl(mass_txtIzz.Text)
            mass_txtIzz.Text = Format(X, "#0.000")
            BodyInertia(2, 2) = X
        Else
            MsgBox("Please enter a number !")
            mass_txtIzz.Text = Format(BodyInertia(2, 2), "#0.000")
            mass_txtIzz.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub mass_txtPanelMass_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtPanelMass.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtPanelMass.Text) Then
            X = CDbl(mass_txtPanelMass.Text)
            '
            If WithinLimits(X, 0.1, 100.0#) = False Then
                mass_txtPanelMass.Focus()
                Exit Sub
            End If
            mass_txtPanelMass.Text = Format(X, "#0.000")
        Else
            MsgBox("Please enter a number !")
            mass_txtPanelMass.Text = CStr(1)
            mass_txtPanelMass.Focus()
        End If
        ''
    End Sub

    Private Sub mass_txtBoomDist_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtBoomDist.Leave
        '
        Dim X As Double
        ' the entries must be a number
        If IsNumeric(mass_txtBoomDist.Text) Then
            X = CDbl(mass_txtBoomDist.Text)
            If WithinLimits(X, 0.4, 10.0#) = False Then
                mass_txtBoomDist.Focus()
                mass_txtBoomDist.Text = Format(GravityDist, "#0.000")
                Exit Sub
            End If
            mass_txtBoomDist.Text = Format(X, "#0.000")
            Select Case mass_cmbBoomFace.Text
                Case "+ X"
                    GravityDist.X = X
                    GravityDist.Y = 0
                    GravityDist.Z = 0
                Case "- X"
                    GravityDist.X = -X
                    GravityDist.Y = 0
                    GravityDist.Z = 0
                Case "+ Y"
                    GravityDist.X = 0
                    GravityDist.Y = X
                    GravityDist.Z = 0
                Case "- Y"
                    GravityDist.X = 0
                    GravityDist.Y = -X
                    GravityDist.Z = 0
                Case "+ Z"
                    GravityDist.X = 0
                    GravityDist.Y = 0
                    GravityDist.Z = X
                Case "- Z"
                    GravityDist.X = 0
                    GravityDist.Y = 0
                    GravityDist.Z = -X
            End Select
        Else
            MsgBox("Please enter a number !")
            mass_txtBoomDist.Text = Format(vctNorme(GravityDist), "#0.000")
            mass_txtBoomDist.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub mass_txtBoomMass_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtBoomMass.Leave
        Dim X As Double
        '
        If IsNumeric(mass_txtBoomMass.Text) Then
            X = CDbl(mass_txtBoomMass.Text)
            If WithinLimits(X, 0, 50.0#) = False Then
                mass_txtBoomMass.Focus()
                mass_txtBoomMass.Text = Format(GravityMass, "#0.000")
                Exit Sub
            End If
            mass_txtBoomMass.Text = Format(X, "#0.000")
            GravityMass = X
        Else
            MsgBox("Please enter a number !")
            mass_txtBoomMass.Text = Format(GravityMass, "#0.000")
            mass_txtBoomMass.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub mass_cmbBoomFace_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mass_cmbBoomFace.SelectedIndexChanged
        '
        Dim X As Double
        X = CDbl(mass_txtBoomDist.Text)
        '
        Select Case mass_cmbBoomFace.Text
            Case "+ X"
                GravityDist.X = X
                GravityDist.Y = 0
                GravityDist.Z = 0
            Case "- X"
                GravityDist.X = -X
                GravityDist.Y = 0
                GravityDist.Z = 0
            Case "+ Y"
                GravityDist.X = 0
                GravityDist.Y = X
                GravityDist.Z = 0
            Case "- Y"
                GravityDist.X = 0
                GravityDist.Y = -X
                GravityDist.Z = 0
            Case "+ Z"
                GravityDist.X = 0
                GravityDist.Y = 0
                GravityDist.Z = X
            Case "- Z"
                GravityDist.X = 0
                GravityDist.Y = 0
                GravityDist.Z = -X
        End Select
        update_attitude_data()
        ''
    End Sub

    Private Sub update_attitude_data()
        '
        ' Author: Wolfgang Hausmann (06.12.99)
        '
        ' Description:
        ' - calculates whole satellite mass
        ' - calculates whole satellite inertia
        ' - calculates whole satellite center of gravity
        '
        ' Note :
        ' This subroutine takes into account the central body, the gravity gradient stabilisation device and the panels
        ' The mass of dampers if exists must be taking into account in the mass and inertia of central body
        '
        ' Input:   BodyMass         ... mass of central body
        '          BodyInertia()    ... inertia of central body
        '          BodyCG           ... centre of gravity of central body (vector)
        '          GravityMass      ... mass of gravitational device
        '          GravityDist      ... position of gravity mass for gravity gradient stabilisation (vector)
        '          DampersMass()    ... masses of dampers
        '          DampersDist()    ... distance of dampers from center of gravity
        '
        ' Output:  SatMass          ... satellite mass
        '          SatInertia()     ... satellite inertia at center of gravity
        '          SatCG            ... satellite centre of gravity
        '
        Dim i As Integer
        Dim TempPanel As CSolarPanel
        Dim PanelCG As vector
        Dim PanelInertia(2) As Single   ' Inertia of panel around its C.G 0:Ixx, 1:Iyy, 2:Izz

        ' Calculate satellite mass without panels
        ' ---------------------------------------
        SatMass = BodyMass + GravityMass + DampersMass(0) + DampersMass(1) + DampersMass(2)
        '
        ' Calculate satellite mass and centre of gravity
        ' -------------------------------------
        SatCG = vctMultScalar(vctAdd(vctMultScalar(BodyCG, BodyMass), vctMultScalar(GravityDist, GravityMass)), 1 / (BodyMass + GravityMass))
        For i = 1 To PanelsNumber
            TempPanel = SolarPanels.Item(i)
            With TempPanel
                Select Case .PanelType
                    Case "Type1"
                        Select Case .PanelFace
                            Case "+ X" : PanelCG.X = (.PanelLength + Satellite.satBodyWidth) / 2 : PanelCG.Y = 0
                            Case "- X" : PanelCG.X = -(.PanelLength + Satellite.satBodyWidth) / 2 : PanelCG.Y = 0
                            Case "+ Y" : PanelCG.X = 0 : PanelCG.Y = (.PanelLength + Satellite.satBodyWidth) / 2
                            Case "- Y" : PanelCG.X = 0 : PanelCG.Y = -(.PanelLength + Satellite.satBodyWidth) / 2
                        End Select
                        PanelCG.Z = 0
                    Case "Type2"
                        Select Case .PanelFace
                            Case "+ X" : PanelCG.X = Satellite.satBodyWidth / 2 : PanelCG.Y = 0 : PanelCG.Z = 0
                            Case "- X" : PanelCG.X = -Satellite.satBodyWidth / 2 : PanelCG.Y = 0 : PanelCG.Z = 0
                            Case "+ Y" : PanelCG.X = 0 : PanelCG.Y = Satellite.satBodyWidth / 2 : PanelCG.Z = 0
                            Case "- Y" : PanelCG.X = 0 : PanelCG.Y = -Satellite.satBodyWidth / 2 : PanelCG.Z = 0
                            Case "+ Z" : PanelCG.X = 0 : PanelCG.Y = 0 : PanelCG.Z = Satellite.satBodyHeight / 2
                            Case "- Z" : PanelCG.X = 0 : PanelCG.Y = 0 : PanelCG.Z = -Satellite.satBodyHeight / 2
                        End Select
                    Case "Type3"
                        Select Case .PanelFace
                            Case "+ X" : PanelCG.X = (.PanelLength + Satellite.satBodyWidth) / 2 : PanelCG.Y = 0
                            Case "- X" : PanelCG.X = -(.PanelLength + Satellite.satBodyWidth) / 2 : PanelCG.Y = 0
                            Case "+ Y" : PanelCG.X = 0 : PanelCG.Y = (.PanelLength + Satellite.satBodyWidth) / 2
                            Case "- Y" : PanelCG.X = 0 : PanelCG.Y = -(.PanelLength + Satellite.satBodyWidth) / 2
                        End Select
                        Select Case .PanelHpos
                            Case "Upper" : PanelCG.Z = Satellite.satBodyHeight / 2
                            Case "Lower" : PanelCG.Z = -Satellite.satBodyHeight / 2
                            Case "Middle" : PanelCG.Z = 0
                        End Select
                End Select
                '
                SatCG = vctMultScalar(vctAdd(vctMultScalar(SatCG, SatMass), vctMultScalar(PanelCG, CDbl(TempPanel.PanelMass))), 1 / (SatMass + .PanelMass))
            End With
            SatMass += TempPanel.PanelMass  ' Update mass with panels
        Next
        '
        ' Calculate satellite matrix of inertia around C.G
        ' ------------------------------------------------
        SatInertia(0, 0) = BodyInertia(0, 0) _
                       + ((GravityDist.Y - SatCG.Y) ^ 2 + (GravityDist.Z - SatCG.Z) ^ 2) * GravityMass _
                       + ((BodyCG.Y ^ 2 - SatCG.Y) + (BodyCG.Z - SatCG.Z) ^ 2) * BodyMass                   ' Ixx
        ' contribution of dampers must be added here here
        SatInertia(1, 1) = BodyInertia(1, 1) _
                       + ((GravityDist.X - SatCG.X) ^ 2 + (GravityDist.Z - SatCG.Z) ^ 2) * GravityMass _
                       + ((BodyCG.X - SatCG.X) ^ 2 + (BodyCG.Z - SatCG.Z) ^ 2) * BodyMass                   ' Iyy
        ' contribution of dampers must be added here here
        SatInertia(2, 2) = BodyInertia(2, 2) _
                        + ((GravityDist.X - SatCG.X) ^ 2 + (GravityDist.Y - SatCG.Y) ^ 2) * GravityMass _
                       + ((BodyCG.X - SatCG.X) ^ 2 + (BodyCG.Y - SatCG.Y) ^ 2) * BodyMass                   ' Izz
        ' contribution of dampers must be added here here
        SatInertia(0, 1) = BodyInertia(0, 1) _
                       + (BodyCG.X - SatCG.X) * (BodyCG.Y - SatCG.Y) * BodyMass _
                       + (GravityDist.X - SatCG.X) * (GravityDist.Y - SatCG.Y) * GravityMass
        SatInertia(0, 2) = BodyInertia(0, 2) _
                       + (BodyCG.X - SatCG.X) * (BodyCG.Z - SatCG.Z) * BodyMass _
                       + (GravityDist.X - SatCG.X) * (GravityDist.Z - SatCG.Z) * GravityMass
        SatInertia(1, 2) = BodyInertia(1, 2) _
                       + (BodyCG.Y - SatCG.Y) * (BodyCG.Z - SatCG.Z) * BodyMass _
                       + (GravityDist.Y - SatCG.Y) * (GravityDist.Z - SatCG.Z) * GravityMass
        SatInertia(1, 0) = SatInertia(0, 1)
        SatInertia(2, 0) = SatInertia(0, 2)
        SatInertia(2, 1) = SatInertia(1, 2)
        '

        For i = 1 To PanelsNumber
            TempPanel = SolarPanels.Item(i)
            With TempPanel
                ' Calculate inertia of panel around its C.G
                Select Case .PanelType
                    Case "Type1"
                        Select Case .PanelFace  ' A COMPLETER AVEC LA ROTATION DU PANNEAU, pour l'instant on suppose que le panneau pointe vers Z
                            Case "+ X", "- X"
                                PanelInertia(0) = .PanelWidth ^ 2 * .PanelMass / 12 : PanelInertia(1) = .PanelLength ^ 2 * .PanelMass / 12
                            Case "+ Y", "- Y"
                                PanelInertia(0) = .PanelLength ^ 2 * .PanelMass / 12 : PanelInertia(1) = .PanelWidth ^ 2 * .PanelMass / 12
                        End Select
                        PanelInertia(2) = (.PanelLength ^ 2 + .PanelWidth ^ 2) * .PanelMass / 12
                    Case "Type2"
                        Select Case .PanelFace
                            Case "+ X", "- X"
                                PanelInertia(0) = (.PanelLength ^ 2 + .PanelWidth ^ 2) * .PanelMass / 12 : PanelInertia(1) = .PanelLength ^ 2 * .PanelMass / 12 : PanelInertia(2) = .PanelWidth ^ 2 * .PanelMass / 12
                            Case "+ Y", "- Y"
                                PanelInertia(0) = .PanelLength ^ 2 * .PanelMass / 12 : PanelInertia(1) = (.PanelLength ^ 2 + .PanelWidth ^ 2) * .PanelMass / 12 : PanelInertia(2) = .PanelWidth ^ 2 * .PanelMass / 12
                            Case "+ Z", "- Z"
                                PanelInertia(0) = .PanelWidth ^ 2 * .PanelMass / 12 : PanelInertia(1) = .PanelLength ^ 2 * .PanelMass / 12 : PanelInertia(2) = (.PanelLength ^ 2 + .PanelWidth ^ 2) * .PanelMass / 12
                        End Select
                    Case "Type3"
                        Select Case .PanelFace
                            Case "+ X", "- X"
                                PanelInertia(0) = .PanelWidth ^ 2 * .PanelMass / 12 : PanelInertia(1) = .PanelLength ^ 2 * .PanelMass / 12
                            Case "+ Y", "- Y"
                                PanelInertia(0) = .PanelLength ^ 2 * .PanelMass / 12 : PanelInertia(1) = .PanelWidth ^ 2 * .PanelMass / 12
                        End Select
                        PanelInertia(2) = (.PanelLength ^ 2 + .PanelWidth ^ 2) * .PanelMass / 12
                End Select
                '
            End With
        Next







        '
        ' Calculates the distance from the centre of mass to the surfaces
        ' ---------------------------------------------------------------
        'Call Rs_vect()
        '
        ' Display data
        ' ------------
        mass_lblSatMass.Text = Format(SatMass, "#0.000")
        mass_lblSatGx.Text = Format(SatCG.X, "#0.000")
        mass_lblSatGy.Text = Format(SatCG.Y, "#0.000")
        mass_lblSatGz.Text = Format(SatCG.Z, "#0.000")
        mass_lblSatIxx.Text = Format(SatInertia(0, 0), "#0.000")
        mass_lblSatIyy.Text = Format(SatInertia(1, 1), "#0.000")
        mass_lblSatIzz.Text = Format(SatInertia(2, 2), "#0.000")
        mass_lblSatIxy.Text = Format(SatInertia(0, 1), "#0.000")
        mass_lblSatIxz.Text = Format(SatInertia(0, 2), "#0.000")
        mass_lblSatIyz.Text = Format(SatInertia(1, 2), "#0.000")
        ''
    End Sub



   

    
    Private Sub TextBox36_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles prop_txtMG_Y.TextChanged

    End Sub

    Private Sub Label20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles zlbl20.Click

    End Sub
End Class
