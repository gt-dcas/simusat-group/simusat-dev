﻿Module Utilitaires

    Public Function checkboth(ByVal place As Object, ByVal value As Single, ByVal max As Single, ByVal a As Boolean, _
                                ByVal min As Single, ByVal i As Boolean, ByVal oldvalue As Single) As Single
        '
        ' Checks if value is within upper & lower limits
        ' ----------------------------------------------
        ' Inputs:
        '   - Input widget
        '   - value
        '   - max
        '   - a, True = equal, False = Not equal
        '   - min
        '   - i, True = equal, False = Not equal
        '   - oldvalue
        '
        Dim xs As String
        Dim ys As String
        Dim flag As Boolean
        '
        flag = True
        xs = " Valeur <= " & max
        ys = min & " <="
        If a = False Then
            xs = " Valeur < " & max
        End If
        If i = False Then
            ys = min & " <"
        End If
        If value < min Or value > max Then
            flag = False
        End If
        If a = False And value = max Then
            flag = False
        End If
        If i = False And value = min Then
            flag = False
        End If
        xs = ys & xs
        If flag = False Then
            xs = MsgBox("Valeur hors limite !! Saisissez une valeur : " _
            & Chr(13) & "                      " & xs, vbExclamation)
            place.Text = oldvalue
            place.SetFocus()
            checkboth = oldvalue
        Else
            checkboth = value
        End If

    End Function
    Public Function value_limiter(ByVal value As Single, ByVal min As Single, ByVal max As Single) As Single
        '
        ' value_limiter function
        '
        ' Keeps the value within a valid range
        '
        If value > max Then
            value = max
        ElseIf value < min Then
            value = min
        End If
        value_limiter = value

    End Function
    Public Function checktypeSng(ByVal place As Object, ByVal value As Single, ByVal oldvalue As Single) As Single
        '
        Dim xs As String
        '
        On Error GoTo Errortype
        checktypeSng = CSng(value)
        Exit Function
        '
Errortype:
        checktypeSng = oldvalue
        xs = MsgBox("Type de donnée non valide", vbCritical)
        place.Text = oldvalue
        place.SetFocus()
        ''
    End Function
    Public Function checkdown(ByVal place As Object, ByVal value As Single, ByVal min As Single, ByVal i As Boolean, ByVal oldvalue As Single) As Single
        '
        ' Checks if value is above lower limit
        '
        ' Inputs:
        '   - Input widget
        '   - value
        '   - min
        '   - i, True = equal, False = Not equal
        '   - oldvalue
        '
        Dim xs As String
        Dim flag As Boolean

        flag = True
        xs = ">= " & min
        If i = False Then
            xs = "> " & min
        End If
        If value < min Then
            flag = False
        End If
        If i = False And value = min Then
            flag = False
        End If
        If flag = False Then
            xs = MsgBox("Valeur insuffisante! Saisissez une valeur " _
            & xs, vbExclamation)
            place.Text = oldvalue
            checkdown = oldvalue
        Else
            checkdown = value
        End If

    End Function
    Public Function checkup(ByVal place As Object, ByVal value As Single, ByVal max As Single, ByVal a As Boolean, ByVal oldvalue As Single) As Single
        '
        ' Checks if value is below upper limit
        '
        ' Inputs:
        '   - Input widget
        '   - value
        '   - max
        '   - a, True = equal, False = Not equal
        '   - oldvalue
        '

        Dim xs As String
        Dim flag As Boolean

        flag = True
        xs = "<= " & max
        If a = False Then
            xs = "< " & max
        End If
        If value > max Then
            flag = False
        End If
        If a = False And value = max Then
            flag = False
        End If
        If flag = False Then
            xs = MsgBox("Valeur excessive !! Saisissez une valeur " & xs, vbExclamation)
            place.Text = oldvalue
            checkup = oldvalue
        Else
            checkup = value
        End If

    End Function
    Public Function checktypeLng(ByVal place As Object, ByVal value As Single, ByVal oldvalue As Long) As Long
        '
        Dim xs As String
        '
        On Error GoTo Errortype
        checktypeLng = CLng(value)
        Exit Function
        '
Errortype:
        checktypeLng = oldvalue
        xs = MsgBox("Type de donnée non valide", vbCritical)
        place.Text = oldvalue
        place.SetFocus()
        ''
    End Function

    Public Function read_line() As String
        '
        ' Lit une ligne du fichier satellite
        '
        Dim X$ = ""
        '
        X$ = LineInput(1)
        If Left$(X$, 1) <> "!" Then
            read_line = Left$(X$, 20)
        Else
            read_line = ""
        End If
        ''
    End Function
    Public Sub load_att_conf(ByVal file As String)
        '
        '
        Dim i As Integer
        Dim s$ = ""
        '
        On Error Resume Next
        FileOpen(1, file, OpenMode.Input)
        On Error GoTo 0
        '
        s$ = (read_line())                      ' "! --------------------------"
        s$ = (read_line())                      ' "! Attitude control subsystem"
        s$ = (read_line())                      ' "! --------------------------"
        s$ = (read_line())                      ' "!"
        '
        s$ = (read_line())                      ' "! Central body geometry mass inertie"
        s$ = (read_line())                      ' "! ----------------------------------"
        BodyMass = (read_line())                ' "Body mass"
        Satellite.satBodyHeight = (read_line()) ' "Body height"
        Satellite.satBodyWidth = (read_line())  ' "Body width"
        BodyCG.X = (read_line())                ' "Body CG.X"
        BodyCG.Y = (read_line())                ' "Body CG.Y"
        BodyCG.Z = (read_line())                ' "Body CG.Z"
        BodyInertia(0, 0) = (read_line())       ' "Body Ixx"
        BodyInertia(1, 1) = (read_line())       ' "Body Iyy"
        BodyInertia(2, 2) = (read_line())       ' "Body Izz"
        BodyInertia(0, 1) = (read_line())       ' "Body Ixy"
        BodyInertia(0, 2) = (read_line())       ' "Body Ixz"
        BodyInertia(1, 2) = (read_line())       ' "Body Iyz"
        '
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! Gravity gradient device"
        s$ = (read_line())                      ' "! -----------------------"
        GravityMass = (read_line())             ' "Gradient gravity device mass")
        GravityDist.X = (read_line())           ' "Gradient gravity device position X")
        GravityDist.Y = (read_line())           ' "Gradient gravity device position Y")
        GravityDist.Z = (read_line())           ' "Gradient gravity device position Z")
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! External panels"
        s$ = (read_line())                      ' "! ---------------"
        PanelsNumber = (read_line())            ' "Number of panels"
        SolarPanels.Clear()
        For i = 1 To PanelsNumber
            Dim TempPanel As New CSolarPanel
            With TempPanel
                .PanelType = Trim(read_line())  ' "Type"
                .PanelName = Trim(read_line())  ' "Name"
                .PanelFace = Trim(read_line())  ' "Mounting face"
                .PanelDir = Trim(read_line())   ' "Orientation"
                .PanelHpos = Trim(read_line())  ' "Height position"
                .PanelLength = (read_line())    ' "Length"
                .PanelWidth = (read_line())     ' "Width"
                .PanelMass = (read_line())      ' "Mass"
            End With
            SolarPanels.Add(TempPanel)
        Next
        '
        s$ = (read_line())                      ' "!")
        s$ = (read_line())                      ' "! Simulation data")
        s$ = (read_line())                      ' "! ---------------")
        dblSimulationStart = (read_line())      ' " Simulation start date"
        dblTimeStep = (read_line())             ' " Simulation end date"
        '
        '
        FileClose(1)
        ''
    End Sub
    Public Sub save_att_conf(ByVal file As String)
        '
        Dim i As Integer
        Dim xs As String
        Dim TempPanel As CSolarPanel = Nothing
        '
        On Error GoTo errorsave
        If My.Computer.FileSystem.FileExists(file) Then Kill(file)
        On Error GoTo 0
        '
        FileOpen(1, file, OpenMode.Output)
        '
        PrintLine(1, "! --------------------------")
        PrintLine(1, "! Attitude control subsystem")
        PrintLine(1, "! --------------------------")
        PrintLine(1, "!")
        '
        PrintLine(1, "! Central body geometry mass inertie")
        PrintLine(1, "! ----------------------------------")
        PrintLine(1, BodyMass, TAB(24), "Body mass")
        PrintLine(1, Satellite.satBodyHeight, TAB(24), "Body height")
        PrintLine(1, Satellite.satBodyWidth, TAB(24), "Body width")
        PrintLine(1, BodyCG.X, TAB(24), "Body CG.X")
        PrintLine(1, BodyCG.Y, TAB(24), "Body CG.Y")
        PrintLine(1, BodyCG.Z, TAB(24), "Body CG.Z")
        PrintLine(1, BodyInertia(0, 0), TAB(24), "Body Ixx")
        PrintLine(1, BodyInertia(1, 1), TAB(24), "Body Iyy")
        PrintLine(1, BodyInertia(2, 2), TAB(24), "Body Izz")
        PrintLine(1, BodyInertia(0, 1), TAB(24), "Body Ixy")
        PrintLine(1, BodyInertia(0, 2), TAB(24), "Body Ixz")
        PrintLine(1, BodyInertia(1, 2), TAB(24), "Body Iyz")
        '
        PrintLine(1, "!")
        PrintLine(1, "! Gravity gradient device")
        PrintLine(1, "! -----------------------")
        PrintLine(1, GravityMass, TAB(24), "Gradient gravity device mass")
        PrintLine(1, GravityDist.X, TAB(24), "Gradient gravity device position X")
        PrintLine(1, GravityDist.Y, TAB(24), "Gradient gravity device position Y")
        PrintLine(1, GravityDist.Z, TAB(24), "Gradient gravity device position Z")
        '
        PrintLine(1, "!")
        PrintLine(1, "! External panels")
        PrintLine(1, "! ---------------")
        PrintLine(1, PanelsNumber, TAB(24), "Panels number")
        For i = 1 To PanelsNumber
            TempPanel = SolarPanels.Item(i)
            With TempPanel
                PrintLine(1, .PanelType, TAB(24), "Type")
                PrintLine(1, .PanelName, TAB(24), "Name")
                PrintLine(1, .PanelFace, TAB(24), "Mounting face")
                PrintLine(1, .PanelDir, TAB(24), "Orientation")
                PrintLine(1, .PanelHpos, TAB(24), "Height position")
                PrintLine(1, .PanelLength, TAB(24), "Length")
                PrintLine(1, .PanelWidth, TAB(24), "Width")
                PrintLine(1, .PanelMass, TAB(24), "Mass")
            End With
        Next
        '
        '
        PrintLine(1, "!")
        PrintLine(1, "! Simulation data")
        PrintLine(1, "! ---------------")
        PrintLine(1, dblSimulationStart)
        PrintLine(1, dblTimeStep)
        '
        FileClose(1)
        '
        Exit Sub
        '
errorsave:
        xs = MsgBox("Writing error" & Chr(13) _
        & " Saving data is not possible !!", vbCritical)
        ''
    End Sub
End Module
