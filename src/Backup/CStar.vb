﻿Imports System.Math

Public Class CStar


    Private m_sinDec As Single  ' sine of dec
    Private m_cosDec As Single  ' cosine of dec
    Private m_sinAsc As Single ' sine of ad
    Private m_cosAsc As Single ' cosine of ad
    Private m_ad As Single ' ad ??
    Private m_dec As Single ' dec ??
    Private m_mg As Single ' magnitude
    '
    Public Sub initStarFromLine(ByVal sLine As String)
        '
        Dim sNb As String = ""
        Dim sMg As String = ""
        Dim sClasse As String = ""
        Dim sAdh As String = ""
        Dim sAdm As String = ""
        Dim sAds As String = ""
        Dim sDecd As String = ""
        Dim sDecm As String = ""
        Dim sDecs As String = ""
        Dim sFin As String = ""
        '
        If Not String.IsNullOrEmpty(sLine) Then
            '
            splitLine(sLine, sNb, sMg, sClasse, sAdh, sAdm, sAds, sDecd, sDecm, sDecs, sFin)
            Dec = Abs(CSng(sDecd)) + CSng(sDecm) / 60 + CSng(sDecs) / 3600
            Dec = Dec * Math.PI / 180
            If CSng(sDecd) < 0 Then
                Dec = -Dec
            End If
            Ad = 15 * CSng(sAdh) + 0.25 * CSng(sAdm) + 0.25 / 60 * CSng(sAds)
            Ad = Ad * Math.PI / 180
            cosDec = Cos(Dec)
            sinDec = Sin(Dec)
            sinAsc = Sin(Ad)
            cosAsc = Cos(Ad)
            Mg = CSng(sMg)
            '
        End If
        '
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="star1"></param>
    ''' <remarks></remarks>
    Public Sub copy(ByVal star1 As CStar)
        Ad = star1.Ad
        cosAsc = star1.cosAsc
        cosDec = star1.cosDec
        Dec = star1.Dec
        Mg = star1.Mg
        sinAsc = star1.sinAsc
        sinDec = star1.sinDec
    End Sub
    ''' <summary>
    ''' On extrait chaque paramètre de la ligne
    ''' </summary>
    ''' <param name="sLine">La ligne à analyser</param>
    ''' <param name="sNb">paramètre NB</param>
    ''' <param name="sMg"></param>
    ''' <param name="sClasse"></param>
    ''' <param name="sAdh"></param>
    ''' <param name="sAdm"></param>
    ''' <param name="sAds"></param>
    ''' <param name="sDecd"></param>
    ''' <param name="sDecm"></param>
    ''' <param name="sDecs"></param>
    ''' <param name="sFin"></param>
    ''' <remarks></remarks>
    ''' 

    Public Sub splitLine(ByRef sLine As String, _
                         ByRef sNb As String, _
                         ByRef sMg As String, _
                         ByRef sClasse As String, _
                         ByRef sAdh As String, _
                         ByRef sAdm As String, _
                         ByRef sAds As String, _
                         ByRef sDecd As String, _
                         ByRef sDecm As String, _
                         ByRef sDecs As String, _
                         ByRef sFin As String)
        '
        Dim nb_Length As Integer = 5
        Dim mg_Length As Integer = 5
        Dim classe_Length As Integer = 5
        Dim adh_Length As Integer = 2
        Dim adm_Length As Integer = 2
        Dim ads_Length As Integer = 6
        Dim decd_Length As Integer = 3
        Dim decm_Length As Integer = 2
        Dim decs_Length As Integer = 5
        Dim fin_Length As Integer = 2
        ' Le total doit faire 37
        Dim index As Integer
        '
        index = 1

        sNb = Strings.Mid(sLine, index, nb_Length)
        index += nb_Length

        sMg = Strings.Mid(sLine, index, mg_Length)
        index += mg_Length

        sClasse = Strings.Mid(sLine, index, classe_Length)
        index += classe_Length

        sAdh = Strings.Mid(sLine, index, adh_Length)
        index += adh_Length

        sAdm = Strings.Mid(sLine, index, adm_Length)
        index += adm_Length

        sAds = Strings.Mid(sLine, index, ads_Length)
        index += ads_Length

        sDecd = Strings.Mid(sLine, index, decd_Length)
        index += decd_Length

        sDecm = Strings.Mid(sLine, index, decm_Length)
        index += decm_Length

        sDecs = Strings.Mid(sLine, index, decs_Length)
        index += decs_Length

        sFin = Strings.Mid(sLine, index, fin_Length)

    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Ad() As Single
        Get
            Return m_ad
        End Get
        Set(ByVal value As Single)
            m_ad = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property cosAsc() As Single
        Get
            Return m_cosAsc
        End Get
        Set(ByVal value As Single)
            m_cosAsc = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property cosDec() As Single
        Get
            Return m_cosDec
        End Get
        Set(ByVal value As Single)
            m_cosDec = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Dec() As Single
        Get
            Return m_dec
        End Get
        Set(ByVal value As Single)
            m_dec = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Mg() As Single
        Get
            Return m_mg
        End Get
        Set(ByVal value As Single)
            m_mg = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property sinAsc() As Single
        Get
            Return m_sinAsc
        End Get
        Set(ByVal value As Single)
            m_sinAsc = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property sinDec() As Single
        Get
            Return m_sinDec
        End Get
        Set(ByVal value As Single)
            m_sinDec = value
        End Set
    End Property
    '
    '
End Class
