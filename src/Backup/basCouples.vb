﻿Module basCouples
    '
    ' author: Lorenzo Donato
    ' changed and transfered to SIMUSAT by Wolfgang Hausmann
    ' (26.11.99)
    '
    ' Included functions and procedures:
    '
    ' Public Sub couple_magnetique(date_cour As Double, Cmag() As Double, b() As Double, Y() As Double)
    '    (couple champ geomagnetique)
    '
    ' Public Sub couple_aero(date_jul As Double, Naer() As Double, Y() As Double)
    '    (couple frottement atmospherique)
    '
    ' Sub couple_gravite(Ngg() As Double, Y() As Double)
    '    (Couple gradient de gravité)
    '
    ' Public Sub couple_solaire(dj As Double, Nsol() As Double, Y() As Double)
    '    (Couple pression solaire)
    '
    ' Public Sub couple_total(djules50_courant As Double)
    '    (couple total)
    '

    Public Bgeo As vector
    Public Cmag As vector   ' Magnetic torque
    Public Cgg As vector    ' Gravity gradient torque
    Public Caer As vector   ' Aerodynamic torque
    Public Csol As vector   ' Solar torque
    Public Ctot As vector   ' Total torque


    Public Function couple_magnetique(ByVal time As Double) As vector
        '
        ' Couple due to magnetic field
        ' This function computes the magnetic field at thae satellite position (SatGeoPos.alt, SatGeoPos.lon, SatGeoPos.lat)
        ' using the IGRF model available on : ftp://nssdcftp.gsfc.nasa.gov/models/
        ' The data are in this function, but the program could be modified in order to read these data in a file that could be easely upgraded

        Dim ns, n, m As Integer
        Dim B0 As vector, B1 As vector ' Valeurs intermédiaires de calcul des composantes du champ
        Dim delta95 As Double
        Dim g(27) As Double, h(27) As Double
        Dim s(27) As Double, Pc(27) As Double, dP(27) As Double, k(27) As Double
        Dim cosmfi As Double, sinmfi As Double, cosmfi_ As Double, sinmfi_ As Double
        Dim fi As Double, colatitude As Double
        Dim r_sat As Double
        '
        Const rt As Long = 6371           ' Km : rayon IGRF de la Terre
        '
        ' Distance satellite terre
        r_sat = vctNorme(SatPos)
        '
        ' Paramètres IGRF pour calculer le champ géomagnetique de la terre
        delta95 = time / 365 - 45
        '
        g(0) = 0
        g(1) = -29682 + 17.6 * delta95
        g(2) = -1789 + 13 * delta95
        g(3) = -2197 - 13.2 * delta95
        g(4) = 3074 + 3.7 * delta95
        g(5) = 1685 - 0.8 * delta95
        g(6) = 1329 + 1.5 * delta95
        g(7) = -2268 - 6.4 * delta95
        g(8) = 1249 - 0.2 * delta95
        g(9) = 769 - 8.1 * delta95

        g(10) = 941 + 0.8 * delta95
        g(11) = 782 + 0.9 * delta95
        g(12) = 291 - 6.9 * delta95
        g(13) = -421 + 0.5 * delta95
        g(14) = 116 - 4.6 * delta95
        g(15) = -210 + 0.8 * delta95
        g(16) = 352 + 0.1 * delta95
        g(17) = 237 - 1.5 * delta95
        g(18) = -122 - 2 * delta95
        g(19) = -167 - 0.1 * delta95

        g(20) = -26 + 2.3 * delta95
        g(21) = 66 + 0.5 * delta95
        g(22) = 64 - 0.4 * delta95
        g(23) = 65 + 0.6 * delta95
        g(24) = -172 + 1.9 * delta95
        g(25) = 2 - 0.2 * delta95
        g(26) = 17 - 0.2 * delta95
        g(27) = -94
        '
        h(0) = 0
        h(1) = 0
        h(2) = 5318 - 18.3 * delta95
        h(3) = 0
        h(4) = -2356 - 15 * delta95
        h(5) = -425 - 8.8 * delta95
        h(6) = 0
        h(7) = -263 + 4.1 * delta95
        h(8) = 302 + 2.2 * delta95
        h(9) = -406 - 12.1 * delta95

        h(10) = 0
        h(11) = 262 + 1.8 * delta95
        h(12) = -232 + 1.2 * delta95
        h(13) = 98 + 2.7 * delta95
        h(14) = -301 - 1 * delta95
        h(15) = 0
        h(16) = 44 + 0.2 * delta95
        h(17) = 157 + 1.2 * delta95
        h(18) = -152 + 0.3
        h(19) = -64 + 1.8 * delta95 * delta95

        h(20) = 99 + 0.9 * delta95
        h(21) = 0
        h(22) = -16 + 0.3 * delta95
        h(23) = 77 - 1.6 * delta95
        h(24) = 67 - 0.2 * delta95
        h(25) = -57 - 0.9 * delta95
        h(26) = 4 + 1 * delta95
        h(27) = 28 + 2.2 * delta95
        '
        ns = 0
        s(0) = 1
        For n = 1 To 6
            s(ns + n) = s(ns) * (2 * n - 1) / n
            g(ns + n) = s(ns + n) * g(ns + n)
            h(ns + n) = s(ns + n) * h(ns + n)
            s(ns + n + 1) = s(ns + n) * Math.Sqrt(2 * n) / Math.Sqrt(n + 1)
            g(ns + n + 1) = s(ns + n + 1) * g(ns + n + 1)
            h(ns + n + 1) = s(ns + n + 1) * h(ns + n + 1)
            '
            For m = 2 To n
                s(ns + n + m) = s(ns + n + m - 1) * Math.Sqrt(n - m + 1) / Math.Sqrt(n + m)
                g(ns + n + m) = s(ns + n + m) * g(ns + n + m)
                h(ns + n + m) = s(ns + n + m) * h(ns + n + m)
            Next
            ns = ns + n
        Next
        '
        colatitude = pi / 2 - SatGeoPos.lat
        fi = Modulo(SatGeoPos.lon, 2 * pi)
        '
        ns = 0
        Pc(0) = 1
        Pc(1) = Math.Cos(colatitude)
        '
        For n = 2 To 6
            Pc(ns + n) = Math.Sin(colatitude) * Pc(ns)
            For m = 0 To n - 1
                Pc(ns + n + m + 1) = Math.Cos(colatitude) * Pc(ns + m + 1) - ((n - 1) ^ 2 - m ^ 2) * _
                Pc(ns - n + m + 2) / (2 * n - 1) / (2 * n - 3)
            Next
            ns = ns + n
        Next
        '
        ns = 0
        dP(0) = 0
        dP(1) = -Math.Sin(colatitude)
        '
        For n = 2 To 6
            dP(ns + n) = Math.Sin(colatitude) * dP(ns) + Math.Cos(colatitude) * Pc(ns)
            For m = 0 To n - 1
                dP(ns + n + m + 1) = Math.Cos(colatitude) * dP(ns + m + 1) - Math.Sin(colatitude) * Pc(ns + m + 1) _
                - ((n - 1) ^ 2 - m ^ 2) / (2 * n - 1) / (2 * n - 3) * dP(ns - n + m + 2)
            Next
            ns = ns + n
        Next
        '
        ns = 0
        B0.X = 0
        B0.Y = 0
        B0.Z = 0
        '
        For n = 1 To 6
            cosmfi = 1
            sinmfi = 0
            B1.X = 0
            B1.Y = 0
            B1.Z = 0
            '
            For m = 0 To n
                B1.X = B1.X + (g(ns + n + m) * cosmfi + h(ns + n + m) * sinmfi) * Pc(ns + n + m)
                B1.Y = B1.Y + (g(ns + n + m) * cosmfi + h(ns + n + m) * sinmfi) * dP(ns + n + m)
                B1.Z = B1.Z + m * (-g(ns + n + m) * sinmfi + h(ns + n + m) * cosmfi) * Pc(ns + n + m)
                cosmfi_ = cosmfi
                sinmfi_ = sinmfi
                cosmfi = Math.Cos(fi) * cosmfi_ - Math.Sin(fi) * sinmfi_
                sinmfi = Math.Sin(fi) * cosmfi_ + Math.Cos(fi) * sinmfi_
            Next
            ns = ns + n
            B0.X = B0.X + (rt / r_sat) ^ (n + 2) * (n + 1) * B1.X
            B0.Y = B0.Y - ((rt / r_sat) ^ (n + 2)) * B1.Y
            B0.Z = B0.Z + (rt / r_sat) ^ (n + 2) * B1.Z
        Next
        ' On obtient finalement (en nanoTeslas) :
        ' B0.X composante centre terre point de calcul (+ vers le haut)
        ' B0.Y composante nord sud (+ vers sud)
        ' B0.Z composante ouest est (+ vers est)
        '
        ' Multiplication par 10e-9 pour avoir les résultats en Teslas
        B0 = vctMultScalar(B0, 0.000000001)
        '
        ' Composantes dans Greenwich
        Bgeo.X = (B0.X * Math.Cos(SatGeoPos.lat) + B0.Y * Math.Sin(SatGeoPos.lat)) * Math.Cos(fi) - B0.Z * Math.Sin(fi)
        Bgeo.Y = (B0.X * Math.Cos(SatGeoPos.lat) + B0.Y * Math.Sin(SatGeoPos.lat)) * Math.Sin(fi) + B0.Z * Math.Cos(fi)
        Bgeo.Z = B0.X * Math.Sin(SatGeoPos.lat) - B0.Y * Math.Cos(SatGeoPos.lat)
        '
        ' Bgeo est une variable globale qui donne le champ magnétique terrestre indépendammment du
        ' traitement que l'on en fait derrière.
        '
        ' Composantes dans le repère satellite
        Call coord_greenwich_gamma(time, Bgeo.X, Bgeo.Y, Bgeo.Z, B0.X, B0.Y, B0.Z)

        'Call gamma50_sat(B0.X, B0.Y, B0.Z, B1.X, B1.Y, B1.Z)
        With Satellite.getSatCOSCoords(time, B0)
            B1.X = .X : B1.Y = .Y : B1.Z = .Z ' Magnetic field in the satellite's COS
        End With
        '
        ' Couple de perturbation magnétique sur le satellite
        dipole_sat.X = dipsat_passive.X + dipsat_control.X * dipsat_active.X
        dipole_sat.Y = dipsat_passive.Y + dipsat_control.Y * dipsat_active.Y
        dipole_sat.Z = dipsat_passive.Z + dipsat_control.Z * dipsat_active.Z
        couple_magnetique = vctCross(dipole_sat, B1)
        ''
    End Function




    Public Function couple_aero(ByVal time As Double) As vector
        '
        ' Author: Lorenzo Donato
        ' Changed by Wolfgang Hausmann (25.02.2000)
        '
        ' Description:
        ' Calculates the atmospheric perturbations.
        '
        Const omega_terre As Double = 360.0# / 86164.0#
        Dim i%
        Dim dist_face_CG As vector          ' Distance of central body face to C.G
        Dim vect As vector                  ' Vecteurs intermédiaires de calcul
        Dim x1 As Double, x2 As Double      ' Valeurs intermédiaires de calcul
        Dim Caer_face As vector             ' Couple aéro par face
        Dim s_panel As Double               ' Surface des panneaux
        Dim lambda As Double                ' Taux de super-rotation
        Dim V_atmos As vector               ' Vitesse atmosphère dans gamma50
        Dim Vatm_sat As vector              ' Vitesse relative du satellite/atmosphère
        Dim Vt As vector                    ' Vitesse tangentielle sur la face
        Dim T_exo As Double                 ' Température exosphérique
        Dim pdyn As Double, Va As Double    ' Pression dynamique, vitesse
        Dim cos_incidence As Double         ' cos de l'angle d'incidence
        Dim pa As vector, ta As vector      ' Composantes normale et tangentielle de l'effort
        '
        ' Initialisation du couple à zéro
        ' -------------------------------
        couple_aero.X = 0
        couple_aero.Y = 0
        couple_aero.Z = 0
        '
        ' Vitesse de l'atmosphère dans gamma50 à la position du satellite
        ' ---------------------------------------------------------------
        Select Case SatGeoPos.alt    ' Taux de super-rotation
            Case Is < 400 : lambda = 1 + 0.4 * (SatGeoPos.alt - 100) / 300
            Case 400 To 600 : lambda = 1.4
            Case Else : lambda = 1.4 - 0.4 * (SatGeoPos.alt - 600) / 200
        End Select
        '
        vect.X = 0            ' Vecteur vitesse rotation terre
        vect.Y = 0
        vect.Z = lambda * omega_terre * rad
        V_atmos = vctCross(vect, SatPos)
        '
        ' Vitesse relative du satellite par rapport à l'atmosphère
        ' --------------------------------------------------------
        ' Vitesse relative dans gamma50 (vitesse satellite - vitesse atmosphère)
        V_atmos = vctMultScalar(V_atmos, -1)
        vect = vctAdd(SatVel, V_atmos)
        ' Vitesse relative dans repère satellite
        With Satellite.getSatCOSCoords(time, vect)
            Vatm_sat.X = .X : Vatm_sat.Y = .Y : Vatm_sat.Z = .Z ' Velocity in the satellite's COS
        End With
        '
        ' Pression dynamique
        ' ------------------
        Va = vctNorme(Vatm_sat)                                    ' Intensité de la vitesse
        T_exo = temp_exo(time, SatGeoPos.lon, SatGeoPos.lat, SatGeoPos.alt)   ' Température exosphérique

        pdyn = 0.5 * densite(T_exo, SatGeoPos.alt) * Va * Va * 10000000.0#
        '      
        '
        ' Calcul des forces et moments
        ' ----------------------------
        For i% = 0 To 5
            cos_incidence = vctDot(vect_face(i), vctToNorm(Vatm_sat))
            If cos_incidence > 0 Then
                '
                ' Calcul de la force normale et du couple
                x1 = -2 * pdyn * cos_incidence ^ 2 * (2 - Cn_aero(i%))
                pa = vctMultScalar(vect_face(i%), x1)
                '
                '                   '
                dist_face_CG.X = 0 : dist_face_CG.Y = 0 : dist_face_CG.Z = 0
                With Satellite
                    Select Case i%
                        Case 0, 3
                            dist_face_CG.X = vctMultScalar(BodyCG, -1).X + Satellite.satBodyWidth / 2
                            s_panel = .satBodyWidth * .satBodyHeight
                        Case 1, 4 : dist_face_CG.Y = vctMultScalar(BodyCG, -1).Y + Satellite.satBodyWidth / 2
                            s_panel = .satBodyWidth * .satBodyHeight
                        Case 2, 5 : dist_face_CG.Z = vctMultScalar(BodyCG, -1).Z + Satellite.satBodyHeight / 2
                            s_panel = .satBodyWidth * .satBodyWidth
                    End Select
                End With
                '
                Caer_face = vctCross(vctMultScalar(dist_face_CG, s_panel), pa)
                couple_aero = vctAdd(Caer_face, couple_aero) ' Ajout au couple total
                '
                ' Calcul de la force tangentielle et du couple
                Vt = vctAdd(Vatm_sat, vctMultScalar(vect_face(i%), -Va * cos_incidence)) ' Vitesse tangentielle à la face
                If vctNorme(Vt) <> 0 Then
                    x2 = -2 * pdyn * cos_incidence * Math.Sqrt(1 - (cos_incidence) ^ 2) * Ct_aero(i%)
                    ta = vctMultScalar(Vt, x2 / vctNorme(Vt))
                    Caer_face = vctCross(vctMultScalar(dist_face_CG, s_panel), ta)
                    couple_aero = vctAdd(Caer_face, couple_aero) ' Ajout au couple total
                End If
            End If
            '
            ' If Math.Abs(couple_aero.X) <= 0.0000000001 Then couple_aero.X = 0
            ' If Math.Abs(couple_aero.Y) <= 0.0000000001 Then couple_aero.Y = 0
            ' If Math.Abs(couple_aero.Z) <= 0.0000000001 Then couple_aero.Z = 0
            '
        Next
        ''
    End Function

    Public Function couple_gravite(ByVal time As Double) As vector
        '
        ' Gradient de gravité
        '
        Dim v1 As vector, v2 As vector  ' Vecteurs intermédiaires de calcul
        Dim Rs1 As vector               ' Unit vector earth satellite
        Dim Iner2(2, 2) As Double       ' Inertial mass moments vector
        Dim r_sat As Double
        Dim i%, j%
        '
        ' Distance satellite terre
        r_sat = vctNorme(SatPos)
        '
        ' Vecteur unitaire terre satellite dans repère satellite
        Rs1 = vctToNorm(Satellite.getSatCOSCoords(time, SatPos))
        '
        ' Building tensor for inertial mass moments
        For i% = 0 To 2
            For j% = 0 To 2
                If j% <> i% Then
                    Iner2(i%, j%) = -SatInertia(i%, j%)
                Else
                    Iner2(i%, j%) = SatInertia(i%, j%)
                End If
            Next
        Next
        '
        v1.X = Iner2(0, 0) * Rs1.X + Iner2(0, 1) * Rs1.Y + Iner2(0, 2) * Rs1.Z
        v1.Y = Iner2(1, 0) * Rs1.X + Iner2(1, 1) * Rs1.Y + Iner2(1, 2) * Rs1.Z
        v1.Z = Iner2(2, 0) * Rs1.X + Iner2(2, 1) * Rs1.Y + Iner2(2, 2) * Rs1.Z
        '
        v2 = vctCross(Rs1, v1)
        '
        couple_gravite = vctMultScalar(v2, 3 * 398601.3 / (r_sat ^ 3))
        '
        ''
    End Function

    Public Function couple_solaire(ByVal time As Double) As vector
        '
        ' Calculates solar pressure moment on the satellite.
        '
        Dim sat_sol_gam As vector       ' Vecteur unitaire satellite soleil dans gamma50
        Dim sat_sol As vector           ' Vecteur unitaire satellite soleil dans repère satellite
        Dim cos_incidence As Double  ' Cos de l'angle d'incidence solaire sur chaque face
        Dim c1 As Double, c2 As Double
        Dim f1 As vector, f2 As vector, csol_face As vector
        Dim i%
        Dim dist_face_CG As vector
        Dim s_panel As Double
        '
        couple_solaire.X = 0
        couple_solaire.Y = 0
        couple_solaire.Z = 0
        '
        '
        ' Coordonnées du Soleil dans Gamma, et vecteur unitaire satellite-soleil (sat_sol)
        update_Sun(time, SunPos)
        sat_sol_gam = vctToNorm(vctSub(SunPos, SatPos))
        '
        If Not blnEclipse Then     ' Si satellite éclairé
            '
            ' Vecteur satellite soleil (sat_sol) sur les 6 faces du satellite (dans repère sat)
            With Satellite.getSatCOSCoords(time, sat_sol_gam)
                sat_sol.X = .X : sat_sol.Y = .Y : sat_sol.Z = .Z
            End With
            '
            ' Couple solaire
            For i% = 0 To 5 ' Balayage des 6 faces (X, Y, Z, -X, -Y, -Z)
                '
                cos_incidence = vctDot(vect_face(i%), sat_sol)
                If cos_incidence > 0 Then   ' Condition de surface éclairée
                    '                   '
                    dist_face_CG.X = 0 : dist_face_CG.Y = 0 : dist_face_CG.Z = 0
                    With Satellite
                        Select Case i%
                            Case 0, 3
                                dist_face_CG.X = vctMultScalar(BodyCG, -1).X + Satellite.satBodyWidth / 2
                                s_panel = .satBodyWidth * .satBodyHeight
                            Case 1, 4 : dist_face_CG.Y = vctMultScalar(BodyCG, -1).Y + Satellite.satBodyWidth / 2
                                s_panel = .satBodyWidth * .satBodyHeight
                            Case 2, 5 : dist_face_CG.Z = vctMultScalar(BodyCG, -1).Z + Satellite.satBodyHeight / 2
                                s_panel = .satBodyWidth * .satBodyWidth
                        End Select
                    End With
                    '
                    c1 = 2 * (Cr_solar(i%) + Cd_solar(i%) / 3)
                    f1 = vctMultScalar(vect_face(i%), c1)
                    c2 = 1 - Cr_solar(i%)
                    f2 = vctMultScalar(sat_sol, c2)
                    ' Force totale
                    f1 = vctAdd(f1, f2)
                    c1 = -0.5 * PS * s_panel * cos_incidence    ' PS = 0.000004644 N/m² Pression de radiations solaire
                    f1 = vctMultScalar(f1, c1)
                    ' Couple pression solaire
                    csol_face = vctCross(dist_face_CG, f1)
                    couple_solaire = vctAdd(couple_solaire, csol_face)
                    '
                End If
            Next
        End If
        ''
    End Function


    Public Function couple_total(ByVal time As Double) As vector
        '
        ' ------------
        ' Couple total
        ' ------------
        '
        Cgg = couple_gravite(time)
        Caer = couple_aero(time)
        Csol = couple_solaire(time)
        Cmag = couple_magnetique(time)
        '
        Ctot = vctAdd(Cmag, Caer)
        Ctot = vctAdd(Csol, Ctot)
        Ctot = vctAdd(Cgg, Ctot)
        '
    End Function


End Module
