Option Strict Off
Option Explicit On
Imports System.Math
Module basMath
    '# ==[ vector calculation routines ]===============================

    ''' <summary>
    ''' returns length of hypothenuse in triangle defined by 2 vectors
    ''' </summary>
    ''' <param name="a">vector defining triangle</param>
    ''' <param name="b">vector defining triangle</param>
    ''' <returns>length of hypothenuse</returns>
    ''' <remarks></remarks>
    Public Function vctDist(ByRef a As vector, ByRef b As vector) As Double
        Dim h1, h2 As Double
        h1 = vctNorme(a) '# get absolute heights
        h2 = vctNorme(b) '# then use the law of cosine
        vctDist = Sqrt(h1 ^ 2 + h2 ^ 2 - 2 * h1 * h2 * Cos(vctAngle(a, b)))
    End Function

    ''' <summary>
    ''' create a vector (x,y,z)
    ''' </summary>
    ''' <param name="X">coordinates of the vector</param>
    ''' <param name="Y">coordinates of the vector</param>
    ''' <param name="Z">coordinates of the vector</param>
    ''' <returns>return vector with x,y,z as coordinates</returns>
    ''' <remarks></remarks>
    Function vct(ByRef X As Double, ByRef Y As Double, ByRef Z As Double) As vector
        vct.X = X
        vct.Y = Y
        vct.Z = Z
    End Function

    ''' <summary>
    ''' calculate the cross product of the two given vectors
    ''' </summary>
    ''' <param name="a">vector to calcutate cross product of</param>
    ''' <param name="b">vector to calcutate cross product of</param>
    ''' <returns>return cross product a x b [vector]</returns>
    ''' <remarks></remarks>
    Function vctCross(ByRef a As vector, ByRef b As vector) As vector
        vctCross.X = (a.Y * b.Z) - (a.Z * b.Y)
        vctCross.Y = (a.Z * b.X) - (a.X * b.Z)
        vctCross.Z = (a.X * b.Y) - (a.Y * b.X)
    End Function

    ''' <summary>
    ''' calculate the norm of a vector
    ''' </summary>
    ''' <param name="vec">vector to calcutate norm of</param>
    ''' <returns>return norm of the vector</returns>
    ''' <remarks>
    ''' todo rename: vctNorme -> vctNorm
    ''' </remarks>
    Function vctNorme(ByRef vec As vector) As Double
        vctNorme = Sqrt(vec.X * vec.X + vec.Y * vec.Y + vec.Z * vec.Z)
    End Function

    ''' <summary>
    ''' calculate a scalar multiplication
    ''' </summary>
    ''' <param name="vec">vector to multiplicate</param>
    ''' <param name="k">scalar to multiplicate</param>
    ''' <returns>return scalar product k * vec [vector]</returns>
    ''' <remarks></remarks>
    Function vctMultScalar(ByRef vec As vector, ByRef k As Double) As vector
        vctMultScalar.X = vec.X * k
        vctMultScalar.Y = vec.Y * k
        vctMultScalar.Z = vec.Z * k
    End Function

    ''' <summary>
    ''' calculate the inverse of a vector
    ''' </summary>
    ''' <param name="vec">vector to inverse</param>
    ''' <returns>return inverse -vec [vector]</returns>
    ''' <remarks></remarks>
    Function vctInverse(ByRef vec As vector) As vector
        vctInverse = vctMultScalar(vec, -1)
    End Function

    ''' <summary>
    ''' calculate vector subtraction for two vectors
    ''' </summary>
    ''' <param name="a">minuend</param>
    ''' <param name="b">subtrahend</param>
    ''' <returns>return subtraction a-b [vector]</returns>
    ''' <remarks></remarks>
    Function vctSub(ByRef a As vector, ByRef b As vector) As vector
        vctSub.X = a.X - b.X
        vctSub.Y = a.Y - b.Y
        vctSub.Z = a.Z - b.Z
    End Function

    ''' <summary>
    ''' calculate vector addition for two vectors
    ''' </summary>
    ''' <param name="a">addends</param>
    ''' <param name="b">addends</param>
    ''' <returns>return sum a+b [vector]</returns>
    ''' <remarks></remarks>
    Function vctAdd(ByRef a As vector, ByRef b As vector) As vector
        vctAdd.X = a.X + b.X
        vctAdd.Y = a.Y + b.Y
        vctAdd.Z = a.Z + b.Z
    End Function

    ''' <summary>
    ''' calculate dot product of two vectors
    ''' </summary>
    ''' <param name="a">factors</param>
    ''' <param name="b">factors</param>
    ''' <returns>return dot product a o b</returns>
    ''' <remarks></remarks>
    Function vctDot(ByRef a As vector, ByRef b As vector) As Double
        vctDot = a.X * b.X + a.Y * b.Y + a.Z * b.Z
    End Function

    ''' <summary>
    ''' calcutate normalized vector for input vector
    ''' </summary>
    ''' <param name="vec">vector to be normalized</param>
    ''' <returns>return normalized vector vec/|vec| [vector]</returns>
    ''' <remarks></remarks>
    Function vctToNorm(ByRef vec As vector) As vector
        Static norme As Double
        norme = vctNorme(vec)
        vctToNorm.X = vec.X / norme
        vctToNorm.Y = vec.Y / norme
        vctToNorm.Z = vec.Z / norme
    End Function

    ''' <summary>
    ''' calcutate angle between two vectors
    ''' </summary>
    ''' <param name="vec1">vector which span the angle</param>
    ''' <param name="vec2">vector which span the angle</param>
    ''' <returns>return angle between vec1 and vec2</returns>
    ''' <remarks>
    ''' todo check whether this function returns [rad] or [deg]
    ''' </remarks>
    Function vctAngle(ByRef vec1 As vector, ByRef vec2 As vector) As Double
        vctAngle = Math.Acos(vctDot(vec1, vec2) / (vctNorme(vec1) * vctNorme(vec2)))
    End Function

    ''' <summary>
    ''' calculate arcus sine
    ''' </summary>
    ''' <param name="X">value of which arcus sine should be calculated</param>
    ''' <returns>return arcus sine ( x )</returns>
    ''' <remarks>
    ''' todo check whether this function returns and takes [rad] or [deg]
    ''' </remarks>
    Public Function ArcSin(ByRef X As Double) As Double
        If X < -1 Then X = -1
        If Abs(X - 1) < 0.00001 Then ArcSin = pi / 2 : Exit Function
        If X = -1 Then ArcSin = -pi / 2 : Exit Function
        ArcSin = Atan(X / Sqrt(-X * X + 1))
    End Function


    ''' <summary>
    ''' This function returns the places after the decimal point. E.g. Frac(4.123) == 0.123
    ''' </summary>
    ''' <param name="X">value of which arcus sine should be calculated</param>
    ''' <returns>return decimal places of x</returns>
    ''' <remarks></remarks>
    Function Frac(ByRef X As Double) As Double
        Frac = X - Int(X)
    End Function

    ''' <summary>
    ''' calcule l'arctangente pour un angle compris entre -pi et pi
    ''' </summary>
    ''' <param name="sinx"></param>
    ''' <param name="cosx"></param>
    ''' <returns></returns>
    ''' <remarks>
    ''' todo what exactly does this function do?
    ''' </remarks>
    Function Atan2(ByRef sinx As Double, ByRef cosx As Double) As Double
        If cosx <> 0 Then
            If cosx >= 0 Then
                Atan2 = Atan(sinx / cosx)
            Else
                If sinx >= 0 Then
                    Atan2 = Atan(sinx / cosx) + pi
                Else
                    Atan2 = Atan(sinx / cosx) - pi
                End If
            End If
        Else
            If sinx > 0 Then
                Atan2 = 0.5 * pi
            Else
                Atan2 = -0.5 * pi
            End If
        End If
    End Function

    Public Function Actan(ByVal sinx As Double, ByVal cosx As Double) As Double
        '
        ' Arctangente entre 0 et 2 pi
        If cosx = 0 Then
            If sinx > 0 Then
                actan = pi / 2
            Else
                actan = 3 * pi / 2
            End If
        ElseIf cosx > 0 Then
            Actan = Math.Atan(sinx / cosx)
        Else
            Actan = pi + Math.Atan(sinx / cosx)
        End If
        ''
    End Function

    ''' <summary>
    ''' calculate third power
    ''' </summary>
    ''' <param name="X">value which should be squared</param>
    ''' <returns>return x^3</returns>
    ''' <remarks>
    ''' todo use Pow(x,3) instead of this here!!
    ''' </remarks>
    Function Cube(ByRef X As Double) As Double
        Cube = X * X * X
    End Function

    ''' <summary>
    ''' calculate modulo of two doubles
    ''' </summary>
    ''' <param name="X">value to be taken into modulus</param>
    ''' <param name="Y">value to be taken into modulus</param>
    ''' <returns>return x mod y</returns>
    ''' <remarks></remarks>

    Function Modulo(ByRef X As Double, ByRef Y As Double) As Double
        Modulo = X - Int(X / Y) * Y
        If Modulo < 0 Then
            Modulo = Modulo + Y
        End If
    End Function

    ''' <summary>
    ''' calculate x modulo (2 PI), x being a double
    ''' </summary>
    ''' <param name="X">values to be taken into modulus</param>
    ''' <returns>return x mod 2PI</returns>
    ''' <remarks></remarks>
    Function Mod2pi(ByRef X As Double) As Double
        Mod2pi = Modulo(X, 2 * pi)
    End Function

    ''' <summary>
    ''' calculate progress of x in dependency of frame y
    '''
    ''' This function calculates the current progress of x (= current state)
    ''' depending on y (= whole e.g. time-frame) and returns this value in percent. \n
    ''' Example: progress(5,10) = 50; progress(1,20) = 5
    ''' </summary>
    ''' <param name="X">current state</param>
    ''' <param name="Y">whole frame</param>
    ''' <returns>return An integer determining the progress x/y in %</returns>
    ''' <remarks>
    ''' todo function is used with doubles as well as with ints. please stick to one type (double). => OK
    ''' </remarks>
    Public Function progress(ByRef X As Double, ByRef Y As Double) As Short
        If Y < 0.00000001 Then
            progress = 100
        Else
            progress = Int((X / Y * 100) + 0.5)
            If progress > 100 Then progress = 100
            If progress < 0 Then progress = 0
        End If
    End Function
    ''' <summary>
    ''' Check whether a variable is inside of well defined limits
    ''' </summary>
    '''     ''' <param name="X">variable to be checked</param>
    ''' <param name="min">lower bound</param>
    ''' <param name="max">upper bound</param>
    ''' <returns>True if x is ok, False if x is out of bounds</returns>
    ''' <remarks>note : When the variable is out of limits, a message box will be raised.</remarks>
    Public Function WithinLimits(ByRef X As Double, ByRef min As Double, ByRef max As Double) As Boolean

        Dim testString As String
        '
        If X >= min And X <= max Then
            WithinLimits = True
        Else
            If X < min Then
                testString = "Value " & X & " is too small"
            Else
                testString = "Value " & X & " is too big"
            End If
            testString = testString & ", the valid range is from " & min & " to " & max & "."
            MsgBox(testString, MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical, "Error!")
            WithinLimits = False
        End If
        ''
    End Function

End Module