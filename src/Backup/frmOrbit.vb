Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports System.Math
Imports System.Net
Imports System.Text

Friend Class frmOrbit
    Inherits System.Windows.Forms.Form

    Private intIDData As Short
    Private intFwdTrackSize, intBckTrackSize As Short
    Private period As Double
    Dim fin_transfert As Boolean
    Dim data_norad As String
    Private RAAN, i, a, e, w, Mu As Double
    Private epoch As Double
    Dim listFiles() As String
    Dim listSat() As String
    Dim indexChange As Boolean = True
    ' Create a new WebClient instance.
    Dim webClient1 As New WebClient()
    Dim url As String
    Dim databuffer As Byte()

    Dim strSatName, strDefaultSatName As String

    ' new or old satellite
    Private tempSat As CSatellite
    '
    ' Attitude
    ' --------
    ' all available axis
    Dim axis(5) As String
    ' all available orientations
    Dim orientation(7) As String
    ' axis of the main direction
    Dim prinAxis As String
    ' orientation of the main direction
    Dim prinOrient As String
    ' axis of the optimized direction
    Dim optAxis As String
    ' orientation of the optimized direction
    Dim optOrient As String

    ' Onglet environnement
    ' --------------------
    Dim dsun(11) As Double
    Dim dmax As Double
    Dim dmin As Double
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="tracksize"></param>
    ''' <param name="a"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function TrackName(ByRef tracksize As Short, ByRef a As Double) As String
        'experimental
        TrackName = ""
        Select Case tracksize
            Case 0
                TrackName = "None"
            Case 12
                TrackName = "1/4 Orbit"
            Case 25
                TrackName = "1/2 Orbit"
            Case 50
                TrackName = "1 Orbit"
        End Select
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub cmbTime_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTime.SelectedIndexChanged
        If allow_event Then
            Dim CalendarEpoch As udtDate
            Select Case cmbTime.Text
                Case "Calendar"
                    txtEpochDate.Visible = True
                    txtEpochTime.Visible = True
                    txtNoradEpoch.Visible = False
                    Call Convert_Julian_Gregorian(SatOrbit0.epoch, CalendarEpoch)
                    With CalendarEpoch
                        txtEpochDate.Text = Format(.day, "00") & "-" & Format(.month, "00") & "-" & Format(.year, "0000")
                        txtEpochTime.Text = Format(.hour, "00") & ":" & Format(.minute, "00") & ":" & Format(.second, "00")
                    End With
                Case "NORAD epoch"
                    txtEpochDate.Visible = False
                    txtEpochTime.Visible = False
                    txtNoradEpoch.Visible = True
                    Call convert_Julian_Gregorian(SatOrbit0.epoch, CalendarEpoch)
                    With CalendarEpoch
                        .day = "01"
                        .month = "01"
                        .hour = "00"
                        .minute = "00"
                        .second = "00"
                    End With
                    txtNoradEpoch.Text = VB.Right(CStr(CalendarEpoch.year), 2) & Format(SatOrbit0.epoch - convert_Gregorian_Julian(CalendarEpoch) + 1, "000.00000000")
            End Select
        End If
    End Sub

   

    ''' <summary>
    ''' Triggerd by the cmdOk button. It saves or changes the satellite dependend whether it is new or not.
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOk.Click
        '
        Me.Close()
        System.Windows.Forms.Application.DoEvents()
        '
    End Sub



    ''' <summary>
    ''' Update of the data and searching for the chosen satellite and its first orbit0
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub frmOrbit_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        '
        '
        Dim CalendarEpoch As udtDate
        Dim X As String
        ReDim listFiles(40)
        ReDim listSat(40)
        Dim i As Short

        'disable events
        allow_event = False
        '
        ' Setting orbit0 data
        ' ------------------
        ' filling of the combo norad type satellite
        Dim tempfile As Integer
        tempfile = FreeFile()

        FileOpen(tempfile, My.Application.Info.DirectoryPath & "\Data\Norad\lst_norad.txt", OpenMode.Input)
        i = 0
        While Not EOF(tempfile)
            X = LineInput(tempfile)
            If X.Substring(0, 1) = " " Then
                cmbType_Sat.Items.Add(RTrim(X.Substring(0, 49)))
                listFiles(i) = RTrim(X.Substring(49))
                listSat(i) = RTrim(X.Substring(0, 49))
                i = i + 1
            Else
                cmbType_Sat.Items.Add(RTrim(X.Substring(0)))
            End If
        End While
        FileClose(tempfile)


        Call Update_OrbitWindow()

        ' Modify satellite
        txtName.Text = SatOrbit0.Name
        ColorBox.Color = System.Drawing.ColorTranslator.FromOle(Satellite.satColor)
        previewColor.BackColor = ColorBox.Color
        cmbFwdTrack.Text = TrackName((Satellite.satFTrackSize), SatOrbit0.SemiMajor_Axis)
        cmbBckTrack.Text = TrackName((Satellite.satBTrackSize), SatOrbit0.SemiMajor_Axis)
        If Satellite.satFootprint Then chkFootprint.CheckState = System.Windows.Forms.CheckState.Checked Else chkFootprint.CheckState = System.Windows.Forms.CheckState.Unchecked

        period = 2 * pi * (SatOrbit0.SemiMajor_Axis ^ 3 / myEarth) ^ 0.5
        period = period / 60 'in minutes

        cmbTime.SelectedIndex = 0
        txtEpochDate.Visible = True
        txtEpochTime.Visible = True
        txtNoradEpoch.Visible = False

        Call convert_Julian_Gregorian(SatOrbit0.epoch, CalendarEpoch)

        With CalendarEpoch
            txtEpochDate.Text = Format(.day, "00") & "-" & Format(.month, "00") & "-" & Format(.year, "0000")
            txtEpochTime.Text = Format(.hour, "00") & ":" & Format(.minute, "00") & ":" & Format(.second, "00")
        End With
        With CalendarEpoch
            .day = "01"
            .month = "01"
            .hour = "00"
            .minute = "00"
            .second = "00"
        End With
        txtNoradEpoch.Text = VB.Right(CStr(CalendarEpoch.year), 2) & Format(SatOrbit0.epoch - convert_Gregorian_Julian(CalendarEpoch) + 1, "000.00000000")

        txtA.Text = CStr(SatOrbit0.SemiMajor_Axis)
        txtE.Text = CStr(SatOrbit0.Eccentricity)
        txtI.Text = CStr(SatOrbit0.Inclination)
        txtW.Text = CStr(SatOrbit0.Argument_Perigee)
        txtRAAN.Text = CStr(SatOrbit0.RAAN)
        txtMu.Text = CStr(SatOrbit0.Mean_Anomaly)
        txtBstar.Text = CStr(SatOrbit0.bstar)


        cmbSemiMajor.Visible = True
        cmbSemiMajor.SelectedIndex = 0
        cmbEccentricity.Visible = True
        cmbEccentricity.SelectedIndex = 0
        cmbRAAN.Visible = True
        cmbRAAN.SelectedIndex = 0 ' "RAAN"
        cmbAnomaly.Visible = True
        cmbAnomaly.SelectedIndex = 0 ' "Mean Anomaly"
        cmbType_Sat.SelectedIndex = 0

        ' -------------------------
        ' Setting data for attitude
        ' -------------------------
        '
        ' providing the available axes
        axis(0) = "x"
        axis(1) = "y"
        axis(2) = "z"
        axis(3) = "-x"
        axis(4) = "-y"
        axis(5) = "-z"
        ' providing the available orientations
        orientation(0) = "nadir"
        orientation(1) = "sun-pointing"
        orientation(2) = "moon-pointing"
        orientation(3) = "velocity"
        orientation(4) = "inertial x"
        orientation(5) = "inertial y"
        orientation(6) = "inertial z"
        orientation(7) = "ground speed"
        '
        ' allowing everything for the axis of the principle and optimized direction
        For i = 0 To UBound(axis)
            cmbPrinAxis.Items.Add(axis(i))
            cmbOptAxis.Items.Add(axis(i))
        Next i
        ' allowing everything for the orientation of the principle and optimized direction
        For i = 0 To UBound(orientation)
            cmbPrinOrient.Items.Add(orientation(i))
            cmbOptOrient.Items.Add(orientation(i))
        Next i

        cmbPrinAxis.SelectedIndex = 5
        cmbPrinOrient.SelectedIndex = 0
        cmbOptAxis.SelectedIndex = 0
        cmbOptOrient.SelectedIndex = 3

        ' setting the data from the satellite (if already setted)
        If Satellite.primaryAxis <> "" Then
            prinAxis = Satellite.primaryAxis
            Select Case prinAxis
                Case "x" : cmbPrinAxis.SelectedIndex = 0
                Case "y" : cmbPrinAxis.SelectedIndex = 1
                Case "z" : cmbPrinAxis.SelectedIndex = 2
                Case "-x" : cmbPrinAxis.SelectedIndex = 3
                Case "-y" : cmbPrinAxis.SelectedIndex = 4
                Case "-z" : cmbPrinAxis.SelectedIndex = 5
            End Select
        End If
        If Satellite.primaryDir <> "" Then
            prinOrient = Satellite.primaryDir
            Select Case prinOrient
                Case "nadir" : cmbPrinOrient.SelectedIndex = 0
                Case "sun-pointing" : cmbPrinOrient.SelectedIndex = 1
                Case "moon-pointing" : cmbPrinOrient.SelectedIndex = 2
                Case "velocity" : cmbPrinOrient.SelectedIndex = 3
                Case "inertial x" : cmbPrinOrient.SelectedIndex = 4
                Case "inertial y" : cmbPrinOrient.SelectedIndex = 5
                Case "inertial z" : cmbPrinOrient.SelectedIndex = 6
                Case "ground speed" : cmbPrinOrient.SelectedIndex = 7
            End Select
        End If
        If Satellite.secondaryAxis <> "" Then
            optAxis = Satellite.secondaryAxis
            Select Case optAxis
                Case "x" : cmbOptAxis.SelectedIndex = 0
                Case "y" : cmbOptAxis.SelectedIndex = 1
                Case "z" : cmbOptAxis.SelectedIndex = 2
                Case "-x" : cmbOptAxis.SelectedIndex = 3
                Case "-y" : cmbOptAxis.SelectedIndex = 4
                Case "-z" : cmbOptAxis.SelectedIndex = 5
            End Select
        End If
        '
        If Satellite.secondaryDir <> "" Then
            optOrient = Satellite.secondaryDir
            Select Case optOrient
                Case "nadir" : cmbOptOrient.SelectedIndex = 0
                Case "sun-pointing" : cmbOptOrient.SelectedIndex = 1
                Case "moon-pointing" : cmbOptOrient.SelectedIndex = 2
                Case "velocity" : cmbOptOrient.SelectedIndex = 3
                Case "inertial x" : cmbOptOrient.SelectedIndex = 4
                Case "inertial y" : cmbOptOrient.SelectedIndex = 5
                Case "inertial z" : cmbOptOrient.SelectedIndex = 6
                Case "ground speed" : cmbOptOrient.SelectedIndex = 7
            End Select
        End If
        '
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        '
        '
        ' --------------------
        ' Onglet environnement
        ' --------------------
        '
        ' Gives values to 12 monthly average values of the Sun-Earth distance
        dsun(0) = 147136383.585747
        dsun(1) = 147734283.137167
        dsun(2) = 148774603.47896
        dsun(3) = 150094009.236035
        dsun(4) = 151234136.704803
        dsun(5) = 151964678.668876
        dsun(6) = 152063368.430357
        dsun(7) = 151511718.826698
        dsun(8) = 150436237.556895
        dsun(9) = 149163195.894725
        dsun(10) = 147961188.654898
        dsun(11) = 147231119.143355
        dmax = 152101686.718472
        dmin = 147090267.044645
        '
        Call env_update_texts()
        Call trace_environment()
        '
        'enable events
        allow_event = True
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Update_OrbitWindow()
        Dim tleNorad As String

        If optMain_0.Checked Then
            fraType_Orbit.Visible = True
            fraDatabase.Visible = False
            optType_Orbit_7.Checked = True
        Else
            fraType_Orbit.Visible = False
            fraDatabase.Visible = True
        End If
        txtName.Text = SatOrbit0.Name
        '
        If optMain_0.Checked Then
            If optType_Orbit_1.Checked Then
                SatOrbit0.Inclination = 63.4
                txtI.ReadOnly = True
                lblI.Enabled = False
                SatOrbit0.Argument_Perigee = 270.0#
                txtW.ReadOnly = True
                lblW.Enabled = False
                SatOrbit0.epoch = 18500
                txtA.Text = CStr(500)
                txtE.Text = CStr(0.74096938)
                txtRAAN.Text = CStr(100)
                txtMu.Text = CStr(0)
            End If
            If optType_Orbit_7.Checked Then
                lblI.Enabled = True
                lblW.Enabled = True
                cmbSemiMajor.Visible = True
                cmbSemiMajor.SelectedIndex = 0
                cmbEccentricity.Visible = True
                cmbEccentricity.SelectedIndex = 0
                cmbRAAN.Visible = True
                cmbRAAN.SelectedIndex = 0 ' "RAAN"
                cmbAnomaly.Visible = True
                cmbAnomaly.SelectedIndex = 0 ' "Mean Anomaly"
            End If
        ElseIf optMain_1.Checked Then
            strSatName = lstSatellites.SelectedIndex.ToString
            tleNorad = Mid(data_norad, InStr(data_norad, strSatName), 166)
            Call norad_to_Orbit(tleNorad, SatOrbit0)
            txtName.Text = SatOrbit0.Name
        End If
        '
        Dim CalendarEpoch As udtDate
        Call convert_Julian_Gregorian(SatOrbit0.epoch, CalendarEpoch)
        With CalendarEpoch
            txtEpochDate.Text = Format(.day, "00") & "-" & Format(.month, "00") & "-" & Format(.year, "0000")
            txtEpochTime.Text = Format(.hour, "00") & ":" & Format(.minute, "00") & ":" & Format(.second, "00")
        End With
        txtNoradEpoch.Text = VB.Right(CStr(CalendarEpoch.year), 2) & Format(SatOrbit0.epoch - convert_Gregorian_Julian(CalendarEpoch) + 1, "000.00000000")
        '
        With SatOrbit0
            txtA.Text = Format(.SemiMajor_Axis, "#0.000")
            txtE.Text = Format(.Eccentricity, "#0.00000")
            txtI.Text = Format(.Inclination, "#0.000")
            txtRAAN.Text = Format(.RAAN, "#0.000")
            txtW.Text = Format(.Argument_Perigee, "#0.0000")
            txtMu.Text = Format(.Mean_Anomaly, "#0.000")
            txtBstar.Text = Format(.bstar, "#0.0000")
        End With
        '
        period = 2 * pi * (SatOrbit0.SemiMajor_Axis ^ 3 / myEarth) ^ 0.5
        period = period / 60 'in minutes
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub txtBstar_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBstar.Leave
        Dim X As Double
        '
        If IsNumeric(txtBstar.Text) Then
            X = CDbl(txtBstar.Text)
            If WithinLimits(X, 0.0#, 1.0#) = False Then
                txtBstar.Focus()
                Exit Sub
            End If
            txtBstar.Text = Format(X, "#0.00000")
            SatOrbit0.bstar = X
        Else
            MsgBox("Please enter a number !")
            txtBstar.Text = Format(SatOrbit0.bstar, "#0.00000")
            txtBstar.Focus()
        End If
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub txtEpochDate_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEpochDate.Leave
        Dim date_convertie As udtDate

        If Not IsDate(txtEpochDate.Text) Then txtEpochDate.Focus() : Exit Sub
        If Not IsDate(txtEpochTime.Text) Then txtEpochTime.Focus() : Exit Sub

        With date_convertie
            .day = Mid(CStr(DateValue(txtEpochDate.Text)), 1, 2)
            .month = Mid(CStr(DateValue(txtEpochDate.Text)), 4, 2)
            .year = Mid(CStr(DateValue(txtEpochDate.Text)), 7, 4)
            .hour = Mid(CStr(TimeValue(txtEpochTime.Text)), 1, 2)
            .minute = Mid(CStr(TimeValue(txtEpochTime.Text)), 4, 2)
            .second = Mid(CStr(TimeValue(txtEpochTime.Text)), 7, 2)
        End With
        SatOrbit0.epoch = convert_Gregorian_Julian(date_convertie)
        '
        ' Update simulation start date
        ' ----------------------------
        Dim date_temp As udtDate
        convert_Julian_Gregorian(SatOrbit0.epoch, date_temp)
        With date_temp
            frmMain.txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        dblSimulationStart = SatOrbit0.epoch
        dblSimulationTime = SatOrbit0.epoch
        update_Date(dblSimulationTime)
        '
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub txtEpochTime_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEpochTime.Leave
        Dim date_convertie As udtDate

        If Not IsDate(txtEpochDate.Text) Then txtEpochDate.Focus() : Exit Sub
        If Not IsDate(txtEpochTime.Text) Then txtEpochTime.Focus() : Exit Sub
        With date_convertie
            .day = Mid(CStr(DateValue(txtEpochDate.Text)), 1, 2)
            .month = Mid(CStr(DateValue(txtEpochDate.Text)), 4, 2)
            .year = Mid(CStr(DateValue(txtEpochDate.Text)), 7, 4)
            .hour = Mid(CStr(TimeValue(txtEpochTime.Text)), 1, 2)
            .minute = Mid(CStr(TimeValue(txtEpochTime.Text)), 4, 2)
            .second = Mid(CStr(TimeValue(txtEpochTime.Text)), 7, 2)
        End With
        SatOrbit0.epoch = convert_Gregorian_Julian(date_convertie)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub txtMu_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMu.Leave
        Dim Mu, X, ZZ As Double 'Y
        If IsNumeric(txtMu.Text) Then
            X = CDbl(txtMu.Text)
            Select Case cmbAnomaly.Text
                Case "Mean Anomaly"
                    If WithinLimits(X, -180.0#, 360.0#) = False Then
                        txtMu.Focus()
                        Exit Sub
                    End If
                    SatOrbit0.Mean_Anomaly = X
                    txtMu.Text = Format(X, "#0.0000")
                Case "True Anomaly"
                    If WithinLimits(X, -180.0#, 360.0#) = False Then
                        txtMu.Focus()
                        Exit Sub
                    End If
                    ZZ = trueAnom_to_eccentricAnom(X * rad, SatOrbit0.Eccentricity)
                    Mu = ZZ - SatOrbit0.Eccentricity * Sin(ZZ)
                    SatOrbit0.Mean_Anomaly = Mu * deg
                    txtMu.Text = Format(X, "#0.0000")
                Case "Eccentric Anomaly"
                    If WithinLimits(X, -180.0#, 360.0#) = False Then
                        txtMu.Focus()
                        Exit Sub
                    End If
                    Mu = X * rad - SatOrbit0.Eccentricity * Sin(X * rad)
                    SatOrbit0.Mean_Anomaly = Mu * deg
                    txtMu.Text = Format(X, "#0.0000")
            End Select
        Else
            MsgBox("Please enter a number !")
            txtMu.Text = Format(SatOrbit0.Mean_Anomaly, "#0.0000")
            txtMu.Focus()
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub cmbAnomaly_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbAnomaly.SelectedIndexChanged
        If allow_event Then
            Select Case cmbAnomaly.Text
                Case "Mean Anomaly"
                    txtMu.Text = Format(SatOrbit0.Mean_Anomaly, "#0.0000")
                Case "True Anomaly"
                    txtMu.Text = Format(get_trueAnom(SatOrbit0), "#0.0000")
                Case "Eccentric Anomaly"
                    txtMu.Text = Format(get_eccentricAnom(SatOrbit0), "#0.0000")
            End Select
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub cmbEccentricity_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEccentricity.SelectedIndexChanged
        If allow_event Then
            Select Case cmbEccentricity.Text
                Case "Perigee Radius"
                    If Not cmbSemiMajor.Text = "Apogee Radius" Then
                        cmbSemiMajor.SelectedIndex = 1
                        txtA.Text = Format(get_apogeeRadius(SatOrbit0), "#0.000")
                        lblAUnits.Text = "km"
                        txtE.Text = Format(get_perigeeRadius(SatOrbit0), "#0.000")
                        lblEUnits.Text = "km"
                    End If
                Case "Perigee Altitude"
                    If Not cmbSemiMajor.Text = "Apogee Altitude" Then
                        cmbSemiMajor.SelectedIndex = 2
                        txtA.Text = Format(get_apogeeAlt(SatOrbit0), "#0.000")
                        lblAUnits.Text = "km"
                        txtE.Text = Format(get_perigeeAlt(SatOrbit0), "#0.000")
                        lblEUnits.Text = "km"
                    End If
                Case Else
                    If ((cmbSemiMajor.Text = "Apogee Radius") Or (cmbSemiMajor.Text = "Apogee Altitude")) Then
                        cmbSemiMajor.SelectedIndex = 0
                        txtA.Text = Format(SatOrbit0.SemiMajor_Axis, "#0.000")
                        lblAUnits.Text = "km"
                        txtE.Text = Format(SatOrbit0.Eccentricity, "#0.0000000")
                        lblEUnits.Text = ""
                    End If
                    txtE.Text = Format(SatOrbit0.Eccentricity, "#0.0000000")
                    lblEUnits.Text = ""
            End Select
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub cmbRAAN_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbRAAN.SelectedIndexChanged
        If allow_event Then
            Select Case cmbRAAN.Text
                Case "RAAN"
                    txtRAAN.Text = Format(SatOrbit0.RAAN, "#0.0000")
                Case "Longitude of Asc. Node"
                    txtRAAN.Text = Format(SatOrbit0.RAAN - temps_sideral(SatOrbit0.epoch) * deg, "#0.0000")
            End Select
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub cmbSemiMajor_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbSemiMajor.SelectedIndexChanged
        If allow_event Then
            Select Case cmbSemiMajor.Text
                Case "Apogee Radius"
                    If Not cmbEccentricity.Text = "Perigee Radius" Then
                        cmbEccentricity.SelectedIndex = 1
                        txtA.Text = Format(get_apogeeRadius(SatOrbit0), "#0.000")
                        lblAUnits.Text = "km"
                        txtE.Text = Format(get_perigeeRadius(SatOrbit0), "#0.000")
                        lblEUnits.Text = "km"
                    End If
                Case "Apogee Altitude"
                    If Not cmbEccentricity.Text = "Perigee Altitude" Then
                        cmbEccentricity.SelectedIndex = 2
                        txtA.Text = Format(get_apogeeAlt(SatOrbit0), "#0.000")
                        lblAUnits.Text = "km"
                        txtE.Text = Format(get_perigeeAlt(SatOrbit0), "#0.000")
                        lblEUnits.Text = "km"
                    End If
                Case "Period"
                    If Not cmbEccentricity.Text = "Eccentricity" Then
                        cmbEccentricity.SelectedIndex = 0
                        txtE.Text = Format(SatOrbit0.Eccentricity, "#0.000000")
                        lblEUnits.Text = ""
                    End If
                    txtA.Text = Format(get_period(SatOrbit0), "#0.00")
                    lblAUnits.Text = "min"
                Case "Mean Motion"
                    If Not cmbEccentricity.Text = "Eccentricity" Then
                        cmbEccentricity.SelectedIndex = 0
                        txtE.Text = Format(SatOrbit0.Eccentricity, "#0.000000")
                        lblEUnits.Text = ""
                    End If
                    txtA.Text = Format(minday / get_period(SatOrbit0), "#0.00000000")
                    lblAUnits.Text = "rev/day"
                Case Else
                    If Not cmbEccentricity.Text = "Eccentricity" Then
                        cmbEccentricity.SelectedIndex = 0
                        txtE.Text = Format(SatOrbit0.Eccentricity, "#0.000000")
                        lblEUnits.Text = ""
                    End If
                    txtA.Text = Format(SatOrbit0.SemiMajor_Axis, "#0.000")
                    lblAUnits.Text = "km"
            End Select
        End If
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub txtA_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtA.Leave
        Dim X As Double
        If IsNumeric(txtA.Text) Then
            X = CDbl(txtA.Text)
            With cmbSemiMajor
                If .Text = "Semimajor Axis" Then
                    If WithinLimits(X, RE, 1.0E+69) = False Then
                        txtA.Focus()
                        Exit Sub
                    End If
                    SatOrbit0.SemiMajor_Axis = X
                ElseIf .Text = "Apogee Radius" Then
                    If WithinLimits(X, RE, 1.0E+69) = False Then
                        txtA.Focus()
                        Exit Sub
                    End If
                    SatOrbit0.SemiMajor_Axis = (X + CDbl(txtE.Text)) / 2
                    SatOrbit0.Eccentricity = (X - CDbl(txtE.Text)) / 2 / SatOrbit0.SemiMajor_Axis
                ElseIf .Text = "Apogee Altitude" Then
                    If WithinLimits(X, 0.0#, 1.0E+69) = False Then
                        txtA.Focus()
                        Exit Sub
                    End If
                    SatOrbit0.SemiMajor_Axis = (X + CDbl(txtE.Text) + 2 * RE) / 2
                    SatOrbit0.Eccentricity = (X - CDbl(txtE.Text)) / 2 / SatOrbit0.SemiMajor_Axis

                ElseIf .Text = "Period" Then
                    If WithinLimits(X, 85.0#, 1.0E+69) = False Then
                        txtA.Focus()
                        Exit Sub
                    End If
                    SatOrbit0.SemiMajor_Axis = (myEarth * ((X * 60) / (2 * pi)) ^ 2) ^ (1 / 3)
                ElseIf .Text = "Mean Motion" Then
                    If WithinLimits(X, 0.0#, 16.93) = False Then
                        txtA.Focus()
                        Exit Sub
                    End If
                    SatOrbit0.SemiMajor_Axis = (myEarth * ((86400 / X) / (2 * pi)) ^ 2) ^ (1 / 3)
                End If

                If .Text = "Mean Motion" Then
                    txtA.Text = Format(X, "#0.00000000")
                Else
                    txtA.Text = Format(X, "#0.000")
                End If
            End With
        Else
            MsgBox("Please enter a number !")
            txtA.Text = Format(SatOrbit0.SemiMajor_Axis, "#0.000")
            txtA.Focus()
        End If
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub txtE_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtE.Leave
        Dim X As Double
        If IsNumeric(txtE.Text) Then
            X = CDbl(txtE.Text)
            If X = 0 Then X = 0.0000001
            With cmbEccentricity
                Select Case .Text
                    Case "Eccentricity"
                        ' [ZB] changed upper limit, since a 1 makes "/ (1 - Orbit.Eccentricity)" crash, of course
                        If WithinLimits(X, 0.0#, 0.99999999) = False Then
                            txtE.Focus()
                            Exit Sub
                        End If
                        txtE.Text = Format(X, "#0.000000")
                        SatOrbit0.Eccentricity = X
                        If (SatOrbit0.SemiMajor_Axis * (1 - SatOrbit0.Eccentricity)) < RE Then
                            SatOrbit0.SemiMajor_Axis = (RE + 100) / (1 - SatOrbit0.Eccentricity)
                            txtA.Text = Format(SatOrbit0.SemiMajor_Axis, "#0.000")
                        End If
                    Case "Perigee Radius"
                        If WithinLimits(X, RE, 1.0E+69) = False Then
                            txtE.Focus()
                            Exit Sub
                        End If
                        SatOrbit0.SemiMajor_Axis = (X + CDbl(txtA.Text)) / 2
                        SatOrbit0.Eccentricity = (CDbl(txtA.Text) - X) / 2 / SatOrbit0.SemiMajor_Axis

                    Case "Perigee Altitude"
                        If WithinLimits(X, 0.0#, 1.0E+69) = False Then
                            txtE.Focus()
                            Exit Sub
                        End If
                        SatOrbit0.SemiMajor_Axis = (X + CDbl(txtA.Text) + 2 * RE) / 2
                        SatOrbit0.Eccentricity = (CDbl(txtA.Text) - X) / 2 / SatOrbit0.SemiMajor_Axis
                End Select
            End With
        Else
            MsgBox("Please enter a number !")
            txtE.Text = Format(SatOrbit0.Eccentricity, "#0.0000")
            txtE.Focus()
        End If
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub txtI_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtI.Leave
        Dim X As Double
        '
        If IsNumeric(txtI.Text) Then
            X = CDbl(txtI.Text)
            If WithinLimits(X, 0.0#, 180.0#) = False Then
                txtI.Focus()
                Exit Sub
            End If
            txtI.Text = Format(X, "#0.000")
            SatOrbit0.Inclination = X
        Else
            MsgBox("Please enter a number !")
            txtI.Text = Format(SatOrbit0.Inclination, "#0.0000")
            txtI.Focus()
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub txtNoradEpoch_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNoradEpoch.Leave
        '
        Dim X As String
        Dim an As Short
        X = Trim(txtNoradEpoch.Text)
        If IsNumeric(X) Then
            an = CShort(VB.Left(X, 2))
            If an > 50 Then
                an = 1900 + an
            Else
                an = 2000 + an
            End If
            '
            SatOrbit0.epoch = 365.25 * 4 * ((an - 1900) \ 4) - 18264
            If (an - 1900) Mod 4 <> 0 Then
                SatOrbit0.epoch = SatOrbit0.epoch + (365 * ((an - 1900) Mod 4)) + 1
            End If

            SatOrbit0.epoch = SatOrbit0.epoch + CDbl(VB.Right(X, Len(X) - 2)) + 2433282.5
        Else
            MsgBox("Please enter a number !")
            txtNoradEpoch.Text = Format(SatOrbit0.epoch, "#0.00000000")
            txtNoradEpoch.Focus()
        End If
        '
        ' Update simulation start date
        ' ----------------------------
        Dim date_temp As udtDate
        convert_Julian_Gregorian(SatOrbit0.epoch, date_temp)
        With date_temp
            frmMain.txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        dblSimulationStart = SatOrbit0.epoch
        dblSimulationTime = SatOrbit0.epoch
        update_Date(dblSimulationTime)
        '
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub txtSSAltitude_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSSAltitude.Leave
        Dim cosi, sem, inclin As Double

        If CDbl(txtSSAltitude.Text) > 5970 Then
            MsgBox("5970 km is the maximum possible altitude for a circular sun-synchronous orbit", MsgBoxStyle.OkOnly, "Entry error")
            txtSSAltitude.Focus()
            Exit Sub
        End If
        sem = RE + CDbl(txtSSAltitude.Text)
        cosi = -(sem ^ 3.5) * 0.000000000000004773624117289
        inclin = Math.Acos(cosi) * deg
        txtSSInclination.Text = Format(inclin, "#0.0000")
        SatOrbit0.SemiMajor_Axis = sem
        SatOrbit0.Inclination = inclin
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub txtSSInclination_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSSInclination.Leave
        Dim cosi, sem, inclin As Double

        inclin = CDbl(txtSSInclination.Text)
        If inclin < 96 Then
            MsgBox("96 deg is the mininum possible inclination for a circular sun-synchronous orbit", MsgBoxStyle.OkOnly, "Entry error")
            txtSSInclination.Focus()
            Exit Sub
        End If
        cosi = Cos(inclin * rad)
        sem = (-cosi * 209484445241100.0#) ^ (2 / 7)
        txtSSAltitude.Text = Format(sem - RE, "#0.000")
        SatOrbit0.SemiMajor_Axis = sem
        SatOrbit0.Inclination = inclin
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub txtW_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtW.Leave
        Dim X As Double
        '
        If IsNumeric(txtW.Text) Then
            X = CDbl(txtW.Text)
            If WithinLimits(X, -180.0#, 360.0#) = False Then
                txtW.Focus()
                Exit Sub
            End If
            txtW.Text = Format(X, "#0.0000")
            SatOrbit0.Argument_Perigee = X
        Else
            MsgBox("Please enter a number !")
            txtW.Text = Format(SatOrbit0.Argument_Perigee, "#0.0000")
            txtW.Focus()
        End If
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub txtRAAN_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtRAAN.Leave
        Dim X As Double
        If IsNumeric(txtRAAN.Text) Then
            X = CDbl(txtRAAN.Text)
            Select Case cmbRAAN.Text
                Case "RAAN"
                    If WithinLimits(X, -180.0#, 360.0#) = False Then
                        txtRAAN.Focus()
                        Exit Sub
                    End If
                    txtRAAN.Text = Format(X, "#0.0000")
                    SatOrbit0.RAAN = X
                Case "Longitude of Asc. Node"
                    If WithinLimits(X, -180.0#, 360.0#) = False Then
                        txtRAAN.Focus()
                        Exit Sub
                    End If
                    txtRAAN.Text = Format(X, "#0.0000")
                    SatOrbit0.RAAN = X + temps_sideral(SatOrbit0.epoch) * deg
            End Select
        Else
            MsgBox("Please enter a number !")
            txtRAAN.Text = Format(SatOrbit0.RAAN, "#0.0000")
            txtRAAN.Focus()
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cmbPrinAxis_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPrinAxis.SelectedIndexChanged
        If allow_event And indexChange Then
            prinAxis = cmbPrinAxis.Text
            ComboBoxAxisChanges(cmbPrinAxis, cmbOptAxis)
            indexChange = True
            Satellite.primaryAxis = prinAxis
        End If
    End Sub



    Private Sub cmbPrinOrient_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPrinOrient.SelectedIndexChanged
        If allow_event And indexChange Then
            ' The orientation of the primary direction has been changed
            prinOrient = cmbPrinOrient.Text
            ComboBoxOrientChanges(cmbPrinOrient, cmbOptOrient)
            indexChange = True
            Satellite.primaryDir = prinOrient
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub cmbOptAxis_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbOptAxis.SelectedIndexChanged
        If allow_event And indexChange Then
            ' The axis of the optimized direction has been changed
            optAxis = cmbOptAxis.Text
            ComboBoxAxisChanges(cmbOptAxis, cmbPrinAxis)
            indexChange = True
            Satellite.secondaryAxis = optAxis
        End If
    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub cmbOptOrient_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbOptOrient.SelectedIndexChanged
        If allow_event And indexChange Then
            '
            optOrient = cmbOptOrient.Text
            ' clearing the old value
            ComboBoxOrientChanges(cmbOptOrient, cmbPrinOrient)
            indexChange = True
            Satellite.secondaryDir = optOrient
        End If
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="cmbDepend"></param>
    ''' <param name="cmbChange"></param>
    ''' <remarks></remarks>
    Private Sub ComboBoxAxisChanges(ByRef cmbDepend As System.Windows.Forms.ComboBox, ByRef cmbChange As System.Windows.Forms.ComboBox)
        Dim i As Short
        Dim tmpaxis, index As Integer
        indexChange = False

        For i = 0 To UBound(axis)
            ' searching for the axis in the combobox
            If cmbDepend.Text = axis(i) Then
                If i >= 3 Then
                    tmpaxis = i - 3
                Else
                    tmpaxis = i
                End If
            End If
            If cmbChange.Text = axis(i) Then
                index = i
            End If
        Next i
        cmbChange.Items.Clear()
        For i = 0 To UBound(axis)
            If tmpaxis <> i And tmpaxis + 3 <> i Then
                cmbChange.Items.Add(axis(i))
            End If
        Next i
        If index <> tmpaxis And index <> tmpaxis + 3 Then
            cmbChange.Text = axis(index)
        Else
            cmbChange.SelectedIndex = 0
        End If
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="cmbDepend"></param>
    ''' <param name="cmbChange"></param>
    ''' <remarks></remarks>
    Private Sub ComboBoxOrientChanges(ByRef cmbDepend As System.Windows.Forms.ComboBox, ByRef cmbChange As System.Windows.Forms.ComboBox)
        Dim i As Short
        Dim tmporient, index As Integer
        indexChange = False

        For i = 0 To UBound(orientation)
            ' searching for the axis in the combobox
            If cmbDepend.Text = orientation(i) Then
                tmporient = i
            End If
            If cmbChange.Text = orientation(i) Then
                index = i
            End If
        Next i
        cmbChange.Items.Clear()
        For i = 0 To UBound(orientation)
            If tmporient <> i Then
                cmbChange.Items.Add(orientation(i))
            End If
        Next i
        If index <> tmporient Then
            cmbChange.Text = orientation(index)
        Else
            cmbChange.SelectedIndex = 0
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Private Sub ChangeColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChangeColor.Click
        If ColorBox.ShowDialog() = DialogResult.OK Then
            previewColor.BackColor = ColorBox.Color
        End If
        ' Set the satellite color
        Satellite.satColor = System.Drawing.ColorTranslator.ToOle(ColorBox.Color)
    End Sub



    Private Sub optMain_0_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optMain_0.CheckedChanged

        If optMain_0.Checked Then
            fraType_Orbit.Visible = True
            fraDatabase.Visible = False
            Call select_orbit_type()
        ElseIf optMain_1.Checked Then
            fraDatabase.Visible = True
            fraType_Orbit.Visible = False
            strSatName = strDefaultSatName
            txtA.Enabled = True
            txtE.Enabled = True
            txtI.Enabled = True
            txtRAAN.Enabled = True
            txtW.Enabled = True
            txtMu.Enabled = True
        End If
    End Sub



    Private Sub select_orbit_type()
        '
        'SatOrbit0.epoch = dblSimulationStart
        cmbTime.SelectedIndex = -1
        strSatName = strDefaultSatName
        txtA.Enabled = True
        txtE.Enabled = True
        txtI.Enabled = True
        txtRAAN.Enabled = True
        txtW.Enabled = True
        txtMu.Enabled = True

        If Not optType_Orbit_0.Checked Then
            lblSSAlt.Visible = False
            lblSSIncl.Visible = False
            txtSSAltitude.Visible = False
            txtSSInclination.Visible = False
            lblSSAlt.Visible = False
            lblSSIncl.Visible = False
        End If

        If optType_Orbit_0.Checked Then ' Geostationnary
            txtA.ReadOnly = False
            txtI.ReadOnly = False
            lblDescription.Text = "A satellite in geostationary orbit0 will remain fixed in the sky above the specified longitude."
            SatOrbit0.SemiMajor_Axis = 42164
            SatOrbit0.Eccentricity = 0
            SatOrbit0.Inclination = 0
            SatOrbit0.Argument_Perigee = 0
            SatOrbit0.RAAN = 0
            SatOrbit0.Mean_Anomaly = 40
            cmbSemiMajor.SelectedIndex = 0
            txtA.Enabled = False
            cmbEccentricity.SelectedIndex = 0
            txtE.Enabled = False
            txtI.Enabled = False
            txtW.Enabled = False
            cmbRAAN.SelectedIndex = 0 '"RAAN"
            txtRAAN.Enabled = False
            txtW.ReadOnly = True
        ElseIf optType_Orbit_1.Checked Then  ' Molniya
            txtA.Enabled = True
            txtE.Enabled = True
            txtI.Enabled = True
            txtRAAN.Enabled = True
            lblDescription.Text = "Molniya orbits are highly eccentric, meaning there is a large difference between the altitude at the apogee and the altitude at the perigee. They are also critically inclined: this keeps the perigee of the orbit in the southern hemisphere."
            SatOrbit0.SemiMajor_Axis = 26562
            SatOrbit0.Eccentricity = 0.74
            SatOrbit0.Inclination = 63.4
            SatOrbit0.RAAN = 0
            SatOrbit0.Argument_Perigee = 270
            SatOrbit0.Mean_Anomaly = 0
            txtA.ReadOnly = True
            txtI.ReadOnly = True
            txtW.ReadOnly = True
        ElseIf optType_Orbit_2.Checked Then  ' Sun synchronous
            txtA.Enabled = True
            txtE.Enabled = True
            txtI.Enabled = True
            txtRAAN.Enabled = True
            txtA.ReadOnly = False
            txtI.ReadOnly = False
            txtW.ReadOnly = False
            lblDescription.Text = "The effect of the oblateness of the Earth is used to cause the orbit plane to rotate at the same rate at which the Earth moves in orbit around the Sun. Thus, at the equator, the satellite passes overhead at the same local time for each revolution."
            txtSSAltitude.Text = CStr(800)
            lblSSAlt.Visible = True
            lblSSIncl.Visible = True
            txtSSAltitude.Visible = True
            txtSSInclination.Visible = True
            lblSSAlt.Visible = True
            lblSSIncl.Visible = True
            Call txtSSAltitude_Leave(txtSSAltitude, New System.EventArgs())
            SatOrbit0.Eccentricity = 0
            SatOrbit0.RAAN = 0
            SatOrbit0.Argument_Perigee = 0
            SatOrbit0.Mean_Anomaly = 0
        ElseIf optType_Orbit_3.Checked Then  ' Critically inclined
            txtA.Enabled = True
            txtE.Enabled = True
            txtI.Enabled = True
            txtRAAN.Enabled = True
            txtA.ReadOnly = False
            txtW.ReadOnly = False
            lblDescription.Text = "These orbits have a perigee that remains at a fixed latitude. The line of apsides does not change over time"
            SatOrbit0.SemiMajor_Axis = 7000
            SatOrbit0.Inclination = 63.4
            SatOrbit0.Eccentricity = 0
            SatOrbit0.RAAN = 0
            SatOrbit0.Argument_Perigee = 0
            SatOrbit0.Mean_Anomaly = 0
            txtI.ReadOnly = True
        ElseIf optType_Orbit_4.Checked Then  ' Cr. inclined + sun synchronous
            txtA.Enabled = True
            txtE.Enabled = True
            txtI.Enabled = True
            txtRAAN.Enabled = True
            txtA.ReadOnly = False
            txtI.ReadOnly = False
            txtW.ReadOnly = False
            lblDescription.Text = "This orbit combines the properties of both orbits. It has a retrograde inclination of 116.565"
            SatOrbit0.SemiMajor_Axis = 7000
            SatOrbit0.Inclination = 116.565
            SatOrbit0.Eccentricity = 0
            SatOrbit0.RAAN = 0
            SatOrbit0.Argument_Perigee = 0
            SatOrbit0.Mean_Anomaly = 0
        ElseIf optType_Orbit_5.Checked Then  ' Repeating ground trace
            txtA.Enabled = True
            txtE.Enabled = True
            txtI.Enabled = True
            txtRAAN.Enabled = True
            txtA.ReadOnly = False
            txtI.ReadOnly = False
            txtW.ReadOnly = False
            lblDescription.Text = "These orbits repeat the ground coverage cycle. They are useful when identical viewing conditions are desired at different times, to detect changes. The ground trace may be caused to repeat every day or to interweave from day to day before repeating."
        ElseIf optType_Orbit_6.Checked Then  ' Circular
            txtA.Enabled = True
            txtE.Enabled = True
            txtI.Enabled = True
            txtRAAN.Enabled = True
            txtA.ReadOnly = False
            txtI.ReadOnly = False
            txtW.ReadOnly = False
            lblDescription.Text = "Circular orbits have a constant radius"
            SatOrbit0.Eccentricity = 0
            SatOrbit0.SemiMajor_Axis = 7374
        ElseIf optType_Orbit_7.Checked Then  ' Other
            txtA.Enabled = True
            txtE.Enabled = True
            txtI.Enabled = True
            txtRAAN.Enabled = True
            txtA.ReadOnly = False
            txtI.ReadOnly = False
            txtW.ReadOnly = False
            lblDescription.Text = ""
            'cmbSemiMajor.SelectedIndex = 0
            'cmbEccentricity.SelectedIndex = 0
            'cmbRAAN.SelectedIndex = 0 '"RAAN"
            'cmbAnomaly.SelectedIndex = 0 ' "Mean anomaly"
        End If

        txtI.Text = Format(SatOrbit0.Inclination, "#0.0000")
        txtW.Text = Format(SatOrbit0.Argument_Perigee, "#0.0000")
        txtA.Text = Format(SatOrbit0.SemiMajor_Axis, "#0.000")
        txtE.Text = Format(SatOrbit0.Eccentricity, "#0.000")
        txtRAAN.Text = Format(SatOrbit0.RAAN, "#0.0000")
        txtMu.Text = Format(SatOrbit0.Mean_Anomaly, "#0.0000")
    End Sub

    Private Sub optType_Orbit_0_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optType_Orbit_0.CheckedChanged
        Call select_orbit_type()
    End Sub
    Private Sub optType_Orbit_1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_1.CheckedChanged
        Call select_orbit_type()
    End Sub

    Private Sub optType_Orbit_2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_2.CheckedChanged
        Call select_orbit_type()
    End Sub

    Private Sub _optType_Orbit_3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optType_Orbit_3.CheckedChanged
        Call select_orbit_type()
    End Sub
    Private Sub optType_Orbit_4_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_4.CheckedChanged
        Call select_orbit_type()
    End Sub

    Private Sub optType_Orbit_5_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_5.CheckedChanged
        Call select_orbit_type()
    End Sub

    Private Sub optType_Orbit_6_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_6.CheckedChanged
        Call select_orbit_type()
    End Sub

    Private Sub optType_Orbit_7_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType_Orbit_7.CheckedChanged
        Call select_orbit_type()
    End Sub

    Private Sub cmbType_Sat_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbType_Sat.SelectedIndexChanged
        If allow_event Then
            Dim strType_Sat As String = ""
            Dim Line As String
            Dim strSat_Name As String 'VB6.FixedLengthString(24)
            Dim k As Short
            Dim j As Integer

            If cmbType_Sat.Text.Substring(0, 1) <> " " Then
                cmbType_Sat.SelectedIndex = cmbType_Sat.SelectedIndex + 1 ' cmbType_Sat.list(cmbType_Sat.listindex + 1)
            End If
            k = 0
            While listFiles(k) <> Nothing
                If listSat(k) = cmbType_Sat.Text Then
                    strType_Sat = listFiles(k)
                    Exit While
                End If
                k += 1
            End While
            data_norad = ""
            FileOpen(1, My.Application.Info.DirectoryPath & "\Data\Norad\" & strType_Sat, OpenMode.Input)
            Do While Not EOF(1)
                Line = LineInput(1)
                If Line.Substring(0, 1) = Chr(34) Then Line = Mid(Line, 2)
                data_norad = data_norad & Line & vbCrLf
            Loop
            FileClose(1)

            lstSatellites.Items.Clear()
            j = 1
            Do While j <= Len(data_norad) - 166
                strSat_Name = Mid(data_norad, j, 24)
                lstSatellites.Items.Add(strSat_Name)
                j = j + 168
            Loop
            lstSatellites.SetSelected(0, True)
        End If
    End Sub

    Private Sub cmdUpdateNORAD_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdateNORAD.Click
        Dim strFile, strUrl As String
        Dim Index As Short

        strUrl = "http://www.celestrak.com/NORAD/elements/"
        ProgressBar1.Value = 0
        ProgressBar1.Maximum = UBound(listFiles)
        ProgressBar1.Visible = True
        etat_connex.Text = "Downloading..."
        For Index = 0 To UBound(listFiles)
            If listFiles(Index) <> Nothing Then
                strFile = listFiles(Index)
                url = strUrl & strFile 'Inet1.url = strUrl & strFile

                ' Download the datas from NORAD.
                databuffer = webClient1.DownloadData(url)
                ' Convert downloaded data into string
                data_norad = Encoding.ASCII.GetString(databuffer)

                fin_transfert = False
                FileOpen(1, My.Application.Info.DirectoryPath & "\Data\Norad\" & strFile, OpenMode.Output)
                WriteLine(1, data_norad)
                FileClose(1)
                ProgressBar1.Value = Index
            End If
        Next
        ProgressBar1.Visible = False
        etat_connex.Text = "Completed !"
        Call cmbType_Sat_SelectedIndexChanged1(cmbType_Sat, New System.EventArgs())
    End Sub

    Private Sub lstSatellites_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSatellites.SelectedIndexChanged
        If allow_event Then
            Dim tleNorad As String

            strSatName = lstSatellites.SelectedItem
            txtName.Text = SatOrbit0.Name
            tleNorad = Mid(data_norad, InStr(data_norad, strSatName), 166)
            txtNORAD.Text = tleNorad
            Call norad_to_Orbit(tleNorad, SatOrbit0)
            ' Epoch display
            Dim CalendarEpoch As udtDate
            Call convert_Julian_Gregorian(SatOrbit0.epoch, CalendarEpoch)
            With CalendarEpoch
                txtEpochDate.Text = Format(.day, "00") & "-" & Format(.month, "00") & "-" & Format(.year, "0000")
                txtEpochTime.Text = Format(.hour, "00") & ":" & Format(.minute, "00") & ":" & Format(.second, "00")
            End With
            txtNoradEpoch.Text = VB.Right(CStr(CalendarEpoch.year), 2) & Format(SatOrbit0.epoch - convert_Gregorian_Julian(CalendarEpoch) + 1, "000.00000000")
            cmbSemiMajor.SelectedIndex = 4
            txtA.Text = Format(minday / get_period(SatOrbit0), "#0.00000000")
            cmbEccentricity.SelectedIndex = 0
            txtE.Text = Format(SatOrbit0.Eccentricity, "#0.000000")
            cmbRAAN.SelectedIndex = 0 '"RAAN"
            txtRAAN.Text = Format(SatOrbit0.RAAN, "#0.0000")
            cmbAnomaly.SelectedIndex = 0 '"Mean Anomaly"
            txtMu.Text = Format(SatOrbit0.Mean_Anomaly, "#0.0000")
            txtI.Text = Format(SatOrbit0.Inclination, "#0.0000")
            txtW.Text = Format(SatOrbit0.Argument_Perigee, "#0.0000")
            txtBstar.Text = Format(SatOrbit0.bstar, "#0.000000")
        End If
    End Sub

    Private Sub txtNORAD_Leave1(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNORAD.Leave
        Call norad_to_Orbit(Trim(txtNORAD.Text), SatOrbit0)
        cmbSemiMajor.SelectedIndex = 4
        cmbEccentricity.SelectedIndex = 0
        cmbRAAN.SelectedIndex = 0 '"RAAN"
        cmbAnomaly.SelectedIndex = 0 '"Mean Anomaly"
        txtI.Text = Format(SatOrbit0.Inclination, "#0.0000")
        txtW.Text = Format(SatOrbit0.Argument_Perigee, "#0.0000")
        txtBstar.Text = Format(SatOrbit0.bstar, "#0.000000")
    End Sub

    Private Sub trace_environment()
        ' Dessins des graphes
        Dim tabXM1 As ArrayList
        Dim tabYM1 As ArrayList
        Dim tabXM2 As ArrayList
        Dim tabYM2 As ArrayList
        Dim tabXM3 As ArrayList
        Dim tabYM3 As ArrayList
        Dim tabXM4 As ArrayList
        Dim tabYM4 As ArrayList
        Dim bCustomLabel As Boolean
        '
        Dim i As Integer
        Dim X As Double, Y As Double
        '
        '    Drawing graph
        '    -------------
        tabXM1 = New ArrayList
        tabYM1 = New ArrayList
        tabXM2 = New ArrayList
        tabYM2 = New ArrayList
        tabXM3 = New ArrayList
        tabYM3 = New ArrayList
        tabXM4 = New ArrayList
        tabYM4 = New ArrayList
        '
        ' Trac� des courbes
        '
        For i = 0 To 11
            X = i + 0.5
            '
            Y = solar_flux(dsun(i))
            tabXM1.Add(X)
            tabYM1.Add(Y)
            '
            Y = Albedo_month(i)
            tabXM2.Add(X)
            tabYM2.Add(Y)
            '
            Y = IR_month(i)
            tabXM3.Add(X)
            tabYM3.Add(Y)
            '
        Next i
        '
        With env_graph
            '
            .Series.Clear()
            .Series.Add("Solar")
            .Series("Solar").Points.DataBindXY(tabXM1, tabYM1)
            .Series("Solar").ChartType = DataVisualization.Charting.SeriesChartType.Line
            .Series("Solar").ShadowOffset = 1
            .Series("Solar").BorderWidth = 2
            .Series("Solar").Color = Color.Yellow
            '
            .Series.Add("Albedo")
            .Series("Albedo").Points.DataBindXY(tabXM2, tabYM2)
            .Series("Albedo").ChartType = DataVisualization.Charting.SeriesChartType.Line
            .Series("Albedo").YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary
            .Series("Albedo").ShadowOffset = 1
            .Series("Albedo").BorderWidth = 2
            .Series("Albedo").Color = Color.Orange
            '
            .Series.Add("Earth IR")
            .Series("Earth IR").Points.DataBindXY(tabXM3, tabYM3)
            .Series("Earth IR").ChartType = DataVisualization.Charting.SeriesChartType.Line
            '.Series("Solar").YAxisType = "Primary"
            .Series("Earth IR").ShadowOffset = 1
            .Series("Earth IR").BorderWidth = 2
            .Series("Earth IR").Color = Color.Red
            '
            If bCustomLabel = False Then
                '
                .ChartAreas(0).AxisX.CustomLabels.Add(0, 1, "J")
                .ChartAreas(0).AxisX.CustomLabels.Add(1, 2, "F")
                .ChartAreas(0).AxisX.CustomLabels.Add(2, 3, "M")
                .ChartAreas(0).AxisX.CustomLabels.Add(3, 4, "A")
                .ChartAreas(0).AxisX.CustomLabels.Add(4, 5, "M")
                .ChartAreas(0).AxisX.CustomLabels.Add(5, 6, "J")
                .ChartAreas(0).AxisX.CustomLabels.Add(6, 7, "J")
                .ChartAreas(0).AxisX.CustomLabels.Add(7, 8, "A")
                .ChartAreas(0).AxisX.CustomLabels.Add(8, 9, "S")
                .ChartAreas(0).AxisX.CustomLabels.Add(9, 10, "O")
                .ChartAreas(0).AxisX.CustomLabels.Add(10, 11, "N")
                .ChartAreas(0).AxisX.CustomLabels.Add(11, 12, "D")
                bCustomLabel = True
                '
            End If
            '
        End With
        ''
    End Sub
    Private Sub env_update_texts()
        '
        ' Updates text widgets with global variables
        env_list_months_alb.SelectedIndex = 0
        env_list_months_ir.SelectedIndex = 0
        env_txt_flux_0.Text = Format(solar_flux(dmin), "0.00")
        env_txt_flux_1.Text = Format(solar_flux(dmax), "0.00")
        env_txt_albedo.Text = Albedo_month(0)
        env_txt_ir.Text = IR_month(0)
        ''
    End Sub

    Private Sub env_cmd_isoterre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles env_cmd_isoterre.Click
        '
        ' This command button gives values to the monthly average values of intensity using
        ' the isothermal-sphere model for the Earth
        '
        Dim i As Integer
        '
        For i = 0 To 11
            IR_month(i) = Format(solar_flux(dsun(i)) * (1 - Albedo_month(i)) / 4, "0.00")
        Next i
        env_txt_ir.Text = IR_month(env_list_months_ir.SelectedIndex)
        Call trace_environment()
        ''
    End Sub

    Private Sub env_cmd_sin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles env_cmd_sin.Click
        '
        ' Gives values to the monthly average values of albedo so as to trace a trigonometric
        ' law.
        '
        Dim i As Integer
        Dim i0 As Integer
        Dim mean As Single
        Dim var As Single
        Dim X As Single

        i0 = env_list_months_alb.SelectedIndex
        mean = env_txt_albedo.Text
        X = checktypeSng(env_txt_var, env_txt_var.Text, 0)
        If mean > 0.5 Then
            var = checkup(env_txt_var, Abs(X), 1 - mean, True, 0)
        Else
            var = checkup(env_txt_var, Abs(X), mean, True, 0)
        End If
        env_txt_var.Text = var
        For i = 0 To 11
            Albedo_month(i) = Format(mean + var * Sin(30 * rad * (i - i0)), "0.00")
        Next i
        Call trace_environment()

    End Sub

    Private Sub env_list_months_alb_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles env_list_months_alb.Click
        '
        env_txt_albedo.Text = Albedo_month(env_list_months_alb.SelectedIndex)
        ''
    End Sub



    Private Sub env_txt_albedo_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles env_txt_albedo.Leave
        '
        Dim i As Integer
        Dim X As Single
        '
        i = env_list_months_alb.SelectedIndex
        X = checktypeSng(env_txt_albedo, env_txt_albedo.Text, Albedo_month(i))
        Albedo_month(i) = checkboth(env_txt_albedo, X, 1, True, 0, True, Albedo_month(i))
        env_txt_albedo.Text = Albedo_month(i)
        '*** env_graph.Column = 3
        '*** env_graph.Row = i + 1
        '*** env_graph.Data = albedo_month(i)
        ''
    End Sub

    Private Sub env_txt_flux_0_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles env_txt_flux_0.Leave
        '
        Dim X As Single
        Dim oldvalue(1) As Single
        '
        oldvalue(0) = Format(solar_flux(dmax), "0.00")
        oldvalue(1) = Format(solar_flux(dmin), "0.00")
        X = checktypeSng(env_txt_flux_0, env_txt_flux_0.Text, oldvalue(0))
        X = checkdown(env_txt_flux_0, X, 0, False, oldvalue(0))
        SunPow = X * 4 * pi * dmin * dmin ' * 1000000.0#
        env_txt_flux_1.Text = Format(solar_flux(dmax), "0.00")

        ' SunPow / 4 / pi / d / d / 1000000.0#


        env_txt_flux_0.Text = Format(X, "0.00")
        Call trace_environment()
        ''
    End Sub

    Private Sub env_txt_flux_1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles env_txt_flux_1.Leave
        '
        Dim X As Single
        Dim oldvalue(1) As Single
        '
        oldvalue(0) = Format(solar_flux(dmax), "0.00")
        oldvalue(1) = Format(solar_flux(dmin), "0.00")
        X = checktypeSng(env_txt_flux_1, env_txt_flux_1.Text, oldvalue(1))
        X = checkdown(env_txt_flux_1, X, 0, False, oldvalue(1))
        SunPow = X * 4 * pi * dmax * dmax * 1000000.0#
        env_txt_flux_0.Text = Format(solar_flux(dmin), "0.00")
        env_txt_flux_1.Text = Format(X, "0.00")
        Call trace_environment()
        ''
    End Sub

    Private Sub env_txt_ir_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles env_txt_ir.Leave
        '
        Dim i As Integer
        Dim X As Single
        '
        i = env_list_months_ir.SelectedIndex
        X = checktypeSng(env_txt_ir, env_txt_ir.Text, IR_month(i))
        IR_month(i) = checkdown(env_txt_ir, X, 0, True, IR_month(i))
        env_txt_ir.Text = IR_month(i)
        Call trace_environment()
        ''
    End Sub


    Private Sub env_list_months_ir_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles env_list_months_ir.SelectedIndexChanged
        env_txt_ir.Text = IR_month(env_list_months_ir.SelectedIndex)
    End Sub

    Private Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        '
        Dim file As String
        Dim xs As String
        '
        On Error GoTo errorsave
        If dlgOrbitSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
        file = dlgOrbitSave.FileName
        If My.Computer.FileSystem.FileExists(file) Then Kill(file)
        On Error GoTo 0
        '
        FileOpen(1, file, OpenMode.Output)
        '
        With SatOrbit0
            PrintLine(1, "! ----------")
            PrintLine(1, "! Orbit Data")
            PrintLine(1, "! ----------")
            PrintLine(1, "!")
            PrintLine(1, .Name, TAB(24), "Orbit name")
            PrintLine(1, .epoch, TAB(24), "Epoch")
            PrintLine(1, .SemiMajor_Axis, TAB(24), "SemiMajor axis")
            PrintLine(1, .Eccentricity, TAB(24), "Eccentricity")
            PrintLine(1, .Inclination, TAB(24), "Inclination")
            PrintLine(1, .RAAN, TAB(24), "Right ascension of ascending node")
            PrintLine(1, .Argument_Perigee, TAB(24), "Perigee argument")
            PrintLine(1, .Mean_Anomaly, TAB(24), "Mean anomaly")
            PrintLine(1, .bstar, TAB(24), "Bstar drag term")
            PrintLine(1, .dec1, TAB(24), "First Time Derivative of Mean Motion")
            PrintLine(1, .dec2, TAB(24), "Second Time Derivative of Mean Motion")
            PrintLine(1, "!")
        End With
        '
        With Satellite
            PrintLine(1, "! -------------")
            PrintLine(1, "! Attitude data")
            PrintLine(1, "! -------------")
            PrintLine(1, "!")
            PrintLine(1, .primaryAxis, TAB(24), "Primary axis")
            PrintLine(1, .primaryDir, TAB(24), "Primary direction")
            PrintLine(1, .secondaryAxis, TAB(24), "Secondary axis axis")
            PrintLine(1, .secondaryDir, TAB(24), "Secondary direction")
        End With
        FileClose(1)
        '
        Exit Sub
        '
errorsave:
        xs = MsgBox("Writing error" & Chr(13) _
        & " Saving data is not possible !!", vbCritical)
        ''
    End Sub


    Private Sub txtName_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtName.Leave
        SatOrbit0.Name = Trim(txtName.Text)
    End Sub

    Private Sub cmdOpen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOpen.Click
        '
        Dim file As String
        Dim s$ = ""
        '
        On Error Resume Next
        If dlgOrbitOpen.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
        file = dlgOrbitOpen.FileName
        FileOpen(1, file, OpenMode.Input)
        '
        On Error GoTo 0
        '
        s$ = (read_line())                      ' "! ----------"
        s$ = (read_line())                      ' "! Orbit Data"
        s$ = (read_line())                      ' "! ----------"
        s$ = (read_line())                      ' "!"
        With SatOrbit0
            .Name = (read_line())
            .epoch = (read_line())
            .SemiMajor_Axis = (read_line())
            .Eccentricity = (read_line())
            .Inclination = (read_line())
            .RAAN = (read_line())
            .Argument_Perigee = (read_line())
            .Mean_Anomaly = (read_line())
            .bstar = (read_line())
            .dec1 = (read_line())
            .dec2 = (read_line())
        End With
        '
        '
        ' Update simulation start date
        ' ----------------------------
        Dim date_temp As udtDate
        convert_Julian_Gregorian(SatOrbit0.epoch, date_temp)
        With date_temp
            frmMain.txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                 & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        dblSimulationStart = SatOrbit0.epoch
        dblSimulationTime = SatOrbit0.epoch
        update_Date(dblSimulationTime)
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! -------------"
        s$ = (read_line())                      ' "! Attitude data"
        s$ = (read_line())                      ' "! -------------"
        s$ = (read_line())                      ' "!"
        With Satellite
            .primaryAxis = Trim(read_line())
            .primaryDir = Trim(read_line())
            .secondaryAxis = Trim(read_line())
            .secondaryDir = Trim(read_line())
            '
            ' Setting values for attitude
            ' ----------------------------
            ' If no attitude is set, set attitude default values
            If .primaryAxis = "" Then
                .primaryAxis = "-z"
                .primaryDir = "nadir"
                .secondaryAxis = "x"
                .secondaryDir = "velocity"
            End If
            '
            ' Principal direction
            cmbPrinAxis.Text = .primaryAxis
            cmbPrinOrient.Text = .primaryDir
            ' Optimized direction
            cmbOptAxis.Text = .secondaryAxis
            cmbOptOrient.Text = .secondaryDir
        End With
        '



        '
        FileClose(1)
        '
        ' Update orbit
        Update_OrbitWindow()
        ' Update attitude
        If Satellite.primaryAxis <> "" Then
            prinAxis = Satellite.primaryAxis
            Select Case prinAxis
                Case "x" : cmbPrinAxis.SelectedIndex = 0
                Case "y" : cmbPrinAxis.SelectedIndex = 1
                Case "z" : cmbPrinAxis.SelectedIndex = 2
                Case "-x" : cmbPrinAxis.SelectedIndex = 3
                Case "-y" : cmbPrinAxis.SelectedIndex = 4
                Case "-z" : cmbPrinAxis.SelectedIndex = 5
            End Select
        End If
        If Satellite.primaryDir <> "" Then
            prinOrient = Satellite.primaryDir
            Select Case prinOrient
                Case "nadir" : cmbPrinOrient.SelectedIndex = 0
                Case "sun-pointing" : cmbPrinOrient.SelectedIndex = 1
                Case "moon-pointing" : cmbPrinOrient.SelectedIndex = 2
                Case "velocity" : cmbPrinOrient.SelectedIndex = 3
                Case "inertial x" : cmbPrinOrient.SelectedIndex = 4
                Case "inertial y" : cmbPrinOrient.SelectedIndex = 5
                Case "inertial z" : cmbPrinOrient.SelectedIndex = 6
                Case "ground speed" : cmbPrinOrient.SelectedIndex = 7
            End Select
        End If
        If Satellite.secondaryAxis <> "" Then
            optAxis = Satellite.secondaryAxis
            Select Case optAxis
                Case "x" : cmbOptAxis.SelectedIndex = 0
                Case "y" : cmbOptAxis.SelectedIndex = 1
                Case "z" : cmbOptAxis.SelectedIndex = 2
                Case "-x" : cmbOptAxis.SelectedIndex = 3
                Case "-y" : cmbOptAxis.SelectedIndex = 4
                Case "-z" : cmbOptAxis.SelectedIndex = 5
            End Select
        End If
        '
        If Satellite.secondaryDir <> "" Then
            optOrient = Satellite.secondaryDir
            Select Case optOrient
                Case "nadir" : cmbOptOrient.SelectedIndex = 0
                Case "sun-pointing" : cmbOptOrient.SelectedIndex = 1
                Case "moon-pointing" : cmbOptOrient.SelectedIndex = 2
                Case "velocity" : cmbOptOrient.SelectedIndex = 3
                Case "inertial x" : cmbOptOrient.SelectedIndex = 4
                Case "inertial y" : cmbOptOrient.SelectedIndex = 5
                Case "inertial z" : cmbOptOrient.SelectedIndex = 6
                Case "ground speed" : cmbOptOrient.SelectedIndex = 7
            End Select
        End If
        ''
    End Sub


    Private Sub cmbFwdTrack_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFwdTrack.SelectedIndexChanged, cmbBckTrack.SelectedIndexChanged
        ' determine the forward tracksize
        Select Case cmbFwdTrack.Text
            Case "1 Orbit" : Satellite.satFTrackSize = 50
            Case "1/2 Orbit" : Satellite.satFTrackSize = 25
            Case "1/4 Orbit" : Satellite.satFTrackSize = 12
            Case "None" : Satellite.satFTrackSize = 0
        End Select
        ' determine the backward tracksize
        Select Case cmbBckTrack.Text
            Case "1 Orbit" : Satellite.satBTrackSize = 50
            Case "1/2 Orbit" : Satellite.satBTrackSize = 25
            Case "1/4 Orbit" : Satellite.satBTrackSize = 12
            Case "None" : Satellite.satBTrackSize = 0
        End Select
    End Sub
    Private Sub chkfootprint_change(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFootprint.CheckStateChanged
        '
        ' Set footprint
        If chkFootprint.CheckState = 1 Then
            Satellite.satFootprint = True
        Else
            Satellite.satFootprint = False
        End If
        '
    End Sub
    Public Function solar_flux(ByVal d As Double) As Single
        '
        ' Direct solar radiation intensity for a given distance to Sun
        ' Input: d distance to Sun
        '
        solar_flux = CSng(SunPow / 4 / pi / d / d / 1000000.0#)
        '
    End Function
End Class
