Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6
Imports Excel = Microsoft.Office.Interop.Excel

Friend Class frmReport
	Inherits System.Windows.Forms.Form


    Private Sub cmdClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        '
        Me.Hide()
        System.Windows.Forms.Application.DoEvents()
        '
    End Sub

    Private Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        '
        Dim intFile As Short
        Dim col As Integer, row As Integer
        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet
        Dim i As Integer
        '
        On Error GoTo err_SaveReport
        '
        If grdReport.RowCount < 1 Then Exit Sub ' Nothing to save
        '
        If optText.Checked Then
            dlgReportSave.Filter = "(*.txt)|*.txt"
            If dlgReportSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
            '
            intFile = FreeFile()
            FileOpen(intFile, dlgReportSave.FileName, OpenMode.Output)
            '
            Me.Cursor = Cursors.WaitCursor
            '
            With grdReport
                ' Labels line
                Print(1, "Date" & vbTab)
                Print(1, "Battery current" & vbTab)
                Print(1, "Battery charge" & vbTab)
                Print(1, "Battery voltage" & vbTab)
                Print(1, "Batterie power" & vbTab)
                Print(1, "Panels power" & vbTab)
                Print(1, "Available power" & vbTab)
                Print(1, "Excess power" & vbTab)
                Print(1, "Useful power" & vbTab)
                '
                For i = 1 To SolarPanels.Count()
                    Print(1, "P" & i & "_Power")
                    If i = SolarPanels.Count() Then Print(1, vbCrLf) Else Print(1, vbTab)
                Next
                '
                ' Units line
                Print(1, "EUR_UTC" & vbTab)
                Print(1, "A" & vbTab)
                Print(1, "%" & vbTab)
                Print(1, "V" & vbTab)
                Print(1, "W" & vbTab)
                Print(1, "W" & vbTab)
                Print(1, "W" & vbTab)
                Print(1, "W" & vbTab)
                Print(1, "W" & vbTab)
                '
                For i = 1 To SolarPanels.Count()
                    Print(1, "W")
                    If i = SolarPanels.Count() Then Print(1, vbCrLf) Else Print(1, vbTab)
                Next
                '
                ' Data
                For row = 0 To .RowCount - 1
                    Print(1, .Item(0, row).Value & vbTab)
                    For col = 1 To .ColumnCount - 1
                        Print(1, Format(.Item(col, row).Value, "#0.000"))
                        If col = .ColumnCount - 1 Then Print(1, vbCrLf) Else Print(1, vbTab)
                    Next
                Next
                '
            End With
            '
            FileClose(intFile)
            '
        Else
            dlgReportSave.Filter = "(*.xls)|*.xls"
            If dlgReportSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
            '
            xlApp = New Excel.ApplicationClass
            xlWorkBook = xlApp.Workbooks.Add()
            xlWorkSheet = xlWorkBook.Sheets.Add(, , 1, )
            xlWorkSheet.Name = "Simusat data"
            '
            Me.Cursor = Cursors.WaitCursor
            '
            ' Fill data
            With grdReport
                ' Labels line
                For col = 0 To .ColumnCount - 1
                    xlWorkSheet.Cells(1, col + 1) = .Columns(col).HeaderText
                Next
                ' Data
                For row = 0 To .RowCount - 1
                    xlWorkSheet.Cells(row + 2, 1) = .Item(0, row).Value
                    For col = 1 To .ColumnCount - 1
                        xlWorkSheet.Cells(row + 2, col + 1) = Format(.Item(col, row).Value, "#0.000")
                        xlWorkSheet.Cells(row + 2, col + 1) = .Item(col, row).Value
                    Next
                Next
            End With
            '
            ' Formatting
            With xlWorkSheet
                With .Range(.Cells(1, 1), .Cells(1, grdReport.ColumnCount))
                    .EntireColumn.AutoFit()
                    .Interior.Color = vbCyan
                    .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                End With
                With .Range(.Cells(2, 2), .Cells(grdReport.RowCount + 1, grdReport.ColumnCount))
                    .NumberFormat = "#0,00"
                    .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                End With
                With .Range(.Cells(2, 1), .Cells(grdReport.RowCount + 1, 1))
                    .Interior.Color = RGB(255, 255, 192)
                    .NumberFormat = "jj/mm/aaaa hh:mm:ss"
                End With
            End With
            '
            xlWorkBook.SaveAs(dlgReportSave.FileName)
            xlApp.Quit()
            '
        End If
        '
        Me.Cursor = Cursors.Default
        '
        Exit Sub
err_SaveReport:
        MsgBox(Err.Description)
        ''
    End Sub


    Private Sub cmdReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        '
        Dim CurrentRow As Integer
        Dim dblReportStart, dblReportTime As Double, dblReportEnd As Double, dblReportStep As Double
        Dim date_temp As udtDate
        Dim x$
        Dim i As Integer
        '
        ' Datagrid preparation
        With grdReport
            .ColumnCount = 0
            .Columns.Add("", "Date")
            .Columns.Add("", "C_Aero (X)")
            .Columns.Add("", "C_Aero (Y)")
            .Columns.Add("", "C_Aero (Z)")
            .Columns.Add("", "C_Aero (Total)")
            .Columns.Add("", "C_Solar (X)")
            .Columns.Add("", "C_Solar (Y)")
            .Columns.Add("", "C_Solar (Z)")
            .Columns.Add("", "C_Solar (Total)")
            .Columns.Add("", "C_Mag (X)")
            .Columns.Add("", "C_Mag (Y)")
            .Columns.Add("", "C_Mag (Z)")
            .Columns.Add("", "C_Mag (Total)")
            .Columns.Add("", "C_Grav (X)")
            .Columns.Add("", "C_Grav (Y)")
            .Columns.Add("", "C_Grav (Z)")
            .Columns.Add("", "C_Grav (Total)")
            .Columns.Add("", "C_Total (X)")
            .Columns.Add("", "C_Total (Y)")
            .Columns.Add("", "C_Total (Z)")
            .Columns.Add("", "C_Total (Total)")
            '
            '
            ' Mise en forme
            .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            For i = 0 To .ColumnCount - 1
                .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.None
                .Columns(i).Width = 80
                .Columns(i).DefaultCellStyle.Format = "0.000E+00"
            Next
            .Columns(0).Width = 140
            .Columns(0).Frozen = True
        End With

        ' Read start date, end date, step
        With date_temp
            .year = Year(txt_date_debut.Value)
            .month = Month(txt_date_debut.Value)
            .day = Microsoft.VisualBasic.DateAndTime.Day(txt_date_debut.Value)
            .hour = Hour(txt_date_debut.Value)
            .minute = Minute(txt_date_debut.Value)
            .second = Second(txt_date_debut.Value)
        End With
        dblReportStart = convert_Gregorian_Julian(date_temp)
        '
        With date_temp
            .year = Year(txt_date_fin.Value)
            .month = Month(txt_date_fin.Value)
            .day = Microsoft.VisualBasic.DateAndTime.Day(txt_date_fin.Value)
            .hour = Hour(txt_date_fin.Value)
            .minute = Minute(txt_date_fin.Value)
            .second = Second(txt_date_fin.Value)
        End With
        '
        dblReportEnd = convert_Gregorian_Julian(date_temp)
        If dblReportEnd < dblReportStart Then
            MsgBox("End date is earlier than start date", MsgBoxStyle.OkOnly, "")
            Exit Sub
        End If
        x$ = updwTimeStep.Text
        x$ = Microsoft.VisualBasic.Left(x$, Len(x$) - 2)
        dblReportStep = CDbl(x$) / secday
        '
        Me.Cursor = Cursors.WaitCursor
        '
        ' Grid initialisation
        grdReport.RowCount = CInt((dblReportEnd - dblReportStart) / dblReportStep + 1)
        dblReportTime = dblReportStart
        '
        '
        ' Calculate and write data in the grid
        CurrentRow = 0
        Do While dblReportTime <= dblReportEnd
            ' 
            ' Orbitography
            update_Sun(dblReportTime, SunPos)
            SatOrbit = Satellite.getOrbit(dblReportTime) ' Gives orbit parameters, SatPos, SatVel, SatGeoPos
            blnEclipse = Eclipse(SatPos, SunPos)
            '
            ' Simulation
            ' compute_torques
            Ctot = couple_total(dblReportTime)  ' The torques Caer(Aero), Cgg(gravity gradient), Csol(solar), Cmag(Magnatic) are calculated in this function

            'Cmag = couple_magnetique(dblReportTime)
            'Caer = couple_aero(dblReportTime)
            'Csol = couple_solaire(dblReportTime)
            'Cgg = couple_gravite(dblReportTime)
            'Ctot = couple_total(dblReportTime)
            '
            ' Write data
            convert_Julian_Gregorian(dblReportTime, date_temp)
            With date_temp
                x$ = Format(.day, "00") & "/" & Format(.month, "00") & "/" & Format(.year, "0000") & " "
                x$ += Format(.hour, "00") & ":" & Format(.minute, "00") & ":" & Format(.second, "00") & " "
            End With
            '
            With grdReport
                .Item(0, CurrentRow).Value = x$
                .Item(1, CurrentRow).Value = Caer.X
                .Item(2, CurrentRow).Value = Caer.Y
                .Item(3, CurrentRow).Value = Caer.Z
                .Item(4, CurrentRow).Value = vctNorme(Caer)
                .Item(5, CurrentRow).Value = Csol.X
                .Item(6, CurrentRow).Value = Csol.Y
                .Item(7, CurrentRow).Value = Csol.Z
                .Item(8, CurrentRow).Value = vctNorme(Csol)
                .Item(9, CurrentRow).Value = Cmag.X
                .Item(10, CurrentRow).Value = Cmag.Y
                .Item(11, CurrentRow).Value = Cmag.Z
                .Item(12, CurrentRow).Value = vctNorme(Cmag)
                .Item(13, CurrentRow).Value = Cgg.X
                .Item(14, CurrentRow).Value = Cgg.Y
                .Item(15, CurrentRow).Value = Cgg.Z
                .Item(16, CurrentRow).Value = vctNorme(Cgg)
                .Item(17, CurrentRow).Value = Ctot.X
                .Item(18, CurrentRow).Value = Ctot.Y
                .Item(19, CurrentRow).Value = Ctot.Z
                .Item(20, CurrentRow).Value = vctNorme(Ctot)

                '

                If blnEclipse Then .Item(0, CurrentRow).Style.BackColor = Color.LightGray Else .Item(0, CurrentRow).Style.BackColor = Color.LightGoldenrodYellow
            End With
            CurrentRow += 1
            dblReportTime = dblReportTime + dblReportStep
            '
        Loop
        '
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub frmReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        Dim i As Integer
        Dim date_temp As udtDate
        '
        ' Initialisation dates d�but et fin par d�faut
        '
        convert_Julian_Gregorian(SatOrbit0.epoch, date_temp)
        With date_temp
            txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
            txt_date_fin.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                    & Format(.hour + 1, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        '
        With grdReport
            .ColumnCount = 0
            .Columns.Add("", "Date")
            .Columns.Add("", "C_Aero (X)")
            .Columns.Add("", "C_Aero (Y)")
            .Columns.Add("", "C_Aero (Z)")
            .Columns.Add("", "C_Aero (Total)")
            .Columns.Add("", "C_Solar (X)")
            .Columns.Add("", "C_Solar (Y)")
            .Columns.Add("", "C_Solar (Z)")
            .Columns.Add("", "C_Solar (Total)")
            .Columns.Add("", "C_Mag (X)")
            .Columns.Add("", "C_Mag (Y)")
            .Columns.Add("", "C_Mag (Z)")
            .Columns.Add("", "C_Mag (Total)")
            .Columns.Add("", "C_Grav (X)")
            .Columns.Add("", "C_Grav (Y)")
            .Columns.Add("", "C_Grav (Z)")
            .Columns.Add("", "C_Grav (Total)")
            .Columns.Add("", "C_Total (X)")
            .Columns.Add("", "C_Total (Y)")
            .Columns.Add("", "C_Total (Z)")
            .Columns.Add("", "C_Total (Total)")
            '

            '
            ' Mise en forme
            .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            For i = 0 To .ColumnCount - 1
                .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.None
                .Columns(i).Width = 80
            Next
            .Columns(0).Width = 140
            .Columns(0).Frozen = True
        End With


    End Sub

    Private Sub frmReport_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        grdReport.Width = Me.Width - 40
        grdReport.Height = Me.Height - 140
    End Sub
End Class