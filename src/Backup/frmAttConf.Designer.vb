﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAttConf
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAttConf))
        Me.tabPower = New System.Windows.Forms.TabControl
        Me.Onglet_att_geom = New System.Windows.Forms.TabPage
        Me.zfra_8 = New System.Windows.Forms.GroupBox
        Me.zlbl_17 = New System.Windows.Forms.Label
        Me.zlbl_18 = New System.Windows.Forms.Label
        Me.mass_cmbBoomFace = New System.Windows.Forms.ComboBox
        Me.mass_txtBoomMass = New System.Windows.Forms.TextBox
        Me.mass_txtBoomDist = New System.Windows.Forms.TextBox
        Me.zlbl_19 = New System.Windows.Forms.Label
        Me.zlbl_16 = New System.Windows.Forms.Label
        Me.zlbl_20 = New System.Windows.Forms.Label
        Me.zfra_4 = New System.Windows.Forms.GroupBox
        Me.mass_lstPanels = New System.Windows.Forms.ListBox
        Me.mass_cmdModify = New System.Windows.Forms.Button
        Me.mass_txtPanelMass = New System.Windows.Forms.TextBox
        Me.mass_cmdAddPanel = New System.Windows.Forms.Button
        Me.zlbl_75 = New System.Windows.Forms.Label
        Me.mass_cmdRemovePanel = New System.Windows.Forms.Button
        Me.zlbl_64 = New System.Windows.Forms.Label
        Me.zlbl_63 = New System.Windows.Forms.Label
        Me.mass_cmbFace = New System.Windows.Forms.ComboBox
        Me.mass_txtPanelLength = New System.Windows.Forms.TextBox
        Me.mass_txtPanelWidth = New System.Windows.Forms.TextBox
        Me.mass_cmbDirection = New System.Windows.Forms.ComboBox
        Me.mass_cmbPosition = New System.Windows.Forms.ComboBox
        Me.mass_optPanelType_0 = New System.Windows.Forms.RadioButton
        Me.mass_optPanelType_1 = New System.Windows.Forms.RadioButton
        Me.mass_optPanelType_2 = New System.Windows.Forms.RadioButton
        Me.zlbl_69 = New System.Windows.Forms.Label
        Me.zlbl_1 = New System.Windows.Forms.Label
        Me.zlbl_5 = New System.Windows.Forms.Label
        Me.zlbl_6 = New System.Windows.Forms.Label
        Me.mass_lblPanelOrient = New System.Windows.Forms.Label
        Me.mass_lblPanelPos = New System.Windows.Forms.Label
        Me.zlbl_3 = New System.Windows.Forms.Label
        Me.zpict_5 = New System.Windows.Forms.PictureBox
        Me.zfra_3 = New System.Windows.Forms.GroupBox
        Me.zlbl_11 = New System.Windows.Forms.Label
        Me.mass_txtBodyMass = New System.Windows.Forms.TextBox
        Me.zlbl_15 = New System.Windows.Forms.Label
        Me.zfra_7 = New System.Windows.Forms.GroupBox
        Me.mass_txtGy = New System.Windows.Forms.TextBox
        Me.zlbl_74 = New System.Windows.Forms.Label
        Me.zlbl_83 = New System.Windows.Forms.Label
        Me.mass_txtGx = New System.Windows.Forms.TextBox
        Me.zlbl_78 = New System.Windows.Forms.Label
        Me.zlbl_82 = New System.Windows.Forms.Label
        Me.mass_txtGz = New System.Windows.Forms.TextBox
        Me.zlbl_79 = New System.Windows.Forms.Label
        Me.zlbl_80 = New System.Windows.Forms.Label
        Me.zfra_6 = New System.Windows.Forms.GroupBox
        Me.mass_txtIyz = New System.Windows.Forms.TextBox
        Me.zlbl_85 = New System.Windows.Forms.Label
        Me.zlbl_86 = New System.Windows.Forms.Label
        Me.zlbl_14 = New System.Windows.Forms.Label
        Me.mass_txtIyy = New System.Windows.Forms.TextBox
        Me.zlbl_13 = New System.Windows.Forms.Label
        Me.zlbl_84 = New System.Windows.Forms.Label
        Me.zlbl_12 = New System.Windows.Forms.Label
        Me.mass_txtIzz = New System.Windows.Forms.TextBox
        Me.mass_txtIxz = New System.Windows.Forms.TextBox
        Me.mass_txtIxx = New System.Windows.Forms.TextBox
        Me.mass_txtIxy = New System.Windows.Forms.TextBox
        Me.zlbl_87 = New System.Windows.Forms.Label
        Me.zlbl_10 = New System.Windows.Forms.Label
        Me.zlbl_88 = New System.Windows.Forms.Label
        Me.zfra_5 = New System.Windows.Forms.GroupBox
        Me.zlbl_0 = New System.Windows.Forms.Label
        Me.zlbl_2 = New System.Windows.Forms.Label
        Me.mass_txtBodyWidth = New System.Windows.Forms.TextBox
        Me.mass_txtBodyHeigth = New System.Windows.Forms.TextBox
        Me.zlbl_65 = New System.Windows.Forms.Label
        Me.zlbl_66 = New System.Windows.Forms.Label
        Me.Onglet_att_prop = New System.Windows.Forms.TabPage
        Me.zfra_14 = New System.Windows.Forms.GroupBox
        Me.prop_txtMG_X = New System.Windows.Forms.TextBox
        Me.prop_txtMG_Y = New System.Windows.Forms.TextBox
        Me.zlbl34 = New System.Windows.Forms.Label
        Me.zlbl31 = New System.Windows.Forms.Label
        Me.zlbl35 = New System.Windows.Forms.Label
        Me.prop_txtMG_Z = New System.Windows.Forms.TextBox
        Me.zfra_11 = New System.Windows.Forms.GroupBox
        Me.zfra_12 = New System.Windows.Forms.GroupBox
        Me.prop_txtCD_0 = New System.Windows.Forms.TextBox
        Me.prop_txtCD_4 = New System.Windows.Forms.TextBox
        Me.zlbl24 = New System.Windows.Forms.Label
        Me.prop_txtCD_3 = New System.Windows.Forms.TextBox
        Me.prop_txtCD_2 = New System.Windows.Forms.TextBox
        Me.zlbl23 = New System.Windows.Forms.Label
        Me.prop_txtCD_5 = New System.Windows.Forms.TextBox
        Me.prop_txtCD_1 = New System.Windows.Forms.TextBox
        Me.zlbl22 = New System.Windows.Forms.Label
        Me.zlbl19 = New System.Windows.Forms.Label
        Me.zlbl20 = New System.Windows.Forms.Label
        Me.zlbl21 = New System.Windows.Forms.Label
        Me.zfra_13 = New System.Windows.Forms.GroupBox
        Me.prop_txtCR_0 = New System.Windows.Forms.TextBox
        Me.prop_txtCR_4 = New System.Windows.Forms.TextBox
        Me.zlbl18 = New System.Windows.Forms.Label
        Me.prop_txtCR_3 = New System.Windows.Forms.TextBox
        Me.zlbl17 = New System.Windows.Forms.Label
        Me.prop_txtCR_2 = New System.Windows.Forms.TextBox
        Me.prop_txtCR_5 = New System.Windows.Forms.TextBox
        Me.zlbl16 = New System.Windows.Forms.Label
        Me.prop_txtCR_1 = New System.Windows.Forms.TextBox
        Me.zlbl15 = New System.Windows.Forms.Label
        Me.zlbl13 = New System.Windows.Forms.Label
        Me.zlbl14 = New System.Windows.Forms.Label
        Me.zfra_1 = New System.Windows.Forms.GroupBox
        Me.zfra_10 = New System.Windows.Forms.GroupBox
        Me.prop_txtCT_0 = New System.Windows.Forms.TextBox
        Me.prop_txtCT_4 = New System.Windows.Forms.TextBox
        Me.zlbl12 = New System.Windows.Forms.Label
        Me.zlbl11 = New System.Windows.Forms.Label
        Me.prop_txtCT_3 = New System.Windows.Forms.TextBox
        Me.zlbl10 = New System.Windows.Forms.Label
        Me.prop_txtCT_2 = New System.Windows.Forms.TextBox
        Me.zlbl6 = New System.Windows.Forms.Label
        Me.prop_txtCT_5 = New System.Windows.Forms.TextBox
        Me.zlbl4 = New System.Windows.Forms.Label
        Me.prop_txtCT_1 = New System.Windows.Forms.TextBox
        Me.zlbl3 = New System.Windows.Forms.Label
        Me.zfra_2 = New System.Windows.Forms.GroupBox
        Me.prop_txtCN_0 = New System.Windows.Forms.TextBox
        Me.prop_txtCN_4 = New System.Windows.Forms.TextBox
        Me.zlbl9 = New System.Windows.Forms.Label
        Me.zlbl1 = New System.Windows.Forms.Label
        Me.zlbl8 = New System.Windows.Forms.Label
        Me.zlbl2 = New System.Windows.Forms.Label
        Me.zlbl7 = New System.Windows.Forms.Label
        Me.prop_txtCN_3 = New System.Windows.Forms.TextBox
        Me.prop_txtCN_2 = New System.Windows.Forms.TextBox
        Me.zlbl5 = New System.Windows.Forms.Label
        Me.prop_txtCN_5 = New System.Windows.Forms.TextBox
        Me.prop_txtCN_1 = New System.Windows.Forms.TextBox
        Me.Onglet_att_act = New System.Windows.Forms.TabPage
        Me.Onglet_att_manag = New System.Windows.Forms.TabPage
        Me.zfra_9 = New System.Windows.Forms.GroupBox
        Me.mass_lblSatMass = New System.Windows.Forms.Label
        Me.zlbl30 = New System.Windows.Forms.Label
        Me.zlbl28 = New System.Windows.Forms.Label
        Me.zfra_15 = New System.Windows.Forms.GroupBox
        Me.mass_lblSatGz = New System.Windows.Forms.Label
        Me.mass_lblSatGy = New System.Windows.Forms.Label
        Me.mass_lblSatGx = New System.Windows.Forms.Label
        Me.zlbl37 = New System.Windows.Forms.Label
        Me.zlbl38 = New System.Windows.Forms.Label
        Me.zlbl40 = New System.Windows.Forms.Label
        Me.zfra_16 = New System.Windows.Forms.GroupBox
        Me.mass_lblSatIxz = New System.Windows.Forms.Label
        Me.mass_lblSatIxy = New System.Windows.Forms.Label
        Me.mass_lblSatIyz = New System.Windows.Forms.Label
        Me.mass_lblSatIxx = New System.Windows.Forms.Label
        Me.mass_lblSatIyy = New System.Windows.Forms.Label
        Me.mass_lblSatIzz = New System.Windows.Forms.Label
        Me.zlbl25 = New System.Windows.Forms.Label
        Me.zlbl26 = New System.Windows.Forms.Label
        Me.zlbl29 = New System.Windows.Forms.Label
        Me.zlbl32 = New System.Windows.Forms.Label
        Me.zlbl33 = New System.Windows.Forms.Label
        Me.zlbl36 = New System.Windows.Forms.Label
        Me.mass_pictSatellite = New System.Windows.Forms.PictureBox
        Me.cmd_quit = New System.Windows.Forms.Button
        Me.tmrAnimate = New System.Windows.Forms.Timer(Me.components)
        Me.tmrTransition = New System.Windows.Forms.Timer(Me.components)
        Me.tabPower.SuspendLayout()
        Me.Onglet_att_geom.SuspendLayout()
        Me.zfra_8.SuspendLayout()
        Me.zfra_4.SuspendLayout()
        CType(Me.zpict_5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.zfra_3.SuspendLayout()
        Me.zfra_7.SuspendLayout()
        Me.zfra_6.SuspendLayout()
        Me.zfra_5.SuspendLayout()
        Me.Onglet_att_prop.SuspendLayout()
        Me.zfra_14.SuspendLayout()
        Me.zfra_11.SuspendLayout()
        Me.zfra_12.SuspendLayout()
        Me.zfra_13.SuspendLayout()
        Me.zfra_1.SuspendLayout()
        Me.zfra_10.SuspendLayout()
        Me.zfra_2.SuspendLayout()
        Me.zfra_9.SuspendLayout()
        Me.zfra_15.SuspendLayout()
        Me.zfra_16.SuspendLayout()
        CType(Me.mass_pictSatellite, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tabPower
        '
        Me.tabPower.Controls.Add(Me.Onglet_att_geom)
        Me.tabPower.Controls.Add(Me.Onglet_att_prop)
        Me.tabPower.Controls.Add(Me.Onglet_att_act)
        Me.tabPower.Controls.Add(Me.Onglet_att_manag)
        Me.tabPower.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabPower.ItemSize = New System.Drawing.Size(100, 28)
        Me.tabPower.Location = New System.Drawing.Point(23, 23)
        Me.tabPower.Name = "tabPower"
        Me.tabPower.SelectedIndex = 0
        Me.tabPower.Size = New System.Drawing.Size(635, 556)
        Me.tabPower.TabIndex = 2
        '
        'Onglet_att_geom
        '
        Me.Onglet_att_geom.Controls.Add(Me.zfra_8)
        Me.Onglet_att_geom.Controls.Add(Me.zfra_4)
        Me.Onglet_att_geom.Controls.Add(Me.zfra_3)
        Me.Onglet_att_geom.Location = New System.Drawing.Point(4, 32)
        Me.Onglet_att_geom.Name = "Onglet_att_geom"
        Me.Onglet_att_geom.Size = New System.Drawing.Size(627, 520)
        Me.Onglet_att_geom.TabIndex = 6
        Me.Onglet_att_geom.Text = "Mass, geometry"
        Me.Onglet_att_geom.UseVisualStyleBackColor = True
        '
        'zfra_8
        '
        Me.zfra_8.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_8.Controls.Add(Me.zlbl_17)
        Me.zfra_8.Controls.Add(Me.zlbl_18)
        Me.zfra_8.Controls.Add(Me.mass_cmbBoomFace)
        Me.zfra_8.Controls.Add(Me.mass_txtBoomMass)
        Me.zfra_8.Controls.Add(Me.mass_txtBoomDist)
        Me.zfra_8.Controls.Add(Me.zlbl_19)
        Me.zfra_8.Controls.Add(Me.zlbl_16)
        Me.zfra_8.Controls.Add(Me.zlbl_20)
        Me.zfra_8.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_8.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_8.Location = New System.Drawing.Point(15, 434)
        Me.zfra_8.Name = "zfra_8"
        Me.zfra_8.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_8.Size = New System.Drawing.Size(592, 69)
        Me.zfra_8.TabIndex = 12
        Me.zfra_8.TabStop = False
        Me.zfra_8.Text = "Gravity gradient boom"
        '
        'zlbl_17
        '
        Me.zlbl_17.AutoSize = True
        Me.zlbl_17.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_17.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_17.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_17.Location = New System.Drawing.Point(285, 31)
        Me.zlbl_17.Name = "zlbl_17"
        Me.zlbl_17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_17.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_17.TabIndex = 25
        Me.zlbl_17.Text = "m"
        Me.zlbl_17.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_18
        '
        Me.zlbl_18.AutoSize = True
        Me.zlbl_18.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_18.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_18.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_18.Location = New System.Drawing.Point(122, 31)
        Me.zlbl_18.Name = "zlbl_18"
        Me.zlbl_18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_18.Size = New System.Drawing.Size(23, 16)
        Me.zlbl_18.TabIndex = 25
        Me.zlbl_18.Text = "kg"
        Me.zlbl_18.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_cmbBoomFace
        '
        Me.mass_cmbBoomFace.BackColor = System.Drawing.SystemColors.Window
        Me.mass_cmbBoomFace.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmbBoomFace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.mass_cmbBoomFace.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmbBoomFace.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_cmbBoomFace.Location = New System.Drawing.Point(392, 28)
        Me.mass_cmbBoomFace.Name = "mass_cmbBoomFace"
        Me.mass_cmbBoomFace.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmbBoomFace.Size = New System.Drawing.Size(66, 24)
        Me.mass_cmbBoomFace.TabIndex = 18
        '
        'mass_txtBoomMass
        '
        Me.mass_txtBoomMass.AcceptsReturn = True
        Me.mass_txtBoomMass.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtBoomMass.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtBoomMass.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtBoomMass.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtBoomMass.Location = New System.Drawing.Point(64, 28)
        Me.mass_txtBoomMass.MaximumSize = New System.Drawing.Size(150, 50)
        Me.mass_txtBoomMass.MaxLength = 0
        Me.mass_txtBoomMass.Name = "mass_txtBoomMass"
        Me.mass_txtBoomMass.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtBoomMass.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtBoomMass.TabIndex = 17
        Me.mass_txtBoomMass.Text = "1"
        '
        'mass_txtBoomDist
        '
        Me.mass_txtBoomDist.AcceptsReturn = True
        Me.mass_txtBoomDist.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtBoomDist.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtBoomDist.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtBoomDist.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtBoomDist.Location = New System.Drawing.Point(230, 28)
        Me.mass_txtBoomDist.MaximumSize = New System.Drawing.Size(150, 50)
        Me.mass_txtBoomDist.MaxLength = 0
        Me.mass_txtBoomDist.Name = "mass_txtBoomDist"
        Me.mass_txtBoomDist.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtBoomDist.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtBoomDist.TabIndex = 16
        Me.mass_txtBoomDist.Text = "1"
        '
        'zlbl_19
        '
        Me.zlbl_19.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_19.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_19.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_19.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_19.Location = New System.Drawing.Point(297, 31)
        Me.zlbl_19.Name = "zlbl_19"
        Me.zlbl_19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_19.Size = New System.Drawing.Size(89, 19)
        Me.zlbl_19.TabIndex = 23
        Me.zlbl_19.Text = "On face"
        Me.zlbl_19.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_16
        '
        Me.zlbl_16.AutoSize = True
        Me.zlbl_16.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_16.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_16.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_16.Location = New System.Drawing.Point(17, 31)
        Me.zlbl_16.Name = "zlbl_16"
        Me.zlbl_16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_16.Size = New System.Drawing.Size(41, 16)
        Me.zlbl_16.TabIndex = 22
        Me.zlbl_16.Text = "Mass"
        Me.zlbl_16.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_20
        '
        Me.zlbl_20.AutoSize = True
        Me.zlbl_20.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_20.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_20.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_20.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_20.Location = New System.Drawing.Point(162, 31)
        Me.zlbl_20.Name = "zlbl_20"
        Me.zlbl_20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_20.Size = New System.Drawing.Size(65, 16)
        Me.zlbl_20.TabIndex = 21
        Me.zlbl_20.Text = "Distance"
        Me.zlbl_20.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zfra_4
        '
        Me.zfra_4.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_4.Controls.Add(Me.mass_lstPanels)
        Me.zfra_4.Controls.Add(Me.mass_cmdModify)
        Me.zfra_4.Controls.Add(Me.mass_txtPanelMass)
        Me.zfra_4.Controls.Add(Me.mass_cmdAddPanel)
        Me.zfra_4.Controls.Add(Me.zlbl_75)
        Me.zfra_4.Controls.Add(Me.mass_cmdRemovePanel)
        Me.zfra_4.Controls.Add(Me.zlbl_64)
        Me.zfra_4.Controls.Add(Me.zlbl_63)
        Me.zfra_4.Controls.Add(Me.mass_cmbFace)
        Me.zfra_4.Controls.Add(Me.mass_txtPanelLength)
        Me.zfra_4.Controls.Add(Me.mass_txtPanelWidth)
        Me.zfra_4.Controls.Add(Me.mass_cmbDirection)
        Me.zfra_4.Controls.Add(Me.mass_cmbPosition)
        Me.zfra_4.Controls.Add(Me.mass_optPanelType_0)
        Me.zfra_4.Controls.Add(Me.mass_optPanelType_1)
        Me.zfra_4.Controls.Add(Me.mass_optPanelType_2)
        Me.zfra_4.Controls.Add(Me.zlbl_69)
        Me.zfra_4.Controls.Add(Me.zlbl_1)
        Me.zfra_4.Controls.Add(Me.zlbl_5)
        Me.zfra_4.Controls.Add(Me.zlbl_6)
        Me.zfra_4.Controls.Add(Me.mass_lblPanelOrient)
        Me.zfra_4.Controls.Add(Me.mass_lblPanelPos)
        Me.zfra_4.Controls.Add(Me.zlbl_3)
        Me.zfra_4.Controls.Add(Me.zpict_5)
        Me.zfra_4.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_4.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_4.Location = New System.Drawing.Point(16, 203)
        Me.zfra_4.Name = "zfra_4"
        Me.zfra_4.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_4.Size = New System.Drawing.Size(591, 218)
        Me.zfra_4.TabIndex = 12
        Me.zfra_4.TabStop = False
        Me.zfra_4.Text = "Panels"
        '
        'mass_lstPanels
        '
        Me.mass_lstPanels.BackColor = System.Drawing.SystemColors.Window
        Me.mass_lstPanels.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_lstPanels.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lstPanels.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_lstPanels.ItemHeight = 16
        Me.mass_lstPanels.Location = New System.Drawing.Point(428, 29)
        Me.mass_lstPanels.Name = "mass_lstPanels"
        Me.mass_lstPanels.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_lstPanels.Size = New System.Drawing.Size(150, 132)
        Me.mass_lstPanels.TabIndex = 27
        '
        'mass_cmdModify
        '
        Me.mass_cmdModify.BackColor = System.Drawing.SystemColors.Control
        Me.mass_cmdModify.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmdModify.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmdModify.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_cmdModify.Location = New System.Drawing.Point(462, 172)
        Me.mass_cmdModify.Name = "mass_cmdModify"
        Me.mass_cmdModify.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmdModify.Size = New System.Drawing.Size(67, 31)
        Me.mass_cmdModify.TabIndex = 28
        Me.mass_cmdModify.Text = "Modify"
        Me.mass_cmdModify.UseVisualStyleBackColor = False
        '
        'mass_txtPanelMass
        '
        Me.mass_txtPanelMass.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtPanelMass.Location = New System.Drawing.Point(226, 170)
        Me.mass_txtPanelMass.Name = "mass_txtPanelMass"
        Me.mass_txtPanelMass.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtPanelMass.TabIndex = 26
        Me.mass_txtPanelMass.Text = "1"
        '
        'mass_cmdAddPanel
        '
        Me.mass_cmdAddPanel.BackColor = System.Drawing.SystemColors.Control
        Me.mass_cmdAddPanel.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmdAddPanel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmdAddPanel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_cmdAddPanel.Location = New System.Drawing.Point(312, 54)
        Me.mass_cmdAddPanel.Name = "mass_cmdAddPanel"
        Me.mass_cmdAddPanel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmdAddPanel.Size = New System.Drawing.Size(103, 29)
        Me.mass_cmdAddPanel.TabIndex = 25
        Me.mass_cmdAddPanel.Text = "Add >>"
        Me.mass_cmdAddPanel.UseVisualStyleBackColor = False
        '
        'zlbl_75
        '
        Me.zlbl_75.AutoSize = True
        Me.zlbl_75.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_75.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_75.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_75.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_75.Location = New System.Drawing.Point(280, 172)
        Me.zlbl_75.Name = "zlbl_75"
        Me.zlbl_75.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_75.Size = New System.Drawing.Size(23, 16)
        Me.zlbl_75.TabIndex = 25
        Me.zlbl_75.Text = "kg"
        Me.zlbl_75.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_cmdRemovePanel
        '
        Me.mass_cmdRemovePanel.BackColor = System.Drawing.SystemColors.Control
        Me.mass_cmdRemovePanel.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmdRemovePanel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmdRemovePanel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_cmdRemovePanel.Location = New System.Drawing.Point(312, 95)
        Me.mass_cmdRemovePanel.Name = "mass_cmdRemovePanel"
        Me.mass_cmdRemovePanel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmdRemovePanel.Size = New System.Drawing.Size(103, 31)
        Me.mass_cmdRemovePanel.TabIndex = 24
        Me.mass_cmdRemovePanel.Text = "<< Remove"
        Me.mass_cmdRemovePanel.UseVisualStyleBackColor = False
        '
        'zlbl_64
        '
        Me.zlbl_64.AutoSize = True
        Me.zlbl_64.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_64.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_64.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_64.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_64.Location = New System.Drawing.Point(282, 143)
        Me.zlbl_64.Name = "zlbl_64"
        Me.zlbl_64.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_64.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_64.TabIndex = 25
        Me.zlbl_64.Text = "m"
        Me.zlbl_64.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_63
        '
        Me.zlbl_63.AutoSize = True
        Me.zlbl_63.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_63.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_63.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_63.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_63.Location = New System.Drawing.Point(282, 113)
        Me.zlbl_63.Name = "zlbl_63"
        Me.zlbl_63.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_63.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_63.TabIndex = 25
        Me.zlbl_63.Text = "m"
        Me.zlbl_63.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_cmbFace
        '
        Me.mass_cmbFace.BackColor = System.Drawing.SystemColors.Window
        Me.mass_cmbFace.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmbFace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.mass_cmbFace.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmbFace.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_cmbFace.Location = New System.Drawing.Point(226, 21)
        Me.mass_cmbFace.Name = "mass_cmbFace"
        Me.mass_cmbFace.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmbFace.Size = New System.Drawing.Size(66, 24)
        Me.mass_cmbFace.TabIndex = 18
        '
        'mass_txtPanelLength
        '
        Me.mass_txtPanelLength.AcceptsReturn = True
        Me.mass_txtPanelLength.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtPanelLength.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtPanelLength.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtPanelLength.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtPanelLength.Location = New System.Drawing.Point(226, 111)
        Me.mass_txtPanelLength.MaximumSize = New System.Drawing.Size(150, 50)
        Me.mass_txtPanelLength.MaxLength = 0
        Me.mass_txtPanelLength.Name = "mass_txtPanelLength"
        Me.mass_txtPanelLength.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtPanelLength.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtPanelLength.TabIndex = 17
        Me.mass_txtPanelLength.Text = "1"
        '
        'mass_txtPanelWidth
        '
        Me.mass_txtPanelWidth.AcceptsReturn = True
        Me.mass_txtPanelWidth.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtPanelWidth.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtPanelWidth.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtPanelWidth.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtPanelWidth.Location = New System.Drawing.Point(226, 141)
        Me.mass_txtPanelWidth.MaximumSize = New System.Drawing.Size(150, 50)
        Me.mass_txtPanelWidth.MaxLength = 0
        Me.mass_txtPanelWidth.Name = "mass_txtPanelWidth"
        Me.mass_txtPanelWidth.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtPanelWidth.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtPanelWidth.TabIndex = 16
        Me.mass_txtPanelWidth.Text = "1"
        '
        'mass_cmbDirection
        '
        Me.mass_cmbDirection.BackColor = System.Drawing.SystemColors.Window
        Me.mass_cmbDirection.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmbDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.mass_cmbDirection.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmbDirection.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_cmbDirection.Location = New System.Drawing.Point(226, 51)
        Me.mass_cmbDirection.Name = "mass_cmbDirection"
        Me.mass_cmbDirection.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmbDirection.Size = New System.Drawing.Size(66, 24)
        Me.mass_cmbDirection.TabIndex = 15
        '
        'mass_cmbPosition
        '
        Me.mass_cmbPosition.BackColor = System.Drawing.SystemColors.Window
        Me.mass_cmbPosition.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_cmbPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.mass_cmbPosition.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_cmbPosition.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_cmbPosition.Items.AddRange(New Object() {"Upper", "Middle", "Bottom"})
        Me.mass_cmbPosition.Location = New System.Drawing.Point(226, 81)
        Me.mass_cmbPosition.Name = "mass_cmbPosition"
        Me.mass_cmbPosition.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_cmbPosition.Size = New System.Drawing.Size(66, 24)
        Me.mass_cmbPosition.TabIndex = 14
        '
        'mass_optPanelType_0
        '
        Me.mass_optPanelType_0.BackColor = System.Drawing.SystemColors.Control
        Me.mass_optPanelType_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_optPanelType_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_optPanelType_0.Location = New System.Drawing.Point(55, 85)
        Me.mass_optPanelType_0.Name = "mass_optPanelType_0"
        Me.mass_optPanelType_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_optPanelType_0.Size = New System.Drawing.Size(14, 13)
        Me.mass_optPanelType_0.TabIndex = 12
        Me.mass_optPanelType_0.TabStop = True
        Me.mass_optPanelType_0.UseVisualStyleBackColor = False
        '
        'mass_optPanelType_1
        '
        Me.mass_optPanelType_1.BackColor = System.Drawing.SystemColors.Control
        Me.mass_optPanelType_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_optPanelType_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_optPanelType_1.Location = New System.Drawing.Point(83, 85)
        Me.mass_optPanelType_1.Name = "mass_optPanelType_1"
        Me.mass_optPanelType_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_optPanelType_1.Size = New System.Drawing.Size(14, 13)
        Me.mass_optPanelType_1.TabIndex = 11
        Me.mass_optPanelType_1.TabStop = True
        Me.mass_optPanelType_1.UseVisualStyleBackColor = False
        '
        'mass_optPanelType_2
        '
        Me.mass_optPanelType_2.BackColor = System.Drawing.SystemColors.Control
        Me.mass_optPanelType_2.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_optPanelType_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_optPanelType_2.Location = New System.Drawing.Point(112, 85)
        Me.mass_optPanelType_2.Name = "mass_optPanelType_2"
        Me.mass_optPanelType_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_optPanelType_2.Size = New System.Drawing.Size(14, 13)
        Me.mass_optPanelType_2.TabIndex = 10
        Me.mass_optPanelType_2.TabStop = True
        Me.mass_optPanelType_2.UseVisualStyleBackColor = False
        '
        'zlbl_69
        '
        Me.zlbl_69.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_69.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_69.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_69.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_69.Location = New System.Drawing.Point(180, 171)
        Me.zlbl_69.Name = "zlbl_69"
        Me.zlbl_69.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_69.Size = New System.Drawing.Size(41, 16)
        Me.zlbl_69.TabIndex = 22
        Me.zlbl_69.Text = "Mass"
        Me.zlbl_69.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_1
        '
        Me.zlbl_1.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_1.Location = New System.Drawing.Point(132, 26)
        Me.zlbl_1.Name = "zlbl_1"
        Me.zlbl_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_1.Size = New System.Drawing.Size(89, 19)
        Me.zlbl_1.TabIndex = 23
        Me.zlbl_1.Text = "Mount face"
        Me.zlbl_1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_5
        '
        Me.zlbl_5.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_5.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_5.Location = New System.Drawing.Point(168, 114)
        Me.zlbl_5.Name = "zlbl_5"
        Me.zlbl_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_5.Size = New System.Drawing.Size(53, 16)
        Me.zlbl_5.TabIndex = 22
        Me.zlbl_5.Text = "Length"
        Me.zlbl_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_6
        '
        Me.zlbl_6.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_6.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_6.Location = New System.Drawing.Point(175, 144)
        Me.zlbl_6.Name = "zlbl_6"
        Me.zlbl_6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_6.Size = New System.Drawing.Size(46, 16)
        Me.zlbl_6.TabIndex = 21
        Me.zlbl_6.Text = "Width"
        Me.zlbl_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'mass_lblPanelOrient
        '
        Me.mass_lblPanelOrient.BackColor = System.Drawing.SystemColors.Control
        Me.mass_lblPanelOrient.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_lblPanelOrient.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblPanelOrient.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_lblPanelOrient.Location = New System.Drawing.Point(132, 54)
        Me.mass_lblPanelOrient.Name = "mass_lblPanelOrient"
        Me.mass_lblPanelOrient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_lblPanelOrient.Size = New System.Drawing.Size(89, 19)
        Me.mass_lblPanelOrient.TabIndex = 20
        Me.mass_lblPanelOrient.Text = "Orientation"
        Me.mass_lblPanelOrient.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_lblPanelPos
        '
        Me.mass_lblPanelPos.BackColor = System.Drawing.SystemColors.Control
        Me.mass_lblPanelPos.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_lblPanelPos.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblPanelPos.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_lblPanelPos.Location = New System.Drawing.Point(147, 84)
        Me.mass_lblPanelPos.Name = "mass_lblPanelPos"
        Me.mass_lblPanelPos.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_lblPanelPos.Size = New System.Drawing.Size(73, 19)
        Me.mass_lblPanelPos.TabIndex = 19
        Me.mass_lblPanelPos.Text = "Position"
        Me.mass_lblPanelPos.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_3
        '
        Me.zlbl_3.AutoSize = True
        Me.zlbl_3.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_3.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_3.Location = New System.Drawing.Point(35, 67)
        Me.zlbl_3.Name = "zlbl_3"
        Me.zlbl_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_3.Size = New System.Drawing.Size(52, 16)
        Me.zlbl_3.TabIndex = 13
        Me.zlbl_3.Text = "Type :"
        '
        'zpict_5
        '
        Me.zpict_5.Cursor = System.Windows.Forms.Cursors.Default
        Me.zpict_5.Image = CType(resources.GetObject("zpict_5.Image"), System.Drawing.Image)
        Me.zpict_5.Location = New System.Drawing.Point(28, 95)
        Me.zpict_5.Name = "zpict_5"
        Me.zpict_5.Size = New System.Drawing.Size(108, 79)
        Me.zpict_5.TabIndex = 24
        Me.zpict_5.TabStop = False
        '
        'zfra_3
        '
        Me.zfra_3.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_3.Controls.Add(Me.zlbl_11)
        Me.zfra_3.Controls.Add(Me.mass_txtBodyMass)
        Me.zfra_3.Controls.Add(Me.zlbl_15)
        Me.zfra_3.Controls.Add(Me.zfra_7)
        Me.zfra_3.Controls.Add(Me.zfra_6)
        Me.zfra_3.Controls.Add(Me.zfra_5)
        Me.zfra_3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_3.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_3.Location = New System.Drawing.Point(15, 15)
        Me.zfra_3.Name = "zfra_3"
        Me.zfra_3.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_3.Size = New System.Drawing.Size(592, 182)
        Me.zfra_3.TabIndex = 11
        Me.zfra_3.TabStop = False
        Me.zfra_3.Text = "Central body"
        '
        'zlbl_11
        '
        Me.zlbl_11.AutoSize = True
        Me.zlbl_11.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_11.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_11.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_11.Location = New System.Drawing.Point(296, 25)
        Me.zlbl_11.Name = "zlbl_11"
        Me.zlbl_11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_11.Size = New System.Drawing.Size(23, 16)
        Me.zlbl_11.TabIndex = 39
        Me.zlbl_11.Text = "kg"
        Me.zlbl_11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_txtBodyMass
        '
        Me.mass_txtBodyMass.AcceptsReturn = True
        Me.mass_txtBodyMass.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtBodyMass.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtBodyMass.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtBodyMass.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtBodyMass.Location = New System.Drawing.Point(250, 22)
        Me.mass_txtBodyMass.MaxLength = 0
        Me.mass_txtBodyMass.Name = "mass_txtBodyMass"
        Me.mass_txtBodyMass.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtBodyMass.Size = New System.Drawing.Size(43, 23)
        Me.mass_txtBodyMass.TabIndex = 38
        Me.mass_txtBodyMass.Text = "10"
        '
        'zlbl_15
        '
        Me.zlbl_15.AutoSize = True
        Me.zlbl_15.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_15.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_15.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_15.Location = New System.Drawing.Point(203, 25)
        Me.zlbl_15.Name = "zlbl_15"
        Me.zlbl_15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_15.Size = New System.Drawing.Size(41, 16)
        Me.zlbl_15.TabIndex = 37
        Me.zlbl_15.Text = "Mass"
        Me.zlbl_15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_7
        '
        Me.zfra_7.Controls.Add(Me.mass_txtGy)
        Me.zfra_7.Controls.Add(Me.zlbl_74)
        Me.zfra_7.Controls.Add(Me.zlbl_83)
        Me.zfra_7.Controls.Add(Me.mass_txtGx)
        Me.zfra_7.Controls.Add(Me.zlbl_78)
        Me.zfra_7.Controls.Add(Me.zlbl_82)
        Me.zfra_7.Controls.Add(Me.mass_txtGz)
        Me.zfra_7.Controls.Add(Me.zlbl_79)
        Me.zfra_7.Controls.Add(Me.zlbl_80)
        Me.zfra_7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_7.ForeColor = System.Drawing.Color.Black
        Me.zfra_7.Location = New System.Drawing.Point(176, 55)
        Me.zfra_7.Name = "zfra_7"
        Me.zfra_7.Size = New System.Drawing.Size(128, 117)
        Me.zfra_7.TabIndex = 36
        Me.zfra_7.TabStop = False
        Me.zfra_7.Text = "C.G location"
        '
        'mass_txtGy
        '
        Me.mass_txtGy.AcceptsReturn = True
        Me.mass_txtGy.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtGy.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtGy.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtGy.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtGy.Location = New System.Drawing.Point(37, 54)
        Me.mass_txtGy.MaxLength = 0
        Me.mass_txtGy.Name = "mass_txtGy"
        Me.mass_txtGy.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtGy.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtGy.TabIndex = 31
        Me.mass_txtGy.Text = "0"
        '
        'zlbl_74
        '
        Me.zlbl_74.AutoSize = True
        Me.zlbl_74.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_74.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_74.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_74.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_74.Location = New System.Drawing.Point(7, 28)
        Me.zlbl_74.Name = "zlbl_74"
        Me.zlbl_74.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_74.Size = New System.Drawing.Size(24, 16)
        Me.zlbl_74.TabIndex = 27
        Me.zlbl_74.Text = "Gx"
        Me.zlbl_74.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_83
        '
        Me.zlbl_83.AutoSize = True
        Me.zlbl_83.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_83.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_83.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_83.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_83.Location = New System.Drawing.Point(7, 57)
        Me.zlbl_83.Name = "zlbl_83"
        Me.zlbl_83.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_83.Size = New System.Drawing.Size(25, 16)
        Me.zlbl_83.TabIndex = 29
        Me.zlbl_83.Text = "Gy"
        Me.zlbl_83.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'mass_txtGx
        '
        Me.mass_txtGx.AcceptsReturn = True
        Me.mass_txtGx.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtGx.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtGx.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtGx.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtGx.Location = New System.Drawing.Point(37, 25)
        Me.mass_txtGx.MaxLength = 0
        Me.mass_txtGx.Name = "mass_txtGx"
        Me.mass_txtGx.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtGx.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtGx.TabIndex = 30
        Me.mass_txtGx.Text = "0"
        '
        'zlbl_78
        '
        Me.zlbl_78.AutoSize = True
        Me.zlbl_78.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_78.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_78.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_78.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_78.Location = New System.Drawing.Point(92, 86)
        Me.zlbl_78.Name = "zlbl_78"
        Me.zlbl_78.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_78.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_78.TabIndex = 33
        Me.zlbl_78.Text = "m"
        Me.zlbl_78.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_82
        '
        Me.zlbl_82.AutoSize = True
        Me.zlbl_82.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_82.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_82.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_82.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_82.Location = New System.Drawing.Point(7, 86)
        Me.zlbl_82.Name = "zlbl_82"
        Me.zlbl_82.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_82.Size = New System.Drawing.Size(24, 16)
        Me.zlbl_82.TabIndex = 28
        Me.zlbl_82.Text = "Gz"
        Me.zlbl_82.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'mass_txtGz
        '
        Me.mass_txtGz.AcceptsReturn = True
        Me.mass_txtGz.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtGz.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtGz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtGz.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtGz.Location = New System.Drawing.Point(37, 83)
        Me.mass_txtGz.MaxLength = 0
        Me.mass_txtGz.Name = "mass_txtGz"
        Me.mass_txtGz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtGz.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtGz.TabIndex = 32
        Me.mass_txtGz.Text = "0"
        '
        'zlbl_79
        '
        Me.zlbl_79.AutoSize = True
        Me.zlbl_79.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_79.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_79.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_79.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_79.Location = New System.Drawing.Point(92, 57)
        Me.zlbl_79.Name = "zlbl_79"
        Me.zlbl_79.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_79.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_79.TabIndex = 34
        Me.zlbl_79.Text = "m"
        Me.zlbl_79.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_80
        '
        Me.zlbl_80.AutoSize = True
        Me.zlbl_80.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_80.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_80.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_80.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_80.Location = New System.Drawing.Point(92, 28)
        Me.zlbl_80.Name = "zlbl_80"
        Me.zlbl_80.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_80.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_80.TabIndex = 35
        Me.zlbl_80.Text = "m"
        Me.zlbl_80.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zfra_6
        '
        Me.zfra_6.Controls.Add(Me.mass_txtIyz)
        Me.zfra_6.Controls.Add(Me.zlbl_85)
        Me.zfra_6.Controls.Add(Me.zlbl_86)
        Me.zfra_6.Controls.Add(Me.zlbl_14)
        Me.zfra_6.Controls.Add(Me.mass_txtIyy)
        Me.zfra_6.Controls.Add(Me.zlbl_13)
        Me.zfra_6.Controls.Add(Me.zlbl_84)
        Me.zfra_6.Controls.Add(Me.zlbl_12)
        Me.zfra_6.Controls.Add(Me.mass_txtIzz)
        Me.zfra_6.Controls.Add(Me.mass_txtIxz)
        Me.zfra_6.Controls.Add(Me.mass_txtIxx)
        Me.zfra_6.Controls.Add(Me.mass_txtIxy)
        Me.zfra_6.Controls.Add(Me.zlbl_87)
        Me.zfra_6.Controls.Add(Me.zlbl_10)
        Me.zfra_6.Controls.Add(Me.zlbl_88)
        Me.zfra_6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_6.ForeColor = System.Drawing.Color.Black
        Me.zfra_6.Location = New System.Drawing.Point(324, 55)
        Me.zfra_6.Name = "zfra_6"
        Me.zfra_6.Size = New System.Drawing.Size(255, 117)
        Me.zfra_6.TabIndex = 36
        Me.zfra_6.TabStop = False
        Me.zfra_6.Text = "Inertias"
        '
        'mass_txtIyz
        '
        Me.mass_txtIyz.AcceptsReturn = True
        Me.mass_txtIyz.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtIyz.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtIyz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtIyz.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtIyz.Location = New System.Drawing.Point(139, 26)
        Me.mass_txtIyz.MaxLength = 0
        Me.mass_txtIyz.Name = "mass_txtIyz"
        Me.mass_txtIyz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtIyz.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtIyz.TabIndex = 30
        '
        'zlbl_85
        '
        Me.zlbl_85.AutoSize = True
        Me.zlbl_85.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_85.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_85.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_85.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_85.Location = New System.Drawing.Point(9, 58)
        Me.zlbl_85.Name = "zlbl_85"
        Me.zlbl_85.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_85.Size = New System.Drawing.Size(29, 16)
        Me.zlbl_85.TabIndex = 5
        Me.zlbl_85.Text = "Iyy"
        Me.zlbl_85.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_86
        '
        Me.zlbl_86.AutoSize = True
        Me.zlbl_86.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_86.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_86.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_86.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_86.Location = New System.Drawing.Point(9, 87)
        Me.zlbl_86.Name = "zlbl_86"
        Me.zlbl_86.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_86.Size = New System.Drawing.Size(27, 16)
        Me.zlbl_86.TabIndex = 6
        Me.zlbl_86.Text = "Izz"
        Me.zlbl_86.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_14
        '
        Me.zlbl_14.AutoSize = True
        Me.zlbl_14.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_14.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_14.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_14.Location = New System.Drawing.Point(196, 87)
        Me.zlbl_14.Name = "zlbl_14"
        Me.zlbl_14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_14.Size = New System.Drawing.Size(46, 16)
        Me.zlbl_14.TabIndex = 33
        Me.zlbl_14.Text = "kg.m²"
        Me.zlbl_14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_txtIyy
        '
        Me.mass_txtIyy.AcceptsReturn = True
        Me.mass_txtIyy.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtIyy.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtIyy.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtIyy.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtIyy.Location = New System.Drawing.Point(36, 54)
        Me.mass_txtIyy.MaxLength = 0
        Me.mass_txtIyy.Name = "mass_txtIyy"
        Me.mass_txtIyy.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtIyy.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtIyy.TabIndex = 7
        Me.mass_txtIyy.Text = "1"
        '
        'zlbl_13
        '
        Me.zlbl_13.AutoSize = True
        Me.zlbl_13.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_13.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_13.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_13.Location = New System.Drawing.Point(196, 58)
        Me.zlbl_13.Name = "zlbl_13"
        Me.zlbl_13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_13.Size = New System.Drawing.Size(46, 16)
        Me.zlbl_13.TabIndex = 34
        Me.zlbl_13.Text = "kg.m²"
        Me.zlbl_13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_84
        '
        Me.zlbl_84.AutoSize = True
        Me.zlbl_84.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_84.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_84.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_84.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_84.Location = New System.Drawing.Point(8, 29)
        Me.zlbl_84.Name = "zlbl_84"
        Me.zlbl_84.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_84.Size = New System.Drawing.Size(27, 16)
        Me.zlbl_84.TabIndex = 6
        Me.zlbl_84.Text = "Ixx"
        Me.zlbl_84.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_12
        '
        Me.zlbl_12.AutoSize = True
        Me.zlbl_12.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_12.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_12.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_12.Location = New System.Drawing.Point(196, 29)
        Me.zlbl_12.Name = "zlbl_12"
        Me.zlbl_12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_12.Size = New System.Drawing.Size(46, 16)
        Me.zlbl_12.TabIndex = 35
        Me.zlbl_12.Text = "kg.m²"
        Me.zlbl_12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mass_txtIzz
        '
        Me.mass_txtIzz.AcceptsReturn = True
        Me.mass_txtIzz.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtIzz.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtIzz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtIzz.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtIzz.Location = New System.Drawing.Point(36, 83)
        Me.mass_txtIzz.MaxLength = 0
        Me.mass_txtIzz.Name = "mass_txtIzz"
        Me.mass_txtIzz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtIzz.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtIzz.TabIndex = 8
        Me.mass_txtIzz.Text = "1"
        '
        'mass_txtIxz
        '
        Me.mass_txtIxz.AcceptsReturn = True
        Me.mass_txtIxz.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtIxz.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtIxz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtIxz.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtIxz.Location = New System.Drawing.Point(139, 84)
        Me.mass_txtIxz.MaxLength = 0
        Me.mass_txtIxz.Name = "mass_txtIxz"
        Me.mass_txtIxz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtIxz.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtIxz.TabIndex = 32
        Me.mass_txtIxz.Text = "0"
        '
        'mass_txtIxx
        '
        Me.mass_txtIxx.AcceptsReturn = True
        Me.mass_txtIxx.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtIxx.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtIxx.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtIxx.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtIxx.Location = New System.Drawing.Point(36, 25)
        Me.mass_txtIxx.MaxLength = 0
        Me.mass_txtIxx.Name = "mass_txtIxx"
        Me.mass_txtIxx.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtIxx.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtIxx.TabIndex = 8
        Me.mass_txtIxx.Text = "1"
        '
        'mass_txtIxy
        '
        Me.mass_txtIxy.AcceptsReturn = True
        Me.mass_txtIxy.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtIxy.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtIxy.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtIxy.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtIxy.Location = New System.Drawing.Point(139, 55)
        Me.mass_txtIxy.MaxLength = 0
        Me.mass_txtIxy.Name = "mass_txtIxy"
        Me.mass_txtIxy.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtIxy.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtIxy.TabIndex = 31
        '
        'zlbl_87
        '
        Me.zlbl_87.AutoSize = True
        Me.zlbl_87.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_87.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_87.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_87.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_87.Location = New System.Drawing.Point(109, 29)
        Me.zlbl_87.Name = "zlbl_87"
        Me.zlbl_87.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_87.Size = New System.Drawing.Size(28, 16)
        Me.zlbl_87.TabIndex = 27
        Me.zlbl_87.Text = "Iyz"
        Me.zlbl_87.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_10
        '
        Me.zlbl_10.AutoSize = True
        Me.zlbl_10.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_10.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_10.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_10.Location = New System.Drawing.Point(109, 87)
        Me.zlbl_10.Name = "zlbl_10"
        Me.zlbl_10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_10.Size = New System.Drawing.Size(27, 16)
        Me.zlbl_10.TabIndex = 28
        Me.zlbl_10.Text = "Ixz"
        Me.zlbl_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_88
        '
        Me.zlbl_88.AutoSize = True
        Me.zlbl_88.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_88.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_88.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_88.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_88.Location = New System.Drawing.Point(109, 58)
        Me.zlbl_88.Name = "zlbl_88"
        Me.zlbl_88.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_88.Size = New System.Drawing.Size(28, 16)
        Me.zlbl_88.TabIndex = 29
        Me.zlbl_88.Text = "Ixy"
        Me.zlbl_88.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_5
        '
        Me.zfra_5.Controls.Add(Me.zlbl_0)
        Me.zfra_5.Controls.Add(Me.zlbl_2)
        Me.zfra_5.Controls.Add(Me.mass_txtBodyWidth)
        Me.zfra_5.Controls.Add(Me.mass_txtBodyHeigth)
        Me.zfra_5.Controls.Add(Me.zlbl_65)
        Me.zfra_5.Controls.Add(Me.zlbl_66)
        Me.zfra_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_5.ForeColor = System.Drawing.Color.Black
        Me.zfra_5.Location = New System.Drawing.Point(14, 55)
        Me.zfra_5.Name = "zfra_5"
        Me.zfra_5.Size = New System.Drawing.Size(143, 117)
        Me.zfra_5.TabIndex = 36
        Me.zfra_5.TabStop = False
        Me.zfra_5.Text = "Size"
        '
        'zlbl_0
        '
        Me.zlbl_0.AutoSize = True
        Me.zlbl_0.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_0.Location = New System.Drawing.Point(13, 40)
        Me.zlbl_0.Name = "zlbl_0"
        Me.zlbl_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_0.Size = New System.Drawing.Size(46, 16)
        Me.zlbl_0.TabIndex = 5
        Me.zlbl_0.Text = "Width"
        Me.zlbl_0.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_2
        '
        Me.zlbl_2.AutoSize = True
        Me.zlbl_2.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_2.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_2.Location = New System.Drawing.Point(9, 69)
        Me.zlbl_2.Name = "zlbl_2"
        Me.zlbl_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_2.Size = New System.Drawing.Size(50, 16)
        Me.zlbl_2.TabIndex = 6
        Me.zlbl_2.Text = "Height"
        Me.zlbl_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'mass_txtBodyWidth
        '
        Me.mass_txtBodyWidth.AcceptsReturn = True
        Me.mass_txtBodyWidth.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtBodyWidth.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtBodyWidth.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtBodyWidth.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtBodyWidth.Location = New System.Drawing.Point(59, 37)
        Me.mass_txtBodyWidth.MaxLength = 0
        Me.mass_txtBodyWidth.Name = "mass_txtBodyWidth"
        Me.mass_txtBodyWidth.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtBodyWidth.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtBodyWidth.TabIndex = 7
        Me.mass_txtBodyWidth.Text = "1"
        '
        'mass_txtBodyHeigth
        '
        Me.mass_txtBodyHeigth.AcceptsReturn = True
        Me.mass_txtBodyHeigth.BackColor = System.Drawing.SystemColors.Window
        Me.mass_txtBodyHeigth.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.mass_txtBodyHeigth.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_txtBodyHeigth.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mass_txtBodyHeigth.Location = New System.Drawing.Point(59, 66)
        Me.mass_txtBodyHeigth.MaxLength = 0
        Me.mass_txtBodyHeigth.Name = "mass_txtBodyHeigth"
        Me.mass_txtBodyHeigth.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_txtBodyHeigth.Size = New System.Drawing.Size(51, 23)
        Me.mass_txtBodyHeigth.TabIndex = 8
        Me.mass_txtBodyHeigth.Text = "1"
        '
        'zlbl_65
        '
        Me.zlbl_65.AutoSize = True
        Me.zlbl_65.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_65.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_65.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_65.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_65.Location = New System.Drawing.Point(112, 40)
        Me.zlbl_65.Name = "zlbl_65"
        Me.zlbl_65.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_65.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_65.TabIndex = 26
        Me.zlbl_65.Text = "m"
        Me.zlbl_65.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_66
        '
        Me.zlbl_66.AutoSize = True
        Me.zlbl_66.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_66.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_66.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_66.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_66.Location = New System.Drawing.Point(112, 69)
        Me.zlbl_66.Name = "zlbl_66"
        Me.zlbl_66.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_66.Size = New System.Drawing.Size(19, 16)
        Me.zlbl_66.TabIndex = 26
        Me.zlbl_66.Text = "m"
        Me.zlbl_66.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Onglet_att_prop
        '
        Me.Onglet_att_prop.BackColor = System.Drawing.Color.Transparent
        Me.Onglet_att_prop.Controls.Add(Me.zfra_14)
        Me.Onglet_att_prop.Controls.Add(Me.zfra_11)
        Me.Onglet_att_prop.Controls.Add(Me.zfra_1)
        Me.Onglet_att_prop.Location = New System.Drawing.Point(4, 32)
        Me.Onglet_att_prop.Name = "Onglet_att_prop"
        Me.Onglet_att_prop.Size = New System.Drawing.Size(627, 520)
        Me.Onglet_att_prop.TabIndex = 1
        Me.Onglet_att_prop.Text = "Satellite properties"
        Me.Onglet_att_prop.UseVisualStyleBackColor = True
        '
        'zfra_14
        '
        Me.zfra_14.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_14.Controls.Add(Me.prop_txtMG_X)
        Me.zfra_14.Controls.Add(Me.prop_txtMG_Y)
        Me.zfra_14.Controls.Add(Me.zlbl34)
        Me.zfra_14.Controls.Add(Me.zlbl31)
        Me.zfra_14.Controls.Add(Me.zlbl35)
        Me.zfra_14.Controls.Add(Me.prop_txtMG_Z)
        Me.zfra_14.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_14.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_14.Location = New System.Drawing.Point(16, 314)
        Me.zfra_14.Name = "zfra_14"
        Me.zfra_14.Size = New System.Drawing.Size(156, 133)
        Me.zfra_14.TabIndex = 37
        Me.zfra_14.TabStop = False
        Me.zfra_14.Text = "Magnetic dipole"
        '
        'prop_txtMG_X
        '
        Me.prop_txtMG_X.AcceptsReturn = True
        Me.prop_txtMG_X.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtMG_X.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtMG_X.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtMG_X.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtMG_X.Location = New System.Drawing.Point(55, 26)
        Me.prop_txtMG_X.MaxLength = 0
        Me.prop_txtMG_X.Name = "prop_txtMG_X"
        Me.prop_txtMG_X.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtMG_X.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtMG_X.TabIndex = 8
        Me.prop_txtMG_X.Text = "1"
        '
        'prop_txtMG_Y
        '
        Me.prop_txtMG_Y.AcceptsReturn = True
        Me.prop_txtMG_Y.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtMG_Y.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtMG_Y.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtMG_Y.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtMG_Y.Location = New System.Drawing.Point(55, 63)
        Me.prop_txtMG_Y.MaxLength = 0
        Me.prop_txtMG_Y.Name = "prop_txtMG_Y"
        Me.prop_txtMG_Y.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtMG_Y.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtMG_Y.TabIndex = 8
        Me.prop_txtMG_Y.Text = "1"
        '
        'zlbl34
        '
        Me.zlbl34.AutoSize = True
        Me.zlbl34.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl34.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl34.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl34.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl34.Location = New System.Drawing.Point(22, 66)
        Me.zlbl34.Name = "zlbl34"
        Me.zlbl34.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl34.Size = New System.Drawing.Size(26, 16)
        Me.zlbl34.TabIndex = 6
        Me.zlbl34.Text = "+Y"
        Me.zlbl34.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl31
        '
        Me.zlbl31.AutoSize = True
        Me.zlbl31.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl31.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl31.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl31.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl31.Location = New System.Drawing.Point(25, 102)
        Me.zlbl31.Name = "zlbl31"
        Me.zlbl31.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl31.Size = New System.Drawing.Size(26, 16)
        Me.zlbl31.TabIndex = 29
        Me.zlbl31.Text = "+Z"
        Me.zlbl31.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl35
        '
        Me.zlbl35.AutoSize = True
        Me.zlbl35.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl35.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl35.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl35.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl35.Location = New System.Drawing.Point(22, 29)
        Me.zlbl35.Name = "zlbl35"
        Me.zlbl35.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl35.Size = New System.Drawing.Size(26, 16)
        Me.zlbl35.TabIndex = 6
        Me.zlbl35.Text = "+X"
        Me.zlbl35.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtMG_Z
        '
        Me.prop_txtMG_Z.AcceptsReturn = True
        Me.prop_txtMG_Z.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtMG_Z.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtMG_Z.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtMG_Z.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtMG_Z.Location = New System.Drawing.Point(55, 99)
        Me.prop_txtMG_Z.MaxLength = 0
        Me.prop_txtMG_Z.Name = "prop_txtMG_Z"
        Me.prop_txtMG_Z.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtMG_Z.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtMG_Z.TabIndex = 31
        Me.prop_txtMG_Z.Text = "1"
        '
        'zfra_11
        '
        Me.zfra_11.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_11.Controls.Add(Me.zfra_12)
        Me.zfra_11.Controls.Add(Me.zfra_13)
        Me.zfra_11.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_11.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_11.Location = New System.Drawing.Point(323, 18)
        Me.zfra_11.Name = "zfra_11"
        Me.zfra_11.Size = New System.Drawing.Size(288, 268)
        Me.zfra_11.TabIndex = 37
        Me.zfra_11.TabStop = False
        Me.zfra_11.Text = "Sun radiation coefficients"
        '
        'zfra_12
        '
        Me.zfra_12.Controls.Add(Me.prop_txtCD_0)
        Me.zfra_12.Controls.Add(Me.prop_txtCD_4)
        Me.zfra_12.Controls.Add(Me.zlbl24)
        Me.zfra_12.Controls.Add(Me.prop_txtCD_3)
        Me.zfra_12.Controls.Add(Me.prop_txtCD_2)
        Me.zfra_12.Controls.Add(Me.zlbl23)
        Me.zfra_12.Controls.Add(Me.prop_txtCD_5)
        Me.zfra_12.Controls.Add(Me.prop_txtCD_1)
        Me.zfra_12.Controls.Add(Me.zlbl22)
        Me.zfra_12.Controls.Add(Me.zlbl19)
        Me.zfra_12.Controls.Add(Me.zlbl20)
        Me.zfra_12.Controls.Add(Me.zlbl21)
        Me.zfra_12.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_12.ForeColor = System.Drawing.Color.Black
        Me.zfra_12.Location = New System.Drawing.Point(151, 34)
        Me.zfra_12.Name = "zfra_12"
        Me.zfra_12.Size = New System.Drawing.Size(119, 214)
        Me.zfra_12.TabIndex = 36
        Me.zfra_12.TabStop = False
        Me.zfra_12.Text = "Diffuse"
        '
        'prop_txtCD_0
        '
        Me.prop_txtCD_0.AcceptsReturn = True
        Me.prop_txtCD_0.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCD_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCD_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCD_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCD_0.Location = New System.Drawing.Point(52, 28)
        Me.prop_txtCD_0.MaxLength = 0
        Me.prop_txtCD_0.Name = "prop_txtCD_0"
        Me.prop_txtCD_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCD_0.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCD_0.TabIndex = 8
        Me.prop_txtCD_0.Text = "1"
        '
        'prop_txtCD_4
        '
        Me.prop_txtCD_4.AcceptsReturn = True
        Me.prop_txtCD_4.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCD_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCD_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCD_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCD_4.Location = New System.Drawing.Point(50, 115)
        Me.prop_txtCD_4.MaxLength = 0
        Me.prop_txtCD_4.Name = "prop_txtCD_4"
        Me.prop_txtCD_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCD_4.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCD_4.TabIndex = 30
        Me.prop_txtCD_4.Text = "1"
        '
        'zlbl24
        '
        Me.zlbl24.AutoSize = True
        Me.zlbl24.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl24.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl24.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl24.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl24.Location = New System.Drawing.Point(20, 147)
        Me.zlbl24.Name = "zlbl24"
        Me.zlbl24.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl24.Size = New System.Drawing.Size(26, 16)
        Me.zlbl24.TabIndex = 29
        Me.zlbl24.Text = "+Z"
        Me.zlbl24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCD_3
        '
        Me.prop_txtCD_3.AcceptsReturn = True
        Me.prop_txtCD_3.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCD_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCD_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCD_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCD_3.Location = New System.Drawing.Point(52, 57)
        Me.prop_txtCD_3.MaxLength = 0
        Me.prop_txtCD_3.Name = "prop_txtCD_3"
        Me.prop_txtCD_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCD_3.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCD_3.TabIndex = 7
        Me.prop_txtCD_3.Text = "1"
        '
        'prop_txtCD_2
        '
        Me.prop_txtCD_2.AcceptsReturn = True
        Me.prop_txtCD_2.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCD_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCD_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCD_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCD_2.Location = New System.Drawing.Point(50, 144)
        Me.prop_txtCD_2.MaxLength = 0
        Me.prop_txtCD_2.Name = "prop_txtCD_2"
        Me.prop_txtCD_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCD_2.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCD_2.TabIndex = 31
        Me.prop_txtCD_2.Text = "1"
        '
        'zlbl23
        '
        Me.zlbl23.AutoSize = True
        Me.zlbl23.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl23.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl23.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl23.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl23.Location = New System.Drawing.Point(19, 60)
        Me.zlbl23.Name = "zlbl23"
        Me.zlbl23.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl23.Size = New System.Drawing.Size(24, 16)
        Me.zlbl23.TabIndex = 5
        Me.zlbl23.Text = "-X"
        Me.zlbl23.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCD_5
        '
        Me.prop_txtCD_5.AcceptsReturn = True
        Me.prop_txtCD_5.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCD_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCD_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCD_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCD_5.Location = New System.Drawing.Point(50, 173)
        Me.prop_txtCD_5.MaxLength = 0
        Me.prop_txtCD_5.Name = "prop_txtCD_5"
        Me.prop_txtCD_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCD_5.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCD_5.TabIndex = 32
        Me.prop_txtCD_5.Text = "1"
        '
        'prop_txtCD_1
        '
        Me.prop_txtCD_1.AcceptsReturn = True
        Me.prop_txtCD_1.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCD_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCD_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCD_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCD_1.Location = New System.Drawing.Point(52, 86)
        Me.prop_txtCD_1.MaxLength = 0
        Me.prop_txtCD_1.Name = "prop_txtCD_1"
        Me.prop_txtCD_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCD_1.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCD_1.TabIndex = 8
        Me.prop_txtCD_1.Text = "1"
        '
        'zlbl22
        '
        Me.zlbl22.AutoSize = True
        Me.zlbl22.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl22.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl22.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl22.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl22.Location = New System.Drawing.Point(20, 176)
        Me.zlbl22.Name = "zlbl22"
        Me.zlbl22.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl22.Size = New System.Drawing.Size(24, 16)
        Me.zlbl22.TabIndex = 28
        Me.zlbl22.Text = "-Z"
        Me.zlbl22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl19
        '
        Me.zlbl19.AutoSize = True
        Me.zlbl19.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl19.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl19.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl19.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl19.Location = New System.Drawing.Point(20, 118)
        Me.zlbl19.Name = "zlbl19"
        Me.zlbl19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl19.Size = New System.Drawing.Size(24, 16)
        Me.zlbl19.TabIndex = 27
        Me.zlbl19.Text = "-Y"
        Me.zlbl19.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl20
        '
        Me.zlbl20.AutoSize = True
        Me.zlbl20.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl20.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl20.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl20.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl20.Location = New System.Drawing.Point(18, 31)
        Me.zlbl20.Name = "zlbl20"
        Me.zlbl20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl20.Size = New System.Drawing.Size(26, 16)
        Me.zlbl20.TabIndex = 6
        Me.zlbl20.Text = "+X"
        Me.zlbl20.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl21
        '
        Me.zlbl21.AutoSize = True
        Me.zlbl21.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl21.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl21.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl21.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl21.Location = New System.Drawing.Point(19, 89)
        Me.zlbl21.Name = "zlbl21"
        Me.zlbl21.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl21.Size = New System.Drawing.Size(26, 16)
        Me.zlbl21.TabIndex = 6
        Me.zlbl21.Text = "+Y"
        Me.zlbl21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_13
        '
        Me.zfra_13.Controls.Add(Me.prop_txtCR_0)
        Me.zfra_13.Controls.Add(Me.prop_txtCR_4)
        Me.zfra_13.Controls.Add(Me.zlbl18)
        Me.zfra_13.Controls.Add(Me.prop_txtCR_3)
        Me.zfra_13.Controls.Add(Me.zlbl17)
        Me.zfra_13.Controls.Add(Me.prop_txtCR_2)
        Me.zfra_13.Controls.Add(Me.prop_txtCR_5)
        Me.zfra_13.Controls.Add(Me.zlbl16)
        Me.zfra_13.Controls.Add(Me.prop_txtCR_1)
        Me.zfra_13.Controls.Add(Me.zlbl15)
        Me.zfra_13.Controls.Add(Me.zlbl13)
        Me.zfra_13.Controls.Add(Me.zlbl14)
        Me.zfra_13.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_13.ForeColor = System.Drawing.Color.Black
        Me.zfra_13.Location = New System.Drawing.Point(16, 34)
        Me.zfra_13.Name = "zfra_13"
        Me.zfra_13.Size = New System.Drawing.Size(119, 214)
        Me.zfra_13.TabIndex = 36
        Me.zfra_13.TabStop = False
        Me.zfra_13.Text = "Reflective"
        '
        'prop_txtCR_0
        '
        Me.prop_txtCR_0.AcceptsReturn = True
        Me.prop_txtCR_0.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCR_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCR_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCR_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCR_0.Location = New System.Drawing.Point(52, 28)
        Me.prop_txtCR_0.MaxLength = 0
        Me.prop_txtCR_0.Name = "prop_txtCR_0"
        Me.prop_txtCR_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCR_0.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCR_0.TabIndex = 8
        Me.prop_txtCR_0.Text = "1"
        '
        'prop_txtCR_4
        '
        Me.prop_txtCR_4.AcceptsReturn = True
        Me.prop_txtCR_4.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCR_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCR_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCR_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCR_4.Location = New System.Drawing.Point(50, 115)
        Me.prop_txtCR_4.MaxLength = 0
        Me.prop_txtCR_4.Name = "prop_txtCR_4"
        Me.prop_txtCR_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCR_4.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCR_4.TabIndex = 30
        Me.prop_txtCR_4.Text = "1"
        '
        'zlbl18
        '
        Me.zlbl18.AutoSize = True
        Me.zlbl18.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl18.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl18.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl18.Location = New System.Drawing.Point(20, 147)
        Me.zlbl18.Name = "zlbl18"
        Me.zlbl18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl18.Size = New System.Drawing.Size(26, 16)
        Me.zlbl18.TabIndex = 29
        Me.zlbl18.Text = "+Z"
        Me.zlbl18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCR_3
        '
        Me.prop_txtCR_3.AcceptsReturn = True
        Me.prop_txtCR_3.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCR_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCR_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCR_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCR_3.Location = New System.Drawing.Point(52, 57)
        Me.prop_txtCR_3.MaxLength = 0
        Me.prop_txtCR_3.Name = "prop_txtCR_3"
        Me.prop_txtCR_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCR_3.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCR_3.TabIndex = 7
        Me.prop_txtCR_3.Text = "1"
        '
        'zlbl17
        '
        Me.zlbl17.AutoSize = True
        Me.zlbl17.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl17.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl17.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl17.Location = New System.Drawing.Point(19, 60)
        Me.zlbl17.Name = "zlbl17"
        Me.zlbl17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl17.Size = New System.Drawing.Size(24, 16)
        Me.zlbl17.TabIndex = 5
        Me.zlbl17.Text = "-X"
        Me.zlbl17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCR_2
        '
        Me.prop_txtCR_2.AcceptsReturn = True
        Me.prop_txtCR_2.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCR_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCR_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCR_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCR_2.Location = New System.Drawing.Point(50, 144)
        Me.prop_txtCR_2.MaxLength = 0
        Me.prop_txtCR_2.Name = "prop_txtCR_2"
        Me.prop_txtCR_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCR_2.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCR_2.TabIndex = 31
        Me.prop_txtCR_2.Text = "1"
        '
        'prop_txtCR_5
        '
        Me.prop_txtCR_5.AcceptsReturn = True
        Me.prop_txtCR_5.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCR_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCR_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCR_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCR_5.Location = New System.Drawing.Point(50, 173)
        Me.prop_txtCR_5.MaxLength = 0
        Me.prop_txtCR_5.Name = "prop_txtCR_5"
        Me.prop_txtCR_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCR_5.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCR_5.TabIndex = 32
        Me.prop_txtCR_5.Text = "1"
        '
        'zlbl16
        '
        Me.zlbl16.AutoSize = True
        Me.zlbl16.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl16.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl16.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl16.Location = New System.Drawing.Point(20, 176)
        Me.zlbl16.Name = "zlbl16"
        Me.zlbl16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl16.Size = New System.Drawing.Size(24, 16)
        Me.zlbl16.TabIndex = 28
        Me.zlbl16.Text = "-Z"
        Me.zlbl16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCR_1
        '
        Me.prop_txtCR_1.AcceptsReturn = True
        Me.prop_txtCR_1.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCR_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCR_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCR_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCR_1.Location = New System.Drawing.Point(52, 86)
        Me.prop_txtCR_1.MaxLength = 0
        Me.prop_txtCR_1.Name = "prop_txtCR_1"
        Me.prop_txtCR_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCR_1.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCR_1.TabIndex = 8
        Me.prop_txtCR_1.Text = "1"
        '
        'zlbl15
        '
        Me.zlbl15.AutoSize = True
        Me.zlbl15.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl15.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl15.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl15.Location = New System.Drawing.Point(19, 89)
        Me.zlbl15.Name = "zlbl15"
        Me.zlbl15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl15.Size = New System.Drawing.Size(26, 16)
        Me.zlbl15.TabIndex = 6
        Me.zlbl15.Text = "+Y"
        Me.zlbl15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl13
        '
        Me.zlbl13.AutoSize = True
        Me.zlbl13.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl13.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl13.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl13.Location = New System.Drawing.Point(18, 31)
        Me.zlbl13.Name = "zlbl13"
        Me.zlbl13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl13.Size = New System.Drawing.Size(26, 16)
        Me.zlbl13.TabIndex = 6
        Me.zlbl13.Text = "+X"
        Me.zlbl13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl14
        '
        Me.zlbl14.AutoSize = True
        Me.zlbl14.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl14.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl14.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl14.Location = New System.Drawing.Point(20, 118)
        Me.zlbl14.Name = "zlbl14"
        Me.zlbl14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl14.Size = New System.Drawing.Size(24, 16)
        Me.zlbl14.TabIndex = 27
        Me.zlbl14.Text = "-Y"
        Me.zlbl14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_1
        '
        Me.zfra_1.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_1.Controls.Add(Me.zfra_10)
        Me.zfra_1.Controls.Add(Me.zfra_2)
        Me.zfra_1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_1.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_1.Location = New System.Drawing.Point(16, 18)
        Me.zfra_1.Name = "zfra_1"
        Me.zfra_1.Size = New System.Drawing.Size(288, 268)
        Me.zfra_1.TabIndex = 37
        Me.zfra_1.TabStop = False
        Me.zfra_1.Text = "Pressure coefficients"
        '
        'zfra_10
        '
        Me.zfra_10.Controls.Add(Me.prop_txtCT_0)
        Me.zfra_10.Controls.Add(Me.prop_txtCT_4)
        Me.zfra_10.Controls.Add(Me.zlbl12)
        Me.zfra_10.Controls.Add(Me.zlbl11)
        Me.zfra_10.Controls.Add(Me.prop_txtCT_3)
        Me.zfra_10.Controls.Add(Me.zlbl10)
        Me.zfra_10.Controls.Add(Me.prop_txtCT_2)
        Me.zfra_10.Controls.Add(Me.zlbl6)
        Me.zfra_10.Controls.Add(Me.prop_txtCT_5)
        Me.zfra_10.Controls.Add(Me.zlbl4)
        Me.zfra_10.Controls.Add(Me.prop_txtCT_1)
        Me.zfra_10.Controls.Add(Me.zlbl3)
        Me.zfra_10.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_10.ForeColor = System.Drawing.Color.Black
        Me.zfra_10.Location = New System.Drawing.Point(151, 34)
        Me.zfra_10.Name = "zfra_10"
        Me.zfra_10.Size = New System.Drawing.Size(119, 214)
        Me.zfra_10.TabIndex = 36
        Me.zfra_10.TabStop = False
        Me.zfra_10.Text = "Tangential"
        '
        'prop_txtCT_0
        '
        Me.prop_txtCT_0.AcceptsReturn = True
        Me.prop_txtCT_0.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCT_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCT_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCT_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCT_0.Location = New System.Drawing.Point(52, 28)
        Me.prop_txtCT_0.MaxLength = 0
        Me.prop_txtCT_0.Name = "prop_txtCT_0"
        Me.prop_txtCT_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCT_0.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCT_0.TabIndex = 8
        Me.prop_txtCT_0.Text = "1"
        '
        'prop_txtCT_4
        '
        Me.prop_txtCT_4.AcceptsReturn = True
        Me.prop_txtCT_4.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCT_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCT_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCT_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCT_4.Location = New System.Drawing.Point(50, 115)
        Me.prop_txtCT_4.MaxLength = 0
        Me.prop_txtCT_4.Name = "prop_txtCT_4"
        Me.prop_txtCT_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCT_4.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCT_4.TabIndex = 30
        Me.prop_txtCT_4.Text = "1"
        '
        'zlbl12
        '
        Me.zlbl12.AutoSize = True
        Me.zlbl12.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl12.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl12.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl12.Location = New System.Drawing.Point(18, 147)
        Me.zlbl12.Name = "zlbl12"
        Me.zlbl12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl12.Size = New System.Drawing.Size(26, 16)
        Me.zlbl12.TabIndex = 29
        Me.zlbl12.Text = "+Z"
        Me.zlbl12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl11
        '
        Me.zlbl11.AutoSize = True
        Me.zlbl11.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl11.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl11.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl11.Location = New System.Drawing.Point(17, 60)
        Me.zlbl11.Name = "zlbl11"
        Me.zlbl11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl11.Size = New System.Drawing.Size(24, 16)
        Me.zlbl11.TabIndex = 5
        Me.zlbl11.Text = "-X"
        Me.zlbl11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCT_3
        '
        Me.prop_txtCT_3.AcceptsReturn = True
        Me.prop_txtCT_3.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCT_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCT_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCT_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCT_3.Location = New System.Drawing.Point(52, 57)
        Me.prop_txtCT_3.MaxLength = 0
        Me.prop_txtCT_3.Name = "prop_txtCT_3"
        Me.prop_txtCT_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCT_3.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCT_3.TabIndex = 7
        Me.prop_txtCT_3.Text = "1"
        '
        'zlbl10
        '
        Me.zlbl10.AutoSize = True
        Me.zlbl10.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl10.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl10.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl10.Location = New System.Drawing.Point(18, 176)
        Me.zlbl10.Name = "zlbl10"
        Me.zlbl10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl10.Size = New System.Drawing.Size(24, 16)
        Me.zlbl10.TabIndex = 28
        Me.zlbl10.Text = "-Z"
        Me.zlbl10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCT_2
        '
        Me.prop_txtCT_2.AcceptsReturn = True
        Me.prop_txtCT_2.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCT_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCT_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCT_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCT_2.Location = New System.Drawing.Point(50, 144)
        Me.prop_txtCT_2.MaxLength = 0
        Me.prop_txtCT_2.Name = "prop_txtCT_2"
        Me.prop_txtCT_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCT_2.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCT_2.TabIndex = 31
        Me.prop_txtCT_2.Text = "1"
        '
        'zlbl6
        '
        Me.zlbl6.AutoSize = True
        Me.zlbl6.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl6.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl6.Location = New System.Drawing.Point(17, 89)
        Me.zlbl6.Name = "zlbl6"
        Me.zlbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl6.Size = New System.Drawing.Size(26, 16)
        Me.zlbl6.TabIndex = 6
        Me.zlbl6.Text = "+Y"
        Me.zlbl6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCT_5
        '
        Me.prop_txtCT_5.AcceptsReturn = True
        Me.prop_txtCT_5.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCT_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCT_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCT_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCT_5.Location = New System.Drawing.Point(50, 173)
        Me.prop_txtCT_5.MaxLength = 0
        Me.prop_txtCT_5.Name = "prop_txtCT_5"
        Me.prop_txtCT_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCT_5.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCT_5.TabIndex = 32
        Me.prop_txtCT_5.Text = "1"
        '
        'zlbl4
        '
        Me.zlbl4.AutoSize = True
        Me.zlbl4.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl4.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl4.Location = New System.Drawing.Point(18, 118)
        Me.zlbl4.Name = "zlbl4"
        Me.zlbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl4.Size = New System.Drawing.Size(24, 16)
        Me.zlbl4.TabIndex = 27
        Me.zlbl4.Text = "-Y"
        Me.zlbl4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCT_1
        '
        Me.prop_txtCT_1.AcceptsReturn = True
        Me.prop_txtCT_1.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCT_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCT_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCT_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCT_1.Location = New System.Drawing.Point(52, 86)
        Me.prop_txtCT_1.MaxLength = 0
        Me.prop_txtCT_1.Name = "prop_txtCT_1"
        Me.prop_txtCT_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCT_1.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCT_1.TabIndex = 8
        Me.prop_txtCT_1.Text = "1"
        '
        'zlbl3
        '
        Me.zlbl3.AutoSize = True
        Me.zlbl3.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl3.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl3.Location = New System.Drawing.Point(16, 31)
        Me.zlbl3.Name = "zlbl3"
        Me.zlbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl3.Size = New System.Drawing.Size(26, 16)
        Me.zlbl3.TabIndex = 6
        Me.zlbl3.Text = "+X"
        Me.zlbl3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_2
        '
        Me.zfra_2.Controls.Add(Me.prop_txtCN_0)
        Me.zfra_2.Controls.Add(Me.prop_txtCN_4)
        Me.zfra_2.Controls.Add(Me.zlbl9)
        Me.zfra_2.Controls.Add(Me.zlbl1)
        Me.zfra_2.Controls.Add(Me.zlbl8)
        Me.zfra_2.Controls.Add(Me.zlbl2)
        Me.zfra_2.Controls.Add(Me.zlbl7)
        Me.zfra_2.Controls.Add(Me.prop_txtCN_3)
        Me.zfra_2.Controls.Add(Me.prop_txtCN_2)
        Me.zfra_2.Controls.Add(Me.zlbl5)
        Me.zfra_2.Controls.Add(Me.prop_txtCN_5)
        Me.zfra_2.Controls.Add(Me.prop_txtCN_1)
        Me.zfra_2.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_2.ForeColor = System.Drawing.Color.Black
        Me.zfra_2.Location = New System.Drawing.Point(16, 34)
        Me.zfra_2.Name = "zfra_2"
        Me.zfra_2.Size = New System.Drawing.Size(119, 214)
        Me.zfra_2.TabIndex = 36
        Me.zfra_2.TabStop = False
        Me.zfra_2.Text = "Normal"
        '
        'prop_txtCN_0
        '
        Me.prop_txtCN_0.AcceptsReturn = True
        Me.prop_txtCN_0.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCN_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCN_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCN_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCN_0.Location = New System.Drawing.Point(52, 28)
        Me.prop_txtCN_0.MaxLength = 0
        Me.prop_txtCN_0.Name = "prop_txtCN_0"
        Me.prop_txtCN_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCN_0.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCN_0.TabIndex = 8
        Me.prop_txtCN_0.Text = "1"
        '
        'prop_txtCN_4
        '
        Me.prop_txtCN_4.AcceptsReturn = True
        Me.prop_txtCN_4.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCN_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCN_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCN_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCN_4.Location = New System.Drawing.Point(50, 115)
        Me.prop_txtCN_4.MaxLength = 0
        Me.prop_txtCN_4.Name = "prop_txtCN_4"
        Me.prop_txtCN_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCN_4.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCN_4.TabIndex = 30
        Me.prop_txtCN_4.Text = "1"
        '
        'zlbl9
        '
        Me.zlbl9.AutoSize = True
        Me.zlbl9.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl9.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl9.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl9.Location = New System.Drawing.Point(20, 147)
        Me.zlbl9.Name = "zlbl9"
        Me.zlbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl9.Size = New System.Drawing.Size(26, 16)
        Me.zlbl9.TabIndex = 29
        Me.zlbl9.Text = "+Z"
        Me.zlbl9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl1
        '
        Me.zlbl1.AutoSize = True
        Me.zlbl1.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl1.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl1.Location = New System.Drawing.Point(19, 60)
        Me.zlbl1.Name = "zlbl1"
        Me.zlbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl1.Size = New System.Drawing.Size(24, 16)
        Me.zlbl1.TabIndex = 5
        Me.zlbl1.Text = "-X"
        Me.zlbl1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl8
        '
        Me.zlbl8.AutoSize = True
        Me.zlbl8.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl8.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl8.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl8.Location = New System.Drawing.Point(20, 176)
        Me.zlbl8.Name = "zlbl8"
        Me.zlbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl8.Size = New System.Drawing.Size(24, 16)
        Me.zlbl8.TabIndex = 28
        Me.zlbl8.Text = "-Z"
        Me.zlbl8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl2
        '
        Me.zlbl2.AutoSize = True
        Me.zlbl2.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl2.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl2.Location = New System.Drawing.Point(19, 89)
        Me.zlbl2.Name = "zlbl2"
        Me.zlbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl2.Size = New System.Drawing.Size(26, 16)
        Me.zlbl2.TabIndex = 6
        Me.zlbl2.Text = "+Y"
        Me.zlbl2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl7
        '
        Me.zlbl7.AutoSize = True
        Me.zlbl7.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl7.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl7.Location = New System.Drawing.Point(20, 118)
        Me.zlbl7.Name = "zlbl7"
        Me.zlbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl7.Size = New System.Drawing.Size(24, 16)
        Me.zlbl7.TabIndex = 27
        Me.zlbl7.Text = "-Y"
        Me.zlbl7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCN_3
        '
        Me.prop_txtCN_3.AcceptsReturn = True
        Me.prop_txtCN_3.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCN_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCN_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCN_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCN_3.Location = New System.Drawing.Point(52, 57)
        Me.prop_txtCN_3.MaxLength = 0
        Me.prop_txtCN_3.Name = "prop_txtCN_3"
        Me.prop_txtCN_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCN_3.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCN_3.TabIndex = 7
        Me.prop_txtCN_3.Text = "1"
        '
        'prop_txtCN_2
        '
        Me.prop_txtCN_2.AcceptsReturn = True
        Me.prop_txtCN_2.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCN_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCN_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCN_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCN_2.Location = New System.Drawing.Point(50, 144)
        Me.prop_txtCN_2.MaxLength = 0
        Me.prop_txtCN_2.Name = "prop_txtCN_2"
        Me.prop_txtCN_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCN_2.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCN_2.TabIndex = 31
        Me.prop_txtCN_2.Text = "1"
        '
        'zlbl5
        '
        Me.zlbl5.AutoSize = True
        Me.zlbl5.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl5.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl5.Location = New System.Drawing.Point(18, 31)
        Me.zlbl5.Name = "zlbl5"
        Me.zlbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl5.Size = New System.Drawing.Size(26, 16)
        Me.zlbl5.TabIndex = 6
        Me.zlbl5.Text = "+X"
        Me.zlbl5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'prop_txtCN_5
        '
        Me.prop_txtCN_5.AcceptsReturn = True
        Me.prop_txtCN_5.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCN_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCN_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCN_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCN_5.Location = New System.Drawing.Point(50, 173)
        Me.prop_txtCN_5.MaxLength = 0
        Me.prop_txtCN_5.Name = "prop_txtCN_5"
        Me.prop_txtCN_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCN_5.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCN_5.TabIndex = 32
        Me.prop_txtCN_5.Text = "1"
        '
        'prop_txtCN_1
        '
        Me.prop_txtCN_1.AcceptsReturn = True
        Me.prop_txtCN_1.BackColor = System.Drawing.SystemColors.Window
        Me.prop_txtCN_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.prop_txtCN_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prop_txtCN_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.prop_txtCN_1.Location = New System.Drawing.Point(52, 86)
        Me.prop_txtCN_1.MaxLength = 0
        Me.prop_txtCN_1.Name = "prop_txtCN_1"
        Me.prop_txtCN_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.prop_txtCN_1.Size = New System.Drawing.Size(43, 23)
        Me.prop_txtCN_1.TabIndex = 8
        Me.prop_txtCN_1.Text = "1"
        '
        'Onglet_att_act
        '
        Me.Onglet_att_act.BackColor = System.Drawing.Color.Transparent
        Me.Onglet_att_act.Location = New System.Drawing.Point(4, 32)
        Me.Onglet_att_act.Name = "Onglet_att_act"
        Me.Onglet_att_act.Size = New System.Drawing.Size(627, 520)
        Me.Onglet_att_act.TabIndex = 5
        Me.Onglet_att_act.Text = "Actuactors"
        Me.Onglet_att_act.UseVisualStyleBackColor = True
        '
        'Onglet_att_manag
        '
        Me.Onglet_att_manag.BackColor = System.Drawing.Color.Transparent
        Me.Onglet_att_manag.Location = New System.Drawing.Point(4, 32)
        Me.Onglet_att_manag.Name = "Onglet_att_manag"
        Me.Onglet_att_manag.Size = New System.Drawing.Size(627, 520)
        Me.Onglet_att_manag.TabIndex = 4
        Me.Onglet_att_manag.Text = "Management"
        Me.Onglet_att_manag.UseVisualStyleBackColor = True
        '
        'zfra_9
        '
        Me.zfra_9.BackColor = System.Drawing.Color.LightGray
        Me.zfra_9.Controls.Add(Me.mass_lblSatMass)
        Me.zfra_9.Controls.Add(Me.zlbl30)
        Me.zfra_9.Controls.Add(Me.zlbl28)
        Me.zfra_9.Controls.Add(Me.zfra_15)
        Me.zfra_9.Controls.Add(Me.zfra_16)
        Me.zfra_9.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_9.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_9.Location = New System.Drawing.Point(678, 427)
        Me.zfra_9.Name = "zfra_9"
        Me.zfra_9.Size = New System.Drawing.Size(350, 152)
        Me.zfra_9.TabIndex = 38
        Me.zfra_9.TabStop = False
        Me.zfra_9.Text = "Global results"
        '
        'mass_lblSatMass
        '
        Me.mass_lblSatMass.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatMass.ForeColor = System.Drawing.Color.Black
        Me.mass_lblSatMass.Location = New System.Drawing.Point(157, 25)
        Me.mass_lblSatMass.Name = "mass_lblSatMass"
        Me.mass_lblSatMass.Size = New System.Drawing.Size(43, 17)
        Me.mass_lblSatMass.TabIndex = 41
        Me.mass_lblSatMass.Text = "0"
        '
        'zlbl30
        '
        Me.zlbl30.AutoSize = True
        Me.zlbl30.BackColor = System.Drawing.Color.Transparent
        Me.zlbl30.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl30.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl30.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl30.Location = New System.Drawing.Point(197, 25)
        Me.zlbl30.Name = "zlbl30"
        Me.zlbl30.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl30.Size = New System.Drawing.Size(23, 16)
        Me.zlbl30.TabIndex = 40
        Me.zlbl30.Text = "kg"
        Me.zlbl30.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl28
        '
        Me.zlbl28.AutoSize = True
        Me.zlbl28.BackColor = System.Drawing.Color.Transparent
        Me.zlbl28.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl28.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl28.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl28.Location = New System.Drawing.Point(110, 25)
        Me.zlbl28.Name = "zlbl28"
        Me.zlbl28.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl28.Size = New System.Drawing.Size(41, 16)
        Me.zlbl28.TabIndex = 40
        Me.zlbl28.Text = "Mass"
        Me.zlbl28.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_15
        '
        Me.zfra_15.Controls.Add(Me.mass_lblSatGz)
        Me.zfra_15.Controls.Add(Me.mass_lblSatGy)
        Me.zfra_15.Controls.Add(Me.mass_lblSatGx)
        Me.zfra_15.Controls.Add(Me.zlbl37)
        Me.zfra_15.Controls.Add(Me.zlbl38)
        Me.zfra_15.Controls.Add(Me.zlbl40)
        Me.zfra_15.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_15.ForeColor = System.Drawing.Color.Black
        Me.zfra_15.Location = New System.Drawing.Point(16, 49)
        Me.zfra_15.Name = "zfra_15"
        Me.zfra_15.Size = New System.Drawing.Size(105, 91)
        Me.zfra_15.TabIndex = 36
        Me.zfra_15.TabStop = False
        Me.zfra_15.Text = "C.G (m)"
        '
        'mass_lblSatGz
        '
        Me.mass_lblSatGz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatGz.Location = New System.Drawing.Point(45, 65)
        Me.mass_lblSatGz.Name = "mass_lblSatGz"
        Me.mass_lblSatGz.Size = New System.Drawing.Size(43, 16)
        Me.mass_lblSatGz.TabIndex = 39
        Me.mass_lblSatGz.Text = "0"
        Me.mass_lblSatGz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatGy
        '
        Me.mass_lblSatGy.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatGy.Location = New System.Drawing.Point(45, 45)
        Me.mass_lblSatGy.Name = "mass_lblSatGy"
        Me.mass_lblSatGy.Size = New System.Drawing.Size(43, 16)
        Me.mass_lblSatGy.TabIndex = 39
        Me.mass_lblSatGy.Text = "0"
        Me.mass_lblSatGy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatGx
        '
        Me.mass_lblSatGx.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatGx.Location = New System.Drawing.Point(45, 25)
        Me.mass_lblSatGx.Name = "mass_lblSatGx"
        Me.mass_lblSatGx.Size = New System.Drawing.Size(43, 16)
        Me.mass_lblSatGx.TabIndex = 39
        Me.mass_lblSatGx.Text = "0"
        Me.mass_lblSatGx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl37
        '
        Me.zlbl37.BackColor = System.Drawing.Color.Transparent
        Me.zlbl37.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl37.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl37.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl37.Location = New System.Drawing.Point(15, 25)
        Me.zlbl37.Name = "zlbl37"
        Me.zlbl37.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl37.Size = New System.Drawing.Size(24, 16)
        Me.zlbl37.TabIndex = 27
        Me.zlbl37.Text = "Gx"
        Me.zlbl37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl38
        '
        Me.zlbl38.BackColor = System.Drawing.Color.Transparent
        Me.zlbl38.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl38.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl38.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl38.Location = New System.Drawing.Point(15, 45)
        Me.zlbl38.Name = "zlbl38"
        Me.zlbl38.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl38.Size = New System.Drawing.Size(25, 16)
        Me.zlbl38.TabIndex = 29
        Me.zlbl38.Text = "Gy"
        Me.zlbl38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl40
        '
        Me.zlbl40.BackColor = System.Drawing.Color.Transparent
        Me.zlbl40.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl40.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl40.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl40.Location = New System.Drawing.Point(15, 65)
        Me.zlbl40.Name = "zlbl40"
        Me.zlbl40.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl40.Size = New System.Drawing.Size(24, 16)
        Me.zlbl40.TabIndex = 28
        Me.zlbl40.Text = "Gz"
        Me.zlbl40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zfra_16
        '
        Me.zfra_16.Controls.Add(Me.mass_lblSatIxz)
        Me.zfra_16.Controls.Add(Me.mass_lblSatIxy)
        Me.zfra_16.Controls.Add(Me.mass_lblSatIyz)
        Me.zfra_16.Controls.Add(Me.mass_lblSatIxx)
        Me.zfra_16.Controls.Add(Me.mass_lblSatIyy)
        Me.zfra_16.Controls.Add(Me.mass_lblSatIzz)
        Me.zfra_16.Controls.Add(Me.zlbl25)
        Me.zfra_16.Controls.Add(Me.zlbl26)
        Me.zfra_16.Controls.Add(Me.zlbl29)
        Me.zfra_16.Controls.Add(Me.zlbl32)
        Me.zfra_16.Controls.Add(Me.zlbl33)
        Me.zfra_16.Controls.Add(Me.zlbl36)
        Me.zfra_16.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_16.ForeColor = System.Drawing.Color.Black
        Me.zfra_16.Location = New System.Drawing.Point(133, 49)
        Me.zfra_16.Name = "zfra_16"
        Me.zfra_16.Size = New System.Drawing.Size(203, 91)
        Me.zfra_16.TabIndex = 37
        Me.zfra_16.TabStop = False
        Me.zfra_16.Text = "Inertias (kg.m²)"
        '
        'mass_lblSatIxz
        '
        Me.mass_lblSatIxz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatIxz.Location = New System.Drawing.Point(131, 65)
        Me.mass_lblSatIxz.Name = "mass_lblSatIxz"
        Me.mass_lblSatIxz.Size = New System.Drawing.Size(48, 16)
        Me.mass_lblSatIxz.TabIndex = 39
        Me.mass_lblSatIxz.Text = "0"
        Me.mass_lblSatIxz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatIxy
        '
        Me.mass_lblSatIxy.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatIxy.Location = New System.Drawing.Point(131, 46)
        Me.mass_lblSatIxy.Name = "mass_lblSatIxy"
        Me.mass_lblSatIxy.Size = New System.Drawing.Size(48, 16)
        Me.mass_lblSatIxy.TabIndex = 39
        Me.mass_lblSatIxy.Text = "0"
        Me.mass_lblSatIxy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatIyz
        '
        Me.mass_lblSatIyz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatIyz.Location = New System.Drawing.Point(131, 26)
        Me.mass_lblSatIyz.Name = "mass_lblSatIyz"
        Me.mass_lblSatIyz.Size = New System.Drawing.Size(48, 16)
        Me.mass_lblSatIyz.TabIndex = 39
        Me.mass_lblSatIyz.Text = "0"
        Me.mass_lblSatIyz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatIxx
        '
        Me.mass_lblSatIxx.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatIxx.Location = New System.Drawing.Point(45, 25)
        Me.mass_lblSatIxx.Name = "mass_lblSatIxx"
        Me.mass_lblSatIxx.Size = New System.Drawing.Size(48, 16)
        Me.mass_lblSatIxx.TabIndex = 39
        Me.mass_lblSatIxx.Text = "0"
        Me.mass_lblSatIxx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatIyy
        '
        Me.mass_lblSatIyy.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatIyy.Location = New System.Drawing.Point(45, 45)
        Me.mass_lblSatIyy.Name = "mass_lblSatIyy"
        Me.mass_lblSatIyy.Size = New System.Drawing.Size(48, 16)
        Me.mass_lblSatIyy.TabIndex = 39
        Me.mass_lblSatIyy.Text = "0"
        Me.mass_lblSatIyy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_lblSatIzz
        '
        Me.mass_lblSatIzz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mass_lblSatIzz.Location = New System.Drawing.Point(45, 65)
        Me.mass_lblSatIzz.Name = "mass_lblSatIzz"
        Me.mass_lblSatIzz.Size = New System.Drawing.Size(48, 16)
        Me.mass_lblSatIzz.TabIndex = 39
        Me.mass_lblSatIzz.Text = "0"
        Me.mass_lblSatIzz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl25
        '
        Me.zlbl25.BackColor = System.Drawing.Color.Transparent
        Me.zlbl25.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl25.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl25.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl25.Location = New System.Drawing.Point(15, 45)
        Me.zlbl25.Name = "zlbl25"
        Me.zlbl25.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl25.Size = New System.Drawing.Size(29, 16)
        Me.zlbl25.TabIndex = 5
        Me.zlbl25.Text = "Iyy"
        Me.zlbl25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl26
        '
        Me.zlbl26.BackColor = System.Drawing.Color.Transparent
        Me.zlbl26.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl26.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl26.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl26.Location = New System.Drawing.Point(15, 65)
        Me.zlbl26.Name = "zlbl26"
        Me.zlbl26.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl26.Size = New System.Drawing.Size(27, 16)
        Me.zlbl26.TabIndex = 6
        Me.zlbl26.Text = "Izz"
        Me.zlbl26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl29
        '
        Me.zlbl29.BackColor = System.Drawing.Color.Transparent
        Me.zlbl29.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl29.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl29.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl29.Location = New System.Drawing.Point(15, 25)
        Me.zlbl29.Name = "zlbl29"
        Me.zlbl29.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl29.Size = New System.Drawing.Size(27, 16)
        Me.zlbl29.TabIndex = 6
        Me.zlbl29.Text = "Ixx"
        Me.zlbl29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl32
        '
        Me.zlbl32.BackColor = System.Drawing.Color.Transparent
        Me.zlbl32.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl32.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl32.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl32.Location = New System.Drawing.Point(98, 26)
        Me.zlbl32.Name = "zlbl32"
        Me.zlbl32.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl32.Size = New System.Drawing.Size(28, 16)
        Me.zlbl32.TabIndex = 27
        Me.zlbl32.Text = "Iyz"
        Me.zlbl32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl33
        '
        Me.zlbl33.BackColor = System.Drawing.Color.Transparent
        Me.zlbl33.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl33.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl33.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl33.Location = New System.Drawing.Point(98, 66)
        Me.zlbl33.Name = "zlbl33"
        Me.zlbl33.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl33.Size = New System.Drawing.Size(27, 16)
        Me.zlbl33.TabIndex = 28
        Me.zlbl33.Text = "Ixz"
        Me.zlbl33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl36
        '
        Me.zlbl36.BackColor = System.Drawing.Color.Transparent
        Me.zlbl36.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl36.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl36.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl36.Location = New System.Drawing.Point(98, 46)
        Me.zlbl36.Name = "zlbl36"
        Me.zlbl36.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl36.Size = New System.Drawing.Size(28, 16)
        Me.zlbl36.TabIndex = 29
        Me.zlbl36.Text = "Ixy"
        Me.zlbl36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mass_pictSatellite
        '
        Me.mass_pictSatellite.BackColor = System.Drawing.SystemColors.Control
        Me.mass_pictSatellite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mass_pictSatellite.Cursor = System.Windows.Forms.Cursors.Default
        Me.mass_pictSatellite.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mass_pictSatellite.Location = New System.Drawing.Point(678, 55)
        Me.mass_pictSatellite.Name = "mass_pictSatellite"
        Me.mass_pictSatellite.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mass_pictSatellite.Size = New System.Drawing.Size(350, 350)
        Me.mass_pictSatellite.TabIndex = 10
        Me.mass_pictSatellite.TabStop = False
        '
        'cmd_quit
        '
        Me.cmd_quit.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_quit.Location = New System.Drawing.Point(434, 585)
        Me.cmd_quit.Name = "cmd_quit"
        Me.cmd_quit.Size = New System.Drawing.Size(108, 36)
        Me.cmd_quit.TabIndex = 3
        Me.cmd_quit.Text = "Close"
        Me.cmd_quit.UseVisualStyleBackColor = True
        '
        'tmrAnimate
        '
        Me.tmrAnimate.Enabled = True
        '
        'tmrTransition
        '
        Me.tmrTransition.Interval = 10
        '
        'frmAttConf
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1049, 632)
        Me.ControlBox = False
        Me.Controls.Add(Me.zfra_9)
        Me.Controls.Add(Me.cmd_quit)
        Me.Controls.Add(Me.tabPower)
        Me.Controls.Add(Me.mass_pictSatellite)
        Me.Name = "frmAttConf"
        Me.Text = "Attitude control data"
        Me.TopMost = True
        Me.tabPower.ResumeLayout(False)
        Me.Onglet_att_geom.ResumeLayout(False)
        Me.zfra_8.ResumeLayout(False)
        Me.zfra_8.PerformLayout()
        Me.zfra_4.ResumeLayout(False)
        Me.zfra_4.PerformLayout()
        CType(Me.zpict_5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.zfra_3.ResumeLayout(False)
        Me.zfra_3.PerformLayout()
        Me.zfra_7.ResumeLayout(False)
        Me.zfra_7.PerformLayout()
        Me.zfra_6.ResumeLayout(False)
        Me.zfra_6.PerformLayout()
        Me.zfra_5.ResumeLayout(False)
        Me.zfra_5.PerformLayout()
        Me.Onglet_att_prop.ResumeLayout(False)
        Me.zfra_14.ResumeLayout(False)
        Me.zfra_14.PerformLayout()
        Me.zfra_11.ResumeLayout(False)
        Me.zfra_12.ResumeLayout(False)
        Me.zfra_12.PerformLayout()
        Me.zfra_13.ResumeLayout(False)
        Me.zfra_13.PerformLayout()
        Me.zfra_1.ResumeLayout(False)
        Me.zfra_10.ResumeLayout(False)
        Me.zfra_10.PerformLayout()
        Me.zfra_2.ResumeLayout(False)
        Me.zfra_2.PerformLayout()
        Me.zfra_9.ResumeLayout(False)
        Me.zfra_9.PerformLayout()
        Me.zfra_15.ResumeLayout(False)
        Me.zfra_16.ResumeLayout(False)
        CType(Me.mass_pictSatellite, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents tabPower As System.Windows.Forms.TabControl
    Public WithEvents Onglet_att_prop As System.Windows.Forms.TabPage
    Public WithEvents Onglet_att_manag As System.Windows.Forms.TabPage
    Friend WithEvents cmd_quit As System.Windows.Forms.Button
    Friend WithEvents Onglet_att_act As System.Windows.Forms.TabPage
    Public WithEvents tmrAnimate As System.Windows.Forms.Timer
    Public WithEvents tmrTransition As System.Windows.Forms.Timer
    Friend WithEvents Onglet_att_geom As System.Windows.Forms.TabPage
    Public WithEvents zfra_4 As System.Windows.Forms.GroupBox
    Public WithEvents mass_lstPanels As System.Windows.Forms.ListBox
    Public WithEvents mass_cmdModify As System.Windows.Forms.Button
    Friend WithEvents mass_txtPanelMass As System.Windows.Forms.TextBox
    Public WithEvents mass_cmdAddPanel As System.Windows.Forms.Button
    Public WithEvents zlbl_75 As System.Windows.Forms.Label
    Public WithEvents mass_cmdRemovePanel As System.Windows.Forms.Button
    Public WithEvents zlbl_64 As System.Windows.Forms.Label
    Public WithEvents zlbl_63 As System.Windows.Forms.Label
    Public WithEvents mass_cmbFace As System.Windows.Forms.ComboBox
    Public WithEvents mass_txtPanelLength As System.Windows.Forms.TextBox
    Public WithEvents mass_txtPanelWidth As System.Windows.Forms.TextBox
    Public WithEvents mass_cmbDirection As System.Windows.Forms.ComboBox
    Public WithEvents mass_cmbPosition As System.Windows.Forms.ComboBox
    Public WithEvents mass_optPanelType_0 As System.Windows.Forms.RadioButton
    Public WithEvents mass_optPanelType_1 As System.Windows.Forms.RadioButton
    Public WithEvents mass_optPanelType_2 As System.Windows.Forms.RadioButton
    Public WithEvents zlbl_69 As System.Windows.Forms.Label
    Public WithEvents zlbl_1 As System.Windows.Forms.Label
    Public WithEvents zlbl_5 As System.Windows.Forms.Label
    Public WithEvents zlbl_6 As System.Windows.Forms.Label
    Public WithEvents mass_lblPanelOrient As System.Windows.Forms.Label
    Public WithEvents mass_lblPanelPos As System.Windows.Forms.Label
    Public WithEvents zlbl_3 As System.Windows.Forms.Label
    Public WithEvents zpict_5 As System.Windows.Forms.PictureBox
    Public WithEvents zfra_3 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_11 As System.Windows.Forms.Label
    Public WithEvents mass_txtBodyMass As System.Windows.Forms.TextBox
    Public WithEvents zlbl_15 As System.Windows.Forms.Label
    Friend WithEvents zfra_7 As System.Windows.Forms.GroupBox
    Public WithEvents mass_txtGy As System.Windows.Forms.TextBox
    Public WithEvents zlbl_74 As System.Windows.Forms.Label
    Public WithEvents zlbl_83 As System.Windows.Forms.Label
    Public WithEvents mass_txtGx As System.Windows.Forms.TextBox
    Public WithEvents zlbl_78 As System.Windows.Forms.Label
    Public WithEvents zlbl_82 As System.Windows.Forms.Label
    Public WithEvents mass_txtGz As System.Windows.Forms.TextBox
    Public WithEvents zlbl_79 As System.Windows.Forms.Label
    Public WithEvents zlbl_80 As System.Windows.Forms.Label
    Friend WithEvents zfra_6 As System.Windows.Forms.GroupBox
    Public WithEvents mass_txtIyz As System.Windows.Forms.TextBox
    Public WithEvents zlbl_85 As System.Windows.Forms.Label
    Public WithEvents zlbl_86 As System.Windows.Forms.Label
    Public WithEvents zlbl_14 As System.Windows.Forms.Label
    Public WithEvents mass_txtIyy As System.Windows.Forms.TextBox
    Public WithEvents zlbl_13 As System.Windows.Forms.Label
    Public WithEvents zlbl_84 As System.Windows.Forms.Label
    Public WithEvents zlbl_12 As System.Windows.Forms.Label
    Public WithEvents mass_txtIzz As System.Windows.Forms.TextBox
    Public WithEvents mass_txtIxz As System.Windows.Forms.TextBox
    Public WithEvents mass_txtIxx As System.Windows.Forms.TextBox
    Public WithEvents mass_txtIxy As System.Windows.Forms.TextBox
    Public WithEvents zlbl_87 As System.Windows.Forms.Label
    Public WithEvents zlbl_10 As System.Windows.Forms.Label
    Public WithEvents zlbl_88 As System.Windows.Forms.Label
    Friend WithEvents zfra_5 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_0 As System.Windows.Forms.Label
    Public WithEvents zlbl_2 As System.Windows.Forms.Label
    Public WithEvents mass_txtBodyWidth As System.Windows.Forms.TextBox
    Public WithEvents mass_txtBodyHeigth As System.Windows.Forms.TextBox
    Public WithEvents zlbl_65 As System.Windows.Forms.Label
    Public WithEvents zlbl_66 As System.Windows.Forms.Label
    Public WithEvents mass_pictSatellite As System.Windows.Forms.PictureBox
    Public WithEvents zfra_8 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_17 As System.Windows.Forms.Label
    Public WithEvents zlbl_18 As System.Windows.Forms.Label
    Public WithEvents mass_cmbBoomFace As System.Windows.Forms.ComboBox
    Public WithEvents mass_txtBoomMass As System.Windows.Forms.TextBox
    Public WithEvents mass_txtBoomDist As System.Windows.Forms.TextBox
    Public WithEvents zlbl_19 As System.Windows.Forms.Label
    Public WithEvents zlbl_16 As System.Windows.Forms.Label
    Public WithEvents zlbl_20 As System.Windows.Forms.Label
    Friend WithEvents zfra_1 As System.Windows.Forms.GroupBox
    Public WithEvents prop_txtCN_4 As System.Windows.Forms.TextBox
    Public WithEvents zlbl1 As System.Windows.Forms.Label
    Public WithEvents zlbl2 As System.Windows.Forms.Label
    Public WithEvents prop_txtCN_3 As System.Windows.Forms.TextBox
    Public WithEvents zlbl5 As System.Windows.Forms.Label
    Public WithEvents prop_txtCN_1 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCN_5 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCN_0 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCN_2 As System.Windows.Forms.TextBox
    Public WithEvents zlbl7 As System.Windows.Forms.Label
    Public WithEvents zlbl8 As System.Windows.Forms.Label
    Public WithEvents zlbl9 As System.Windows.Forms.Label
    Friend WithEvents zfra_2 As System.Windows.Forms.GroupBox
    Friend WithEvents zfra_11 As System.Windows.Forms.GroupBox
    Friend WithEvents zfra_12 As System.Windows.Forms.GroupBox
    Public WithEvents prop_txtCD_0 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCD_4 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCD_3 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCD_2 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCD_5 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCD_1 As System.Windows.Forms.TextBox
    Friend WithEvents zfra_13 As System.Windows.Forms.GroupBox
    Public WithEvents prop_txtCR_0 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCR_4 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCR_3 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCR_2 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCR_5 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCR_1 As System.Windows.Forms.TextBox
    Friend WithEvents zfra_10 As System.Windows.Forms.GroupBox
    Public WithEvents prop_txtCT_0 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCT_4 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCT_3 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCT_2 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCT_5 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtCT_1 As System.Windows.Forms.TextBox
    Public WithEvents prop_txtMG_X As System.Windows.Forms.TextBox
    Public WithEvents zlbl31 As System.Windows.Forms.Label
    Public WithEvents prop_txtMG_Z As System.Windows.Forms.TextBox
    Public WithEvents prop_txtMG_Y As System.Windows.Forms.TextBox
    Public WithEvents zlbl34 As System.Windows.Forms.Label
    Public WithEvents zlbl35 As System.Windows.Forms.Label
    Friend WithEvents zfra_14 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl24 As System.Windows.Forms.Label
    Public WithEvents zlbl23 As System.Windows.Forms.Label
    Public WithEvents zlbl22 As System.Windows.Forms.Label
    Public WithEvents zlbl19 As System.Windows.Forms.Label
    Public WithEvents zlbl20 As System.Windows.Forms.Label
    Public WithEvents zlbl21 As System.Windows.Forms.Label
    Public WithEvents zlbl18 As System.Windows.Forms.Label
    Public WithEvents zlbl17 As System.Windows.Forms.Label
    Public WithEvents zlbl16 As System.Windows.Forms.Label
    Public WithEvents zlbl15 As System.Windows.Forms.Label
    Public WithEvents zlbl13 As System.Windows.Forms.Label
    Public WithEvents zlbl14 As System.Windows.Forms.Label
    Public WithEvents zlbl12 As System.Windows.Forms.Label
    Public WithEvents zlbl11 As System.Windows.Forms.Label
    Public WithEvents zlbl10 As System.Windows.Forms.Label
    Public WithEvents zlbl6 As System.Windows.Forms.Label
    Public WithEvents zlbl4 As System.Windows.Forms.Label
    Public WithEvents zlbl3 As System.Windows.Forms.Label
    Friend WithEvents zfra_16 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl25 As System.Windows.Forms.Label
    Public WithEvents zlbl26 As System.Windows.Forms.Label
    Public WithEvents zlbl29 As System.Windows.Forms.Label
    Public WithEvents zlbl32 As System.Windows.Forms.Label
    Public WithEvents zlbl33 As System.Windows.Forms.Label
    Public WithEvents zlbl36 As System.Windows.Forms.Label
    Friend WithEvents zfra_9 As System.Windows.Forms.GroupBox
    Friend WithEvents zfra_15 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl37 As System.Windows.Forms.Label
    Public WithEvents zlbl38 As System.Windows.Forms.Label
    Public WithEvents zlbl40 As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatGx As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatGz As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatGy As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatIxz As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatIxy As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatIyz As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatIxx As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatIyy As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatIzz As System.Windows.Forms.Label
    Friend WithEvents mass_lblSatMass As System.Windows.Forms.Label
    Public WithEvents zlbl30 As System.Windows.Forms.Label
    Public WithEvents zlbl28 As System.Windows.Forms.Label

End Class
