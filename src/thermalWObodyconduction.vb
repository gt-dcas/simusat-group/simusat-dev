﻿Module thermal
    ' Central body thermical characteristics
    Public BodyAlf(5) As Single     ' Absorptivity (solar spectrum) of body faces  0: +x / 1: -x / 2: +y / 3: -y / 4: +z / 5: -z
    Public BodyEps(5) As Single     ' Emissivity of body faces (IR spectrum)
    Public BodySpecHeat As Single   ' Body specific heat
    Public BodyTemp As Single       ' Body temperature
    Public T_init_body As Single    ' Initial temperature of central body

    Public flux_solaire As Single
    Public albedo As Single
    Public cos_sun_earth_sat As Double
    Public flux_ir As Double
    Public flux_albedo As Double

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Face_SunInc(5) As Double
    Public Face_SolarFlux(5) As Double     ' Solar flux on faces   
    Public Face_EarthInc(5) As Double
    Public Face_AlbedoFlux(5) As Double    ' Albedo flux on faces   
    Public Face_IRFlux(5) As Double        ' Infrared flux on faces 

    'Dim Face_conduct(5) As Single       ' Power due to conduction with solar panel
    'Public CFprueba As Double
    'Public Posprueba As Double

    ''' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


    Public Function solar_intensity(ByVal d As Double) As Single
        '
        ' Direct solar radiation intensity for a given distance to Sun
        ' Input: d distance to Sun
        '
        solar_intensity = CSng(SunPow / 4 / pi / d / d / 1000000.0#)
        '
    End Function

    Public Function flux_incident_overall_power(ByVal SolarPanel As CSolarPanel, ByVal Time As Double) As Double 'Used only in power.vb
        '
        ' Returns the value of the normal-equivalent incident energie flux related to current
        ' sun & earth position, also taking into account the effect of sun incidence relative to the panel
        '
        ' Input : solar panel
        '
        Dim fs As Double
        Dim fa As Double
        Dim sun_flux_ratio As Double
        Dim albedo_flux_ratio As Double
        '
        sun_flux_ratio = flux_effective_ratio(SolarPanel.SunIncidence(Time))
        If blnEclipse Then
            sun_flux_ratio = 0
        End If
        If sun_flux_ratio > 0 Then
            fs = solar_intensity(vctNorm(SunPos)) * sun_flux_ratio
        Else
            fs = 0
        End If
        albedo_flux_ratio = flux_effective_ratio(SolarPanel.EarthIncidence(Time))
        If albedo_flux_ratio > 0 Then
            fa = flux_albedo * albedo_flux_ratio
        Else
            fa = 0
        End If
        flux_incident_overall_power = fa + fs
        SolarPanel.PanelEffFlux = flux_incident_overall_power
        ''
    End Function

    Public Function flux_effective_ratio(ByVal teta As Double) As Double
        '
        ' Returns portion of equivalent incident flux related to the
        ' sun angle over the cells plain taking account the curve which is entered in the solar cells definition
        ' in the energy subsystem data window.
        '
        ' Input:
        '   - Angle of incidence
        '
        Dim p(2) As Double
        Dim tm As Double
        Dim td As Double
        Dim X As Double
        tm = cell_tetam * rad
        td = cell_tetad * rad
        '
        ' Features of Parabolical section
        p(0) = (Math.Sin(td) * (tm - td) - Math.Cos(td)) / (tm - td) ^ 2
        p(1) = -Math.Sin(td) - 2 * p(0) * td
        p(2) = -p(0) * tm * tm - p(1) * tm
        If teta <= td Then
            X = Math.Cos(teta)
        ElseIf teta > td And teta < tm Then
            X = p(0) * teta * teta + p(1) * teta + p(2)
        Else
            X = 0
        End If
        If X < 0 Then
            X = 0
        End If
        flux_effective_ratio = X
        ''
    End Function

    Public Sub flux_calculation(ByVal j_current As Double)
        '
        ' Solar flux, albedo and earth IR calculation
        '
        ' Entrée : j_current : Current time in Julian days
        '
        Dim r_sat_earth As Double
        Dim r_sun_earth As Double
        Dim earth_angle_from_sat As Double
        Dim X As Double
        Dim udtGregorianNow As udtDate
        '
        '
        ' Solar flux calculation
        ' ----------------------
        ' The current value for the solar constant results from the Sat-Sun distance and
        ' decreases with is 2nd power
        flux_solaire = solar_intensity(vctNorm(SunPos))
        '
        ' Albedo calculation
        ' ------------------
        ' SatPos and SunPos are respectively Earth_Sat and Earth_Sun vectors
        ' Then Sat_Earth and Sat_Sun unitary vectors are obtained, as well as Earth's disc
        ' (by mean of its angular radius earth_angle_from_sat).
        ' The boolean blnEclipse (already obtained by orbitography updating) specifies if the satellite is in eclipse or not (True/False respectively).
        r_sat_earth = vctNorm(SatPos)
        r_sun_earth = vctNorm(SunPos)
        earth_angle_from_sat = Math.Asin(RE / r_sat_earth)
        '
        ' Albedo is related to current sat/earth/sun angle through the factor 
        ' [cos(0.9*angle)]^1.5 (CNES) means that above 100° no albedo reaches the satellite

        cos_sun_earth_sat = vctDot(SunPos, SatPos) / r_sun_earth / r_sat_earth
        X = Math.Acos(cos_sun_earth_sat)
        X = Math.Cos(0.9 * X)
        If X < 0 Then X = 0 ' If flux is negative (angle sun/sat/earth > 100°
        X = Math.Sqrt(X ^ 3)
        ' Current values for albedo and ir depend on which is the current month
        ' within the year.
        convert_Julian_Gregorian(j_current, udtGregorianNow)

        'albedo = 0.3
        If Alb_Cer_bln Then
            albedo = AlbedoCoef(SatGeoPos.lat, udtGregorianNow.month)
        Else
            albedo = Albedo_month(udtGregorianNow.month - 1)
        End If
        'albedo = AlbedoCoef(SatGeoPos.lat, udtGregorianNow.month)
        flux_albedo = flux_solaire * albedo * X
        '
        ' IR flux calculation
        ' -------------------
        'flux_ir = IR_month(udtGregorianNow.month - 1) * (RE / r_sat_earth) ^ 2
        If EarthIR_Cer_bln Then
            flux_ir = flux_rad(EarthTemp(SatGeoPos.lat, udtGregorianNow.month) - 273.15)  'Reason for -273.15: Function is done for Celsius degrees but Earth Temp is in Kelvin
        Else
            flux_ir = IR_month(udtGregorianNow.month - 1)
        End If
        ''
    End Sub
    Public Sub compute_thermal(ByVal Time As Double, ByVal TimeStep As Double)
        '
        Dim panel As CSolarPanel
        Dim deltaT As Double
        Dim alf(1) As Double    ' Absorptivity
        Dim eps(1) As Double    ' Emissivity
        Dim fs(1) As Double, fs1(1) As Double, fs2(1) As Double     ' Solar + albedo flux
        Dim fir(1) As Double    ' Earth Infra Red flux
        Dim i As Integer
        Dim X As Single
        Dim Y As Single

        '
        ' ----------------------------
        ' Central body thermal balance 
        ' ----------------------------
        Dim SunDir As vector                ' Sun direction in the satellite's coordinates system 
        Dim EarthDir As vector              ' Earth direction in the satellite's coordinates system 
        Dim Face_dir(5) As vector
        Dim Face_conduct(5) As Single       ' Power due to conduction with solar panel
        Dim Face_Surf(5) As Single
        Dim tmpVect As vector

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ini
        'Dim Face_SolarFlux As Double     ' Solar flux on faces   
        'Dim Face_AlbedoFlux As Double    ' Albedo flux on faces   
        'Dim Face_IRFlux As Double        ' Infrared flux on faces 
        'Dim Face_SunInc As Single
        'Dim Face_EarthInc As Single
        '
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' fin
        '
        ' Calculate flux for each face 
        ' ----------------------------
        ' Effective absorbed and emmited flux (Watts) taking account of surface, absortivity alf and emissivity eps of each face
        With Satellite.getSatCOSCoords(Time, SunPos)
            SunDir.X = .X : SunDir.Y = .Y : SunDir.Z = .Z
        End With
        '
        Face_dir(0).X = 1 : Face_dir(0).Y = 0 : Face_dir(0).Z = 0
        Face_dir(1).X = -1 : Face_dir(1).Y = 0 : Face_dir(1).Z = 0
        Face_dir(2).X = 0 : Face_dir(2).Y = 1 : Face_dir(2).Z = 0
        Face_dir(3).X = 0 : Face_dir(3).Y = -1 : Face_dir(3).Z = 0
        Face_dir(4).X = 0 : Face_dir(4).Y = 0 : Face_dir(4).Z = 1
        Face_dir(5).X = 0 : Face_dir(5).Y = 0 : Face_dir(5).Z = -1
        '
        For i = 0 To 5
            '
            ' Face surface
            Select Case i
                Case 0 To 3 : Face_Surf(i) = Satellite.satBodyWidth * Satellite.satBodyHeight    ' +X -X +Y -Y
                Case Else : Face_Surf(i) = Satellite.satBodyWidth * Satellite.satBodyWidth     ' +Z -Z
            End Select
            '
            'Sun flux
            Face_SunInc(i) = Math.Acos(vctDot(SunDir, Face_dir(i)) / vctNorm(SunDir) / vctNorm(Face_dir(i)))
            '
            Face_SolarFlux(i) = flux_solaire * Math.Cos(Face_SunInc(i)) * Face_Surf(i) * BodyAlf(i)
            If blnEclipse Or Math.Cos(Face_SunInc(i)) <= 0 Then
                Face_SolarFlux(i) = 0
            End If

            '
            ' Albedo flux

            tmpVect.X = -SatPos.X : tmpVect.Y = -SatPos.Y : tmpVect.Z = -SatPos.Z
            With Satellite.getSatCOSCoords(Time, tmpVect)
                EarthDir.X = .X : EarthDir.Y = .Y : EarthDir.Z = .Z ' Earth direction in the satellite's COS
            End With
            '
            Face_EarthInc(i) = Math.Acos(vctDot(EarthDir, Face_dir(i)) / vctNorm(EarthDir) / vctNorm(Face_dir(i))) * 180 / pi 'in degrees
            'Posprueba = vctNorm(SatPos)
            'CFprueba = EarthIRConfFactor(Face_EarthInc(i), Posprueba)
            Face_AlbedoFlux(i) = flux_albedo * EarthIRConfFactor(Face_EarthInc(i), vctNorm(SatPos)) * Face_Surf(i) * BodyAlf(i)


            ' Received Infrared flux
            Face_IRFlux(i) = flux_ir * EarthIRConfFactor(Face_EarthInc(i), vctNorm(SatPos)) * Face_Surf(i) * BodyEps(i)
        Next
        '
        ' Add interaction with solar panels (type 2 panel with body)
        ' ---------------------------------
        For i = 1 To SolarPanels.Count()
            panel = SolarPanels.Item(i)

            If panel.PanelType = "Type2" Then
                With panel
                    Select Case .PanelFace
                        '
                        Case "+ X"
                            '
                            ' - Remove external flux masked by the solar panel
                            X = Math.Max(1 - .PanelSurf / Face_Surf(0), 0)
                            Face_SolarFlux(0) *= X
                            Face_AlbedoFlux(0) *= X
                            Face_IRFlux(0) *= X
                            '
                            ' Add infrared flux coming from solar panel
                            Face_IRFlux(0) += .PanelSurf * .PanelBackEps * flux_rad(.PanelTemp)
                            '
                            ' Add conduction with solar panel
                            Face_conduct(0) = .PanelConduction * (.PanelTemp - BodyTemp) * .PanelSurf
                            '
                        Case "- X"
                            '
                            X = Math.Max(1 - .PanelSurf / Face_Surf(1), 0)
                            Face_SolarFlux(1) *= X
                            Face_AlbedoFlux(1) *= X
                            Face_IRFlux(1) *= X
                            '
                            Face_IRFlux(1) += .PanelSurf * .PanelBackEps * flux_rad(.PanelTemp)
                            '
                            Face_conduct(1) = .PanelConduction * (.PanelTemp - BodyTemp) * .PanelSurf
                            '
                        Case "+ Y"
                            '
                            X = Math.Max(1 - .PanelSurf / Face_Surf(2), 0)
                            Face_SolarFlux(2) *= X
                            Face_AlbedoFlux(2) *= X
                            Face_IRFlux(2) *= X
                            '
                            Face_IRFlux(2) += .PanelSurf * .PanelBackEps * flux_rad(.PanelTemp)
                            '
                            Face_conduct(2) = .PanelConduction * (.PanelTemp - BodyTemp) * .PanelSurf
                            '
                        Case "- Y"
                            '
                            X = Math.Max(1 - .PanelSurf / Face_Surf(3), 0)
                            Face_SolarFlux(3) *= X
                            Face_AlbedoFlux(3) *= X
                            Face_IRFlux(3) *= X
                            '
                            Face_IRFlux(3) += .PanelSurf * .PanelBackEps * flux_rad(.PanelTemp)
                            '
                            Face_conduct(3) = .PanelConduction * (.PanelTemp - BodyTemp) * .PanelSurf
                            '                        
                        Case "+ Z"
                            '
                            X = Math.Max(1 - .PanelSurf / Face_Surf(4), 0)
                            Face_SolarFlux(4) *= X
                            Face_AlbedoFlux(4) *= X
                            Face_IRFlux(4) *= X
                            '
                            Face_IRFlux(4) += .PanelSurf * .PanelBackEps * flux_rad(.PanelTemp)
                            '
                            Face_conduct(4) = .PanelConduction * (.PanelTemp - BodyTemp) * .PanelSurf
                            '  
                        Case "- Z"
                            '
                            X = Math.Max(1 - .PanelSurf / Face_Surf(5), 0)
                            Face_SolarFlux(5) *= X
                            Face_AlbedoFlux(5) *= X
                            Face_IRFlux(5) *= X
                            '
                            Face_IRFlux(5) += .PanelSurf * .PanelBackEps * flux_rad(.PanelTemp)
                            '
                            Face_conduct(5) = .PanelConduction * (.PanelTemp - BodyTemp) * .PanelSurf
                            '                      
                    End Select
                End With
            End If
        Next

        ' Global thermal balance of body
        ' ------------------------------
        X = 0
        For i = 0 To 5
            ' Add each face global power

            X += Face_SolarFlux(i) + Face_AlbedoFlux(i) + Face_IRFlux(i) + Face_conduct(i)

            X -= flux_rad(BodyTemp) * Face_Surf(i) * BodyEps(i)

        Next
        'Add dissipated power
        X += pdiss
        ' Compute temperature
        deltaT = (X / BodySpecHeat) * TimeStep * secday
        ' 
        BodyTemp += deltaT
        '
        ' ----------------------------
        ' Solar panels thermal balance
        ' ----------------------------
        For i = 1 To SolarPanels.Count()
            panel = SolarPanels.Item(i)
            ' Solar panels flux
            ' Note:
            ' 0 Cells face
            ' 1 Back face
            '
            With panel
                ' Cells face flux
                alf(0) = .PanelCellAlf
                eps(0) = .PanelCellEps
                fs1(0) = flux_incident_solar(panel, 0, Time)
                fs2(0) = flux_incident_albedo(panel, 0, Time)
                fs(0) = fs1(0) + fs2(0)
                fir(0) = flux_incident_ir(panel, 0, Time)
                '
                ' Back face flux
                alf(1) = .PanelBackAlf
                eps(1) = .PanelBackEps
                fs(1) = flux_incident_solar(panel, pi, Time) + flux_incident_albedo(panel, pi, Time)
                fir(1) = flux_incident_ir(panel, pi, Time)
                '

                If .PanelType = "Type2" Then  ' If type "Type2" no external flux on the back face
                    fs(1) = 0
                    fir(1) = 0
                End If
                '
                ' Thermal balance in panel
                Y = 0
                Y = (alf(0) * fs(0) + eps(0) * fir(0) + alf(1) * fs(1) + eps(1) * fir(1) - (eps(0) + eps(1)) * flux_rad(.PanelTemp)) * .PanelSurf
                '
                printOnConsoleIfBebugMode("Thermal balance in panel X is:" + X.ToString)
                If .PanelType = "Type2" Then
                    '
                    ' Add infrared flux coming from body
                    Select Case .PanelFace
                        Case "+ X"
                            Y += BodyEps(0) * flux_rad(BodyTemp) * .PanelSurf
                        Case "- X"
                            Y += BodyEps(1) * flux_rad(BodyTemp) * .PanelSurf
                        Case "+ Y"
                            Y += BodyEps(2) * flux_rad(BodyTemp) * .PanelSurf
                        Case "- Y"
                            Y += BodyEps(3) * flux_rad(BodyTemp) * .PanelSurf
                        Case "+ Z"
                            Y += BodyEps(4) * flux_rad(BodyTemp) * .PanelSurf
                        Case "- Z"
                            Y += BodyEps(5) * flux_rad(BodyTemp) * .PanelSurf
                    End Select
                    '
                    ' Add conduction with body

                    Y += .PanelConduction * (BodyTemp - .PanelTemp) * .PanelSurf

                    '
                End If
                ')
                deltaT = (Y / .PanelSpecHeat) * TimeStep * secday
                'deltaT = (X - .PanelPower) / .PanelSpecHeat * TimeStep * secday
                '
                ' Current temperature in panel
                .PanelTemp += deltaT
                '
                .PanelSolarFlux = fs1(0)
                .PanelAlbedoFlux = fs2(0)
                .PanelIRFlux = fir(0)
            End With
        Next
        ''
    End Sub

    Public Function flux_incident_solar(ByVal panel As CSolarPanel, ByVal angle As Single, ByVal Time As Double) As Double
        '
        ' Return values of equivalent incident energie flux related to current
        ' sun and earth position without taking care of effect of sun angle.
        '
        ' Input :
        '   - panel
        '   - angle = 0 : front face of the panel
        '   - angle = pi : back face of the panel
        '
        '
        Dim costetasun As Double = Math.Cos(panel.SunIncidence(Time) + angle)

        flux_incident_solar = flux_solaire * costetasun

        If costetasun < 0 Or blnEclipse Then
            flux_incident_solar = 0
        End If

    End Function


    Public Function flux_incident_albedo(ByVal panel As CSolarPanel, ByVal angle As Single, ByVal Time As Double) As Double
        '
        ' angle = 0 : front face of the panel
        ' angle = pi : back face of the panel
        '
        Dim tetaearth As Double
        '
        tetaearth = (panel.EarthIncidence(Time) + angle) * 180 / pi

        flux_incident_albedo = flux_albedo * EarthIRConfFactor(tetaearth, vctNorm(SatPos))
        ''
    End Function


    Public Function flux_incident_ir(ByVal panel As CSolarPanel, ByVal angle As Single, ByVal Time As Double) As Double
        '
        ' angle = 0 : front face of the panel
        ' angle = pi : back face of the panel
        '
        Dim tetaearth As Double
        '
        tetaearth = (panel.EarthIncidence(Time) + angle) * 180 / pi

        flux_incident_ir = flux_ir * EarthIRConfFactor(tetaearth, vctNorm(SatPos))
        ''

    End Function

    Public Function flux_rad(ByVal T As Double) As Double
        If T < -273.15 Then
            'MsgBox(T.ToString(T) + " is not a temperature")
            flux_rad = 0
        Else
            flux_rad = C_Stefan * Math.Pow(T + 273.15, 4)
        End If
    End Function

End Module
