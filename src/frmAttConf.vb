﻿
Imports Tao.OpenGl
Imports Tao.Platform.Windows
Imports System.Math
Imports Microsoft.VisualBasic


Public Class frmAttConf
    Inherits System.Windows.Forms.Form

    Dim eyePos As vector                ' Eye position
    Dim eyeUp As vector                 ' Eye up vector
    Dim eye_distance As Double          ' Distance from (0,0,0) to the eye
    Dim eyeVertAngle As Double          ' Angles that correspond to the eye's position
    Dim eyeHorAngle As Double
    Dim old_x As Double                 ' Old mouse coordinates for calculating relative changes
    Dim old_y As Double
    '
    Dim transitionHorAngle As Double    ' Variables used for camera transitions
    Dim transitionVertAngle As Double
    Dim transitionHorStart As Double
    Dim transitionVertStart As Double
    Dim transitionStartTime As Integer
    Dim transitionDuration As Integer
    '
    ' Font for axis labels
    Dim font1 As Short
    Dim font2 As Short
    '
    ' Id textures
    '
    Dim tx_panel As Integer             ' Panneau
    Dim tx_sat As Integer               ' Satellite
    '
    Dim panel1 As Short
    Dim panel2 As Short
    Dim updatePower As New frmPowerConf

    'Update power configuration. This avoids errors in the output data screen : Overall diagram
    Public Sub update_power()
        updatePower.update_power_conf()
    End Sub

#Region "Graphics"

    ''' <summary>
    ''' Move the camera when click with the mouse
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_pictSatellite_MouseDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles mass_pictSatellite.MouseDown
        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim X As Single = eventArgs.X
        Dim Y As Single = eventArgs.Y
        old_x = X
        old_y = Y

    End Sub

    ''' <summary>
    ''' Move the camera when the mouse moves
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub pictSatellite_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles mass_pictSatellite.MouseMove
        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim X As Single = eventArgs.X
        Dim Y As Single = eventArgs.Y
        '
        If Button = 1 Or Button = 2 Then
            ' Disable the timer in case the user is attempting to control the view while
            ' a transition is in progress
            Me.tmrTransition.Enabled = False
        End If
        '
        If Button = 1 Then
            ' rotation
            eyeVertAngle = eyeVertAngle - (old_y - Y) / 200
            eyeHorAngle = eyeHorAngle - (X - old_x) / 200
            If eyeVertAngle > pi / 2 Then eyeVertAngle = pi / 2
            If eyeVertAngle < -pi / 2 Then eyeVertAngle = -pi / 2
            old_x = X
            old_y = Y
        ElseIf Button = 2 Then
            ' Zoom
            eye_distance = eye_distance + (Y - old_y) / 4
            If eye_distance < 3 Then eye_distance = 3
            old_y = Y
        Else
            Exit Sub
        End If
        '
        ' Calculate new eye position
        eyePos.X = eye_distance * Cos(eyeVertAngle) * Sin(eyeHorAngle)
        eyePos.Y = eye_distance * Sin(eyeVertAngle)
        eyePos.Z = eye_distance * Cos(eyeVertAngle) * Cos(eyeHorAngle)
        '
        ' Redraw
        gl_DrawSatellite()
        ''
    End Sub

    Public Sub glInitialize()
        '
        m_intptrHdc_sat = User.GetDC(mass_pictSatellite.Handle)
        '
        setupPixelFormat(mass_pictSatellite.CreateGraphics.GetHdc)
        hGLRC_sat = Wgl.wglCreateContext(m_intptrHdc_sat)
        Wgl.wglMakeCurrent(m_intptrHdc_sat, hGLRC_sat)

        ' Set up openGL states

        Gl.glEnable(Gl.GL_DEPTH_TEST)

        Gl.glMatrixMode(Gl.GL_MODELVIEW)

        Gl.glLoadIdentity()

        Gl.glEnable(Gl.GL_CULL_FACE)

        Gl.glEnable(Gl.GL_LINE_SMOOTH)

        'Gl.glEnable(Gl.GL_POLYGON_SMOOTH)
        'on remplace le POLYGON_SMOOTH par POLYGON, sinon apparition de triangles sur certaines configs
        Gl.glEnable(Gl.GL_POLYGON)

        Gl.glShadeModel(Gl.GL_SMOOTH)

        Gl.glHint(Gl.GL_PERSPECTIVE_CORRECTION_HINT, Gl.GL_NICEST)

        Gl.glEnable(Gl.GL_TEXTURE_2D)

        ' Lighting

        Gl.glEnable(Gl.GL_LIGHTING)
        Dim Pos(3) As Single

        Gl.glEnable(Gl.GL_LIGHT0)
        Pos(0) = 1
        Pos(1) = 1
        Pos(2) = 1
        Pos(3) = 1

        Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_AMBIENT, Pos(0))
        Pos(0) = 1
        Pos(1) = 1
        Pos(2) = 1
        Pos(3) = 1

        Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_DIFFUSE, Pos(0))

        ' Build fonts
        gl_BuildFont2D(mass_pictSatellite, font1, font2)

        ' Build textures
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\" & "Data\Textures\goldfoil.jpg", tx_sat)
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\" & "Data\Textures\SolarPanel.jpg", tx_panel)

        ' Setup Display Lists
        GLPanelList()

        ' Set background color
        Gl.glClearColor(0, 0, 0, 0)
        '
        Gl.glPolygonMode(Gl.GL_FRONT_AND_BACK, Gl.GL_FILL)
        Gl.glMatrixMode((Gl.GL_PROJECTION))
        Gl.glLoadIdentity()

        'ScaleWidth et ScaleHeight remplacés pas Width et Height
        Gl.glViewport(0, 0, Me.mass_pictSatellite.Width, Me.mass_pictSatellite.Height)
        Glu.gluPerspective(45, Me.mass_pictSatellite.Width / Me.mass_pictSatellite.Height, 1, 1000)

        Gl.glClear(Gl.GL_COLOR_BUFFER_BIT Or Gl.GL_DEPTH_BUFFER_BIT)

        ' Initialize the eye position and angles
        eyeVertAngle = pi / 4
        eyeHorAngle = pi / 4
        eye_distance = 5
        eyePos.X = eye_distance * Cos(eyeVertAngle) * Sin(eyeHorAngle)
        eyePos.Y = eye_distance * Sin(eyeVertAngle)
        eyePos.Z = eye_distance * Cos(eyeVertAngle) * Cos(eyeHorAngle)
        eyeUp.X = 0 : eyeUp.Y = 1 : eyeUp.Z = 0
        ''
    End Sub
    Public Sub gl_DrawSatellite()
        '
        Dim rotMatrixSat(16) As Double
        Dim w, h, rmax As Single ' Width, height of central body and radius of bounding sphere
        Dim lp, wp As Double ' Length, width of solar panel
        Dim alf As Double ' Angle to rotate solar panel on the right face
        Static teta As Object ' Rotation angle of type 1 solar panel to animate 3D display
        Dim satscale As Double ' Global scale of satellite (to fill well the attitude sphere)
        Dim m$


        ' ScaleWidth et ScaleHeight remplacés pas Width et Height
        ' Il faut recalculer le point de vue avec viewport et perpesctive a chaque itération
        Gl.glPushMatrix()
        Gl.glMatrixMode(Gl.GL_PROJECTION)
        Gl.glLoadIdentity()
        Gl.glViewport(0, 0, Me.mass_pictSatellite.Width, Me.mass_pictSatellite.Height)
        Glu.gluPerspective(45, Me.mass_pictSatellite.Width / Me.mass_pictSatellite.Height, 0.5, 100)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glPopMatrix()

        Gl.glClear(Gl.GL_COLOR_BUFFER_BIT Or Gl.GL_DEPTH_BUFFER_BIT)

        Gl.glDisable(Gl.GL_CULL_FACE)
        '
        Wgl.wglMakeCurrent(m_intptrHdc_sat, hGLRC_sat)

        Gl.glClear(Gl.GL_COLOR_BUFFER_BIT Or Gl.GL_DEPTH_BUFFER_BIT)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glLoadIdentity()
        '
        ' Set the camera parameters & render the scene
        Glu.gluLookAt(eyePos.X, eyePos.Y, eyePos.Z, 0.0#, 0.0#, 0.0#, eyeUp.X, eyeUp.Y, eyeUp.Z)
        '
        Gl.glLineWidth(1)
        Gl.glEnable(Gl.GL_LIGHTING)
        Gl.glColor3f(1, 1, 1)
        Gl.glEnable(Gl.GL_LIGHTING)
        Gl.glEnable(Gl.GL_TEXTURE_2D)
        Gl.glEnable(Gl.GL_COLOR_MATERIAL)
        'Gl.glEnable(Gl.GL_POLYGON_SMOOTH)
        'on remplace le POLYGON_SMOOTH par POLYGON, sinon apparition de triangles sur certaines configs
        Gl.glEnable(Gl.GL_POLYGON)
        Gl.glEnable(Gl.GL_NORMALIZE)
        '
        Dim i As Short
        '
        Gl.glDisable(Gl.GL_COLOR_MATERIAL)
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glPushMatrix()
        Gl.glEnable((Gl.GL_TEXTURE_2D))
        '
        Gl.glRotated(-90, 1, 0, 0)
        Gl.glRotated(-90, 0, 0, 1)
        '
        ' Draw satellite body
        ' -------------------
        ' Preliminary calculation for scaling
        h = Satellite.satBodyHeight
        w = Satellite.satBodyWidth
        rmax = Sqrt(2 * w * w / 4 + h * h / 4) ' Bounding sphere radius
        satscale = 0.2 * 4 / rmax       ' So that rmax corresponds to 50% of the sphere radius
        '
        Gl.glEnable(Gl.GL_LIGHT1)
        Gl.glPushMatrix()
        Gl.glScalef(w * satscale, w * satscale, h * satscale) ' Fill at 50% of the attitude sphere, the radius of which is 4
        '
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, tx_sat) ' Goldfoil cover
        Gl.glColor4f(1, 1, 1, 1)
        For i = 0 To 3 ' Side faces
            Gl.glRotatef(90, 0, 0, 1)
            Gl.glBegin((Gl.GL_QUADS))
            Gl.glTexCoord2f(0, 0)
            Gl.glVertex3f(0.5, 0.5, -0.5)
            Gl.glTexCoord2f(0, 1)
            Gl.glVertex3f(0.5, 0.5, 0.5)
            Gl.glTexCoord2f(1, 1)
            Gl.glVertex3f(0.5, -0.5, 0.5)
            Gl.glTexCoord2f(1, 0)
            Gl.glVertex3f(0.5, -0.5, -0.5)
            Gl.glEnd()
        Next
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Upper face
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(0.5, 0.5, 0.5)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(-0.5, 0.5, 0.5)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(-0.5, -0.5, 0.5)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(0.5, -0.5, 0.5)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Lower face
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(0.5, 0.5, -0.5)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(-0.5, 0.5, -0.5)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(-0.5, -0.5, -0.5)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(0.5, -0.5, -0.5)
        Gl.glEnd()
        Gl.glPopMatrix()
        '
        ' Draw panels
        ' -----------
        For i = 1 To SolarPanels.Count()
            ' DEBUT DU TRACE PANNEAU
            lp = SolarPanels.Item(i).PanelLength
            wp = SolarPanels.Item(i).PanelWidth
            '
            Gl.glPushMatrix()
            '
            If SolarPanels.Item(i).paneltype = "Type1" Then
                '
                teta = teta + 0.25
                If teta >= 180 Then teta = teta - 360
                m$ = SolarPanels.Item(i).PanelFace
                Select Case m$
                    Case "+ X" : alf = 0
                    Case "+ Y" : alf = 90
                    Case "- X" : alf = 180
                    Case "- Y" : alf = 270
                End Select
                '
                Gl.glRotatef(alf, 0, 0, 1)
                Gl.glPushMatrix()
                Gl.glTranslatef(w / 2 * satscale, 0, -0.01) ' -0.01 to keep the axis visible (out of the panel)
                Gl.glScalef(wp / 2 * satscale, wp / 2 * satscale, wp / 2 * satscale)
                Gl.glRotatef(teta, 1, 0, 0)
                Gl.glCallList(panel2)
                Gl.glPopMatrix()
                Gl.glTranslatef((w / 2 + lp / 2 + wp / 2) * satscale, 0, -0.01) ' -0.01 to keep the axis visible (out of the panel)
                Gl.glScalef(lp / 2 * satscale, wp / 2 * satscale, wp / 2 * satscale)
                Gl.glRotatef(teta, 1, 0, 0)
                Gl.glCallList(panel1)
                '
            ElseIf SolarPanels.Item(i).paneltype = "Type2" Then
                m$ = SolarPanels.Item(i).PanelFace
                Select Case m$
                    Case "+ X"
                        Gl.glRotatef(90, 0, 1, 0)       ' This rotation to have the length of the panel along z axis
                        Gl.glTranslatef(0, 0, (1.025 * w / 2 + 0.025 * w) * satscale)
                    Case "+ Y"
                        Gl.glRotatef(90, 0, 0, 1)       ' This rotation to have the length of the panel along z axis
                        Gl.glRotatef(90, 0, 1, 0)
                        Gl.glTranslatef(0, 0, (w / 2 + 0.025 * w) * satscale)
                    Case "- X"
                        Gl.glRotatef(-90, 0, 1, 0)
                        Gl.glTranslatef(0, 0, (w / 2 + 0.025 * w) * satscale)
                    Case "- Y"
                        Gl.glRotatef(90, 0, 0, 1)       ' This rotation to have the length of the panel along z axis
                        Gl.glRotatef(-90, 0, 1, 0)
                        Gl.glTranslatef(0, 0, (w / 2 + 0.025 * w) * satscale)
                    Case "+ Z"
                        Gl.glTranslatef(0, 0, (h / 2 + 0.025 * w) * satscale)
                    Case "- Z"
                        Gl.glRotatef(180, 1, 0, 0)
                        Gl.glTranslatef(0, 0, (h / 2 + 0.025 * w) * satscale)
                End Select
                '
                Gl.glScalef(lp / 2 * satscale, wp / 2 * satscale, wp / 2 * satscale)
                Gl.glCallList(panel1)
                '
            Else
                m$ = SolarPanels.Item(i).PanelFace
                Select Case m$
                    Case "+ X" : alf = 0
                    Case "+ Y" : alf = 90
                    Case "- X" : alf = 180
                    Case "- Y" : alf = 270
                End Select
                '
                Gl.glRotatef(alf, 0, 0, 1)
                Gl.glTranslatef((w / 2 + lp / 2) * satscale, 0, 0)
                '
                m$ = SolarPanels.Item(i).PanelHpos
                Select Case m$
                    Case "Upper"
                        Gl.glTranslatef(0, 0, (h / 2 - 0.025 * h) * satscale)
                    Case "Lower"
                        Gl.glTranslatef(0, 0, -(h / 2 - 0.025 * h) * satscale)
                    Case "Middle"
                        Gl.glTranslatef(0, 0, -0.01 * satscale) ' to keep the axis visible (out of the panel)
                End Select
                '
                Gl.glScalef(lp / 2 * satscale, wp / 2 * satscale, wp / 2 * satscale)
                If SolarPanels.Item(i).PanelDir = "- Z" Then
                    Gl.glRotatef(180, 1, 0, 0)
                End If
                '
                Gl.glCallList(panel1)
                '
            End If
            '
            Gl.glPopMatrix()
            ' FIN TRACE PANNEAU
        Next
        '
        ' Draw x,y,z axis
        ' ---------------
        '
        Gl.glDisable(Gl.GL_LIGHTING) ' Axis aren't textured nor lighted
        Gl.glDisable(Gl.GL_TEXTURE_2D)
        Gl.glLineWidth(2)
        '
        Gl.glBegin(Gl.GL_LINES)
        Gl.glColor3f(1, 0, 0) 'x-axis in red
        Gl.glVertex3f(0, 0, 0)
        Gl.glVertex3f(1.5 * Max(w, h) * satscale, 0, 0)
        Gl.glColor3f(0, 1, 0) 'y-axis in green
        Gl.glVertex3f(0, 0, 0)
        Gl.glVertex3f(0, 1.5 * Max(w, h) * satscale, 0)
        Gl.glColor3f(0, 1, 1) 'z-axis in cyan
        Gl.glVertex3f(0, 0, 0)
        Gl.glVertex3f(0, 0, 1.5 * Max(w, h) * satscale)
        Gl.glEnd()
        '
        ' Draw labels (same colors as the axis)
        gl_DrawTextColored("X", 1.7 * Max(w, h) * satscale, 0, 0, &HFF, font2)
        gl_DrawTextColored("Y", 0, 1.7 * Max(w, h) * satscale, 0, &HFF00, font2)
        gl_DrawTextColored("Z", 0, 0, 1.7 * Max(w, h) * satscale, &HFFFF00, font2)
        '
        ' Reactive lighting and texturing
        Gl.glLineWidth(1)
        Gl.glEnable(Gl.GL_LIGHTING)
        Gl.glEnable((Gl.GL_TEXTURE_2D))
        '
        ' Restore OpenGl Modelview Matrix
        Gl.glPopMatrix()
        '
        Gl.glDisable(Gl.GL_LIGHTING)
        Gl.glDisable(Gl.GL_TEXTURE_2D)
        Gl.glDisable(Gl.GL_NORMALIZE)
        Gl.glDisable(Gl.GL_POLYGON_SMOOTH)
        Gl.glDisable(Gl.GL_COLOR_MATERIAL)
        Gl.glDisable(Gl.GL_TEXTURE_2D)
        Gl.glDisable(Gl.GL_LIGHTING)
        '
        Gl.glFlush()
        Gdi.SwapBuffers(m_intptrHdc_sat)
        'Gdi.SwapBuffers(User.GetDC(pan_pictSatellite.Handle))
        GC.Collect()
        ''
    End Sub
    Sub GLPanelList()
        '
        Dim i As Short
        '
        ' Solar panel
        ' -----------
        panel1 = Gl.glGenLists(1)
        Gl.glNewList(panel1, Gl.GL_COMPILE)
        '
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, tx_panel) ' Solarpanel
        Gl.glColor4f(1, 1, 1, 1)
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Upper face
        Gl.glTexCoord2f(0, 0)
        Gl.glVertex3f(1, 1, 0.03)
        Gl.glTexCoord2f(0, 1)
        Gl.glVertex3f(-1, 1, 0.03)
        Gl.glTexCoord2f(1, 1)
        Gl.glVertex3f(-1, -1, 0.03)
        Gl.glTexCoord2f(1, 0)
        Gl.glVertex3f(1, -1, 0.03)
        Gl.glEnd()
        '
        Gl.glDisable(Gl.GL_LIGHTING)
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, 0) ' Solarpanel
        Gl.glColor4f(0.2, 0.2, 0.2, 1)
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Lower face
        Gl.glVertex3f(1, 1, -0.03)
        Gl.glVertex3f(-1, 1, -0.03)
        Gl.glVertex3f(-1, -1, -0.03)
        Gl.glVertex3f(1, -1, -0.03)
        Gl.glEnd()
        '
        Gl.glColor4f(0.5, 0.5, 0.5, 1)
        For i = 0 To 3 ' Side faces
            Gl.glRotated(90 * i, 0, 0, 1)
            Gl.glBegin((Gl.GL_QUADS))
            Gl.glVertex3f(1, 1, -0.03)
            Gl.glVertex3f(1, 1, 0.03)
            Gl.glVertex3f(1, -1, 0.03)
            Gl.glVertex3f(1, -1, -0.03)
            Gl.glEnd()
        Next
        '
        Gl.glEnable(Gl.GL_LIGHTING)
        '
        Gl.glEndList()
        '
        ' Mechanism
        ' ---------
        panel2 = Gl.glGenLists(1)
        Gl.glNewList(panel2, Gl.GL_COMPILE)
        '
        Gl.glDisable(Gl.GL_LIGHTING)
        Gl.glBindTexture(Gl.GL_TEXTURE_2D, 0)
        Gl.glColor4f(0.4, 0.4, 0.4, 1)
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Upper face
        Gl.glVertex3f(0, 0.1, 0.03)
        Gl.glVertex3f(0, -0.1, 0.03)
        Gl.glVertex3f(0.9, -0.9, 0.03)
        Gl.glVertex3f(0.9, -0.7, 0.03)
        Gl.glEnd()
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0, 0.1, 0.03)
        Gl.glVertex3f(0, -0.1, 0.03)
        Gl.glVertex3f(0.9, 0.7, 0.03)
        Gl.glVertex3f(0.9, 0.9, 0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, -0.9, 0.03)
        Gl.glVertex3f(0.99, -0.9, 0.03)
        Gl.glVertex3f(0.99, 0.9, 0.03)
        Gl.glVertex3f(0.9, 0.9, 0.03)
        Gl.glEnd()
        '
        Gl.glColor4f(0.4, 0.4, 0.4, 1)
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Upper face
        Gl.glVertex3f(0, 0.1, -0.03)
        Gl.glVertex3f(0, -0.1, -0.03)
        Gl.glVertex3f(0.9, -0.9, -0.03)
        Gl.glVertex3f(0.9, -0.7, -0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0, 0.1, -0.03)
        Gl.glVertex3f(0, -0.1, -0.03)
        Gl.glVertex3f(0.9, 0.7, -0.03)
        Gl.glVertex3f(0.9, 0.9, -0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, -0.9, -0.03)
        Gl.glVertex3f(0.99, -0.9, -0.03)
        Gl.glVertex3f(0.99, 0.9, -0.03)
        Gl.glVertex3f(0.9, 0.9, -0.03)
        Gl.glEnd()
        '
        Gl.glColor4f(0.3, 0.3, 0.3, 1)
        '
        Gl.glBegin((Gl.GL_QUADS)) ' Sides
        Gl.glVertex3f(0.9, -0.9, -0.03)
        Gl.glVertex3f(0.9, -0.9, 0.03)
        Gl.glVertex3f(0, -0.1, 0.03)
        Gl.glVertex3f(0, -0.1, -0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, 0.9, -0.03)
        Gl.glVertex3f(0.9, 0.9, 0.03)
        Gl.glVertex3f(0, 0.1, 0.03)
        Gl.glVertex3f(0, 0.1, -0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, -0.7, -0.03)
        Gl.glVertex3f(0.9, -0.7, 0.03)
        Gl.glVertex3f(0, 0.1, 0.03)
        Gl.glVertex3f(0, 0.1, -0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, 0.7, -0.03)
        Gl.glVertex3f(0.9, 0.7, 0.03)
        Gl.glVertex3f(0, -0.1, 0.03)
        Gl.glVertex3f(0, -0.1, -0.03)
        Gl.glEnd()
        '
        Gl.glBegin((Gl.GL_QUADS))
        Gl.glVertex3f(0.9, -0.9, -0.03)
        Gl.glVertex3f(0.9, -0.9, 0.03)
        Gl.glVertex3f(0.9, 0.9, 0.03)
        Gl.glVertex3f(0.9, 0.9, -0.03)
        Gl.glEnd()
        '
        Gl.glEnable(Gl.GL_LIGHTING)
        '
        Gl.glEndList()
        ''
    End Sub

    ''' <summary>
    ''' Drawing update at each tick (100 ms interval)
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub tmrAnimate_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tmrAnimate.Tick
        '
        gl_DrawSatellite()
        ''
    End Sub

    ''' <summary>
    ''' Make a transition in drawing update (10 ms interval)
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub tmrTransition_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tmrTransition.Tick
        '
        Dim progress, progressF As Double
        '
        ' Calculate the current progress
        progress = (GetTickCount - transitionStartTime) / transitionDuration
        If progress > 1.0# Then progress = 1.0#

        ' We don't want a linear progression, so apply a function to the value
        ' (start fast and slow down gradually)
        progressF = ((progress - 1) ^ 3 + 1)
        '
        ' Calculate the eye position for this step of the transition
        eyeHorAngle = transitionHorStart + transitionHorAngle * progressF
        eyeVertAngle = transitionVertStart + transitionVertAngle * progressF
        eyePos.X = eye_distance * Cos(eyeVertAngle) * Sin(eyeHorAngle)
        eyePos.Y = eye_distance * Sin(eyeVertAngle)
        eyePos.Z = eye_distance * Cos(eyeVertAngle) * Cos(eyeHorAngle)

        ' If we're done, disable the timer
        If progress = 1.0# Then
            tmrTransition.Enabled = False
        End If
        '
        ' Redraw the satellite
        gl_DrawSatellite()
        ''
    End Sub

    ''' <summary>
    ''' Calculate the angles that correspond to a position
    ''' </summary>
    ''' <param name="Pos"></param>
    ''' <param name="horAngle"></param>
    ''' <param name="vertAngle"></param>
    ''' <remarks></remarks>
    Private Sub calcAnglesFromPosition(ByRef Pos As vector, ByRef horAngle As Double, ByRef vertAngle As Double)
        '
        Dim xz, xzOriginal As vector
        '
        vertAngle = Math.Asin(Pos.Y / vctNorm(Pos)) ' calculate the elevation angle (simple trigonometry)
        xz.X = Pos.X : xz.Y = 0 : xz.Z = Pos.Z ' project the vector into the xz plane
        If xz.X = 0 And xz.Y = 0 And xz.Z = 0 Then
            horAngle = 0 ' the horizontal angle is arbitrary for a 90° vertical rotation
        Else
            xzOriginal.X = 0 : xzOriginal.Y = 0 : xzOriginal.Z = 1 ' original position (horAngle and vertAngle = 0) is on the positive z-axis
            ' calculate the rotation angle in the xz plane relative to the z-axis
            horAngle = Math.Acos(vctDot(xzOriginal, xz) / vctNorm(xzOriginal) / vctNorm(xz))
            If Pos.X < 0 Then horAngle = -horAngle ' ArcCos returns an angle between 0 and pi; for angles greater than pi, the x coordinate is negative and we need to use the negative angle
        End If
        ''
    End Sub

    ''' <summary>
    ''' Camera transition from the current eye position to a position facing the solar panel
    ''' </summary>
    ''' <param name="PanelFace"></param>
    ''' <remarks></remarks>
    Private Sub CameraTransition(ByRef PanelFace As String)
        '
        Dim targetPos As vector
        Dim horEnd, vertEnd As Double
        Dim temp As Double
        '
        ' Calculate the angles that correspond to the current eye position
        Call calcAnglesFromPosition(eyePos, transitionHorStart, transitionVertStart)
        '
        ' Get the target position in the OpenGL coordinate system
        With targetPos
            Select Case PanelFace
                Case "+ X" : .X = 1 : .Y = 0.4 : .Z = 0.4
                Case "+ Y" : .X = -0.4 : .Y = 1 : .Z = 0.4
                Case "- X" : .X = -1 : .Y = -0.4 : .Z = 0.4
                Case "- Y" : .X = 0.4 : .Y = -1 : .Z = 0.4
                Case "+ Z" : .X = 0.4 : .Y = 0.4 : .Z = 1
                Case "- Z" : .X = 0.4 : .Y = 0.4 : .Z = -1
            End Select
            ' This just "rotates" the values of the vector
            temp = .X
            .X = .Y
            .Y = .Z
            .Z = temp

            '.ConvertFromEngineeringCOS
        End With
        '
        ' Calculate the angles that correspond to the target position
        Call calcAnglesFromPosition(targetPos, horEnd, vertEnd)
        '
        ' Calculate the transition angles
        transitionHorAngle = horEnd - transitionHorStart
        If transitionHorAngle > pi Then transitionHorAngle = transitionHorAngle - 2 * pi ' -pi < angle < pi
        transitionVertAngle = vertEnd - transitionVertStart
        If transitionVertAngle > pi Then transitionVertAngle = transitionVertAngle - 2 * pi ' -pi < angle < pi
        '
        ' If there is a rotation to be performed, set the time variables
        If transitionHorAngle <> 0 Or transitionVertAngle <> 0 Then
            transitionDuration = 1500
            transitionStartTime = GetTickCount
            ' Start the transition: enable the timer, which will envoke tmrTransition_Timer()
            tmrTransition.Enabled = True
        End If
        ''
    End Sub

#End Region

    Public Sub att_conf_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        '
        ' Fill combo gradient gravity boom
        With mass_cmbBoomFace
            .Enabled = True
            .Items.Clear()
            .Items.Add("+ X")
            .Items.Add("- X")
            .Items.Add("+ Y")
            .Items.Add("- Y")
            .Items.Add("+ Z")
            .Items.Add("- Z")
            .Text = "+ X"
        End With
        '
        mass_txtPanelLength.Text = CStr(1)
        mass_txtPanelWidth.Text = CStr(1)
        mass_txtPanelMass.Text = CStr(1)
        '
        update_attitude_conf()
        update_attitude_data()

        tabPower.TabPages("Onglet_att_manag").Enabled = False     'THIS LINE DISABLES THE MANAGEMENT TAB. DELETE IT WHEN EVENTS WORKS
        ''
    End Sub

    Public Sub update_attitude_conf()
        '
        Dim i As Short
        Dim X As String
        On Error GoTo err_2
        '
        ' ----------------
        ' Mass, geometry TAB
        ' ----------------
        ' Central body
        mass_txtBodyHeight.Text = Satellite.satBodyHeight
        mass_txtBodyWidth.Text = Satellite.satBodyWidth
        mass_txtBodyMass.Text = BodyMass
        mass_txtGx.Text = Format(BodyCG.X, "#0.000")
        mass_txtGy.Text = Format(BodyCG.Y, "#0.000")
        mass_txtGz.Text = Format(BodyCG.Z, "#0.000")
        mass_txtIxx.Text = Format(BodyInertia(0, 0), "#0.000")
        mass_txtIyy.Text = Format(BodyInertia(1, 1), "#0.000")
        mass_txtIzz.Text = Format(BodyInertia(2, 2), "#0.000")
        mass_txtIxy.Text = Format(BodyInertia(0, 1), "#0.000")
        mass_txtIxz.Text = Format(BodyInertia(0, 2), "#0.000")
        mass_txtIyz.Text = Format(BodyInertia(1, 2), "#0.000")
        mass_txtBoomMass.Text = Format(GravityMass, "#0.000")
        If GravityMass <> 0 And vctNorm(GravityDist) <> 0 Then
            mass_txtBoomDist.Text = Abs(GravityDist.X + GravityDist.Y + GravityDist.Z)
            If GravityDist.X > 0 Then
                mass_cmbBoomFace.Text = "+ X"
            ElseIf GravityDist.X < 0 Then
                mass_cmbBoomFace.Text = "- X"
            ElseIf GravityDist.Y > 0 Then
                mass_cmbBoomFace.Text = "+ Y"
            ElseIf GravityDist.Y < 0 Then
                mass_cmbBoomFace.Text = "- Y"
            ElseIf GravityDist.Z > 0 Then
                mass_cmbBoomFace.Text = "+ Z"
            ElseIf GravityDist.Z < 0 Then
                mass_cmbBoomFace.Text = "- Z"
            End If
        End If
        '
        ' Panels data 
        mass_lstPanels.Items.Clear()
        PanelsNumber = SolarPanels.Count() ' Update PanelsNumber
        For i = 1 To PanelsNumber
            With SolarPanels.Item(i)
                mass_lstPanels.Items.Add(.PanelName)
            End With
        Next
        '
        ' Select the last Panel in the ListBox
        mass_lstPanels.SelectedIndex = mass_lstPanels.Items.Count - 1
        '
        ' Redraw the satellite
        If mass_lstPanels.Items.Count <> 0 Then
            CameraTransition(SolarPanels.Item(mass_lstPanels.Items.Count).PanelFace)
        End If
        '
        ' ------------------------
        ' Satellite properties TAB
        ' ------------------------
        ' Pressure coefficients
        prop_txtCN_0.Text = Format(Cn_aero(0), "#0.000")
        prop_txtCN_1.Text = Format(Cn_aero(1), "#0.000")
        prop_txtCN_2.Text = Format(Cn_aero(2), "#0.000")
        prop_txtCN_3.Text = Format(Cn_aero(3), "#0.000")
        prop_txtCN_4.Text = Format(Cn_aero(4), "#0.000")
        prop_txtCN_5.Text = Format(Cn_aero(5), "#0.000")
        prop_txtCT_0.Text = Format(Ct_aero(0), "#0.000")
        prop_txtCT_1.Text = Format(Ct_aero(1), "#0.000")
        prop_txtCT_2.Text = Format(Ct_aero(2), "#0.000")
        prop_txtCT_3.Text = Format(Ct_aero(3), "#0.000")
        prop_txtCT_4.Text = Format(Ct_aero(4), "#0.000")
        prop_txtCT_5.Text = Format(Ct_aero(5), "#0.000")
        '
        ' Sun radiation coefficients
        prop_txtCR_0.Text = Format(Cr_solar(0), "#0.000")
        prop_txtCR_1.Text = Format(Cr_solar(1), "#0.000")
        prop_txtCR_2.Text = Format(Cr_solar(2), "#0.000")
        prop_txtCR_3.Text = Format(Cr_solar(3), "#0.000")
        prop_txtCR_4.Text = Format(Cr_solar(4), "#0.000")
        prop_txtCR_5.Text = Format(Cr_solar(5), "#0.000")
        prop_txtCD_0.Text = Format(Cd_solar(0), "#0.000")
        prop_txtCD_1.Text = Format(Cd_solar(1), "#0.000")
        prop_txtCD_2.Text = Format(Cd_solar(2), "#0.000")
        prop_txtCD_3.Text = Format(Cd_solar(3), "#0.000")
        prop_txtCD_4.Text = Format(Cd_solar(4), "#0.000")
        prop_txtCD_5.Text = Format(Cd_solar(5), "#0.000")
        '
        ' Magnetic dipole (because of passive properties)
        prop_txtMG_X.Text = Format(dipsat_passive.X, "#0.000")
        prop_txtMG_Y.Text = Format(dipsat_passive.Y, "#0.000")
        prop_txtMG_Z.Text = Format(dipsat_passive.Z, "#0.000")
        '
        ' --------------
        ' Actuactors TAB
        ' --------------
        ' Flywheel
        act_txtIRX.Text = Format(JR(0), "#0.000")
        act_txtIRY.Text = Format(JR(1), "#0.000")
        act_txtIRZ.Text = Format(JR(2), "#0.000")
        act_txtWRX.Text = Format(om(0), "#0.000")
        act_txtWRY.Text = Format(om(1), "#0.000")
        act_txtWRZ.Text = Format(om(2), "#0.000")
        '
        ' Magnetic dipole (because of magnetic control system)
        act_txtMG_X.Text = Format(dipsat_active.X, "#0.000")
        act_txtMG_Y.Text = Format(dipsat_active.Y, "#0.000")
        act_txtMG_Z.Text = Format(dipsat_active.Z, "#0.000")
        '
        ' Damper
        act_txtMX.Text = Format(DampersMass(0), "#0.000")
        act_txtMY.Text = Format(DampersMass(1), "#0.000")
        act_txtMZ.Text = Format(DampersMass(2), "#0.000")
        act_txtDX.Text = Format(DampersDist(0), "#0.000")
        act_txtDY.Text = Format(DampersDist(1), "#0.000")
        act_txtDZ.Text = Format(DampersDist(2), "#0.000")
        act_txtCX.Text = Format(c(0), "#0.000")
        act_txtCY.Text = Format(c(1), "#0.000")
        act_txtCZ.Text = Format(c(2), "#0.000")
        act_txtKX.Text = Format(kd(0), "#0.000")
        act_txtKY.Text = Format(kd(1), "#0.000")
        act_txtKZ.Text = Format(kd(2), "#0.000")
        '
        ' Nozzle
        act_txtMN_X.Text = Format(jet(0), "#0.000")
        act_txtMN_Y.Text = Format(jet(1), "#0.000")
        act_txtMN_Z.Text = Format(jet(2), "#0.000")
        '
        Exit Sub
        '
err_2:
        ''
    End Sub

    Private Sub update_attitude_data()
        '
        ' Author: Wolfgang Hausmann (06.12.99)
        '
        ' Description:
        ' - calculates whole satellite mass
        ' - calculates whole satellite inertia
        ' - calculates whole satellite center of gravity
        '
        ' Note :
        ' This subroutine takes into account the central body, the gravity gradient stabilisation device and the panels
        ' The mass of dampers if exists must be taking into account in the mass and inertia of central body
        '
        ' Input:   BodyMass         ... mass of central body
        '          BodyInertia()    ... inertia of central body
        '          BodyCG           ... centre of gravity of central body (vector)
        '          GravityMass      ... mass of gravitational device
        '          GravityDist      ... position of gravity mass for gravity gradient stabilisation (vector)
        '          DampersMass()    ... masses of dampers
        '          DampersDist()    ... distance of dampers from center of gravity
        '
        ' Output:  SatMass          ... satellite mass
        '          SatInertia()     ... satellite inertia at center of gravity
        '          SatCG            ... satellite centre of gravity
        '
        Dim i As Integer
        Dim TempPanel As CSolarPanel
        Dim PanelCG As vector
        'Dim PanelInertia(2) As Single           ' Inertia of panel around its C.G 0:Ixx, 1:Iyy, 2:Izz
        '
        Dim GravityInertia(,) As Double = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}
        Dim PanelInertia(2, 2) As Double        ' (NEW: 03.05.17) PanelInertia should be represented by a matrix rather than a vector

        '


        Dim PanelCGs As New ArrayList


        ' Calculate satellite mass without panels
        ' ---------------------------------------
        SatMass = BodyMass + GravityMass + DampersMass(0) + DampersMass(1) + DampersMass(2)
        '
        ' Calculate satellite mass and centre of gravity
        ' -------------------------------------
        SatCG = vctMultScalar(vctAdd(vctMultScalar(BodyCG, BodyMass), vctMultScalar(GravityDist, GravityMass)), 1 / (BodyMass + GravityMass))



        For i = 1 To PanelsNumber
            TempPanel = SolarPanels.Item(i)
            With TempPanel
                Select Case .PanelType
                    Case "Type1"
                        Select Case .PanelFace
                            Case "+ X" : PanelCG.X = (.PanelLength + Satellite.satBodyWidth) / 2 : PanelCG.Y = 0
                            Case "- X" : PanelCG.X = -(.PanelLength + Satellite.satBodyWidth) / 2 : PanelCG.Y = 0
                            Case "+ Y" : PanelCG.X = 0 : PanelCG.Y = (.PanelLength + Satellite.satBodyWidth) / 2
                            Case "- Y" : PanelCG.X = 0 : PanelCG.Y = -(.PanelLength + Satellite.satBodyWidth) / 2
                        End Select
                        PanelCG.Z = 0
                    Case "Type2"
                        Select Case .PanelFace
                            Case "+ X" : PanelCG.X = Satellite.satBodyWidth / 2 : PanelCG.Y = 0 : PanelCG.Z = 0
                            Case "- X" : PanelCG.X = -Satellite.satBodyWidth / 2 : PanelCG.Y = 0 : PanelCG.Z = 0
                            Case "+ Y" : PanelCG.X = 0 : PanelCG.Y = Satellite.satBodyWidth / 2 : PanelCG.Z = 0
                            Case "- Y" : PanelCG.X = 0 : PanelCG.Y = -Satellite.satBodyWidth / 2 : PanelCG.Z = 0
                            Case "+ Z" : PanelCG.X = 0 : PanelCG.Y = 0 : PanelCG.Z = Satellite.satBodyHeight / 2
                            Case "- Z" : PanelCG.X = 0 : PanelCG.Y = 0 : PanelCG.Z = -Satellite.satBodyHeight / 2
                        End Select
                    Case "Type3"
                        Select Case .PanelFace
                            Case "+ X" : PanelCG.X = (.PanelLength + Satellite.satBodyWidth) / 2 : PanelCG.Y = 0
                            Case "- X" : PanelCG.X = -(.PanelLength + Satellite.satBodyWidth) / 2 : PanelCG.Y = 0
                            Case "+ Y" : PanelCG.X = 0 : PanelCG.Y = (.PanelLength + Satellite.satBodyWidth) / 2
                            Case "- Y" : PanelCG.X = 0 : PanelCG.Y = -(.PanelLength + Satellite.satBodyWidth) / 2
                        End Select
                        Select Case .PanelHpos
                            Case "Upper" : PanelCG.Z = Satellite.satBodyHeight / 2
                            Case "Lower" : PanelCG.Z = -Satellite.satBodyHeight / 2
                            Case "Middle" : PanelCG.Z = 0
                        End Select
                End Select
                '
                SatCG = vctMultScalar(vctAdd(vctMultScalar(SatCG, SatMass), vctMultScalar(PanelCG, CDbl(TempPanel.PanelMass))), 1 / (SatMass + .PanelMass))
            End With
            SatMass += TempPanel.PanelMass  ' Update mass with panels
            PanelCGs.Add(PanelCG)
        Next

        ' (NEW: 31.07.18) simplified the code by utilizing the function huygens instead of doing it line by line

        SatInertia = Huygens(BodyInertia, BodyMass, BodyCG, SatCG)
        GravityInertia = Huygens(GravityInertia, GravityMass, GravityDist, SatCG)
        SatInertia = MatrixAdd(SatInertia, GravityInertia)

        '
        '
        ''''''''''''
        Dim j As Integer = 0
        ''''''''''
        For i = 1 To PanelsNumber
            TempPanel = SolarPanels.Item(i)
            PanelInertia = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}        ' (NEW: 03.05.17) Resetting panel inertia matrix in case there are several solar panels
            With TempPanel
                ' Calculate inertia of panel around its C.G
                Select Case .PanelType
                    Case "Type1"
                        Select Case .PanelFace  ' Prise en compte des panneaux solaires dans le calcul de l'inertie
                            Case "+ X", "- X"
                                PanelInertia(0, 0) = .PanelWidth ^ 2 * .PanelMass / 12
                                PanelInertia(1, 1) = .PanelLength ^ 2 * .PanelMass / 12
                                PanelInertia(2, 2) = (.PanelLength ^ 2 + .PanelWidth ^ 2) * .PanelMass / 12
                                PanelInertia = MatrixNewBasis_FromRotation(PanelInertia, "X", .PanelRollAngle)  ' (NEW: 03.05.17) Calculate panel inertia matrix in satellite's basis

                            Case "+ Y", "- Y"
                                PanelInertia(0, 0) = .PanelLength ^ 2 * .PanelMass / 12
                                PanelInertia(1, 1) = .PanelWidth ^ 2 * .PanelMass / 12
                                PanelInertia(2, 2) = (.PanelLength ^ 2 + .PanelWidth ^ 2) * .PanelMass / 12
                                PanelInertia = MatrixNewBasis_FromRotation(PanelInertia, "Y", .PanelRollAngle)  ' (NEW: 03.05.17) Calculate panel inertia matrix in satellite's basis
                        End Select
                    Case "Type2"
                        Select Case .PanelFace
                            Case "+ X", "- X"
                                PanelInertia(0, 0) = (.PanelLength ^ 2 + .PanelWidth ^ 2) * .PanelMass / 12 : PanelInertia(1, 1) = .PanelLength ^ 2 * .PanelMass / 12 : PanelInertia(2, 2) = .PanelWidth ^ 2 * .PanelMass / 12
                            Case "+ Y", "- Y"
                                PanelInertia(0, 0) = .PanelLength ^ 2 * .PanelMass / 12 : PanelInertia(1, 1) = (.PanelLength ^ 2 + .PanelWidth ^ 2) * .PanelMass / 12 : PanelInertia(2, 2) = .PanelWidth ^ 2 * .PanelMass / 12
                            Case "+ Z", "- Z"
                                PanelInertia(0, 0) = .PanelWidth ^ 2 * .PanelMass / 12 : PanelInertia(1, 1) = .PanelLength ^ 2 * .PanelMass / 12 : PanelInertia(2, 2) = (.PanelLength ^ 2 + .PanelWidth ^ 2) * .PanelMass / 12
                        End Select
                    Case "Type3"
                        Select Case .PanelFace
                            Case "+ X", "- X"
                                PanelInertia(0, 0) = .PanelWidth ^ 2 * .PanelMass / 12 : PanelInertia(1, 1) = .PanelLength ^ 2 * .PanelMass / 12
                            Case "+ Y", "- Y"
                                PanelInertia(0, 0) = .PanelLength ^ 2 * .PanelMass / 12 : PanelInertia(1, 1) = .PanelWidth ^ 2 * .PanelMass / 12
                        End Select
                        PanelInertia(2, 2) = (.PanelLength ^ 2 + .PanelWidth ^ 2) * .PanelMass / 12
                End Select


                '
                ' (NEW: 03.05.17) Huygens theorem
                ' (NEW: 26.07.18) each iteration the correct value of PanelCG is used

                Dim cg As vector = PanelCGs.Item(j)
                PanelInertia = Huygens(PanelInertia, CDbl(.PanelMass), cg, SatCG)
                j += 1



                ' (NEW: 03.05.17) Now that SatInertia and PanelInertia are calculated at the same point (SatCG) 
                ' and in the same basis (Satellite's basis), they can be added
                SatInertia = MatrixAdd(SatInertia, PanelInertia)

                '
            End With
        Next


        '
        ' Calculates the distance from the centre of mass to the surfaces
        ' ---------------------------------------------------------------
        'Call Rs_vect()
        '
        ' Display data in  'synthesis' box
        ' ------------

        mass_lblSatMass.Text = Format(SatMass, "#0.000")
        mass_lblSatGx.Text = Format(SatCG.X, "#0.000")
        mass_lblSatGy.Text = Format(SatCG.Y, "#0.000")
        mass_lblSatGz.Text = Format(SatCG.Z, "#0.000")
        mass_lblSatIxx.Text = Format(SatInertia(0, 0), "#0.000")
        mass_lblSatIyy.Text = Format(SatInertia(1, 1), "#0.000")
        mass_lblSatIzz.Text = Format(SatInertia(2, 2), "#0.000")
        mass_lblSatIxy.Text = Format(SatInertia(0, 1), "#0.000")
        mass_lblSatIxz.Text = Format(SatInertia(0, 2), "#0.000")
        mass_lblSatIyz.Text = Format(SatInertia(1, 2), "#0.000")

        ''
    End Sub

    ''' <summary>
    ''' Define shortcuts Enter
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmAttConf_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyData = Keys.Enter Then
            mass_lblmsg.Focus()
            e.SuppressKeyPress = True ' Pas besoin que l'évènement KeyPress le gère on vient de le faire
        End If
    End Sub

#Region "Mass, geometry TAB"

    ''' <summary>
    ''' Change orientations in the orientation combo according to the face on which the panel is mounted
    ''' (Only for the orientable panels)
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_cmbFace_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_cmbFace.SelectedIndexChanged
        '
        If mass_optPanelType_0.Checked Then
            Select Case mass_cmbFace.Text
                '
                Case "+ X", "- X"
                    With mass_cmbDirection
                        .Enabled = True
                        .Items.Clear()
                        .Items.Add("+ Y")
                        .Items.Add("+ Z")
                        .Items.Add("- Y")
                        .Items.Add("- Z")
                        .SelectedIndex = 0
                    End With
                    '
                Case "+ Y", "- Y"
                    With mass_cmbDirection
                        .Enabled = True
                        .Items.Clear()
                        .Items.Add("+ X")
                        .Items.Add("+ Z")
                        .Items.Add("- X")
                        .Items.Add("- Z")
                        .SelectedIndex = 0
                    End With
                    '
            End Select
            '
        End If
        ''
    End Sub

    ''' <summary>
    ''' Create the new panel and set its characteristics
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_cmdAddPanel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_cmdAddPanel.Click
        '
        Dim TempPanel As New CSolarPanel
        Dim i As Integer
        Dim TestPanel As New CSolarPanel
        '
        If mass_lstPanels.Items.Count = 8 Then
            MsgBox("You cannot mount more than 8 panels") : Exit Sub
        End If
        '
        With TempPanel
            If mass_optPanelType_0.Checked = True Then .PanelType = "Type1"
            If mass_optPanelType_1.Checked = True Then .PanelType = "Type2"
            If mass_optPanelType_2.Checked = True Then .PanelType = "Type3"
            .PanelLength = CSng(mass_txtPanelLength.Text)
            .PanelWidth = CSng(mass_txtPanelWidth.Text)
            .PanelMass = CSng(mass_txtPanelMass.Text)
            .PanelFace = mass_cmbFace.Text
            .PanelDir = mass_cmbDirection.Text
            .PanelHpos = mass_cmbPosition.Text
            .PanelNbString = mass_txtPanelNStrings.Text
            .PanelSurf = .PanelLength * .PanelWidth
            If .PanelType = "Type3" Then
                .PanelName = mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & " : " & .PanelDir & "]"
            Else
                .PanelName = mass_lstPanels.Items.Count + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
            End If
            '
            For i = 1 To PanelsNumber
                TestPanel = SolarPanels(i)
                ' Check if the panel doesn't already exist
                If TestPanel.PanelType = .PanelType And TestPanel.PanelFace = .PanelFace And TestPanel.PanelHpos = .PanelHpos Then
                    MsgBox("This panel already exists") : Exit Sub
                End If
                ' Check if the panel doesn't interfere with an existing panel (typically a Type1 and a Type3 "Middle")
                If TestPanel.PanelType = "Type1" And .PanelType = "Type3" And .PanelHpos = "Middle" And TestPanel.PanelFace = .PanelFace Then
                    MsgBox("You can't superimpose 2 panels") : Exit Sub
                End If
                If TestPanel.PanelType = "Type3" And .PanelType = "Type1" And TestPanel.PanelHpos = "Middle" And TestPanel.PanelFace = .PanelFace Then
                    MsgBox("You can't superimpose 2 panels") : Exit Sub
                End If
            Next
            '
            ' If not, add it to the ListBox
            mass_lstPanels.Items.Add((.PanelName))
            'Default values to allow fast checking and avoiding possible NaN
            .PanelSpecHeat = 10000 : .PanelCellAlf = 0.5 : .PanelCellEps = 0.5 : .PanelBackAlf = 0.5 : .PanelBackEps = 0.5 : .PanelConduction = 5
        End With
        '
        SolarPanels.Add(TempPanel)
        PanelsNumber = SolarPanels.Count() ' Update PanelsNumber
        '
        ' Select the added Panel in the ListBox
        mass_lstPanels.SelectedIndex = mass_lstPanels.Items.Count - 1
        '
        update_attitude_data()
        blnIsNewPrj = True
        '
        ' Redraw the satellite
        CameraTransition(SolarPanels.Item(mass_lstPanels.Items.Count).PanelFace)
        ''
        'Update power configuration
        update_power()
    End Sub
    ''' <summary>
    ''' Remove an existing panel
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_cmdRemovePanel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_cmdRemovePanel.Click
        '
        Dim Index, i As Short
        '
        If (mass_lstPanels.SelectedIndex <> -1) Then
            '
            Index = mass_lstPanels.SelectedIndex + 1
            SolarPanels.Remove((Index))
            '
            ' Change the name of panels (to have numbers from 1 to panelsnumber)
            mass_lstPanels.Items.Clear()
            PanelsNumber = SolarPanels.Count() ' Update PanelsNumber
            For i = 1 To PanelsNumber
                With SolarPanels.Item(i)
                    If .PanelType = "Type3" Then
                        .PanelName = i & "_" & .PanelType & " [" & .PanelFace & " : " & .paneldir & "]"
                    Else
                        .PanelName = i & "_" & .PanelType & " [" & .PanelFace & "]"
                    End If
                    mass_lstPanels.Items.Add(.PanelName)
                End With
            Next
            '
        End If
        '
        ' Select the last Panel in the ListBox
        mass_lstPanels.SelectedIndex = mass_lstPanels.Items.Count - 1
        '
        update_attitude_data()
        blnIsNewPrj = True
        '
        ' Redraw the satellite
        If mass_lstPanels.Items.Count <> 0 Then
            CameraTransition(SolarPanels.Item(mass_lstPanels.Items.Count).PanelFace)
        End If
        ''
        ' Graph Power window updating
        ''''''''''''''''''''''''''''''
        'Delecting panel "propreties" if there aren't panels
        If mass_lstPanels.Items.Count = 0 Then
            frmGraphPwr.optParGen.Checked = True
        End If
        'Modifying the list of properties added to be plotted

        Dim Compareto As String = "P" & Index.ToString
        Dim Compare As String
        Dim j As Integer = 0

        If frmGraphPwr.lstData.Items.Count = 0 Then Exit Sub

        'Delete the properties to be plot from the panel deleted
        Do
            Compare = frmGraphPwr.lstData.Items(j)
            If Compare.StartsWith(Compareto) Then
                frmGraphPwr.lstData.Items.Remove(frmGraphPwr.lstData.Items(j))
                j = 0
            Else
                j = j + 1
            End If
        Loop While j <= (frmGraphPwr.lstData.Items.Count - 1)

        'Modify list in Graph Power
        If Index > mass_lstPanels.Items.Count Or frmGraphPwr.lstData.Items.Count = 0 Then Exit Sub 'Goes out if there aren't items or we hve removed the last panel in the list

        For j = 0 To (frmGraphPwr.lstData.Items.Count - 1)   'We look every item in the list

            For k As Integer = (Index + 1) To mass_lstPanels.Items.Count + 1 'We look if it is from any panel with an index bigger than the on ewe have removed

                Compareto = "P" + k.ToString
                Compare = frmGraphPwr.lstData.Items(j)

                If Compare.StartsWith(Compareto) Then
                    frmGraphPwr.lstData.Items(j) = Replace(Compare, k, k - 1, 1, 1)  'If we find one, we modify its index to the previous one
                End If

            Next

        Next
        'Update power configuration
        update_power()
    End Sub

    ''' <summary>
    ''' Modify the selected panel in listpanels
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_cmdModify_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_cmdModify.Click
        '
        Dim i As Integer
        Dim TestPanel As New CSolarPanel
        Dim ProvType As String = ""
        '
        If SolarPanels.Count() = 0 Then Exit Sub
        With SolarPanels.Item(mass_lstPanels.SelectedIndex + 1)
            If mass_optPanelType_0.Checked = True Then
                ProvType = "Type1"
            End If
            If mass_optPanelType_1.Checked = True Then
                ProvType = "Type2"
            End If
            If mass_optPanelType_2.Checked = True Then
                ProvType = "Type3"
            End If
            .PanelLength = mass_txtPanelLength.Text
            .PanelWidth = mass_txtPanelWidth.Text
            .PanelMass = CInt(mass_txtPanelMass.Text)
            .PanelNbString = mass_txtPanelNStrings.Text
            .PanelSurf = .PanelLength * .PanelWidth
            For i = 1 To SolarPanels.Count()
                If i = (mass_lstPanels.SelectedIndex + 1) Then GoTo LastLine Else GoTo CompareLine
CompareLine:
                TestPanel = SolarPanels(i)
                ' Check if the panel doesn't already exist 
                If TestPanel.PanelType = ProvType And TestPanel.PanelFace = mass_cmbFace.Text And TestPanel.PanelHpos = mass_cmbPosition.Text Then
                    MsgBox("This panel already exists") : Call mass_lstPanels_SelectedIndexChanged(eventSender, eventArgs) : Exit Sub
                End If
                ' Check if the panel doesn't interfere with an existing panel (typically a Type1 and a Type3 "Middle")
                If TestPanel.PanelType = "Type1" And ProvType = "Type3" And mass_cmbPosition.Text = "Middle" And TestPanel.PanelFace = mass_cmbFace.Text Then
                    MsgBox("You can't superimpose 2 panels") : Call mass_lstPanels_SelectedIndexChanged(eventSender, eventArgs) : Exit Sub
                End If
                If TestPanel.PanelType = "Type3" And ProvType = "Type1" And TestPanel.PanelHpos = "Middle" And TestPanel.PanelFace = mass_cmbFace.Text Then
                    MsgBox("You can't superimpose 2 panels") : Call mass_lstPanels_SelectedIndexChanged(eventSender, eventArgs) : Exit Sub
                End If
                GoTo LastLine
LastLine:
            Next
            .Paneltype = ProvType
            .PanelFace = mass_cmbFace.Text
            .PanelDir = mass_cmbDirection.Text
            .PanelHpos = mass_cmbPosition.Text
            If .PanelType = "Type3" Then
                .PanelName = mass_lstPanels.SelectedIndex + 1 & "_" & .PanelType & " [" & .PanelFace & " : " & .PanelDir & "]"
                mass_lstPanels.Items(mass_lstPanels.SelectedIndex) = .PanelName
            Else
                .PanelName = mass_lstPanels.SelectedIndex + 1 & "_" & .PanelType & " [" & .PanelFace & "]"
                mass_lstPanels.Items(mass_lstPanels.SelectedIndex) = .PanelName
            End If
        End With
        '
        update_attitude_data()
        blnIsNewPrj = True
        '
        ' Redraw the satellite
        CameraTransition(SolarPanels.Item(mass_lstPanels.SelectedIndex + 1).PanelFace)
        ''
    End Sub

    ''' <summary>
    ''' Fill boxes with data of the selected panel
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub mass_lstPanels_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_lstPanels.SelectedIndexChanged
        '
        Dim TempPanel As CSolarPanel
        '
        If (mass_lstPanels.SelectedIndex >= 0) Then
            TempPanel = SolarPanels.Item(mass_lstPanels.SelectedIndex + 1)
        Else
            Exit Sub
        End If
        '
        mass_optPanelType_0.Checked = True
        ' Update and fill combo boxes and data with TempPanel characteristics
        '
        Select Case TempPanel.PanelType
            '
            Case "Type1" ' Orientable panel
                mass_optPanelType_0.Checked = True
                mass_optPanelType_1.Checked = False
                mass_optPanelType_2.Checked = False
                With mass_cmbFace
                    .Items.Clear()
                    .Items.Add("+ X")
                    .Items.Add("+ Y")
                    .Items.Add("- X")
                    .Items.Add("- Y")
                    .Text = TempPanel.PanelFace
                End With
                '
                With mass_cmbDirection
                    .Items.Clear()
                    .Enabled = False
                End With
                '
                With mass_cmbPosition
                    .Items.Clear()
                    .Enabled = False
                End With
                '
            Case "Type2" ' Panel on a face
                mass_optPanelType_0.Checked = False
                mass_optPanelType_1.Checked = True
                mass_optPanelType_2.Checked = False
                With mass_cmbFace
                    .Enabled = True
                    .Items.Clear()
                    .Items.Add("+ X")
                    .Items.Add("- X")
                    .Items.Add("+ Y")
                    .Items.Add("- Y")
                    .Items.Add("+ Z")
                    .Items.Add("- Z")
                    .Text = TempPanel.PanelFace
                End With
                '
                With mass_cmbDirection
                    .Items.Clear()
                    .Enabled = False
                End With
                '
                With mass_cmbPosition
                    .Items.Clear()
                    .Enabled = False
                End With
                '
            Case "Type3" ' Fixed perpendicular to a face
                mass_optPanelType_0.Checked = False
                mass_optPanelType_1.Checked = False
                mass_optPanelType_2.Checked = True
                With mass_cmbFace
                    .Enabled = True
                    .Items.Clear()
                    .Items.Add("+ X")
                    .Items.Add("- X")
                    .Items.Add("+ Y")
                    .Items.Add("- Y")
                    .Text = TempPanel.PanelFace
                End With
                '
                With mass_cmbDirection
                    .Items.Clear()
                    .Enabled = True
                    .Items.Add("+ Z")
                    .Items.Add("- Z")
                    .Text = TempPanel.PanelDir
                End With
                '
                With mass_cmbPosition
                    .Items.Clear()
                    .Enabled = True
                    .Items.Add("Upper")
                    .Items.Add("Middle")
                    .Items.Add("Lower")
                    .Text = TempPanel.PanelHpos
                End With
                '
                '
        End Select
        mass_txtPanelWidth.Text = CStr(TempPanel.PanelWidth)
        mass_txtPanelLength.Text = CStr(TempPanel.PanelLength)
        mass_txtPanelMass.Text = CStr(TempPanel.PanelMass)
        mass_txtPanelNStrings.Text = CStr(TempPanel.PanelNbString)
        '
        ' Redraw the satellite
        CameraTransition((TempPanel.PanelFace))
        ''
    End Sub

    Private Sub frmMountPanels_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        '
        ' Initializations
        '
        Dim i As Short
        Dim bmp As Bitmap
        '
        ' Initialize image
        bmp = New Bitmap(My.Application.Info.DirectoryPath & "\Data\Textures\Pan1.bmp")
        bmp.MakeTransparent(Color.Red)
        zpict_5.Image = bmp

        ' Fill body geometry
        mass_txtBodyHeight.Text = Format(Satellite.satBodyHeight, "0.0#")
        mass_txtBodyWidth.Text = Format(Satellite.satBodyWidth, "0.0#")
        '
        ' Set default for panel type
        mass_optPanelType_0.Checked = True
        With mass_cmbFace
            .Enabled = True
            .Items.Clear()
            .Items.Add("+ X")
            .Items.Add("+ Y")
            .Items.Add("- X")
            .Items.Add("- Y")
            .SelectedIndex = 0
        End With
        '
        With mass_cmbDirection ' Initially looking +Z
            .Items.Clear()
            .Enabled = False
        End With
        mass_lblPanelOrient.Enabled = False
        '
        With mass_cmbPosition
            .Items.Clear()
            .Enabled = False
        End With
        mass_lblPanelPos.Enabled = False
        ' optPanelType_CheckedChanged(optPanelType.Item(0), New System.EventArgs())
        '
        ' Fill List with the satellite panels
        mass_lstPanels.Items.Clear()
        Dim panelTmp As CSolarPanel
        If SolarPanels.Count() > 0 Then
            '
            For i = 1 To SolarPanels.Count()
                ' Add the panel to the array and the ListBox
                panelTmp = SolarPanels.Item(i)
                mass_lstPanels.Items.Add(panelTmp.PanelName)
            Next
            '
            mass_lstPanels.SelectedIndex = 0
            '
        End If
        '
        ' Initialize the OpenGL window
        glInitialize()
        '
        ' Move the camera to face the panel
        CameraTransition(("+ X"))
        gl_DrawSatellite()
        ''
    End Sub

#Region "Reading values"

    Private Sub mass_txtBodyHeigth_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_txtBodyHeight.Leave
        '
        Dim X As Double
        ' the entries must be a number
        If IsNumeric(mass_txtBodyHeight.Text) Then
            X = CDbl(mass_txtBodyHeight.Text)
            X = value_limiter(X, BodyHeigth_min, BodyHeigth_max)
            mass_txtBodyHeight.Text = Format(X, "#0.000")
            Satellite.satBodyHeight = CSng(X)
            If Math.Abs(BodyCG.Z) > X / 2 Then
                mass_txtGz_Leave(mass_txtGz, New System.EventArgs)
                MsgBox("CG position changed because it was not longer inside the body" & vbCrLf & "Please check new values.")
            End If
        Else
            MsgBox("Please enter a number !")
            mass_txtBodyHeight.Text = Format(Satellite.satBodyHeight, "#0.000")
            mass_txtBodyHeight.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_txtBodyWidth_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_txtBodyWidth.Leave
        '
        Dim X As Double
        ' the entries must be a number
        If IsNumeric(mass_txtBodyWidth.Text) Then
            X = CDbl(mass_txtBodyWidth.Text)
            X = value_limiter(X, BodyWidth_min, BodyWidth_max)
            mass_txtBodyWidth.Text = Format(X, "#0.000")
            Satellite.satBodyWidth = CSng(X)
            If Math.Abs(BodyCG.X) > X / 2 Or Math.Abs(BodyCG.Y) > X / 2 Then
                mass_txtGx_Leave(mass_txtGx, New System.EventArgs)
                mass_txtGy_Leave(mass_txtGy, New System.EventArgs)
                MsgBox("CG position changed because it was not longer inside the body" & vbCrLf & "Please check new values.")
            End If
        Else
            MsgBox("Please enter a number !")
            mass_txtBodyWidth.Text = Format(Satellite.satBodyWidth, "#0.000")
            mass_txtBodyWidth.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_txtPanelLength_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_txtPanelLength.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtPanelLength.Text) Then
            X = CDbl(mass_txtPanelLength.Text)
            X = value_limiter(X, PanelLength_min, PanelLength_max)
            mass_txtPanelLength.Text = Format(X, "#0.000")
        Else
            MsgBox("Please enter a number !")
            mass_txtPanelLength.Text = CStr(1)
            mass_txtPanelLength.Focus()
        End If
        ''
    End Sub

    Private Sub mass_txtPanelWidth_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mass_txtPanelWidth.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtPanelWidth.Text) Then
            X = CDbl(mass_txtPanelWidth.Text)
            X = value_limiter(X, PanelWidth_min, PanelWidth_max)
            mass_txtPanelWidth.Text = Format(X, "#0.000")
        Else
            MsgBox("Please enter a number !")
            mass_txtPanelWidth.Text = CStr(1)
            mass_txtPanelWidth.Focus()
        End If
        ''
    End Sub

    Private Sub mass_optPanelType_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles mass_optPanelType_0.CheckedChanged, mass_optPanelType_1.CheckedChanged, mass_optPanelType_2.CheckedChanged
        '
        ' Update and fill combo boxes and data with TempPanel characteristics
        '
        If mass_optPanelType_0.Checked = True Then      ' Orientable panel
            mass_optPanelType_0.Checked = True
            mass_optPanelType_1.Checked = False
            mass_optPanelType_2.Checked = False
            With mass_cmbFace
                .Items.Clear()
                .Items.Add("+ X")
                .Items.Add("+ Y")
                .Items.Add("- X")
                .Items.Add("- Y")
                .Text = "+ X"
            End With
            '
            With mass_cmbDirection
                .Items.Clear()
                .Enabled = False
            End With
            '
            With mass_cmbPosition
                .Items.Clear()
                .Enabled = False
            End With
            mass_lblPanelOrient.Enabled = False
            mass_lblPanelPos.Enabled = False
            '
        ElseIf mass_optPanelType_1.Checked = True Then  ' Panel on a face
            mass_optPanelType_0.Checked = False
            mass_optPanelType_1.Checked = True
            mass_optPanelType_2.Checked = False
            With mass_cmbFace
                .Enabled = True
                .Items.Clear()
                .Items.Add("+ X")
                .Items.Add("- X")
                .Items.Add("+ Y")
                .Items.Add("- Y")
                .Items.Add("+ Z")
                .Items.Add("- Z")
                .Text = "+ X"
            End With
            '
            With mass_cmbDirection
                .Items.Clear()
                .Enabled = False
            End With
            '
            With mass_cmbPosition
                .Items.Clear()
                .Enabled = False
            End With
            mass_lblPanelOrient.Enabled = False
            mass_lblPanelPos.Enabled = False
            '
        Else                                            ' Fixed perpendicular to a face
            mass_optPanelType_0.Checked = False
            mass_optPanelType_1.Checked = False
            mass_optPanelType_2.Checked = True
            With mass_cmbFace
                .Enabled = True
                .Items.Clear()
                .Items.Add("+ X")
                .Items.Add("- X")
                .Items.Add("+ Y")
                .Items.Add("- Y")
                .Text = "+ X"
            End With
            '
            With mass_cmbDirection
                .Items.Clear()
                .Enabled = True
                .Items.Add("+ Z")
                .Items.Add("- Z")
                .Text = "+ Z"
            End With
            mass_lblPanelOrient.Enabled = True
            mass_lblPanelPos.Enabled = True
            '
            With mass_cmbPosition
                .Items.Clear()
                .Enabled = True
                .Items.Add("Upper")
                .Items.Add("Middle")
                .Items.Add("Lower")
                .Text = "Upper"
            End With
            '
            '
        End If
        ''
    End Sub

    Private Sub mass_txtGx_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtGx.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtGx.Text) Then
            X = CDbl(mass_txtGx.Text)
            X = value_limiter(X, CDbl(-Satellite.satBodyWidth / 2), CDbl(Satellite.satBodyWidth / 2))   ' The CG must be "inside" the central body
            mass_txtGx.Text = Format(X, "#0.000")
            BodyCG.X = X
        Else
            MsgBox("Please enter a number !")
            mass_txtGx.Text = Format(BodyCG.X, "#0.000")
            mass_txtGx.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_txtGy_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtGy.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtGy.Text) Then
            X = CDbl(mass_txtGy.Text)
            X = value_limiter(X, CDbl(-Satellite.satBodyWidth / 2), CDbl(Satellite.satBodyWidth / 2))   ' The CG must be "inside" the central body
            mass_txtGy.Text = Format(X, "#0.000")
            BodyCG.Y = X
        Else
            MsgBox("Please enter a number !")
            mass_txtGy.Text = Format(BodyCG.Y, "#0.000")
            mass_txtGy.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_txtGz_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtGz.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtGz.Text) Then
            X = CDbl(mass_txtGz.Text)
            X = value_limiter(X, CDbl(-Satellite.satBodyHeight / 2), CDbl(Satellite.satBodyHeight / 2)) ' The CG must be "inside" the central body
            mass_txtGz.Text = Format(X, "#0.000")
            BodyCG.Z = X
        Else
            MsgBox("Please enter a number !")
            mass_txtGz.Text = Format(BodyCG.Z, "#0.000")
            mass_txtGz.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_txtIxx_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtIxx.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtIxx.Text) Then
            X = CDbl(mass_txtIxx.Text)
            X = value_limiter(X, BodyInertia_min, BodyInertia_max)
            mass_txtIxx.Text = Format(X, "#0.000")
            BodyInertia(0, 0) = X
        Else
            MsgBox("Please enter a number !")
            mass_txtIxx.Text = Format(BodyInertia(0, 0), "#0.000")
            mass_txtIxx.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_txtIxy_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtIxy.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtIxy.Text) Then
            X = CDbl(mass_txtIxy.Text)
            X = value_limiter(X, BodyInertia_min, BodyInertia_max)
            mass_txtIxy.Text = Format(X, "#0.000")
            BodyInertia(0, 1) = X
            BodyInertia(1, 0) = X
        Else
            MsgBox("Please enter a number !")
            mass_txtIxy.Text = Format(BodyInertia(0, 1), "#0.000")
            mass_txtIxy.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_txtBodyMass_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtBodyMass.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtBodyMass.Text) Then
            X = CDbl(mass_txtBodyMass.Text)
            X = value_limiter(X, BodyMass_min, BodyMass_max)
            mass_txtBodyMass.Text = Format(X, "#0.000")
            BodyMass = X
        Else
            MsgBox("Please enter a number !")
            mass_txtBodyMass.Text = Format(BodyMass, "#0.000")
            mass_txtBodyMass.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_txtIxz_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtIxz.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtIxz.Text) Then
            X = CDbl(mass_txtIxz.Text)
            X = value_limiter(X, 0, 100)                       ' 0 / 100 values based on Simusat VB6
            mass_txtIxz.Text = Format(X, "#0.000")
            BodyInertia(0, 2) = X
            BodyInertia(2, 0) = X
        Else
            MsgBox("Please enter a number !")
            mass_txtIxz.Text = Format(BodyInertia(0, 2), "#0.000")
            mass_txtIxz.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_txtIyy_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtIyy.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtIyy.Text) Then
            X = CDbl(mass_txtIyy.Text)
            X = value_limiter(X, BodyInertia_min, BodyInertia_max)
            mass_txtIyy.Text = Format(X, "#0.000")
            BodyInertia(1, 1) = X
        Else
            MsgBox("Please enter a number !")
            mass_txtIyy.Text = Format(BodyInertia(1, 1), "#0.000")
            mass_txtIyy.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_txtIyz_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtIyz.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtIyz.Text) Then
            X = CDbl(mass_txtIyz.Text)
            X = value_limiter(X, BodyInertia_min, BodyInertia_max)
            mass_txtIyz.Text = Format(X, "#0.000")
            BodyInertia(1, 2) = X
            BodyInertia(2, 1) = X
        Else
            MsgBox("Please enter a number !")
            mass_txtIyz.Text = Format(BodyInertia(1, 2), "#0.000")
            mass_txtIyz.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_txtIzz_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtIzz.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtIzz.Text) Then
            X = CDbl(mass_txtIzz.Text)
            X = value_limiter(X, BodyInertia_min, BodyInertia_max)
            mass_txtIzz.Text = Format(X, "#0.000")
            BodyInertia(2, 2) = X
        Else
            MsgBox("Please enter a number !")
            mass_txtIzz.Text = Format(BodyInertia(2, 2), "#0.000")
            mass_txtIzz.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_txtPanelMass_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtPanelMass.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtPanelMass.Text) Then
            X = CDbl(mass_txtPanelMass.Text)
            X = value_limiter(X, PanelMass_min, PanelMass_max)
            mass_txtPanelMass.Text = Format(X, "#0.000")
        Else
            MsgBox("Please enter a number !")
            mass_txtPanelMass.Text = CStr(1)
            mass_txtPanelMass.Focus()
        End If
        ''
    End Sub

    Private Sub nstrings_txtPanel_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtPanelNStrings.Leave
        '
        Dim X As Double
        '
        If IsNumeric(mass_txtPanelNStrings.Text) Then
            X = CDbl(mass_txtPanelNStrings.Text)
            X = value_limiter(X, PanelStrings_min, PanelStrings_max)
            mass_txtPanelNStrings.Text = Format(X, "#0")
        Else
            MsgBox("Please enter a number !")
            mass_txtPanelNStrings.Text = CStr(1)
            mass_txtPanelNStrings.Focus()
        End If
        ''
    End Sub

    Private Sub mass_txtBoomDist_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtBoomDist.Leave
        '
        Dim X As Double
        ' the entries must be a number
        If IsNumeric(mass_txtBoomDist.Text) Then
            X = CDbl(mass_txtBoomDist.Text)
            X = value_limiter(X, BoomDist_min, BoomDist_max)
            mass_txtBoomDist.Text = Format(X, "#0.000")
            Select Case mass_cmbBoomFace.Text
                Case "+ X"
                    GravityDist.X = X
                    GravityDist.Y = 0
                    GravityDist.Z = 0
                Case "- X"
                    GravityDist.X = -X
                    GravityDist.Y = 0
                    GravityDist.Z = 0
                Case "+ Y"
                    GravityDist.X = 0
                    GravityDist.Y = X
                    GravityDist.Z = 0
                Case "- Y"
                    GravityDist.X = 0
                    GravityDist.Y = -X
                    GravityDist.Z = 0
                Case "+ Z"
                    GravityDist.X = 0
                    GravityDist.Y = 0
                    GravityDist.Z = X
                Case "- Z"
                    GravityDist.X = 0
                    GravityDist.Y = 0
                    GravityDist.Z = -X
            End Select
        Else
            MsgBox("Please enter a number !")
            mass_txtBoomDist.Text = Format(vctNorm(GravityDist), "#0.000")
            mass_txtBoomDist.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_txtBoomMass_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles mass_txtBoomMass.Leave
        Dim X As Double
        '
        If IsNumeric(mass_txtBoomMass.Text) Then
            X = CDbl(mass_txtBoomMass.Text)
            X = value_limiter(X, BoomMass_min, BoomMass_max)
            mass_txtBoomMass.Text = Format(X, "#0.000")
            GravityMass = X
        Else
            MsgBox("Please enter a number !")
            mass_txtBoomMass.Text = Format(GravityMass, "#0.000")
            mass_txtBoomMass.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub mass_cmbBoomFace_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mass_cmbBoomFace.SelectedIndexChanged
        '
        Dim X As Double
        X = CDbl(mass_txtBoomDist.Text)
        '
        Select Case mass_cmbBoomFace.Text
            Case "+ X"
                GravityDist.X = X
                GravityDist.Y = 0
                GravityDist.Z = 0
            Case "- X"
                GravityDist.X = -X
                GravityDist.Y = 0
                GravityDist.Z = 0
            Case "+ Y"
                GravityDist.X = 0
                GravityDist.Y = X
                GravityDist.Z = 0
            Case "- Y"
                GravityDist.X = 0
                GravityDist.Y = -X
                GravityDist.Z = 0
            Case "+ Z"
                GravityDist.X = 0
                GravityDist.Y = 0
                GravityDist.Z = X
            Case "- Z"
                GravityDist.X = 0
                GravityDist.Y = 0
                GravityDist.Z = -X
        End Select
        update_attitude_data()
        'blnIsNewPrj = True
        ''
    End Sub

#End Region

#End Region

#Region "Satellite properties TAB"

#Region "Reading values"

    Private Sub prop_txtCN_0_Leave(sender As Object, e As EventArgs) Handles prop_txtCN_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCN_0.Text) Then
            X = CDbl(prop_txtCN_0.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCN_0.Text = Format(X, "#0.000")
            Cn_aero(0) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCN_0.Text = Format(Cn_aero(0), "#0.000")
            prop_txtCN_0.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCN_3_Leave(sender As Object, e As EventArgs) Handles prop_txtCN_3.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCN_3.Text) Then
            X = CDbl(prop_txtCN_3.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCN_3.Text = Format(X, "#0.000")
            Cn_aero(3) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCN_3.Text = Format(Cn_aero(3), "#0.000")
            prop_txtCN_3.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCN_1_Leave(sender As Object, e As EventArgs) Handles prop_txtCN_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCN_1.Text) Then
            X = CDbl(prop_txtCN_1.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCN_1.Text = Format(X, "#0.000")
            Cn_aero(1) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCN_1.Text = Format(Cn_aero(1), "#0.000")
            prop_txtCN_1.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCN_4_Leave(sender As Object, e As EventArgs) Handles prop_txtCN_4.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCN_4.Text) Then
            X = CDbl(prop_txtCN_4.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCN_4.Text = Format(X, "#0.000")
            Cn_aero(4) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCN_4.Text = Format(Cn_aero(4), "#0.000")
            prop_txtCN_4.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCN_2_Leave(sender As Object, e As EventArgs) Handles prop_txtCN_2.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCN_2.Text) Then
            X = CDbl(prop_txtCN_2.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCN_2.Text = Format(X, "#0.000")
            Cn_aero(2) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCN_2.Text = Format(Cn_aero(2), "#0.000")
            prop_txtCN_2.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCN_5_Leave(sender As Object, e As EventArgs) Handles prop_txtCN_5.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCN_5.Text) Then
            X = CDbl(prop_txtCN_5.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCN_5.Text = Format(X, "#0.000")
            Cn_aero(5) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCN_5.Text = Format(Cn_aero(5), "#0.000")
            prop_txtCN_5.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCT_0_Leave(sender As Object, e As EventArgs) Handles prop_txtCT_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCT_0.Text) Then
            X = CDbl(prop_txtCT_0.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCT_0.Text = Format(X, "#0.000")
            Ct_aero(0) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCT_0.Text = Format(Ct_aero(0), "#0.000")
            prop_txtCT_0.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCT_3_Leave(sender As Object, e As EventArgs) Handles prop_txtCT_3.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCT_3.Text) Then
            X = CDbl(prop_txtCT_3.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCT_3.Text = Format(X, "#0.000")
            Ct_aero(3) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCT_3.Text = Format(Ct_aero(3), "#0.000")
            prop_txtCT_3.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCT_1_Leave(sender As Object, e As EventArgs) Handles prop_txtCT_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCT_1.Text) Then
            X = CDbl(prop_txtCT_1.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCT_1.Text = Format(X, "#0.000")
            Ct_aero(1) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCT_1.Text = Format(Ct_aero(1), "#0.000")
            prop_txtCT_1.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCT_4_Leave(sender As Object, e As EventArgs) Handles prop_txtCT_4.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCT_4.Text) Then
            X = CDbl(prop_txtCT_4.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCT_4.Text = Format(X, "#0.000")
            Ct_aero(4) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCT_4.Text = Format(Ct_aero(4), "#0.000")
            prop_txtCT_4.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCT_2_Leave(sender As Object, e As EventArgs) Handles prop_txtCT_2.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCT_2.Text) Then
            X = CDbl(prop_txtCT_2.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCT_2.Text = Format(X, "#0.000")
            Ct_aero(2) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCT_2.Text = Format(Ct_aero(2), "#0.000")
            prop_txtCT_2.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCT_5_Leave(sender As Object, e As EventArgs) Handles prop_txtCT_5.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCT_5.Text) Then
            X = CDbl(prop_txtCT_5.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCT_5.Text = Format(X, "#0.000")
            Ct_aero(5) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCT_5.Text = Format(Ct_aero(5), "#0.000")
            prop_txtCT_5.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCR_0_Leave(sender As Object, e As EventArgs) Handles prop_txtCR_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCR_0.Text) Then
            X = CDbl(prop_txtCR_0.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCR_0.Text = Format(X, "#0.000")
            Cr_solar(0) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCR_0.Text = Format(Cr_solar(0), "#0.000")
            prop_txtCR_0.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCR_3_Leave(sender As Object, e As EventArgs) Handles prop_txtCR_3.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCR_3.Text) Then
            X = CDbl(prop_txtCR_3.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCR_3.Text = Format(X, "#0.000")
            Cr_solar(3) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCR_3.Text = Format(Cr_solar(3), "#0.000")
            prop_txtCR_3.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCR_1_Leave(sender As Object, e As EventArgs) Handles prop_txtCR_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCR_1.Text) Then
            X = CDbl(prop_txtCR_1.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCR_1.Text = Format(X, "#0.000")
            Cr_solar(1) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCR_1.Text = Format(Cr_solar(1), "#0.000")
            prop_txtCR_1.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCR_4_Leave(sender As Object, e As EventArgs) Handles prop_txtCR_4.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCR_4.Text) Then
            X = CDbl(prop_txtCR_4.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCR_4.Text = Format(X, "#0.000")
            Cr_solar(4) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCR_4.Text = Format(Cr_solar(4), "#0.000")
            prop_txtCR_4.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCR_2_Leave(sender As Object, e As EventArgs) Handles prop_txtCR_2.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCR_2.Text) Then
            X = CDbl(prop_txtCR_2.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCR_2.Text = Format(X, "#0.000")
            Cr_solar(2) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCR_2.Text = Format(Cr_solar(2), "#0.000")
            prop_txtCR_2.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCR_5_Leave(sender As Object, e As EventArgs) Handles prop_txtCR_5.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCR_5.Text) Then
            X = CDbl(prop_txtCR_5.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCR_5.Text = Format(X, "#0.000")
            Cr_solar(5) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCR_5.Text = Format(Cr_solar(5), "#0.000")
            prop_txtCR_5.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCD_0_Leave(sender As Object, e As EventArgs) Handles prop_txtCD_0.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCD_0.Text) Then
            X = CDbl(prop_txtCD_0.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCD_0.Text = Format(X, "#0.000")
            Cd_solar(0) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCD_0.Text = Format(Cd_solar(0), "#0.000")
            prop_txtCD_0.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCD_3_Leave(sender As Object, e As EventArgs) Handles prop_txtCD_3.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCD_3.Text) Then
            X = CDbl(prop_txtCD_3.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCD_3.Text = Format(X, "#0.000")
            Cd_solar(3) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCD_3.Text = Format(Cd_solar(3), "#0.000")
            prop_txtCD_3.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCD_1_Leave(sender As Object, e As EventArgs) Handles prop_txtCD_1.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCD_1.Text) Then
            X = CDbl(prop_txtCD_1.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCD_1.Text = Format(X, "#0.000")
            Cd_solar(1) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCD_1.Text = Format(Cd_solar(1), "#0.000")
            prop_txtCD_1.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCD_4_Leave(sender As Object, e As EventArgs) Handles prop_txtCD_4.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCD_4.Text) Then
            X = CDbl(prop_txtCD_4.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCD_4.Text = Format(X, "#0.000")
            Cd_solar(4) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCD_4.Text = Format(Cd_solar(4), "#0.000")
            prop_txtCD_4.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCD_2_Leave(sender As Object, e As EventArgs) Handles prop_txtCD_2.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCD_2.Text) Then
            X = CDbl(prop_txtCD_2.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCD_2.Text = Format(X, "#0.000")
            Cd_solar(2) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCD_2.Text = Format(Cd_solar(2), "#0.000")
            prop_txtCD_2.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtCD_5_Leave(sender As Object, e As EventArgs) Handles prop_txtCD_5.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtCD_5.Text) Then
            X = CDbl(prop_txtCD_5.Text)
            X = value_limiter(X, Coeff_min, Coeff_max)
            prop_txtCD_5.Text = Format(X, "#0.000")
            Cd_solar(5) = X
        Else
            MsgBox("Please enter a number !")
            prop_txtCD_5.Text = Format(Cd_solar(5), "#0.000")
            prop_txtCD_5.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtMG_X_Leave(sender As Object, e As EventArgs) Handles prop_txtMG_X.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtMG_X.Text) Then
            X = CDbl(prop_txtMG_X.Text)
            X = value_limiter(X, dipsat_passive_min, dipsat_passive_max)
            prop_txtMG_X.Text = Format(X, "#0.000")
            dipsat_passive.X = X
        Else
            MsgBox("Please enter a number !")
            prop_txtMG_X.Text = Format(dipsat_passive.X, "#0.000")
            prop_txtMG_X.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtMG_Y_Leave(sender As Object, e As EventArgs) Handles prop_txtMG_Y.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtMG_Y.Text) Then
            X = CDbl(prop_txtMG_Y.Text)
            X = value_limiter(X, dipsat_passive_min, dipsat_passive_max)
            prop_txtMG_Y.Text = Format(X, "#0.000")
            dipsat_passive.Y = X
        Else
            MsgBox("Please enter a number !")
            prop_txtMG_Y.Text = Format(dipsat_passive.Y, "#0.000")
            prop_txtMG_Y.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub prop_txtMG_Z_Leave(sender As Object, e As EventArgs) Handles prop_txtMG_Z.Leave
        '
        Dim X As Double
        '
        If IsNumeric(prop_txtMG_Z.Text) Then
            X = CDbl(prop_txtMG_Z.Text)
            X = value_limiter(X, dipsat_passive_min, dipsat_passive_max)
            prop_txtMG_Z.Text = Format(X, "#0.000")
            dipsat_passive.Z = X
        Else
            MsgBox("Please enter a number !")
            prop_txtMG_Z.Text = Format(dipsat_passive.Z, "#0.000")
            prop_txtMG_Z.Focus()
        End If
        '
        blnIsNewPrj = True
        ''
    End Sub

#End Region

#End Region

#Region "Actuactors TAB"

    ''' <summary>
    ''' Modify the selected event in eventlist
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub act_cmdModify_Click(sender As Object, e As EventArgs) Handles act_cmdModify.Click
        '
        Dim TempEvent As New CEvent
        Dim i As Integer
        Dim Index As Integer
        '
        If simloop Then
            MsgBox("You cannot modify an event while the simulation is running") : Exit Sub
        End If
        '
        If EventsList.Count() = 0 Then Exit Sub
        '
        Index = gest_lstEvents.SelectedIndex
        TempEvent = EventsList.Item(Index + 1)
        With TempEvent
            .EventTime = CDbl(gest_txtTime.Text)
            .EventDescription = gest_txtDescription.Text
            .EventName = "Event_" + CStr(.EventTime) + "s"
            '
            For i = 0 To 2
                .EventJR(i) = JR(i)
                .Eventom(i) = om(i)
                .Eventjet(i) = jet(i)
            Next
            '
            .Eventdipsat_active.X = dipsat_active.X
            .Eventdipsat_active.Y = dipsat_active.Y
            .Eventdipsat_active.Z = dipsat_active.Z
        End With
        '
        EventsList.Remove(Index + 1)
        gest_lstEvents.Items.RemoveAt(Index)
        EventsNumber = EventsList.Count()
        '
        Index = 1
        While Index <= EventsNumber AndAlso TempEvent.isBiggerThan(EventsList.Item(Index))
            Index = Index + 1
        End While
        '
        gest_lstEvents.Items.Insert(Index - 1, TempEvent.EventName)
        EventsList.Add(TempEvent, , Index)
        gest_lstEvents.SelectedIndex = Index - 1
        EventsNumber = EventsList.Count()
        ''
    End Sub

#Region "Reading values"

    Private Sub act_txtMX_Leave(sender As Object, e As EventArgs) Handles act_txtMX.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtMX.Text) Then
            X = CDbl(act_txtMX.Text)
            X = value_limiter(X, DampersMass_min, DampersMass_max)
            act_txtMX.Text = Format(X, "#0.000")
            DampersMass(0) = X
        Else
            MsgBox("Please enter a number !")
            act_txtMX.Text = Format(DampersMass(0), "#0.000")
            act_txtMX.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtMY_Leave(sender As Object, e As EventArgs) Handles act_txtMY.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtMY.Text) Then
            X = CDbl(act_txtMY.Text)
            X = value_limiter(X, DampersMass_min, DampersMass_max)
            act_txtMY.Text = Format(X, "#0.000")
            DampersMass(1) = X
        Else
            MsgBox("Please enter a number !")
            act_txtMY.Text = Format(DampersMass(1), "#0.000")
            act_txtMY.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtMZ_Leave(sender As Object, e As EventArgs) Handles act_txtMZ.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtMZ.Text) Then
            X = CDbl(act_txtMZ.Text)
            X = value_limiter(X, DampersMass_min, DampersMass_max)
            act_txtMZ.Text = Format(X, "#0.000")
            DampersMass(2) = X
        Else
            MsgBox("Please enter a number !")
            act_txtMZ.Text = Format(DampersMass(2), "#0.000")
            act_txtMZ.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtDX_Leave(sender As Object, e As EventArgs) Handles act_txtDX.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtDX.Text) Then
            X = CDbl(act_txtDX.Text)
            X = value_limiter(X, DampersDist_min, DampersDist_max)
            act_txtDX.Text = Format(X, "#0.000")
            DampersDist(0) = X
        Else
            MsgBox("Please enter a number !")
            act_txtDX.Text = Format(DampersDist(0), "#0.000")
            act_txtDX.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtDY_Leave(sender As Object, e As EventArgs) Handles act_txtDY.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtDY.Text) Then
            X = CDbl(act_txtDY.Text)
            X = value_limiter(X, DampersDist_min, DampersDist_max)
            act_txtDY.Text = Format(X, "#0.000")
            DampersDist(1) = X
        Else
            MsgBox("Please enter a number !")
            act_txtDY.Text = Format(DampersDist(1), "#0.000")
            act_txtDY.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtDZ_Leave(sender As Object, e As EventArgs) Handles act_txtDZ.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtDZ.Text) Then
            X = CDbl(act_txtDZ.Text)
            X = value_limiter(X, DampersDist_min, DampersDist_max)
            act_txtDZ.Text = Format(X, "#0.000")
            DampersDist(2) = X
        Else
            MsgBox("Please enter a number !")
            act_txtDZ.Text = Format(DampersDist(2), "#0.000")
            act_txtDZ.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtCX_Leave(sender As Object, e As EventArgs) Handles act_txtCX.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtCX.Text) Then
            X = CDbl(act_txtCX.Text)
            X = value_limiter(X, c_min, c_max)
            act_txtCX.Text = Format(X, "#0.000")
            c(0) = X
        Else
            MsgBox("Please enter a number !")
            act_txtCX.Text = Format(c(0), "#0.000")
            act_txtCX.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtCY_Leave(sender As Object, e As EventArgs) Handles act_txtCY.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtCY.Text) Then
            X = CDbl(act_txtCY.Text)
            X = value_limiter(X, c_min, c_max)
            act_txtCY.Text = Format(X, "#0.000")
            c(1) = X
        Else
            MsgBox("Please enter a number !")
            act_txtCY.Text = Format(c(1), "#0.000")
            act_txtCY.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtCZ_Leave(sender As Object, e As EventArgs) Handles act_txtCZ.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtCZ.Text) Then
            X = CDbl(act_txtCZ.Text)
            X = value_limiter(X, c_min, c_max)
            act_txtCZ.Text = Format(X, "#0.000")
            c(2) = X
        Else
            MsgBox("Please enter a number !")
            act_txtCZ.Text = Format(c(2), "#0.000")
            act_txtCZ.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtKX_Leave(sender As Object, e As EventArgs) Handles act_txtKX.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtKX.Text) Then
            X = CDbl(act_txtKX.Text)
            X = value_limiter(X, kd_min, kd_max)
            act_txtKX.Text = Format(X, "#0.000")
            kd(0) = X
        Else
            MsgBox("Please enter a number !")
            act_txtKX.Text = Format(kd(0), "#0.000")
            act_txtKX.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtKY_Leave(sender As Object, e As EventArgs) Handles act_txtKY.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtKY.Text) Then
            X = CDbl(act_txtKY.Text)
            X = value_limiter(X, kd_min, kd_max)
            act_txtKY.Text = Format(X, "#0.000")
            kd(1) = X
        Else
            MsgBox("Please enter a number !")
            act_txtKY.Text = Format(kd(1), "#0.000")
            act_txtKY.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtKZ_Leave(sender As Object, e As EventArgs) Handles act_txtKZ.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtKZ.Text) Then
            X = CDbl(act_txtKZ.Text)
            X = value_limiter(X, kd_min, kd_max)
            act_txtKZ.Text = Format(X, "#0.000")
            kd(2) = X
        Else
            MsgBox("Please enter a number !")
            act_txtKZ.Text = Format(kd(2), "#0.000")
            act_txtKZ.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtMG_X_Leave(sender As Object, e As EventArgs) Handles act_txtMG_X.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtMG_X.Text) Then
            X = CDbl(act_txtMG_X.Text)
            X = value_limiter(X, dipsat_active_min, dipsat_active_max)
            act_txtMG_X.Text = Format(X, "#0.000")
            dipsat_active.X = X
        Else
            MsgBox("Please enter a number !")
            act_txtMG_X.Text = Format(dipsat_active.X, "#0.000")
            act_txtMG_X.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub act_txtMG_Y_Leave(sender As Object, e As EventArgs) Handles act_txtMG_Y.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtMG_Y.Text) Then
            X = CDbl(act_txtMG_Y.Text)
            X = value_limiter(X, dipsat_active_min, dipsat_active_max)
            act_txtMG_Y.Text = Format(X, "#0.000")
            dipsat_active.Y = X
        Else
            MsgBox("Please enter a number !")
            act_txtMG_Y.Text = Format(dipsat_active.Y, "#0.000")
            act_txtMG_Y.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub act_txtMG_Z_Leave(sender As Object, e As EventArgs) Handles act_txtMG_Z.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtMG_Z.Text) Then
            X = CDbl(act_txtMG_Z.Text)
            X = value_limiter(X, dipsat_active_min, dipsat_active_max)
            act_txtMG_Z.Text = Format(X, "#0.000")
            dipsat_active.Z = X
        Else
            MsgBox("Please enter a number !")
            act_txtMG_Z.Text = Format(dipsat_active.Z, "#0.000")
            act_txtMG_Z.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub act_txtMN_X_Leave(sender As Object, e As EventArgs) Handles act_txtMN_X.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtMN_X.Text) Then
            X = CDbl(act_txtMN_X.Text)
            X = value_limiter(X, jet_min, jet_max)
            act_txtMN_X.Text = Format(X, "#0.000")
            jet(0) = X
        Else
            MsgBox("Please enter a number !")
            act_txtMN_X.Text = Format(jet(0), "#0.000")
            act_txtMN_X.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub act_txtMN_Y_Leave(sender As Object, e As EventArgs) Handles act_txtMN_Y.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtMN_Y.Text) Then
            X = CDbl(act_txtMN_Y.Text)
            X = value_limiter(X, jet_min, jet_max)
            act_txtMN_Y.Text = Format(X, "#0.000")
            jet(1) = X
        Else
            MsgBox("Please enter a number !")
            act_txtMN_Y.Text = Format(jet(1), "#0.000")
            act_txtMN_Y.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub act_txtMN_Z_Leave(sender As Object, e As EventArgs) Handles act_txtMN_Z.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtMN_Z.Text) Then
            X = CDbl(act_txtMN_Z.Text)
            X = value_limiter(X, jet_min, jet_max)
            act_txtMN_Z.Text = Format(X, "#0.000")
            jet(2) = X
        Else
            MsgBox("Please enter a number !")
            act_txtMN_Z.Text = Format(jet(2), "#0.000")
            act_txtMN_Z.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub act_txtIRX_Leave(sender As Object, e As EventArgs) Handles act_txtIRX.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtIRX.Text) Then
            X = CDbl(act_txtIRX.Text)
            X = value_limiter(X, JR_min, JR_max)
            act_txtIRX.Text = Format(X, "#0.000")
            JR(0) = X
        Else
            MsgBox("Please enter a number !")
            act_txtIRX.Text = Format(JR(0), "#0.000")
            act_txtIRX.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtIRY_Leave(sender As Object, e As EventArgs) Handles act_txtIRY.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtIRY.Text) Then
            X = CDbl(act_txtIRY.Text)
            X = value_limiter(X, JR_min, JR_max)
            act_txtIRY.Text = Format(X, "#0.000")
            JR(1) = X
        Else
            MsgBox("Please enter a number !")
            act_txtIRY.Text = Format(JR(1), "#0.000")
            act_txtIRY.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtIRZ_Leave(sender As Object, e As EventArgs) Handles act_txtIRZ.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtIRZ.Text) Then
            X = CDbl(act_txtIRZ.Text)
            X = value_limiter(X, JR_min, JR_max)
            act_txtIRZ.Text = Format(X, "#0.000")
            JR(2) = X
        Else
            MsgBox("Please enter a number !")
            act_txtIRZ.Text = Format(JR(2), "#0.000")
            act_txtIRZ.Focus()
        End If
        update_attitude_data()
        blnIsNewPrj = True
        ''
    End Sub

    Private Sub act_txtWRX_Leave(sender As Object, e As EventArgs) Handles act_txtWRX.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtWRX.Text) Then
            X = CDbl(act_txtWRX.Text)
            X = value_limiter(X, om_min, om_max)
            act_txtWRX.Text = Format(X, "#0.000")
            om(0) = X
        Else
            MsgBox("Please enter a number !")
            act_txtWRX.Text = Format(om(0), "#0.000")
            act_txtWRX.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub act_txtWRY_Leave(sender As Object, e As EventArgs) Handles act_txtWRY.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtWRY.Text) Then
            X = CDbl(act_txtWRY.Text)
            X = value_limiter(X, om_min, om_max)
            act_txtWRY.Text = Format(X, "#0.000")
            om(1) = X
        Else
            MsgBox("Please enter a number !")
            act_txtWRY.Text = Format(om(1), "#0.000")
            act_txtWRY.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

    Private Sub act_txtWRZ_Leave(sender As Object, e As EventArgs) Handles act_txtWRZ.Leave
        '
        Dim X As Double
        '
        If IsNumeric(act_txtWRZ.Text) Then
            X = CDbl(act_txtWRZ.Text)
            X = value_limiter(X, om_min, om_max)
            act_txtWRZ.Text = Format(X, "#0.000")
            om(2) = X
        Else
            MsgBox("Please enter a number !")
            act_txtWRZ.Text = Format(om(2), "#0.000")
            act_txtWRZ.Focus()
        End If
        update_attitude_data()
        ''
    End Sub

#End Region

#End Region

#Region "Management TAB"

    ''' <summary>
    ''' Create a new event and sort the list of events
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub gest_cmdAdd_Click(sender As Object, e As EventArgs) Handles gest_cmdAdd.Click
        '
        Dim TempEvent As New CEvent
        Dim i As Integer
        '
        If simloop Then
            MsgBox("You cannot add an event while the simulation is running") : Exit Sub
        End If
        '
        If gest_lstEvents.Items.Count = 20 Then
            MsgBox("You cannot create more than 20 events") : Exit Sub
        End If
        '
        For i = 1 To EventsNumber
            If CDbl(gest_txtTime.Text) = EventsList.Item(i).EventTime Then
                MsgBox("Two events cannot happen at the same time !" & vbCrLf & "Please select another time of occurrence.")
                Exit Sub
            End If
        Next
        '
        With TempEvent
            .EventTime = CDbl(gest_txtTime.Text)
            .EventDescription = gest_txtDescription.Text
            .EventName = "Event_" + CStr(.EventTime) + "s"
            '
            For i = 0 To 2
                .EventJR(i) = JR(i)
                .Eventom(i) = om(i)
                .Eventjet(i) = jet(i)
            Next
            '
            .Eventdipsat_active.X = dipsat_active.X
            .Eventdipsat_active.Y = dipsat_active.Y
            .Eventdipsat_active.Z = dipsat_active.Z
        End With
        '
        If EventsNumber = 0 Then
            gest_lstEvents.Items.Add(TempEvent.EventName)
            EventsList.Add(TempEvent)
        Else
            i = 1
            While i <= EventsNumber AndAlso TempEvent.isBiggerThan(EventsList.Item(i))
                i = i + 1
            End While
            '
            gest_lstEvents.Items.Insert(i - 1, TempEvent.EventName)
            EventsList.Add(TempEvent, , i)
        End If
        '
        EventsNumber = EventsList.Count() ' Update EventsNumber
        '
        ' Select the added Event in the ListBox
        gest_lstEvents.SelectedIndex = gest_lstEvents.Items.Count - 1
        ''
    End Sub

    ''' <summary>
    ''' Remove an existing event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub gest_cmdRemove_Click(sender As Object, e As EventArgs) Handles gest_cmdRemove.Click
        '
        Dim Index As Short
        '
        If simloop Then
            MsgBox("You cannot remove an event while the simulation is running") : Exit Sub
        End If
        '
        If (gest_lstEvents.SelectedIndex <> -1) Then
            Index = gest_lstEvents.SelectedIndex
            EventsList.Remove((Index + 1))
            gest_lstEvents.Items.RemoveAt(Index)
            EventsNumber = EventsList.Count() ' Update EventsNumber
        End If
        '
        If EventsNumber > 0 Then
            ' Select the next event in the ListBox
            gest_lstEvents.SelectedIndex = Index
        End If
        ''
    End Sub

    ''' <summary>
    ''' Modify the selected event in eventlist and sort the list of events if necessary
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub gest_cmdModify_Click(sender As Object, e As EventArgs) Handles gest_cmdModify.Click
        '
        Dim TempEvent As New CEvent
        Dim i As Integer
        Dim Index As Integer
        '
        If simloop Then
            MsgBox("You cannot modify an event while the simulation is running") : Exit Sub
        End If
        '
        If EventsList.Count() = 0 Then Exit Sub
        '
        Index = gest_lstEvents.SelectedIndex
        TempEvent = EventsList.Item(Index + 1)
        With TempEvent
            .EventTime = CDbl(gest_txtTime.Text)
            .EventDescription = gest_txtDescription.Text
            .EventName = "Event_" + CStr(.EventTime) + "s"
            '
            For i = 0 To 2
                .EventJR(i) = JR(i)
                .Eventom(i) = om(i)
                .Eventjet(i) = jet(i)
            Next
            '
            .Eventdipsat_active.X = dipsat_active.X
            .Eventdipsat_active.Y = dipsat_active.Y
            .Eventdipsat_active.Z = dipsat_active.Z
        End With
        '
        EventsList.Remove(Index + 1)
        gest_lstEvents.Items.RemoveAt(Index)
        EventsNumber = EventsList.Count()
        '
        Index = 1
        While Index <= EventsNumber AndAlso TempEvent.isBiggerThan(EventsList.Item(Index))
            Index = Index + 1
        End While
        '
        gest_lstEvents.Items.Insert(Index - 1, TempEvent.EventName)
        EventsList.Add(TempEvent, , Index)
        gest_lstEvents.SelectedIndex = Index - 1
        EventsNumber = EventsList.Count()
        ''
    End Sub

    ''' <summary>
    ''' Fill boxes with data of the selected event
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub gest_lstEvents_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles gest_lstEvents.SelectedIndexChanged
        '
        Dim TempEvent As New CEvent
        '
        If simloop Then
            MsgBox("You cannot load data of an event while the simulation is running") : Exit Sub
        End If
        '
        If (gest_lstEvents.SelectedIndex >= 0) Then
            TempEvent = EventsList.Item(gest_lstEvents.SelectedIndex + 1)
        Else
            Exit Sub
        End If
        '
        act_txtName.Text = TempEvent.EventName
        gest_txtTime.Text = CStr(TempEvent.EventTime)
        gest_txtDescription.Text = TempEvent.EventDescription
        '
        TempEvent.loadEvent()
        '
        update_attitude_conf()
        update_attitude_data()
        ''
    End Sub

    Private Sub gest_cmdSave_Click(sender As Object, e As EventArgs) Handles gest_cmdSave.Click
        '
        Dim file As String
        Dim xs As String
        Dim i As Integer
        Dim TempEvent As New CEvent
        '
        On Error GoTo errorsave
        dlgScenarioSave.FileName = gest_txtName.Text
        If dlgScenarioSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
        file = dlgScenarioSave.FileName
        If My.Computer.FileSystem.FileExists(file) Then Kill(file)
        On Error GoTo 0
        '
        gest_txtName.Text = file
        FileOpen(1, file, OpenMode.Output)
        '
        PrintLine(1, "!")
        PrintLine(1, "! --------------")
        PrintLine(1, "! List of events")
        PrintLine(1, "! --------------")
        '
        PrintLine(1, "!")
        PrintLine(1, EventsNumber, TAB(24), "Events number")
        PrintLine(1, "!")
        '
        For i = 1 To EventsNumber
            TempEvent = EventsList.Item(i)
            With TempEvent
                PrintLine(1, .EventName, TAB(24), "Event name")
                PrintLine(1, "--------------")
                PrintLine(1, .EventTime, TAB(24), "Time of occurrence")
                PrintLine(1, TAB(24), "Description")
                PrintLine(1, .EventDescription)
                '
                PrintLine(1, .EventJR(0), TAB(24), "Flywheel")
                PrintLine(1, .EventJR(1), TAB(24), "|")
                PrintLine(1, .EventJR(2), TAB(24), "|")
                PrintLine(1, .Eventom(0), TAB(24), "|")
                PrintLine(1, .Eventom(1), TAB(24), "|")
                PrintLine(1, .Eventom(2), TAB(24), "|")
                '
                PrintLine(1, .Eventdipsat_active.X, TAB(24), "Magnetic control system moment")
                PrintLine(1, .Eventdipsat_active.Y, TAB(24), "|")
                PrintLine(1, .Eventdipsat_active.Z, TAB(24), "|")
                '
                PrintLine(1, .Eventjet(0), TAB(24), "Nozzle moment")
                PrintLine(1, .Eventjet(1), TAB(24), "|")
                PrintLine(1, .Eventjet(2), TAB(24), "|")
                PrintLine(1, "!")
            End With
        Next
        '
        FileClose(1)
        '
        Exit Sub
        '
errorsave:
        xs = MsgBox("Writing error" & Chr(13) _
        & " Saving data is not possible !!", vbCritical)
        ''
    End Sub

    Private Sub gest_cmdOpen_Click(sender As Object, e As EventArgs) Handles gest_cmdOpen.Click
        '
        Dim file As String
        Dim s$ = ""
        Dim i As Integer
        Dim TempEvent As New CEvent
        '
        On Error Resume Next
        If dlgScenarioOpen.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
        file = dlgScenarioOpen.FileName
        gest_txtName.Text = file
        FileOpen(1, file, OpenMode.Input)
        '
        On Error GoTo 0
        '
        s$ = (read_line())                      ' "!"
        s$ = (read_line())                      ' "! --------------"
        s$ = (read_line())                      ' "! List of events"
        s$ = (read_line())                      ' "! --------------"
        '
        s$ = (read_line())                      ' "!"
        EventsNumber = (read_line())            ' "Events number"
        s$ = (read_line())                      ' "!"
        '
        EventsList.Clear()
        For i = 1 To EventsNumber
            TempEvent = New CEvent
            With TempEvent
                .EventName = (read_line())              ' "Event name"
                s$ = (read_line())                      ' "--------------"
                .EventTime = (read_line())              ' "Time of occurrence"
                s$ = (read_line())                      ' "Description"
                .EventDescription = (read_whole_line()) '
                '
                .EventJR(0) = (read_line())             ' "Flywheel"
                .EventJR(1) = (read_line())             ' "|"
                .EventJR(2) = (read_line())             ' "|"
                .Eventom(0) = (read_line())             ' "|"
                .Eventom(1) = (read_line())             ' "|"
                .Eventom(2) = (read_line())             ' "|"
                '
                .Eventdipsat_active.X = (read_line())   ' "Magnetic control system moment"
                .Eventdipsat_active.Y = (read_line())   ' "|"
                .Eventdipsat_active.Z = (read_line())   ' "|"
                '
                .Eventjet(0) = (read_line())            ' "Nozzle moment"
                .Eventjet(1) = (read_line())            ' "|"
                .Eventjet(2) = (read_line())            ' "|"
                s$ = (read_line())                      ' "!"
            End With
            EventsList.Add(TempEvent)
            gest_lstEvents.Items.Add(TempEvent.EventName)
        Next
        '
        FileClose(1)
        ''
    End Sub

#Region "Reading values"

    Private Sub gest_txtTime_Leave(sender As Object, e As EventArgs) Handles gest_txtTime.Leave
        '
        Dim X As Long
        '
        If IsNumeric(gest_txtTime.Text) AndAlso Int(gest_txtTime.Text) = Math.Abs(CLng(gest_txtTime.Text)) Then
            X = CLng(gest_txtTime.Text)
            gest_txtTime.Text = Format(X, "#0")
        Else
            MsgBox("Please enter a positive integer !")
            gest_txtTime.Text = CStr(0)
            gest_txtTime.Focus()
        End If
        ''
    End Sub

#End Region

#End Region

    Private Sub cmd_quit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmd_quit.Click
        Me.Hide()
    End Sub

    Private Sub frmAttConf_closed_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = True
        Me.Hide()
    End Sub

#Region "ONLY FOR TEST functions"

    ''' <summary>
    ''' Public copy of update_attitude_data()
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub update_attitude_data_fortest()
        update_attitude_data()
    End Sub

    ''' <summary>
    ''' Public copy of almost every Button_Leave() function
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub update_window_data_fortest()
        mass_txtBodyHeigth_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtBodyWidth_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtPanelLength_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtPanelWidth_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtGx_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtGy_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtGz_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtIxx_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtIxy_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtIxz_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtIyy_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtIyz_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtIzz_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtBodyMass_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtPanelMass_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtBoomDist_Leave(mass_txtBodyHeight, New System.EventArgs)
        mass_txtBoomMass_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCN_0_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCN_1_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCN_2_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCN_3_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCN_4_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCN_5_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCT_0_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCT_1_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCT_2_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCT_3_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCT_4_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCT_5_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCR_0_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCR_1_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCR_2_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCR_3_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCR_4_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCR_5_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCD_0_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCD_1_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCD_2_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCD_3_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCD_4_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtCD_5_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtMG_X_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtMG_Y_Leave(mass_txtBodyHeight, New System.EventArgs)
        prop_txtMG_Z_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtMX_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtMY_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtMZ_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtDX_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtDY_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtDZ_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtCX_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtCY_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtCZ_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtKX_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtKY_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtKZ_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtMG_X_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtMG_Y_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtMG_Z_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtMN_X_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtMN_Y_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtMN_Z_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtIRX_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtIRY_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtIRZ_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtWRX_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtWRY_Leave(mass_txtBodyHeight, New System.EventArgs)
        act_txtWRZ_Leave(mass_txtBodyHeight, New System.EventArgs)
    End Sub

#End Region

#Region "COPY NEEDED functions"

    ''' <summary>
    ''' Public copy of update_attitude_data()
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub update_attitude_data_for_frmMain()
        update_attitude_data()
    End Sub

#End Region

End Class
