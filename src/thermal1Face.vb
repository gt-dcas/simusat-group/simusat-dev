﻿Module thermalball
    ' Central body thermical characteristics
    Public BodyAlf(5) As Single     ' Absorptivity of body faces  0: +x / 1: -x / 2: +y / 3: -y / 4: +z / 5: -z
    Public BodyEps(5) As Single     ' Emissivity of body faces
    Public BodySpecHeat As Single   ' Body specific heat
    Public BodyTemp As Single       ' Body temperature
    Public T_init_body As Single    ' Initial temperature of central body

    Public flux_solaire As Single
    Public albedo As Single
    Public cos_sun_earth_sat As Double
    Public flux_ir As Double
    Public flux_albedo As Double

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Face_SolarFlux As Double     ' Solar flux on faces   
    Public Face_AlbedoFlux As Double    ' Albedo flux on faces   
    Public Face_IRFlux As Double        ' Infrared flux on faces 
    Public Face_SunInc As Single
    Public Face_EarthInc As Double
    Public CFprueba As Double
    Public Posprueba As Double
    ''' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


    Public Function solar_intensity(ByVal d As Double) As Single
        '
        ' Direct solar radiation intensity for a given distance to Sun
        ' Input: d distance to Sun
        '
        solar_intensity = CSng(SunPow / 4 / pi / d / d / 1000000.0#)
        '
    End Function

    Public Function flux_incident_overall_power(ByVal SolarPanel As CSolarPanel, ByVal Time As Double) As Double
        '
        ' Returns the value of the normal-equivalent incident energie flux related to current
        ' sun & earth position, also taking into account the effect of sun incidence relative to the panel
        '
        ' Input : solar panel
        '
        Dim fs As Double
        Dim fa As Double
        Dim sun_flux_ratio As Double
        Dim albedo_flux_ratio As Double
        '
        sun_flux_ratio = flux_effective_ratio(SolarPanel.SunIncidence(Time))
        If blnEclipse Then
            sun_flux_ratio = 0
        End If
        If sun_flux_ratio > 0 Then
            fs = solar_intensity(vctNorm(SunPos)) * sun_flux_ratio
        Else
            fs = 0
        End If
        albedo_flux_ratio = flux_effective_ratio(SolarPanel.EarthIncidence(Time))
        If albedo_flux_ratio > 0 Then
            fa = flux_albedo * albedo_flux_ratio
        Else
            fa = 0
        End If
        flux_incident_overall_power = fa + fs
        SolarPanel.PanelEffFlux = flux_incident_overall_power
        ''
    End Function

    Public Function flux_effective_ratio(ByVal teta As Double) As Double
        '
        ' Returns portion of equivalent incident flux related to the
        ' sun angle over the cells plain taking account the curve which is entered in the solar cells definition
        ' in the energy subsystem data window.
        '
        ' Input:
        '   - Angle of incidence
        '
        Dim p(2) As Double
        Dim tm As Double
        Dim td As Double
        Dim X As Double
        tm = cell_tetam * rad
        td = cell_tetad * rad
        '
        ' Features of Parabolical section
        p(0) = (Math.Sin(td) * (tm - td) - Math.Cos(td)) / (tm - td) ^ 2
        p(1) = -Math.Sin(td) - 2 * p(0) * td
        p(2) = -p(0) * tm * tm - p(1) * tm
        If teta <= td Then
            X = Math.Cos(teta)
        ElseIf teta > td And teta < tm Then
            X = p(0) * teta * teta + p(1) * teta + p(2)
        Else
            X = 0
        End If
        If X < 0 Then
            X = 0
        End If
        flux_effective_ratio = X
        ''
    End Function

    Public Sub flux_calculation(ByVal j_current As Double)
        '
        ' Solar flux, albedo and earth IR calculation
        '
        ' Entrée : j_current : Current time in Julian days
        '
        Dim r_sat_earth As Double
        Dim r_sun_earth As Double
        Dim earth_angle_from_sat As Double
        Dim X As Double
        Dim udtGregorianNow As udtDate
        '
        '
        ' Solar flux calculation
        ' ----------------------
        ' The current value for the solar constant results from the Sat-Sun distance and
        ' decreases with is 2nd power
        flux_solaire = solar_intensity(vctNorm(SunPos))
        '
        ' Albedo calculation
        ' ------------------
        ' SatPos and SunPos are respectively Earth_Sat and Earth_Sun vectors
        ' Then Sat_Earth and Sat_Sun unitary vectors are obtained, as well as Earth's disc
        ' (by mean of its angular radius earth_angle_from_sat).
        ' The boolean blnEclipse (already obtained by orbitography updating) specifies if the satellite is in eclipse or not (True/False respectively).
        r_sat_earth = vctNorm(SatPos)
        r_sun_earth = vctNorm(SunPos)
        earth_angle_from_sat = Math.Asin(RE / r_sat_earth)
        '
        ' That is, flux_solaire * albedo * ka * cos (angle (sat/sun,sat/earth))
        ' with ka = 0.664 + 0.521 * earth_angle_from_sat - 0.203 * earth_angle_from_sat ^ 2
        ' and earth_angle_from_sat = earth's angular radius as seen from the satellite
        ' See LARSON-WERTZ
        ' We take the equatorial radius of earth since its value is the greatest
        ' Albedo is related to current sat/earth/sun angle through the factor 
        ' [cos(0.9*angle)]^1.5 (CNES) means that above 100° no albedo reaches the satellite

        cos_sun_earth_sat = vctDot(SunPos, SatPos) / r_sun_earth / r_sat_earth
        X = Math.Acos(cos_sun_earth_sat)
        X = Math.Cos(0.9 * X)
        If X < 0 Then X = 0 ' If flux is negative (angle sun/sat/earth > 100°
        X = Math.Sqrt(X ^ 3)
        ' Current values for albedo and ir depend on which is the current month
        ' within the year.
        convert_Julian_Gregorian(j_current, udtGregorianNow)

        'albedo = 0.3
        albedo = AlbedoCoef(SatGeoPos.lat, udtGregorianNow.month)
        flux_albedo = flux_solaire * albedo * X
        '
        ' IR flux calculation
        ' -------------------
        'flux_ir = IR_month(udtGregorianNow.month - 1) * (RE / r_sat_earth) ^ 2
        flux_ir = C_Stefan * Math.Pow(EarthTemp(SatGeoPos.lat, udtGregorianNow.month), 4)
        ''
    End Sub
    Public Sub compute_thermal(ByVal Time As Double, ByVal TimeStep As Double)
        '
        Dim deltaT As Double
        Dim alf(1) As Double    ' Absorptivity
        Dim eps(1) As Double    ' Emissivity
        Dim fs(1) As Double, fs1(1) As Double, fs2(1) As Double     ' Solar + albedo flux
        Dim fir(1) As Double    ' Earth Infra Red flux
        Dim X As Single
        Dim SunDir As vector                ' Sun direction in the satellite's coordinates system 
        Dim EarthDir As vector              ' Earth direction in the satellite's coordinates system 
        Dim Face_dir As vector
        Dim tmpVect As vector
        '
        ' ----------------------------
        ' Central body thermal balance 
        ' ----------------------------
        With Satellite.getSatCOSCoords(Time, SunPos)
            SunDir.X = .X : SunDir.Y = .Y : SunDir.Z = .Z
        End With
        '
        Face_dir.X = 1 : Face_dir.Y = 0 : Face_dir.Z = 0
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' ini
        'Dim Face_SolarFlux As Double     ' Solar flux on faces   
        'Dim Face_AlbedoFlux As Double    ' Albedo flux on faces   
        'Dim Face_IRFlux As Double        ' Infrared flux on faces 
        'Dim Face_SunInc As Single
        'Dim Face_EarthInc As Single
        '
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' fin
        '
        'Sun flux
        Face_SunInc = Math.Acos(vctDot(SunDir, Face_dir) / vctNorm(SunDir) / vctNorm(Face_dir))
        Face_SolarFlux = flux_solaire * Math.Cos(Face_SunInc) * 1 * 0.3 'BodyAlf(i)*facesurf
        If blnEclipse Or Math.Cos(Face_SunInc) <= 0 Then
            Face_SolarFlux = 0
        End If

        '
        ' Albedo flux

        tmpVect.X = -SatPos.X : tmpVect.Y = -SatPos.Y : tmpVect.Z = -SatPos.Z 'modif here for thermal try...
        With Satellite.getSatCOSCoords(Time, tmpVect)
            EarthDir.X = .X : EarthDir.Y = .Y : EarthDir.Z = .Z ' Earth direction in the satellite's COS
        End With
        Face_EarthInc = Math.Acos(vctDot(EarthDir, Face_dir) / vctNorm(EarthDir) / vctNorm(Face_dir)) * 180 / pi
        Posprueba = vctNorm(SatPos)
        CFprueba = EarthIRConfFactor(Face_EarthInc, Posprueba)
        Face_AlbedoFlux = flux_albedo * EarthIRConfFactor(Face_EarthInc, vctNorm(SatPos)) * 0.3 'BodyAlf(i)*facesurf


        ' Received Infrared flux
        Face_IRFlux = flux_ir * EarthIRConfFactor(Face_EarthInc, vctNorm(SatPos)) * 1 * 0.02 'BodyEps(i)*facesurf



        ' Global thermal balance of body
        ' ------------------------------
        X = 0
        ' Add each face global power

        X += Face_SolarFlux + Face_AlbedoFlux + Face_IRFlux

        X -= C_Stefan * (BodyTemp + 273.15) ^ 4 * 1 * 0.02 'Face_Surf(i) * BodyEps(i) 

        ' Add dissipated power
        X += pdiss

        ' Compute temperature
        deltaT = (X / BodySpecHeat) * TimeStep * secday
        ' 
        BodyTemp += deltaT
        '

    End Sub

End Module
