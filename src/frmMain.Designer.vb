<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMain
#Region "Code g�n�r� par le Concepteur Windows Form "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Cet appel est requis par le Concepteur Windows Form.
		InitializeComponent()
		Form_Initialize_renamed()
	End Sub
	'Form remplace la m�thode Dispose pour nettoyer la liste des composants.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requise par le Concepteur Windows Form
	Private components As System.ComponentModel.IContainer
    Public WithEvents mnuOpenProject As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileLine1 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuSaveAs As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileLine3 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuQuit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSatellite As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnu_MountPanels As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnu2DView As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnu3DView As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuAttitudeSphere As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSimulationLine1 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuIncrease As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuDecrease As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuSimulationLine2 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuPlay As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuPause As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuStop As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSimulationLine4 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuOneStepFwd As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuOneStepBack As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSimulationLine3 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents mnuSimulation As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    ' vue 3D satellite
    ' vue 2D planisphere
    ' vue 3D espace
    Public dlgMainOpen As System.Windows.Forms.OpenFileDialog
    Public dlgMainSave As System.Windows.Forms.SaveFileDialog
    'REMARQUE�: la proc�dure suivante est requise par le Concepteur Windows Form
    'Elle peut �tre modifi�e � l'aide du Concepteur Windows Form.
    'Ne la modifiez pas � l'aide de l'�diteur de code.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOpenProject = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileLine1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSaveAs = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileLine3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuQuit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSatellite = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnu_MountPanels = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSimulation = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnu2DView = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnu3DView = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAttitudeSphere = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSimulationLine1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuIncrease = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDecrease = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSimulationLine2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuPlay = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPause = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStop = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSimulationLine4 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuOneStepFwd = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOneStepBack = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSimulationLine3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.dlgMainOpen = New System.Windows.Forms.OpenFileDialog()
        Me.dlgMainSave = New System.Windows.Forms.SaveFileDialog()
        Me.cmd = New System.Windows.Forms.RadioButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.zlbl_8 = New System.Windows.Forms.Label()
        Me.zlbl_11 = New System.Windows.Forms.Label()
        Me.zlbl_10 = New System.Windows.Forms.Label()
        Me.zlbl_7 = New System.Windows.Forms.Label()
        Me.cmdGestAtt = New System.Windows.Forms.RadioButton()
        Me.cmdAct = New System.Windows.Forms.RadioButton()
        Me.cmdMassGeo = New System.Windows.Forms.RadioButton()
        Me.cmdProperties = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.zlbl_15 = New System.Windows.Forms.Label()
        Me.zlbl_14 = New System.Windows.Forms.Label()
        Me.cmdCurvesAtt = New System.Windows.Forms.RadioButton()
        Me.cmdReportAtt = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.OrbitOpen = New System.Windows.Forms.RadioButton()
        Me.OrbitSave = New System.Windows.Forms.RadioButton()
        Me.zlbl_13 = New System.Windows.Forms.Label()
        Me.zlbl_12 = New System.Windows.Forms.Label()
        Me.cmdOrbit = New System.Windows.Forms.RadioButton()
        Me.cmdEnvir = New System.Windows.Forms.RadioButton()
        Me.fraDisplay = New System.Windows.Forms.GroupBox()
        Me.zlbl_6 = New System.Windows.Forms.Label()
        Me.zlbl_5 = New System.Windows.Forms.Label()
        Me.zlbl_4 = New System.Windows.Forms.Label()
        Me.optView_3 = New System.Windows.Forms.RadioButton()
        Me.optView_0 = New System.Windows.Forms.RadioButton()
        Me.optView_1 = New System.Windows.Forms.RadioButton()
        Me.fraSimulation = New System.Windows.Forms.GroupBox()
        Me.updwTimeStep = New System.Windows.Forms.DomainUpDown()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.zlbl_1 = New System.Windows.Forms.Label()
        Me.txt_date_debut = New System.Windows.Forms.DateTimePicker()
        Me.zlbl_3 = New System.Windows.Forms.Label()
        Me.zlbl_0 = New System.Windows.Forms.Label()
        Me.cmdOneFwd = New System.Windows.Forms.Button()
        Me.cmdFwd = New System.Windows.Forms.Button()
        Me.cmdOneBack = New System.Windows.Forms.Button()
        Me.cmdStop = New System.Windows.Forms.Button()
        Me.cmdPause = New System.Windows.Forms.Button()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.zlb_16 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmdPowerDiagram = New System.Windows.Forms.RadioButton()
        Me.cmdCurvesPwr = New System.Windows.Forms.RadioButton()
        Me.cmdReportPwr = New System.Windows.Forms.RadioButton()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.zlbl_9 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmdBat = New System.Windows.Forms.RadioButton()
        Me.cmdGestPwr = New System.Windows.Forms.RadioButton()
        Me.cmdTherm = New System.Windows.Forms.RadioButton()
        Me.cmdCells = New System.Windows.Forms.RadioButton()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmdActuactors = New System.Windows.Forms.RadioButton()
        Me.cmdSave = New System.Windows.Forms.RadioButton()
        Me.cmdOpen = New System.Windows.Forms.RadioButton()
        Me.cmdNew = New System.Windows.Forms.RadioButton()
        Me.zlbl_16 = New System.Windows.Forms.Label()
        Me.zlbl_17 = New System.Windows.Forms.Label()
        Me.zlb_18 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.zlbl_18 = New System.Windows.Forms.Label()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.fraDisplay.SuspendLayout()
        Me.fraSimulation.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuOpenProject, Me.mnuFileLine1, Me.mnuSaveAs, Me.mnuFileLine3, Me.mnuQuit})
        Me.mnuFile.Name = "mnuFile"
        resources.ApplyResources(Me.mnuFile, "mnuFile")
        '
        'mnuOpenProject
        '
        Me.mnuOpenProject.Name = "mnuOpenProject"
        resources.ApplyResources(Me.mnuOpenProject, "mnuOpenProject")
        '
        'mnuFileLine1
        '
        Me.mnuFileLine1.Name = "mnuFileLine1"
        resources.ApplyResources(Me.mnuFileLine1, "mnuFileLine1")
        '
        'mnuSaveAs
        '
        Me.mnuSaveAs.Name = "mnuSaveAs"
        resources.ApplyResources(Me.mnuSaveAs, "mnuSaveAs")
        '
        'mnuFileLine3
        '
        Me.mnuFileLine3.Name = "mnuFileLine3"
        resources.ApplyResources(Me.mnuFileLine3, "mnuFileLine3")
        '
        'mnuQuit
        '
        Me.mnuQuit.Name = "mnuQuit"
        resources.ApplyResources(Me.mnuQuit, "mnuQuit")
        '
        'mnuSatellite
        '
        Me.mnuSatellite.Name = "mnuSatellite"
        resources.ApplyResources(Me.mnuSatellite, "mnuSatellite")
        '
        'mnu_MountPanels
        '
        Me.mnu_MountPanels.Name = "mnu_MountPanels"
        resources.ApplyResources(Me.mnu_MountPanels, "mnu_MountPanels")
        '
        'mnuSimulation
        '
        Me.mnuSimulation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnu2DView, Me.mnu3DView, Me.mnuAttitudeSphere, Me.mnuSimulationLine1, Me.mnuIncrease, Me.mnuDecrease, Me.mnuSimulationLine2, Me.mnuPlay, Me.mnuPause, Me.mnuStop, Me.mnuSimulationLine4, Me.mnuOneStepFwd, Me.mnuOneStepBack, Me.mnuSimulationLine3})
        Me.mnuSimulation.Name = "mnuSimulation"
        resources.ApplyResources(Me.mnuSimulation, "mnuSimulation")
        '
        'mnu2DView
        '
        Me.mnu2DView.Name = "mnu2DView"
        resources.ApplyResources(Me.mnu2DView, "mnu2DView")
        '
        'mnu3DView
        '
        Me.mnu3DView.Name = "mnu3DView"
        resources.ApplyResources(Me.mnu3DView, "mnu3DView")
        '
        'mnuAttitudeSphere
        '
        Me.mnuAttitudeSphere.Name = "mnuAttitudeSphere"
        resources.ApplyResources(Me.mnuAttitudeSphere, "mnuAttitudeSphere")
        '
        'mnuSimulationLine1
        '
        Me.mnuSimulationLine1.Name = "mnuSimulationLine1"
        resources.ApplyResources(Me.mnuSimulationLine1, "mnuSimulationLine1")
        '
        'mnuIncrease
        '
        Me.mnuIncrease.Name = "mnuIncrease"
        resources.ApplyResources(Me.mnuIncrease, "mnuIncrease")
        '
        'mnuDecrease
        '
        Me.mnuDecrease.Name = "mnuDecrease"
        resources.ApplyResources(Me.mnuDecrease, "mnuDecrease")
        '
        'mnuSimulationLine2
        '
        Me.mnuSimulationLine2.Name = "mnuSimulationLine2"
        resources.ApplyResources(Me.mnuSimulationLine2, "mnuSimulationLine2")
        '
        'mnuPlay
        '
        Me.mnuPlay.Name = "mnuPlay"
        resources.ApplyResources(Me.mnuPlay, "mnuPlay")
        '
        'mnuPause
        '
        Me.mnuPause.Name = "mnuPause"
        resources.ApplyResources(Me.mnuPause, "mnuPause")
        '
        'mnuStop
        '
        Me.mnuStop.Name = "mnuStop"
        resources.ApplyResources(Me.mnuStop, "mnuStop")
        '
        'mnuSimulationLine4
        '
        Me.mnuSimulationLine4.Name = "mnuSimulationLine4"
        resources.ApplyResources(Me.mnuSimulationLine4, "mnuSimulationLine4")
        '
        'mnuOneStepFwd
        '
        Me.mnuOneStepFwd.Name = "mnuOneStepFwd"
        resources.ApplyResources(Me.mnuOneStepFwd, "mnuOneStepFwd")
        '
        'mnuOneStepBack
        '
        Me.mnuOneStepBack.Name = "mnuOneStepBack"
        resources.ApplyResources(Me.mnuOneStepBack, "mnuOneStepBack")
        '
        'mnuSimulationLine3
        '
        Me.mnuSimulationLine3.Name = "mnuSimulationLine3"
        resources.ApplyResources(Me.mnuSimulationLine3, "mnuSimulationLine3")
        '
        'mnuHelp
        '
        Me.mnuHelp.Name = "mnuHelp"
        resources.ApplyResources(Me.mnuHelp, "mnuHelp")
        '
        'dlgMainOpen
        '
        resources.ApplyResources(Me.dlgMainOpen, "dlgMainOpen")
        '
        'dlgMainSave
        '
        resources.ApplyResources(Me.dlgMainSave, "dlgMainSave")
        '
        'cmd
        '
        Me.cmd.BackColor = System.Drawing.Color.Red
        Me.cmd.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmd.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmd.ForeColor = System.Drawing.SystemColors.ControlText
        resources.ApplyResources(Me.cmd, "cmd")
        Me.cmd.Name = "cmd"
        Me.cmd.UseVisualStyleBackColor = True
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'zlbl_8
        '
        resources.ApplyResources(Me.zlbl_8, "zlbl_8")
        Me.zlbl_8.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_8.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_8.ForeColor = System.Drawing.Color.White
        Me.zlbl_8.Name = "zlbl_8"
        '
        'zlbl_11
        '
        resources.ApplyResources(Me.zlbl_11, "zlbl_11")
        Me.zlbl_11.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_11.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_11.ForeColor = System.Drawing.Color.White
        Me.zlbl_11.Name = "zlbl_11"
        '
        'zlbl_10
        '
        resources.ApplyResources(Me.zlbl_10, "zlbl_10")
        Me.zlbl_10.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_10.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_10.ForeColor = System.Drawing.Color.White
        Me.zlbl_10.Name = "zlbl_10"
        '
        'zlbl_7
        '
        resources.ApplyResources(Me.zlbl_7, "zlbl_7")
        Me.zlbl_7.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_7.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_7.ForeColor = System.Drawing.Color.White
        Me.zlbl_7.Name = "zlbl_7"
        '
        'cmdGestAtt
        '
        resources.ApplyResources(Me.cmdGestAtt, "cmdGestAtt")
        Me.cmdGestAtt.BackColor = System.Drawing.SystemColors.Control
        Me.cmdGestAtt.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdGestAtt.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdGestAtt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdGestAtt.Name = "cmdGestAtt"
        Me.cmdGestAtt.UseVisualStyleBackColor = False
        '
        'cmdAct
        '
        resources.ApplyResources(Me.cmdAct, "cmdAct")
        Me.cmdAct.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAct.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAct.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdAct.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAct.Name = "cmdAct"
        Me.cmdAct.UseVisualStyleBackColor = False
        '
        'cmdMassGeo
        '
        resources.ApplyResources(Me.cmdMassGeo, "cmdMassGeo")
        Me.cmdMassGeo.BackColor = System.Drawing.SystemColors.Control
        Me.cmdMassGeo.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdMassGeo.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdMassGeo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdMassGeo.Name = "cmdMassGeo"
        Me.cmdMassGeo.UseVisualStyleBackColor = False
        '
        'cmdProperties
        '
        resources.ApplyResources(Me.cmdProperties, "cmdProperties")
        Me.cmdProperties.BackColor = System.Drawing.SystemColors.Control
        Me.cmdProperties.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdProperties.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdProperties.Name = "cmdProperties"
        Me.cmdProperties.UseVisualStyleBackColor = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.zlbl_15)
        Me.GroupBox3.Controls.Add(Me.zlbl_14)
        Me.GroupBox3.Controls.Add(Me.cmdCurvesAtt)
        Me.GroupBox3.Controls.Add(Me.cmdReportAtt)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.ForeColor = System.Drawing.Color.White
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'zlbl_15
        '
        resources.ApplyResources(Me.zlbl_15, "zlbl_15")
        Me.zlbl_15.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_15.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_15.ForeColor = System.Drawing.Color.White
        Me.zlbl_15.Name = "zlbl_15"
        '
        'zlbl_14
        '
        resources.ApplyResources(Me.zlbl_14, "zlbl_14")
        Me.zlbl_14.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_14.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_14.ForeColor = System.Drawing.Color.White
        Me.zlbl_14.Name = "zlbl_14"
        '
        'cmdCurvesAtt
        '
        resources.ApplyResources(Me.cmdCurvesAtt, "cmdCurvesAtt")
        Me.cmdCurvesAtt.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCurvesAtt.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCurvesAtt.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdCurvesAtt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCurvesAtt.Name = "cmdCurvesAtt"
        Me.cmdCurvesAtt.UseVisualStyleBackColor = False
        '
        'cmdReportAtt
        '
        resources.ApplyResources(Me.cmdReportAtt, "cmdReportAtt")
        Me.cmdReportAtt.BackColor = System.Drawing.SystemColors.Control
        Me.cmdReportAtt.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdReportAtt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdReportAtt.Name = "cmdReportAtt"
        Me.cmdReportAtt.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.GroupBox2.Controls.Add(Me.OrbitOpen)
        Me.GroupBox2.Controls.Add(Me.OrbitSave)
        Me.GroupBox2.Controls.Add(Me.zlbl_13)
        Me.GroupBox2.Controls.Add(Me.zlbl_12)
        Me.GroupBox2.Controls.Add(Me.cmdOrbit)
        Me.GroupBox2.Controls.Add(Me.cmdEnvir)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.ForeColor = System.Drawing.Color.OldLace
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'OrbitOpen
        '
        resources.ApplyResources(Me.OrbitOpen, "OrbitOpen")
        Me.OrbitOpen.BackColor = System.Drawing.SystemColors.Control
        Me.OrbitOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.OrbitOpen.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.OrbitOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.OrbitOpen.Name = "OrbitOpen"
        Me.OrbitOpen.UseVisualStyleBackColor = False
        '
        'OrbitSave
        '
        resources.ApplyResources(Me.OrbitSave, "OrbitSave")
        Me.OrbitSave.BackColor = System.Drawing.SystemColors.Control
        Me.OrbitSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.OrbitSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.OrbitSave.Name = "OrbitSave"
        Me.OrbitSave.UseVisualStyleBackColor = False
        '
        'zlbl_13
        '
        resources.ApplyResources(Me.zlbl_13, "zlbl_13")
        Me.zlbl_13.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_13.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_13.ForeColor = System.Drawing.Color.White
        Me.zlbl_13.Name = "zlbl_13"
        '
        'zlbl_12
        '
        resources.ApplyResources(Me.zlbl_12, "zlbl_12")
        Me.zlbl_12.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_12.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_12.ForeColor = System.Drawing.Color.White
        Me.zlbl_12.Name = "zlbl_12"
        '
        'cmdOrbit
        '
        resources.ApplyResources(Me.cmdOrbit, "cmdOrbit")
        Me.cmdOrbit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOrbit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOrbit.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdOrbit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOrbit.Name = "cmdOrbit"
        Me.cmdOrbit.UseVisualStyleBackColor = False
        '
        'cmdEnvir
        '
        resources.ApplyResources(Me.cmdEnvir, "cmdEnvir")
        Me.cmdEnvir.BackColor = System.Drawing.SystemColors.Control
        Me.cmdEnvir.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdEnvir.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdEnvir.Name = "cmdEnvir"
        Me.cmdEnvir.UseVisualStyleBackColor = False
        '
        'fraDisplay
        '
        Me.fraDisplay.Controls.Add(Me.zlbl_6)
        Me.fraDisplay.Controls.Add(Me.zlbl_5)
        Me.fraDisplay.Controls.Add(Me.zlbl_4)
        Me.fraDisplay.Controls.Add(Me.optView_3)
        Me.fraDisplay.Controls.Add(Me.optView_0)
        Me.fraDisplay.Controls.Add(Me.optView_1)
        resources.ApplyResources(Me.fraDisplay, "fraDisplay")
        Me.fraDisplay.ForeColor = System.Drawing.Color.White
        Me.fraDisplay.Name = "fraDisplay"
        Me.fraDisplay.TabStop = False
        '
        'zlbl_6
        '
        resources.ApplyResources(Me.zlbl_6, "zlbl_6")
        Me.zlbl_6.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_6.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_6.ForeColor = System.Drawing.Color.White
        Me.zlbl_6.Name = "zlbl_6"
        '
        'zlbl_5
        '
        resources.ApplyResources(Me.zlbl_5, "zlbl_5")
        Me.zlbl_5.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_5.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_5.ForeColor = System.Drawing.Color.White
        Me.zlbl_5.Name = "zlbl_5"
        '
        'zlbl_4
        '
        resources.ApplyResources(Me.zlbl_4, "zlbl_4")
        Me.zlbl_4.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_4.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_4.ForeColor = System.Drawing.Color.White
        Me.zlbl_4.Name = "zlbl_4"
        '
        'optView_3
        '
        resources.ApplyResources(Me.optView_3, "optView_3")
        Me.optView_3.BackColor = System.Drawing.SystemColors.Control
        Me.optView_3.Cursor = System.Windows.Forms.Cursors.Default
        Me.optView_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optView_3.Name = "optView_3"
        Me.optView_3.UseVisualStyleBackColor = False
        '
        'optView_0
        '
        resources.ApplyResources(Me.optView_0, "optView_0")
        Me.optView_0.BackColor = System.Drawing.SystemColors.Control
        Me.optView_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.optView_0.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.optView_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optView_0.Name = "optView_0"
        Me.optView_0.UseVisualStyleBackColor = False
        '
        'optView_1
        '
        resources.ApplyResources(Me.optView_1, "optView_1")
        Me.optView_1.BackColor = System.Drawing.SystemColors.Control
        Me.optView_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.optView_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optView_1.Name = "optView_1"
        Me.optView_1.UseVisualStyleBackColor = False
        '
        'fraSimulation
        '
        Me.fraSimulation.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.fraSimulation.Controls.Add(Me.updwTimeStep)
        Me.fraSimulation.Controls.Add(Me.lblDate)
        Me.fraSimulation.Controls.Add(Me.zlbl_1)
        Me.fraSimulation.Controls.Add(Me.txt_date_debut)
        Me.fraSimulation.Controls.Add(Me.zlbl_3)
        Me.fraSimulation.Controls.Add(Me.zlbl_0)
        Me.fraSimulation.Controls.Add(Me.cmdOneFwd)
        Me.fraSimulation.Controls.Add(Me.cmdFwd)
        Me.fraSimulation.Controls.Add(Me.cmdOneBack)
        Me.fraSimulation.Controls.Add(Me.cmdStop)
        Me.fraSimulation.Controls.Add(Me.cmdPause)
        resources.ApplyResources(Me.fraSimulation, "fraSimulation")
        Me.fraSimulation.ForeColor = System.Drawing.Color.White
        Me.fraSimulation.Name = "fraSimulation"
        Me.fraSimulation.TabStop = False
        '
        'updwTimeStep
        '
        resources.ApplyResources(Me.updwTimeStep, "updwTimeStep")
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items1"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items2"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items3"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items4"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items5"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items6"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items7"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items8"))
        Me.updwTimeStep.Items.Add(resources.GetString("updwTimeStep.Items9"))
        Me.updwTimeStep.Name = "updwTimeStep"
        Me.updwTimeStep.ReadOnly = True
        '
        'lblDate
        '
        Me.lblDate.BackColor = System.Drawing.Color.White
        Me.lblDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDate.Cursor = System.Windows.Forms.Cursors.Default
        resources.ApplyResources(Me.lblDate, "lblDate")
        Me.lblDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDate.Name = "lblDate"
        '
        'zlbl_1
        '
        resources.ApplyResources(Me.zlbl_1, "zlbl_1")
        Me.zlbl_1.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_1.ForeColor = System.Drawing.Color.White
        Me.zlbl_1.Name = "zlbl_1"
        '
        'txt_date_debut
        '
        resources.ApplyResources(Me.txt_date_debut, "txt_date_debut")
        Me.txt_date_debut.CausesValidation = False
        Me.txt_date_debut.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txt_date_debut.Name = "txt_date_debut"
        Me.txt_date_debut.Value = New Date(2000, 1, 1, 0, 0, 0, 0)
        '
        'zlbl_3
        '
        resources.ApplyResources(Me.zlbl_3, "zlbl_3")
        Me.zlbl_3.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_3.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_3.ForeColor = System.Drawing.Color.White
        Me.zlbl_3.Name = "zlbl_3"
        '
        'zlbl_0
        '
        resources.ApplyResources(Me.zlbl_0, "zlbl_0")
        Me.zlbl_0.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_0.ForeColor = System.Drawing.Color.White
        Me.zlbl_0.Name = "zlbl_0"
        '
        'cmdOneFwd
        '
        resources.ApplyResources(Me.cmdOneFwd, "cmdOneFwd")
        Me.cmdOneFwd.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOneFwd.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOneFwd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOneFwd.Name = "cmdOneFwd"
        Me.cmdOneFwd.UseVisualStyleBackColor = False
        '
        'cmdFwd
        '
        resources.ApplyResources(Me.cmdFwd, "cmdFwd")
        Me.cmdFwd.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFwd.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFwd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFwd.Name = "cmdFwd"
        Me.cmdFwd.UseVisualStyleBackColor = False
        '
        'cmdOneBack
        '
        resources.ApplyResources(Me.cmdOneBack, "cmdOneBack")
        Me.cmdOneBack.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOneBack.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOneBack.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOneBack.Name = "cmdOneBack"
        Me.cmdOneBack.UseVisualStyleBackColor = False
        '
        'cmdStop
        '
        resources.ApplyResources(Me.cmdStop, "cmdStop")
        Me.cmdStop.BackColor = System.Drawing.Color.White
        Me.cmdStop.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdStop.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdStop.Name = "cmdStop"
        Me.cmdStop.UseVisualStyleBackColor = False
        '
        'cmdPause
        '
        resources.ApplyResources(Me.cmdPause, "cmdPause")
        Me.cmdPause.BackColor = System.Drawing.Color.White
        Me.cmdPause.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdPause.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdPause.Name = "cmdPause"
        Me.cmdPause.UseVisualStyleBackColor = False
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.zlb_16)
        Me.GroupBox7.Controls.Add(Me.Label4)
        Me.GroupBox7.Controls.Add(Me.Label5)
        Me.GroupBox7.Controls.Add(Me.cmdPowerDiagram)
        Me.GroupBox7.Controls.Add(Me.cmdCurvesPwr)
        Me.GroupBox7.Controls.Add(Me.cmdReportPwr)
        resources.ApplyResources(Me.GroupBox7, "GroupBox7")
        Me.GroupBox7.ForeColor = System.Drawing.Color.White
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.TabStop = False
        '
        'zlb_16
        '
        resources.ApplyResources(Me.zlb_16, "zlb_16")
        Me.zlb_16.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlb_16.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlb_16.ForeColor = System.Drawing.Color.White
        Me.zlb_16.Name = "zlb_16"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Name = "Label4"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Name = "Label5"
        '
        'cmdPowerDiagram
        '
        resources.ApplyResources(Me.cmdPowerDiagram, "cmdPowerDiagram")
        Me.cmdPowerDiagram.BackColor = System.Drawing.SystemColors.Control
        Me.cmdPowerDiagram.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdPowerDiagram.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdPowerDiagram.Name = "cmdPowerDiagram"
        Me.cmdPowerDiagram.UseVisualStyleBackColor = False
        '
        'cmdCurvesPwr
        '
        resources.ApplyResources(Me.cmdCurvesPwr, "cmdCurvesPwr")
        Me.cmdCurvesPwr.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCurvesPwr.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCurvesPwr.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdCurvesPwr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCurvesPwr.Name = "cmdCurvesPwr"
        Me.cmdCurvesPwr.UseVisualStyleBackColor = False
        '
        'cmdReportPwr
        '
        resources.ApplyResources(Me.cmdReportPwr, "cmdReportPwr")
        Me.cmdReportPwr.BackColor = System.Drawing.SystemColors.Control
        Me.cmdReportPwr.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdReportPwr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdReportPwr.Name = "cmdReportPwr"
        Me.cmdReportPwr.UseVisualStyleBackColor = False
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'zlbl_9
        '
        resources.ApplyResources(Me.zlbl_9, "zlbl_9")
        Me.zlbl_9.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_9.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_9.ForeColor = System.Drawing.Color.White
        Me.zlbl_9.Name = "zlbl_9"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Name = "Label1"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Name = "Label2"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Name = "Label3"
        '
        'cmdBat
        '
        resources.ApplyResources(Me.cmdBat, "cmdBat")
        Me.cmdBat.BackColor = System.Drawing.SystemColors.Control
        Me.cmdBat.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdBat.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdBat.Name = "cmdBat"
        Me.cmdBat.UseVisualStyleBackColor = False
        '
        'cmdGestPwr
        '
        resources.ApplyResources(Me.cmdGestPwr, "cmdGestPwr")
        Me.cmdGestPwr.BackColor = System.Drawing.SystemColors.Control
        Me.cmdGestPwr.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdGestPwr.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdGestPwr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdGestPwr.Name = "cmdGestPwr"
        Me.cmdGestPwr.UseVisualStyleBackColor = False
        '
        'cmdTherm
        '
        resources.ApplyResources(Me.cmdTherm, "cmdTherm")
        Me.cmdTherm.BackColor = System.Drawing.SystemColors.Control
        Me.cmdTherm.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdTherm.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdTherm.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdTherm.Name = "cmdTherm"
        Me.cmdTherm.UseVisualStyleBackColor = False
        '
        'cmdCells
        '
        resources.ApplyResources(Me.cmdCells, "cmdCells")
        Me.cmdCells.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCells.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCells.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCells.Name = "cmdCells"
        Me.cmdCells.UseVisualStyleBackColor = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem})
        resources.ApplyResources(Me.MenuStrip1, "MenuStrip1")
        Me.MenuStrip1.Name = "MenuStrip1"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        resources.ApplyResources(Me.AboutToolStripMenuItem, "AboutToolStripMenuItem")
        '
        'cmdActuactors
        '
        resources.ApplyResources(Me.cmdActuactors, "cmdActuactors")
        Me.cmdActuactors.BackColor = System.Drawing.SystemColors.Control
        Me.cmdActuactors.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdActuactors.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdActuactors.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdActuactors.Name = "cmdActuactors"
        Me.cmdActuactors.UseVisualStyleBackColor = False
        '
        'cmdSave
        '
        resources.ApplyResources(Me.cmdSave, "cmdSave")
        Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.UseVisualStyleBackColor = False
        '
        'cmdOpen
        '
        resources.ApplyResources(Me.cmdOpen, "cmdOpen")
        Me.cmdOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOpen.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cmdOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOpen.Name = "cmdOpen"
        Me.cmdOpen.UseVisualStyleBackColor = False
        '
        'cmdNew
        '
        resources.ApplyResources(Me.cmdNew, "cmdNew")
        Me.cmdNew.BackColor = System.Drawing.SystemColors.Control
        Me.cmdNew.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdNew.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNew.Name = "cmdNew"
        Me.cmdNew.UseVisualStyleBackColor = False
        '
        'zlbl_16
        '
        resources.ApplyResources(Me.zlbl_16, "zlbl_16")
        Me.zlbl_16.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_16.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_16.ForeColor = System.Drawing.Color.White
        Me.zlbl_16.Name = "zlbl_16"
        '
        'zlbl_17
        '
        resources.ApplyResources(Me.zlbl_17, "zlbl_17")
        Me.zlbl_17.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_17.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_17.ForeColor = System.Drawing.Color.White
        Me.zlbl_17.Name = "zlbl_17"
        '
        'zlb_18
        '
        resources.ApplyResources(Me.zlb_18, "zlb_18")
        Me.zlb_18.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlb_18.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlb_18.ForeColor = System.Drawing.Color.White
        Me.zlb_18.Name = "zlb_18"
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.GroupBox4.Controls.Add(Me.zlbl_18)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.cmdMassGeo)
        Me.GroupBox4.Controls.Add(Me.zlbl_17)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.zlb_18)
        Me.GroupBox4.Controls.Add(Me.cmdProperties)
        Me.GroupBox4.Controls.Add(Me.cmdSave)
        Me.GroupBox4.Controls.Add(Me.zlbl_9)
        Me.GroupBox4.Controls.Add(Me.zlbl_16)
        Me.GroupBox4.Controls.Add(Me.cmdGestPwr)
        Me.GroupBox4.Controls.Add(Me.cmdOpen)
        Me.GroupBox4.Controls.Add(Me.zlbl_10)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.cmdNew)
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Controls.Add(Me.cmdAct)
        Me.GroupBox4.Controls.Add(Me.cmdTherm)
        Me.GroupBox4.Controls.Add(Me.cmdBat)
        Me.GroupBox4.Controls.Add(Me.zlbl_7)
        Me.GroupBox4.Controls.Add(Me.cmdCells)
        Me.GroupBox4.Controls.Add(Me.zlbl_8)
        Me.GroupBox4.Controls.Add(Me.zlbl_11)
        Me.GroupBox4.Controls.Add(Me.cmdGestAtt)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.ForeColor = System.Drawing.Color.OldLace
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'zlbl_18
        '
        resources.ApplyResources(Me.zlbl_18, "zlbl_18")
        Me.zlbl_18.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.zlbl_18.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_18.ForeColor = System.Drawing.Color.White
        Me.zlbl_18.Name = "zlbl_18"
        '
        'frmMain
        '
        Me.AcceptButton = Me.cmdFwd
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(33, Byte), Integer))
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.fraSimulation)
        Me.Controls.Add(Me.fraDisplay)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.fraDisplay.ResumeLayout(False)
        Me.fraDisplay.PerformLayout()
        Me.fraSimulation.ResumeLayout(False)
        Me.fraSimulation.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents cmdActuactors As System.Windows.Forms.RadioButton
    Public WithEvents cmd As System.Windows.Forms.RadioButton
    Public WithEvents zlbl_8 As System.Windows.Forms.Label
    Public WithEvents zlbl_11 As System.Windows.Forms.Label
    Public WithEvents zlbl_10 As System.Windows.Forms.Label
    Public WithEvents zlbl_7 As System.Windows.Forms.Label
    Public WithEvents cmdGestAtt As System.Windows.Forms.RadioButton
    Public WithEvents cmdMassGeo As System.Windows.Forms.RadioButton
    Public WithEvents cmdProperties As System.Windows.Forms.RadioButton
    Public WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_15 As System.Windows.Forms.Label
    Public WithEvents zlbl_14 As System.Windows.Forms.Label
    Public WithEvents cmdCurvesAtt As System.Windows.Forms.RadioButton
    Public WithEvents cmdReportAtt As System.Windows.Forms.RadioButton
    Public WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_13 As System.Windows.Forms.Label
    Public WithEvents zlbl_12 As System.Windows.Forms.Label
    Public WithEvents cmdOrbit As System.Windows.Forms.RadioButton
    Public WithEvents cmdEnvir As System.Windows.Forms.RadioButton
    Public WithEvents fraDisplay As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_6 As System.Windows.Forms.Label
    Public WithEvents zlbl_5 As System.Windows.Forms.Label
    Public WithEvents zlbl_4 As System.Windows.Forms.Label
    Public WithEvents optView_3 As System.Windows.Forms.RadioButton
    Public WithEvents optView_0 As System.Windows.Forms.RadioButton
    Public WithEvents optView_1 As System.Windows.Forms.RadioButton
    Public WithEvents fraSimulation As System.Windows.Forms.GroupBox
    Friend WithEvents updwTimeStep As System.Windows.Forms.DomainUpDown
    Public WithEvents lblDate As System.Windows.Forms.Label
    Public WithEvents zlbl_1 As System.Windows.Forms.Label
    Friend WithEvents txt_date_debut As System.Windows.Forms.DateTimePicker
    Public WithEvents zlbl_3 As System.Windows.Forms.Label
    Public WithEvents zlbl_0 As System.Windows.Forms.Label
    Public WithEvents cmdOneFwd As System.Windows.Forms.Button
    Public WithEvents cmdFwd As System.Windows.Forms.Button
    Public WithEvents cmdOneBack As System.Windows.Forms.Button
    Public WithEvents cmdStop As System.Windows.Forms.Button
    Public WithEvents cmdPause As System.Windows.Forms.Button
    Public WithEvents cmdAct As System.Windows.Forms.RadioButton
    Public WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Public WithEvents zlb_16 As System.Windows.Forms.Label
    Public WithEvents Label4 As System.Windows.Forms.Label
    Public WithEvents Label5 As System.Windows.Forms.Label
    Public WithEvents cmdPowerDiagram As System.Windows.Forms.RadioButton
    Public WithEvents cmdCurvesPwr As System.Windows.Forms.RadioButton
    Public WithEvents cmdReportPwr As System.Windows.Forms.RadioButton
    Public WithEvents zlbl_9 As System.Windows.Forms.Label
    Public WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents Label2 As System.Windows.Forms.Label
    Public WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents cmdBat As System.Windows.Forms.RadioButton
    Public WithEvents cmdGestPwr As System.Windows.Forms.RadioButton
    Public WithEvents cmdTherm As System.Windows.Forms.RadioButton
    Public WithEvents cmdCells As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents cmdSave As System.Windows.Forms.RadioButton
    Public WithEvents cmdOpen As System.Windows.Forms.RadioButton
    Public WithEvents cmdNew As System.Windows.Forms.RadioButton
    Public WithEvents zlbl_16 As System.Windows.Forms.Label
    Public WithEvents zlbl_17 As System.Windows.Forms.Label
    Public WithEvents zlb_18 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_18 As System.Windows.Forms.Label
    Public WithEvents OrbitOpen As System.Windows.Forms.RadioButton
    Public WithEvents OrbitSave As System.Windows.Forms.RadioButton
#End Region
End Class