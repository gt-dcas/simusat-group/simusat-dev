Option Strict Off
Option Explicit On
Imports System.Math
Module basMath

    ' ---------------------------
    ' Vector calculation routines
    ' ---------------------------

    ''' <summary>
    ''' returns length of hypothenuse in triangle defined by 2 vectors
    ''' </summary>
    ''' <param name="a">vector defining triangle</param>
    ''' <param name="b">vector defining triangle</param>
    ''' <returns>length of hypothenuse</returns>
    ''' <remarks></remarks>
    Public Function vctDist(ByRef a As vector, ByRef b As vector) As Double
        Dim h1, h2 As Double
        h1 = vctNorm(a) '# get absolute heights
        h2 = vctNorm(b) '# then use the law of cosine
        vctDist = Sqrt(h1 ^ 2 + h2 ^ 2 - 2 * h1 * h2 * Cos(vctAngle(a, b)))
    End Function

    ''' <summary>
    ''' create a vector (x,y,z)
    ''' </summary>
    ''' <param name="X">coordinates of the vector</param>
    ''' <param name="Y">coordinates of the vector</param>
    ''' <param name="Z">coordinates of the vector</param>
    ''' <returns>return vector with x,y,z as coordinates</returns>
    ''' <remarks></remarks>
    Function vct(ByRef X As Double, ByRef Y As Double, ByRef Z As Double) As vector
        vct.X = X
        vct.Y = Y
        vct.Z = Z
    End Function

    ''' <summary>
    ''' calculate the cross product of the two given vectors
    ''' </summary>
    ''' <param name="a">vector to calcutate cross product of</param>
    ''' <param name="b">vector to calcutate cross product of</param>
    ''' <returns>return cross product a x b [vector]</returns>
    ''' <remarks></remarks>
    Function vctCross(ByRef a As vector, ByRef b As vector) As vector
        vctCross.X = (a.Y * b.Z) - (a.Z * b.Y)
        vctCross.Y = (a.Z * b.X) - (a.X * b.Z)
        vctCross.Z = (a.X * b.Y) - (a.Y * b.X)
    End Function

    ''' <summary>
    ''' calculate the norm of a vector
    ''' </summary>
    ''' <param name="vec">vector to calcutate norm of</param>
    ''' <returns>return norm of the vector</returns>
    ''' <remarks></remarks>
    Function vctNorm(ByRef vec As vector) As Double
        vctNorm = Sqrt(vec.X * vec.X + vec.Y * vec.Y + vec.Z * vec.Z)
    End Function

    ''' <summary>
    ''' calculate a scalar multiplication
    ''' </summary>
    ''' <param name="vec">vector to multiplicate</param>
    ''' <param name="k">scalar to multiplicate</param>
    ''' <returns>return scalar product k * vec [vector]</returns>
    ''' <remarks></remarks>
    Function vctMultScalar(ByRef vec As vector, ByRef k As Double) As vector
        vctMultScalar.X = vec.X * k
        vctMultScalar.Y = vec.Y * k
        vctMultScalar.Z = vec.Z * k
    End Function

    ''' <summary>
    ''' calculate the inverse of a vector
    ''' </summary>
    ''' <param name="vec">vector to inverse</param>
    ''' <returns>return inverse -vec [vector]</returns>
    ''' <remarks></remarks>
    Function vctInverse(ByRef vec As vector) As vector
        vctInverse = vctMultScalar(vec, -1)
    End Function

    ''' <summary>
    ''' calculate vector subtraction for two vectors
    ''' </summary>
    ''' <param name="a">minuend</param>
    ''' <param name="b">subtrahend</param>
    ''' <returns>return subtraction a-b [vector]</returns>
    ''' <remarks></remarks>
    Function vctSub(ByRef a As vector, ByRef b As vector) As vector
        vctSub.X = a.X - b.X
        vctSub.Y = a.Y - b.Y
        vctSub.Z = a.Z - b.Z
    End Function

    ''' <summary>
    ''' calculate vector addition for two vectors
    ''' </summary>
    ''' <param name="a">addends</param>
    ''' <param name="b">addends</param>
    ''' <returns>return sum a+b [vector]</returns>
    ''' <remarks></remarks>
    Function vctAdd(ByRef a As vector, ByRef b As vector) As vector
        vctAdd.X = a.X + b.X
        vctAdd.Y = a.Y + b.Y
        vctAdd.Z = a.Z + b.Z
    End Function

    ''' <summary>
    ''' calculate dot product of two vectors
    ''' </summary>
    ''' <param name="a">factors</param>
    ''' <param name="b">factors</param>
    ''' <returns>return dot product a o b</returns>
    ''' <remarks></remarks>
    Function vctDot(ByRef a As vector, ByRef b As vector) As Double
        vctDot = a.X * b.X + a.Y * b.Y + a.Z * b.Z
    End Function

    ''' <summary>
    ''' calcutate normalized vector for input vector
    ''' </summary>
    ''' <param name="vec">vector to be normalized</param>
    ''' <returns>return normalized vector vec/|vec| [vector]</returns>
    ''' <remarks></remarks>
    Function vctToNorm(ByRef vec As vector) As vector
        Static norm As Double
        norm = vctNorm(vec)
        vctToNorm.X = vec.X / norm
        vctToNorm.Y = vec.Y / norm
        vctToNorm.Z = vec.Z / norm
    End Function

    ''' <summary>
    ''' calcutate angle between two vectors
    ''' </summary>
    ''' <param name="vec1">vector which span the angle</param>
    ''' <param name="vec2">vector which span the angle</param>
    ''' <returns>return angle between vec1 and vec2</returns>
    ''' <remarks>
    ''' todo check whether this function returns [rad] or [deg]
    ''' </remarks>
    Function vctAngle(ByRef vec1 As vector, ByRef vec2 As vector) As Double
        vctAngle = Math.Acos(vctDot(vec1, vec2) / (vctNorm(vec1) * vctNorm(vec2)))
    End Function

    ' -----------------
    ' Various functions
    ' -----------------

    ''' <summary>
    ''' calculate arcus sine
    ''' </summary>
    ''' <param name="X">value of which arcus sine should be calculated</param>
    ''' <returns>return arcus sine ( x )</returns>
    ''' <remarks>
    ''' todo check whether this function returns and takes [rad] or [deg]
    ''' </remarks>
    Public Function ArcSin(ByRef X As Double) As Double
        If X < -1 Then X = -1
        If Abs(X - 1) < 0.00001 Then ArcSin = pi / 2 : Exit Function
        If X = -1 Then ArcSin = -pi / 2 : Exit Function
        ArcSin = Atan(X / Sqrt(-X * X + 1))
    End Function

    ''' <summary>
    ''' This function returns the places after the decimal point. E.g. Frac(4.123) == 0.123
    ''' </summary>
    ''' <param name="X">value of which arcus sine should be calculated</param>
    ''' <returns>return decimal places of x</returns>
    ''' <remarks></remarks>
    Function Frac(ByRef X As Double) As Double
        Frac = X - Int(X)
    End Function

    ''' <summary>
    ''' calcule l'arctangente pour un angle compris entre -pi et pi
    ''' </summary>
    ''' <param name="sinx"></param>
    ''' <param name="cosx"></param>
    ''' <returns></returns>
    ''' <remarks>
    ''' todo what exactly does this function do?
    ''' </remarks>
    Function Atan2(ByRef sinx As Double, ByRef cosx As Double) As Double
        If cosx <> 0 Then
            If cosx >= 0 Then
                Atan2 = Atan(sinx / cosx)
            Else
                If sinx >= 0 Then
                    Atan2 = Atan(sinx / cosx) + pi
                Else
                    Atan2 = Atan(sinx / cosx) - pi
                End If
            End If
        Else
            If sinx > 0 Then
                Atan2 = 0.5 * pi
            Else
                Atan2 = -0.5 * pi
            End If
        End If
    End Function
    Public Function Actan(ByVal sinx As Double, ByVal cosx As Double) As Double
        '
        ' Arctangente entre 0 et 2 pi
        If cosx = 0 Then
            If sinx > 0 Then
                Actan = pi / 2
            Else
                Actan = 3 * pi / 2
            End If
        ElseIf cosx > 0 Then
            Actan = Math.Atan(sinx / cosx)
        Else
            Actan = pi + Math.Atan(sinx / cosx)
        End If
        ''
    End Function

    ''' <summary>
    ''' calculate third power
    ''' </summary>
    ''' <param name="X">value which should be squared</param>
    ''' <returns>return x^3</returns>
    ''' <remarks>
    ''' todo use Pow(x,3) instead of this here!!
    ''' </remarks>
    Function Cube(ByRef X As Double) As Double
        Cube = X * X * X
    End Function

    ''' <summary>
    ''' calculate modulo of two doubles
    ''' </summary>
    ''' <param name="X">value to be taken into modulus</param>
    ''' <param name="Y">value to be taken into modulus</param>
    ''' <returns>return x mod y</returns>
    ''' <remarks></remarks>
    Function Modulo(ByRef X As Double, ByRef Y As Double) As Double
        Modulo = X - Int(X / Y) * Y
        If Modulo < 0 Then
            Modulo = Modulo + Y
        End If
    End Function

    ''' <summary>
    ''' calculate x modulo (2 PI), x being a double
    ''' </summary>
    ''' <param name="X">values to be taken into modulus</param>
    ''' <returns>return x mod 2PI</returns>
    ''' <remarks></remarks>
    Function Mod2pi(ByRef X As Double) As Double
        Mod2pi = Modulo(X, 2 * pi)
    End Function

    ''' <summary>
    ''' calculate progress of x in dependency of frame y
    '''
    ''' This function calculates the current progress of x (= current state)
    ''' depending on y (= whole e.g. time-frame) and returns this value in percent. \n
    ''' Example: progress(5,10) = 50; progress(1,20) = 5
    ''' </summary>
    ''' <param name="X">current state</param>
    ''' <param name="Y">whole frame</param>
    ''' <returns>return An integer determining the progress x/y in %</returns>
    ''' <remarks>
    ''' todo function is used with doubles as well as with ints. please stick to one type (double). => OK
    ''' </remarks>
    Public Function progress(ByRef X As Double, ByRef Y As Double) As Short
        If Y < 0.00000001 Then
            progress = 100
        Else
            progress = Int((X / Y * 100) + 0.5)
            If progress > 100 Then progress = 100
            If progress < 0 Then progress = 0
        End If
    End Function

    ''' <summary>
    ''' Check if X and Y are equal, with a certain precision
    ''' </summary>
    ''' <param name="X"></param>
    ''' <param name="Y"></param>
    ''' <param name="delta">precision</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function almostEqual(ByRef X As Double, ByRef Y As Double, ByRef delta As Double) As Boolean
        If Abs(X - Y) < delta Then
            almostEqual = True
        Else
            almostEqual = False
        End If
    End Function

    ' ---------------------------
    ' Matrix calculation routines
    ' ---------------------------

    ''' <summary>
    ''' Calculate the rotation matrix 3x3 for a given angle around a given axis
    ''' </summary>
    ''' <param name="axis">Rotation axis : "X", "Y" or "Z"</param>
    ''' <param name="angle">Rotation angle : in radians</param>
    ''' <returns>Rotation matrix</returns>
    ''' <remarks></remarks>
    Public Function RotationMatrix(ByRef axis As String, ByRef angle As Double) As Double(,)
        '
        Dim Temp(2, 2) As Double
        '
        Select Case axis
            Case "X"
                Temp(0, 0) = 1 : Temp(0, 1) = 0 : Temp(0, 2) = 0
                Temp(1, 0) = 0 : Temp(1, 1) = Cos(angle) : Temp(1, 2) = -Sin(angle)
                Temp(2, 0) = 0 : Temp(2, 1) = Sin(angle) : Temp(2, 2) = Cos(angle)
            Case "Y"
                Temp(0, 0) = Cos(angle) : Temp(0, 1) = 0 : Temp(0, 2) = Sin(angle)
                Temp(1, 0) = 0 : Temp(1, 1) = 1 : Temp(1, 2) = 0
                Temp(2, 0) = -Sin(angle) : Temp(2, 1) = 0 : Temp(2, 2) = Cos(angle)
            Case "Z"
                Temp(0, 0) = Cos(angle) : Temp(0, 1) = -Sin(angle) : Temp(0, 2) = 0
                Temp(1, 0) = Sin(angle) : Temp(1, 1) = Cos(angle) : Temp(1, 2) = 0
                Temp(2, 0) = 0 : Temp(2, 1) = 0 : Temp(2, 2) = 1
        End Select
        '
        RotationMatrix = Temp
        '
    End Function

    ''' <summary>
    ''' Calculate the product m1 x m2 between two matrix 3x3
    ''' </summary>
    ''' <param name="m1"></param>
    ''' <param name="m2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function MatrixProd(ByRef m1 As Double(,), ByRef m2 As Double(,)) As Double(,)
        '
        Dim Temp(2, 2) As Double
        Dim i, j As Integer
        '
        For i = 0 To 2
            For j = 0 To 2
                Temp(i, j) = m1(i, 0) * m2(0, j) + m1(i, 1) * m2(1, j) + m1(i, 2) * m2(2, j)
            Next
        Next
        '
        MatrixProd = Temp
        '
    End Function

    ''' <summary>
    ''' Calculate the sum of two matrix 3x3
    ''' </summary>
    ''' <param name="m1"></param>
    ''' <param name="m2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function MatrixAdd(ByRef m1 As Double(,), ByRef m2 As Double(,)) As Double(,)
        '
        Dim Temp(2, 2) As Double
        Dim i, j As Integer
        '
        For i = 0 To 2
            For j = 0 To 2
                Temp(i, j) = m1(i, j) + m2(i, j)
            Next
        Next
        '
        MatrixAdd = Temp
        '
    End Function

    ''' <summary>
    ''' Calculate the multiplication of a matrix 3x3 by a scalar
    ''' </summary>
    ''' <param name="m"></param>
    ''' <param name="k"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function MatrixMultScalar(ByRef m As Double(,), ByRef k As Double) As Double(,)
        '
        Dim Temp(2, 2) As Double
        Dim i, j As Integer
        '
        For i = 0 To 2
            For j = 0 To 2
                Temp(i, j) = k * m(i, j)
            Next
        Next
        '
        MatrixMultScalar = Temp
        '
    End Function

    ''' <summary>
    ''' Application of Huygens Theorem to deplace an inertia matrix from a point to another
    ''' </summary>
    ''' <param name="m">Inertia matrix expressed at start point</param>
    ''' <param name="mass">Mass</param>
    ''' <param name="start">Start point</param>
    ''' <param name="dest">Destination point</param>
    ''' <returns>Inertia matrix expressed at destination point</returns>
    ''' <remarks></remarks>
    Public Function Huygens(ByRef m As Double(,), ByRef mass As Double, ByRef start As vector, ByRef dest As vector) As Double(,)
        '
        Dim TempMatrix(2, 2) As Double
        Dim a As Double = dest.X - start.X
        Dim b As Double = dest.Y - start.Y
        Dim c As Double = dest.Z - start.Z
        TempMatrix(0, 0) = b ^ 2 + c ^ 2
        TempMatrix(0, 1) = -a * b
        TempMatrix(0, 2) = -a * c
        TempMatrix(1, 0) = -a * b
        TempMatrix(1, 1) = a ^ 2 + c ^ 2
        TempMatrix(1, 2) = -b * c
        TempMatrix(2, 0) = -a * c
        TempMatrix(2, 1) = -b * c
        TempMatrix(2, 2) = a ^ 2 + b ^ 2
        Huygens = MatrixAdd(m, MatrixMultScalar(TempMatrix, mass))
        ''
    End Function

    ''' <summary>
    ''' Calculate the new matrix expressed in a new basis that is a rotation around 1 axis of the former one
    ''' </summary>
    ''' <param name="m">Matrix expressed in the former basis</param>
    ''' <param name="axis">Rotation axis : "X", "Y" or "Z"</param>
    ''' <param name="angle">Rotation angle from the former basis to the new one : in radians</param>
    ''' <returns>Matrix expressed in the new basis</returns>
    ''' <remarks></remarks>
    Public Function MatrixNewBasis_FromRotation(ByRef m As Double(,), ByRef axis As String, ByRef angle As Double) As Double(,)
        '
        Dim ChangeOfBasis As Double(,) = RotationMatrix(axis, angle)
        Dim ChangeOfBasisInv As Double(,) = RotationMatrix(axis, -angle)
        MatrixNewBasis_FromRotation = MatrixProd(MatrixProd(ChangeOfBasisInv, m), ChangeOfBasis)
        ''
    End Function

End Module