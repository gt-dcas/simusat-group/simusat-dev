<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmFirst
#Region "Code g�n�r� par le Concepteur Windows Form "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Cet appel est requis par le Concepteur Windows Form.
		InitializeComponent()
	End Sub
	'Form remplace la m�thode Dispose pour nettoyer la liste des composants.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requise par le Concepteur Windows Form
	Private components As System.ComponentModel.IContainer
    Public WithEvents lblLoading As System.Windows.Forms.Label
	Public WithEvents zimg As System.Windows.Forms.PictureBox
	'REMARQUE�: la proc�dure suivante est requise par le Concepteur Windows Form
	'Elle peut �tre modifi�e � l'aide du Concepteur Windows Form.
	'Ne la modifiez pas � l'aide de l'�diteur de code.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFirst))
        Me.lblLoading = New System.Windows.Forms.Label
        Me.zimg = New System.Windows.Forms.PictureBox
        Me.ProgressBar = New System.Windows.Forms.ProgressBar
        Me.zlbl_1 = New System.Windows.Forms.Label
        CType(Me.zimg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblLoading
        '
        Me.lblLoading.BackColor = System.Drawing.SystemColors.Window
        Me.lblLoading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLoading.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLoading.Location = New System.Drawing.Point(36, 345)
        Me.lblLoading.Name = "lblLoading"
        Me.lblLoading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLoading.Size = New System.Drawing.Size(139, 19)
        Me.lblLoading.TabIndex = 1
        Me.lblLoading.Text = "Loading in progress"
        '
        'zimg
        '
        Me.zimg.Cursor = System.Windows.Forms.Cursors.Default
        Me.zimg.Image = CType(resources.GetObject("zimg.Image"), System.Drawing.Image)
        Me.zimg.Location = New System.Drawing.Point(0, 0)
        Me.zimg.Name = "zimg"
        Me.zimg.Size = New System.Drawing.Size(545, 381)
        Me.zimg.TabIndex = 2
        Me.zimg.TabStop = False
        '
        'ProgressBar
        '
        Me.ProgressBar.ForeColor = System.Drawing.Color.Navy
        Me.ProgressBar.Location = New System.Drawing.Point(180, 349)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(326, 10)
        Me.ProgressBar.TabIndex = 3
        '
        'zlbl_1
        '
        Me.zlbl_1.BackColor = System.Drawing.SystemColors.Window
        Me.zlbl_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_1.Location = New System.Drawing.Point(307, 223)
        Me.zlbl_1.Name = "zlbl_1"
        Me.zlbl_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_1.Size = New System.Drawing.Size(214, 19)
        Me.zlbl_1.TabIndex = 1
        Me.zlbl_1.Text = "Attitude control system"
        '
        'frmFirst
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(544, 381)
        Me.ControlBox = False
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.zlbl_1)
        Me.Controls.Add(Me.lblLoading)
        Me.Controls.Add(Me.zimg)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFirst"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SATORB"
        CType(Me.zimg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Public WithEvents zlbl_1 As System.Windows.Forms.Label
#End Region 
End Class