Option Strict Off
Option Explicit On

Imports System.IO
Imports Tao.OpenGl
Imports Tao.Platform.Windows
Imports System.Math
Imports System.Linq
Imports System.Threading

''' <summary>
''' ***************************************************************************
''' *  Program update in VB.NET - 17 june 2011                                *
''' *  Developpers : Benjamin and Florent - DUT computer science Internship   *
''' ***************************************************************************
''' </summary>
''' <remarks>Form main</remarks>
Friend Class frmMain
    Inherits System.Windows.Forms.Form

    Sub Main() 'CAMBIAR DE NOMBRE
        Dim args() As String = System.Environment.GetCommandLineArgs()

        For i As Integer = 0 To args.Length - 1
            Console.WriteLine("Arg: " & i & " is " & args(i))
        Next

        Console.ReadKey()
    End Sub

#Region "Buttons"


    Private Sub cmdNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNew.Click
        '

        Dim answer As Short
        Dim FileName As String
        '
        If blnIsNewPrj Then
            answer = MsgBox("You will loose your current data file." & vbCrLf & "Do you want to save it first?", MsgBoxStyle.YesNoCancel Or MsgBoxStyle.Exclamation, "Warning!")
            If answer = MsgBoxResult.Yes Then
                dlgMainSave.FileName = Satellite.SatName
                If dlgMainSave.ShowDialog = Windows.Forms.DialogResult.OK Then
                    save_att_and_pwr_conf(dlgMainSave.FileName)
                    Satellite.SatName = Path.GetFileNameWithoutExtension(dlgMainSave.FileName)
                End If
            ElseIf answer = MsgBoxResult.Cancel Then
                Exit Sub
            End If
        End If
        '
        FileName = My.Application.Info.DirectoryPath & "\resources\blank_file.sc"
        Me.dlgMainOpen.FilterIndex = 1 'This line avoids an exception when opening a new file. The exception is due to the If sentence in the load_att_and_pwr_conf function to handle retrocompatibility. The exception ocurred when a ancient .att file was opened before
        '
        'Give name to the new project
        Satellite.SatName = InputBox("Name the new satellite:", "New Satellite", "New sat")
        If Satellite.SatName = "" Then
            Satellite.SatName = "New_Sat"
            Exit Sub 'If we press on cancel, the data is not reset
        End If
        '
        load_att_and_pwr_conf(FileName)
        'MsgBox("You successfully created a new file.")
        '
        selectTimeStepIndex()
        basOrbitography.update_StartDate()
        dblSimulationTime = dblSimulationStart
        basOrbitography.update_CurrentDate(dblSimulationTime)
        '
        frmAttConf.update_attitude_conf()
        frmAttConf.update_attitude_data_for_frmMain()
        '
        blnIsNewPrj = True
        ''
    End Sub
    Private Sub cmdOpen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOpen.Click
        '
        Dim answer As Short
        '
        If blnIsNewPrj Then
            answer = MsgBox("You will loose your current data file." & vbCrLf & "Do you want to save it first?", MsgBoxStyle.YesNoCancel Or MsgBoxStyle.Exclamation, "Warning!")
            If answer = MsgBoxResult.Yes Then
                dlgMainSave.FileName = Satellite.SatName
                If dlgMainSave.ShowDialog = Windows.Forms.DialogResult.OK Then
                    save_att_and_pwr_conf(dlgMainSave.FileName)
                    Satellite.SatName = Path.GetFileNameWithoutExtension(dlgMainSave.FileName)
                End If
            ElseIf answer = MsgBoxResult.Cancel Then
                Exit Sub
            End If
        End If
        '
        If dlgMainOpen.ShowDialog = Windows.Forms.DialogResult.Cancel Then Exit Sub
        load_att_and_pwr_conf(dlgMainOpen.FileName)
        Satellite.SatName = Path.GetFileNameWithoutExtension(dlgMainOpen.FileName)
        '
        selectTimeStepIndex()
        basOrbitography.update_StartDate()
        dblSimulationTime = dblSimulationStart
        basOrbitography.update_CurrentDate(dblSimulationTime)
        frmOrbit.env_update_texts()
        '
        frmAttConf.update_attitude_conf()
        frmAttConf.update_attitude_data_for_frmMain()
        '
        blnIsNewPrj = False
        ''
    End Sub
    Private Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        '
        dlgMainSave.FileName = Satellite.SatName
        If dlgMainSave.ShowDialog = Windows.Forms.DialogResult.OK Then
            save_att_and_pwr_conf(dlgMainSave.FileName)
            Satellite.SatName = Path.GetFileNameWithoutExtension(dlgMainSave.FileName)
            blnIsNewPrj = False
        End If
        ''
    End Sub

    Private Sub cmdOneBack_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOneBack.Click
        'Enable Timer
        simloop = False
        'Enable Play button to reuse it just then
        If Not cmdFwd.Enabled Then
            cmdFwd.Enabled = True
        End If
        dblSimulationTime = dblSimulationTime - dblTimeStep
        Sim_update()
    End Sub
    Private Sub cmdStop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStop.Click

        cmdFwd.Enabled = True
        '
        simloop = False
        '
        initialize_power()  ' Power initialisation
        dblSimulationTime = dblSimulationStart
        Sim_update()

        'Desable message for the warning
        frmPowerDiagram.Label4.Enabled = False
        frmPowerDiagram.Label4.Visible = False
    End Sub
    Public Sub cmdPause_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdPause.Click
        cmdFwd.Enabled = True
        simloop = False
    End Sub
    Public Sub cmdFwd_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFwd.Click
        '
        'In cas none of the orbit are selected, set the circular one by default
        If Not frmOrbit.optType_Orbit_0.Checked And Not frmOrbit.optType_Orbit_1.Checked And Not frmOrbit.optType_Orbit_2.Checked And Not frmOrbit.optType_Orbit_3.Checked And Not frmOrbit.optType_Orbit_4.Checked And Not frmOrbit.optType_Orbit_5.Checked And Not frmOrbit.optType_Orbit_6.Checked And Not frmOrbit.optType_Orbit_7.Checked Then
            frmOrbit.optType_Orbit_7.Checked = True
            Sim_update()
        End If
        '
        ' Message in case there is no solar panel added
        If PanelsNumber = 0 Then
            MsgBox("WARNING : No solar panel added")
        End If

        cmdFwd.Enabled = False
        TickLast = GetTickCount
        If Not simloop Then
            simloop = True
            simulationloop()
        Else
            simulationloop() 'add to make the orbit run even if the display already has been open
        End If

        ''
    End Sub
    Private Sub cmdOneFwd_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOneFwd.Click
        'Enable Timer
        simloop = False
        dblSimulationTime = dblSimulationTime + dblTimeStep
        Sim_update()
        'Enable Play button to reuse it just then
        If Not cmdFwd.Enabled Then
            cmdFwd.Enabled = True
        End If
    End Sub

    Private Sub txt_date_debut_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_date_debut.Leave
        '
        Dim date_temp As udtDate
        '
        ' Calcul du jour julien de d�but de simulation
        With date_temp
            .year = Year(txt_date_debut.Value)
            .month = Month(txt_date_debut.Value)
            .day = Microsoft.VisualBasic.DateAndTime.Day(txt_date_debut.Value)
            .hour = Hour(txt_date_debut.Value)
            .minute = Minute(txt_date_debut.Value)
            .second = Second(txt_date_debut.Value)
        End With
        dblSimulationStart = convert_Gregorian_Julian(date_temp)
        '
        dblSimulationTime = dblSimulationStart
        update_CurrentDate((dblSimulationTime))
        ''
    End Sub

    Private Sub updwTimeStep_SelectedItemChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles updwTimeStep.SelectedItemChanged
        '
        Dim x$
        x$ = updwTimeStep.Text
        '
        If x = "1 day" Then
            dblTimeStep = 1
        Else
            x$ = Microsoft.VisualBasic.Left(x$, Len(x$) - 2)
            dblTimeStep = CDbl(x$) / secday
        End If
        ''
    End Sub
    ' Dim OrbitDisplayed As Boolean = False
    Private Sub cmdOrbit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOrbit.Click
        frmOrbit.tabOrbit.SelectedIndex = 0
        'If OrbitDisplayed = False Then / Test pour la Molnyia
        frmOrbit.Show()
        frmOrbit.Focus()
        'OrbitDisplayed = True
        'Else
        'frmOrbit.Hide()
        'OrbitDisplayed = False
        'End If

    End Sub
    Private Sub cmdEnvir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEnvir.Click
        frmOrbit.tabOrbit.SelectedIndex = 1
        frmOrbit.Show()
        frmOrbit.Focus()
    End Sub

    Private Sub optView_0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optView_0.Click
        Sim_update()
        resizeGLWindow(CInt(frmDisplay.picMapView.Width), CInt(frmDisplay.picMapView.Height))
        Sim_update()
        basDeclaration.displayOpened = True
        frmDisplay.Show()
        View_Change()
        frmDisplay.Focus()
        Sim_update()
    End Sub
    Private Sub optView_1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optView_1.Click
        Sim_update()
        resizeGLWindow(CInt(frmDisplay.picMapView.Width), CInt(frmDisplay.picMapView.Height))
        Sim_update()
        basDeclaration.displayOpened = True
        frmDisplay.Show()
        View_Change()
        frmDisplay.Focus()
    End Sub
    Private Sub optView_3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optView_3.Click
        Sim_update()
        resizeGLWindow(CInt(frmDisplay.picMapView.Width), CInt(frmDisplay.picMapView.Height))
        Sim_update()
        basDeclaration.displayOpened = True
        frmDisplay.Show()
        View_Change()
        frmDisplay.Focus()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        frmAbout.Show()
        frmAbout.Focus()
    End Sub

#Region "Attitude Control Sub-System"

    Private Sub cmdMassGeo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdMassGeo.Click
        frmAttConf.tabPower.SelectedIndex = 0
        frmAttConf.Show()
        frmAttConf.Focus()
    End Sub
    Private Sub cmdProperties_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdProperties.Click
        frmAttConf.tabPower.SelectedIndex = 1
        frmAttConf.Show()
        frmAttConf.Focus()
    End Sub
    Private Sub cmdActuactors_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAct.Click
        frmAttConf.tabPower.SelectedIndex = 2
        frmAttConf.Show()
        frmAttConf.Focus()
    End Sub
    Private Sub cmdGestAtt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGestAtt.Click
        frmAttConf.tabPower.SelectedIndex = 3
        frmAttConf.Show()
        frmAttConf.Focus()
    End Sub

    Private Sub cmdCurvesAtt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCurvesAtt.Click
        frmGraphAtt.Show()
        frmGraphAtt.Focus()
    End Sub
    Private Sub cmdReportAtt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReportAtt.Click
        frmReportAtt.Show()
        frmReportAtt.Focus()
    End Sub

#End Region

#Region "Energy Sub-System"

    Private Sub cmdCells_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCells.Click
        frmPowerConf.tabPower.SelectedIndex = 0
        frmPowerConf.Show()
        frmPowerConf.Focus()
    End Sub
    Private Sub cmdTherm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTherm.Click
        frmPowerConf.tabPower.SelectedIndex = 1
        frmPowerConf.Show()
        frmPowerConf.Focus()
    End Sub
    Private Sub cmdBat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBat.Click
        frmPowerConf.tabPower.SelectedIndex = 2
        frmPowerConf.Show()
        frmPowerConf.Focus()
    End Sub
    Private Sub cmdGestPwr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGestPwr.Click
        frmPowerConf.tabPower.SelectedIndex = 3
        frmPowerConf.Show()
        frmPowerConf.Focus()
    End Sub

    Private Sub cmdCurvesPwr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCurvesPwr.Click
        frmGraphPwr.Show()
        frmGraphPwr.Focus()
    End Sub
    Private Sub cmdReportPwr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReportPwr.Click
        frmReportPwr.Show()
        frmReportPwr.Focus()
    End Sub
    Private Sub cmdPowerDiagram_Click(sender As Object, e As EventArgs) Handles cmdPowerDiagram.Click
        'Desable message for the warning
        frmPowerDiagram.Label4.Enabled = False
        frmPowerDiagram.Label4.Visible = False

        frmPowerDiagram.Show()
        frmPowerDiagram.Focus()

    End Sub

#End Region

#End Region

    ''' <summary>
    ''' Closing frmFirst
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmMain_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        frmFirst.Hide()
    End Sub

    ''' <summary>
    ''' Updating
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    ''' 
    Private Sub frmMain_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.GotFocus
        Sim_update()
    End Sub

    ''' <summary>
    ''' Load the form Main
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Public Sub frmMain_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        '
        Dim i As Integer
        Dim date_temp As udtDate
        '        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        '

        gl_initialize() 'on initialise une seule fois
        '
        ' -----------------
        ' Loading star file
        ' -----------------
        readStars("Data\Stars\star.txt")
        '
        ' ------------------------------------
        ' Set defaults for satellite and orbit
        ' ------------------------------------
        Satellite = New CSatellite
        '
        ' Set default
        Call Satellite.construct("New_Sat", 65535, 0, 0, False)
        With Satellite
            .satBodyHeight = 1
            .satBodyWidth = 1
            .primaryAxis = "-z"
            .primaryDir = "nadir"
            .secondaryAxis = "x"
            .secondaryDir = "velocity"
        End With
        '
        With SatOrbit0
            .SemiMajor_Axis = 8000
            .Eccentricity = 0
            .Inclination = 0
            .Argument_Perigee = 0
            .RAAN = 0
            .Mean_Anomaly = 0
            .epoch = 2457754.5 ' january 1st 2017 0h
            .dec1 = 0
            .dec2 = 0
            .bstar = 0
        End With
        '
        SatOrbit = SatOrbit0
        '
        dblTimeStep = 60 / secday
        updwTimeStep.SelectedIndex = 6
        '
        dblSimulationStart = SatOrbit0.epoch
        dblSimulationTime = dblSimulationStart
        convert_Julian_Gregorian(dblSimulationStart, date_temp)
        With date_temp
            txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        '
        load_thermal_models()
        '
        frmOrbit.Update_OrbitWindow()
        '
        ' Set attitude sphere
        attsph = New CAttitudeSphere
        LoadAttSphere()
        '
        ' --------------------------------------------
        ' Default values (Attitude Control Sub-System)
        ' --------------------------------------------
        load_default_att_values()
        '
        ' Damper properties
        zd(0) = 0 : zd(1) = 0 : zd(2) = 0
        zdp(0) = 0 : zdp(1) = 0 : zdp(2) = 0
        zdpp(0) = 0 : zdpp(1) = 0 : zdpp(2) = 0
        '
        ' Wheels properties
        OMp(0) = 0 : OMp(1) = 0 : OMp(2) = 0
        OMin(0) = 0 : OMin(1) = 0 : OMin(2) = 0
        OMin0(0) = 0 : OMin0(1) = 0 : OMin0(1) = 0
        '
        ' Vectors normal to the six surfaces of the satellite
        ' 0 : face +x
        ' 1 : face +y
        ' 2 : face +z
        ' 3 : face -x
        ' 4 : face -y
        ' 5 : face -z
        For i = 0 To 5
            vect_face(i).X = 0 : vect_face(i).Y = 0 : vect_face(i).Z = 0
        Next
        vect_face(0).X = 1 : vect_face(1).Y = 1 : vect_face(2).Z = 1
        vect_face(3).X = -1 : vect_face(4).Y = -1 : vect_face(5).Z = -1
        '
        ' Initialisation of the inertial vector and the appropriate start-vector
        For i = 0 To 5
            yi0(i) = 0
            yi(i) = 0
        Next
        '
        ' Magnetic dipole of the satellite
        dipsat_control.X = 1 : dipsat_control.Y = 1 : dipsat_control.Z = 1
        '
        ' Gas jets
        jet_control(0) = 1 : jet_control(1) = 1 : jet_control(2) = 1
        '
        ' ----------------------------------
        ' Default values (Energy Sub-System)
        ' ----------------------------------
        '
        ' Cells performance

        cell_type = 0
        cell_isc = 0.53
        'Console.WriteLine("ISC " + cell_isc)
        cell_imp = 0.5
        cell_voc = 1
        cell_vmp = 0.86
        cell_disc = 0.0003
        cell_dimp = 0.00025
        cell_dvoc = -0.002
        cell_dvmp = -0.002
        cell_radisc = 1
        cell_radimp = 1
        cell_radvoc = 1
        cell_radvmp = 1
        cell_ns = 15
        cell_surf = 16
        cell_tref = 28
        cell_tetad = 50
        cell_tetam = 75
        '
        ' Operation parameters and distribution system performance
        pdem(0) = 50    ' 0 Sunshine 1 Eclipse
        diss(0) = 15
        pdem(1) = 50
        diss(1) = 15
        rendgs = 90
        rendbat = 90
        '
        ' Variables related to batteries system with default values
        cnom = 10
        bat_ns = 10
        bat_np = 1
        bat_type = 0
        '
        ' Operation parameters
        chvmax = 1.5
        ichmax = 100
        kcharge = 1.05
        chini = 100
        bat_courant_entretien = 0.001
        '
        ' Charge behaviour parameters
        ch0(0) = 0 : ch0(1) = 0 : ch0(2) = 0 : ch0(3) = 1.3 : ch0(4) = 0 : ch0(5) = 0
        ch1(0) = 15 : ch1(1) = 0.1 : ch1(2) = -5 : ch1(3) = 1.4 : ch1(4) = -0.001 : ch1(5) = 0.01
        ch2(0) = 65 : ch2(1) = 0.5 : ch2(2) = -5 : ch2(3) = 1.41 : ch2(4) = -0.001 : ch2(5) = 0.01
        och(0) = 120 : och(1) = 0.1 : och(2) = -5 : och(3) = 1.59 : och(4) = -0.002 : och(5) = 0.02
        '
        ' Discharge behaviour parameters
        de0(0) = 0 : de0(1) = 0 : de0(2) = 0 : de0(3) = 1.3 : de0(4) = 0 : de0(5) = 0
        de1(0) = 15 : de1(1) = 0.1 : de1(2) = -5 : de1(3) = 1.21 : de1(4) = 0.001 : de1(5) = -0.01
        de2(0) = 95 : de2(1) = 0.1 : de2(2) = -2 : de2(3) = 1.2 : de2(4) = 0.001 : de2(5) = -0.01
        de100(0) = 100 : de100(1) = 0 : de100(2) = 0 : de100(3) = 1.0# : de100(4) = 0 : de100(5) = 0
        cmax_cnom = 1.2
        c10_cnom = 0.6
        tbatmin = -50
        cmin100 = 30
        tbat99 = 50
        '
        ' Self-discharge behaviour parameters
        tc0 = 100
        tc50 = 15
        '
        ' -------------------------
        ' Init values for radiation
        ' -------------------------
        'Thermal data
        BodySpecHeat = 10000
        For i = 0 To 5
            BodyAlf(i) = 0.5
            BodyEps(i) = 0.5
        Next
        'Environment
        SunPow = 3.8E+26
        For i = 0 To 11
            Albedo_month(i) = 0.38
            IR_month(i) = 237
        Next i
        '
        ' Min/max values for the data range of each variable
        load_data_range()
        '
        ' All the Initializing is done
        Me.Show()
        cmdStop_Click(cmdStop, New System.EventArgs())
        View_Change() ' Select 2D view 
        blnSimulationInitialized = True
        displayOpened = False
        '
        selectTimeStepIndex()
        '
        'basOrbitography.update_StartDate()
        'basOrbitography.update_CurrentDate(dblSimulationTime)
        '
        ' Sim_update()
        ''
    End Sub

    ''' <summary>
    ''' Define shortcuts Open / Save / Attitude Control / Energy
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmMain_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyData = (Keys.Control Or Keys.O) Then
            cmdOpen_Click(cmdOpen, New System.EventArgs)
            e.SuppressKeyPress = True ' Pas besoin que l'�v�nement KeyPress le g�re on vient de le faire
        ElseIf e.KeyData = (Keys.Control Or Keys.S) Then
            cmdSave_Click(cmdSave, New System.EventArgs)
            e.SuppressKeyPress = True ' Pas besoin que l'�v�nement KeyPress le g�re on vient de le faire
        ElseIf e.KeyData = (Keys.Control Or Keys.A) Then
            cmdMassGeo_Click(cmdMassGeo, New System.EventArgs)
            e.SuppressKeyPress = True ' Pas besoin que l'�v�nement KeyPress le g�re on vient de le faire
        ElseIf e.KeyData = (Keys.Control Or Keys.E) Then
            cmdCells_Click(cmdCells, New System.EventArgs)
            e.SuppressKeyPress = True ' Pas besoin que l'�v�nement KeyPress le g�re on vient de le faire
        End If
    End Sub

    '

    ''' <summary>
    ''' Load the form AttitudeSphereSettings
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadAttSphere()

        Dim i As Short
        Dim sender As System.Object = 0
        Dim e As System.EventArgs = Nothing


        ' Sphere Attitude Settings
        With frmDisplay
            ' allowing everything for the axis of the principal and optimized direction
            For i = 0 To attsph.getAxisUbound
                .cmbPrinAxis.Items.Add(attsph.getAxis(i))
                .cmbOptAxis.Items.Add(attsph.getAxis(i))
            Next i
            ' allowing everything for the orientation of the principle and optimized direction
            For i = 0 To attsph.getOrientationUbound
                .cmbPrinOrient.Items.Add(attsph.getOrientation(i))
                .cmbOptOrient.Items.Add(attsph.getOrientation(i))
            Next i

            'set default values for the sphere attitude
            attsph.prinAxis = attsph.getAxis(5)
            ' .cmbPrinAxis_TextChanged(sender, e)
            attsph.prinOrient = attsph.getOrientation(0)
            '.cmbPrinOrient_TextChanged(sender, e)

            attsph.optAxis = attsph.getAxis(0)
            '.cmbOptAxis_TextChanged(sender, e)
            attsph.optOrient = attsph.getOrientation(3)
            '.cmbOptOrient_TextChanged(sender, e)

            'Update entries of the attitude comboboxes
            .cmbPrinAxis_SelectedIndexChanged(sender, e)
            .cmbPrinOrient_SelectedIndexChanged(sender, e)
            .cmbOptAxis_SelectedIndexChanged(sender, e)
            .cmbOptOrient_SelectedIndexChanged(sender, e)
        End With
        ''
    End Sub

    ''' <summary>
    ''' Load star file
    ''' </summary>
    ''' <param name="sPath">Path containing the star file</param>
    ''' <remarks></remarks>
    Public Sub readStars(ByVal sPath As String)

        Dim compteur_stars As Integer = 0
        Dim streamStars As New System.IO.StreamReader(sPath)
        Dim i As Integer = 0
        If System.IO.File.Exists(sPath) Then
            While Not streamStars.EndOfStream
                Dim sLine As String
                sLine = streamStars.ReadLine()
                Dim uneStar As New CStar
                uneStar.initStarFromLine(sLine)
                stars(compteur_stars) = uneStar
                compteur_stars = compteur_stars + 1
                i = i + 1
            End While
        End If

    End Sub

    '

    ''' <summary>
    ''' Initialize OpenGL
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub gl_initialize()
        '
        ' Initialisation OPENGL
        ' --------------
        m_intptrHdc = User.GetDC(frmDisplay.picMapView.Handle)
        '
        setupPixelFormat(frmDisplay.picMapView.CreateGraphics.GetHdc)
        hGLRC = Wgl.wglCreateContext(m_intptrHdc)
        Wgl.wglMakeCurrent(m_intptrHdc, hGLRC)
        '
        ' Param�trage
        ' -----------
        Gl.glEnable(Gl.GL_DEPTH_TEST) ' Teste les faces cach�es
        Gl.glEnable(Gl.GL_TEXTURE_2D)
        Gl.glMatrixMode(Gl.GL_PROJECTION) ' Matrice transfo 3D � 2D
        Gl.glMatrixMode(Gl.GL_MODELVIEW)
        Gl.glEnable(Gl.GL_BLEND)
        Gl.glBlendFunc(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA)
        Gl.glEnable(Gl.GL_LINE_SMOOTH)
        '
        ' G�n�ration des textures
        ' -----------------------
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Maps\earth_natural_2048.jpg", earthtexture) ' terre 3D
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\soleil.bmp", tx_soleil) ' Soleil 2D
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\sat_jour.bmp", sat_jour) ' Sat_jour
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\sat_nuit.bmp", sat_nuit) ' Sat en �clipse
        Gl.glClearColor(0, 0, 0.2, 0) ' met une transparence en fond la couleur de fond
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\lune.bmp", tx_lune) ' Lune vue 2D
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\moon.jpg", tx_moon) ' Texture lune vue 3D
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\halo.jpg", tx_halo) ' Texture etoile proche
        LoadTextureFromFile(My.Application.Info.DirectoryPath & "\Data\Textures\soleil_3D.jpg", tx_soleil3D) ' Soleil 3D
        '
        ' -------------
        ' Listes openGL
        ' -------------
        '
        ' Sphere with n% facettes
        Dim lt0, lg0, n As Short

        Gl.glNewList(glSphere, Gl.GL_COMPILE)
        n = 200
        For lg0 = 0 To n - 1
            For lt0 = -n / 4 To n / 4 - 1
                Gl.glBegin((Gl.GL_QUADS))
                Gl.glNormal3f(Cos(lt0 * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin(lt0 * pi / n * 2))
                Gl.glTexCoord2f(lg0 / n, 0.5 + lt0 / n * 2)
                Gl.glVertex3f(Cos(lt0 * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin(lt0 * pi / n * 2))
                Gl.glNormal3f(Cos((lt0 + 1) * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                Gl.glTexCoord2f(lg0 / n, 0.5 + (lt0 + 1) / n * 2)
                Gl.glVertex3f(Cos((lt0 + 1) * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                Gl.glNormal3f(Cos((lt0 + 1) * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                Gl.glTexCoord2f((lg0 + 1) / n, 0.5 + (lt0 + 1) / n * 2)
                Gl.glVertex3f(Cos((lt0 + 1) * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                Gl.glNormal3f(Cos(lt0 * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin(lt0 * pi / n * 2))
                Gl.glTexCoord2f((lg0 + 1) / n, 0.5 + lt0 / n * 2)
                Gl.glVertex3f(Cos(lt0 * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin(lt0 * pi / n * 2))
                Gl.glEnd()
            Next
        Next

        Gl.glEndList()
        Dim r As Double

        Gl.glNewList(glStation, Gl.GL_COMPILE)
        n = 8
        r = 0.02
        For lg0 = 0 To n - 1
            For lt0 = 0 To n / 4 - 1
                Gl.glBegin((Gl.GL_QUADS))
                Gl.glNormal3f(Cos(lt0 * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin(lt0 * pi / n * 2))
                Gl.glTexCoord2f(lg0 / n, 0.5 + lt0 / n * 2)
                Gl.glVertex3f(r * Cos(lt0 * pi / n * 2) * Cos(lg0 * pi / n * 2), r * Cos(lt0 * pi / n * 2) * Sin(lg0 * pi / n * 2), r * Sin(lt0 * pi / n * 2))
                Gl.glNormal3f(Cos((lt0 + 1) * pi / n * 2) * Cos(lg0 * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin(lg0 * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                Gl.glTexCoord2f(lg0 / n, 0.5 + (lt0 + 1) / n * 2)
                Gl.glVertex3f(r * Cos((lt0 + 1) * pi / n * 2) * Cos(lg0 * pi / n * 2), r * Cos((lt0 + 1) * pi / n * 2) * Sin(lg0 * pi / n * 2), r * Sin((lt0 + 1) * pi / n * 2))
                Gl.glNormal3f(Cos((lt0 + 1) * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos((lt0 + 1) * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin((lt0 + 1) * pi / n * 2))
                Gl.glTexCoord2f((lg0 + 1) / n, 0.5 + (lt0 + 1) / n * 2)
                Gl.glVertex3f(r * Cos((lt0 + 1) * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), r * Cos((lt0 + 1) * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), r * Sin((lt0 + 1) * pi / n * 2))
                Gl.glNormal3f(Cos(lt0 * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), Cos(lt0 * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), Sin(lt0 * pi / n * 2))
                Gl.glTexCoord2f((lg0 + 1) / n, 0.5 + lt0 / n * 2)
                Gl.glVertex3f(r * Cos(lt0 * pi / n * 2) * Cos((lg0 + 1) * pi / n * 2), r * Cos(lt0 * pi / n * 2) * Sin((lg0 + 1) * pi / n * 2), r * Sin(lt0 * pi / n * 2))
                Gl.glEnd()
            Next
        Next

        Gl.glEndList()

        ' Fonts
        ' -----
        gl_BuildFont2D(frmDisplay.picMapView, font1, font2)
        '
        ' Other initialisations
        ' ---------------------
        Gl.glClearColor(0, 0, 0, 0) ' D�finit la couleur de fond
        distance_oeil = 5
        psi = pi / 2
        phi = 0
        OeilX = distance_oeil * Cos(phi) * Cos(psi)
        OeilY = distance_oeil * Sin(phi)
        OeilZ = distance_oeil * Cos(phi) * Sin(psi)
        ''
    End Sub

    ''' <summary>
    ''' Needed for OpenGL repaint
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Form_Initialize_Renamed()
        blnSimulationInitialized = False
    End Sub

    ''' <summary>
    ''' Select the right time step index
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub selectTimeStepIndex()
        Dim timeStep As Double = dblTimeStep * secday
        '
        If almostEqual(timeStep, secday, timeStep / 1000) Then
            Me.updwTimeStep.SelectedIndex = 0
            frmGraphAtt.updwTimeStep.SelectedIndex = 0
            frmReportAtt.updwTimeStep.SelectedIndex = 0
            frmGraphPwr.updwTimeStep.SelectedIndex = 0
            frmReportPwr.updwTimeStep.SelectedIndex = 0
            '
        ElseIf almostEqual(timeStep, 3600, timeStep / 1000) Then
            Me.updwTimeStep.SelectedIndex = 1
            frmGraphAtt.updwTimeStep.SelectedIndex = 0
            frmReportAtt.updwTimeStep.SelectedIndex = 0
            frmGraphPwr.updwTimeStep.SelectedIndex = 0
            frmReportPwr.updwTimeStep.SelectedIndex = 0
            '
        ElseIf almostEqual(timeStep, 1800, timeStep / 1000) Then
            Me.updwTimeStep.SelectedIndex = 2
            frmGraphAtt.updwTimeStep.SelectedIndex = 1
            frmReportAtt.updwTimeStep.SelectedIndex = 1
            frmGraphPwr.updwTimeStep.SelectedIndex = 1
            frmReportPwr.updwTimeStep.SelectedIndex = 1
            '
        ElseIf almostEqual(timeStep, 600, timeStep / 1000) Then
            Me.updwTimeStep.SelectedIndex = 3
            frmGraphAtt.updwTimeStep.SelectedIndex = 2
            frmReportAtt.updwTimeStep.SelectedIndex = 2
            frmGraphPwr.updwTimeStep.SelectedIndex = 2
            frmReportPwr.updwTimeStep.SelectedIndex = 2
            '
        ElseIf almostEqual(timeStep, 300, timeStep / 1000) Then
            Me.updwTimeStep.SelectedIndex = 4
            frmGraphAtt.updwTimeStep.SelectedIndex = 3
            frmReportAtt.updwTimeStep.SelectedIndex = 3
            frmGraphPwr.updwTimeStep.SelectedIndex = 3
            frmReportPwr.updwTimeStep.SelectedIndex = 3
            '
        ElseIf almostEqual(timeStep, 180, timeStep / 1000) Then
            Me.updwTimeStep.SelectedIndex = 5
            frmGraphAtt.updwTimeStep.SelectedIndex = 4
            frmReportAtt.updwTimeStep.SelectedIndex = 4
            frmGraphPwr.updwTimeStep.SelectedIndex = 4
            frmReportPwr.updwTimeStep.SelectedIndex = 4
            '
        ElseIf almostEqual(timeStep, 60, timeStep / 1000) Then
            Me.updwTimeStep.SelectedIndex = 6
            frmGraphAtt.updwTimeStep.SelectedIndex = 5
            frmReportAtt.updwTimeStep.SelectedIndex = 5
            frmGraphPwr.updwTimeStep.SelectedIndex = 5
            frmReportPwr.updwTimeStep.SelectedIndex = 5
            '
        ElseIf almostEqual(timeStep, 10, timeStep / 1000) Then
            Me.updwTimeStep.SelectedIndex = 7
            frmGraphAtt.updwTimeStep.SelectedIndex = 6
            frmReportAtt.updwTimeStep.SelectedIndex = 6
            frmGraphPwr.updwTimeStep.SelectedIndex = 6
            frmReportPwr.updwTimeStep.SelectedIndex = 6
            '
        ElseIf almostEqual(timeStep, 5, timeStep / 1000) Then
            Me.updwTimeStep.SelectedIndex = 8
            frmGraphAtt.updwTimeStep.SelectedIndex = 7
            frmReportAtt.updwTimeStep.SelectedIndex = 7
            frmGraphPwr.updwTimeStep.SelectedIndex = 7
            frmReportPwr.updwTimeStep.SelectedIndex = 7
            '
        ElseIf almostEqual(timeStep, 1, timeStep / 1000) Then
            Me.updwTimeStep.SelectedIndex = 9
            frmGraphAtt.updwTimeStep.SelectedIndex = 8
            frmReportAtt.updwTimeStep.SelectedIndex = 8
            frmGraphPwr.updwTimeStep.SelectedIndex = 8
            frmReportPwr.updwTimeStep.SelectedIndex = 8
        End If
        ''
    End Sub

    ''' <summary>
    ''' Check the view
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub View_Change() ' ByVal eventArgs As System.EventArgs
        If allow_event Then
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
            If Me.optView_0.Checked Then  ' si vue 2D

                mnu2DView.Checked = True
                mnu3DView.Checked = False
                mnuAttitudeSphere.Checked = False
                '
                frmDisplay.chkSphere.Enabled = False
                frmDisplay.chkDayNight.Enabled = True
                frmDisplay.lblSphereOrient.Enabled = False
                frmDisplay.cmbOptAxis.Enabled = False
                frmDisplay.cmbOptOrient.Enabled = False
                frmDisplay.cmbPrinOrient.Enabled = False
                frmDisplay.cmbPrinAxis.Enabled = False
                '
                selectedView = VIEW_2D
                Sim_update() 'so that the window is shown even if you close and re-open the window
                'whithout any simulation running

                '
            ElseIf Me.optView_1.Checked Then 'vue 3D espace 
                mnu2DView.Checked = False
                mnu3DView.Checked = True
                mnuAttitudeSphere.Checked = False
                '
                frmDisplay.chkSphere.Enabled = False
                frmDisplay.chkDayNight.Enabled = True
                frmDisplay.lblSphereOrient.Enabled = False
                frmDisplay.cmbOptAxis.Enabled = False
                frmDisplay.cmbOptOrient.Enabled = False
                frmDisplay.cmbPrinOrient.Enabled = False
                frmDisplay.cmbPrinAxis.Enabled = False
                '
                selectedView = VIEW_3D
                Sim_update() 'so that the window is shown even if you close and re-open the window
                'whithout any simulation running

                '
            ElseIf Me.optView_3.Checked Then 'vue 3D satellite
                mnu2DView.Checked = False
                mnu3DView.Checked = False
                mnuAttitudeSphere.Checked = True
                '
                frmDisplay.chkSphere.Enabled = True
                frmDisplay.chkDayNight.Enabled = False
                frmDisplay.lblSphereOrient.Enabled = True
                frmDisplay.cmbOptAxis.Enabled = True
                frmDisplay.cmbOptOrient.Enabled = True
                frmDisplay.cmbPrinOrient.Enabled = True
                frmDisplay.cmbPrinAxis.Enabled = True
                '
                selectedView = VIEW_ATTITUDE
                Sim_update() 'so that the window is shown even if you close and re-open the window
                'whithout any simulation running

                '
            End If
            '
            Sim_update()
            resizeGLWindow(frmDisplay.picMapView.Width, frmDisplay.picMapView.Height)
            Sim_update()
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            Sim_update()
        End If
    End Sub

    ''' <summary>
    ''' Update the simulation
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Sim_update()
        '
        ' Allows only one call to Sim_Update
        'Static blnUpdateActive As Boolean
        '
        'If blnUpdateActive Then Exit Sub
        'blnUpdateActive = True
        '
        ' ------------
        ' Orbitography
        ' ------------
        update_CurrentDate(dblSimulationTime)
        update_Sun(dblSimulationTime, SunPos)
        update_Moon(dblSimulationTime, MoonPos)
        '
        If Not SatOrbit.Name Is Nothing Then
            SatOrbit = Satellite.getOrbit(dblSimulationTime) ' Gives orbit parameters, SatPos, SatVel, SatGeoPos
            blnEclipse = Eclipse(SatPos, SunPos) ' Eclipses
        End If
        '
        ' ---------------
        ' Graphic display
        ' ---------------
        If displayOpened Then
            Select Case selectedView
                Case VIEW_ATTITUDE
                    attsph.gl_Paint()
                Case VIEW_2D
                    frmDisplay.gl_draw_2D()
                Case VIEW_3D
                    frmDisplay.gl_draw_3D()
            End Select
        End If
        ' ---------------
        ' PowerDiagram
        ' ---------------
        frmPowerDiagram.update_panels()
        '
        'blnUpdateActive = False
        ''
    End Sub

    ''' <summary>
    ''' Refresh simulation
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub simulationloop()
        '
        Dim Index As Integer = 1 'Index of current event
        Dim CurrentEvent As New CEvent
        '
        cmdStop.Focus()
        '
        If EventsNumber > 0 Then
            While simloop
                While simloop AndAlso Index <= EventsNumber
                    CurrentEvent = EventsList.Item(Index)
                    CurrentEvent.loadEvent()
                    frmAttConf.update_attitude_conf()
                    frmAttConf.update_attitude_data_for_frmMain()
                    '
                    While simloop AndAlso (dblSimulationTime - dblSimulationStart) < CurrentEvent.EventTime
                        TickNow = GetTickCount
                        ticks = TickNow - TickLast
                        dblSimulationTime = dblSimulationTime + dblTimeStep * ticks / 1000
                        power.compute_power(dblSimulationTime, dblTimeStep * ticks / 1000)
                        Sim_update()
                        TickLast = TickNow
                        System.Windows.Forms.Application.DoEvents()
                    End While
                    '
                    Index = Index + 1
                End While
                '
                TickNow = GetTickCount
                ticks = TickNow - TickLast
                dblSimulationTime = dblSimulationTime + dblTimeStep * ticks / 1000
                power.compute_power(dblSimulationTime, dblTimeStep * ticks / 1000)
                Sim_update()
                TickLast = TickNow
                System.Windows.Forms.Application.DoEvents()
            End While
        Else
            While simloop
                TickNow = GetTickCount
                ticks = TickNow - TickLast
                dblSimulationTime = dblSimulationTime + dblTimeStep * ticks / 1000
                power.compute_power(dblSimulationTime, dblTimeStep * ticks / 1000)
                Sim_update()
                TickLast = TickNow
                System.Windows.Forms.Application.DoEvents()
            End While
        End If
        ''
    End Sub

    '

    ''' <summary>
    ''' Closing message
    ''' </summary>
    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '
        Dim answer As Short
        '
        answer = MsgBox("Are you sure that you want to close Simusat?", MsgBoxStyle.YesNoCancel Or MsgBoxStyle.Exclamation, "Warning!")
        If answer = MsgBoxResult.Yes Then
            If blnIsNewPrj Then
                answer = MsgBox("You may loose your current data file." & vbCrLf & "Do you want to save it first?", MsgBoxStyle.YesNoCancel Or MsgBoxStyle.Exclamation, "Warning!")
                If answer = MsgBoxResult.Yes Then
                    dlgMainSave.FileName = "Sans titre"
                    Dim answer2 As Windows.Forms.DialogResult = dlgMainSave.ShowDialog
                    If Not (answer2 = Windows.Forms.DialogResult.Cancel Or answer2 = Windows.Forms.DialogResult.Abort) Then
                        save_att_and_pwr_conf(dlgMainSave.FileName)
                    ElseIf answer2 = Windows.Forms.DialogResult.Cancel Then
                        e.Cancel = True
                        Exit Sub
                    End If
                    End
                ElseIf answer = MsgBoxResult.Cancel Then
                    e.Cancel = True
                    Exit Sub
                Else
                    End
                End If
            Else
                End
            End If
        Else
            e.Cancel = True
        End If

    End Sub
    Private Sub frmMain_CtrlQ(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.Control AndAlso e.KeyCode = Keys.Q Then
            Me.Close()
        End If
    End Sub

    ''' <summary>
    ''' Only for TEST
    ''' Used to make a public version of cmd[...]_Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub cmdStop_test(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.cmdStop_Click(sender, e)
    End Sub

    Public Sub cmdOneBack_test(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cmdOneBack_Click(sender, e)
    End Sub
    Public Sub cmdOneFwd_test(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cmdOneFwd_Click(sender, e)
    End Sub

    Private Sub OrbitOpen_Click(sender As Object, e As EventArgs) Handles OrbitOpen.Click
        'Call frmOrbit.cmdOrbitOpen_Click(sender, e)
        cmdOrbit.PerformClick()
        'frmOrbit.Hide()
        frmOrbit.cmdOrbitOpen.PerformClick()
        frmOrbit.cmdOk.PerformClick()
    End Sub

    Private Sub OrbitSave_Click(sender As Object, e As EventArgs) Handles OrbitSave.Click
        'Call frmOrbit.cmdOrbitSave_Click(sender, e)
        cmdOrbit.PerformClick()
        'frmOrbit.Hide()
        frmOrbit.cmdOrbitSave.PerformClick()
        frmOrbit.cmdOk.PerformClick()
    End Sub

    Private Sub cmdPowerDiagram_CheckedChanged(sender As Object, e As EventArgs) Handles cmdPowerDiagram.CheckedChanged

    End Sub

    Private Sub cmdNew_CheckedChanged(sender As Object, e As EventArgs) Handles cmdNew.CheckedChanged

    End Sub

    Private Sub lblDate_Click(sender As Object, e As EventArgs) Handles lblDate.Click

    End Sub
End Class
