﻿Imports System.Windows.Forms.DataVisualization.Charting

Public Class frmGraphAtt

    Private Sub frmGraph_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '
        Dim date_temp As udtDate
        '

        ' Initialisation date début par défaut
        '
        convert_Julian_Gregorian(SatOrbit0.epoch, date_temp)
        With date_temp
            txt_date_debut.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        '
        ' Initialisation date fin par défaut
        '
        convert_Julian_Gregorian(SatOrbit0.epoch + 1 / 24, date_temp)
        With date_temp
            txt_date_fin.Value = Format(.day, " 00/") & Format(.month, "00/") & Format(.year, "0000 ") _
                    & Format(.hour, "00:") & Format(.minute, "00:") & Format(.second, "00 ")
        End With
        '
        ' Initialisations diverses
        '
        cmbGeneral.Items.Add("xxxx")

        cmbGeneral.SelectedIndex = 0
        '
        cmbTorqueData.Items.Add("Aero")
        cmbTorqueData.Items.Add("Solar")
        cmbTorqueData.Items.Add("Magnetic")
        cmbTorqueData.Items.Add("Gravity gradient")
        cmbTorqueData.Items.Add("Total")
        cmbTorqueData.SelectedIndex = 0
        '
        cmbTorqueAxis.Items.Clear()
        cmbTorqueAxis.Items.Add("X")
        cmbTorqueAxis.Items.Add("Y")
        cmbTorqueAxis.Items.Add("Z")
        cmbTorqueAxis.Items.Add("Total") '
        cmbTorqueAxis.SelectedIndex = 0
        '
        cmbTorqueAxis.Enabled = True
        cmbTorqueData.Enabled = True
        cmbGeneral.Enabled = False
        optParGen.Checked = False
        ''
    End Sub

    ''' <summary>
    ''' Define shortcuts Save / Draw
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmGraphAtt_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyData = (Keys.Control Or Keys.S) Then
            cmdSave_Click(cmdSave, New System.EventArgs)
            e.SuppressKeyPress = True ' Pas besoin que l'évènement KeyPress le gère on vient de le faire
        ElseIf e.KeyData = (Keys.Control Or Keys.Enter) Then
            cmdDraw_Click(cmdDraw, New System.EventArgs)
            e.SuppressKeyPress = True ' Pas besoin que l'évènement KeyPress le gère on vient de le faire
        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        '
        Dim NewData As String
        Dim i As Integer
        '

        If lstData.Items.Count = 8 Then
            MsgBox("You cannot select more than 8 parameters") : Exit Sub
        End If
        '
        If optParGen.Checked Then
            NewData = cmbGeneral.Items(cmbGeneral.SelectedIndex)
            For i = 0 To lstData.Items.Count - 1
                If lstData.Items(i) = NewData Then Exit Sub ' If the parameter is already in the list don't put it another time
            Next
            lstData.Items.Add(NewData)
        Else
            NewData = "T_" & cmbTorqueData.Items(cmbTorqueData.SelectedIndex) & " " & cmbTorqueAxis.Items(cmbTorqueAxis.SelectedIndex)
            For i = 0 To lstData.Items.Count - 1
                If lstData.Items(i) = NewData Then Exit Sub ' If the parameter is already in the list no action is done
            Next
            lstData.Items.Add(NewData)
        End If
        ''
    End Sub

    Private Sub cmdDraw_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDraw.Click
        Graph.ChartAreas("ChartArea1").AxisX.StripLines.Clear() 'Delete Eclipse shadow to adjust to the timestep
        draw_curves()
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        If lstData.SelectedIndex <> -1 Then
            lstData.Items.Remove(lstData.Items(lstData.SelectedIndex))
        End If
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        '
        Dim filename As String
        '
        dlgGraphSave.FileName = "Sans titre"
        dlgGraphSave.Filter = "(*.bmp)|*.bmp"
        If dlgGraphSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
        '
        filename = dlgGraphSave.FileName
        Graph.SaveImage(filename, ChartImageFormat.Bmp)
        '
    End Sub

    Private Sub optParPan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optParPan.CheckedChanged
        If optParPan.Checked Then
            cmbGeneral.Enabled = False
            cmbTorqueData.Enabled = True
            cmbTorqueAxis.Enabled = True
        Else
            cmbGeneral.Enabled = True
            cmbTorqueData.Enabled = False
            cmbTorqueAxis.Enabled = False
        End If

    End Sub

    Private Sub draw_curves()
        '
        Dim i As Integer
        Dim NbCurves As Integer             ' Number of curves to draw
        Dim ParameterName(7) As String
        Dim ParameterUnit(7) As String
        Dim ParameterAxis(7) As Boolean

        Dim dblReportStart, dblReportTime As Double, dblReportEnd As Double, dblReportStep As Double
        Dim date_temp As udtDate
        Dim x$
        ' 
        ' Init parameters to draw
        ' --------------------------
        NbCurves = lstData.Items.Count
        For i = 0 To NbCurves - 1
            ParameterName(i) = lstData.Items(i)
            ParameterAxis(i) = False
        Next
        If chkAxis_1.Checked Then ParameterAxis(0) = True
        If chkAxis_2.Checked Then ParameterAxis(1) = True
        If chkAxis_3.Checked Then ParameterAxis(2) = True
        If chkAxis_4.Checked Then ParameterAxis(3) = True
        If chkAxis_5.Checked Then ParameterAxis(4) = True
        If chkAxis_6.Checked Then ParameterAxis(5) = True
        If chkAxis_7.Checked Then ParameterAxis(7) = True
        If chkAxis_8.Checked Then ParameterAxis(8) = True
        '
        ' Preparing arrays to draw charts
        ' -------------------------------
        Dim tabXM(lstData.Items.Count - 1) As ArrayList
        Dim tabYM(lstData.Items.Count - 1) As ArrayList
        '
        For i = 0 To NbCurves - 1
            tabXM(i) = New ArrayList
            tabYM(i) = New ArrayList
        Next
        '
        ' Attitude simulation
        ' -------------------

        ' Read start date, end date, step
        With date_temp
            .year = Year(txt_date_debut.Value)
            .month = Month(txt_date_debut.Value)
            .day = Microsoft.VisualBasic.DateAndTime.Day(txt_date_debut.Value)
            .hour = Hour(txt_date_debut.Value)
            .minute = Minute(txt_date_debut.Value)
            .second = Second(txt_date_debut.Value)
        End With
        dblReportStart = convert_Gregorian_Julian(date_temp)
        '
        With date_temp
            .year = Year(txt_date_fin.Value)
            .month = Month(txt_date_fin.Value)
            .day = Microsoft.VisualBasic.DateAndTime.Day(txt_date_fin.Value)
            .hour = Hour(txt_date_fin.Value)
            .minute = Minute(txt_date_fin.Value)
            .second = Second(txt_date_fin.Value)
        End With
        '
        dblReportEnd = convert_Gregorian_Julian(date_temp)
        If dblReportEnd < dblReportStart Then
            MsgBox("End date is earlier than start date", MsgBoxStyle.OkOnly, "")
            Exit Sub
        End If
        x$ = updwTimeStep.Text
        x$ = Microsoft.VisualBasic.Left(x$, Len(x$) - 2)
        dblReportStep = CDbl(x$) / secday
        '
        If (dblReportEnd - dblReportStart) / dblReportStep > 6000 Then
            If (MessageBox.Show("You have selected a long analysis which may need much time to compute. Do you want to continue ?", "Long analysis...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No) Then
                Exit Sub
            End If
        End If

        Me.Cursor = Cursors.WaitCursor
        '
        ' Initialisation
        dblReportTime = dblReportStart
        '
        Dim count As Integer = 0  'For the striplines
        ' Calculate and write data in the arrays
        '
        Do While dblReportTime <= dblReportEnd
            ' 
            ' Orbitography
            update_Sun(dblReportTime, SunPos)
            SatOrbit = Satellite.getOrbit(dblReportTime)
            blnEclipse = Eclipse(SatPos, SunPos)
            '
            ' Torques calculation
            calcul_couples(dblReportTime)  ' The torques Caer(Aero), Cgg(gravity gradient), Csol(solar), Cmag(Magnatic) are calculated in this function
            '
            ' Write data
            convert_Julian_Gregorian(dblReportTime, date_temp)
            With date_temp
                x$ = Format(.day, "00") & "/" & Format(.month, "00") & "/" & Format(.year, "0000") & " "
                x$ += Format(.hour, "00") & ":" & Format(.minute, "00") & ":" & Format(.second, "00") & " "
            End With
            '
            For i = 0 To NbCurves - 1
                x$ = ParameterName(i)
                Select Case x$
                    'Case "T_Aero X" : tabYM(i).Add(XX)
                    Case "T_Aero X" : tabYM(i).Add(Caer.X)
                    Case "T_Aero Y" : tabYM(i).Add(Caer.Y)
                    Case "T_Aero Z" : tabYM(i).Add(Caer.Z)
                    Case "T_Aero Total" : tabYM(i).Add(vctNorm(Caer))
                    Case "T_Solar X" : tabYM(i).Add(Csol.X)
                    Case "T_Solar Y" : tabYM(i).Add(Csol.Y)
                    Case "T_Solar Z" : tabYM(i).Add(Csol.Z)
                    Case "T_Solar Total" : tabYM(i).Add(vctNorm(Csol))
                    Case "T_Gravity gradient X" : tabYM(i).Add(Cgg.X)
                    Case "T_Gravity gradient Y" : tabYM(i).Add(Cgg.Y)
                    Case "T_Gravity gradient Z" : tabYM(i).Add(Cgg.Z)
                    Case "T_Gravity gradient Total" : tabYM(i).Add(vctNorm(Cgg))
                    Case "T_Magnetic X" : tabYM(i).Add(Cmag.X)
                    Case "T_Magnetic Y" : tabYM(i).Add(Cmag.Y)
                    Case "T_Magnetic Z" : tabYM(i).Add(Cmag.Z)
                    Case "T_Magnetic Total" : tabYM(i).Add(vctNorm(Cmag))
                    Case "T_Total X" : tabYM(i).Add(Ctot.X)
                    Case "T_Total Y" : tabYM(i).Add(Ctot.Y)
                    Case "T_Total Z" : tabYM(i).Add(Ctot.Z)
                    Case "T_Total Total" : tabYM(i).Add(vctNorm(Ctot))
                End Select
            Next
            '
            '
            For i = 0 To NbCurves - 1
                tabXM(i).Add((dblReportTime - dblReportStart) * 24 * 60)   ' X axis in minutes since beginning simulation
            Next
            '
            dblReportTime = dblReportTime + dblReportStep
            '
            If blnEclipse Then
                ' Adding grey shadow for eclipses
                AddStripline(dblReportStep, count)
            End If
            count += 1
            '
        Loop
        '
        ' Drawing curves on the graph
        ' ---------------------------
        '
        ' Parameters units
        For i = 0 To NbCurves - 1
            ParameterName(i) = ParameterName(i) & " (N.m)"
        Next
        '
        With Graph
            .Titles("Title1").Text = "Simulation from " & Format(txt_date_debut.Value, "dd/MM/yyyy HH:mm:ss") & " to " & Format(txt_date_fin.Value, "dd/MM/yyyy HH:mm:ss")
            ' X and Y axis definition
            .ChartAreas("ChartArea1").AxisX.Title = "Time (minutes from the start of the simulation)"
            .ChartAreas("ChartArea1").AxisX.IntervalAutoMode = DataVisualization.Charting.IntervalAutoMode.VariableCount
            .ChartAreas("ChartArea1").AxisY.IntervalAutoMode = DataVisualization.Charting.IntervalAutoMode.VariableCount
            .ChartAreas("ChartArea1").AxisY2.IntervalAutoMode = DataVisualization.Charting.IntervalAutoMode.VariableCount
            .ChartAreas("ChartArea1").AxisY.LabelStyle.Format = "0.00E+00"
            '
            ' Drawing curves
            '
            .Series.Clear()
            For i = 0 To NbCurves - 1
                .Series.Add(ParameterName(i))
                .Series(ParameterName(i)).Points.DataBindXY(tabXM(i), tabYM(i))
                If ParameterAxis(i) = True Then
                    .Series(ParameterName(i)).YAxisType = AxisType.Secondary
                Else
                    .Series(ParameterName(i)).YAxisType = AxisType.Primary
                End If
                .Series(ParameterName(i)).ChartType = DataVisualization.Charting.SeriesChartType.Line
                .Series(ParameterName(i)).BorderWidth = 2
            Next
            '
        End With
        '
        '
        Me.Cursor = Cursors.Default
        '
        ''
    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Hide()
    End Sub


    Private Sub AddStripline(dblReportStep As Double, count As Integer)
        Dim stripline1 As New StripLine
        stripline1.BackColor = Color.Gray
        stripline1.BorderColor = Color.Gray
        stripline1.Interval = 0
        stripline1.IntervalOffset = count * dblReportStep * secday / 60
        stripline1.StripWidth = dblReportStep * secday / 60

        'stripline1.StripWidth = dblReportStep * secday / 60
        Graph.ChartAreas("ChartArea1").AxisX.StripLines.Add(stripline1)
    End Sub
End Class