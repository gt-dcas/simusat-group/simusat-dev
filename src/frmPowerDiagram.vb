﻿Public Class frmPowerDiagram

    Public Sub frmPowerDiagram_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        TimerPowDia.Interval = 10
        TimerPowDia.Enabled = True
    End Sub

    Private Sub frmshown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        TimerPowDia.Interval = 10
        TimerPowDia.Enabled = True
    End Sub

    Private Sub TimerPwDia_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerPowDia.Tick
        frmMain.Sim_update()
        TimerPowDia.Enabled = False
    End Sub


    Private Sub update_panel_pow()
        '
        Dim ecl As Integer
        Dim X, voltage As Double
        'Dim update As New frmMain
        '
        'update.Sim_update()

        Select Case blnEclipse
            Case True
                ecl = 1
                img_pan_eclipse.Visible = True
            Case False
                ecl = 0
                img_pan_eclipse.Visible = False
        End Select
        If Format(pbat, "0.00") < 0 Then
            lab_pbat.ForeColor = Color.Red
            fl_charge.Visible = True
            fl_decharge.Visible = False
            lab_pbat_diss.Visible = False
            lab_diss_charge.Visible = True
        ElseIf pbat = 0 Then
            lab_pbat.ForeColor = Color.Black
            fl_charge.Visible = False
            fl_decharge.Visible = False
            lab_pbat_diss.Visible = False
            lab_diss_charge.Visible = True
        Else
            lab_pbat.ForeColor = Color.Black
            fl_charge.Visible = False
            fl_decharge.Visible = True
            lab_pbat_diss.Visible = True
            lab_diss_charge.Visible = False
        End If
        '
        If panels_power = 0 Then
            fl_gs.Visible = False
        Else
            fl_gs.Visible = True
        End If

        lab_pgs.Text = Format(panels_power, "0.00 ")
        lab_pbat.Text = Format(Math.Abs(pbat), "0.00 ")
        lab_pava.Text = Format(pexcess + pdem(ecl), "0.00 ")

        If pexcess <= 0.005 Then
            fl_shunt.Visible = False
            lab_exc.ForeColor = Color.Red
            lab_pava.ForeColor = Color.Red
            lab_puse.ForeColor = Color.Red
            X = 0
        Else
            fl_shunt.Visible = True
            lab_exc.ForeColor = Color.Black
            lab_pava.ForeColor = Color.Black
            lab_puse.ForeColor = Color.Black
            X = pexcess
        End If

        If dblSimulationTime > dblSimulationStart Then

            'Warning addition to complete panels string
            If gst_type = 0 Then
                voltage = batv * bat_ns     ' Not regulated case
            Else
                voltage = Vregul            ' Regulated case
            End If

            If voltage > Voc * cell_ns And PanelsNumber > 0 Then
                Label4.Enabled = True
                Label4.Visible = True
                Label4.Text = "WARNING : The minimum voltage produced by a string must be greater than the maximum voltage required on the power bus, which is normally the battery charging voltage. Maybe you should add more panel strings."

            Else
                Label4.Enabled = False
                Label4.Visible = False

            End If

        End If

        ' 

        lab_exc.Text = Format(X, "0.00 ")
        lab_puse.Text = Format(puseful, "0.00 ")
        lab_pgs_diss.Text = Format(pgs_diss, "0.00 ")
        lab_pbat_diss.Text = Format(pbat_diss, "0.00 ")
        lab_diss_charge.Text = Format(pbat_diss_charge, "0.00 ")
        lab_disscu.Text = Format(pcu_diss, "0.00 ")
        ''
    End Sub

    Private Sub update_panel_bat()
        '
        ' Updates labels and indicators of batteries current state
        ' Set battery state
        If pbat >= 0 Then
            ' Discharging         
            lab_state.Text = "Discharge..."
            If batch <= 0 Then lab_state.Text = "Discharged..."
        ElseIf pbat = 0 Then
            lab_state.Text = "Wait..."
        Else
            ' Charging ...
            lab_state.Text = "Charge..."
        End If
        '
        ' Set voltmeter and amperemeter
        lab_voltmeter.Text = Format(batv * bat_ns, "00.00") & " V"
        lab_amperemeter.Text = Format(bati * bat_np, "00.00") & " A"
        ' Set chargemeter
        Label3.Text = Format(batch, "#0.0%")
        prg_chargemeter.Value = batch * 100
        If batch >= 0.5 Then
            prg_chargemeter.ForeColor = Color.Green
        ElseIf batch > 0.2 And batch < 0.5 Then
            prg_chargemeter.ForeColor = Color.Orange
        Else
            prg_chargemeter.ForeColor = Color.Red
        End If
        ''
    End Sub

    Public Sub update_panels()
        '
        ' Calls recursively all panel-updating Subs
        Call update_panel_bat()
        Call update_panel_pow()
        ''
    End Sub

    Private Sub cmd_close_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmd_close.Click
        '
        'Desable message for the warning
        Label4.Enabled = False
        Label4.Visible = False
        Me.Close()

        '
    End Sub

End Class