﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPowerConf
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ChartArea13 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Series13 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title13 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Dim ChartArea14 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend11 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series14 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title14 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPowerConf))
        Dim ChartArea15 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend12 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series15 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title15 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Dim ChartArea16 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend13 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series16 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title16 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Dim ChartArea17 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend14 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series17 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title17 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Dim ChartArea18 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend15 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series18 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title18 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Me.tabPower = New System.Windows.Forms.TabControl()
        Me.Onglet_power_cells = New System.Windows.Forms.TabPage()
        Me.cel_cosaprox_graph = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.cel_charac_graph = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.zfra_14 = New System.Windows.Forms.GroupBox()
        Me.cel_opt_cour = New System.Windows.Forms.RadioButton()
        Me.cel_opt_puiss = New System.Windows.Forms.RadioButton()
        Me.zfra_19 = New System.Windows.Forms.GroupBox()
        Me.cel_txt_radvmp = New System.Windows.Forms.TextBox()
        Me.cel_txt_radvoc = New System.Windows.Forms.TextBox()
        Me.cel_txt_radimp = New System.Windows.Forms.TextBox()
        Me.cel_txt_radisc = New System.Windows.Forms.TextBox()
        Me.cel_opt_beol_0 = New System.Windows.Forms.RadioButton()
        Me.cel_opt_beol_1 = New System.Windows.Forms.RadioButton()
        Me.zlbl_24 = New System.Windows.Forms.Label()
        Me.zlbl_25 = New System.Windows.Forms.Label()
        Me.zlbl_26 = New System.Windows.Forms.Label()
        Me.zlbl_27 = New System.Windows.Forms.Label()
        Me.zfra_8 = New System.Windows.Forms.GroupBox()
        Me.cel_txt_tref = New System.Windows.Forms.TextBox()
        Me.cel_txt_isc = New System.Windows.Forms.TextBox()
        Me.cel_txt_disc = New System.Windows.Forms.TextBox()
        Me.cel_txt_imp = New System.Windows.Forms.TextBox()
        Me.cel_txt_dimp = New System.Windows.Forms.TextBox()
        Me.cel_txt_voc = New System.Windows.Forms.TextBox()
        Me.cel_txt_dvoc = New System.Windows.Forms.TextBox()
        Me.cel_txt_vmp = New System.Windows.Forms.TextBox()
        Me.cel_txt_dvmp = New System.Windows.Forms.TextBox()
        Me.cel_cmb_type = New System.Windows.Forms.ComboBox()
        Me.zlbl_58 = New System.Windows.Forms.Label()
        Me.zlbl_30 = New System.Windows.Forms.Label()
        Me.zlbl_31 = New System.Windows.Forms.Label()
        Me.zlbl_32 = New System.Windows.Forms.Label()
        Me.zlbl_38 = New System.Windows.Forms.Label()
        Me.zlbl_41 = New System.Windows.Forms.Label()
        Me.zlbl_42 = New System.Windows.Forms.Label()
        Me.zlbl_51 = New System.Windows.Forms.Label()
        Me.zlbl_55 = New System.Windows.Forms.Label()
        Me.zfra_21 = New System.Windows.Forms.GroupBox()
        Me.cel_txt_eff = New System.Windows.Forms.TextBox()
        Me.zlbl_22 = New System.Windows.Forms.Label()
        Me.cel_txt_surf = New System.Windows.Forms.TextBox()
        Me.cel_txt_ns = New System.Windows.Forms.TextBox()
        Me.zlbl_23 = New System.Windows.Forms.Label()
        Me.zlbl_87 = New System.Windows.Forms.Label()
        Me.zlbl_86 = New System.Windows.Forms.Label()
        Me.zfra_23 = New System.Windows.Forms.GroupBox()
        Me.cel_txt_tetad = New System.Windows.Forms.TextBox()
        Me.cel_txt_tetam = New System.Windows.Forms.TextBox()
        Me.zlbl_68 = New System.Windows.Forms.Label()
        Me.zlbl_53 = New System.Windows.Forms.Label()
        Me.zlbl_54 = New System.Windows.Forms.Label()
        Me.Onglet_power_therm = New System.Windows.Forms.TabPage()
        Me.zlbl_93 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.thr_txtBodyAlf_mz = New System.Windows.Forms.TextBox()
        Me.thr_txtBodyEps_mz = New System.Windows.Forms.TextBox()
        Me.thr_txtBodyAlf_pz = New System.Windows.Forms.TextBox()
        Me.thr_txtBodyEps_pz = New System.Windows.Forms.TextBox()
        Me.thr_txtBodyAlf_my = New System.Windows.Forms.TextBox()
        Me.thr_txtBodyEps_my = New System.Windows.Forms.TextBox()
        Me.thr_txtBodyAlf_py = New System.Windows.Forms.TextBox()
        Me.thr_txtBodyEps_py = New System.Windows.Forms.TextBox()
        Me.thr_txtBodyAlf_mx = New System.Windows.Forms.TextBox()
        Me.thr_txtBodyEps_mx = New System.Windows.Forms.TextBox()
        Me.thr_txtBodyAlf_px = New System.Windows.Forms.TextBox()
        Me.thr_txtBodyEps_px = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.thr_txtInitBodyTemp = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.thr_txtBodyCspec = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.zfra_25 = New System.Windows.Forms.GroupBox()
        Me.thr_txtTBat = New System.Windows.Forms.TextBox()
        Me.zlbl_35 = New System.Windows.Forms.Label()
        Me.zlbl_34 = New System.Windows.Forms.Label()
        Me.zfra_24 = New System.Windows.Forms.GroupBox()
        Me.thr_txtCond_8 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackAlf_8 = New System.Windows.Forms.TextBox()
        Me.thr_txtInitTemp = New System.Windows.Forms.TextBox()
        Me.thr_txtCellAlf_8 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackEps_8 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellEps_8 = New System.Windows.Forms.TextBox()
        Me.thr_txtCspec_8 = New System.Windows.Forms.TextBox()
        Me.thr_txtCond_7 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackAlf_7 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellAlf_7 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackEps_7 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellEps_7 = New System.Windows.Forms.TextBox()
        Me.thr_txtCspec_7 = New System.Windows.Forms.TextBox()
        Me.thr_txtCond_6 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackAlf_6 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellAlf_6 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackEps_6 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellEps_6 = New System.Windows.Forms.TextBox()
        Me.thr_txtCspec_6 = New System.Windows.Forms.TextBox()
        Me.thr_txtCond_5 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackAlf_5 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellAlf_5 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackEps_5 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellEps_5 = New System.Windows.Forms.TextBox()
        Me.thr_txtCspec_5 = New System.Windows.Forms.TextBox()
        Me.thr_txtCond_4 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackAlf_4 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellAlf_4 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackEps_4 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellEps_3 = New System.Windows.Forms.TextBox()
        Me.thr_txtCspec_4 = New System.Windows.Forms.TextBox()
        Me.thr_txtCond_3 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackAlf_3 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellAlf_3 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackEps_3 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellEps_2 = New System.Windows.Forms.TextBox()
        Me.thr_txtCspec_3 = New System.Windows.Forms.TextBox()
        Me.thr_txtCond_2 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackAlf_2 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellAlf_2 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackEps_2 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellEps_1 = New System.Windows.Forms.TextBox()
        Me.thr_txtCond_1 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackAlf_1 = New System.Windows.Forms.TextBox()
        Me.thr_txtBackEps_1 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellAlf_1 = New System.Windows.Forms.TextBox()
        Me.thr_txtCellEps_4 = New System.Windows.Forms.TextBox()
        Me.thr_txtCspec_2 = New System.Windows.Forms.TextBox()
        Me.thr_txtCspec_1 = New System.Windows.Forms.TextBox()
        Me.thr_lblPanName_8 = New System.Windows.Forms.Label()
        Me.thr_lblPanName_7 = New System.Windows.Forms.Label()
        Me.thr_lblPanName_6 = New System.Windows.Forms.Label()
        Me.thr_lblPanName_5 = New System.Windows.Forms.Label()
        Me.thr_lblPanName_4 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.zlbl_91 = New System.Windows.Forms.Label()
        Me.thr_lblPanName_3 = New System.Windows.Forms.Label()
        Me.zlbl_89 = New System.Windows.Forms.Label()
        Me.thr_lblPanName_2 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.zlbl_73 = New System.Windows.Forms.Label()
        Me.zlbl_72 = New System.Windows.Forms.Label()
        Me.zlbl_33 = New System.Windows.Forms.Label()
        Me.thr_lblPanName_1 = New System.Windows.Forms.Label()
        Me.Onglet_power_bat = New System.Windows.Forms.TabPage()
        Me.bat_onglet_batterie = New System.Windows.Forms.TabControl()
        Me.bat_onglet_batterie_TabPage0 = New System.Windows.Forms.TabPage()
        Me.bat_charge_graph = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.bat_frm_charge = New System.Windows.Forms.GroupBox()
        Me.bat_txt_2_5 = New System.Windows.Forms.TextBox()
        Me.bat_txt_1_5 = New System.Windows.Forms.TextBox()
        Me.bat_txt_0_5 = New System.Windows.Forms.TextBox()
        Me.bat_txt_och_5 = New System.Windows.Forms.TextBox()
        Me.bat_txt_2_1 = New System.Windows.Forms.TextBox()
        Me.bat_txt_1_1 = New System.Windows.Forms.TextBox()
        Me.bat_txt_0_1 = New System.Windows.Forms.TextBox()
        Me.bat_txt_2_2 = New System.Windows.Forms.TextBox()
        Me.bat_txt_1_2 = New System.Windows.Forms.TextBox()
        Me.bat_txt_0_2 = New System.Windows.Forms.TextBox()
        Me.bat_txt_och_1 = New System.Windows.Forms.TextBox()
        Me.bat_txt_och_2 = New System.Windows.Forms.TextBox()
        Me.bat_txt_och_4 = New System.Windows.Forms.TextBox()
        Me.bat_txt_och_3 = New System.Windows.Forms.TextBox()
        Me.bat_txt_och_0 = New System.Windows.Forms.TextBox()
        Me.bat_txt_0_4 = New System.Windows.Forms.TextBox()
        Me.bat_txt_1_4 = New System.Windows.Forms.TextBox()
        Me.bat_txt_2_4 = New System.Windows.Forms.TextBox()
        Me.bat_txt_0_3 = New System.Windows.Forms.TextBox()
        Me.bat_txt_1_3 = New System.Windows.Forms.TextBox()
        Me.bat_txt_2_3 = New System.Windows.Forms.TextBox()
        Me.bat_txt_0_0 = New System.Windows.Forms.TextBox()
        Me.bat_txt_1_0 = New System.Windows.Forms.TextBox()
        Me.bat_txt_2_0 = New System.Windows.Forms.TextBox()
        Me.zpict_1 = New System.Windows.Forms.Panel()
        Me.zlbl_62 = New System.Windows.Forms.Label()
        Me.zlbl_56 = New System.Windows.Forms.Label()
        Me.zlbl_59 = New System.Windows.Forms.Label()
        Me.zlbl_61 = New System.Windows.Forms.Label()
        Me.zlbl_60 = New System.Windows.Forms.Label()
        Me.zlbl_57 = New System.Windows.Forms.Label()
        Me.zpict_4 = New System.Windows.Forms.Panel()
        Me.bat_ch_opt_temp = New System.Windows.Forms.RadioButton()
        Me.bat_ch_opt_courant = New System.Windows.Forms.RadioButton()
        Me.bat_onglet_batterie_TabPage1 = New System.Windows.Forms.TabPage()
        Me.bat_decharge_graph = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.bat_frm_decharge = New System.Windows.Forms.GroupBox()
        Me.bat_txt_d100_5 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d100_1 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d100_2 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d100_4 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d100_3 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d100_0 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d2_0 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d1_0 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d0_0 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d2_3 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d1_3 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d0_3 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d2_4 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d1_4 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d0_4 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d0_2 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d1_2 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d2_2 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d0_1 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d1_1 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d2_1 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d0_5 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d1_5 = New System.Windows.Forms.TextBox()
        Me.bat_txt_d2_5 = New System.Windows.Forms.TextBox()
        Me.zpict_2 = New System.Windows.Forms.Panel()
        Me.zlbl_7 = New System.Windows.Forms.Label()
        Me.zlbl_9 = New System.Windows.Forms.Label()
        Me.zlbl_18 = New System.Windows.Forms.Label()
        Me.zlbl_19 = New System.Windows.Forms.Label()
        Me.zlbl_20 = New System.Windows.Forms.Label()
        Me.zlbl_21 = New System.Windows.Forms.Label()
        Me.zpict_3 = New System.Windows.Forms.Panel()
        Me.bat_dch_opt_temp = New System.Windows.Forms.RadioButton()
        Me.bat_dch_opt_courant = New System.Windows.Forms.RadioButton()
        Me.bat_onglet_batterie_TabPage2 = New System.Windows.Forms.TabPage()
        Me.bat_auto_graph = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.zfra_2 = New System.Windows.Forms.GroupBox()
        Me.bat_txt_tc0 = New System.Windows.Forms.TextBox()
        Me.bat_txt_tc50 = New System.Windows.Forms.TextBox()
        Me.zlbl_16 = New System.Windows.Forms.Label()
        Me.zlbl_17 = New System.Windows.Forms.Label()
        Me.bat_onglet_batterie_TabPage3 = New System.Windows.Forms.TabPage()
        Me.bat_cdispo_graph = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.zfra_5 = New System.Windows.Forms.GroupBox()
        Me.bat_txt_c10 = New System.Windows.Forms.TextBox()
        Me.bat_txt_cmax = New System.Windows.Forms.TextBox()
        Me.zlbl_12 = New System.Windows.Forms.Label()
        Me.zlbl_11 = New System.Windows.Forms.Label()
        Me.zfra_7 = New System.Windows.Forms.GroupBox()
        Me.bat_txt_t99 = New System.Windows.Forms.TextBox()
        Me.bat_txt_cmin = New System.Windows.Forms.TextBox()
        Me.bat_txt_tmin = New System.Windows.Forms.TextBox()
        Me.zlbl_15 = New System.Windows.Forms.Label()
        Me.zlbl_14 = New System.Windows.Forms.Label()
        Me.zlbl_13 = New System.Windows.Forms.Label()
        Me.zfra_10 = New System.Windows.Forms.GroupBox()
        Me.bat_txt_ns = New System.Windows.Forms.TextBox()
        Me.bat_txt_np = New System.Windows.Forms.TextBox()
        Me.zlbl_10 = New System.Windows.Forms.Label()
        Me.zlbl_4 = New System.Windows.Forms.Label()
        Me.zfra_9 = New System.Windows.Forms.GroupBox()
        Me.bat_cmb_type = New System.Windows.Forms.ComboBox()
        Me.bat_txt_c = New System.Windows.Forms.TextBox()
        Me.zlbl_29 = New System.Windows.Forms.Label()
        Me.zlbl_8 = New System.Windows.Forms.Label()
        Me.Onglet_power_manag = New System.Windows.Forms.TabPage()
        Me.zpict_6 = New System.Windows.Forms.PictureBox()
        Me.zfra_18 = New System.Windows.Forms.GroupBox()
        Me.gst_txt_chini = New System.Windows.Forms.TextBox()
        Me.gst_txt_ichmax = New System.Windows.Forms.TextBox()
        Me.gst_txt_kcharge = New System.Windows.Forms.TextBox()
        Me.gst_txt_chvmax = New System.Windows.Forms.TextBox()
        Me.gst_txt_cour_ent = New System.Windows.Forms.TextBox()
        Me.zlbl_49 = New System.Windows.Forms.Label()
        Me.zlbl_39 = New System.Windows.Forms.Label()
        Me.zlbl_48 = New System.Windows.Forms.Label()
        Me.zlbl_47 = New System.Windows.Forms.Label()
        Me.zlbl_50 = New System.Windows.Forms.Label()
        Me.zfra_16 = New System.Windows.Forms.GroupBox()
        Me.gst_txt_rendgs = New System.Windows.Forms.TextBox()
        Me.gst_txt_rendbat = New System.Windows.Forms.TextBox()
        Me.gst_opt_regul_0 = New System.Windows.Forms.RadioButton()
        Me.gst_opt_regul_1 = New System.Windows.Forms.RadioButton()
        Me.gst_txt_Vregul = New System.Windows.Forms.TextBox()
        Me.zlbl_81 = New System.Windows.Forms.Label()
        Me.zlbl_44 = New System.Windows.Forms.Label()
        Me.zlbl_45 = New System.Windows.Forms.Label()
        Me.zlbl_28 = New System.Windows.Forms.Label()
        Me.zfra_6 = New System.Windows.Forms.GroupBox()
        Me.zfra_15 = New System.Windows.Forms.GroupBox()
        Me.gst_txt_pdem_0 = New System.Windows.Forms.TextBox()
        Me.gst_txt_diss_0 = New System.Windows.Forms.TextBox()
        Me.zfra_17 = New System.Windows.Forms.GroupBox()
        Me.gst_txt_pdem_1 = New System.Windows.Forms.TextBox()
        Me.gst_txt_diss_1 = New System.Windows.Forms.TextBox()
        Me.zlbl_40 = New System.Windows.Forms.Label()
        Me.zlbl_43 = New System.Windows.Forms.Label()
        Me.cmd_quit = New System.Windows.Forms.Button()
        Me.pwr_lblmsg = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.tabPower.SuspendLayout()
        Me.Onglet_power_cells.SuspendLayout()
        CType(Me.cel_cosaprox_graph, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cel_charac_graph, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.zfra_14.SuspendLayout()
        Me.zfra_19.SuspendLayout()
        Me.zfra_8.SuspendLayout()
        Me.zfra_21.SuspendLayout()
        Me.zfra_23.SuspendLayout()
        Me.Onglet_power_therm.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.zfra_25.SuspendLayout()
        Me.zfra_24.SuspendLayout()
        Me.Onglet_power_bat.SuspendLayout()
        Me.bat_onglet_batterie.SuspendLayout()
        Me.bat_onglet_batterie_TabPage0.SuspendLayout()
        CType(Me.bat_charge_graph, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bat_frm_charge.SuspendLayout()
        Me.zpict_4.SuspendLayout()
        Me.bat_onglet_batterie_TabPage1.SuspendLayout()
        CType(Me.bat_decharge_graph, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bat_frm_decharge.SuspendLayout()
        Me.zpict_3.SuspendLayout()
        Me.bat_onglet_batterie_TabPage2.SuspendLayout()
        CType(Me.bat_auto_graph, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.zfra_2.SuspendLayout()
        Me.bat_onglet_batterie_TabPage3.SuspendLayout()
        CType(Me.bat_cdispo_graph, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.zfra_5.SuspendLayout()
        Me.zfra_7.SuspendLayout()
        Me.zfra_10.SuspendLayout()
        Me.zfra_9.SuspendLayout()
        Me.Onglet_power_manag.SuspendLayout()
        CType(Me.zpict_6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.zfra_18.SuspendLayout()
        Me.zfra_16.SuspendLayout()
        Me.zfra_6.SuspendLayout()
        Me.zfra_15.SuspendLayout()
        Me.zfra_17.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabPower
        '
        Me.tabPower.Controls.Add(Me.Onglet_power_cells)
        Me.tabPower.Controls.Add(Me.Onglet_power_therm)
        Me.tabPower.Controls.Add(Me.Onglet_power_bat)
        Me.tabPower.Controls.Add(Me.Onglet_power_manag)
        Me.tabPower.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabPower.ItemSize = New System.Drawing.Size(100, 28)
        Me.tabPower.Location = New System.Drawing.Point(23, 23)
        Me.tabPower.Name = "tabPower"
        Me.tabPower.SelectedIndex = 0
        Me.tabPower.Size = New System.Drawing.Size(1037, 531)
        Me.tabPower.TabIndex = 2
        '
        'Onglet_power_cells
        '
        Me.Onglet_power_cells.BackColor = System.Drawing.Color.Transparent
        Me.Onglet_power_cells.Controls.Add(Me.cel_cosaprox_graph)
        Me.Onglet_power_cells.Controls.Add(Me.cel_charac_graph)
        Me.Onglet_power_cells.Controls.Add(Me.zfra_14)
        Me.Onglet_power_cells.Controls.Add(Me.zfra_19)
        Me.Onglet_power_cells.Controls.Add(Me.zfra_8)
        Me.Onglet_power_cells.Controls.Add(Me.zfra_21)
        Me.Onglet_power_cells.Controls.Add(Me.zfra_23)
        Me.Onglet_power_cells.Location = New System.Drawing.Point(4, 32)
        Me.Onglet_power_cells.Name = "Onglet_power_cells"
        Me.Onglet_power_cells.Size = New System.Drawing.Size(1029, 495)
        Me.Onglet_power_cells.TabIndex = 1
        Me.Onglet_power_cells.Text = "Solar cells"
        Me.Onglet_power_cells.UseVisualStyleBackColor = True
        '
        'cel_cosaprox_graph
        '
        Me.cel_cosaprox_graph.BackColor = System.Drawing.Color.PaleTurquoise
        Me.cel_cosaprox_graph.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cel_cosaprox_graph.BorderlineColor = System.Drawing.Color.Teal
        Me.cel_cosaprox_graph.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.cel_cosaprox_graph.BorderlineWidth = 2
        ChartArea13.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount
        ChartArea13.AxisX.Minimum = 0R
        ChartArea13.AxisY.Minimum = 0R
        ChartArea13.BackColor = System.Drawing.Color.Transparent
        ChartArea13.Name = "ChartArea1"
        Me.cel_cosaprox_graph.ChartAreas.Add(ChartArea13)
        Me.cel_cosaprox_graph.Location = New System.Drawing.Point(709, 193)
        Me.cel_cosaprox_graph.Margin = New System.Windows.Forms.Padding(2)
        Me.cel_cosaprox_graph.Name = "cel_cosaprox_graph"
        Series13.ChartArea = "ChartArea1"
        Series13.Name = "Series1"
        Me.cel_cosaprox_graph.Series.Add(Series13)
        Me.cel_cosaprox_graph.Size = New System.Drawing.Size(292, 282)
        Me.cel_cosaprox_graph.TabIndex = 90
        Me.cel_cosaprox_graph.Text = "Chart1"
        Title13.Name = "Titre"
        Title13.Text = "Effect of solar incidence"
        Me.cel_cosaprox_graph.Titles.Add(Title13)
        '
        'cel_charac_graph
        '
        Me.cel_charac_graph.BackColor = System.Drawing.Color.PaleTurquoise
        Me.cel_charac_graph.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cel_charac_graph.BorderlineColor = System.Drawing.Color.DarkCyan
        Me.cel_charac_graph.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.cel_charac_graph.BorderlineWidth = 2
        ChartArea14.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount
        ChartArea14.AxisX.Minimum = 0R
        ChartArea14.AxisY.Minimum = 0R
        ChartArea14.BackColor = System.Drawing.Color.Transparent
        ChartArea14.Name = "ChartArea1"
        Me.cel_charac_graph.ChartAreas.Add(ChartArea14)
        Legend11.BackColor = System.Drawing.Color.PaleTurquoise
        Legend11.Name = "Legend1"
        Me.cel_charac_graph.Legends.Add(Legend11)
        Me.cel_charac_graph.Location = New System.Drawing.Point(121, 193)
        Me.cel_charac_graph.Margin = New System.Windows.Forms.Padding(2)
        Me.cel_charac_graph.Name = "cel_charac_graph"
        Series14.ChartArea = "ChartArea1"
        Series14.Legend = "Legend1"
        Series14.Name = "Series1"
        Me.cel_charac_graph.Series.Add(Series14)
        Me.cel_charac_graph.Size = New System.Drawing.Size(568, 282)
        Me.cel_charac_graph.TabIndex = 89
        Me.cel_charac_graph.Text = "Chart1"
        Title14.Name = "Title1"
        Title14.Text = "Electrical characteristics (one string)"
        Me.cel_charac_graph.Titles.Add(Title14)
        '
        'zfra_14
        '
        Me.zfra_14.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_14.Controls.Add(Me.cel_opt_cour)
        Me.zfra_14.Controls.Add(Me.cel_opt_puiss)
        Me.zfra_14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zfra_14.Location = New System.Drawing.Point(12, 255)
        Me.zfra_14.Name = "zfra_14"
        Me.zfra_14.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_14.Size = New System.Drawing.Size(98, 65)
        Me.zfra_14.TabIndex = 6
        Me.zfra_14.TabStop = False
        '
        'cel_opt_cour
        '
        Me.cel_opt_cour.BackColor = System.Drawing.SystemColors.Control
        Me.cel_opt_cour.Checked = True
        Me.cel_opt_cour.Cursor = System.Windows.Forms.Cursors.Default
        Me.cel_opt_cour.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cel_opt_cour.Location = New System.Drawing.Point(10, 33)
        Me.cel_opt_cour.Name = "cel_opt_cour"
        Me.cel_opt_cour.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_opt_cour.Size = New System.Drawing.Size(76, 26)
        Me.cel_opt_cour.TabIndex = 9
        Me.cel_opt_cour.TabStop = True
        Me.cel_opt_cour.Text = "Current"
        Me.cel_opt_cour.UseVisualStyleBackColor = False
        '
        'cel_opt_puiss
        '
        Me.cel_opt_puiss.BackColor = System.Drawing.SystemColors.Control
        Me.cel_opt_puiss.Cursor = System.Windows.Forms.Cursors.Default
        Me.cel_opt_puiss.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cel_opt_puiss.Location = New System.Drawing.Point(10, 10)
        Me.cel_opt_puiss.Name = "cel_opt_puiss"
        Me.cel_opt_puiss.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_opt_puiss.Size = New System.Drawing.Size(76, 26)
        Me.cel_opt_puiss.TabIndex = 7
        Me.cel_opt_puiss.TabStop = True
        Me.cel_opt_puiss.Text = "Power"
        Me.cel_opt_puiss.UseVisualStyleBackColor = False
        '
        'zfra_19
        '
        Me.zfra_19.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_19.Controls.Add(Me.cel_txt_radvmp)
        Me.zfra_19.Controls.Add(Me.cel_txt_radvoc)
        Me.zfra_19.Controls.Add(Me.cel_txt_radimp)
        Me.zfra_19.Controls.Add(Me.cel_txt_radisc)
        Me.zfra_19.Controls.Add(Me.cel_opt_beol_0)
        Me.zfra_19.Controls.Add(Me.cel_opt_beol_1)
        Me.zfra_19.Controls.Add(Me.zlbl_24)
        Me.zfra_19.Controls.Add(Me.zlbl_25)
        Me.zfra_19.Controls.Add(Me.zlbl_26)
        Me.zfra_19.Controls.Add(Me.zlbl_27)
        Me.zfra_19.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_19.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_19.Location = New System.Drawing.Point(383, 23)
        Me.zfra_19.Name = "zfra_19"
        Me.zfra_19.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_19.Size = New System.Drawing.Size(213, 153)
        Me.zfra_19.TabIndex = 20
        Me.zfra_19.TabStop = False
        Me.zfra_19.Text = "Radiations effect"
        '
        'cel_txt_radvmp
        '
        Me.cel_txt_radvmp.AcceptsReturn = True
        Me.cel_txt_radvmp.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_radvmp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_radvmp.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_radvmp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_radvmp.Location = New System.Drawing.Point(91, 111)
        Me.cel_txt_radvmp.MaxLength = 0
        Me.cel_txt_radvmp.Name = "cel_txt_radvmp"
        Me.cel_txt_radvmp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_radvmp.Size = New System.Drawing.Size(50, 23)
        Me.cel_txt_radvmp.TabIndex = 26
        Me.cel_txt_radvmp.Tag = "1"
        Me.cel_txt_radvmp.Text = "0.5"
        Me.cel_txt_radvmp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_txt_radvoc
        '
        Me.cel_txt_radvoc.AcceptsReturn = True
        Me.cel_txt_radvoc.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_radvoc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_radvoc.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_radvoc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_radvoc.Location = New System.Drawing.Point(91, 83)
        Me.cel_txt_radvoc.MaxLength = 0
        Me.cel_txt_radvoc.Name = "cel_txt_radvoc"
        Me.cel_txt_radvoc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_radvoc.Size = New System.Drawing.Size(50, 23)
        Me.cel_txt_radvoc.TabIndex = 25
        Me.cel_txt_radvoc.Tag = "1"
        Me.cel_txt_radvoc.Text = "0.5"
        Me.cel_txt_radvoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_txt_radimp
        '
        Me.cel_txt_radimp.AcceptsReturn = True
        Me.cel_txt_radimp.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_radimp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_radimp.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_radimp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_radimp.Location = New System.Drawing.Point(91, 56)
        Me.cel_txt_radimp.MaxLength = 0
        Me.cel_txt_radimp.Name = "cel_txt_radimp"
        Me.cel_txt_radimp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_radimp.Size = New System.Drawing.Size(50, 23)
        Me.cel_txt_radimp.TabIndex = 24
        Me.cel_txt_radimp.Tag = "1"
        Me.cel_txt_radimp.Text = "0.5"
        Me.cel_txt_radimp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_txt_radisc
        '
        Me.cel_txt_radisc.AcceptsReturn = True
        Me.cel_txt_radisc.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_radisc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_radisc.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_radisc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_radisc.Location = New System.Drawing.Point(91, 29)
        Me.cel_txt_radisc.MaxLength = 0
        Me.cel_txt_radisc.Name = "cel_txt_radisc"
        Me.cel_txt_radisc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_radisc.Size = New System.Drawing.Size(50, 23)
        Me.cel_txt_radisc.TabIndex = 23
        Me.cel_txt_radisc.Tag = "1"
        Me.cel_txt_radisc.Text = "0.5"
        Me.cel_txt_radisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_opt_beol_0
        '
        Me.cel_opt_beol_0.BackColor = System.Drawing.SystemColors.Control
        Me.cel_opt_beol_0.Checked = True
        Me.cel_opt_beol_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.cel_opt_beol_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_opt_beol_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cel_opt_beol_0.Location = New System.Drawing.Point(159, 55)
        Me.cel_opt_beol_0.Name = "cel_opt_beol_0"
        Me.cel_opt_beol_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_opt_beol_0.Size = New System.Drawing.Size(52, 20)
        Me.cel_opt_beol_0.TabIndex = 22
        Me.cel_opt_beol_0.TabStop = True
        Me.cel_opt_beol_0.Text = "BOL"
        Me.cel_opt_beol_0.UseVisualStyleBackColor = False
        '
        'cel_opt_beol_1
        '
        Me.cel_opt_beol_1.BackColor = System.Drawing.SystemColors.Control
        Me.cel_opt_beol_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cel_opt_beol_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_opt_beol_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cel_opt_beol_1.Location = New System.Drawing.Point(159, 68)
        Me.cel_opt_beol_1.Name = "cel_opt_beol_1"
        Me.cel_opt_beol_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_opt_beol_1.Size = New System.Drawing.Size(52, 33)
        Me.cel_opt_beol_1.TabIndex = 21
        Me.cel_opt_beol_1.TabStop = True
        Me.cel_opt_beol_1.Text = "EOL"
        Me.cel_opt_beol_1.UseVisualStyleBackColor = False
        '
        'zlbl_24
        '
        Me.zlbl_24.AutoSize = True
        Me.zlbl_24.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_24.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_24.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_24.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_24.Location = New System.Drawing.Point(7, 114)
        Me.zlbl_24.Name = "zlbl_24"
        Me.zlbl_24.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_24.Size = New System.Drawing.Size(78, 16)
        Me.zlbl_24.TabIndex = 30
        Me.zlbl_24.Text = "Vmp/Vmpo"
        Me.zlbl_24.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_25
        '
        Me.zlbl_25.AutoSize = True
        Me.zlbl_25.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_25.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_25.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_25.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_25.Location = New System.Drawing.Point(13, 86)
        Me.zlbl_25.Name = "zlbl_25"
        Me.zlbl_25.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_25.Size = New System.Drawing.Size(70, 16)
        Me.zlbl_25.TabIndex = 29
        Me.zlbl_25.Text = "Voc/Voco"
        Me.zlbl_25.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_26
        '
        Me.zlbl_26.AutoSize = True
        Me.zlbl_26.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_26.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_26.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_26.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_26.Location = New System.Drawing.Point(15, 59)
        Me.zlbl_26.Name = "zlbl_26"
        Me.zlbl_26.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_26.Size = New System.Drawing.Size(70, 16)
        Me.zlbl_26.TabIndex = 28
        Me.zlbl_26.Text = "Imp/Impo"
        Me.zlbl_26.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_27
        '
        Me.zlbl_27.AutoSize = True
        Me.zlbl_27.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_27.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_27.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_27.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_27.Location = New System.Drawing.Point(23, 32)
        Me.zlbl_27.Name = "zlbl_27"
        Me.zlbl_27.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_27.Size = New System.Drawing.Size(62, 16)
        Me.zlbl_27.TabIndex = 27
        Me.zlbl_27.Text = "Isc/Isco"
        Me.zlbl_27.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zfra_8
        '
        Me.zfra_8.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_8.Controls.Add(Me.cel_txt_tref)
        Me.zfra_8.Controls.Add(Me.cel_txt_isc)
        Me.zfra_8.Controls.Add(Me.cel_txt_disc)
        Me.zfra_8.Controls.Add(Me.cel_txt_imp)
        Me.zfra_8.Controls.Add(Me.cel_txt_dimp)
        Me.zfra_8.Controls.Add(Me.cel_txt_voc)
        Me.zfra_8.Controls.Add(Me.cel_txt_dvoc)
        Me.zfra_8.Controls.Add(Me.cel_txt_vmp)
        Me.zfra_8.Controls.Add(Me.cel_txt_dvmp)
        Me.zfra_8.Controls.Add(Me.cel_cmb_type)
        Me.zfra_8.Controls.Add(Me.zlbl_58)
        Me.zfra_8.Controls.Add(Me.zlbl_30)
        Me.zfra_8.Controls.Add(Me.zlbl_31)
        Me.zfra_8.Controls.Add(Me.zlbl_32)
        Me.zfra_8.Controls.Add(Me.zlbl_38)
        Me.zfra_8.Controls.Add(Me.zlbl_41)
        Me.zfra_8.Controls.Add(Me.zlbl_42)
        Me.zfra_8.Controls.Add(Me.zlbl_51)
        Me.zfra_8.Controls.Add(Me.zlbl_55)
        Me.zfra_8.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_8.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_8.Location = New System.Drawing.Point(12, 23)
        Me.zfra_8.Name = "zfra_8"
        Me.zfra_8.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_8.Size = New System.Drawing.Size(353, 153)
        Me.zfra_8.TabIndex = 31
        Me.zfra_8.TabStop = False
        Me.zfra_8.Text = "Characteristics"
        '
        'cel_txt_tref
        '
        Me.cel_txt_tref.AcceptsReturn = True
        Me.cel_txt_tref.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_tref.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_tref.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_tref.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_tref.Location = New System.Drawing.Point(33, 97)
        Me.cel_txt_tref.MaxLength = 0
        Me.cel_txt_tref.Name = "cel_txt_tref"
        Me.cel_txt_tref.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_tref.Size = New System.Drawing.Size(41, 23)
        Me.cel_txt_tref.TabIndex = 41
        Me.cel_txt_tref.Tag = "1"
        Me.cel_txt_tref.Text = "28"
        Me.cel_txt_tref.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_txt_isc
        '
        Me.cel_txt_isc.AcceptsReturn = True
        Me.cel_txt_isc.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_isc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_isc.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_isc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_isc.Location = New System.Drawing.Point(145, 29)
        Me.cel_txt_isc.MaxLength = 0
        Me.cel_txt_isc.Name = "cel_txt_isc"
        Me.cel_txt_isc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_isc.Size = New System.Drawing.Size(50, 23)
        Me.cel_txt_isc.TabIndex = 40
        Me.cel_txt_isc.Tag = "1"
        Me.cel_txt_isc.Text = "0.5"
        Me.cel_txt_isc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_txt_disc
        '
        Me.cel_txt_disc.AcceptsReturn = True
        Me.cel_txt_disc.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_disc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_disc.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_disc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_disc.Location = New System.Drawing.Point(282, 29)
        Me.cel_txt_disc.MaxLength = 0
        Me.cel_txt_disc.Name = "cel_txt_disc"
        Me.cel_txt_disc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_disc.Size = New System.Drawing.Size(50, 23)
        Me.cel_txt_disc.TabIndex = 39
        Me.cel_txt_disc.Tag = "1"
        Me.cel_txt_disc.Text = "0.5"
        Me.cel_txt_disc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_txt_imp
        '
        Me.cel_txt_imp.AcceptsReturn = True
        Me.cel_txt_imp.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_imp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_imp.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_imp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_imp.Location = New System.Drawing.Point(145, 56)
        Me.cel_txt_imp.MaxLength = 0
        Me.cel_txt_imp.Name = "cel_txt_imp"
        Me.cel_txt_imp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_imp.Size = New System.Drawing.Size(50, 23)
        Me.cel_txt_imp.TabIndex = 38
        Me.cel_txt_imp.Tag = "1"
        Me.cel_txt_imp.Text = "0.5"
        Me.cel_txt_imp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_txt_dimp
        '
        Me.cel_txt_dimp.AcceptsReturn = True
        Me.cel_txt_dimp.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_dimp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_dimp.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_dimp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_dimp.Location = New System.Drawing.Point(282, 56)
        Me.cel_txt_dimp.MaxLength = 0
        Me.cel_txt_dimp.Name = "cel_txt_dimp"
        Me.cel_txt_dimp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_dimp.Size = New System.Drawing.Size(50, 23)
        Me.cel_txt_dimp.TabIndex = 37
        Me.cel_txt_dimp.Tag = "1"
        Me.cel_txt_dimp.Text = "0.5"
        Me.cel_txt_dimp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_txt_voc
        '
        Me.cel_txt_voc.AcceptsReturn = True
        Me.cel_txt_voc.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_voc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_voc.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_voc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_voc.Location = New System.Drawing.Point(145, 83)
        Me.cel_txt_voc.MaxLength = 0
        Me.cel_txt_voc.Name = "cel_txt_voc"
        Me.cel_txt_voc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_voc.Size = New System.Drawing.Size(50, 23)
        Me.cel_txt_voc.TabIndex = 36
        Me.cel_txt_voc.Tag = "1"
        Me.cel_txt_voc.Text = "0.5"
        Me.cel_txt_voc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_txt_dvoc
        '
        Me.cel_txt_dvoc.AcceptsReturn = True
        Me.cel_txt_dvoc.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_dvoc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_dvoc.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_dvoc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_dvoc.Location = New System.Drawing.Point(282, 83)
        Me.cel_txt_dvoc.MaxLength = 0
        Me.cel_txt_dvoc.Name = "cel_txt_dvoc"
        Me.cel_txt_dvoc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_dvoc.Size = New System.Drawing.Size(50, 23)
        Me.cel_txt_dvoc.TabIndex = 35
        Me.cel_txt_dvoc.Tag = "1"
        Me.cel_txt_dvoc.Text = "0.5"
        Me.cel_txt_dvoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_txt_vmp
        '
        Me.cel_txt_vmp.AcceptsReturn = True
        Me.cel_txt_vmp.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_vmp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_vmp.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_vmp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_vmp.Location = New System.Drawing.Point(145, 111)
        Me.cel_txt_vmp.MaxLength = 0
        Me.cel_txt_vmp.Name = "cel_txt_vmp"
        Me.cel_txt_vmp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_vmp.Size = New System.Drawing.Size(50, 23)
        Me.cel_txt_vmp.TabIndex = 34
        Me.cel_txt_vmp.Tag = "1"
        Me.cel_txt_vmp.Text = "0.5"
        Me.cel_txt_vmp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_txt_dvmp
        '
        Me.cel_txt_dvmp.AcceptsReturn = True
        Me.cel_txt_dvmp.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_dvmp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_dvmp.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_dvmp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_dvmp.Location = New System.Drawing.Point(282, 111)
        Me.cel_txt_dvmp.MaxLength = 0
        Me.cel_txt_dvmp.Name = "cel_txt_dvmp"
        Me.cel_txt_dvmp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_dvmp.Size = New System.Drawing.Size(50, 23)
        Me.cel_txt_dvmp.TabIndex = 33
        Me.cel_txt_dvmp.Tag = "1"
        Me.cel_txt_dvmp.Text = "0.5"
        Me.cel_txt_dvmp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_cmb_type
        '
        Me.cel_cmb_type.BackColor = System.Drawing.SystemColors.Window
        Me.cel_cmb_type.Cursor = System.Windows.Forms.Cursors.Default
        Me.cel_cmb_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cel_cmb_type.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_cmb_type.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_cmb_type.Items.AddRange(New Object() {"AsGa", "Silicium", "Other"})
        Me.cel_cmb_type.Location = New System.Drawing.Point(10, 29)
        Me.cel_cmb_type.Name = "cel_cmb_type"
        Me.cel_cmb_type.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_cmb_type.Size = New System.Drawing.Size(88, 24)
        Me.cel_cmb_type.TabIndex = 32
        '
        'zlbl_58
        '
        Me.zlbl_58.AutoSize = True
        Me.zlbl_58.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_58.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_58.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_58.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_58.Location = New System.Drawing.Point(18, 74)
        Me.zlbl_58.Name = "zlbl_58"
        Me.zlbl_58.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_58.Size = New System.Drawing.Size(67, 16)
        Me.zlbl_58.TabIndex = 50
        Me.zlbl_58.Text = "Tref (°C)"
        '
        'zlbl_30
        '
        Me.zlbl_30.AutoSize = True
        Me.zlbl_30.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_30.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_30.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_30.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_30.Location = New System.Drawing.Point(114, 32)
        Me.zlbl_30.Name = "zlbl_30"
        Me.zlbl_30.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_30.Size = New System.Drawing.Size(28, 16)
        Me.zlbl_30.TabIndex = 49
        Me.zlbl_30.Text = "Isc"
        Me.zlbl_30.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_31
        '
        Me.zlbl_31.AutoSize = True
        Me.zlbl_31.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_31.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_31.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_31.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_31.Location = New System.Drawing.Point(217, 32)
        Me.zlbl_31.Name = "zlbl_31"
        Me.zlbl_31.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_31.Size = New System.Drawing.Size(59, 16)
        Me.zlbl_31.TabIndex = 48
        Me.zlbl_31.Text = "dIsc/dT"
        Me.zlbl_31.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_32
        '
        Me.zlbl_32.AutoSize = True
        Me.zlbl_32.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_32.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_32.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_32.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_32.Location = New System.Drawing.Point(110, 59)
        Me.zlbl_32.Name = "zlbl_32"
        Me.zlbl_32.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_32.Size = New System.Drawing.Size(32, 16)
        Me.zlbl_32.TabIndex = 47
        Me.zlbl_32.Text = "Imp"
        Me.zlbl_32.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_38
        '
        Me.zlbl_38.AutoSize = True
        Me.zlbl_38.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_38.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_38.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_38.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_38.Location = New System.Drawing.Point(213, 59)
        Me.zlbl_38.Name = "zlbl_38"
        Me.zlbl_38.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_38.Size = New System.Drawing.Size(63, 16)
        Me.zlbl_38.TabIndex = 46
        Me.zlbl_38.Text = "dImp/dT"
        Me.zlbl_38.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_41
        '
        Me.zlbl_41.AutoSize = True
        Me.zlbl_41.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_41.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_41.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_41.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_41.Location = New System.Drawing.Point(109, 86)
        Me.zlbl_41.Name = "zlbl_41"
        Me.zlbl_41.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_41.Size = New System.Drawing.Size(32, 16)
        Me.zlbl_41.TabIndex = 45
        Me.zlbl_41.Text = "Voc"
        Me.zlbl_41.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_42
        '
        Me.zlbl_42.AutoSize = True
        Me.zlbl_42.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_42.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_42.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_42.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_42.Location = New System.Drawing.Point(212, 86)
        Me.zlbl_42.Name = "zlbl_42"
        Me.zlbl_42.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_42.Size = New System.Drawing.Size(63, 16)
        Me.zlbl_42.TabIndex = 44
        Me.zlbl_42.Text = "dVoc/dT"
        Me.zlbl_42.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_51
        '
        Me.zlbl_51.AutoSize = True
        Me.zlbl_51.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_51.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_51.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_51.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_51.Location = New System.Drawing.Point(106, 114)
        Me.zlbl_51.Name = "zlbl_51"
        Me.zlbl_51.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_51.Size = New System.Drawing.Size(36, 16)
        Me.zlbl_51.TabIndex = 43
        Me.zlbl_51.Text = "Vmp"
        Me.zlbl_51.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_55
        '
        Me.zlbl_55.AutoSize = True
        Me.zlbl_55.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_55.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_55.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_55.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_55.Location = New System.Drawing.Point(212, 114)
        Me.zlbl_55.Name = "zlbl_55"
        Me.zlbl_55.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_55.Size = New System.Drawing.Size(67, 16)
        Me.zlbl_55.TabIndex = 42
        Me.zlbl_55.Text = "dVmp/dT"
        Me.zlbl_55.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zfra_21
        '
        Me.zfra_21.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_21.Controls.Add(Me.cel_txt_eff)
        Me.zfra_21.Controls.Add(Me.zlbl_22)
        Me.zfra_21.Controls.Add(Me.cel_txt_surf)
        Me.zfra_21.Controls.Add(Me.cel_txt_ns)
        Me.zfra_21.Controls.Add(Me.zlbl_23)
        Me.zfra_21.Controls.Add(Me.zlbl_87)
        Me.zfra_21.Controls.Add(Me.zlbl_86)
        Me.zfra_21.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_21.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_21.Location = New System.Drawing.Point(613, 23)
        Me.zfra_21.Name = "zfra_21"
        Me.zfra_21.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_21.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_21.Size = New System.Drawing.Size(178, 153)
        Me.zfra_21.TabIndex = 81
        Me.zfra_21.TabStop = False
        Me.zfra_21.Text = "Setting"
        '
        'cel_txt_eff
        '
        Me.cel_txt_eff.AcceptsReturn = True
        Me.cel_txt_eff.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_eff.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_eff.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_eff.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_eff.Location = New System.Drawing.Point(118, 52)
        Me.cel_txt_eff.MaxLength = 0
        Me.cel_txt_eff.Name = "cel_txt_eff"
        Me.cel_txt_eff.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_eff.Size = New System.Drawing.Size(49, 23)
        Me.cel_txt_eff.TabIndex = 88
        Me.cel_txt_eff.Tag = "1"
        Me.cel_txt_eff.Text = "0.14"
        Me.cel_txt_eff.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'zlbl_22
        '
        Me.zlbl_22.AutoSize = True
        Me.zlbl_22.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_22.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_22.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_22.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_22.Location = New System.Drawing.Point(14, 54)
        Me.zlbl_22.Name = "zlbl_22"
        Me.zlbl_22.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_22.Size = New System.Drawing.Size(102, 16)
        Me.zlbl_22.TabIndex = 4
        Me.zlbl_22.Text = "Efficiency (%)"
        Me.zlbl_22.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cel_txt_surf
        '
        Me.cel_txt_surf.AcceptsReturn = True
        Me.cel_txt_surf.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_surf.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_surf.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_surf.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_surf.Location = New System.Drawing.Point(118, 24)
        Me.cel_txt_surf.MaxLength = 0
        Me.cel_txt_surf.Name = "cel_txt_surf"
        Me.cel_txt_surf.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_surf.Size = New System.Drawing.Size(49, 23)
        Me.cel_txt_surf.TabIndex = 84
        Me.cel_txt_surf.Tag = "1"
        Me.cel_txt_surf.Text = "0.14"
        Me.cel_txt_surf.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_txt_ns
        '
        Me.cel_txt_ns.AcceptsReturn = True
        Me.cel_txt_ns.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_ns.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_ns.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_ns.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_ns.Location = New System.Drawing.Point(118, 82)
        Me.cel_txt_ns.MaxLength = 0
        Me.cel_txt_ns.Name = "cel_txt_ns"
        Me.cel_txt_ns.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_ns.Size = New System.Drawing.Size(49, 23)
        Me.cel_txt_ns.TabIndex = 82
        Me.cel_txt_ns.Tag = "1"
        Me.cel_txt_ns.Text = "0.5"
        Me.cel_txt_ns.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'zlbl_23
        '
        Me.zlbl_23.AutoSize = True
        Me.zlbl_23.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_23.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_23.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_23.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_23.Location = New System.Drawing.Point(8, 27)
        Me.zlbl_23.Name = "zlbl_23"
        Me.zlbl_23.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_23.Size = New System.Drawing.Size(108, 16)
        Me.zlbl_23.TabIndex = 87
        Me.zlbl_23.Text = "Cell area (cm²)"
        Me.zlbl_23.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_87
        '
        Me.zlbl_87.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_87.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_87.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_87.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_87.Location = New System.Drawing.Point(16, 114)
        Me.zlbl_87.Name = "zlbl_87"
        Me.zlbl_87.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_87.Size = New System.Drawing.Size(151, 39)
        Me.zlbl_87.TabIndex = 86
        Me.zlbl_87.Text = "String number is defined in Mount panels tab"
        Me.zlbl_87.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'zlbl_86
        '
        Me.zlbl_86.AutoSize = True
        Me.zlbl_86.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_86.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_86.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_86.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_86.Location = New System.Drawing.Point(10, 85)
        Me.zlbl_86.Name = "zlbl_86"
        Me.zlbl_86.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_86.Size = New System.Drawing.Size(106, 16)
        Me.zlbl_86.TabIndex = 85
        Me.zlbl_86.Text = "Cells per string"
        Me.zlbl_86.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zfra_23
        '
        Me.zfra_23.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_23.Controls.Add(Me.cel_txt_tetad)
        Me.zfra_23.Controls.Add(Me.cel_txt_tetam)
        Me.zfra_23.Controls.Add(Me.zlbl_68)
        Me.zfra_23.Controls.Add(Me.zlbl_53)
        Me.zfra_23.Controls.Add(Me.zlbl_54)
        Me.zfra_23.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_23.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_23.Location = New System.Drawing.Point(809, 23)
        Me.zfra_23.Name = "zfra_23"
        Me.zfra_23.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_23.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_23.Size = New System.Drawing.Size(192, 153)
        Me.zfra_23.TabIndex = 88
        Me.zfra_23.TabStop = False
        Me.zfra_23.Text = "Effect of incidence"
        '
        'cel_txt_tetad
        '
        Me.cel_txt_tetad.AcceptsReturn = True
        Me.cel_txt_tetad.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_tetad.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_tetad.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_tetad.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_tetad.Location = New System.Drawing.Point(143, 61)
        Me.cel_txt_tetad.MaxLength = 0
        Me.cel_txt_tetad.Name = "cel_txt_tetad"
        Me.cel_txt_tetad.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_tetad.Size = New System.Drawing.Size(38, 23)
        Me.cel_txt_tetad.TabIndex = 90
        Me.cel_txt_tetad.Tag = "1"
        Me.cel_txt_tetad.Text = "50"
        Me.cel_txt_tetad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cel_txt_tetam
        '
        Me.cel_txt_tetam.AcceptsReturn = True
        Me.cel_txt_tetam.BackColor = System.Drawing.SystemColors.Window
        Me.cel_txt_tetam.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cel_txt_tetam.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cel_txt_tetam.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cel_txt_tetam.Location = New System.Drawing.Point(143, 32)
        Me.cel_txt_tetam.MaxLength = 0
        Me.cel_txt_tetam.Name = "cel_txt_tetam"
        Me.cel_txt_tetam.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cel_txt_tetam.Size = New System.Drawing.Size(38, 23)
        Me.cel_txt_tetam.TabIndex = 89
        Me.cel_txt_tetam.Tag = "1"
        Me.cel_txt_tetam.Text = "75"
        Me.cel_txt_tetam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'zlbl_68
        '
        Me.zlbl_68.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_68.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_68.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_68.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_68.Location = New System.Drawing.Point(8, 97)
        Me.zlbl_68.Name = "zlbl_68"
        Me.zlbl_68.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_68.Size = New System.Drawing.Size(171, 37)
        Me.zlbl_68.TabIndex = 92
        Me.zlbl_68.Text = "(the incidence at which the curve leaves the cosine law)"
        '
        'zlbl_53
        '
        Me.zlbl_53.AutoSize = True
        Me.zlbl_53.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_53.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_53.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_53.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_53.Location = New System.Drawing.Point(8, 65)
        Me.zlbl_53.Name = "zlbl_53"
        Me.zlbl_53.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_53.Size = New System.Drawing.Size(134, 16)
        Me.zlbl_53.TabIndex = 92
        Me.zlbl_53.Text = "Divergence inc. (°)"
        '
        'zlbl_54
        '
        Me.zlbl_54.AutoSize = True
        Me.zlbl_54.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_54.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_54.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_54.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_54.Location = New System.Drawing.Point(18, 35)
        Me.zlbl_54.Name = "zlbl_54"
        Me.zlbl_54.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_54.Size = New System.Drawing.Size(125, 16)
        Me.zlbl_54.TabIndex = 91
        Me.zlbl_54.Text = "Max incidence (°)"
        '
        'Onglet_power_therm
        '
        Me.Onglet_power_therm.BackColor = System.Drawing.Color.Transparent
        Me.Onglet_power_therm.Controls.Add(Me.zlbl_93)
        Me.Onglet_power_therm.Controls.Add(Me.GroupBox1)
        Me.Onglet_power_therm.Controls.Add(Me.zfra_25)
        Me.Onglet_power_therm.Controls.Add(Me.zfra_24)
        Me.Onglet_power_therm.Location = New System.Drawing.Point(4, 32)
        Me.Onglet_power_therm.Name = "Onglet_power_therm"
        Me.Onglet_power_therm.Size = New System.Drawing.Size(1029, 495)
        Me.Onglet_power_therm.TabIndex = 5
        Me.Onglet_power_therm.Text = "Thermical data"
        Me.Onglet_power_therm.UseVisualStyleBackColor = True
        '
        'zlbl_93
        '
        Me.zlbl_93.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_93.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_93.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_93.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_93.Location = New System.Drawing.Point(352, 430)
        Me.zlbl_93.Name = "zlbl_93"
        Me.zlbl_93.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_93.Size = New System.Drawing.Size(644, 52)
        Me.zlbl_93.TabIndex = 87
        Me.zlbl_93.Text = resources.GetString("zlbl_93.Text")
        Me.zlbl_93.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.thr_txtBodyAlf_mz)
        Me.GroupBox1.Controls.Add(Me.thr_txtBodyEps_mz)
        Me.GroupBox1.Controls.Add(Me.thr_txtBodyAlf_pz)
        Me.GroupBox1.Controls.Add(Me.thr_txtBodyEps_pz)
        Me.GroupBox1.Controls.Add(Me.thr_txtBodyAlf_my)
        Me.GroupBox1.Controls.Add(Me.thr_txtBodyEps_my)
        Me.GroupBox1.Controls.Add(Me.thr_txtBodyAlf_py)
        Me.GroupBox1.Controls.Add(Me.thr_txtBodyEps_py)
        Me.GroupBox1.Controls.Add(Me.thr_txtBodyAlf_mx)
        Me.GroupBox1.Controls.Add(Me.thr_txtBodyEps_mx)
        Me.GroupBox1.Controls.Add(Me.thr_txtBodyAlf_px)
        Me.GroupBox1.Controls.Add(Me.thr_txtBodyEps_px)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.thr_txtInitBodyTemp)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.thr_txtBodyCspec)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Maroon
        Me.GroupBox1.Location = New System.Drawing.Point(733, 31)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(263, 381)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Central body"
        '
        'thr_txtBodyAlf_mz
        '
        Me.thr_txtBodyAlf_mz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBodyAlf_mz.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBodyAlf_mz.Location = New System.Drawing.Point(154, 239)
        Me.thr_txtBodyAlf_mz.Name = "thr_txtBodyAlf_mz"
        Me.thr_txtBodyAlf_mz.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBodyAlf_mz.TabIndex = 65
        Me.thr_txtBodyAlf_mz.Text = "0"
        Me.thr_txtBodyAlf_mz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBodyEps_mz
        '
        Me.thr_txtBodyEps_mz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBodyEps_mz.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBodyEps_mz.Location = New System.Drawing.Point(75, 239)
        Me.thr_txtBodyEps_mz.Name = "thr_txtBodyEps_mz"
        Me.thr_txtBodyEps_mz.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBodyEps_mz.TabIndex = 66
        Me.thr_txtBodyEps_mz.Text = "0"
        Me.thr_txtBodyEps_mz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBodyAlf_pz
        '
        Me.thr_txtBodyAlf_pz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBodyAlf_pz.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBodyAlf_pz.Location = New System.Drawing.Point(154, 209)
        Me.thr_txtBodyAlf_pz.Name = "thr_txtBodyAlf_pz"
        Me.thr_txtBodyAlf_pz.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBodyAlf_pz.TabIndex = 70
        Me.thr_txtBodyAlf_pz.Text = "0"
        Me.thr_txtBodyAlf_pz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBodyEps_pz
        '
        Me.thr_txtBodyEps_pz.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBodyEps_pz.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBodyEps_pz.Location = New System.Drawing.Point(75, 209)
        Me.thr_txtBodyEps_pz.Name = "thr_txtBodyEps_pz"
        Me.thr_txtBodyEps_pz.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBodyEps_pz.TabIndex = 71
        Me.thr_txtBodyEps_pz.Text = "0"
        Me.thr_txtBodyEps_pz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBodyAlf_my
        '
        Me.thr_txtBodyAlf_my.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBodyAlf_my.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBodyAlf_my.Location = New System.Drawing.Point(154, 179)
        Me.thr_txtBodyAlf_my.Name = "thr_txtBodyAlf_my"
        Me.thr_txtBodyAlf_my.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBodyAlf_my.TabIndex = 69
        Me.thr_txtBodyAlf_my.Text = "0"
        Me.thr_txtBodyAlf_my.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBodyEps_my
        '
        Me.thr_txtBodyEps_my.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBodyEps_my.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBodyEps_my.Location = New System.Drawing.Point(75, 179)
        Me.thr_txtBodyEps_my.Name = "thr_txtBodyEps_my"
        Me.thr_txtBodyEps_my.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBodyEps_my.TabIndex = 67
        Me.thr_txtBodyEps_my.Text = "0"
        Me.thr_txtBodyEps_my.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBodyAlf_py
        '
        Me.thr_txtBodyAlf_py.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBodyAlf_py.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBodyAlf_py.Location = New System.Drawing.Point(154, 149)
        Me.thr_txtBodyAlf_py.Name = "thr_txtBodyAlf_py"
        Me.thr_txtBodyAlf_py.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBodyAlf_py.TabIndex = 68
        Me.thr_txtBodyAlf_py.Text = "0"
        Me.thr_txtBodyAlf_py.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBodyEps_py
        '
        Me.thr_txtBodyEps_py.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBodyEps_py.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBodyEps_py.Location = New System.Drawing.Point(75, 149)
        Me.thr_txtBodyEps_py.Name = "thr_txtBodyEps_py"
        Me.thr_txtBodyEps_py.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBodyEps_py.TabIndex = 62
        Me.thr_txtBodyEps_py.Text = "0"
        Me.thr_txtBodyEps_py.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBodyAlf_mx
        '
        Me.thr_txtBodyAlf_mx.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBodyAlf_mx.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBodyAlf_mx.Location = New System.Drawing.Point(154, 119)
        Me.thr_txtBodyAlf_mx.Name = "thr_txtBodyAlf_mx"
        Me.thr_txtBodyAlf_mx.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBodyAlf_mx.TabIndex = 61
        Me.thr_txtBodyAlf_mx.Text = "0"
        Me.thr_txtBodyAlf_mx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBodyEps_mx
        '
        Me.thr_txtBodyEps_mx.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBodyEps_mx.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBodyEps_mx.Location = New System.Drawing.Point(75, 119)
        Me.thr_txtBodyEps_mx.Name = "thr_txtBodyEps_mx"
        Me.thr_txtBodyEps_mx.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBodyEps_mx.TabIndex = 60
        Me.thr_txtBodyEps_mx.Text = "0"
        Me.thr_txtBodyEps_mx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBodyAlf_px
        '
        Me.thr_txtBodyAlf_px.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBodyAlf_px.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBodyAlf_px.Location = New System.Drawing.Point(154, 89)
        Me.thr_txtBodyAlf_px.Name = "thr_txtBodyAlf_px"
        Me.thr_txtBodyAlf_px.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBodyAlf_px.TabIndex = 63
        Me.thr_txtBodyAlf_px.Text = "0"
        Me.thr_txtBodyAlf_px.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBodyEps_px
        '
        Me.thr_txtBodyEps_px.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBodyEps_px.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBodyEps_px.Location = New System.Drawing.Point(75, 89)
        Me.thr_txtBodyEps_px.Name = "thr_txtBodyEps_px"
        Me.thr_txtBodyEps_px.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBodyEps_px.TabIndex = 64
        Me.thr_txtBodyEps_px.Text = "0"
        Me.thr_txtBodyEps_px.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(151, 60)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(68, 27)
        Me.Label8.TabIndex = 57
        Me.Label8.Text = "a"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(70, 60)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(70, 26)
        Me.Label11.TabIndex = 58
        Me.Label11.Text = "e"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'thr_txtInitBodyTemp
        '
        Me.thr_txtInitBodyTemp.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtInitBodyTemp.ForeColor = System.Drawing.Color.Black
        Me.thr_txtInitBodyTemp.Location = New System.Drawing.Point(154, 336)
        Me.thr_txtInitBodyTemp.Name = "thr_txtInitBodyTemp"
        Me.thr_txtInitBodyTemp.Size = New System.Drawing.Size(59, 23)
        Me.thr_txtInitBodyTemp.TabIndex = 56
        Me.thr_txtInitBodyTemp.Text = "0"
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(216, 337)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 28)
        Me.Label1.TabIndex = 55
        Me.Label1.Text = "°C"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(19, 343)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(130, 16)
        Me.Label2.TabIndex = 54
        Me.Label2.Text = "Initial temperature"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'thr_txtBodyCspec
        '
        Me.thr_txtBodyCspec.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBodyCspec.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBodyCspec.Location = New System.Drawing.Point(154, 295)
        Me.thr_txtBodyCspec.Name = "thr_txtBodyCspec"
        Me.thr_txtBodyCspec.Size = New System.Drawing.Size(80, 23)
        Me.thr_txtBodyCspec.TabIndex = 23
        Me.thr_txtBodyCspec.Text = "100"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(26, 242)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(29, 16)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "- Z"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(29, 212)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(26, 16)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "+Z"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(31, 183)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(23, 16)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "-Y"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(24, 153)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(31, 16)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "+ Y"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(26, 123)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(29, 16)
        Me.Label12.TabIndex = 9
        Me.Label12.Text = "- X"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(6, 291)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(143, 31)
        Me.Label15.TabIndex = 10
        Me.Label15.Text = "Body heat capacity" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(Joules/°K)"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(24, 92)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(31, 16)
        Me.Label16.TabIndex = 11
        Me.Label16.Text = "+ X"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label19
        '
        Me.Label19.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(6, 66)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(73, 23)
        Me.Label19.TabIndex = 12
        Me.Label19.Text = "Face"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zfra_25
        '
        Me.zfra_25.Controls.Add(Me.thr_txtTBat)
        Me.zfra_25.Controls.Add(Me.zlbl_35)
        Me.zfra_25.Controls.Add(Me.zlbl_34)
        Me.zfra_25.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_25.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_25.Location = New System.Drawing.Point(55, 418)
        Me.zfra_25.Name = "zfra_25"
        Me.zfra_25.Size = New System.Drawing.Size(249, 64)
        Me.zfra_25.TabIndex = 3
        Me.zfra_25.TabStop = False
        Me.zfra_25.Text = "Battery"
        '
        'thr_txtTBat
        '
        Me.thr_txtTBat.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtTBat.ForeColor = System.Drawing.Color.Black
        Me.thr_txtTBat.Location = New System.Drawing.Point(126, 26)
        Me.thr_txtTBat.Name = "thr_txtTBat"
        Me.thr_txtTBat.Size = New System.Drawing.Size(66, 23)
        Me.thr_txtTBat.TabIndex = 23
        Me.thr_txtTBat.Text = "0"
        '
        'zlbl_35
        '
        Me.zlbl_35.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_35.ForeColor = System.Drawing.Color.Black
        Me.zlbl_35.Location = New System.Drawing.Point(198, 26)
        Me.zlbl_35.Name = "zlbl_35"
        Me.zlbl_35.Size = New System.Drawing.Size(29, 22)
        Me.zlbl_35.TabIndex = 22
        Me.zlbl_35.Text = "°C"
        Me.zlbl_35.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_34
        '
        Me.zlbl_34.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_34.ForeColor = System.Drawing.Color.Black
        Me.zlbl_34.Location = New System.Drawing.Point(15, 26)
        Me.zlbl_34.Name = "zlbl_34"
        Me.zlbl_34.Size = New System.Drawing.Size(106, 22)
        Me.zlbl_34.TabIndex = 22
        Me.zlbl_34.Text = "Temperature"
        Me.zlbl_34.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zfra_24
        '
        Me.zfra_24.Controls.Add(Me.thr_txtCond_8)
        Me.zfra_24.Controls.Add(Me.thr_txtBackAlf_8)
        Me.zfra_24.Controls.Add(Me.thr_txtInitTemp)
        Me.zfra_24.Controls.Add(Me.thr_txtCellAlf_8)
        Me.zfra_24.Controls.Add(Me.thr_txtBackEps_8)
        Me.zfra_24.Controls.Add(Me.thr_txtCellEps_8)
        Me.zfra_24.Controls.Add(Me.thr_txtCspec_8)
        Me.zfra_24.Controls.Add(Me.thr_txtCond_7)
        Me.zfra_24.Controls.Add(Me.thr_txtBackAlf_7)
        Me.zfra_24.Controls.Add(Me.thr_txtCellAlf_7)
        Me.zfra_24.Controls.Add(Me.thr_txtBackEps_7)
        Me.zfra_24.Controls.Add(Me.thr_txtCellEps_7)
        Me.zfra_24.Controls.Add(Me.thr_txtCspec_7)
        Me.zfra_24.Controls.Add(Me.thr_txtCond_6)
        Me.zfra_24.Controls.Add(Me.thr_txtBackAlf_6)
        Me.zfra_24.Controls.Add(Me.thr_txtCellAlf_6)
        Me.zfra_24.Controls.Add(Me.thr_txtBackEps_6)
        Me.zfra_24.Controls.Add(Me.thr_txtCellEps_6)
        Me.zfra_24.Controls.Add(Me.thr_txtCspec_6)
        Me.zfra_24.Controls.Add(Me.thr_txtCond_5)
        Me.zfra_24.Controls.Add(Me.thr_txtBackAlf_5)
        Me.zfra_24.Controls.Add(Me.thr_txtCellAlf_5)
        Me.zfra_24.Controls.Add(Me.thr_txtBackEps_5)
        Me.zfra_24.Controls.Add(Me.thr_txtCellEps_5)
        Me.zfra_24.Controls.Add(Me.thr_txtCspec_5)
        Me.zfra_24.Controls.Add(Me.thr_txtCond_4)
        Me.zfra_24.Controls.Add(Me.thr_txtBackAlf_4)
        Me.zfra_24.Controls.Add(Me.thr_txtCellAlf_4)
        Me.zfra_24.Controls.Add(Me.thr_txtBackEps_4)
        Me.zfra_24.Controls.Add(Me.thr_txtCellEps_3)
        Me.zfra_24.Controls.Add(Me.thr_txtCspec_4)
        Me.zfra_24.Controls.Add(Me.thr_txtCond_3)
        Me.zfra_24.Controls.Add(Me.thr_txtBackAlf_3)
        Me.zfra_24.Controls.Add(Me.thr_txtCellAlf_3)
        Me.zfra_24.Controls.Add(Me.thr_txtBackEps_3)
        Me.zfra_24.Controls.Add(Me.thr_txtCellEps_2)
        Me.zfra_24.Controls.Add(Me.thr_txtCspec_3)
        Me.zfra_24.Controls.Add(Me.thr_txtCond_2)
        Me.zfra_24.Controls.Add(Me.thr_txtBackAlf_2)
        Me.zfra_24.Controls.Add(Me.thr_txtCellAlf_2)
        Me.zfra_24.Controls.Add(Me.thr_txtBackEps_2)
        Me.zfra_24.Controls.Add(Me.thr_txtCellEps_1)
        Me.zfra_24.Controls.Add(Me.thr_txtCond_1)
        Me.zfra_24.Controls.Add(Me.thr_txtBackAlf_1)
        Me.zfra_24.Controls.Add(Me.thr_txtBackEps_1)
        Me.zfra_24.Controls.Add(Me.thr_txtCellAlf_1)
        Me.zfra_24.Controls.Add(Me.thr_txtCellEps_4)
        Me.zfra_24.Controls.Add(Me.thr_txtCspec_2)
        Me.zfra_24.Controls.Add(Me.thr_txtCspec_1)
        Me.zfra_24.Controls.Add(Me.thr_lblPanName_8)
        Me.zfra_24.Controls.Add(Me.thr_lblPanName_7)
        Me.zfra_24.Controls.Add(Me.thr_lblPanName_6)
        Me.zfra_24.Controls.Add(Me.thr_lblPanName_5)
        Me.zfra_24.Controls.Add(Me.thr_lblPanName_4)
        Me.zfra_24.Controls.Add(Me.Label18)
        Me.zfra_24.Controls.Add(Me.zlbl_91)
        Me.zfra_24.Controls.Add(Me.thr_lblPanName_3)
        Me.zfra_24.Controls.Add(Me.zlbl_89)
        Me.zfra_24.Controls.Add(Me.thr_lblPanName_2)
        Me.zfra_24.Controls.Add(Me.Label17)
        Me.zfra_24.Controls.Add(Me.Label6)
        Me.zfra_24.Controls.Add(Me.Label13)
        Me.zfra_24.Controls.Add(Me.Label9)
        Me.zfra_24.Controls.Add(Me.Label3)
        Me.zfra_24.Controls.Add(Me.Label14)
        Me.zfra_24.Controls.Add(Me.zlbl_73)
        Me.zfra_24.Controls.Add(Me.zlbl_72)
        Me.zfra_24.Controls.Add(Me.zlbl_33)
        Me.zfra_24.Controls.Add(Me.thr_lblPanName_1)
        Me.zfra_24.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_24.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_24.Location = New System.Drawing.Point(30, 31)
        Me.zfra_24.Name = "zfra_24"
        Me.zfra_24.Size = New System.Drawing.Size(675, 381)
        Me.zfra_24.TabIndex = 2
        Me.zfra_24.TabStop = False
        Me.zfra_24.Text = "Solar panels"
        '
        'thr_txtCond_8
        '
        Me.thr_txtCond_8.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCond_8.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCond_8.Location = New System.Drawing.Point(573, 299)
        Me.thr_txtCond_8.Name = "thr_txtCond_8"
        Me.thr_txtCond_8.Size = New System.Drawing.Size(70, 23)
        Me.thr_txtCond_8.TabIndex = 24
        Me.thr_txtCond_8.Text = "0"
        Me.thr_txtCond_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackAlf_8
        '
        Me.thr_txtBackAlf_8.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackAlf_8.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackAlf_8.Location = New System.Drawing.Point(488, 299)
        Me.thr_txtBackAlf_8.Name = "thr_txtBackAlf_8"
        Me.thr_txtBackAlf_8.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackAlf_8.TabIndex = 24
        Me.thr_txtBackAlf_8.Text = "0"
        Me.thr_txtBackAlf_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtInitTemp
        '
        Me.thr_txtInitTemp.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtInitTemp.ForeColor = System.Drawing.Color.Black
        Me.thr_txtInitTemp.Location = New System.Drawing.Point(389, 338)
        Me.thr_txtInitTemp.Name = "thr_txtInitTemp"
        Me.thr_txtInitTemp.Size = New System.Drawing.Size(59, 23)
        Me.thr_txtInitTemp.TabIndex = 38
        Me.thr_txtInitTemp.Text = "0"
        '
        'thr_txtCellAlf_8
        '
        Me.thr_txtCellAlf_8.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellAlf_8.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellAlf_8.Location = New System.Drawing.Point(333, 300)
        Me.thr_txtCellAlf_8.Name = "thr_txtCellAlf_8"
        Me.thr_txtCellAlf_8.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellAlf_8.TabIndex = 38
        Me.thr_txtCellAlf_8.Text = "0"
        Me.thr_txtCellAlf_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackEps_8
        '
        Me.thr_txtBackEps_8.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackEps_8.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackEps_8.Location = New System.Drawing.Point(422, 299)
        Me.thr_txtBackEps_8.Name = "thr_txtBackEps_8"
        Me.thr_txtBackEps_8.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackEps_8.TabIndex = 37
        Me.thr_txtBackEps_8.Text = "0"
        Me.thr_txtBackEps_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellEps_8
        '
        Me.thr_txtCellEps_8.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellEps_8.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellEps_8.Location = New System.Drawing.Point(267, 300)
        Me.thr_txtCellEps_8.Name = "thr_txtCellEps_8"
        Me.thr_txtCellEps_8.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellEps_8.TabIndex = 36
        Me.thr_txtCellEps_8.Text = "0"
        Me.thr_txtCellEps_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCspec_8
        '
        Me.thr_txtCspec_8.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCspec_8.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCspec_8.Location = New System.Drawing.Point(162, 300)
        Me.thr_txtCspec_8.Name = "thr_txtCspec_8"
        Me.thr_txtCspec_8.Size = New System.Drawing.Size(80, 23)
        Me.thr_txtCspec_8.TabIndex = 41
        Me.thr_txtCspec_8.Text = "0"
        Me.thr_txtCspec_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCond_7
        '
        Me.thr_txtCond_7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCond_7.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCond_7.Location = New System.Drawing.Point(573, 269)
        Me.thr_txtCond_7.Name = "thr_txtCond_7"
        Me.thr_txtCond_7.Size = New System.Drawing.Size(70, 23)
        Me.thr_txtCond_7.TabIndex = 40
        Me.thr_txtCond_7.Text = "0"
        Me.thr_txtCond_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackAlf_7
        '
        Me.thr_txtBackAlf_7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackAlf_7.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackAlf_7.Location = New System.Drawing.Point(488, 269)
        Me.thr_txtBackAlf_7.Name = "thr_txtBackAlf_7"
        Me.thr_txtBackAlf_7.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackAlf_7.TabIndex = 40
        Me.thr_txtBackAlf_7.Text = "0"
        Me.thr_txtBackAlf_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellAlf_7
        '
        Me.thr_txtCellAlf_7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellAlf_7.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellAlf_7.Location = New System.Drawing.Point(333, 270)
        Me.thr_txtCellAlf_7.Name = "thr_txtCellAlf_7"
        Me.thr_txtCellAlf_7.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellAlf_7.TabIndex = 39
        Me.thr_txtCellAlf_7.Text = "0"
        Me.thr_txtCellAlf_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackEps_7
        '
        Me.thr_txtBackEps_7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackEps_7.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackEps_7.Location = New System.Drawing.Point(422, 269)
        Me.thr_txtBackEps_7.Name = "thr_txtBackEps_7"
        Me.thr_txtBackEps_7.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackEps_7.TabIndex = 35
        Me.thr_txtBackEps_7.Text = "0"
        Me.thr_txtBackEps_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellEps_7
        '
        Me.thr_txtCellEps_7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellEps_7.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellEps_7.Location = New System.Drawing.Point(267, 270)
        Me.thr_txtCellEps_7.Name = "thr_txtCellEps_7"
        Me.thr_txtCellEps_7.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellEps_7.TabIndex = 31
        Me.thr_txtCellEps_7.Text = "0"
        Me.thr_txtCellEps_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCspec_7
        '
        Me.thr_txtCspec_7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCspec_7.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCspec_7.Location = New System.Drawing.Point(162, 270)
        Me.thr_txtCspec_7.Name = "thr_txtCspec_7"
        Me.thr_txtCspec_7.Size = New System.Drawing.Size(80, 23)
        Me.thr_txtCspec_7.TabIndex = 30
        Me.thr_txtCspec_7.Text = "0"
        Me.thr_txtCspec_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCond_6
        '
        Me.thr_txtCond_6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCond_6.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCond_6.Location = New System.Drawing.Point(573, 239)
        Me.thr_txtCond_6.Name = "thr_txtCond_6"
        Me.thr_txtCond_6.Size = New System.Drawing.Size(70, 23)
        Me.thr_txtCond_6.TabIndex = 29
        Me.thr_txtCond_6.Text = "0"
        Me.thr_txtCond_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackAlf_6
        '
        Me.thr_txtBackAlf_6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackAlf_6.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackAlf_6.Location = New System.Drawing.Point(488, 239)
        Me.thr_txtBackAlf_6.Name = "thr_txtBackAlf_6"
        Me.thr_txtBackAlf_6.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackAlf_6.TabIndex = 29
        Me.thr_txtBackAlf_6.Text = "0"
        Me.thr_txtBackAlf_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellAlf_6
        '
        Me.thr_txtCellAlf_6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellAlf_6.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellAlf_6.Location = New System.Drawing.Point(333, 241)
        Me.thr_txtCellAlf_6.Name = "thr_txtCellAlf_6"
        Me.thr_txtCellAlf_6.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellAlf_6.TabIndex = 34
        Me.thr_txtCellAlf_6.Text = "0"
        Me.thr_txtCellAlf_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackEps_6
        '
        Me.thr_txtBackEps_6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackEps_6.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackEps_6.Location = New System.Drawing.Point(422, 239)
        Me.thr_txtBackEps_6.Name = "thr_txtBackEps_6"
        Me.thr_txtBackEps_6.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackEps_6.TabIndex = 33
        Me.thr_txtBackEps_6.Text = "0"
        Me.thr_txtBackEps_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellEps_6
        '
        Me.thr_txtCellEps_6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellEps_6.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellEps_6.Location = New System.Drawing.Point(267, 240)
        Me.thr_txtCellEps_6.Name = "thr_txtCellEps_6"
        Me.thr_txtCellEps_6.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellEps_6.TabIndex = 32
        Me.thr_txtCellEps_6.Text = "0"
        Me.thr_txtCellEps_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCspec_6
        '
        Me.thr_txtCspec_6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCspec_6.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCspec_6.Location = New System.Drawing.Point(162, 240)
        Me.thr_txtCspec_6.Name = "thr_txtCspec_6"
        Me.thr_txtCspec_6.Size = New System.Drawing.Size(80, 23)
        Me.thr_txtCspec_6.TabIndex = 51
        Me.thr_txtCspec_6.Text = "0"
        Me.thr_txtCspec_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCond_5
        '
        Me.thr_txtCond_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCond_5.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCond_5.Location = New System.Drawing.Point(573, 209)
        Me.thr_txtCond_5.Name = "thr_txtCond_5"
        Me.thr_txtCond_5.Size = New System.Drawing.Size(70, 23)
        Me.thr_txtCond_5.TabIndex = 50
        Me.thr_txtCond_5.Text = "0"
        Me.thr_txtCond_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackAlf_5
        '
        Me.thr_txtBackAlf_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackAlf_5.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackAlf_5.Location = New System.Drawing.Point(488, 209)
        Me.thr_txtBackAlf_5.Name = "thr_txtBackAlf_5"
        Me.thr_txtBackAlf_5.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackAlf_5.TabIndex = 50
        Me.thr_txtBackAlf_5.Text = "0"
        Me.thr_txtBackAlf_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellAlf_5
        '
        Me.thr_txtCellAlf_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellAlf_5.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellAlf_5.Location = New System.Drawing.Point(333, 210)
        Me.thr_txtCellAlf_5.Name = "thr_txtCellAlf_5"
        Me.thr_txtCellAlf_5.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellAlf_5.TabIndex = 49
        Me.thr_txtCellAlf_5.Text = "0"
        Me.thr_txtCellAlf_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackEps_5
        '
        Me.thr_txtBackEps_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackEps_5.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackEps_5.Location = New System.Drawing.Point(422, 209)
        Me.thr_txtBackEps_5.Name = "thr_txtBackEps_5"
        Me.thr_txtBackEps_5.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackEps_5.TabIndex = 54
        Me.thr_txtBackEps_5.Text = "0"
        Me.thr_txtBackEps_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellEps_5
        '
        Me.thr_txtCellEps_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellEps_5.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellEps_5.Location = New System.Drawing.Point(267, 210)
        Me.thr_txtCellEps_5.Name = "thr_txtCellEps_5"
        Me.thr_txtCellEps_5.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellEps_5.TabIndex = 53
        Me.thr_txtCellEps_5.Text = "0"
        Me.thr_txtCellEps_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCspec_5
        '
        Me.thr_txtCspec_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCspec_5.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCspec_5.Location = New System.Drawing.Point(162, 210)
        Me.thr_txtCspec_5.Name = "thr_txtCspec_5"
        Me.thr_txtCspec_5.Size = New System.Drawing.Size(80, 23)
        Me.thr_txtCspec_5.TabIndex = 52
        Me.thr_txtCspec_5.Text = "0"
        Me.thr_txtCspec_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCond_4
        '
        Me.thr_txtCond_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCond_4.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCond_4.Location = New System.Drawing.Point(573, 179)
        Me.thr_txtCond_4.Name = "thr_txtCond_4"
        Me.thr_txtCond_4.Size = New System.Drawing.Size(70, 23)
        Me.thr_txtCond_4.TabIndex = 48
        Me.thr_txtCond_4.Text = "0"
        Me.thr_txtCond_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackAlf_4
        '
        Me.thr_txtBackAlf_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackAlf_4.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackAlf_4.Location = New System.Drawing.Point(488, 179)
        Me.thr_txtBackAlf_4.Name = "thr_txtBackAlf_4"
        Me.thr_txtBackAlf_4.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackAlf_4.TabIndex = 48
        Me.thr_txtBackAlf_4.Text = "0"
        Me.thr_txtBackAlf_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellAlf_4
        '
        Me.thr_txtCellAlf_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellAlf_4.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellAlf_4.Location = New System.Drawing.Point(333, 180)
        Me.thr_txtCellAlf_4.Name = "thr_txtCellAlf_4"
        Me.thr_txtCellAlf_4.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellAlf_4.TabIndex = 44
        Me.thr_txtCellAlf_4.Text = "0"
        Me.thr_txtCellAlf_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackEps_4
        '
        Me.thr_txtBackEps_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackEps_4.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackEps_4.Location = New System.Drawing.Point(422, 179)
        Me.thr_txtBackEps_4.Name = "thr_txtBackEps_4"
        Me.thr_txtBackEps_4.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackEps_4.TabIndex = 43
        Me.thr_txtBackEps_4.Text = "0"
        Me.thr_txtBackEps_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellEps_3
        '
        Me.thr_txtCellEps_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellEps_3.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellEps_3.Location = New System.Drawing.Point(267, 150)
        Me.thr_txtCellEps_3.Name = "thr_txtCellEps_3"
        Me.thr_txtCellEps_3.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellEps_3.TabIndex = 42
        Me.thr_txtCellEps_3.Text = "0"
        Me.thr_txtCellEps_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCspec_4
        '
        Me.thr_txtCspec_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCspec_4.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCspec_4.Location = New System.Drawing.Point(162, 180)
        Me.thr_txtCspec_4.Name = "thr_txtCspec_4"
        Me.thr_txtCspec_4.Size = New System.Drawing.Size(80, 23)
        Me.thr_txtCspec_4.TabIndex = 47
        Me.thr_txtCspec_4.Text = "0"
        Me.thr_txtCspec_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCond_3
        '
        Me.thr_txtCond_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCond_3.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCond_3.Location = New System.Drawing.Point(573, 149)
        Me.thr_txtCond_3.Name = "thr_txtCond_3"
        Me.thr_txtCond_3.Size = New System.Drawing.Size(70, 23)
        Me.thr_txtCond_3.TabIndex = 46
        Me.thr_txtCond_3.Text = "0"
        Me.thr_txtCond_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackAlf_3
        '
        Me.thr_txtBackAlf_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackAlf_3.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackAlf_3.Location = New System.Drawing.Point(488, 149)
        Me.thr_txtBackAlf_3.Name = "thr_txtBackAlf_3"
        Me.thr_txtBackAlf_3.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackAlf_3.TabIndex = 46
        Me.thr_txtBackAlf_3.Text = "0"
        Me.thr_txtBackAlf_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellAlf_3
        '
        Me.thr_txtCellAlf_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellAlf_3.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellAlf_3.Location = New System.Drawing.Point(333, 150)
        Me.thr_txtCellAlf_3.Name = "thr_txtCellAlf_3"
        Me.thr_txtCellAlf_3.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellAlf_3.TabIndex = 45
        Me.thr_txtCellAlf_3.Text = "0"
        Me.thr_txtCellAlf_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackEps_3
        '
        Me.thr_txtBackEps_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackEps_3.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackEps_3.Location = New System.Drawing.Point(422, 149)
        Me.thr_txtBackEps_3.Name = "thr_txtBackEps_3"
        Me.thr_txtBackEps_3.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackEps_3.TabIndex = 19
        Me.thr_txtBackEps_3.Text = "0"
        Me.thr_txtBackEps_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellEps_2
        '
        Me.thr_txtCellEps_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellEps_2.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellEps_2.Location = New System.Drawing.Point(267, 120)
        Me.thr_txtCellEps_2.Name = "thr_txtCellEps_2"
        Me.thr_txtCellEps_2.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellEps_2.TabIndex = 20
        Me.thr_txtCellEps_2.Text = "0"
        Me.thr_txtCellEps_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCspec_3
        '
        Me.thr_txtCspec_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCspec_3.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCspec_3.Location = New System.Drawing.Point(162, 150)
        Me.thr_txtCspec_3.Name = "thr_txtCspec_3"
        Me.thr_txtCspec_3.Size = New System.Drawing.Size(80, 23)
        Me.thr_txtCspec_3.TabIndex = 21
        Me.thr_txtCspec_3.Text = "0"
        Me.thr_txtCspec_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCond_2
        '
        Me.thr_txtCond_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCond_2.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCond_2.Location = New System.Drawing.Point(573, 119)
        Me.thr_txtCond_2.Name = "thr_txtCond_2"
        Me.thr_txtCond_2.Size = New System.Drawing.Size(70, 23)
        Me.thr_txtCond_2.TabIndex = 18
        Me.thr_txtCond_2.Text = "0"
        Me.thr_txtCond_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackAlf_2
        '
        Me.thr_txtBackAlf_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackAlf_2.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackAlf_2.Location = New System.Drawing.Point(488, 119)
        Me.thr_txtBackAlf_2.Name = "thr_txtBackAlf_2"
        Me.thr_txtBackAlf_2.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackAlf_2.TabIndex = 18
        Me.thr_txtBackAlf_2.Text = "0"
        Me.thr_txtBackAlf_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellAlf_2
        '
        Me.thr_txtCellAlf_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellAlf_2.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellAlf_2.Location = New System.Drawing.Point(333, 120)
        Me.thr_txtCellAlf_2.Name = "thr_txtCellAlf_2"
        Me.thr_txtCellAlf_2.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellAlf_2.TabIndex = 15
        Me.thr_txtCellAlf_2.Text = "0"
        Me.thr_txtCellAlf_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackEps_2
        '
        Me.thr_txtBackEps_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackEps_2.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackEps_2.Location = New System.Drawing.Point(422, 119)
        Me.thr_txtBackEps_2.Name = "thr_txtBackEps_2"
        Me.thr_txtBackEps_2.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackEps_2.TabIndex = 16
        Me.thr_txtBackEps_2.Text = "0"
        Me.thr_txtBackEps_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellEps_1
        '
        Me.thr_txtCellEps_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellEps_1.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellEps_1.Location = New System.Drawing.Point(267, 90)
        Me.thr_txtCellEps_1.Name = "thr_txtCellEps_1"
        Me.thr_txtCellEps_1.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellEps_1.TabIndex = 17
        Me.thr_txtCellEps_1.Text = "0"
        Me.thr_txtCellEps_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCond_1
        '
        Me.thr_txtCond_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCond_1.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCond_1.Location = New System.Drawing.Point(573, 89)
        Me.thr_txtCond_1.Name = "thr_txtCond_1"
        Me.thr_txtCond_1.Size = New System.Drawing.Size(70, 23)
        Me.thr_txtCond_1.TabIndex = 26
        Me.thr_txtCond_1.Text = "0"
        Me.thr_txtCond_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackAlf_1
        '
        Me.thr_txtBackAlf_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackAlf_1.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackAlf_1.Location = New System.Drawing.Point(488, 89)
        Me.thr_txtBackAlf_1.Name = "thr_txtBackAlf_1"
        Me.thr_txtBackAlf_1.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackAlf_1.TabIndex = 26
        Me.thr_txtBackAlf_1.Text = "0"
        Me.thr_txtBackAlf_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtBackEps_1
        '
        Me.thr_txtBackEps_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtBackEps_1.ForeColor = System.Drawing.Color.Black
        Me.thr_txtBackEps_1.Location = New System.Drawing.Point(422, 89)
        Me.thr_txtBackEps_1.Name = "thr_txtBackEps_1"
        Me.thr_txtBackEps_1.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtBackEps_1.TabIndex = 27
        Me.thr_txtBackEps_1.Text = "0"
        Me.thr_txtBackEps_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellAlf_1
        '
        Me.thr_txtCellAlf_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellAlf_1.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellAlf_1.Location = New System.Drawing.Point(333, 90)
        Me.thr_txtCellAlf_1.Name = "thr_txtCellAlf_1"
        Me.thr_txtCellAlf_1.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellAlf_1.TabIndex = 28
        Me.thr_txtCellAlf_1.Text = "0"
        Me.thr_txtCellAlf_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCellEps_4
        '
        Me.thr_txtCellEps_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCellEps_4.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCellEps_4.Location = New System.Drawing.Point(267, 180)
        Me.thr_txtCellEps_4.Name = "thr_txtCellEps_4"
        Me.thr_txtCellEps_4.Size = New System.Drawing.Size(60, 23)
        Me.thr_txtCellEps_4.TabIndex = 25
        Me.thr_txtCellEps_4.Text = "0"
        Me.thr_txtCellEps_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCspec_2
        '
        Me.thr_txtCspec_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCspec_2.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCspec_2.Location = New System.Drawing.Point(162, 120)
        Me.thr_txtCspec_2.Name = "thr_txtCspec_2"
        Me.thr_txtCspec_2.Size = New System.Drawing.Size(80, 23)
        Me.thr_txtCspec_2.TabIndex = 22
        Me.thr_txtCspec_2.Text = "0"
        Me.thr_txtCspec_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_txtCspec_1
        '
        Me.thr_txtCspec_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_txtCspec_1.ForeColor = System.Drawing.Color.Black
        Me.thr_txtCspec_1.Location = New System.Drawing.Point(162, 89)
        Me.thr_txtCspec_1.Name = "thr_txtCspec_1"
        Me.thr_txtCspec_1.Size = New System.Drawing.Size(80, 23)
        Me.thr_txtCspec_1.TabIndex = 23
        Me.thr_txtCspec_1.Text = "0"
        Me.thr_txtCspec_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'thr_lblPanName_8
        '
        Me.thr_lblPanName_8.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_lblPanName_8.ForeColor = System.Drawing.Color.Black
        Me.thr_lblPanName_8.Location = New System.Drawing.Point(3, 301)
        Me.thr_lblPanName_8.Name = "thr_lblPanName_8"
        Me.thr_lblPanName_8.Size = New System.Drawing.Size(150, 24)
        Me.thr_lblPanName_8.TabIndex = 6
        Me.thr_lblPanName_8.Text = "8"
        Me.thr_lblPanName_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'thr_lblPanName_7
        '
        Me.thr_lblPanName_7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_lblPanName_7.ForeColor = System.Drawing.Color.Black
        Me.thr_lblPanName_7.Location = New System.Drawing.Point(3, 270)
        Me.thr_lblPanName_7.Name = "thr_lblPanName_7"
        Me.thr_lblPanName_7.Size = New System.Drawing.Size(150, 24)
        Me.thr_lblPanName_7.TabIndex = 7
        Me.thr_lblPanName_7.Text = "7"
        Me.thr_lblPanName_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'thr_lblPanName_6
        '
        Me.thr_lblPanName_6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_lblPanName_6.ForeColor = System.Drawing.Color.Black
        Me.thr_lblPanName_6.Location = New System.Drawing.Point(3, 240)
        Me.thr_lblPanName_6.Name = "thr_lblPanName_6"
        Me.thr_lblPanName_6.Size = New System.Drawing.Size(150, 24)
        Me.thr_lblPanName_6.TabIndex = 8
        Me.thr_lblPanName_6.Text = "6"
        Me.thr_lblPanName_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'thr_lblPanName_5
        '
        Me.thr_lblPanName_5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_lblPanName_5.ForeColor = System.Drawing.Color.Black
        Me.thr_lblPanName_5.Location = New System.Drawing.Point(3, 211)
        Me.thr_lblPanName_5.Name = "thr_lblPanName_5"
        Me.thr_lblPanName_5.Size = New System.Drawing.Size(150, 24)
        Me.thr_lblPanName_5.TabIndex = 5
        Me.thr_lblPanName_5.Text = "5"
        Me.thr_lblPanName_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'thr_lblPanName_4
        '
        Me.thr_lblPanName_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_lblPanName_4.ForeColor = System.Drawing.Color.Black
        Me.thr_lblPanName_4.Location = New System.Drawing.Point(3, 182)
        Me.thr_lblPanName_4.Name = "thr_lblPanName_4"
        Me.thr_lblPanName_4.Size = New System.Drawing.Size(150, 24)
        Me.thr_lblPanName_4.TabIndex = 3
        Me.thr_lblPanName_4.Text = "4"
        Me.thr_lblPanName_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label18
        '
        Me.Label18.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Black
        Me.Label18.Location = New System.Drawing.Point(564, 12)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(88, 76)
        Me.Label18.TabIndex = 12
        Me.Label18.Text = "Conduction" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "factor" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "with body" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(W/m²/°K)"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl_91
        '
        Me.zlbl_91.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_91.ForeColor = System.Drawing.Color.Black
        Me.zlbl_91.Location = New System.Drawing.Point(445, 32)
        Me.zlbl_91.Name = "zlbl_91"
        Me.zlbl_91.Size = New System.Drawing.Size(80, 23)
        Me.zlbl_91.TabIndex = 12
        Me.zlbl_91.Text = "Back Face"
        Me.zlbl_91.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'thr_lblPanName_3
        '
        Me.thr_lblPanName_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_lblPanName_3.ForeColor = System.Drawing.Color.Black
        Me.thr_lblPanName_3.Location = New System.Drawing.Point(3, 151)
        Me.thr_lblPanName_3.Name = "thr_lblPanName_3"
        Me.thr_lblPanName_3.Size = New System.Drawing.Size(150, 24)
        Me.thr_lblPanName_3.TabIndex = 13
        Me.thr_lblPanName_3.Text = "3"
        Me.thr_lblPanName_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_89
        '
        Me.zlbl_89.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_89.ForeColor = System.Drawing.Color.Black
        Me.zlbl_89.Location = New System.Drawing.Point(291, 33)
        Me.zlbl_89.Name = "zlbl_89"
        Me.zlbl_89.Size = New System.Drawing.Size(80, 23)
        Me.zlbl_89.TabIndex = 14
        Me.zlbl_89.Text = "Cell Face"
        Me.zlbl_89.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'thr_lblPanName_2
        '
        Me.thr_lblPanName_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_lblPanName_2.ForeColor = System.Drawing.Color.Black
        Me.thr_lblPanName_2.Location = New System.Drawing.Point(3, 121)
        Me.thr_lblPanName_2.Name = "thr_lblPanName_2"
        Me.thr_lblPanName_2.Size = New System.Drawing.Size(150, 24)
        Me.thr_lblPanName_2.TabIndex = 9
        Me.thr_lblPanName_2.Text = "2"
        Me.thr_lblPanName_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(485, 60)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(68, 27)
        Me.Label17.TabIndex = 10
        Me.Label17.Text = "a"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(335, 61)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 27)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "a"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(417, 60)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(70, 26)
        Me.Label13.TabIndex = 10
        Me.Label13.Text = "e"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label9
        '
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label9.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(420, 55)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(126, 24)
        Me.Label9.TabIndex = 10
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(267, 61)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 26)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "e"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label14
        '
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(267, 56)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(126, 24)
        Me.Label14.TabIndex = 10
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl_73
        '
        Me.zlbl_73.AutoSize = True
        Me.zlbl_73.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_73.ForeColor = System.Drawing.Color.Black
        Me.zlbl_73.Location = New System.Drawing.Point(454, 341)
        Me.zlbl_73.Name = "zlbl_73"
        Me.zlbl_73.Size = New System.Drawing.Size(24, 16)
        Me.zlbl_73.TabIndex = 10
        Me.zlbl_73.Text = "°C"
        Me.zlbl_73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl_72
        '
        Me.zlbl_72.AutoSize = True
        Me.zlbl_72.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_72.ForeColor = System.Drawing.Color.Black
        Me.zlbl_72.Location = New System.Drawing.Point(172, 341)
        Me.zlbl_72.Name = "zlbl_72"
        Me.zlbl_72.Size = New System.Drawing.Size(208, 16)
        Me.zlbl_72.TabIndex = 10
        Me.zlbl_72.Text = "Initial temperature (all panels)"
        Me.zlbl_72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zlbl_33
        '
        Me.zlbl_33.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_33.ForeColor = System.Drawing.Color.Black
        Me.zlbl_33.Location = New System.Drawing.Point(157, 37)
        Me.zlbl_33.Name = "zlbl_33"
        Me.zlbl_33.Size = New System.Drawing.Size(91, 50)
        Me.zlbl_33.TabIndex = 10
        Me.zlbl_33.Text = "Heat" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "capacity" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(Joules/°K)"
        Me.zlbl_33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'thr_lblPanName_1
        '
        Me.thr_lblPanName_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.thr_lblPanName_1.ForeColor = System.Drawing.Color.Black
        Me.thr_lblPanName_1.Location = New System.Drawing.Point(3, 94)
        Me.thr_lblPanName_1.Name = "thr_lblPanName_1"
        Me.thr_lblPanName_1.Size = New System.Drawing.Size(150, 24)
        Me.thr_lblPanName_1.TabIndex = 11
        Me.thr_lblPanName_1.Text = "SolarPanel2_3"
        Me.thr_lblPanName_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Onglet_power_bat
        '
        Me.Onglet_power_bat.BackColor = System.Drawing.Color.Transparent
        Me.Onglet_power_bat.Controls.Add(Me.bat_onglet_batterie)
        Me.Onglet_power_bat.Controls.Add(Me.zfra_10)
        Me.Onglet_power_bat.Controls.Add(Me.zfra_9)
        Me.Onglet_power_bat.Location = New System.Drawing.Point(4, 32)
        Me.Onglet_power_bat.Name = "Onglet_power_bat"
        Me.Onglet_power_bat.Size = New System.Drawing.Size(1029, 495)
        Me.Onglet_power_bat.TabIndex = 3
        Me.Onglet_power_bat.Text = "Batteries"
        Me.Onglet_power_bat.UseVisualStyleBackColor = True
        '
        'bat_onglet_batterie
        '
        Me.bat_onglet_batterie.Controls.Add(Me.bat_onglet_batterie_TabPage0)
        Me.bat_onglet_batterie.Controls.Add(Me.bat_onglet_batterie_TabPage1)
        Me.bat_onglet_batterie.Controls.Add(Me.bat_onglet_batterie_TabPage2)
        Me.bat_onglet_batterie.Controls.Add(Me.bat_onglet_batterie_TabPage3)
        Me.bat_onglet_batterie.ItemSize = New System.Drawing.Size(42, 18)
        Me.bat_onglet_batterie.Location = New System.Drawing.Point(26, 101)
        Me.bat_onglet_batterie.Name = "bat_onglet_batterie"
        Me.bat_onglet_batterie.SelectedIndex = 3
        Me.bat_onglet_batterie.Size = New System.Drawing.Size(977, 380)
        Me.bat_onglet_batterie.TabIndex = 205
        '
        'bat_onglet_batterie_TabPage0
        '
        Me.bat_onglet_batterie_TabPage0.BackColor = System.Drawing.Color.Transparent
        Me.bat_onglet_batterie_TabPage0.Controls.Add(Me.bat_charge_graph)
        Me.bat_onglet_batterie_TabPage0.Controls.Add(Me.bat_frm_charge)
        Me.bat_onglet_batterie_TabPage0.Controls.Add(Me.zpict_4)
        Me.bat_onglet_batterie_TabPage0.ForeColor = System.Drawing.Color.Maroon
        Me.bat_onglet_batterie_TabPage0.Location = New System.Drawing.Point(4, 22)
        Me.bat_onglet_batterie_TabPage0.Name = "bat_onglet_batterie_TabPage0"
        Me.bat_onglet_batterie_TabPage0.Size = New System.Drawing.Size(969, 354)
        Me.bat_onglet_batterie_TabPage0.TabIndex = 0
        Me.bat_onglet_batterie_TabPage0.Text = "Charge"
        Me.bat_onglet_batterie_TabPage0.UseVisualStyleBackColor = True
        '
        'bat_charge_graph
        '
        Me.bat_charge_graph.BackColor = System.Drawing.Color.PaleTurquoise
        Me.bat_charge_graph.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.bat_charge_graph.BorderlineColor = System.Drawing.Color.DarkCyan
        Me.bat_charge_graph.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.bat_charge_graph.BorderlineWidth = 2
        ChartArea15.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount
        ChartArea15.AxisX.Minimum = 0R
        ChartArea15.AxisY.IsStartedFromZero = False
        ChartArea15.BackColor = System.Drawing.Color.Transparent
        ChartArea15.Name = "ChartArea1"
        Me.bat_charge_graph.ChartAreas.Add(ChartArea15)
        Legend12.BackColor = System.Drawing.Color.PaleTurquoise
        Legend12.Name = "Legend1"
        Me.bat_charge_graph.Legends.Add(Legend12)
        Me.bat_charge_graph.Location = New System.Drawing.Point(369, 67)
        Me.bat_charge_graph.Margin = New System.Windows.Forms.Padding(2)
        Me.bat_charge_graph.Name = "bat_charge_graph"
        Series15.ChartArea = "ChartArea1"
        Series15.Legend = "Legend1"
        Series15.Name = "Series1"
        Me.bat_charge_graph.Series.Add(Series15)
        Me.bat_charge_graph.Size = New System.Drawing.Size(568, 268)
        Me.bat_charge_graph.TabIndex = 262
        Me.bat_charge_graph.Text = "Chart1"
        Title15.Name = "Title1"
        Me.bat_charge_graph.Titles.Add(Title15)
        '
        'bat_frm_charge
        '
        Me.bat_frm_charge.BackColor = System.Drawing.SystemColors.Control
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_2_5)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_1_5)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_0_5)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_och_5)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_2_1)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_1_1)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_0_1)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_2_2)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_1_2)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_0_2)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_och_1)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_och_2)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_och_4)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_och_3)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_och_0)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_0_4)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_1_4)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_2_4)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_0_3)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_1_3)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_2_3)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_0_0)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_1_0)
        Me.bat_frm_charge.Controls.Add(Me.bat_txt_2_0)
        Me.bat_frm_charge.Controls.Add(Me.zpict_1)
        Me.bat_frm_charge.Controls.Add(Me.zlbl_62)
        Me.bat_frm_charge.Controls.Add(Me.zlbl_56)
        Me.bat_frm_charge.Controls.Add(Me.zlbl_59)
        Me.bat_frm_charge.Controls.Add(Me.zlbl_61)
        Me.bat_frm_charge.Controls.Add(Me.zlbl_60)
        Me.bat_frm_charge.Controls.Add(Me.zlbl_57)
        Me.bat_frm_charge.ForeColor = System.Drawing.SystemColors.ControlText
        Me.bat_frm_charge.Location = New System.Drawing.Point(12, 32)
        Me.bat_frm_charge.Name = "bat_frm_charge"
        Me.bat_frm_charge.Padding = New System.Windows.Forms.Padding(0)
        Me.bat_frm_charge.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_frm_charge.Size = New System.Drawing.Size(341, 305)
        Me.bat_frm_charge.TabIndex = 261
        Me.bat_frm_charge.TabStop = False
        Me.bat_frm_charge.Text = "Charge data at 25°C and Cnom"
        '
        'bat_txt_2_5
        '
        Me.bat_txt_2_5.AcceptsReturn = True
        Me.bat_txt_2_5.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_2_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_2_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_2_5.Location = New System.Drawing.Point(224, 270)
        Me.bat_txt_2_5.MaxLength = 0
        Me.bat_txt_2_5.Name = "bat_txt_2_5"
        Me.bat_txt_2_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_2_5.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_2_5.TabIndex = 286
        Me.bat_txt_2_5.Tag = "1"
        Me.bat_txt_2_5.Text = "0.001"
        '
        'bat_txt_1_5
        '
        Me.bat_txt_1_5.AcceptsReturn = True
        Me.bat_txt_1_5.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_1_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_1_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_1_5.Location = New System.Drawing.Point(176, 270)
        Me.bat_txt_1_5.MaxLength = 0
        Me.bat_txt_1_5.Name = "bat_txt_1_5"
        Me.bat_txt_1_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_1_5.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_1_5.TabIndex = 285
        Me.bat_txt_1_5.Tag = "1"
        Me.bat_txt_1_5.Text = "0.001"
        '
        'bat_txt_0_5
        '
        Me.bat_txt_0_5.AcceptsReturn = True
        Me.bat_txt_0_5.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_0_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_0_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_0_5.Location = New System.Drawing.Point(128, 270)
        Me.bat_txt_0_5.MaxLength = 0
        Me.bat_txt_0_5.Name = "bat_txt_0_5"
        Me.bat_txt_0_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_0_5.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_0_5.TabIndex = 284
        Me.bat_txt_0_5.Tag = "1"
        Me.bat_txt_0_5.Text = "0"
        '
        'bat_txt_och_5
        '
        Me.bat_txt_och_5.AcceptsReturn = True
        Me.bat_txt_och_5.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_och_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_och_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_och_5.Location = New System.Drawing.Point(272, 270)
        Me.bat_txt_och_5.MaxLength = 0
        Me.bat_txt_och_5.Name = "bat_txt_och_5"
        Me.bat_txt_och_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_och_5.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_och_5.TabIndex = 283
        Me.bat_txt_och_5.Tag = "1"
        Me.bat_txt_och_5.Text = "0.002"
        '
        'bat_txt_2_1
        '
        Me.bat_txt_2_1.AcceptsReturn = True
        Me.bat_txt_2_1.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_2_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_2_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_2_1.Location = New System.Drawing.Point(224, 166)
        Me.bat_txt_2_1.MaxLength = 0
        Me.bat_txt_2_1.Name = "bat_txt_2_1"
        Me.bat_txt_2_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_2_1.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_2_1.TabIndex = 282
        Me.bat_txt_2_1.Tag = "1"
        Me.bat_txt_2_1.Text = "0.5"
        '
        'bat_txt_1_1
        '
        Me.bat_txt_1_1.AcceptsReturn = True
        Me.bat_txt_1_1.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_1_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_1_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_1_1.Location = New System.Drawing.Point(176, 166)
        Me.bat_txt_1_1.MaxLength = 0
        Me.bat_txt_1_1.Name = "bat_txt_1_1"
        Me.bat_txt_1_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_1_1.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_1_1.TabIndex = 281
        Me.bat_txt_1_1.Tag = "1"
        Me.bat_txt_1_1.Text = "0.1"
        '
        'bat_txt_0_1
        '
        Me.bat_txt_0_1.AcceptsReturn = True
        Me.bat_txt_0_1.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_0_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_0_1.Enabled = False
        Me.bat_txt_0_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_0_1.Location = New System.Drawing.Point(128, 166)
        Me.bat_txt_0_1.MaxLength = 0
        Me.bat_txt_0_1.Name = "bat_txt_0_1"
        Me.bat_txt_0_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_0_1.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_0_1.TabIndex = 280
        Me.bat_txt_0_1.Tag = "1"
        Me.bat_txt_0_1.Text = "0"
        '
        'bat_txt_2_2
        '
        Me.bat_txt_2_2.AcceptsReturn = True
        Me.bat_txt_2_2.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_2_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_2_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_2_2.Location = New System.Drawing.Point(224, 190)
        Me.bat_txt_2_2.MaxLength = 0
        Me.bat_txt_2_2.Name = "bat_txt_2_2"
        Me.bat_txt_2_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_2_2.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_2_2.TabIndex = 279
        Me.bat_txt_2_2.Tag = "1"
        Me.bat_txt_2_2.Text = "0"
        '
        'bat_txt_1_2
        '
        Me.bat_txt_1_2.AcceptsReturn = True
        Me.bat_txt_1_2.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_1_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_1_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_1_2.Location = New System.Drawing.Point(176, 190)
        Me.bat_txt_1_2.MaxLength = 0
        Me.bat_txt_1_2.Name = "bat_txt_1_2"
        Me.bat_txt_1_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_1_2.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_1_2.TabIndex = 278
        Me.bat_txt_1_2.Tag = "1"
        Me.bat_txt_1_2.Text = "0"
        '
        'bat_txt_0_2
        '
        Me.bat_txt_0_2.AcceptsReturn = True
        Me.bat_txt_0_2.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_0_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_0_2.Enabled = False
        Me.bat_txt_0_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_0_2.Location = New System.Drawing.Point(128, 190)
        Me.bat_txt_0_2.MaxLength = 0
        Me.bat_txt_0_2.Name = "bat_txt_0_2"
        Me.bat_txt_0_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_0_2.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_0_2.TabIndex = 277
        Me.bat_txt_0_2.Tag = "1"
        Me.bat_txt_0_2.Text = "0"
        '
        'bat_txt_och_1
        '
        Me.bat_txt_och_1.AcceptsReturn = True
        Me.bat_txt_och_1.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_och_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_och_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_och_1.Location = New System.Drawing.Point(272, 166)
        Me.bat_txt_och_1.MaxLength = 0
        Me.bat_txt_och_1.Name = "bat_txt_och_1"
        Me.bat_txt_och_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_och_1.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_och_1.TabIndex = 276
        Me.bat_txt_och_1.Tag = "1"
        Me.bat_txt_och_1.Text = "0.1"
        '
        'bat_txt_och_2
        '
        Me.bat_txt_och_2.AcceptsReturn = True
        Me.bat_txt_och_2.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_och_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_och_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_och_2.Location = New System.Drawing.Point(272, 190)
        Me.bat_txt_och_2.MaxLength = 0
        Me.bat_txt_och_2.Name = "bat_txt_och_2"
        Me.bat_txt_och_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_och_2.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_och_2.TabIndex = 275
        Me.bat_txt_och_2.Tag = "1"
        Me.bat_txt_och_2.Text = "0"
        '
        'bat_txt_och_4
        '
        Me.bat_txt_och_4.AcceptsReturn = True
        Me.bat_txt_och_4.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_och_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_och_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_och_4.Location = New System.Drawing.Point(272, 246)
        Me.bat_txt_och_4.MaxLength = 0
        Me.bat_txt_och_4.Name = "bat_txt_och_4"
        Me.bat_txt_och_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_och_4.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_och_4.TabIndex = 274
        Me.bat_txt_och_4.Tag = "1"
        Me.bat_txt_och_4.Text = "-0.002"
        '
        'bat_txt_och_3
        '
        Me.bat_txt_och_3.AcceptsReturn = True
        Me.bat_txt_och_3.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_och_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_och_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_och_3.Location = New System.Drawing.Point(272, 222)
        Me.bat_txt_och_3.MaxLength = 0
        Me.bat_txt_och_3.Name = "bat_txt_och_3"
        Me.bat_txt_och_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_och_3.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_och_3.TabIndex = 273
        Me.bat_txt_och_3.Tag = "1"
        Me.bat_txt_och_3.Text = "1.590"
        '
        'bat_txt_och_0
        '
        Me.bat_txt_och_0.AcceptsReturn = True
        Me.bat_txt_och_0.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_och_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_och_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_och_0.Location = New System.Drawing.Point(272, 142)
        Me.bat_txt_och_0.MaxLength = 0
        Me.bat_txt_och_0.Name = "bat_txt_och_0"
        Me.bat_txt_och_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_och_0.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_och_0.TabIndex = 272
        Me.bat_txt_och_0.Tag = "1"
        Me.bat_txt_och_0.Text = "120"
        '
        'bat_txt_0_4
        '
        Me.bat_txt_0_4.AcceptsReturn = True
        Me.bat_txt_0_4.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_0_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_0_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_0_4.Location = New System.Drawing.Point(128, 246)
        Me.bat_txt_0_4.MaxLength = 0
        Me.bat_txt_0_4.Name = "bat_txt_0_4"
        Me.bat_txt_0_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_0_4.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_0_4.TabIndex = 271
        Me.bat_txt_0_4.Tag = "1"
        Me.bat_txt_0_4.Text = "0"
        '
        'bat_txt_1_4
        '
        Me.bat_txt_1_4.AcceptsReturn = True
        Me.bat_txt_1_4.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_1_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_1_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_1_4.Location = New System.Drawing.Point(176, 246)
        Me.bat_txt_1_4.MaxLength = 0
        Me.bat_txt_1_4.Name = "bat_txt_1_4"
        Me.bat_txt_1_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_1_4.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_1_4.TabIndex = 270
        Me.bat_txt_1_4.Tag = "1"
        Me.bat_txt_1_4.Text = "-0.001"
        '
        'bat_txt_2_4
        '
        Me.bat_txt_2_4.AcceptsReturn = True
        Me.bat_txt_2_4.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_2_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_2_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_2_4.Location = New System.Drawing.Point(224, 246)
        Me.bat_txt_2_4.MaxLength = 0
        Me.bat_txt_2_4.Name = "bat_txt_2_4"
        Me.bat_txt_2_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_2_4.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_2_4.TabIndex = 269
        Me.bat_txt_2_4.Tag = "1"
        Me.bat_txt_2_4.Text = "-0.001"
        '
        'bat_txt_0_3
        '
        Me.bat_txt_0_3.AcceptsReturn = True
        Me.bat_txt_0_3.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_0_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_0_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_0_3.Location = New System.Drawing.Point(128, 222)
        Me.bat_txt_0_3.MaxLength = 0
        Me.bat_txt_0_3.Name = "bat_txt_0_3"
        Me.bat_txt_0_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_0_3.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_0_3.TabIndex = 268
        Me.bat_txt_0_3.Tag = "1"
        Me.bat_txt_0_3.Text = "1.300"
        '
        'bat_txt_1_3
        '
        Me.bat_txt_1_3.AcceptsReturn = True
        Me.bat_txt_1_3.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_1_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_1_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_1_3.Location = New System.Drawing.Point(176, 222)
        Me.bat_txt_1_3.MaxLength = 0
        Me.bat_txt_1_3.Name = "bat_txt_1_3"
        Me.bat_txt_1_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_1_3.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_1_3.TabIndex = 267
        Me.bat_txt_1_3.Tag = "1"
        Me.bat_txt_1_3.Text = "1.400"
        '
        'bat_txt_2_3
        '
        Me.bat_txt_2_3.AcceptsReturn = True
        Me.bat_txt_2_3.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_2_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_2_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_2_3.Location = New System.Drawing.Point(224, 222)
        Me.bat_txt_2_3.MaxLength = 0
        Me.bat_txt_2_3.Name = "bat_txt_2_3"
        Me.bat_txt_2_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_2_3.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_2_3.TabIndex = 266
        Me.bat_txt_2_3.Tag = "1"
        Me.bat_txt_2_3.Text = "1.410"
        '
        'bat_txt_0_0
        '
        Me.bat_txt_0_0.AcceptsReturn = True
        Me.bat_txt_0_0.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_0_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_0_0.Enabled = False
        Me.bat_txt_0_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_0_0.Location = New System.Drawing.Point(128, 142)
        Me.bat_txt_0_0.MaxLength = 0
        Me.bat_txt_0_0.Name = "bat_txt_0_0"
        Me.bat_txt_0_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_0_0.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_0_0.TabIndex = 265
        Me.bat_txt_0_0.Tag = "1"
        Me.bat_txt_0_0.Text = "0"
        '
        'bat_txt_1_0
        '
        Me.bat_txt_1_0.AcceptsReturn = True
        Me.bat_txt_1_0.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_1_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_1_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_1_0.Location = New System.Drawing.Point(176, 142)
        Me.bat_txt_1_0.MaxLength = 0
        Me.bat_txt_1_0.Name = "bat_txt_1_0"
        Me.bat_txt_1_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_1_0.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_1_0.TabIndex = 264
        Me.bat_txt_1_0.Tag = "1"
        Me.bat_txt_1_0.Text = "15"
        '
        'bat_txt_2_0
        '
        Me.bat_txt_2_0.AcceptsReturn = True
        Me.bat_txt_2_0.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_2_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_2_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_2_0.Location = New System.Drawing.Point(224, 142)
        Me.bat_txt_2_0.MaxLength = 0
        Me.bat_txt_2_0.Name = "bat_txt_2_0"
        Me.bat_txt_2_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_2_0.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_2_0.TabIndex = 263
        Me.bat_txt_2_0.Tag = "1"
        Me.bat_txt_2_0.Text = "65"
        '
        'zpict_1
        '
        Me.zpict_1.BackColor = System.Drawing.SystemColors.Window
        Me.zpict_1.BackgroundImage = CType(resources.GetObject("zpict_1.BackgroundImage"), System.Drawing.Image)
        Me.zpict_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.zpict_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.zpict_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zpict_1.Location = New System.Drawing.Point(64, 35)
        Me.zpict_1.Name = "zpict_1"
        Me.zpict_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zpict_1.Size = New System.Drawing.Size(253, 101)
        Me.zpict_1.TabIndex = 262
        Me.zpict_1.TabStop = True
        '
        'zlbl_62
        '
        Me.zlbl_62.AutoSize = True
        Me.zlbl_62.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_62.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_62.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zlbl_62.Location = New System.Drawing.Point(14, 273)
        Me.zlbl_62.Name = "zlbl_62"
        Me.zlbl_62.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_62.Size = New System.Drawing.Size(109, 16)
        Me.zlbl_62.TabIndex = 292
        Me.zlbl_62.Text = "dV/dlog(Ic) (V)"
        Me.zlbl_62.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_56
        '
        Me.zlbl_56.AutoSize = True
        Me.zlbl_56.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_56.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_56.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zlbl_56.Location = New System.Drawing.Point(23, 169)
        Me.zlbl_56.Name = "zlbl_56"
        Me.zlbl_56.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_56.Size = New System.Drawing.Size(100, 16)
        Me.zlbl_56.TabIndex = 291
        Me.zlbl_56.Text = "dC/dT (%/°C)"
        Me.zlbl_56.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_59
        '
        Me.zlbl_59.AutoSize = True
        Me.zlbl_59.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_59.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_59.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zlbl_59.Location = New System.Drawing.Point(10, 193)
        Me.zlbl_59.Name = "zlbl_59"
        Me.zlbl_59.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_59.Size = New System.Drawing.Size(113, 16)
        Me.zlbl_59.TabIndex = 290
        Me.zlbl_59.Text = "dC/dlog(Ic) (%)"
        Me.zlbl_59.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_61
        '
        Me.zlbl_61.AutoSize = True
        Me.zlbl_61.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_61.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_61.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zlbl_61.Location = New System.Drawing.Point(26, 249)
        Me.zlbl_61.Name = "zlbl_61"
        Me.zlbl_61.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_61.Size = New System.Drawing.Size(96, 16)
        Me.zlbl_61.TabIndex = 289
        Me.zlbl_61.Text = "dV/dT (V/°C)"
        Me.zlbl_61.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_60
        '
        Me.zlbl_60.AutoSize = True
        Me.zlbl_60.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_60.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_60.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zlbl_60.Location = New System.Drawing.Point(39, 225)
        Me.zlbl_60.Name = "zlbl_60"
        Me.zlbl_60.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_60.Size = New System.Drawing.Size(83, 16)
        Me.zlbl_60.TabIndex = 288
        Me.zlbl_60.Text = "Voltage (V)"
        Me.zlbl_60.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_57
        '
        Me.zlbl_57.AutoSize = True
        Me.zlbl_57.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_57.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_57.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zlbl_57.Location = New System.Drawing.Point(38, 145)
        Me.zlbl_57.Name = "zlbl_57"
        Me.zlbl_57.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_57.Size = New System.Drawing.Size(84, 16)
        Me.zlbl_57.TabIndex = 287
        Me.zlbl_57.Text = "Charge (%)"
        Me.zlbl_57.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zpict_4
        '
        Me.zpict_4.BackColor = System.Drawing.SystemColors.Control
        Me.zpict_4.Controls.Add(Me.bat_ch_opt_temp)
        Me.zpict_4.Controls.Add(Me.bat_ch_opt_courant)
        Me.zpict_4.Cursor = System.Windows.Forms.Cursors.Default
        Me.zpict_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zpict_4.Location = New System.Drawing.Point(550, 18)
        Me.zpict_4.Name = "zpict_4"
        Me.zpict_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zpict_4.Size = New System.Drawing.Size(225, 32)
        Me.zpict_4.TabIndex = 206
        Me.zpict_4.TabStop = True
        '
        'bat_ch_opt_temp
        '
        Me.bat_ch_opt_temp.BackColor = System.Drawing.SystemColors.Control
        Me.bat_ch_opt_temp.Cursor = System.Windows.Forms.Cursors.Default
        Me.bat_ch_opt_temp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.bat_ch_opt_temp.Location = New System.Drawing.Point(100, 3)
        Me.bat_ch_opt_temp.Name = "bat_ch_opt_temp"
        Me.bat_ch_opt_temp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_ch_opt_temp.Size = New System.Drawing.Size(114, 24)
        Me.bat_ch_opt_temp.TabIndex = 210
        Me.bat_ch_opt_temp.TabStop = True
        Me.bat_ch_opt_temp.Text = "Temperature"
        Me.bat_ch_opt_temp.UseVisualStyleBackColor = False
        '
        'bat_ch_opt_courant
        '
        Me.bat_ch_opt_courant.BackColor = System.Drawing.SystemColors.Control
        Me.bat_ch_opt_courant.Cursor = System.Windows.Forms.Cursors.Default
        Me.bat_ch_opt_courant.ForeColor = System.Drawing.SystemColors.ControlText
        Me.bat_ch_opt_courant.Location = New System.Drawing.Point(3, 3)
        Me.bat_ch_opt_courant.Name = "bat_ch_opt_courant"
        Me.bat_ch_opt_courant.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_ch_opt_courant.Size = New System.Drawing.Size(91, 24)
        Me.bat_ch_opt_courant.TabIndex = 209
        Me.bat_ch_opt_courant.TabStop = True
        Me.bat_ch_opt_courant.Text = "Current"
        Me.bat_ch_opt_courant.UseVisualStyleBackColor = False
        '
        'bat_onglet_batterie_TabPage1
        '
        Me.bat_onglet_batterie_TabPage1.BackColor = System.Drawing.Color.Transparent
        Me.bat_onglet_batterie_TabPage1.Controls.Add(Me.bat_decharge_graph)
        Me.bat_onglet_batterie_TabPage1.Controls.Add(Me.bat_frm_decharge)
        Me.bat_onglet_batterie_TabPage1.Controls.Add(Me.zpict_3)
        Me.bat_onglet_batterie_TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.bat_onglet_batterie_TabPage1.Name = "bat_onglet_batterie_TabPage1"
        Me.bat_onglet_batterie_TabPage1.Size = New System.Drawing.Size(969, 354)
        Me.bat_onglet_batterie_TabPage1.TabIndex = 1
        Me.bat_onglet_batterie_TabPage1.Text = "Discharge"
        Me.bat_onglet_batterie_TabPage1.UseVisualStyleBackColor = True
        '
        'bat_decharge_graph
        '
        Me.bat_decharge_graph.BackColor = System.Drawing.Color.PaleTurquoise
        Me.bat_decharge_graph.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.bat_decharge_graph.BorderlineColor = System.Drawing.Color.DarkCyan
        Me.bat_decharge_graph.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.bat_decharge_graph.BorderlineWidth = 2
        ChartArea16.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount
        ChartArea16.AxisX.Minimum = 0R
        ChartArea16.AxisY.IsStartedFromZero = False
        ChartArea16.BackColor = System.Drawing.Color.Transparent
        ChartArea16.Name = "ChartArea1"
        Me.bat_decharge_graph.ChartAreas.Add(ChartArea16)
        Legend13.BackColor = System.Drawing.Color.PaleTurquoise
        Legend13.Name = "Legend1"
        Me.bat_decharge_graph.Legends.Add(Legend13)
        Me.bat_decharge_graph.Location = New System.Drawing.Point(375, 69)
        Me.bat_decharge_graph.Margin = New System.Windows.Forms.Padding(2)
        Me.bat_decharge_graph.Name = "bat_decharge_graph"
        Series16.ChartArea = "ChartArea1"
        Series16.Legend = "Legend1"
        Series16.Name = "Series1"
        Me.bat_decharge_graph.Series.Add(Series16)
        Me.bat_decharge_graph.Size = New System.Drawing.Size(568, 268)
        Me.bat_decharge_graph.TabIndex = 263
        Me.bat_decharge_graph.Text = "Chart1"
        Title16.Name = "Title1"
        Title16.Text = "Discharge characteristics at 25°C"
        Me.bat_decharge_graph.Titles.Add(Title16)
        '
        'bat_frm_decharge
        '
        Me.bat_frm_decharge.BackColor = System.Drawing.SystemColors.Control
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d100_5)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d100_1)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d100_2)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d100_4)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d100_3)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d100_0)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d2_0)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d1_0)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d0_0)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d2_3)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d1_3)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d0_3)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d2_4)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d1_4)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d0_4)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d0_2)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d1_2)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d2_2)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d0_1)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d1_1)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d2_1)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d0_5)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d1_5)
        Me.bat_frm_decharge.Controls.Add(Me.bat_txt_d2_5)
        Me.bat_frm_decharge.Controls.Add(Me.zpict_2)
        Me.bat_frm_decharge.Controls.Add(Me.zlbl_7)
        Me.bat_frm_decharge.Controls.Add(Me.zlbl_9)
        Me.bat_frm_decharge.Controls.Add(Me.zlbl_18)
        Me.bat_frm_decharge.Controls.Add(Me.zlbl_19)
        Me.bat_frm_decharge.Controls.Add(Me.zlbl_20)
        Me.bat_frm_decharge.Controls.Add(Me.zlbl_21)
        Me.bat_frm_decharge.ForeColor = System.Drawing.SystemColors.ControlText
        Me.bat_frm_decharge.Location = New System.Drawing.Point(12, 32)
        Me.bat_frm_decharge.Name = "bat_frm_decharge"
        Me.bat_frm_decharge.Padding = New System.Windows.Forms.Padding(0)
        Me.bat_frm_decharge.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_frm_decharge.Size = New System.Drawing.Size(341, 305)
        Me.bat_frm_decharge.TabIndex = 229
        Me.bat_frm_decharge.TabStop = False
        Me.bat_frm_decharge.Text = "Discharge characteristics at 25°C and Cnom"
        '
        'bat_txt_d100_5
        '
        Me.bat_txt_d100_5.AcceptsReturn = True
        Me.bat_txt_d100_5.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d100_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d100_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d100_5.Location = New System.Drawing.Point(278, 267)
        Me.bat_txt_d100_5.MaxLength = 0
        Me.bat_txt_d100_5.Name = "bat_txt_d100_5"
        Me.bat_txt_d100_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d100_5.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d100_5.TabIndex = 254
        Me.bat_txt_d100_5.Tag = "1"
        Me.bat_txt_d100_5.Text = "-0.01"
        Me.bat_txt_d100_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d100_1
        '
        Me.bat_txt_d100_1.AcceptsReturn = True
        Me.bat_txt_d100_1.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d100_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d100_1.Enabled = False
        Me.bat_txt_d100_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d100_1.Location = New System.Drawing.Point(278, 163)
        Me.bat_txt_d100_1.MaxLength = 0
        Me.bat_txt_d100_1.Name = "bat_txt_d100_1"
        Me.bat_txt_d100_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d100_1.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d100_1.TabIndex = 253
        Me.bat_txt_d100_1.Tag = "1"
        Me.bat_txt_d100_1.Text = "0"
        Me.bat_txt_d100_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d100_2
        '
        Me.bat_txt_d100_2.AcceptsReturn = True
        Me.bat_txt_d100_2.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d100_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d100_2.Enabled = False
        Me.bat_txt_d100_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d100_2.Location = New System.Drawing.Point(278, 187)
        Me.bat_txt_d100_2.MaxLength = 0
        Me.bat_txt_d100_2.Name = "bat_txt_d100_2"
        Me.bat_txt_d100_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d100_2.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d100_2.TabIndex = 252
        Me.bat_txt_d100_2.Tag = "1"
        Me.bat_txt_d100_2.Text = "0"
        Me.bat_txt_d100_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d100_4
        '
        Me.bat_txt_d100_4.AcceptsReturn = True
        Me.bat_txt_d100_4.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d100_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d100_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d100_4.Location = New System.Drawing.Point(278, 243)
        Me.bat_txt_d100_4.MaxLength = 0
        Me.bat_txt_d100_4.Name = "bat_txt_d100_4"
        Me.bat_txt_d100_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d100_4.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d100_4.TabIndex = 251
        Me.bat_txt_d100_4.Tag = "1"
        Me.bat_txt_d100_4.Text = "0.001"
        Me.bat_txt_d100_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d100_3
        '
        Me.bat_txt_d100_3.AcceptsReturn = True
        Me.bat_txt_d100_3.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d100_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d100_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d100_3.Location = New System.Drawing.Point(278, 219)
        Me.bat_txt_d100_3.MaxLength = 0
        Me.bat_txt_d100_3.Name = "bat_txt_d100_3"
        Me.bat_txt_d100_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d100_3.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d100_3.TabIndex = 250
        Me.bat_txt_d100_3.Tag = "1"
        Me.bat_txt_d100_3.Text = "1.000"
        Me.bat_txt_d100_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d100_0
        '
        Me.bat_txt_d100_0.AcceptsReturn = True
        Me.bat_txt_d100_0.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d100_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d100_0.Enabled = False
        Me.bat_txt_d100_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d100_0.Location = New System.Drawing.Point(278, 139)
        Me.bat_txt_d100_0.MaxLength = 0
        Me.bat_txt_d100_0.Name = "bat_txt_d100_0"
        Me.bat_txt_d100_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d100_0.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d100_0.TabIndex = 249
        Me.bat_txt_d100_0.Tag = "1"
        Me.bat_txt_d100_0.Text = "100"
        Me.bat_txt_d100_0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d2_0
        '
        Me.bat_txt_d2_0.AcceptsReturn = True
        Me.bat_txt_d2_0.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d2_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d2_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d2_0.Location = New System.Drawing.Point(230, 139)
        Me.bat_txt_d2_0.MaxLength = 0
        Me.bat_txt_d2_0.Name = "bat_txt_d2_0"
        Me.bat_txt_d2_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d2_0.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d2_0.TabIndex = 248
        Me.bat_txt_d2_0.Tag = "1"
        Me.bat_txt_d2_0.Text = "85"
        Me.bat_txt_d2_0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d1_0
        '
        Me.bat_txt_d1_0.AcceptsReturn = True
        Me.bat_txt_d1_0.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d1_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d1_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d1_0.Location = New System.Drawing.Point(182, 139)
        Me.bat_txt_d1_0.MaxLength = 0
        Me.bat_txt_d1_0.Name = "bat_txt_d1_0"
        Me.bat_txt_d1_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d1_0.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d1_0.TabIndex = 247
        Me.bat_txt_d1_0.Tag = "1"
        Me.bat_txt_d1_0.Text = "15"
        Me.bat_txt_d1_0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d0_0
        '
        Me.bat_txt_d0_0.AcceptsReturn = True
        Me.bat_txt_d0_0.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d0_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d0_0.Enabled = False
        Me.bat_txt_d0_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d0_0.Location = New System.Drawing.Point(134, 139)
        Me.bat_txt_d0_0.MaxLength = 0
        Me.bat_txt_d0_0.Name = "bat_txt_d0_0"
        Me.bat_txt_d0_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d0_0.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d0_0.TabIndex = 246
        Me.bat_txt_d0_0.Tag = "1"
        Me.bat_txt_d0_0.Text = "0"
        Me.bat_txt_d0_0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d2_3
        '
        Me.bat_txt_d2_3.AcceptsReturn = True
        Me.bat_txt_d2_3.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d2_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d2_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d2_3.Location = New System.Drawing.Point(230, 219)
        Me.bat_txt_d2_3.MaxLength = 0
        Me.bat_txt_d2_3.Name = "bat_txt_d2_3"
        Me.bat_txt_d2_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d2_3.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d2_3.TabIndex = 245
        Me.bat_txt_d2_3.Tag = "1"
        Me.bat_txt_d2_3.Text = "1.210"
        Me.bat_txt_d2_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d1_3
        '
        Me.bat_txt_d1_3.AcceptsReturn = True
        Me.bat_txt_d1_3.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d1_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d1_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d1_3.Location = New System.Drawing.Point(182, 219)
        Me.bat_txt_d1_3.MaxLength = 0
        Me.bat_txt_d1_3.Name = "bat_txt_d1_3"
        Me.bat_txt_d1_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d1_3.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d1_3.TabIndex = 244
        Me.bat_txt_d1_3.Tag = "1"
        Me.bat_txt_d1_3.Text = "1.250"
        Me.bat_txt_d1_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d0_3
        '
        Me.bat_txt_d0_3.AcceptsReturn = True
        Me.bat_txt_d0_3.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d0_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d0_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d0_3.Location = New System.Drawing.Point(134, 219)
        Me.bat_txt_d0_3.MaxLength = 0
        Me.bat_txt_d0_3.Name = "bat_txt_d0_3"
        Me.bat_txt_d0_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d0_3.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d0_3.TabIndex = 243
        Me.bat_txt_d0_3.Tag = "1"
        Me.bat_txt_d0_3.Text = "1.300"
        Me.bat_txt_d0_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d2_4
        '
        Me.bat_txt_d2_4.AcceptsReturn = True
        Me.bat_txt_d2_4.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d2_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d2_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d2_4.Location = New System.Drawing.Point(230, 243)
        Me.bat_txt_d2_4.MaxLength = 0
        Me.bat_txt_d2_4.Name = "bat_txt_d2_4"
        Me.bat_txt_d2_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d2_4.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d2_4.TabIndex = 242
        Me.bat_txt_d2_4.Tag = "1"
        Me.bat_txt_d2_4.Text = "0.001"
        Me.bat_txt_d2_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d1_4
        '
        Me.bat_txt_d1_4.AcceptsReturn = True
        Me.bat_txt_d1_4.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d1_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d1_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d1_4.Location = New System.Drawing.Point(182, 243)
        Me.bat_txt_d1_4.MaxLength = 0
        Me.bat_txt_d1_4.Name = "bat_txt_d1_4"
        Me.bat_txt_d1_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d1_4.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d1_4.TabIndex = 241
        Me.bat_txt_d1_4.Tag = "1"
        Me.bat_txt_d1_4.Text = "0.001"
        Me.bat_txt_d1_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d0_4
        '
        Me.bat_txt_d0_4.AcceptsReturn = True
        Me.bat_txt_d0_4.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d0_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d0_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d0_4.Location = New System.Drawing.Point(134, 243)
        Me.bat_txt_d0_4.MaxLength = 0
        Me.bat_txt_d0_4.Name = "bat_txt_d0_4"
        Me.bat_txt_d0_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d0_4.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d0_4.TabIndex = 240
        Me.bat_txt_d0_4.Tag = "1"
        Me.bat_txt_d0_4.Text = "0"
        Me.bat_txt_d0_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d0_2
        '
        Me.bat_txt_d0_2.AcceptsReturn = True
        Me.bat_txt_d0_2.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d0_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d0_2.Enabled = False
        Me.bat_txt_d0_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d0_2.Location = New System.Drawing.Point(134, 187)
        Me.bat_txt_d0_2.MaxLength = 0
        Me.bat_txt_d0_2.Name = "bat_txt_d0_2"
        Me.bat_txt_d0_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d0_2.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d0_2.TabIndex = 239
        Me.bat_txt_d0_2.Tag = "1"
        Me.bat_txt_d0_2.Text = "0"
        Me.bat_txt_d0_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d1_2
        '
        Me.bat_txt_d1_2.AcceptsReturn = True
        Me.bat_txt_d1_2.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d1_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d1_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d1_2.Location = New System.Drawing.Point(182, 187)
        Me.bat_txt_d1_2.MaxLength = 0
        Me.bat_txt_d1_2.Name = "bat_txt_d1_2"
        Me.bat_txt_d1_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d1_2.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d1_2.TabIndex = 238
        Me.bat_txt_d1_2.Tag = "1"
        Me.bat_txt_d1_2.Text = "0"
        Me.bat_txt_d1_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d2_2
        '
        Me.bat_txt_d2_2.AcceptsReturn = True
        Me.bat_txt_d2_2.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d2_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d2_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d2_2.Location = New System.Drawing.Point(230, 187)
        Me.bat_txt_d2_2.MaxLength = 0
        Me.bat_txt_d2_2.Name = "bat_txt_d2_2"
        Me.bat_txt_d2_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d2_2.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d2_2.TabIndex = 237
        Me.bat_txt_d2_2.Tag = "1"
        Me.bat_txt_d2_2.Text = "0"
        Me.bat_txt_d2_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d0_1
        '
        Me.bat_txt_d0_1.AcceptsReturn = True
        Me.bat_txt_d0_1.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d0_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d0_1.Enabled = False
        Me.bat_txt_d0_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d0_1.Location = New System.Drawing.Point(134, 163)
        Me.bat_txt_d0_1.MaxLength = 0
        Me.bat_txt_d0_1.Name = "bat_txt_d0_1"
        Me.bat_txt_d0_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d0_1.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d0_1.TabIndex = 236
        Me.bat_txt_d0_1.Tag = "1"
        Me.bat_txt_d0_1.Text = "0"
        Me.bat_txt_d0_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d1_1
        '
        Me.bat_txt_d1_1.AcceptsReturn = True
        Me.bat_txt_d1_1.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d1_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d1_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d1_1.Location = New System.Drawing.Point(182, 163)
        Me.bat_txt_d1_1.MaxLength = 0
        Me.bat_txt_d1_1.Name = "bat_txt_d1_1"
        Me.bat_txt_d1_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d1_1.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d1_1.TabIndex = 235
        Me.bat_txt_d1_1.Tag = "1"
        Me.bat_txt_d1_1.Text = "0"
        Me.bat_txt_d1_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d2_1
        '
        Me.bat_txt_d2_1.AcceptsReturn = True
        Me.bat_txt_d2_1.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d2_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d2_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d2_1.Location = New System.Drawing.Point(230, 163)
        Me.bat_txt_d2_1.MaxLength = 0
        Me.bat_txt_d2_1.Name = "bat_txt_d2_1"
        Me.bat_txt_d2_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d2_1.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d2_1.TabIndex = 234
        Me.bat_txt_d2_1.Tag = "1"
        Me.bat_txt_d2_1.Text = "0"
        Me.bat_txt_d2_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d0_5
        '
        Me.bat_txt_d0_5.AcceptsReturn = True
        Me.bat_txt_d0_5.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d0_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d0_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d0_5.Location = New System.Drawing.Point(134, 267)
        Me.bat_txt_d0_5.MaxLength = 0
        Me.bat_txt_d0_5.Name = "bat_txt_d0_5"
        Me.bat_txt_d0_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d0_5.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d0_5.TabIndex = 233
        Me.bat_txt_d0_5.Tag = "1"
        Me.bat_txt_d0_5.Text = "0"
        Me.bat_txt_d0_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d1_5
        '
        Me.bat_txt_d1_5.AcceptsReturn = True
        Me.bat_txt_d1_5.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d1_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d1_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d1_5.Location = New System.Drawing.Point(182, 267)
        Me.bat_txt_d1_5.MaxLength = 0
        Me.bat_txt_d1_5.Name = "bat_txt_d1_5"
        Me.bat_txt_d1_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d1_5.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d1_5.TabIndex = 232
        Me.bat_txt_d1_5.Tag = "1"
        Me.bat_txt_d1_5.Text = "-0.01"
        Me.bat_txt_d1_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_d2_5
        '
        Me.bat_txt_d2_5.AcceptsReturn = True
        Me.bat_txt_d2_5.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_d2_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_d2_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_d2_5.Location = New System.Drawing.Point(230, 267)
        Me.bat_txt_d2_5.MaxLength = 0
        Me.bat_txt_d2_5.Name = "bat_txt_d2_5"
        Me.bat_txt_d2_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_d2_5.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_d2_5.TabIndex = 231
        Me.bat_txt_d2_5.Tag = "1"
        Me.bat_txt_d2_5.Text = "-0.01"
        Me.bat_txt_d2_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'zpict_2
        '
        Me.zpict_2.BackColor = System.Drawing.SystemColors.Window
        Me.zpict_2.BackgroundImage = CType(resources.GetObject("zpict_2.BackgroundImage"), System.Drawing.Image)
        Me.zpict_2.Cursor = System.Windows.Forms.Cursors.Default
        Me.zpict_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zpict_2.Location = New System.Drawing.Point(74, 29)
        Me.zpict_2.Name = "zpict_2"
        Me.zpict_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zpict_2.Size = New System.Drawing.Size(250, 99)
        Me.zpict_2.TabIndex = 230
        Me.zpict_2.TabStop = True
        '
        'zlbl_7
        '
        Me.zlbl_7.AutoSize = True
        Me.zlbl_7.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_7.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_7.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zlbl_7.Location = New System.Drawing.Point(44, 142)
        Me.zlbl_7.Name = "zlbl_7"
        Me.zlbl_7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_7.Size = New System.Drawing.Size(84, 16)
        Me.zlbl_7.TabIndex = 260
        Me.zlbl_7.Text = "Charge (%)"
        Me.zlbl_7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_9
        '
        Me.zlbl_9.AutoSize = True
        Me.zlbl_9.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_9.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_9.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zlbl_9.Location = New System.Drawing.Point(43, 222)
        Me.zlbl_9.Name = "zlbl_9"
        Me.zlbl_9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_9.Size = New System.Drawing.Size(83, 16)
        Me.zlbl_9.TabIndex = 259
        Me.zlbl_9.Text = "Voltage (V)"
        Me.zlbl_9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_18
        '
        Me.zlbl_18.AutoSize = True
        Me.zlbl_18.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_18.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_18.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zlbl_18.Location = New System.Drawing.Point(32, 246)
        Me.zlbl_18.Name = "zlbl_18"
        Me.zlbl_18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_18.Size = New System.Drawing.Size(96, 16)
        Me.zlbl_18.TabIndex = 258
        Me.zlbl_18.Text = "dV/dT (V/°C)"
        Me.zlbl_18.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_19
        '
        Me.zlbl_19.AutoSize = True
        Me.zlbl_19.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_19.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_19.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zlbl_19.Location = New System.Drawing.Point(15, 190)
        Me.zlbl_19.Name = "zlbl_19"
        Me.zlbl_19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_19.Size = New System.Drawing.Size(113, 16)
        Me.zlbl_19.TabIndex = 257
        Me.zlbl_19.Text = "dC/dlog(Ic) (%)"
        Me.zlbl_19.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_20
        '
        Me.zlbl_20.AutoSize = True
        Me.zlbl_20.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_20.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_20.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zlbl_20.Location = New System.Drawing.Point(28, 166)
        Me.zlbl_20.Name = "zlbl_20"
        Me.zlbl_20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_20.Size = New System.Drawing.Size(100, 16)
        Me.zlbl_20.TabIndex = 256
        Me.zlbl_20.Text = "dC/dT (%/°C)"
        Me.zlbl_20.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_21
        '
        Me.zlbl_21.AutoSize = True
        Me.zlbl_21.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_21.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_21.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zlbl_21.Location = New System.Drawing.Point(19, 270)
        Me.zlbl_21.Name = "zlbl_21"
        Me.zlbl_21.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_21.Size = New System.Drawing.Size(109, 16)
        Me.zlbl_21.TabIndex = 255
        Me.zlbl_21.Text = "dV/dlog(Ic) (V)"
        Me.zlbl_21.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zpict_3
        '
        Me.zpict_3.BackColor = System.Drawing.SystemColors.Control
        Me.zpict_3.Controls.Add(Me.bat_dch_opt_temp)
        Me.zpict_3.Controls.Add(Me.bat_dch_opt_courant)
        Me.zpict_3.Cursor = System.Windows.Forms.Cursors.Default
        Me.zpict_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.zpict_3.Location = New System.Drawing.Point(528, 19)
        Me.zpict_3.Name = "zpict_3"
        Me.zpict_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zpict_3.Size = New System.Drawing.Size(220, 34)
        Me.zpict_3.TabIndex = 209
        Me.zpict_3.TabStop = True
        '
        'bat_dch_opt_temp
        '
        Me.bat_dch_opt_temp.BackColor = System.Drawing.SystemColors.Control
        Me.bat_dch_opt_temp.Cursor = System.Windows.Forms.Cursors.Default
        Me.bat_dch_opt_temp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.bat_dch_opt_temp.Location = New System.Drawing.Point(83, 6)
        Me.bat_dch_opt_temp.Name = "bat_dch_opt_temp"
        Me.bat_dch_opt_temp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_dch_opt_temp.Size = New System.Drawing.Size(130, 23)
        Me.bat_dch_opt_temp.TabIndex = 212
        Me.bat_dch_opt_temp.TabStop = True
        Me.bat_dch_opt_temp.Text = "Temperature"
        Me.bat_dch_opt_temp.UseVisualStyleBackColor = False
        '
        'bat_dch_opt_courant
        '
        Me.bat_dch_opt_courant.BackColor = System.Drawing.SystemColors.Control
        Me.bat_dch_opt_courant.Checked = True
        Me.bat_dch_opt_courant.Cursor = System.Windows.Forms.Cursors.Default
        Me.bat_dch_opt_courant.ForeColor = System.Drawing.SystemColors.ControlText
        Me.bat_dch_opt_courant.Location = New System.Drawing.Point(3, 4)
        Me.bat_dch_opt_courant.Name = "bat_dch_opt_courant"
        Me.bat_dch_opt_courant.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_dch_opt_courant.Size = New System.Drawing.Size(81, 27)
        Me.bat_dch_opt_courant.TabIndex = 211
        Me.bat_dch_opt_courant.TabStop = True
        Me.bat_dch_opt_courant.Text = "Current"
        Me.bat_dch_opt_courant.UseVisualStyleBackColor = False
        '
        'bat_onglet_batterie_TabPage2
        '
        Me.bat_onglet_batterie_TabPage2.BackColor = System.Drawing.Color.Transparent
        Me.bat_onglet_batterie_TabPage2.Controls.Add(Me.bat_auto_graph)
        Me.bat_onglet_batterie_TabPage2.Controls.Add(Me.zfra_2)
        Me.bat_onglet_batterie_TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.bat_onglet_batterie_TabPage2.Name = "bat_onglet_batterie_TabPage2"
        Me.bat_onglet_batterie_TabPage2.Size = New System.Drawing.Size(969, 354)
        Me.bat_onglet_batterie_TabPage2.TabIndex = 2
        Me.bat_onglet_batterie_TabPage2.Text = "Self-discharge"
        Me.bat_onglet_batterie_TabPage2.UseVisualStyleBackColor = True
        '
        'bat_auto_graph
        '
        Me.bat_auto_graph.BackColor = System.Drawing.Color.PaleTurquoise
        Me.bat_auto_graph.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.bat_auto_graph.BorderlineColor = System.Drawing.Color.DarkCyan
        Me.bat_auto_graph.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.bat_auto_graph.BorderlineWidth = 2
        ChartArea17.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount
        ChartArea17.AxisX.Minimum = 0R
        ChartArea17.AxisY.IsStartedFromZero = False
        ChartArea17.BackColor = System.Drawing.Color.Transparent
        ChartArea17.Name = "ChartArea1"
        Me.bat_auto_graph.ChartAreas.Add(ChartArea17)
        Legend14.BackColor = System.Drawing.Color.PaleTurquoise
        Legend14.Name = "Legend1"
        Me.bat_auto_graph.Legends.Add(Legend14)
        Me.bat_auto_graph.Location = New System.Drawing.Point(316, 34)
        Me.bat_auto_graph.Margin = New System.Windows.Forms.Padding(2)
        Me.bat_auto_graph.Name = "bat_auto_graph"
        Series17.ChartArea = "ChartArea1"
        Series17.Legend = "Legend1"
        Series17.Name = "Series1"
        Me.bat_auto_graph.Series.Add(Series17)
        Me.bat_auto_graph.Size = New System.Drawing.Size(618, 292)
        Me.bat_auto_graph.TabIndex = 263
        Me.bat_auto_graph.Text = "Chart1"
        Title17.Name = "Title1"
        Me.bat_auto_graph.Titles.Add(Title17)
        '
        'zfra_2
        '
        Me.zfra_2.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_2.Controls.Add(Me.bat_txt_tc0)
        Me.zfra_2.Controls.Add(Me.bat_txt_tc50)
        Me.zfra_2.Controls.Add(Me.zlbl_16)
        Me.zfra_2.Controls.Add(Me.zlbl_17)
        Me.zfra_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zfra_2.Location = New System.Drawing.Point(59, 104)
        Me.zfra_2.Name = "zfra_2"
        Me.zfra_2.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_2.Size = New System.Drawing.Size(207, 83)
        Me.zfra_2.TabIndex = 212
        Me.zfra_2.TabStop = False
        Me.zfra_2.Text = "Time constant"
        '
        'bat_txt_tc0
        '
        Me.bat_txt_tc0.AcceptsReturn = True
        Me.bat_txt_tc0.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_tc0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_tc0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_tc0.Location = New System.Drawing.Point(139, 25)
        Me.bat_txt_tc0.MaxLength = 0
        Me.bat_txt_tc0.Name = "bat_txt_tc0"
        Me.bat_txt_tc0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_tc0.Size = New System.Drawing.Size(53, 23)
        Me.bat_txt_tc0.TabIndex = 214
        Me.bat_txt_tc0.Tag = "1"
        Me.bat_txt_tc0.Text = "100"
        '
        'bat_txt_tc50
        '
        Me.bat_txt_tc50.AcceptsReturn = True
        Me.bat_txt_tc50.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_tc50.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_tc50.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_tc50.Location = New System.Drawing.Point(139, 53)
        Me.bat_txt_tc50.MaxLength = 0
        Me.bat_txt_tc50.Name = "bat_txt_tc50"
        Me.bat_txt_tc50.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_tc50.Size = New System.Drawing.Size(53, 23)
        Me.bat_txt_tc50.TabIndex = 213
        Me.bat_txt_tc50.Tag = "1"
        Me.bat_txt_tc50.Text = "15"
        '
        'zlbl_16
        '
        Me.zlbl_16.AutoSize = True
        Me.zlbl_16.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_16.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_16.Location = New System.Drawing.Point(15, 28)
        Me.zlbl_16.Name = "zlbl_16"
        Me.zlbl_16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_16.Size = New System.Drawing.Size(118, 16)
        Me.zlbl_16.TabIndex = 216
        Me.zlbl_16.Text = "tc at 0°C (days)"
        '
        'zlbl_17
        '
        Me.zlbl_17.AutoSize = True
        Me.zlbl_17.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_17.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_17.Location = New System.Drawing.Point(7, 56)
        Me.zlbl_17.Name = "zlbl_17"
        Me.zlbl_17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_17.Size = New System.Drawing.Size(126, 16)
        Me.zlbl_17.TabIndex = 215
        Me.zlbl_17.Text = "tc at 50°C (days)"
        '
        'bat_onglet_batterie_TabPage3
        '
        Me.bat_onglet_batterie_TabPage3.BackColor = System.Drawing.Color.Transparent
        Me.bat_onglet_batterie_TabPage3.Controls.Add(Me.bat_cdispo_graph)
        Me.bat_onglet_batterie_TabPage3.Controls.Add(Me.zfra_5)
        Me.bat_onglet_batterie_TabPage3.Controls.Add(Me.zfra_7)
        Me.bat_onglet_batterie_TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.bat_onglet_batterie_TabPage3.Name = "bat_onglet_batterie_TabPage3"
        Me.bat_onglet_batterie_TabPage3.Size = New System.Drawing.Size(969, 354)
        Me.bat_onglet_batterie_TabPage3.TabIndex = 3
        Me.bat_onglet_batterie_TabPage3.Text = "Available capacity"
        Me.bat_onglet_batterie_TabPage3.UseVisualStyleBackColor = True
        '
        'bat_cdispo_graph
        '
        Me.bat_cdispo_graph.BackColor = System.Drawing.Color.PaleTurquoise
        Me.bat_cdispo_graph.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.bat_cdispo_graph.BorderlineColor = System.Drawing.Color.DarkCyan
        Me.bat_cdispo_graph.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.bat_cdispo_graph.BorderlineWidth = 2
        ChartArea18.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount
        ChartArea18.AxisX.IsStartedFromZero = False
        ChartArea18.AxisY.IsStartedFromZero = False
        ChartArea18.BackColor = System.Drawing.Color.Transparent
        ChartArea18.Name = "ChartArea1"
        Me.bat_cdispo_graph.ChartAreas.Add(ChartArea18)
        Legend15.BackColor = System.Drawing.Color.PaleTurquoise
        Legend15.Name = "Legend1"
        Me.bat_cdispo_graph.Legends.Add(Legend15)
        Me.bat_cdispo_graph.Location = New System.Drawing.Point(332, 23)
        Me.bat_cdispo_graph.Margin = New System.Windows.Forms.Padding(2)
        Me.bat_cdispo_graph.Name = "bat_cdispo_graph"
        Series18.ChartArea = "ChartArea1"
        Series18.Legend = "Legend1"
        Series18.Name = "Series1"
        Me.bat_cdispo_graph.Series.Add(Series18)
        Me.bat_cdispo_graph.Size = New System.Drawing.Size(605, 308)
        Me.bat_cdispo_graph.TabIndex = 264
        Me.bat_cdispo_graph.Text = "Chart1"
        Title18.Name = "Title1"
        Me.bat_cdispo_graph.Titles.Add(Title18)
        '
        'zfra_5
        '
        Me.zfra_5.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_5.Controls.Add(Me.bat_txt_c10)
        Me.zfra_5.Controls.Add(Me.bat_txt_cmax)
        Me.zfra_5.Controls.Add(Me.zlbl_12)
        Me.zfra_5.Controls.Add(Me.zlbl_11)
        Me.zfra_5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zfra_5.Location = New System.Drawing.Point(32, 48)
        Me.zfra_5.Name = "zfra_5"
        Me.zfra_5.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_5.Size = New System.Drawing.Size(275, 89)
        Me.zfra_5.TabIndex = 217
        Me.zfra_5.TabStop = False
        Me.zfra_5.Text = "Effect of discharge current"
        '
        'bat_txt_c10
        '
        Me.bat_txt_c10.AcceptsReturn = True
        Me.bat_txt_c10.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_c10.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_c10.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_c10.Location = New System.Drawing.Point(217, 54)
        Me.bat_txt_c10.MaxLength = 0
        Me.bat_txt_c10.Name = "bat_txt_c10"
        Me.bat_txt_c10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_c10.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_c10.TabIndex = 219
        Me.bat_txt_c10.Tag = "1"
        Me.bat_txt_c10.Text = "0.6"
        Me.bat_txt_c10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_cmax
        '
        Me.bat_txt_cmax.AcceptsReturn = True
        Me.bat_txt_cmax.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_cmax.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_cmax.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_cmax.Location = New System.Drawing.Point(217, 25)
        Me.bat_txt_cmax.MaxLength = 0
        Me.bat_txt_cmax.Name = "bat_txt_cmax"
        Me.bat_txt_cmax.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_cmax.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_cmax.TabIndex = 218
        Me.bat_txt_cmax.Tag = "1"
        Me.bat_txt_cmax.Text = "1.1"
        Me.bat_txt_cmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'zlbl_12
        '
        Me.zlbl_12.AutoSize = True
        Me.zlbl_12.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_12.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_12.Location = New System.Drawing.Point(8, 57)
        Me.zlbl_12.Name = "zlbl_12"
        Me.zlbl_12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_12.Size = New System.Drawing.Size(203, 16)
        Me.zlbl_12.TabIndex = 221
        Me.zlbl_12.Text = "C/Cnom at discharge 10Cnom"
        Me.zlbl_12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_11
        '
        Me.zlbl_11.AutoSize = True
        Me.zlbl_11.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_11.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_11.Location = New System.Drawing.Point(126, 28)
        Me.zlbl_11.Name = "zlbl_11"
        Me.zlbl_11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_11.Size = New System.Drawing.Size(85, 16)
        Me.zlbl_11.TabIndex = 220
        Me.zlbl_11.Text = "Cmax/Cnom"
        Me.zlbl_11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zfra_7
        '
        Me.zfra_7.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_7.Controls.Add(Me.bat_txt_t99)
        Me.zfra_7.Controls.Add(Me.bat_txt_cmin)
        Me.zfra_7.Controls.Add(Me.bat_txt_tmin)
        Me.zfra_7.Controls.Add(Me.zlbl_15)
        Me.zfra_7.Controls.Add(Me.zlbl_14)
        Me.zfra_7.Controls.Add(Me.zlbl_13)
        Me.zfra_7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zfra_7.Location = New System.Drawing.Point(32, 143)
        Me.zfra_7.Name = "zfra_7"
        Me.zfra_7.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_7.Size = New System.Drawing.Size(275, 117)
        Me.zfra_7.TabIndex = 222
        Me.zfra_7.TabStop = False
        Me.zfra_7.Text = "Effect of temperature"
        '
        'bat_txt_t99
        '
        Me.bat_txt_t99.AcceptsReturn = True
        Me.bat_txt_t99.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_t99.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_t99.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_t99.Location = New System.Drawing.Point(151, 83)
        Me.bat_txt_t99.MaxLength = 0
        Me.bat_txt_t99.Name = "bat_txt_t99"
        Me.bat_txt_t99.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_t99.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_t99.TabIndex = 225
        Me.bat_txt_t99.Tag = "1"
        Me.bat_txt_t99.Text = "50"
        Me.bat_txt_t99.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_cmin
        '
        Me.bat_txt_cmin.AcceptsReturn = True
        Me.bat_txt_cmin.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_cmin.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_cmin.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_cmin.Location = New System.Drawing.Point(151, 54)
        Me.bat_txt_cmin.MaxLength = 0
        Me.bat_txt_cmin.Name = "bat_txt_cmin"
        Me.bat_txt_cmin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_cmin.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_cmin.TabIndex = 224
        Me.bat_txt_cmin.Tag = "1"
        Me.bat_txt_cmin.Text = "30"
        Me.bat_txt_cmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_tmin
        '
        Me.bat_txt_tmin.AcceptsReturn = True
        Me.bat_txt_tmin.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_tmin.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_tmin.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_tmin.Location = New System.Drawing.Point(151, 25)
        Me.bat_txt_tmin.MaxLength = 0
        Me.bat_txt_tmin.Name = "bat_txt_tmin"
        Me.bat_txt_tmin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_tmin.Size = New System.Drawing.Size(45, 23)
        Me.bat_txt_tmin.TabIndex = 223
        Me.bat_txt_tmin.Tag = "1"
        Me.bat_txt_tmin.Text = "-50"
        Me.bat_txt_tmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'zlbl_15
        '
        Me.zlbl_15.AutoSize = True
        Me.zlbl_15.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_15.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_15.Location = New System.Drawing.Point(48, 86)
        Me.zlbl_15.Name = "zlbl_15"
        Me.zlbl_15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_15.Size = New System.Drawing.Size(97, 16)
        Me.zlbl_15.TabIndex = 228
        Me.zlbl_15.Text = "T 99.9% (°C)"
        Me.zlbl_15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_14
        '
        Me.zlbl_14.AutoSize = True
        Me.zlbl_14.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_14.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_14.Location = New System.Drawing.Point(17, 57)
        Me.zlbl_14.Name = "zlbl_14"
        Me.zlbl_14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_14.Size = New System.Drawing.Size(128, 16)
        Me.zlbl_14.TabIndex = 227
        Me.zlbl_14.Text = "Cmin (% C à Tref)"
        Me.zlbl_14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_13
        '
        Me.zlbl_13.AutoSize = True
        Me.zlbl_13.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_13.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_13.Location = New System.Drawing.Point(73, 28)
        Me.zlbl_13.Name = "zlbl_13"
        Me.zlbl_13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_13.Size = New System.Drawing.Size(72, 16)
        Me.zlbl_13.TabIndex = 226
        Me.zlbl_13.Text = "Tmin (°C)"
        Me.zlbl_13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zfra_10
        '
        Me.zfra_10.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_10.Controls.Add(Me.bat_txt_ns)
        Me.zfra_10.Controls.Add(Me.bat_txt_np)
        Me.zfra_10.Controls.Add(Me.zlbl_10)
        Me.zfra_10.Controls.Add(Me.zlbl_4)
        Me.zfra_10.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_10.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_10.Location = New System.Drawing.Point(566, 20)
        Me.zfra_10.Name = "zfra_10"
        Me.zfra_10.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_10.Size = New System.Drawing.Size(228, 64)
        Me.zfra_10.TabIndex = 10
        Me.zfra_10.TabStop = False
        Me.zfra_10.Text = "Cell number"
        '
        'bat_txt_ns
        '
        Me.bat_txt_ns.AcceptsReturn = True
        Me.bat_txt_ns.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_ns.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_ns.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bat_txt_ns.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_ns.Location = New System.Drawing.Point(62, 29)
        Me.bat_txt_ns.MaxLength = 0
        Me.bat_txt_ns.Name = "bat_txt_ns"
        Me.bat_txt_ns.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_ns.Size = New System.Drawing.Size(37, 23)
        Me.bat_txt_ns.TabIndex = 12
        Me.bat_txt_ns.Tag = "1"
        Me.bat_txt_ns.Text = "1"
        Me.bat_txt_ns.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'bat_txt_np
        '
        Me.bat_txt_np.AcceptsReturn = True
        Me.bat_txt_np.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_np.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_np.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bat_txt_np.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_np.Location = New System.Drawing.Point(178, 29)
        Me.bat_txt_np.MaxLength = 0
        Me.bat_txt_np.Name = "bat_txt_np"
        Me.bat_txt_np.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_np.Size = New System.Drawing.Size(37, 23)
        Me.bat_txt_np.TabIndex = 11
        Me.bat_txt_np.Tag = "1"
        Me.bat_txt_np.Text = "1"
        Me.bat_txt_np.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'zlbl_10
        '
        Me.zlbl_10.AutoSize = True
        Me.zlbl_10.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_10.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_10.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_10.Location = New System.Drawing.Point(12, 32)
        Me.zlbl_10.Name = "zlbl_10"
        Me.zlbl_10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_10.Size = New System.Drawing.Size(44, 16)
        Me.zlbl_10.TabIndex = 14
        Me.zlbl_10.Text = "Serial"
        '
        'zlbl_4
        '
        Me.zlbl_4.AutoSize = True
        Me.zlbl_4.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_4.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_4.Location = New System.Drawing.Point(118, 32)
        Me.zlbl_4.Name = "zlbl_4"
        Me.zlbl_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_4.Size = New System.Drawing.Size(54, 16)
        Me.zlbl_4.TabIndex = 13
        Me.zlbl_4.Text = "Parallel"
        '
        'zfra_9
        '
        Me.zfra_9.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_9.Controls.Add(Me.bat_cmb_type)
        Me.zfra_9.Controls.Add(Me.bat_txt_c)
        Me.zfra_9.Controls.Add(Me.zlbl_29)
        Me.zfra_9.Controls.Add(Me.zlbl_8)
        Me.zfra_9.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_9.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_9.Location = New System.Drawing.Point(26, 20)
        Me.zfra_9.Name = "zfra_9"
        Me.zfra_9.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_9.Size = New System.Drawing.Size(521, 64)
        Me.zfra_9.TabIndex = 15
        Me.zfra_9.TabStop = False
        Me.zfra_9.Text = "Cells data"
        '
        'bat_cmb_type
        '
        Me.bat_cmb_type.BackColor = System.Drawing.SystemColors.Window
        Me.bat_cmb_type.Cursor = System.Windows.Forms.Cursors.Default
        Me.bat_cmb_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.bat_cmb_type.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bat_cmb_type.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_cmb_type.Items.AddRange(New Object() {"Nickel-Cadmium", "Nickel-Hydrogen", "Lithium-Ion", "Other"})
        Me.bat_cmb_type.Location = New System.Drawing.Point(62, 28)
        Me.bat_cmb_type.Name = "bat_cmb_type"
        Me.bat_cmb_type.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_cmb_type.Size = New System.Drawing.Size(149, 24)
        Me.bat_cmb_type.TabIndex = 17
        '
        'bat_txt_c
        '
        Me.bat_txt_c.AcceptsReturn = True
        Me.bat_txt_c.BackColor = System.Drawing.SystemColors.Window
        Me.bat_txt_c.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.bat_txt_c.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bat_txt_c.ForeColor = System.Drawing.SystemColors.WindowText
        Me.bat_txt_c.Location = New System.Drawing.Point(446, 28)
        Me.bat_txt_c.MaxLength = 0
        Me.bat_txt_c.Name = "bat_txt_c"
        Me.bat_txt_c.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.bat_txt_c.Size = New System.Drawing.Size(54, 23)
        Me.bat_txt_c.TabIndex = 16
        Me.bat_txt_c.Tag = "1"
        Me.bat_txt_c.Text = "1"
        Me.bat_txt_c.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'zlbl_29
        '
        Me.zlbl_29.AutoSize = True
        Me.zlbl_29.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_29.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_29.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_29.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_29.Location = New System.Drawing.Point(15, 32)
        Me.zlbl_29.Name = "zlbl_29"
        Me.zlbl_29.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_29.Size = New System.Drawing.Size(40, 16)
        Me.zlbl_29.TabIndex = 19
        Me.zlbl_29.Text = "Type"
        '
        'zlbl_8
        '
        Me.zlbl_8.AutoSize = True
        Me.zlbl_8.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_8.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_8.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_8.Location = New System.Drawing.Point(233, 31)
        Me.zlbl_8.Name = "zlbl_8"
        Me.zlbl_8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_8.Size = New System.Drawing.Size(207, 16)
        Me.zlbl_8.TabIndex = 18
        Me.zlbl_8.Text = "Nominal capacity per cell (Ah)"
        '
        'Onglet_power_manag
        '
        Me.Onglet_power_manag.BackColor = System.Drawing.Color.Transparent
        Me.Onglet_power_manag.Controls.Add(Me.zpict_6)
        Me.Onglet_power_manag.Controls.Add(Me.zfra_18)
        Me.Onglet_power_manag.Controls.Add(Me.zfra_16)
        Me.Onglet_power_manag.Controls.Add(Me.zfra_6)
        Me.Onglet_power_manag.Location = New System.Drawing.Point(4, 32)
        Me.Onglet_power_manag.Name = "Onglet_power_manag"
        Me.Onglet_power_manag.Size = New System.Drawing.Size(1029, 495)
        Me.Onglet_power_manag.TabIndex = 4
        Me.Onglet_power_manag.Text = "Management"
        Me.Onglet_power_manag.UseVisualStyleBackColor = True
        '
        'zpict_6
        '
        Me.zpict_6.BackgroundImage = Global.Simusat.Resources.Management
        Me.zpict_6.Location = New System.Drawing.Point(665, 104)
        Me.zpict_6.Name = "zpict_6"
        Me.zpict_6.Size = New System.Drawing.Size(320, 300)
        Me.zpict_6.TabIndex = 73
        Me.zpict_6.TabStop = False
        '
        'zfra_18
        '
        Me.zfra_18.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_18.Controls.Add(Me.gst_txt_chini)
        Me.zfra_18.Controls.Add(Me.gst_txt_ichmax)
        Me.zfra_18.Controls.Add(Me.gst_txt_kcharge)
        Me.zfra_18.Controls.Add(Me.gst_txt_chvmax)
        Me.zfra_18.Controls.Add(Me.gst_txt_cour_ent)
        Me.zfra_18.Controls.Add(Me.zlbl_49)
        Me.zfra_18.Controls.Add(Me.zlbl_39)
        Me.zfra_18.Controls.Add(Me.zlbl_48)
        Me.zfra_18.Controls.Add(Me.zlbl_47)
        Me.zfra_18.Controls.Add(Me.zlbl_50)
        Me.zfra_18.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_18.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_18.Location = New System.Drawing.Point(61, 285)
        Me.zfra_18.Name = "zfra_18"
        Me.zfra_18.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_18.Size = New System.Drawing.Size(563, 119)
        Me.zfra_18.TabIndex = 51
        Me.zfra_18.TabStop = False
        Me.zfra_18.Text = "Batteries management"
        '
        'gst_txt_chini
        '
        Me.gst_txt_chini.AcceptsReturn = True
        Me.gst_txt_chini.BackColor = System.Drawing.SystemColors.Window
        Me.gst_txt_chini.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gst_txt_chini.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gst_txt_chini.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gst_txt_chini.Location = New System.Drawing.Point(249, 28)
        Me.gst_txt_chini.MaxLength = 0
        Me.gst_txt_chini.Name = "gst_txt_chini"
        Me.gst_txt_chini.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_txt_chini.Size = New System.Drawing.Size(50, 23)
        Me.gst_txt_chini.TabIndex = 56
        Me.gst_txt_chini.Tag = "1"
        Me.gst_txt_chini.Text = "100"
        Me.gst_txt_chini.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gst_txt_ichmax
        '
        Me.gst_txt_ichmax.AcceptsReturn = True
        Me.gst_txt_ichmax.BackColor = System.Drawing.SystemColors.Window
        Me.gst_txt_ichmax.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gst_txt_ichmax.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gst_txt_ichmax.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gst_txt_ichmax.Location = New System.Drawing.Point(249, 56)
        Me.gst_txt_ichmax.MaxLength = 0
        Me.gst_txt_ichmax.Name = "gst_txt_ichmax"
        Me.gst_txt_ichmax.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_txt_ichmax.Size = New System.Drawing.Size(49, 23)
        Me.gst_txt_ichmax.TabIndex = 55
        Me.gst_txt_ichmax.Tag = "1"
        Me.gst_txt_ichmax.Text = "100"
        Me.gst_txt_ichmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gst_txt_kcharge
        '
        Me.gst_txt_kcharge.AcceptsReturn = True
        Me.gst_txt_kcharge.BackColor = System.Drawing.SystemColors.Window
        Me.gst_txt_kcharge.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gst_txt_kcharge.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gst_txt_kcharge.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gst_txt_kcharge.Location = New System.Drawing.Point(486, 28)
        Me.gst_txt_kcharge.MaxLength = 0
        Me.gst_txt_kcharge.Name = "gst_txt_kcharge"
        Me.gst_txt_kcharge.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_txt_kcharge.Size = New System.Drawing.Size(49, 23)
        Me.gst_txt_kcharge.TabIndex = 54
        Me.gst_txt_kcharge.Tag = "1"
        Me.gst_txt_kcharge.Text = "1.1"
        Me.gst_txt_kcharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gst_txt_chvmax
        '
        Me.gst_txt_chvmax.AcceptsReturn = True
        Me.gst_txt_chvmax.BackColor = System.Drawing.SystemColors.Window
        Me.gst_txt_chvmax.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gst_txt_chvmax.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gst_txt_chvmax.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gst_txt_chvmax.Location = New System.Drawing.Point(249, 84)
        Me.gst_txt_chvmax.MaxLength = 0
        Me.gst_txt_chvmax.Name = "gst_txt_chvmax"
        Me.gst_txt_chvmax.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_txt_chvmax.Size = New System.Drawing.Size(49, 23)
        Me.gst_txt_chvmax.TabIndex = 53
        Me.gst_txt_chvmax.Tag = "1"
        Me.gst_txt_chvmax.Text = "1.5"
        Me.gst_txt_chvmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gst_txt_cour_ent
        '
        Me.gst_txt_cour_ent.AcceptsReturn = True
        Me.gst_txt_cour_ent.BackColor = System.Drawing.SystemColors.Window
        Me.gst_txt_cour_ent.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gst_txt_cour_ent.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gst_txt_cour_ent.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gst_txt_cour_ent.Location = New System.Drawing.Point(486, 56)
        Me.gst_txt_cour_ent.MaxLength = 0
        Me.gst_txt_cour_ent.Name = "gst_txt_cour_ent"
        Me.gst_txt_cour_ent.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_txt_cour_ent.Size = New System.Drawing.Size(49, 23)
        Me.gst_txt_cour_ent.TabIndex = 52
        Me.gst_txt_cour_ent.Tag = "1"
        Me.gst_txt_cour_ent.Text = "80"
        Me.gst_txt_cour_ent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'zlbl_49
        '
        Me.zlbl_49.AutoSize = True
        Me.zlbl_49.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_49.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_49.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_49.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_49.Location = New System.Drawing.Point(119, 31)
        Me.zlbl_49.Name = "zlbl_49"
        Me.zlbl_49.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_49.Size = New System.Drawing.Size(124, 16)
        Me.zlbl_49.TabIndex = 61
        Me.zlbl_49.Text = "Initial charge (%)"
        Me.zlbl_49.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_39
        '
        Me.zlbl_39.AutoSize = True
        Me.zlbl_39.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_39.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_39.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_39.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_39.Location = New System.Drawing.Point(19, 59)
        Me.zlbl_39.Name = "zlbl_39"
        Me.zlbl_39.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_39.Size = New System.Drawing.Size(224, 16)
        Me.zlbl_39.TabIndex = 60
        Me.zlbl_39.Text = "Maximum charge current (A/cell)"
        Me.zlbl_39.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_48
        '
        Me.zlbl_48.AutoSize = True
        Me.zlbl_48.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_48.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_48.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_48.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_48.Location = New System.Drawing.Point(338, 31)
        Me.zlbl_48.Name = "zlbl_48"
        Me.zlbl_48.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_48.Size = New System.Drawing.Size(144, 16)
        Me.zlbl_48.TabIndex = 59
        Me.zlbl_48.Text = "Recharge coefficient"
        Me.zlbl_48.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_47
        '
        Me.zlbl_47.AutoSize = True
        Me.zlbl_47.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_47.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_47.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_47.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_47.Location = New System.Drawing.Point(19, 87)
        Me.zlbl_47.Name = "zlbl_47"
        Me.zlbl_47.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_47.Size = New System.Drawing.Size(225, 16)
        Me.zlbl_47.TabIndex = 58
        Me.zlbl_47.Text = "Maximum charge voltage (V/cell)"
        Me.zlbl_47.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_50
        '
        Me.zlbl_50.AutoSize = True
        Me.zlbl_50.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_50.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_50.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_50.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_50.Location = New System.Drawing.Point(320, 59)
        Me.zlbl_50.Name = "zlbl_50"
        Me.zlbl_50.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_50.Size = New System.Drawing.Size(162, 16)
        Me.zlbl_50.TabIndex = 57
        Me.zlbl_50.Text = "Holding current (A/cell)"
        Me.zlbl_50.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zfra_16
        '
        Me.zfra_16.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_16.Controls.Add(Me.gst_txt_rendgs)
        Me.zfra_16.Controls.Add(Me.gst_txt_rendbat)
        Me.zfra_16.Controls.Add(Me.gst_opt_regul_0)
        Me.zfra_16.Controls.Add(Me.gst_opt_regul_1)
        Me.zfra_16.Controls.Add(Me.gst_txt_Vregul)
        Me.zfra_16.Controls.Add(Me.zlbl_81)
        Me.zfra_16.Controls.Add(Me.zlbl_44)
        Me.zfra_16.Controls.Add(Me.zlbl_45)
        Me.zfra_16.Controls.Add(Me.zlbl_28)
        Me.zfra_16.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_16.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_16.Location = New System.Drawing.Point(364, 104)
        Me.zfra_16.Name = "zfra_16"
        Me.zfra_16.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_16.Size = New System.Drawing.Size(260, 165)
        Me.zfra_16.TabIndex = 62
        Me.zfra_16.TabStop = False
        Me.zfra_16.Text = "Management / Distribution "
        '
        'gst_txt_rendgs
        '
        Me.gst_txt_rendgs.AcceptsReturn = True
        Me.gst_txt_rendgs.BackColor = System.Drawing.SystemColors.Window
        Me.gst_txt_rendgs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gst_txt_rendgs.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gst_txt_rendgs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gst_txt_rendgs.Location = New System.Drawing.Point(176, 107)
        Me.gst_txt_rendgs.MaxLength = 0
        Me.gst_txt_rendgs.Name = "gst_txt_rendgs"
        Me.gst_txt_rendgs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_txt_rendgs.Size = New System.Drawing.Size(41, 23)
        Me.gst_txt_rendgs.TabIndex = 67
        Me.gst_txt_rendgs.Tag = "1"
        Me.gst_txt_rendgs.Text = "90"
        Me.gst_txt_rendgs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gst_txt_rendbat
        '
        Me.gst_txt_rendbat.AcceptsReturn = True
        Me.gst_txt_rendbat.BackColor = System.Drawing.SystemColors.Window
        Me.gst_txt_rendbat.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gst_txt_rendbat.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gst_txt_rendbat.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gst_txt_rendbat.Location = New System.Drawing.Point(176, 135)
        Me.gst_txt_rendbat.MaxLength = 0
        Me.gst_txt_rendbat.Name = "gst_txt_rendbat"
        Me.gst_txt_rendbat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_txt_rendbat.Size = New System.Drawing.Size(41, 23)
        Me.gst_txt_rendbat.TabIndex = 66
        Me.gst_txt_rendbat.Tag = "1"
        Me.gst_txt_rendbat.Text = "90"
        Me.gst_txt_rendbat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gst_opt_regul_0
        '
        Me.gst_opt_regul_0.BackColor = System.Drawing.SystemColors.Control
        Me.gst_opt_regul_0.Checked = True
        Me.gst_opt_regul_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.gst_opt_regul_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gst_opt_regul_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gst_opt_regul_0.Location = New System.Drawing.Point(16, 24)
        Me.gst_opt_regul_0.Name = "gst_opt_regul_0"
        Me.gst_opt_regul_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_opt_regul_0.Size = New System.Drawing.Size(97, 25)
        Me.gst_opt_regul_0.TabIndex = 65
        Me.gst_opt_regul_0.TabStop = True
        Me.gst_opt_regul_0.Text = "Regulation"
        Me.gst_opt_regul_0.UseVisualStyleBackColor = False
        '
        'gst_opt_regul_1
        '
        Me.gst_opt_regul_1.BackColor = System.Drawing.SystemColors.Control
        Me.gst_opt_regul_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.gst_opt_regul_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gst_opt_regul_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gst_opt_regul_1.Location = New System.Drawing.Point(16, 46)
        Me.gst_opt_regul_1.Name = "gst_opt_regul_1"
        Me.gst_opt_regul_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_opt_regul_1.Size = New System.Drawing.Size(134, 29)
        Me.gst_opt_regul_1.TabIndex = 64
        Me.gst_opt_regul_1.TabStop = True
        Me.gst_opt_regul_1.Text = "No regulation"
        Me.gst_opt_regul_1.UseVisualStyleBackColor = False
        '
        'gst_txt_Vregul
        '
        Me.gst_txt_Vregul.AcceptsReturn = True
        Me.gst_txt_Vregul.BackColor = System.Drawing.SystemColors.Window
        Me.gst_txt_Vregul.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gst_txt_Vregul.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gst_txt_Vregul.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gst_txt_Vregul.Location = New System.Drawing.Point(200, 25)
        Me.gst_txt_Vregul.MaxLength = 0
        Me.gst_txt_Vregul.Name = "gst_txt_Vregul"
        Me.gst_txt_Vregul.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_txt_Vregul.Size = New System.Drawing.Size(45, 23)
        Me.gst_txt_Vregul.TabIndex = 63
        Me.gst_txt_Vregul.Tag = "1"
        Me.gst_txt_Vregul.Text = "100"
        Me.gst_txt_Vregul.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'zlbl_81
        '
        Me.zlbl_81.AutoSize = True
        Me.zlbl_81.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_81.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_81.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_81.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_81.Location = New System.Drawing.Point(13, 87)
        Me.zlbl_81.Name = "zlbl_81"
        Me.zlbl_81.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_81.Size = New System.Drawing.Size(113, 16)
        Me.zlbl_81.TabIndex = 70
        Me.zlbl_81.Text = "Efficiency (%) :"
        '
        'zlbl_44
        '
        Me.zlbl_44.AutoSize = True
        Me.zlbl_44.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_44.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_44.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_44.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_44.Location = New System.Drawing.Point(17, 110)
        Me.zlbl_44.Name = "zlbl_44"
        Me.zlbl_44.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_44.Size = New System.Drawing.Size(155, 16)
        Me.zlbl_44.TabIndex = 70
        Me.zlbl_44.Text = "Solar panels / Payload"
        '
        'zlbl_45
        '
        Me.zlbl_45.AutoSize = True
        Me.zlbl_45.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_45.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_45.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_45.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_45.Location = New System.Drawing.Point(38, 138)
        Me.zlbl_45.Name = "zlbl_45"
        Me.zlbl_45.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_45.Size = New System.Drawing.Size(134, 16)
        Me.zlbl_45.TabIndex = 69
        Me.zlbl_45.Text = "Batteries / Payload"
        '
        'zlbl_28
        '
        Me.zlbl_28.AutoSize = True
        Me.zlbl_28.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_28.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_28.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_28.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_28.Location = New System.Drawing.Point(136, 27)
        Me.zlbl_28.Name = "zlbl_28"
        Me.zlbl_28.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_28.Size = New System.Drawing.Size(57, 16)
        Me.zlbl_28.TabIndex = 68
        Me.zlbl_28.Text = "Voltage"
        Me.zlbl_28.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zfra_6
        '
        Me.zfra_6.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_6.Controls.Add(Me.zfra_15)
        Me.zfra_6.Controls.Add(Me.zfra_17)
        Me.zfra_6.Controls.Add(Me.zlbl_40)
        Me.zfra_6.Controls.Add(Me.zlbl_43)
        Me.zfra_6.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_6.ForeColor = System.Drawing.Color.Maroon
        Me.zfra_6.Location = New System.Drawing.Point(61, 103)
        Me.zfra_6.Name = "zfra_6"
        Me.zfra_6.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_6.Size = New System.Drawing.Size(287, 166)
        Me.zfra_6.TabIndex = 72
        Me.zfra_6.TabStop = False
        Me.zfra_6.Text = "Power"
        '
        'zfra_15
        '
        Me.zfra_15.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_15.Controls.Add(Me.gst_txt_pdem_0)
        Me.zfra_15.Controls.Add(Me.gst_txt_diss_0)
        Me.zfra_15.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zfra_15.Location = New System.Drawing.Point(125, 33)
        Me.zfra_15.Name = "zfra_15"
        Me.zfra_15.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_15.Size = New System.Drawing.Size(69, 85)
        Me.zfra_15.TabIndex = 76
        Me.zfra_15.TabStop = False
        Me.zfra_15.Text = "Day"
        '
        'gst_txt_pdem_0
        '
        Me.gst_txt_pdem_0.AcceptsReturn = True
        Me.gst_txt_pdem_0.BackColor = System.Drawing.SystemColors.Window
        Me.gst_txt_pdem_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gst_txt_pdem_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gst_txt_pdem_0.Location = New System.Drawing.Point(12, 24)
        Me.gst_txt_pdem_0.MaxLength = 0
        Me.gst_txt_pdem_0.Name = "gst_txt_pdem_0"
        Me.gst_txt_pdem_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_txt_pdem_0.Size = New System.Drawing.Size(45, 23)
        Me.gst_txt_pdem_0.TabIndex = 78
        Me.gst_txt_pdem_0.Tag = "1"
        Me.gst_txt_pdem_0.Text = "50"
        Me.gst_txt_pdem_0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gst_txt_diss_0
        '
        Me.gst_txt_diss_0.AcceptsReturn = True
        Me.gst_txt_diss_0.BackColor = System.Drawing.SystemColors.Window
        Me.gst_txt_diss_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gst_txt_diss_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gst_txt_diss_0.Location = New System.Drawing.Point(12, 52)
        Me.gst_txt_diss_0.MaxLength = 0
        Me.gst_txt_diss_0.Name = "gst_txt_diss_0"
        Me.gst_txt_diss_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_txt_diss_0.Size = New System.Drawing.Size(45, 23)
        Me.gst_txt_diss_0.TabIndex = 77
        Me.gst_txt_diss_0.Tag = "1"
        Me.gst_txt_diss_0.Text = "15"
        Me.gst_txt_diss_0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'zfra_17
        '
        Me.zfra_17.BackColor = System.Drawing.SystemColors.Control
        Me.zfra_17.Controls.Add(Me.gst_txt_pdem_1)
        Me.zfra_17.Controls.Add(Me.gst_txt_diss_1)
        Me.zfra_17.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zfra_17.Location = New System.Drawing.Point(202, 33)
        Me.zfra_17.Name = "zfra_17"
        Me.zfra_17.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra_17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra_17.Size = New System.Drawing.Size(69, 85)
        Me.zfra_17.TabIndex = 73
        Me.zfra_17.TabStop = False
        Me.zfra_17.Text = "Eclipse "
        '
        'gst_txt_pdem_1
        '
        Me.gst_txt_pdem_1.AcceptsReturn = True
        Me.gst_txt_pdem_1.BackColor = System.Drawing.SystemColors.Window
        Me.gst_txt_pdem_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gst_txt_pdem_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gst_txt_pdem_1.Location = New System.Drawing.Point(12, 24)
        Me.gst_txt_pdem_1.MaxLength = 0
        Me.gst_txt_pdem_1.Name = "gst_txt_pdem_1"
        Me.gst_txt_pdem_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_txt_pdem_1.Size = New System.Drawing.Size(45, 23)
        Me.gst_txt_pdem_1.TabIndex = 75
        Me.gst_txt_pdem_1.Tag = "1"
        Me.gst_txt_pdem_1.Text = "50"
        Me.gst_txt_pdem_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gst_txt_diss_1
        '
        Me.gst_txt_diss_1.AcceptsReturn = True
        Me.gst_txt_diss_1.BackColor = System.Drawing.SystemColors.Window
        Me.gst_txt_diss_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.gst_txt_diss_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gst_txt_diss_1.Location = New System.Drawing.Point(12, 52)
        Me.gst_txt_diss_1.MaxLength = 0
        Me.gst_txt_diss_1.Name = "gst_txt_diss_1"
        Me.gst_txt_diss_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.gst_txt_diss_1.Size = New System.Drawing.Size(45, 23)
        Me.gst_txt_diss_1.TabIndex = 74
        Me.gst_txt_diss_1.Tag = "1"
        Me.gst_txt_diss_1.Text = "15"
        Me.gst_txt_diss_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'zlbl_40
        '
        Me.zlbl_40.AutoSize = True
        Me.zlbl_40.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_40.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_40.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_40.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_40.Location = New System.Drawing.Point(12, 60)
        Me.zlbl_40.Name = "zlbl_40"
        Me.zlbl_40.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_40.Size = New System.Drawing.Size(107, 16)
        Me.zlbl_40.TabIndex = 80
        Me.zlbl_40.Text = "Requested (W)"
        '
        'zlbl_43
        '
        Me.zlbl_43.AutoSize = True
        Me.zlbl_43.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_43.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_43.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_43.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_43.Location = New System.Drawing.Point(14, 88)
        Me.zlbl_43.Name = "zlbl_43"
        Me.zlbl_43.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_43.Size = New System.Drawing.Size(105, 16)
        Me.zlbl_43.TabIndex = 79
        Me.zlbl_43.Text = "Dissipated (%)"
        '
        'cmd_quit
        '
        Me.cmd_quit.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_quit.Location = New System.Drawing.Point(482, 560)
        Me.cmd_quit.Name = "cmd_quit"
        Me.cmd_quit.Size = New System.Drawing.Size(108, 36)
        Me.cmd_quit.TabIndex = 3
        Me.cmd_quit.Text = "Close"
        Me.cmd_quit.UseVisualStyleBackColor = True
        '
        'pwr_lblmsg
        '
        Me.pwr_lblmsg.AutoSize = True
        Me.pwr_lblmsg.BackColor = System.Drawing.Color.Red
        Me.pwr_lblmsg.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pwr_lblmsg.Location = New System.Drawing.Point(27, 566)
        Me.pwr_lblmsg.Name = "pwr_lblmsg"
        Me.pwr_lblmsg.Size = New System.Drawing.Size(0, 16)
        Me.pwr_lblmsg.TabIndex = 41
        '
        'Label20
        '
        Me.Label20.Enabled = False
        Me.Label20.Location = New System.Drawing.Point(27, 560)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(423, 38)
        Me.Label20.TabIndex = 42
        Me.Label20.Text = "Label20"
        Me.Label20.Visible = False
        '
        'frmPowerConf
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1079, 607)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.pwr_lblmsg)
        Me.Controls.Add(Me.cmd_quit)
        Me.Controls.Add(Me.tabPower)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmPowerConf"
        Me.Text = "Energy Data"
        Me.tabPower.ResumeLayout(False)
        Me.Onglet_power_cells.ResumeLayout(False)
        CType(Me.cel_cosaprox_graph, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cel_charac_graph, System.ComponentModel.ISupportInitialize).EndInit()
        Me.zfra_14.ResumeLayout(False)
        Me.zfra_19.ResumeLayout(False)
        Me.zfra_19.PerformLayout()
        Me.zfra_8.ResumeLayout(False)
        Me.zfra_8.PerformLayout()
        Me.zfra_21.ResumeLayout(False)
        Me.zfra_21.PerformLayout()
        Me.zfra_23.ResumeLayout(False)
        Me.zfra_23.PerformLayout()
        Me.Onglet_power_therm.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.zfra_25.ResumeLayout(False)
        Me.zfra_25.PerformLayout()
        Me.zfra_24.ResumeLayout(False)
        Me.zfra_24.PerformLayout()
        Me.Onglet_power_bat.ResumeLayout(False)
        Me.bat_onglet_batterie.ResumeLayout(False)
        Me.bat_onglet_batterie_TabPage0.ResumeLayout(False)
        CType(Me.bat_charge_graph, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bat_frm_charge.ResumeLayout(False)
        Me.bat_frm_charge.PerformLayout()
        Me.zpict_4.ResumeLayout(False)
        Me.bat_onglet_batterie_TabPage1.ResumeLayout(False)
        CType(Me.bat_decharge_graph, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bat_frm_decharge.ResumeLayout(False)
        Me.bat_frm_decharge.PerformLayout()
        Me.zpict_3.ResumeLayout(False)
        Me.bat_onglet_batterie_TabPage2.ResumeLayout(False)
        CType(Me.bat_auto_graph, System.ComponentModel.ISupportInitialize).EndInit()
        Me.zfra_2.ResumeLayout(False)
        Me.zfra_2.PerformLayout()
        Me.bat_onglet_batterie_TabPage3.ResumeLayout(False)
        CType(Me.bat_cdispo_graph, System.ComponentModel.ISupportInitialize).EndInit()
        Me.zfra_5.ResumeLayout(False)
        Me.zfra_5.PerformLayout()
        Me.zfra_7.ResumeLayout(False)
        Me.zfra_7.PerformLayout()
        Me.zfra_10.ResumeLayout(False)
        Me.zfra_10.PerformLayout()
        Me.zfra_9.ResumeLayout(False)
        Me.zfra_9.PerformLayout()
        Me.Onglet_power_manag.ResumeLayout(False)
        CType(Me.zpict_6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.zfra_18.ResumeLayout(False)
        Me.zfra_18.PerformLayout()
        Me.zfra_16.ResumeLayout(False)
        Me.zfra_16.PerformLayout()
        Me.zfra_6.ResumeLayout(False)
        Me.zfra_6.PerformLayout()
        Me.zfra_15.ResumeLayout(False)
        Me.zfra_15.PerformLayout()
        Me.zfra_17.ResumeLayout(False)
        Me.zfra_17.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents tabPower As System.Windows.Forms.TabControl
    Public WithEvents Onglet_power_cells As System.Windows.Forms.TabPage
    Public WithEvents zfra_14 As System.Windows.Forms.GroupBox
    Public WithEvents cel_opt_cour As System.Windows.Forms.RadioButton
    Public WithEvents cel_opt_puiss As System.Windows.Forms.RadioButton
    Public WithEvents zfra_19 As System.Windows.Forms.GroupBox
    Public WithEvents cel_txt_radvmp As System.Windows.Forms.TextBox
    Public WithEvents cel_txt_radvoc As System.Windows.Forms.TextBox
    Public WithEvents cel_txt_radimp As System.Windows.Forms.TextBox
    Public WithEvents cel_txt_radisc As System.Windows.Forms.TextBox
    Public WithEvents cel_opt_beol_0 As System.Windows.Forms.RadioButton
    Public WithEvents cel_opt_beol_1 As System.Windows.Forms.RadioButton
    Public WithEvents zlbl_24 As System.Windows.Forms.Label
    Public WithEvents zlbl_25 As System.Windows.Forms.Label
    Public WithEvents zlbl_26 As System.Windows.Forms.Label
    Public WithEvents zlbl_27 As System.Windows.Forms.Label
    Public WithEvents zfra_8 As System.Windows.Forms.GroupBox
    Public WithEvents cel_txt_tref As System.Windows.Forms.TextBox
    Public WithEvents cel_txt_isc As System.Windows.Forms.TextBox
    Public WithEvents cel_txt_disc As System.Windows.Forms.TextBox
    Public WithEvents cel_txt_imp As System.Windows.Forms.TextBox
    Public WithEvents cel_txt_dimp As System.Windows.Forms.TextBox
    Public WithEvents cel_txt_voc As System.Windows.Forms.TextBox
    Public WithEvents cel_txt_dvoc As System.Windows.Forms.TextBox
    Public WithEvents cel_txt_vmp As System.Windows.Forms.TextBox
    Public WithEvents cel_txt_dvmp As System.Windows.Forms.TextBox
    Public WithEvents cel_cmb_type As System.Windows.Forms.ComboBox
    Public WithEvents zlbl_58 As System.Windows.Forms.Label
    Public WithEvents zlbl_30 As System.Windows.Forms.Label
    Public WithEvents zlbl_31 As System.Windows.Forms.Label
    Public WithEvents zlbl_32 As System.Windows.Forms.Label
    Public WithEvents zlbl_38 As System.Windows.Forms.Label
    Public WithEvents zlbl_41 As System.Windows.Forms.Label
    Public WithEvents zlbl_42 As System.Windows.Forms.Label
    Public WithEvents zlbl_51 As System.Windows.Forms.Label
    Public WithEvents zlbl_55 As System.Windows.Forms.Label
    Public WithEvents zfra_21 As System.Windows.Forms.GroupBox
    Public WithEvents cel_txt_surf As System.Windows.Forms.TextBox
    Public WithEvents cel_txt_ns As System.Windows.Forms.TextBox
    Public WithEvents zlbl_23 As System.Windows.Forms.Label
    Public WithEvents zlbl_87 As System.Windows.Forms.Label
    Public WithEvents zlbl_86 As System.Windows.Forms.Label
    Public WithEvents zfra_23 As System.Windows.Forms.GroupBox
    Public WithEvents cel_txt_tetad As System.Windows.Forms.TextBox
    Public WithEvents cel_txt_tetam As System.Windows.Forms.TextBox
    Public WithEvents zlbl_53 As System.Windows.Forms.Label
    Public WithEvents zlbl_54 As System.Windows.Forms.Label
    Public WithEvents Onglet_power_bat As System.Windows.Forms.TabPage
    Public WithEvents bat_onglet_batterie As System.Windows.Forms.TabControl
    Public WithEvents bat_onglet_batterie_TabPage0 As System.Windows.Forms.TabPage
    Public WithEvents bat_frm_charge As System.Windows.Forms.GroupBox
    Public WithEvents bat_txt_2_5 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_1_5 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_0_5 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_och_5 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_2_1 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_1_1 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_0_1 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_2_2 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_1_2 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_0_2 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_och_1 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_och_2 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_och_4 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_och_3 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_och_0 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_0_4 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_1_4 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_2_4 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_0_3 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_1_3 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_2_3 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_0_0 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_1_0 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_2_0 As System.Windows.Forms.TextBox
    Public WithEvents zpict_1 As System.Windows.Forms.Panel
    Public WithEvents zlbl_62 As System.Windows.Forms.Label
    Public WithEvents zlbl_56 As System.Windows.Forms.Label
    Public WithEvents zlbl_59 As System.Windows.Forms.Label
    Public WithEvents zlbl_61 As System.Windows.Forms.Label
    Public WithEvents zlbl_60 As System.Windows.Forms.Label
    Public WithEvents zlbl_57 As System.Windows.Forms.Label
    Public WithEvents zpict_4 As System.Windows.Forms.Panel
    Public WithEvents bat_ch_opt_temp As System.Windows.Forms.RadioButton
    Public WithEvents bat_ch_opt_courant As System.Windows.Forms.RadioButton
    Public WithEvents bat_onglet_batterie_TabPage1 As System.Windows.Forms.TabPage
    Public WithEvents bat_frm_decharge As System.Windows.Forms.GroupBox
    Public WithEvents bat_txt_d100_5 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d100_1 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d100_2 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d100_4 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d100_3 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d100_0 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d2_0 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d1_0 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d0_0 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d2_3 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d1_3 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d0_3 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d2_4 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d1_4 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d0_4 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d0_2 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d1_2 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d2_2 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d0_1 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d1_1 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d2_1 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d0_5 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d1_5 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_d2_5 As System.Windows.Forms.TextBox
    Public WithEvents zpict_2 As System.Windows.Forms.Panel
    Public WithEvents zlbl_7 As System.Windows.Forms.Label
    Public WithEvents zlbl_9 As System.Windows.Forms.Label
    Public WithEvents zlbl_18 As System.Windows.Forms.Label
    Public WithEvents zlbl_19 As System.Windows.Forms.Label
    Public WithEvents zlbl_20 As System.Windows.Forms.Label
    Public WithEvents zlbl_21 As System.Windows.Forms.Label
    Public WithEvents zpict_3 As System.Windows.Forms.Panel
    Public WithEvents bat_dch_opt_temp As System.Windows.Forms.RadioButton
    Public WithEvents bat_dch_opt_courant As System.Windows.Forms.RadioButton
    Public WithEvents bat_onglet_batterie_TabPage2 As System.Windows.Forms.TabPage
    Public WithEvents zfra_2 As System.Windows.Forms.GroupBox
    Public WithEvents bat_txt_tc0 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_tc50 As System.Windows.Forms.TextBox
    Public WithEvents zlbl_16 As System.Windows.Forms.Label
    Public WithEvents zlbl_17 As System.Windows.Forms.Label
    Public WithEvents bat_onglet_batterie_TabPage3 As System.Windows.Forms.TabPage
    Public WithEvents zfra_5 As System.Windows.Forms.GroupBox
    Public WithEvents bat_txt_c10 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_cmax As System.Windows.Forms.TextBox
    Public WithEvents zlbl_12 As System.Windows.Forms.Label
    Public WithEvents zlbl_11 As System.Windows.Forms.Label
    Public WithEvents zfra_7 As System.Windows.Forms.GroupBox
    Public WithEvents bat_txt_t99 As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_cmin As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_tmin As System.Windows.Forms.TextBox
    Public WithEvents zlbl_15 As System.Windows.Forms.Label
    Public WithEvents zlbl_14 As System.Windows.Forms.Label
    Public WithEvents zlbl_13 As System.Windows.Forms.Label
    Public WithEvents zfra_10 As System.Windows.Forms.GroupBox
    Public WithEvents bat_txt_ns As System.Windows.Forms.TextBox
    Public WithEvents bat_txt_np As System.Windows.Forms.TextBox
    Public WithEvents zlbl_10 As System.Windows.Forms.Label
    Public WithEvents zlbl_4 As System.Windows.Forms.Label
    Public WithEvents zfra_9 As System.Windows.Forms.GroupBox
    Public WithEvents bat_cmb_type As System.Windows.Forms.ComboBox
    Public WithEvents bat_txt_c As System.Windows.Forms.TextBox
    Public WithEvents zlbl_29 As System.Windows.Forms.Label
    Public WithEvents zlbl_8 As System.Windows.Forms.Label
    Public WithEvents Onglet_power_manag As System.Windows.Forms.TabPage
    Public WithEvents zfra_18 As System.Windows.Forms.GroupBox
    Public WithEvents gst_txt_chini As System.Windows.Forms.TextBox
    Public WithEvents gst_txt_ichmax As System.Windows.Forms.TextBox
    Public WithEvents gst_txt_kcharge As System.Windows.Forms.TextBox
    Public WithEvents gst_txt_chvmax As System.Windows.Forms.TextBox
    Public WithEvents gst_txt_cour_ent As System.Windows.Forms.TextBox
    Public WithEvents zlbl_49 As System.Windows.Forms.Label
    Public WithEvents zlbl_39 As System.Windows.Forms.Label
    Public WithEvents zlbl_48 As System.Windows.Forms.Label
    Public WithEvents zlbl_47 As System.Windows.Forms.Label
    Public WithEvents zlbl_50 As System.Windows.Forms.Label
    Public WithEvents zfra_16 As System.Windows.Forms.GroupBox
    Public WithEvents gst_txt_rendgs As System.Windows.Forms.TextBox
    Public WithEvents gst_txt_rendbat As System.Windows.Forms.TextBox
    Public WithEvents gst_opt_regul_0 As System.Windows.Forms.RadioButton
    Public WithEvents gst_opt_regul_1 As System.Windows.Forms.RadioButton
    Public WithEvents gst_txt_Vregul As System.Windows.Forms.TextBox
    Public WithEvents zlbl_44 As System.Windows.Forms.Label
    Public WithEvents zlbl_45 As System.Windows.Forms.Label
    Public WithEvents zlbl_28 As System.Windows.Forms.Label
    Public WithEvents zfra_6 As System.Windows.Forms.GroupBox
    Public WithEvents zfra_15 As System.Windows.Forms.GroupBox
    Public WithEvents gst_txt_pdem_0 As System.Windows.Forms.TextBox
    Public WithEvents gst_txt_diss_0 As System.Windows.Forms.TextBox
    Public WithEvents zfra_17 As System.Windows.Forms.GroupBox
    Public WithEvents gst_txt_pdem_1 As System.Windows.Forms.TextBox
    Public WithEvents gst_txt_diss_1 As System.Windows.Forms.TextBox
    Public WithEvents zlbl_40 As System.Windows.Forms.Label
    Public WithEvents zlbl_43 As System.Windows.Forms.Label
    Friend WithEvents cel_charac_graph As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents cmd_quit As System.Windows.Forms.Button
    Friend WithEvents cel_cosaprox_graph As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents bat_charge_graph As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents bat_decharge_graph As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents bat_auto_graph As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents bat_cdispo_graph As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents Onglet_power_therm As System.Windows.Forms.TabPage
    Public WithEvents zlbl_22 As System.Windows.Forms.Label
    Public WithEvents zlbl_68 As System.Windows.Forms.Label
    Public WithEvents zlbl_81 As System.Windows.Forms.Label
    Friend WithEvents zpict_6 As System.Windows.Forms.PictureBox
    Friend WithEvents zfra_24 As System.Windows.Forms.GroupBox
    Friend WithEvents thr_txtBackAlf_8 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellAlf_8 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackEps_8 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellEps_8 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCspec_8 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackAlf_7 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellAlf_7 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackEps_7 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellEps_7 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCspec_7 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackAlf_6 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellAlf_6 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackEps_6 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellEps_6 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCspec_6 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackAlf_5 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellAlf_5 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackEps_5 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellEps_5 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCspec_5 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackAlf_4 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellAlf_4 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackEps_4 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellEps_3 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCspec_4 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackAlf_3 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellAlf_3 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackEps_3 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellEps_2 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCspec_3 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackAlf_2 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellAlf_2 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackEps_2 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellEps_1 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackAlf_1 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBackEps_1 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellAlf_1 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCellEps_4 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCspec_2 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCspec_1 As System.Windows.Forms.TextBox
    Friend WithEvents thr_lblPanName_8 As System.Windows.Forms.Label
    Friend WithEvents thr_lblPanName_7 As System.Windows.Forms.Label
    Friend WithEvents thr_lblPanName_6 As System.Windows.Forms.Label
    Friend WithEvents thr_lblPanName_5 As System.Windows.Forms.Label
    Friend WithEvents thr_lblPanName_4 As System.Windows.Forms.Label
    Friend WithEvents zlbl_91 As System.Windows.Forms.Label
    Friend WithEvents thr_lblPanName_3 As System.Windows.Forms.Label
    Friend WithEvents zlbl_89 As System.Windows.Forms.Label
    Friend WithEvents thr_lblPanName_2 As System.Windows.Forms.Label
    Friend WithEvents zlbl_33 As System.Windows.Forms.Label
    Friend WithEvents thr_lblPanName_1 As System.Windows.Forms.Label
    Friend WithEvents zfra_25 As System.Windows.Forms.GroupBox
    Friend WithEvents thr_txtTBat As System.Windows.Forms.TextBox
    Friend WithEvents zlbl_34 As System.Windows.Forms.Label
    Friend WithEvents zlbl_35 As System.Windows.Forms.Label
    Friend WithEvents thr_txtInitTemp As System.Windows.Forms.TextBox
    Friend WithEvents zlbl_72 As System.Windows.Forms.Label
    Friend WithEvents zlbl_73 As System.Windows.Forms.Label
    Public WithEvents zlbl_93 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents thr_txtBodyCspec As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents thr_txtInitBodyTemp As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents thr_txtBodyAlf_mz As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBodyEps_mz As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBodyAlf_pz As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBodyEps_pz As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBodyAlf_my As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBodyEps_my As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBodyAlf_py As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBodyEps_py As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBodyAlf_mx As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBodyEps_mx As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBodyAlf_px As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtBodyEps_px As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents thr_txtCond_8 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCond_7 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCond_6 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCond_5 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCond_4 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCond_3 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCond_2 As System.Windows.Forms.TextBox
    Friend WithEvents thr_txtCond_1 As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Public WithEvents cel_txt_eff As System.Windows.Forms.TextBox
    Friend WithEvents pwr_lblmsg As System.Windows.Forms.Label
    Friend WithEvents Label20 As Label
End Class
