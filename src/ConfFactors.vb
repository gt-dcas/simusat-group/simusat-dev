﻿Module ConfFactors
    'Dim eta As Double = 0
    'Dim dist As Double = 2
    
    Dim etainterp As vector
    Dim distinterp As vector
    Dim cfactorsinterp As vector
    Dim cfinterpaux As vector

    Public Function conf_factor_earth(ByVal d As Double, ByRef RE As Double) As Double
        '
        'Calculates de configuration factor for a point (little sphere) at a distance d from Earth
        'Chung, B.T.F. and Sumitra, P.S., 1972, "Radiation shape factors from plane point sources," J. Heat Transfer, vol. 94, no. 3, pp. 328-330, August. 
        '
        Dim aux As Double = 1 - Math.Pow(RE, 2) / Math.Pow(d, 2)
        '
        conf_factor_earth = 0.5 * (1 - Math.Sqrt(aux))
        '
    End Function

    Public Function EarthIRConfFactor(ByVal eta As Double, ByVal SatPos As Double) As Double

        'James R. Wertz, Wiley J. Larson. Space Mission Analysis and Design. s.l. : Space Technology Library, 1999.

        Dim i As Integer = 1 'line 'Starts in one because 0 is the headers
        Dim j As Integer = 0 'column
        Dim dist As Double = SatPos - RE
        'Whole file inserted in "lines" variable
        Dim lines() As String = System.IO.File.ReadAllLines(My.Application.Info.DirectoryPath & EarthConfFactorFile) 'ligne 0:header, 

        'We calculate the limits of the table
        Dim nlines As Integer = lines.Length - 1
        Dim ncol As Integer = ColumnNumber(lines)


        'Line selection
        While CsvVal(lines, i, 0) < eta And i < nlines
            etainterp.X = CsvVal(lines, i, 0)
            etainterp.Y = CsvVal(lines, i + 1, 0)
            i = i + 1
        End While
        If i = 1 Then
            etainterp.X = CsvVal(lines, i, 0)
            etainterp.Y = CsvVal(lines, i + 1, 0)
        Else
            i = i - 1  'To always point to the first component
        End If



        'Column Selection
        j = 1
        While CsvVal(lines, 0, j) < dist And j < ncol
            cfinterpaux.X = CsvVal(lines, i, j)
            cfinterpaux.Y = CsvVal(lines, i + 1, j)

            distinterp.X = CsvVal(lines, 0, j)
            distinterp.Y = CsvVal(lines, 0, j + 1)

            cfactorsinterp.X = ConfFactor(etainterp, cfinterpaux, eta)
            j = j + 1
        End While
        If j = 1 Then
            cfinterpaux.X = CsvVal(lines, i, j)
            cfinterpaux.Y = CsvVal(lines, i + 1, j)

            distinterp.X = CsvVal(lines, 0, j)
            distinterp.Y = CsvVal(lines, 0, j + 1)

            cfactorsinterp.X = ConfFactor(etainterp, cfinterpaux, eta)
        Else
            j = j - 1  'To always point to the first component
        End If

        'Second component for final interpolation
        cfinterpaux.X = CsvVal(lines, i, j + 1)
        cfinterpaux.Y = CsvVal(lines, i + 1, j + 1)
        cfactorsinterp.Y = ConfFactor(etainterp, cfinterpaux, eta)


        '
        'Final interpolation
        '
        EarthIRConfFactor = ConfFactor(distinterp, cfactorsinterp, dist)

    End Function


    '
    'This function calculates the number of columns
    '
    Friend Function ColumnNumber(ByVal lines As Array) As Integer

        'Dim multiColQuery = From lin In lines
        'fields = lin.Split(",")
        '                      Select From str In fields Skip 1
        '                                  Select Convert.ToInt32(Str)
        'Dim results = multiColQuery.ToList()
        ' Find out how many columns we have.  
        ColumnNumber = lines(0).Split(",").Length
    End Function




    '
    'This function interpolates in the table'
    '
    Friend Function ConfFactor(ByVal etas As vector, ByVal conffactors As vector, eta As Double) As Double
        'Linear interpolation
        Dim CF As Double
        CF = conffactors.X + (eta - etas.X) * (conffactors.Y - conffactors.X) / (etas.Y - etas.X)

        If CF >= 0 Then
            ConfFactor = CF
        Else : ConfFactor = 0
        End If

    End Function






    Friend Function CsvVal(ByVal table As Array, ByVal i As Integer, ByVal j As Integer) As Double
        Dim line As String = table(i)
        Dim lineArray() As String = Split(line, ",")
        CsvVal = Val(lineArray(j))
    End Function

End Module

