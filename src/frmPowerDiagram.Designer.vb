﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPowerDiagram
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPowerDiagram))
        Me.zfra_0 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lab_amperemeter = New System.Windows.Forms.Label()
        Me.prg_chargemeter = New System.Windows.Forms.ProgressBar()
        Me.lab_voltmeter = New System.Windows.Forms.Label()
        Me.zlbl_7 = New System.Windows.Forms.Label()
        Me.zlbl_6 = New System.Windows.Forms.Label()
        Me.zlbl_5 = New System.Windows.Forms.Label()
        Me.zlbl_4 = New System.Windows.Forms.Label()
        Me.lab_state = New System.Windows.Forms.Label()
        Me.cmd_close = New System.Windows.Forms.Button()
        Me.lab_exc = New System.Windows.Forms.Label()
        Me.lab_puse = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lab_pava = New System.Windows.Forms.Label()
        Me.lab_pbat = New System.Windows.Forms.Label()
        Me.lab_pgs = New System.Windows.Forms.Label()
        Me.lab_disscu = New System.Windows.Forms.Label()
        Me.lab_pbat_diss = New System.Windows.Forms.Label()
        Me.lab_diss_charge = New System.Windows.Forms.Label()
        Me.lab_pgs_diss = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.zlbl_0 = New System.Windows.Forms.Label()
        Me.zlbl_3 = New System.Windows.Forms.Label()
        Me.zlbl_2 = New System.Windows.Forms.Label()
        Me.zlbl_1 = New System.Windows.Forms.Label()
        Me.img_pan_eclipse = New System.Windows.Forms.PictureBox()
        Me.fl_charge = New System.Windows.Forms.PictureBox()
        Me.fl_decharge = New System.Windows.Forms.PictureBox()
        Me.fl_shunt = New System.Windows.Forms.PictureBox()
        Me.fl_gs = New System.Windows.Forms.PictureBox()
        Me.zpct_0 = New System.Windows.Forms.PictureBox()
        Me.TimerPowDia = New System.Windows.Forms.Timer(Me.components)
        Me.Label4 = New System.Windows.Forms.Label()
        Me.zfra_0.SuspendLayout()
        CType(Me.img_pan_eclipse, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.fl_charge, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.fl_decharge, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.fl_shunt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.fl_gs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.zpct_0, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'zfra_0
        '
        Me.zfra_0.Controls.Add(Me.Label3)
        Me.zfra_0.Controls.Add(Me.lab_amperemeter)
        Me.zfra_0.Controls.Add(Me.prg_chargemeter)
        Me.zfra_0.Controls.Add(Me.lab_voltmeter)
        Me.zfra_0.Controls.Add(Me.zlbl_7)
        Me.zfra_0.Controls.Add(Me.zlbl_6)
        Me.zfra_0.Controls.Add(Me.zlbl_5)
        Me.zfra_0.Controls.Add(Me.zlbl_4)
        Me.zfra_0.Controls.Add(Me.lab_state)
        Me.zfra_0.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra_0.Location = New System.Drawing.Point(12, 431)
        Me.zfra_0.Name = "zfra_0"
        Me.zfra_0.Size = New System.Drawing.Size(584, 115)
        Me.zfra_0.TabIndex = 31
        Me.zfra_0.TabStop = False
        Me.zfra_0.Text = " "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(96, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 16)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "50%"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lab_amperemeter
        '
        Me.lab_amperemeter.AutoSize = True
        Me.lab_amperemeter.BackColor = System.Drawing.Color.Black
        Me.lab_amperemeter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lab_amperemeter.Font = New System.Drawing.Font("Verdana", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_amperemeter.ForeColor = System.Drawing.Color.LawnGreen
        Me.lab_amperemeter.Location = New System.Drawing.Point(419, 52)
        Me.lab_amperemeter.Name = "lab_amperemeter"
        Me.lab_amperemeter.Size = New System.Drawing.Size(144, 40)
        Me.lab_amperemeter.TabIndex = 3
        Me.lab_amperemeter.Text = "01.45 A"
        '
        'prg_chargemeter
        '
        Me.prg_chargemeter.BackColor = System.Drawing.Color.DimGray
        Me.prg_chargemeter.ForeColor = System.Drawing.Color.LimeGreen
        Me.prg_chargemeter.Location = New System.Drawing.Point(22, 62)
        Me.prg_chargemeter.Name = "prg_chargemeter"
        Me.prg_chargemeter.Size = New System.Drawing.Size(193, 20)
        Me.prg_chargemeter.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.prg_chargemeter.TabIndex = 7
        Me.prg_chargemeter.Value = 20
        '
        'lab_voltmeter
        '
        Me.lab_voltmeter.AutoSize = True
        Me.lab_voltmeter.BackColor = System.Drawing.Color.Black
        Me.lab_voltmeter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lab_voltmeter.Font = New System.Drawing.Font("Verdana", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_voltmeter.ForeColor = System.Drawing.Color.LawnGreen
        Me.lab_voltmeter.Location = New System.Drawing.Point(250, 52)
        Me.lab_voltmeter.Name = "lab_voltmeter"
        Me.lab_voltmeter.Size = New System.Drawing.Size(144, 40)
        Me.lab_voltmeter.TabIndex = 3
        Me.lab_voltmeter.Text = "28.45 V"
        '
        'zlbl_7
        '
        Me.zlbl_7.AutoSize = True
        Me.zlbl_7.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_7.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_7.ForeColor = System.Drawing.Color.Black
        Me.zlbl_7.Location = New System.Drawing.Point(452, 30)
        Me.zlbl_7.Name = "zlbl_7"
        Me.zlbl_7.Size = New System.Drawing.Size(64, 18)
        Me.zlbl_7.TabIndex = 1
        Me.zlbl_7.Text = "Current"
        '
        'zlbl_6
        '
        Me.zlbl_6.AutoSize = True
        Me.zlbl_6.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_6.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_6.ForeColor = System.Drawing.Color.Black
        Me.zlbl_6.Location = New System.Drawing.Point(285, 30)
        Me.zlbl_6.Name = "zlbl_6"
        Me.zlbl_6.Size = New System.Drawing.Size(63, 18)
        Me.zlbl_6.TabIndex = 1
        Me.zlbl_6.Text = "Voltage"
        '
        'zlbl_5
        '
        Me.zlbl_5.AutoSize = True
        Me.zlbl_5.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_5.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_5.ForeColor = System.Drawing.Color.Black
        Me.zlbl_5.Location = New System.Drawing.Point(185, 85)
        Me.zlbl_5.Name = "zlbl_5"
        Me.zlbl_5.Size = New System.Drawing.Size(54, 18)
        Me.zlbl_5.TabIndex = 1
        Me.zlbl_5.Text = "100%"
        '
        'zlbl_4
        '
        Me.zlbl_4.AutoSize = True
        Me.zlbl_4.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_4.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_4.ForeColor = System.Drawing.Color.Black
        Me.zlbl_4.Location = New System.Drawing.Point(17, 85)
        Me.zlbl_4.Name = "zlbl_4"
        Me.zlbl_4.Size = New System.Drawing.Size(34, 18)
        Me.zlbl_4.TabIndex = 1
        Me.zlbl_4.Text = "0%"
        '
        'lab_state
        '
        Me.lab_state.AutoSize = True
        Me.lab_state.BackColor = System.Drawing.Color.Transparent
        Me.lab_state.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_state.ForeColor = System.Drawing.Color.DarkGreen
        Me.lab_state.Location = New System.Drawing.Point(65, 30)
        Me.lab_state.Name = "lab_state"
        Me.lab_state.Size = New System.Drawing.Size(123, 23)
        Me.lab_state.TabIndex = 1
        Me.lab_state.Text = "Charging ..."
        '
        'cmd_close
        '
        Me.cmd_close.BackColor = System.Drawing.SystemColors.Control
        Me.cmd_close.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmd_close.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_close.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmd_close.Location = New System.Drawing.Point(459, 567)
        Me.cmd_close.Name = "cmd_close"
        Me.cmd_close.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmd_close.Size = New System.Drawing.Size(88, 37)
        Me.cmd_close.TabIndex = 30
        Me.cmd_close.Text = "Close"
        Me.cmd_close.UseVisualStyleBackColor = False
        '
        'lab_exc
        '
        Me.lab_exc.BackColor = System.Drawing.Color.White
        Me.lab_exc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lab_exc.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_exc.ForeColor = System.Drawing.Color.Black
        Me.lab_exc.Location = New System.Drawing.Point(375, 329)
        Me.lab_exc.Name = "lab_exc"
        Me.lab_exc.Size = New System.Drawing.Size(72, 28)
        Me.lab_exc.TabIndex = 23
        Me.lab_exc.Text = "00.00"
        Me.lab_exc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lab_puse
        '
        Me.lab_puse.BackColor = System.Drawing.Color.White
        Me.lab_puse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lab_puse.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_puse.ForeColor = System.Drawing.Color.Black
        Me.lab_puse.Location = New System.Drawing.Point(375, 178)
        Me.lab_puse.Name = "lab_puse"
        Me.lab_puse.Size = New System.Drawing.Size(72, 28)
        Me.lab_puse.TabIndex = 22
        Me.lab_puse.Text = "00.00"
        Me.lab_puse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(277, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(116, 28)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Power (W)"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lab_pava
        '
        Me.lab_pava.BackColor = System.Drawing.Color.White
        Me.lab_pava.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lab_pava.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_pava.ForeColor = System.Drawing.Color.Black
        Me.lab_pava.Location = New System.Drawing.Point(242, 178)
        Me.lab_pava.Name = "lab_pava"
        Me.lab_pava.Size = New System.Drawing.Size(72, 28)
        Me.lab_pava.TabIndex = 24
        Me.lab_pava.Text = "00.00"
        Me.lab_pava.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lab_pbat
        '
        Me.lab_pbat.BackColor = System.Drawing.Color.White
        Me.lab_pbat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lab_pbat.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_pbat.ForeColor = System.Drawing.Color.Black
        Me.lab_pbat.Location = New System.Drawing.Point(119, 317)
        Me.lab_pbat.Name = "lab_pbat"
        Me.lab_pbat.Size = New System.Drawing.Size(72, 28)
        Me.lab_pbat.TabIndex = 20
        Me.lab_pbat.Text = "00.00"
        Me.lab_pbat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lab_pgs
        '
        Me.lab_pgs.BackColor = System.Drawing.Color.White
        Me.lab_pgs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lab_pgs.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_pgs.ForeColor = System.Drawing.Color.Black
        Me.lab_pgs.Location = New System.Drawing.Point(119, 94)
        Me.lab_pgs.Name = "lab_pgs"
        Me.lab_pgs.Size = New System.Drawing.Size(72, 28)
        Me.lab_pgs.TabIndex = 18
        Me.lab_pgs.Text = "00.00"
        Me.lab_pgs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lab_disscu
        '
        Me.lab_disscu.AutoSize = True
        Me.lab_disscu.BackColor = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.lab_disscu.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_disscu.ForeColor = System.Drawing.Color.Gold
        Me.lab_disscu.Location = New System.Drawing.Point(513, 251)
        Me.lab_disscu.Name = "lab_disscu"
        Me.lab_disscu.Size = New System.Drawing.Size(54, 18)
        Me.lab_disscu.TabIndex = 17
        Me.lab_disscu.Text = "00.00"
        '
        'lab_pbat_diss
        '
        Me.lab_pbat_diss.AutoSize = True
        Me.lab_pbat_diss.BackColor = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.lab_pbat_diss.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_pbat_diss.ForeColor = System.Drawing.Color.Gold
        Me.lab_pbat_diss.Location = New System.Drawing.Point(239, 322)
        Me.lab_pbat_diss.Name = "lab_pbat_diss"
        Me.lab_pbat_diss.Size = New System.Drawing.Size(54, 18)
        Me.lab_pbat_diss.TabIndex = 16
        Me.lab_pbat_diss.Text = "00.00"
        '
        'lab_diss_charge
        '
        Me.lab_diss_charge.AutoSize = True
        Me.lab_diss_charge.BackColor = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.lab_diss_charge.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_diss_charge.ForeColor = System.Drawing.Color.Gold
        Me.lab_diss_charge.Location = New System.Drawing.Point(239, 322)
        Me.lab_diss_charge.Name = "lab_diss_charge"
        Me.lab_diss_charge.Size = New System.Drawing.Size(54, 18)
        Me.lab_diss_charge.TabIndex = 15
        Me.lab_diss_charge.Text = "00.00"
        '
        'lab_pgs_diss
        '
        Me.lab_pgs_diss.AutoSize = True
        Me.lab_pgs_diss.BackColor = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.lab_pgs_diss.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_pgs_diss.ForeColor = System.Drawing.Color.Gold
        Me.lab_pgs_diss.Location = New System.Drawing.Point(239, 99)
        Me.lab_pgs_diss.Name = "lab_pgs_diss"
        Me.lab_pgs_diss.Size = New System.Drawing.Size(54, 18)
        Me.lab_pgs_diss.TabIndex = 14
        Me.lab_pgs_diss.Text = "00.00"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Gold
        Me.Label1.Location = New System.Drawing.Point(430, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 18)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Dissipation (W)"
        '
        'zlbl_0
        '
        Me.zlbl_0.AutoSize = True
        Me.zlbl_0.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_0.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_0.ForeColor = System.Drawing.Color.ForestGreen
        Me.zlbl_0.Location = New System.Drawing.Point(480, 374)
        Me.zlbl_0.Name = "zlbl_0"
        Me.zlbl_0.Size = New System.Drawing.Size(91, 18)
        Me.zlbl_0.TabIndex = 12
        Me.zlbl_0.Text = "Dissipator"
        '
        'zlbl_3
        '
        Me.zlbl_3.AutoSize = True
        Me.zlbl_3.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_3.ForeColor = System.Drawing.Color.DarkRed
        Me.zlbl_3.Location = New System.Drawing.Point(491, 120)
        Me.zlbl_3.Name = "zlbl_3"
        Me.zlbl_3.Size = New System.Drawing.Size(72, 18)
        Me.zlbl_3.TabIndex = 11
        Me.zlbl_3.Text = "Payload"
        '
        'zlbl_2
        '
        Me.zlbl_2.AutoSize = True
        Me.zlbl_2.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_2.ForeColor = System.Drawing.Color.DarkOrange
        Me.zlbl_2.Location = New System.Drawing.Point(43, 383)
        Me.zlbl_2.Name = "zlbl_2"
        Me.zlbl_2.Size = New System.Drawing.Size(68, 18)
        Me.zlbl_2.TabIndex = 10
        Me.zlbl_2.Text = "Battery"
        '
        'zlbl_1
        '
        Me.zlbl_1.AutoSize = True
        Me.zlbl_1.BackColor = System.Drawing.Color.Transparent
        Me.zlbl_1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_1.ForeColor = System.Drawing.Color.Navy
        Me.zlbl_1.Location = New System.Drawing.Point(29, 233)
        Me.zlbl_1.Name = "zlbl_1"
        Me.zlbl_1.Size = New System.Drawing.Size(109, 18)
        Me.zlbl_1.TabIndex = 19
        Me.zlbl_1.Text = "Solar panels"
        '
        'img_pan_eclipse
        '
        Me.img_pan_eclipse.Image = Global.Simusat.Resources.pann_eclipse
        Me.img_pan_eclipse.Location = New System.Drawing.Point(32, 46)
        Me.img_pan_eclipse.Name = "img_pan_eclipse"
        Me.img_pan_eclipse.Size = New System.Drawing.Size(65, 184)
        Me.img_pan_eclipse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.img_pan_eclipse.TabIndex = 29
        Me.img_pan_eclipse.TabStop = False
        '
        'fl_charge
        '
        Me.fl_charge.Image = Global.Simusat.Resources.fleche_charge
        Me.fl_charge.Location = New System.Drawing.Point(131, 300)
        Me.fl_charge.Name = "fl_charge"
        Me.fl_charge.Size = New System.Drawing.Size(52, 9)
        Me.fl_charge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.fl_charge.TabIndex = 28
        Me.fl_charge.TabStop = False
        '
        'fl_decharge
        '
        Me.fl_decharge.Image = Global.Simusat.Resources.fleche_decharge
        Me.fl_decharge.Location = New System.Drawing.Point(131, 300)
        Me.fl_decharge.Name = "fl_decharge"
        Me.fl_decharge.Size = New System.Drawing.Size(52, 9)
        Me.fl_decharge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.fl_decharge.TabIndex = 27
        Me.fl_decharge.TabStop = False
        '
        'fl_shunt
        '
        Me.fl_shunt.Image = Global.Simusat.Resources.fleche_shunt
        Me.fl_shunt.Location = New System.Drawing.Point(384, 313)
        Me.fl_shunt.Name = "fl_shunt"
        Me.fl_shunt.Size = New System.Drawing.Size(52, 9)
        Me.fl_shunt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.fl_shunt.TabIndex = 26
        Me.fl_shunt.TabStop = False
        '
        'fl_gs
        '
        Me.fl_gs.Image = Global.Simusat.Resources.fleche_gs
        Me.fl_gs.Location = New System.Drawing.Point(130, 71)
        Me.fl_gs.Name = "fl_gs"
        Me.fl_gs.Size = New System.Drawing.Size(52, 9)
        Me.fl_gs.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.fl_gs.TabIndex = 25
        Me.fl_gs.TabStop = False
        '
        'zpct_0
        '
        Me.zpct_0.Image = CType(resources.GetObject("zpct_0.Image"), System.Drawing.Image)
        Me.zpct_0.Location = New System.Drawing.Point(12, 12)
        Me.zpct_0.Name = "zpct_0"
        Me.zpct_0.Size = New System.Drawing.Size(584, 400)
        Me.zpct_0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.zpct_0.TabIndex = 9
        Me.zpct_0.TabStop = False
        '
        'TimerPowDia
        '
        '
        'Label4
        '
        Me.Label4.Enabled = False
        Me.Label4.Location = New System.Drawing.Point(12, 549)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(424, 55)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "Label4"
        Me.Label4.Visible = False
        '
        'frmPowerDiagram
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(607, 621)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.zfra_0)
        Me.Controls.Add(Me.cmd_close)
        Me.Controls.Add(Me.img_pan_eclipse)
        Me.Controls.Add(Me.fl_charge)
        Me.Controls.Add(Me.fl_decharge)
        Me.Controls.Add(Me.fl_shunt)
        Me.Controls.Add(Me.fl_gs)
        Me.Controls.Add(Me.lab_exc)
        Me.Controls.Add(Me.lab_puse)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lab_pava)
        Me.Controls.Add(Me.lab_pbat)
        Me.Controls.Add(Me.lab_pgs)
        Me.Controls.Add(Me.lab_disscu)
        Me.Controls.Add(Me.lab_pbat_diss)
        Me.Controls.Add(Me.lab_diss_charge)
        Me.Controls.Add(Me.lab_pgs_diss)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.zlbl_0)
        Me.Controls.Add(Me.zlbl_3)
        Me.Controls.Add(Me.zlbl_2)
        Me.Controls.Add(Me.zlbl_1)
        Me.Controls.Add(Me.zpct_0)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPowerDiagram"
        Me.Text = "Energy Sub-System Simulation"
        Me.zfra_0.ResumeLayout(False)
        Me.zfra_0.PerformLayout()
        CType(Me.img_pan_eclipse, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.fl_charge, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.fl_decharge, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.fl_shunt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.fl_gs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.zpct_0, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents zfra_0 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lab_amperemeter As System.Windows.Forms.Label
    Friend WithEvents prg_chargemeter As System.Windows.Forms.ProgressBar
    Friend WithEvents lab_voltmeter As System.Windows.Forms.Label
    Friend WithEvents zlbl_7 As System.Windows.Forms.Label
    Friend WithEvents zlbl_6 As System.Windows.Forms.Label
    Friend WithEvents zlbl_5 As System.Windows.Forms.Label
    Friend WithEvents zlbl_4 As System.Windows.Forms.Label
    Friend WithEvents lab_state As System.Windows.Forms.Label
    Public WithEvents cmd_close As System.Windows.Forms.Button
    Friend WithEvents img_pan_eclipse As System.Windows.Forms.PictureBox
    Friend WithEvents fl_charge As System.Windows.Forms.PictureBox
    Friend WithEvents fl_decharge As System.Windows.Forms.PictureBox
    Friend WithEvents fl_shunt As System.Windows.Forms.PictureBox
    Friend WithEvents fl_gs As System.Windows.Forms.PictureBox
    Friend WithEvents lab_exc As System.Windows.Forms.Label
    Friend WithEvents lab_puse As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lab_pava As System.Windows.Forms.Label
    Friend WithEvents lab_pbat As System.Windows.Forms.Label
    Friend WithEvents lab_pgs As System.Windows.Forms.Label
    Friend WithEvents lab_disscu As System.Windows.Forms.Label
    Friend WithEvents lab_pbat_diss As System.Windows.Forms.Label
    Friend WithEvents lab_diss_charge As System.Windows.Forms.Label
    Friend WithEvents lab_pgs_diss As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents zlbl_0 As System.Windows.Forms.Label
    Friend WithEvents zlbl_3 As System.Windows.Forms.Label
    Friend WithEvents zlbl_2 As System.Windows.Forms.Label
    Friend WithEvents zlbl_1 As System.Windows.Forms.Label
    Friend WithEvents zpct_0 As System.Windows.Forms.PictureBox
    Friend WithEvents TimerPowDia As System.Windows.Forms.Timer
    Friend WithEvents Label4 As Label
End Class
