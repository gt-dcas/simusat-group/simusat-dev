<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmOrbit
#Region "Code g�n�r� par le Concepteur Windows Form "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Cet appel est requis par le Concepteur Windows Form.
		InitializeComponent()
	End Sub
	'Form remplace la m�thode Dispose pour nettoyer la liste des composants.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requise par le Concepteur Windows Form
	Private components As System.ComponentModel.IContainer
    Public WithEvents optType_Orbit_1 As System.Windows.Forms.RadioButton
    Public WithEvents optType_Orbit_0 As System.Windows.Forms.RadioButton
    Public WithEvents optType_Orbit_2 As System.Windows.Forms.RadioButton
    Public WithEvents optType_Orbit_3 As System.Windows.Forms.RadioButton
    Public WithEvents optType_Orbit_4 As System.Windows.Forms.RadioButton
    Public WithEvents optType_Orbit_5 As System.Windows.Forms.RadioButton
    Public WithEvents optType_Orbit_6 As System.Windows.Forms.RadioButton
    Public WithEvents optType_Orbit_7 As System.Windows.Forms.RadioButton
    Public WithEvents txtSSAltitude As System.Windows.Forms.TextBox
    Public WithEvents txtSSInclination As System.Windows.Forms.TextBox
    Public WithEvents lblSSAlt As System.Windows.Forms.Label
    Public WithEvents lblSSIncl As System.Windows.Forms.Label
    Public WithEvents fraType_Orbit As System.Windows.Forms.GroupBox
    Public WithEvents optMain_0 As System.Windows.Forms.RadioButton
    Public WithEvents optMain_1 As System.Windows.Forms.RadioButton
    Public WithEvents txtName As System.Windows.Forms.TextBox
    Public WithEvents txtA As System.Windows.Forms.TextBox
    Public WithEvents txtE As System.Windows.Forms.TextBox
    Public WithEvents txtI As System.Windows.Forms.TextBox
    Public WithEvents txtRAAN As System.Windows.Forms.TextBox
    Public WithEvents txtMu As System.Windows.Forms.TextBox
    Public WithEvents txtW As System.Windows.Forms.TextBox
    Public WithEvents cmbSemiMajor As System.Windows.Forms.ComboBox
    Public WithEvents cmbEccentricity As System.Windows.Forms.ComboBox
    Public WithEvents txtNoradEpoch As System.Windows.Forms.TextBox
    Public WithEvents cmbAnomaly As System.Windows.Forms.ComboBox
    Public WithEvents txtBstar As System.Windows.Forms.TextBox
    Public WithEvents cmbRAAN As System.Windows.Forms.ComboBox
    Public WithEvents cmbTime As System.Windows.Forms.ComboBox
    Public WithEvents txtEpochDate As System.Windows.Forms.MaskedTextBox
    Public WithEvents txtEpochTime As System.Windows.Forms.MaskedTextBox
    Public WithEvents lblI As System.Windows.Forms.Label
    Public WithEvents lblW As System.Windows.Forms.Label
    Public WithEvents lblEpoch As System.Windows.Forms.Label
    Public WithEvents lblAUnits As System.Windows.Forms.Label
    Public WithEvents lblEUnits As System.Windows.Forms.Label
    Public WithEvents lblIUnits As System.Windows.Forms.Label
    Public WithEvents lblWUnits As System.Windows.Forms.Label
    Public WithEvents lblRAANUnits As System.Windows.Forms.Label
    Public WithEvents lblAnomUnits As System.Windows.Forms.Label
    Public WithEvents lblBstar As System.Windows.Forms.Label
    Public WithEvents fraElements As System.Windows.Forms.GroupBox
    Public WithEvents zlbl_4 As System.Windows.Forms.Label
    Public WithEvents zfra As System.Windows.Forms.GroupBox
    Public WithEvents cmbPrinAxis As System.Windows.Forms.ComboBox
    Public WithEvents cmbPrinOrient As System.Windows.Forms.ComboBox
    Public WithEvents cmbOptAxis As System.Windows.Forms.ComboBox
    Public WithEvents cmbOptOrient As System.Windows.Forms.ComboBox
    Public WithEvents zlbl_8 As System.Windows.Forms.Label
    Public WithEvents zlbl_7 As System.Windows.Forms.Label
    Public WithEvents zlbl_0 As System.Windows.Forms.Label
    Public WithEvents fraAttitude As System.Windows.Forms.GroupBox
    Public WithEvents cmbFwdTrack As System.Windows.Forms.ComboBox
    Public WithEvents cmbBckTrack As System.Windows.Forms.ComboBox
    Public WithEvents chkFootprint As System.Windows.Forms.CheckBox
    Public WithEvents lblFwdTrack As System.Windows.Forms.Label
    Public WithEvents lblBckTrack As System.Windows.Forms.Label
    Public WithEvents zfrm_2 As System.Windows.Forms.GroupBox
    'Public WithEvents ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
    'REMARQUE�: la proc�dure suivante est requise par le Concepteur Windows Form
    'Elle peut �tre modifi�e � l'aide du Concepteur Windows Form.
    'Ne la modifiez pas � l'aide de l'�diteur de code.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend2 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOrbit))
        Me.optType_Orbit_1 = New System.Windows.Forms.RadioButton()
        Me.zlbl_7 = New System.Windows.Forms.Label()
        Me.zfra = New System.Windows.Forms.GroupBox()
        Me.cmdOrbitOpen = New System.Windows.Forms.Button()
        Me.cmdOrbitSave = New System.Windows.Forms.Button()
        Me.fraDatabase = New System.Windows.Forms.GroupBox()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.cmbType_Sat = New System.Windows.Forms.ComboBox()
        Me.lstSatellites = New System.Windows.Forms.ListBox()
        Me.txtNORAD = New System.Windows.Forms.TextBox()
        Me.cmdUpdateNORAD = New System.Windows.Forms.Button()
        Me.zlbl_1 = New System.Windows.Forms.Label()
        Me.etat_connex = New System.Windows.Forms.Label()
        Me.zlbl_3 = New System.Windows.Forms.Label()
        Me.zlbl_2 = New System.Windows.Forms.Label()
        Me.optMain_0 = New System.Windows.Forms.RadioButton()
        Me.optMain_1 = New System.Windows.Forms.RadioButton()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.fraType_Orbit = New System.Windows.Forms.GroupBox()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.optType_Orbit_0 = New System.Windows.Forms.RadioButton()
        Me.optType_Orbit_2 = New System.Windows.Forms.RadioButton()
        Me.optType_Orbit_3 = New System.Windows.Forms.RadioButton()
        Me.optType_Orbit_4 = New System.Windows.Forms.RadioButton()
        Me.optType_Orbit_5 = New System.Windows.Forms.RadioButton()
        Me.optType_Orbit_6 = New System.Windows.Forms.RadioButton()
        Me.optType_Orbit_7 = New System.Windows.Forms.RadioButton()
        Me.txtSSAltitude = New System.Windows.Forms.TextBox()
        Me.txtSSInclination = New System.Windows.Forms.TextBox()
        Me.lblSSAlt = New System.Windows.Forms.Label()
        Me.lblSSIncl = New System.Windows.Forms.Label()
        Me.fraElements = New System.Windows.Forms.GroupBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.txtA = New System.Windows.Forms.TextBox()
        Me.txtE = New System.Windows.Forms.TextBox()
        Me.txtI = New System.Windows.Forms.TextBox()
        Me.txtRAAN = New System.Windows.Forms.TextBox()
        Me.txtMu = New System.Windows.Forms.TextBox()
        Me.txtW = New System.Windows.Forms.TextBox()
        Me.cmbSemiMajor = New System.Windows.Forms.ComboBox()
        Me.cmbEccentricity = New System.Windows.Forms.ComboBox()
        Me.txtNoradEpoch = New System.Windows.Forms.TextBox()
        Me.cmbAnomaly = New System.Windows.Forms.ComboBox()
        Me.txtBstar = New System.Windows.Forms.TextBox()
        Me.cmbRAAN = New System.Windows.Forms.ComboBox()
        Me.cmbTime = New System.Windows.Forms.ComboBox()
        Me.txtEpochDate = New System.Windows.Forms.MaskedTextBox()
        Me.txtEpochTime = New System.Windows.Forms.MaskedTextBox()
        Me.lblI = New System.Windows.Forms.Label()
        Me.lblW = New System.Windows.Forms.Label()
        Me.lblEpoch = New System.Windows.Forms.Label()
        Me.lblAUnits = New System.Windows.Forms.Label()
        Me.lblEUnits = New System.Windows.Forms.Label()
        Me.lblIUnits = New System.Windows.Forms.Label()
        Me.lblWUnits = New System.Windows.Forms.Label()
        Me.lblRAANUnits = New System.Windows.Forms.Label()
        Me.lblAnomUnits = New System.Windows.Forms.Label()
        Me.lblBstar = New System.Windows.Forms.Label()
        Me.zlbl_4 = New System.Windows.Forms.Label()
        Me.fraAttitude = New System.Windows.Forms.GroupBox()
        Me.cmbPrinAxis = New System.Windows.Forms.ComboBox()
        Me.cmbPrinOrient = New System.Windows.Forms.ComboBox()
        Me.cmbOptAxis = New System.Windows.Forms.ComboBox()
        Me.cmbOptOrient = New System.Windows.Forms.ComboBox()
        Me.zlbl_8 = New System.Windows.Forms.Label()
        Me.zlbl_0 = New System.Windows.Forms.Label()
        Me.zfrm_2 = New System.Windows.Forms.GroupBox()
        Me.cmbBckTrackCO = New System.Windows.Forms.ComboBox()
        Me.cmbFwdTrackCO = New System.Windows.Forms.ComboBox()
        Me.previewColor = New System.Windows.Forms.PictureBox()
        Me.ChangeColor = New System.Windows.Forms.Button()
        Me.cmbFwdTrack = New System.Windows.Forms.ComboBox()
        Me.cmbBckTrack = New System.Windows.Forms.ComboBox()
        Me.chkFootprint = New System.Windows.Forms.CheckBox()
        Me.lblFwdTrack = New System.Windows.Forms.Label()
        Me.lblBckTrack = New System.Windows.Forms.Label()
        Me.cmdOk = New System.Windows.Forms.Button()
        Me.ColorBox = New System.Windows.Forms.ColorDialog()
        Me.tabOrbit = New System.Windows.Forms.TabControl()
        Me.tabOrbit_Orbit = New System.Windows.Forms.TabPage()
        Me.tabOrbit_Environment = New System.Windows.Forms.TabPage()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.env_graph = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.CERES_month = New System.Windows.Forms.ComboBox()
        Me.CERES_graph = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.zgrp_3 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Earth_Ce_check = New System.Windows.Forms.CheckBox()
        Me.env_txt_ir = New System.Windows.Forms.TextBox()
        Me.env_cmd_isoterre = New System.Windows.Forms.Button()
        Me.env_list_months_ir = New System.Windows.Forms.ComboBox()
        Me.zgrp_2 = New System.Windows.Forms.GroupBox()
        Me.Alb_Ce_check = New System.Windows.Forms.CheckBox()
        Me.zlbl_12 = New System.Windows.Forms.Label()
        Me.env_cmd_sin = New System.Windows.Forms.Button()
        Me.env_txt_var = New System.Windows.Forms.TextBox()
        Me.env_txt_albedo = New System.Windows.Forms.TextBox()
        Me.env_list_months_alb = New System.Windows.Forms.ComboBox()
        Me.zgrp_1 = New System.Windows.Forms.GroupBox()
        Me.zlbl_11 = New System.Windows.Forms.Label()
        Me.zlbl_13 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.zlbl_10 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.zlbl_9 = New System.Windows.Forms.Label()
        Me.env_txt_flux_1 = New System.Windows.Forms.TextBox()
        Me.env_txt_flux_0 = New System.Windows.Forms.TextBox()
        Me.dlgOrbitOpen = New System.Windows.Forms.OpenFileDialog()
        Me.dlgOrbitSave = New System.Windows.Forms.SaveFileDialog()
        Me.orbit_lblmsg = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.zfra.SuspendLayout()
        Me.fraDatabase.SuspendLayout()
        Me.fraType_Orbit.SuspendLayout()
        Me.fraElements.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.fraAttitude.SuspendLayout()
        Me.zfrm_2.SuspendLayout()
        CType(Me.previewColor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabOrbit.SuspendLayout()
        Me.tabOrbit_Orbit.SuspendLayout()
        Me.tabOrbit_Environment.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.env_graph, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.CERES_graph, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.zgrp_3.SuspendLayout()
        Me.zgrp_2.SuspendLayout()
        Me.zgrp_1.SuspendLayout()
        Me.SuspendLayout()
        '
        'optType_Orbit_1
        '
        Me.optType_Orbit_1.BackColor = System.Drawing.SystemColors.Control
        Me.optType_Orbit_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.optType_Orbit_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optType_Orbit_1.Location = New System.Drawing.Point(290, 16)
        Me.optType_Orbit_1.Name = "optType_Orbit_1"
        Me.optType_Orbit_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optType_Orbit_1.Size = New System.Drawing.Size(130, 20)
        Me.optType_Orbit_1.TabIndex = 68
        Me.optType_Orbit_1.Text = "Molniya"
        Me.optType_Orbit_1.UseVisualStyleBackColor = False
        '
        'zlbl_7
        '
        Me.zlbl_7.AutoSize = True
        Me.zlbl_7.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_7.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_7.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_7.Location = New System.Drawing.Point(3, 55)
        Me.zlbl_7.Name = "zlbl_7"
        Me.zlbl_7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_7.Size = New System.Drawing.Size(130, 18)
        Me.zlbl_7.TabIndex = 16
        Me.zlbl_7.Text = "Secondary direction"
        Me.zlbl_7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zfra
        '
        Me.zfra.BackColor = System.Drawing.SystemColors.Control
        Me.zfra.Controls.Add(Me.cmdOrbitOpen)
        Me.zfra.Controls.Add(Me.cmdOrbitSave)
        Me.zfra.Controls.Add(Me.fraDatabase)
        Me.zfra.Controls.Add(Me.optMain_0)
        Me.zfra.Controls.Add(Me.optMain_1)
        Me.zfra.Controls.Add(Me.txtName)
        Me.zfra.Controls.Add(Me.fraType_Orbit)
        Me.zfra.Controls.Add(Me.fraElements)
        Me.zfra.Controls.Add(Me.zlbl_4)
        Me.zfra.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.zfra.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfra.ForeColor = System.Drawing.Color.Maroon
        Me.zfra.Location = New System.Drawing.Point(17, 15)
        Me.zfra.Name = "zfra"
        Me.zfra.Padding = New System.Windows.Forms.Padding(0)
        Me.zfra.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfra.Size = New System.Drawing.Size(963, 345)
        Me.zfra.TabIndex = 18
        Me.zfra.TabStop = False
        Me.zfra.Text = "Orbit"
        '
        'cmdOrbitOpen
        '
        Me.cmdOrbitOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOrbitOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOrbitOpen.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdOrbitOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOrbitOpen.Location = New System.Drawing.Point(285, 22)
        Me.cmdOrbitOpen.Name = "cmdOrbitOpen"
        Me.cmdOrbitOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOrbitOpen.Size = New System.Drawing.Size(100, 32)
        Me.cmdOrbitOpen.TabIndex = 75
        Me.cmdOrbitOpen.Text = "Open"
        Me.cmdOrbitOpen.UseVisualStyleBackColor = False
        '
        'cmdOrbitSave
        '
        Me.cmdOrbitSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOrbitSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOrbitSave.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdOrbitSave.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdOrbitSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOrbitSave.Location = New System.Drawing.Point(391, 23)
        Me.cmdOrbitSave.Name = "cmdOrbitSave"
        Me.cmdOrbitSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOrbitSave.Size = New System.Drawing.Size(100, 31)
        Me.cmdOrbitSave.TabIndex = 76
        Me.cmdOrbitSave.Text = "Export"
        Me.cmdOrbitSave.UseVisualStyleBackColor = False
        '
        'fraDatabase
        '
        Me.fraDatabase.BackColor = System.Drawing.SystemColors.Control
        Me.fraDatabase.Controls.Add(Me.ProgressBar1)
        Me.fraDatabase.Controls.Add(Me.cmbType_Sat)
        Me.fraDatabase.Controls.Add(Me.lstSatellites)
        Me.fraDatabase.Controls.Add(Me.txtNORAD)
        Me.fraDatabase.Controls.Add(Me.cmdUpdateNORAD)
        Me.fraDatabase.Controls.Add(Me.zlbl_1)
        Me.fraDatabase.Controls.Add(Me.etat_connex)
        Me.fraDatabase.Controls.Add(Me.zlbl_3)
        Me.fraDatabase.Controls.Add(Me.zlbl_2)
        Me.fraDatabase.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraDatabase.Location = New System.Drawing.Point(18, 82)
        Me.fraDatabase.Name = "fraDatabase"
        Me.fraDatabase.Padding = New System.Windows.Forms.Padding(0)
        Me.fraDatabase.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraDatabase.Size = New System.Drawing.Size(473, 245)
        Me.fraDatabase.TabIndex = 74
        Me.fraDatabase.TabStop = False
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(19, 119)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(202, 10)
        Me.ProgressBar1.TabIndex = 58
        '
        'cmbType_Sat
        '
        Me.cmbType_Sat.BackColor = System.Drawing.SystemColors.Window
        Me.cmbType_Sat.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbType_Sat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbType_Sat.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbType_Sat.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbType_Sat.Location = New System.Drawing.Point(18, 35)
        Me.cmbType_Sat.Name = "cmbType_Sat"
        Me.cmbType_Sat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbType_Sat.Size = New System.Drawing.Size(264, 24)
        Me.cmbType_Sat.TabIndex = 53
        '
        'lstSatellites
        '
        Me.lstSatellites.BackColor = System.Drawing.SystemColors.Window
        Me.lstSatellites.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstSatellites.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstSatellites.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstSatellites.ItemHeight = 16
        Me.lstSatellites.Location = New System.Drawing.Point(293, 38)
        Me.lstSatellites.Name = "lstSatellites"
        Me.lstSatellites.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstSatellites.Size = New System.Drawing.Size(158, 132)
        Me.lstSatellites.Sorted = True
        Me.lstSatellites.TabIndex = 52
        '
        'txtNORAD
        '
        Me.txtNORAD.AcceptsReturn = True
        Me.txtNORAD.BackColor = System.Drawing.SystemColors.Window
        Me.txtNORAD.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNORAD.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNORAD.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNORAD.Location = New System.Drawing.Point(14, 176)
        Me.txtNORAD.MaxLength = 0
        Me.txtNORAD.Multiline = True
        Me.txtNORAD.Name = "txtNORAD"
        Me.txtNORAD.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNORAD.Size = New System.Drawing.Size(437, 54)
        Me.txtNORAD.TabIndex = 51
        Me.txtNORAD.Text = "COMS 1                  " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "1 36744U 10032A   12024.75077558 -.00000356  00000-0  1" &
    "0000-3 0  3008" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "2 36744   0.0150  79.7669 0001099 251.3913 190.7496  1.00271143 " &
    " 5931"
        '
        'cmdUpdateNORAD
        '
        Me.cmdUpdateNORAD.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUpdateNORAD.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdUpdateNORAD.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUpdateNORAD.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdUpdateNORAD.Location = New System.Drawing.Point(19, 70)
        Me.cmdUpdateNORAD.Name = "cmdUpdateNORAD"
        Me.cmdUpdateNORAD.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdUpdateNORAD.Size = New System.Drawing.Size(202, 27)
        Me.cmdUpdateNORAD.TabIndex = 50
        Me.cmdUpdateNORAD.Text = "Update NORAD TLEs database"
        Me.cmdUpdateNORAD.UseVisualStyleBackColor = False
        '
        'zlbl_1
        '
        Me.zlbl_1.AutoSize = True
        Me.zlbl_1.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_1.Location = New System.Drawing.Point(20, 14)
        Me.zlbl_1.Name = "zlbl_1"
        Me.zlbl_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_1.Size = New System.Drawing.Size(115, 16)
        Me.zlbl_1.TabIndex = 57
        Me.zlbl_1.Text = "Type of satellite"
        '
        'etat_connex
        '
        Me.etat_connex.AutoSize = True
        Me.etat_connex.BackColor = System.Drawing.SystemColors.Control
        Me.etat_connex.Cursor = System.Windows.Forms.Cursors.Default
        Me.etat_connex.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.etat_connex.ForeColor = System.Drawing.Color.Navy
        Me.etat_connex.Location = New System.Drawing.Point(16, 102)
        Me.etat_connex.Name = "etat_connex"
        Me.etat_connex.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.etat_connex.Size = New System.Drawing.Size(0, 14)
        Me.etat_connex.TabIndex = 56
        '
        'zlbl_3
        '
        Me.zlbl_3.AutoSize = True
        Me.zlbl_3.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_3.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_3.Location = New System.Drawing.Point(20, 155)
        Me.zlbl_3.Name = "zlbl_3"
        Me.zlbl_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_3.Size = New System.Drawing.Size(208, 16)
        Me.zlbl_3.TabIndex = 55
        Me.zlbl_3.Text = "NORAD Two Line Elements Set"
        '
        'zlbl_2
        '
        Me.zlbl_2.AutoSize = True
        Me.zlbl_2.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_2.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_2.Location = New System.Drawing.Point(306, 18)
        Me.zlbl_2.Name = "zlbl_2"
        Me.zlbl_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_2.Size = New System.Drawing.Size(113, 16)
        Me.zlbl_2.TabIndex = 54
        Me.zlbl_2.Text = "List of satellites"
        '
        'optMain_0
        '
        Me.optMain_0.BackColor = System.Drawing.SystemColors.Control
        Me.optMain_0.Checked = True
        Me.optMain_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.optMain_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optMain_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optMain_0.Location = New System.Drawing.Point(21, 66)
        Me.optMain_0.Name = "optMain_0"
        Me.optMain_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optMain_0.Size = New System.Drawing.Size(127, 22)
        Me.optMain_0.TabIndex = 47
        Me.optMain_0.TabStop = True
        Me.optMain_0.Text = "Enter the orbit"
        Me.optMain_0.UseVisualStyleBackColor = False
        '
        'optMain_1
        '
        Me.optMain_1.BackColor = System.Drawing.SystemColors.Control
        Me.optMain_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.optMain_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optMain_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optMain_1.Location = New System.Drawing.Point(173, 65)
        Me.optMain_1.Name = "optMain_1"
        Me.optMain_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optMain_1.Size = New System.Drawing.Size(197, 24)
        Me.optMain_1.TabIndex = 46
        Me.optMain_1.TabStop = True
        Me.optMain_1.Text = "Load a NORAD TLE set"
        Me.optMain_1.UseVisualStyleBackColor = False
        '
        'txtName
        '
        Me.txtName.AcceptsReturn = True
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtName.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtName.Location = New System.Drawing.Point(76, 23)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtName.Size = New System.Drawing.Size(179, 26)
        Me.txtName.TabIndex = 45
        Me.txtName.Text = "Orbit_1"
        '
        'fraType_Orbit
        '
        Me.fraType_Orbit.BackColor = System.Drawing.SystemColors.Control
        Me.fraType_Orbit.Controls.Add(Me.lblDescription)
        Me.fraType_Orbit.Controls.Add(Me.optType_Orbit_1)
        Me.fraType_Orbit.Controls.Add(Me.optType_Orbit_0)
        Me.fraType_Orbit.Controls.Add(Me.optType_Orbit_2)
        Me.fraType_Orbit.Controls.Add(Me.optType_Orbit_3)
        Me.fraType_Orbit.Controls.Add(Me.optType_Orbit_4)
        Me.fraType_Orbit.Controls.Add(Me.optType_Orbit_5)
        Me.fraType_Orbit.Controls.Add(Me.optType_Orbit_6)
        Me.fraType_Orbit.Controls.Add(Me.optType_Orbit_7)
        Me.fraType_Orbit.Controls.Add(Me.txtSSAltitude)
        Me.fraType_Orbit.Controls.Add(Me.txtSSInclination)
        Me.fraType_Orbit.Controls.Add(Me.lblSSAlt)
        Me.fraType_Orbit.Controls.Add(Me.lblSSIncl)
        Me.fraType_Orbit.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraType_Orbit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraType_Orbit.Location = New System.Drawing.Point(18, 84)
        Me.fraType_Orbit.Name = "fraType_Orbit"
        Me.fraType_Orbit.Padding = New System.Windows.Forms.Padding(0)
        Me.fraType_Orbit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraType_Orbit.Size = New System.Drawing.Size(437, 245)
        Me.fraType_Orbit.TabIndex = 58
        Me.fraType_Orbit.TabStop = False
        '
        'lblDescription
        '
        Me.lblDescription.BackColor = System.Drawing.Color.Transparent
        Me.lblDescription.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescription.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblDescription.Location = New System.Drawing.Point(15, 156)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescription.Size = New System.Drawing.Size(411, 86)
        Me.lblDescription.TabIndex = 73
        '
        'optType_Orbit_0
        '
        Me.optType_Orbit_0.BackColor = System.Drawing.SystemColors.Control
        Me.optType_Orbit_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.optType_Orbit_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optType_Orbit_0.Location = New System.Drawing.Point(12, 16)
        Me.optType_Orbit_0.Name = "optType_Orbit_0"
        Me.optType_Orbit_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optType_Orbit_0.Size = New System.Drawing.Size(146, 20)
        Me.optType_Orbit_0.TabIndex = 67
        Me.optType_Orbit_0.Text = "Geostationary"
        Me.optType_Orbit_0.UseVisualStyleBackColor = False
        '
        'optType_Orbit_2
        '
        Me.optType_Orbit_2.BackColor = System.Drawing.SystemColors.Control
        Me.optType_Orbit_2.Cursor = System.Windows.Forms.Cursors.Default
        Me.optType_Orbit_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optType_Orbit_2.Location = New System.Drawing.Point(12, 40)
        Me.optType_Orbit_2.Name = "optType_Orbit_2"
        Me.optType_Orbit_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optType_Orbit_2.Size = New System.Drawing.Size(190, 20)
        Me.optType_Orbit_2.TabIndex = 66
        Me.optType_Orbit_2.Text = "Sun synchronous"
        Me.optType_Orbit_2.UseVisualStyleBackColor = False
        '
        'optType_Orbit_3
        '
        Me.optType_Orbit_3.BackColor = System.Drawing.SystemColors.Control
        Me.optType_Orbit_3.Cursor = System.Windows.Forms.Cursors.Default
        Me.optType_Orbit_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optType_Orbit_3.Location = New System.Drawing.Point(290, 40)
        Me.optType_Orbit_3.Name = "optType_Orbit_3"
        Me.optType_Orbit_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optType_Orbit_3.Size = New System.Drawing.Size(145, 20)
        Me.optType_Orbit_3.TabIndex = 65
        Me.optType_Orbit_3.Text = "Critically inclined"
        Me.optType_Orbit_3.UseVisualStyleBackColor = False
        '
        'optType_Orbit_4
        '
        Me.optType_Orbit_4.BackColor = System.Drawing.SystemColors.Control
        Me.optType_Orbit_4.Cursor = System.Windows.Forms.Cursors.Default
        Me.optType_Orbit_4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optType_Orbit_4.Location = New System.Drawing.Point(12, 66)
        Me.optType_Orbit_4.Name = "optType_Orbit_4"
        Me.optType_Orbit_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optType_Orbit_4.Size = New System.Drawing.Size(272, 20)
        Me.optType_Orbit_4.TabIndex = 64
        Me.optType_Orbit_4.Text = "Critically inclined, sun synchronous"
        Me.optType_Orbit_4.UseVisualStyleBackColor = False
        '
        'optType_Orbit_5
        '
        Me.optType_Orbit_5.BackColor = System.Drawing.SystemColors.Control
        Me.optType_Orbit_5.Cursor = System.Windows.Forms.Cursors.Default
        Me.optType_Orbit_5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optType_Orbit_5.Location = New System.Drawing.Point(12, 92)
        Me.optType_Orbit_5.Name = "optType_Orbit_5"
        Me.optType_Orbit_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optType_Orbit_5.Size = New System.Drawing.Size(190, 20)
        Me.optType_Orbit_5.TabIndex = 63
        Me.optType_Orbit_5.Text = "Repeating ground trace"
        Me.optType_Orbit_5.UseVisualStyleBackColor = False
        Me.optType_Orbit_5.Visible = False
        '
        'optType_Orbit_6
        '
        Me.optType_Orbit_6.BackColor = System.Drawing.SystemColors.Control
        Me.optType_Orbit_6.Cursor = System.Windows.Forms.Cursors.Default
        Me.optType_Orbit_6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optType_Orbit_6.Location = New System.Drawing.Point(290, 66)
        Me.optType_Orbit_6.Name = "optType_Orbit_6"
        Me.optType_Orbit_6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optType_Orbit_6.Size = New System.Drawing.Size(105, 20)
        Me.optType_Orbit_6.TabIndex = 62
        Me.optType_Orbit_6.Text = "Circular"
        Me.optType_Orbit_6.UseVisualStyleBackColor = False
        '
        'optType_Orbit_7
        '
        Me.optType_Orbit_7.BackColor = System.Drawing.SystemColors.Control
        Me.optType_Orbit_7.Cursor = System.Windows.Forms.Cursors.Default
        Me.optType_Orbit_7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optType_Orbit_7.Location = New System.Drawing.Point(290, 92)
        Me.optType_Orbit_7.Name = "optType_Orbit_7"
        Me.optType_Orbit_7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optType_Orbit_7.Size = New System.Drawing.Size(77, 20)
        Me.optType_Orbit_7.TabIndex = 61
        Me.optType_Orbit_7.Text = "Other"
        Me.optType_Orbit_7.UseVisualStyleBackColor = False
        '
        'txtSSAltitude
        '
        Me.txtSSAltitude.AcceptsReturn = True
        Me.txtSSAltitude.BackColor = System.Drawing.SystemColors.Window
        Me.txtSSAltitude.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSSAltitude.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSSAltitude.Location = New System.Drawing.Point(112, 129)
        Me.txtSSAltitude.MaxLength = 0
        Me.txtSSAltitude.Name = "txtSSAltitude"
        Me.txtSSAltitude.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSSAltitude.Size = New System.Drawing.Size(57, 23)
        Me.txtSSAltitude.TabIndex = 60
        Me.txtSSAltitude.Visible = False
        '
        'txtSSInclination
        '
        Me.txtSSInclination.AcceptsReturn = True
        Me.txtSSInclination.BackColor = System.Drawing.SystemColors.Window
        Me.txtSSInclination.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSSInclination.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSSInclination.Location = New System.Drawing.Point(318, 129)
        Me.txtSSInclination.MaxLength = 0
        Me.txtSSInclination.Name = "txtSSInclination"
        Me.txtSSInclination.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSSInclination.Size = New System.Drawing.Size(68, 23)
        Me.txtSSInclination.TabIndex = 59
        Me.txtSSInclination.Visible = False
        '
        'lblSSAlt
        '
        Me.lblSSAlt.AutoSize = True
        Me.lblSSAlt.BackColor = System.Drawing.SystemColors.Control
        Me.lblSSAlt.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSSAlt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSSAlt.Location = New System.Drawing.Point(12, 132)
        Me.lblSSAlt.Name = "lblSSAlt"
        Me.lblSSAlt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSSAlt.Size = New System.Drawing.Size(94, 16)
        Me.lblSSAlt.TabIndex = 70
        Me.lblSSAlt.Text = "Altitude (km)"
        Me.lblSSAlt.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblSSAlt.Visible = False
        '
        'lblSSIncl
        '
        Me.lblSSIncl.AutoSize = True
        Me.lblSSIncl.BackColor = System.Drawing.SystemColors.Control
        Me.lblSSIncl.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSSIncl.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSSIncl.Location = New System.Drawing.Point(196, 132)
        Me.lblSSIncl.Name = "lblSSIncl"
        Me.lblSSIncl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSSIncl.Size = New System.Drawing.Size(117, 16)
        Me.lblSSIncl.TabIndex = 69
        Me.lblSSIncl.Text = "Inclination (deg)"
        Me.lblSSIncl.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblSSIncl.Visible = False
        '
        'fraElements
        '
        Me.fraElements.BackColor = System.Drawing.SystemColors.Control
        Me.fraElements.Controls.Add(Me.PictureBox1)
        Me.fraElements.Controls.Add(Me.txtA)
        Me.fraElements.Controls.Add(Me.txtE)
        Me.fraElements.Controls.Add(Me.txtI)
        Me.fraElements.Controls.Add(Me.txtRAAN)
        Me.fraElements.Controls.Add(Me.txtMu)
        Me.fraElements.Controls.Add(Me.txtW)
        Me.fraElements.Controls.Add(Me.cmbSemiMajor)
        Me.fraElements.Controls.Add(Me.cmbEccentricity)
        Me.fraElements.Controls.Add(Me.txtNoradEpoch)
        Me.fraElements.Controls.Add(Me.cmbAnomaly)
        Me.fraElements.Controls.Add(Me.txtBstar)
        Me.fraElements.Controls.Add(Me.cmbRAAN)
        Me.fraElements.Controls.Add(Me.cmbTime)
        Me.fraElements.Controls.Add(Me.txtEpochDate)
        Me.fraElements.Controls.Add(Me.txtEpochTime)
        Me.fraElements.Controls.Add(Me.lblI)
        Me.fraElements.Controls.Add(Me.lblW)
        Me.fraElements.Controls.Add(Me.lblEpoch)
        Me.fraElements.Controls.Add(Me.lblAUnits)
        Me.fraElements.Controls.Add(Me.lblEUnits)
        Me.fraElements.Controls.Add(Me.lblIUnits)
        Me.fraElements.Controls.Add(Me.lblWUnits)
        Me.fraElements.Controls.Add(Me.lblRAANUnits)
        Me.fraElements.Controls.Add(Me.lblAnomUnits)
        Me.fraElements.Controls.Add(Me.lblBstar)
        Me.fraElements.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraElements.ForeColor = System.Drawing.Color.Maroon
        Me.fraElements.Location = New System.Drawing.Point(510, 25)
        Me.fraElements.Name = "fraElements"
        Me.fraElements.Padding = New System.Windows.Forms.Padding(0)
        Me.fraElements.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraElements.Size = New System.Drawing.Size(433, 304)
        Me.fraElements.TabIndex = 19
        Me.fraElements.TabStop = False
        Me.fraElements.Text = "Orbit parameters"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Black
        Me.PictureBox1.Location = New System.Drawing.Point(18, 63)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(390, 1)
        Me.PictureBox1.TabIndex = 45
        Me.PictureBox1.TabStop = False
        '
        'txtA
        '
        Me.txtA.AcceptsReturn = True
        Me.txtA.BackColor = System.Drawing.SystemColors.Window
        Me.txtA.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtA.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtA.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtA.Location = New System.Drawing.Point(234, 73)
        Me.txtA.MaxLength = 0
        Me.txtA.Name = "txtA"
        Me.txtA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtA.Size = New System.Drawing.Size(100, 23)
        Me.txtA.TabIndex = 32
        '
        'txtE
        '
        Me.txtE.AcceptsReturn = True
        Me.txtE.BackColor = System.Drawing.SystemColors.Window
        Me.txtE.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtE.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtE.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtE.Location = New System.Drawing.Point(234, 105)
        Me.txtE.MaxLength = 0
        Me.txtE.Name = "txtE"
        Me.txtE.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtE.Size = New System.Drawing.Size(100, 23)
        Me.txtE.TabIndex = 31
        '
        'txtI
        '
        Me.txtI.AcceptsReturn = True
        Me.txtI.BackColor = System.Drawing.SystemColors.Window
        Me.txtI.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtI.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtI.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtI.Location = New System.Drawing.Point(234, 137)
        Me.txtI.MaxLength = 0
        Me.txtI.Name = "txtI"
        Me.txtI.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtI.Size = New System.Drawing.Size(100, 23)
        Me.txtI.TabIndex = 30
        '
        'txtRAAN
        '
        Me.txtRAAN.AcceptsReturn = True
        Me.txtRAAN.BackColor = System.Drawing.SystemColors.Window
        Me.txtRAAN.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRAAN.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRAAN.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRAAN.Location = New System.Drawing.Point(234, 170)
        Me.txtRAAN.MaxLength = 0
        Me.txtRAAN.Name = "txtRAAN"
        Me.txtRAAN.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRAAN.Size = New System.Drawing.Size(100, 23)
        Me.txtRAAN.TabIndex = 29
        '
        'txtMu
        '
        Me.txtMu.AcceptsReturn = True
        Me.txtMu.BackColor = System.Drawing.SystemColors.Window
        Me.txtMu.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMu.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMu.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMu.Location = New System.Drawing.Point(234, 234)
        Me.txtMu.MaxLength = 0
        Me.txtMu.Name = "txtMu"
        Me.txtMu.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMu.Size = New System.Drawing.Size(100, 23)
        Me.txtMu.TabIndex = 28
        '
        'txtW
        '
        Me.txtW.AcceptsReturn = True
        Me.txtW.BackColor = System.Drawing.SystemColors.Window
        Me.txtW.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtW.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtW.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtW.Location = New System.Drawing.Point(234, 202)
        Me.txtW.MaxLength = 0
        Me.txtW.Name = "txtW"
        Me.txtW.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtW.Size = New System.Drawing.Size(100, 23)
        Me.txtW.TabIndex = 27
        '
        'cmbSemiMajor
        '
        Me.cmbSemiMajor.BackColor = System.Drawing.SystemColors.Window
        Me.cmbSemiMajor.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbSemiMajor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSemiMajor.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbSemiMajor.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbSemiMajor.Items.AddRange(New Object() {"Semimajor Axis", "Apogee Radius", "Apogee Altitude", "Period", "Mean Motion"})
        Me.cmbSemiMajor.Location = New System.Drawing.Point(30, 75)
        Me.cmbSemiMajor.Name = "cmbSemiMajor"
        Me.cmbSemiMajor.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbSemiMajor.Size = New System.Drawing.Size(183, 24)
        Me.cmbSemiMajor.TabIndex = 26
        '
        'cmbEccentricity
        '
        Me.cmbEccentricity.BackColor = System.Drawing.SystemColors.Window
        Me.cmbEccentricity.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbEccentricity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEccentricity.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbEccentricity.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbEccentricity.Items.AddRange(New Object() {"Eccentricity", "Perigee Radius", "Perigee Altitude"})
        Me.cmbEccentricity.Location = New System.Drawing.Point(30, 105)
        Me.cmbEccentricity.Name = "cmbEccentricity"
        Me.cmbEccentricity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbEccentricity.Size = New System.Drawing.Size(183, 24)
        Me.cmbEccentricity.TabIndex = 25
        '
        'txtNoradEpoch
        '
        Me.txtNoradEpoch.AcceptsReturn = True
        Me.txtNoradEpoch.BackColor = System.Drawing.SystemColors.Window
        Me.txtNoradEpoch.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNoradEpoch.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNoradEpoch.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNoradEpoch.Location = New System.Drawing.Point(234, 24)
        Me.txtNoradEpoch.MaxLength = 0
        Me.txtNoradEpoch.Name = "txtNoradEpoch"
        Me.txtNoradEpoch.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNoradEpoch.Size = New System.Drawing.Size(124, 23)
        Me.txtNoradEpoch.TabIndex = 24
        Me.txtNoradEpoch.Text = "12025.12345678"
        Me.txtNoradEpoch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNoradEpoch.Visible = False
        '
        'cmbAnomaly
        '
        Me.cmbAnomaly.BackColor = System.Drawing.SystemColors.Window
        Me.cmbAnomaly.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbAnomaly.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAnomaly.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbAnomaly.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbAnomaly.Items.AddRange(New Object() {"Mean Anomaly", "True Anomaly", "Eccentric Anomaly"})
        Me.cmbAnomaly.Location = New System.Drawing.Point(30, 235)
        Me.cmbAnomaly.Name = "cmbAnomaly"
        Me.cmbAnomaly.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbAnomaly.Size = New System.Drawing.Size(183, 24)
        Me.cmbAnomaly.TabIndex = 23
        '
        'txtBstar
        '
        Me.txtBstar.AcceptsReturn = True
        Me.txtBstar.BackColor = System.Drawing.SystemColors.Window
        Me.txtBstar.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBstar.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBstar.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBstar.Location = New System.Drawing.Point(234, 266)
        Me.txtBstar.MaxLength = 0
        Me.txtBstar.Name = "txtBstar"
        Me.txtBstar.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBstar.Size = New System.Drawing.Size(100, 23)
        Me.txtBstar.TabIndex = 22
        '
        'cmbRAAN
        '
        Me.cmbRAAN.BackColor = System.Drawing.SystemColors.Window
        Me.cmbRAAN.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbRAAN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRAAN.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbRAAN.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbRAAN.Items.AddRange(New Object() {"RAAN", "Longitude of Asc. Node"})
        Me.cmbRAAN.Location = New System.Drawing.Point(30, 173)
        Me.cmbRAAN.Name = "cmbRAAN"
        Me.cmbRAAN.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbRAAN.Size = New System.Drawing.Size(183, 24)
        Me.cmbRAAN.TabIndex = 21
        '
        'cmbTime
        '
        Me.cmbTime.BackColor = System.Drawing.SystemColors.Window
        Me.cmbTime.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTime.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTime.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbTime.Items.AddRange(New Object() {"Calendar", "NORAD epoch"})
        Me.cmbTime.Location = New System.Drawing.Point(74, 24)
        Me.cmbTime.Name = "cmbTime"
        Me.cmbTime.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbTime.Size = New System.Drawing.Size(139, 24)
        Me.cmbTime.TabIndex = 20
        '
        'txtEpochDate
        '
        Me.txtEpochDate.AllowPromptAsInput = False
        Me.txtEpochDate.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEpochDate.Location = New System.Drawing.Point(234, 24)
        Me.txtEpochDate.Mask = "##-##-####"
        Me.txtEpochDate.Name = "txtEpochDate"
        Me.txtEpochDate.Size = New System.Drawing.Size(90, 23)
        Me.txtEpochDate.TabIndex = 33
        Me.txtEpochDate.Text = "26012012"
        Me.txtEpochDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtEpochDate.Visible = False
        '
        'txtEpochTime
        '
        Me.txtEpochTime.AllowPromptAsInput = False
        Me.txtEpochTime.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEpochTime.Location = New System.Drawing.Point(330, 24)
        Me.txtEpochTime.Mask = "##:##:##"
        Me.txtEpochTime.Name = "txtEpochTime"
        Me.txtEpochTime.Size = New System.Drawing.Size(76, 23)
        Me.txtEpochTime.TabIndex = 34
        Me.txtEpochTime.Text = "222718"
        Me.txtEpochTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtEpochTime.Visible = False
        '
        'lblI
        '
        Me.lblI.AutoSize = True
        Me.lblI.BackColor = System.Drawing.SystemColors.Control
        Me.lblI.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblI.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblI.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblI.Location = New System.Drawing.Point(137, 140)
        Me.lblI.Name = "lblI"
        Me.lblI.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblI.Size = New System.Drawing.Size(76, 16)
        Me.lblI.TabIndex = 44
        Me.lblI.Text = "Inclination"
        Me.lblI.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblW
        '
        Me.lblW.AutoSize = True
        Me.lblW.BackColor = System.Drawing.SystemColors.Control
        Me.lblW.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblW.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblW.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblW.Location = New System.Drawing.Point(71, 205)
        Me.lblW.Name = "lblW"
        Me.lblW.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblW.Size = New System.Drawing.Size(142, 16)
        Me.lblW.TabIndex = 43
        Me.lblW.Text = "Argument of Perigee"
        Me.lblW.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblEpoch
        '
        Me.lblEpoch.BackColor = System.Drawing.SystemColors.Control
        Me.lblEpoch.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblEpoch.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEpoch.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblEpoch.Location = New System.Drawing.Point(21, 28)
        Me.lblEpoch.Name = "lblEpoch"
        Me.lblEpoch.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblEpoch.Size = New System.Drawing.Size(50, 19)
        Me.lblEpoch.TabIndex = 42
        Me.lblEpoch.Text = "Epoch"
        '
        'lblAUnits
        '
        Me.lblAUnits.AutoSize = True
        Me.lblAUnits.BackColor = System.Drawing.SystemColors.Control
        Me.lblAUnits.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAUnits.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAUnits.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAUnits.Location = New System.Drawing.Point(340, 80)
        Me.lblAUnits.Name = "lblAUnits"
        Me.lblAUnits.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAUnits.Size = New System.Drawing.Size(26, 16)
        Me.lblAUnits.TabIndex = 41
        Me.lblAUnits.Text = "km"
        '
        'lblEUnits
        '
        Me.lblEUnits.AutoSize = True
        Me.lblEUnits.BackColor = System.Drawing.SystemColors.Control
        Me.lblEUnits.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblEUnits.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEUnits.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblEUnits.Location = New System.Drawing.Point(304, 105)
        Me.lblEUnits.Name = "lblEUnits"
        Me.lblEUnits.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblEUnits.Size = New System.Drawing.Size(0, 19)
        Me.lblEUnits.TabIndex = 40
        '
        'lblIUnits
        '
        Me.lblIUnits.AutoSize = True
        Me.lblIUnits.BackColor = System.Drawing.SystemColors.Control
        Me.lblIUnits.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblIUnits.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIUnits.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblIUnits.Location = New System.Drawing.Point(340, 140)
        Me.lblIUnits.Name = "lblIUnits"
        Me.lblIUnits.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblIUnits.Size = New System.Drawing.Size(32, 16)
        Me.lblIUnits.TabIndex = 39
        Me.lblIUnits.Text = "deg"
        '
        'lblWUnits
        '
        Me.lblWUnits.AutoSize = True
        Me.lblWUnits.BackColor = System.Drawing.SystemColors.Control
        Me.lblWUnits.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWUnits.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWUnits.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWUnits.Location = New System.Drawing.Point(340, 205)
        Me.lblWUnits.Name = "lblWUnits"
        Me.lblWUnits.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWUnits.Size = New System.Drawing.Size(32, 16)
        Me.lblWUnits.TabIndex = 38
        Me.lblWUnits.Text = "deg"
        '
        'lblRAANUnits
        '
        Me.lblRAANUnits.AutoSize = True
        Me.lblRAANUnits.BackColor = System.Drawing.SystemColors.Control
        Me.lblRAANUnits.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRAANUnits.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRAANUnits.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRAANUnits.Location = New System.Drawing.Point(340, 176)
        Me.lblRAANUnits.Name = "lblRAANUnits"
        Me.lblRAANUnits.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRAANUnits.Size = New System.Drawing.Size(32, 16)
        Me.lblRAANUnits.TabIndex = 37
        Me.lblRAANUnits.Text = "deg"
        '
        'lblAnomUnits
        '
        Me.lblAnomUnits.AutoSize = True
        Me.lblAnomUnits.BackColor = System.Drawing.SystemColors.Control
        Me.lblAnomUnits.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAnomUnits.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnomUnits.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAnomUnits.Location = New System.Drawing.Point(340, 237)
        Me.lblAnomUnits.Name = "lblAnomUnits"
        Me.lblAnomUnits.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAnomUnits.Size = New System.Drawing.Size(32, 16)
        Me.lblAnomUnits.TabIndex = 36
        Me.lblAnomUnits.Text = "deg"
        '
        'lblBstar
        '
        Me.lblBstar.AutoSize = True
        Me.lblBstar.BackColor = System.Drawing.SystemColors.Control
        Me.lblBstar.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBstar.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBstar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBstar.Location = New System.Drawing.Point(171, 269)
        Me.lblBstar.Name = "lblBstar"
        Me.lblBstar.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBstar.Size = New System.Drawing.Size(42, 16)
        Me.lblBstar.TabIndex = 35
        Me.lblBstar.Text = "Bstar"
        Me.lblBstar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'zlbl_4
        '
        Me.zlbl_4.AutoSize = True
        Me.zlbl_4.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_4.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_4.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_4.Location = New System.Drawing.Point(18, 26)
        Me.zlbl_4.Name = "zlbl_4"
        Me.zlbl_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_4.Size = New System.Drawing.Size(52, 18)
        Me.zlbl_4.TabIndex = 72
        Me.zlbl_4.Text = "Name"
        '
        'fraAttitude
        '
        Me.fraAttitude.BackColor = System.Drawing.SystemColors.Control
        Me.fraAttitude.Controls.Add(Me.cmbPrinAxis)
        Me.fraAttitude.Controls.Add(Me.cmbPrinOrient)
        Me.fraAttitude.Controls.Add(Me.cmbOptAxis)
        Me.fraAttitude.Controls.Add(Me.cmbOptOrient)
        Me.fraAttitude.Controls.Add(Me.zlbl_8)
        Me.fraAttitude.Controls.Add(Me.zlbl_7)
        Me.fraAttitude.Controls.Add(Me.zlbl_0)
        Me.fraAttitude.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraAttitude.ForeColor = System.Drawing.Color.Maroon
        Me.fraAttitude.Location = New System.Drawing.Point(17, 366)
        Me.fraAttitude.Name = "fraAttitude"
        Me.fraAttitude.Padding = New System.Windows.Forms.Padding(0)
        Me.fraAttitude.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraAttitude.Size = New System.Drawing.Size(491, 92)
        Me.fraAttitude.TabIndex = 9
        Me.fraAttitude.TabStop = False
        Me.fraAttitude.Text = "Attitude"
        '
        'cmbPrinAxis
        '
        Me.cmbPrinAxis.BackColor = System.Drawing.SystemColors.Window
        Me.cmbPrinAxis.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbPrinAxis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPrinAxis.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPrinAxis.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbPrinAxis.Location = New System.Drawing.Point(138, 21)
        Me.cmbPrinAxis.Name = "cmbPrinAxis"
        Me.cmbPrinAxis.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbPrinAxis.Size = New System.Drawing.Size(49, 26)
        Me.cmbPrinAxis.TabIndex = 14
        '
        'cmbPrinOrient
        '
        Me.cmbPrinOrient.BackColor = System.Drawing.SystemColors.Window
        Me.cmbPrinOrient.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbPrinOrient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPrinOrient.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPrinOrient.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbPrinOrient.Location = New System.Drawing.Point(193, 21)
        Me.cmbPrinOrient.Name = "cmbPrinOrient"
        Me.cmbPrinOrient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbPrinOrient.Size = New System.Drawing.Size(128, 26)
        Me.cmbPrinOrient.TabIndex = 13
        '
        'cmbOptAxis
        '
        Me.cmbOptAxis.BackColor = System.Drawing.SystemColors.Window
        Me.cmbOptAxis.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbOptAxis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOptAxis.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbOptAxis.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbOptAxis.Location = New System.Drawing.Point(138, 52)
        Me.cmbOptAxis.Name = "cmbOptAxis"
        Me.cmbOptAxis.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbOptAxis.Size = New System.Drawing.Size(49, 26)
        Me.cmbOptAxis.TabIndex = 12
        '
        'cmbOptOrient
        '
        Me.cmbOptOrient.BackColor = System.Drawing.SystemColors.Window
        Me.cmbOptOrient.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbOptOrient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOptOrient.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbOptOrient.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbOptOrient.Location = New System.Drawing.Point(193, 53)
        Me.cmbOptOrient.Name = "cmbOptOrient"
        Me.cmbOptOrient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbOptOrient.Size = New System.Drawing.Size(128, 26)
        Me.cmbOptOrient.TabIndex = 11
        '
        'zlbl_8
        '
        Me.zlbl_8.AutoSize = True
        Me.zlbl_8.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_8.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_8.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_8.Location = New System.Drawing.Point(18, 29)
        Me.zlbl_8.Name = "zlbl_8"
        Me.zlbl_8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_8.Size = New System.Drawing.Size(114, 18)
        Me.zlbl_8.TabIndex = 17
        Me.zlbl_8.Text = "Primary direction"
        Me.zlbl_8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'zlbl_0
        '
        Me.zlbl_0.BackColor = System.Drawing.SystemColors.Control
        Me.zlbl_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.zlbl_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.zlbl_0.Location = New System.Drawing.Point(324, 26)
        Me.zlbl_0.Name = "zlbl_0"
        Me.zlbl_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zlbl_0.Size = New System.Drawing.Size(163, 59)
        Me.zlbl_0.TabIndex = 15
        Me.zlbl_0.Text = "Secondary direction is optimized if no direct alignment is possible"
        Me.zlbl_0.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'zfrm_2
        '
        Me.zfrm_2.BackColor = System.Drawing.SystemColors.Control
        Me.zfrm_2.Controls.Add(Me.cmbBckTrackCO)
        Me.zfrm_2.Controls.Add(Me.cmbFwdTrackCO)
        Me.zfrm_2.Controls.Add(Me.previewColor)
        Me.zfrm_2.Controls.Add(Me.ChangeColor)
        Me.zfrm_2.Controls.Add(Me.cmbFwdTrack)
        Me.zfrm_2.Controls.Add(Me.cmbBckTrack)
        Me.zfrm_2.Controls.Add(Me.chkFootprint)
        Me.zfrm_2.Controls.Add(Me.lblFwdTrack)
        Me.zfrm_2.Controls.Add(Me.lblBckTrack)
        Me.zfrm_2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zfrm_2.ForeColor = System.Drawing.Color.Maroon
        Me.zfrm_2.Location = New System.Drawing.Point(527, 366)
        Me.zfrm_2.Name = "zfrm_2"
        Me.zfrm_2.Padding = New System.Windows.Forms.Padding(0)
        Me.zfrm_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.zfrm_2.Size = New System.Drawing.Size(453, 92)
        Me.zfrm_2.TabIndex = 2
        Me.zfrm_2.TabStop = False
        Me.zfrm_2.Text = "Graphics"
        '
        'cmbBckTrackCO
        '
        Me.cmbBckTrackCO.BackColor = System.Drawing.SystemColors.Window
        Me.cmbBckTrackCO.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbBckTrackCO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBckTrackCO.Enabled = False
        Me.cmbBckTrackCO.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBckTrackCO.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbBckTrackCO.Items.AddRange(New Object() {"1 Orbit", "1/2 Orbit", "1/4 Orbit", "None"})
        Me.cmbBckTrackCO.Location = New System.Drawing.Point(167, 54)
        Me.cmbBckTrackCO.Name = "cmbBckTrackCO"
        Me.cmbBckTrackCO.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbBckTrackCO.Size = New System.Drawing.Size(85, 24)
        Me.cmbBckTrackCO.TabIndex = 19
        '
        'cmbFwdTrackCO
        '
        Me.cmbFwdTrackCO.BackColor = System.Drawing.SystemColors.Window
        Me.cmbFwdTrackCO.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbFwdTrackCO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFwdTrackCO.Enabled = False
        Me.cmbFwdTrackCO.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFwdTrackCO.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbFwdTrackCO.Items.AddRange(New Object() {"1 Orbit", "1/2 Orbit", "1/4 Orbit", "None"})
        Me.cmbFwdTrackCO.Location = New System.Drawing.Point(167, 23)
        Me.cmbFwdTrackCO.Name = "cmbFwdTrackCO"
        Me.cmbFwdTrackCO.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbFwdTrackCO.Size = New System.Drawing.Size(85, 24)
        Me.cmbFwdTrackCO.TabIndex = 41
        '
        'previewColor
        '
        Me.previewColor.BackColor = System.Drawing.Color.Yellow
        Me.previewColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.previewColor.Location = New System.Drawing.Point(376, 54)
        Me.previewColor.Name = "previewColor"
        Me.previewColor.Size = New System.Drawing.Size(22, 22)
        Me.previewColor.TabIndex = 10
        Me.previewColor.TabStop = False
        '
        'ChangeColor
        '
        Me.ChangeColor.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChangeColor.ForeColor = System.Drawing.Color.Black
        Me.ChangeColor.Location = New System.Drawing.Point(301, 51)
        Me.ChangeColor.Name = "ChangeColor"
        Me.ChangeColor.Size = New System.Drawing.Size(64, 27)
        Me.ChangeColor.TabIndex = 9
        Me.ChangeColor.Text = "Color"
        Me.ChangeColor.UseVisualStyleBackColor = True
        '
        'cmbFwdTrack
        '
        Me.cmbFwdTrack.BackColor = System.Drawing.SystemColors.Window
        Me.cmbFwdTrack.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbFwdTrack.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFwdTrack.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFwdTrack.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbFwdTrack.Items.AddRange(New Object() {"1 Orbit", "1/2 Orbit", "1/4 Orbit", "None"})
        Me.cmbFwdTrack.Location = New System.Drawing.Point(167, 23)
        Me.cmbFwdTrack.Name = "cmbFwdTrack"
        Me.cmbFwdTrack.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbFwdTrack.Size = New System.Drawing.Size(85, 24)
        Me.cmbFwdTrack.TabIndex = 5
        '
        'cmbBckTrack
        '
        Me.cmbBckTrack.BackColor = System.Drawing.SystemColors.Window
        Me.cmbBckTrack.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbBckTrack.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBckTrack.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBckTrack.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbBckTrack.Items.AddRange(New Object() {"1 Orbit", "1/2 Orbit", "1/4 Orbit", "None"})
        Me.cmbBckTrack.Location = New System.Drawing.Point(167, 54)
        Me.cmbBckTrack.Name = "cmbBckTrack"
        Me.cmbBckTrack.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbBckTrack.Size = New System.Drawing.Size(85, 24)
        Me.cmbBckTrack.TabIndex = 4
        '
        'chkFootprint
        '
        Me.chkFootprint.BackColor = System.Drawing.SystemColors.Control
        Me.chkFootprint.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkFootprint.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFootprint.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkFootprint.Location = New System.Drawing.Point(301, 23)
        Me.chkFootprint.Name = "chkFootprint"
        Me.chkFootprint.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkFootprint.Size = New System.Drawing.Size(132, 25)
        Me.chkFootprint.TabIndex = 3
        Me.chkFootprint.Text = "Show footprint"
        Me.chkFootprint.UseVisualStyleBackColor = False
        '
        'lblFwdTrack
        '
        Me.lblFwdTrack.AutoSize = True
        Me.lblFwdTrack.BackColor = System.Drawing.SystemColors.Control
        Me.lblFwdTrack.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFwdTrack.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFwdTrack.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFwdTrack.Location = New System.Drawing.Point(29, 26)
        Me.lblFwdTrack.Name = "lblFwdTrack"
        Me.lblFwdTrack.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFwdTrack.Size = New System.Drawing.Size(131, 16)
        Me.lblFwdTrack.TabIndex = 7
        Me.lblFwdTrack.Text = "Leading Track Size"
        Me.lblFwdTrack.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblBckTrack
        '
        Me.lblBckTrack.AutoSize = True
        Me.lblBckTrack.BackColor = System.Drawing.SystemColors.Control
        Me.lblBckTrack.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBckTrack.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBckTrack.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBckTrack.Location = New System.Drawing.Point(29, 57)
        Me.lblBckTrack.Name = "lblBckTrack"
        Me.lblBckTrack.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBckTrack.Size = New System.Drawing.Size(127, 16)
        Me.lblBckTrack.TabIndex = 6
        Me.lblBckTrack.Text = "Trailing Track Size"
        Me.lblBckTrack.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmdOk
        '
        Me.cmdOk.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOk.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOk.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdOk.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOk.Location = New System.Drawing.Point(469, 537)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOk.Size = New System.Drawing.Size(129, 37)
        Me.cmdOk.TabIndex = 0
        Me.cmdOk.Text = "OK / Close"
        Me.cmdOk.UseVisualStyleBackColor = False
        '
        'ColorBox
        '
        Me.ColorBox.Color = System.Drawing.Color.Yellow
        '
        'tabOrbit
        '
        Me.tabOrbit.Controls.Add(Me.tabOrbit_Orbit)
        Me.tabOrbit.Controls.Add(Me.tabOrbit_Environment)
        Me.tabOrbit.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabOrbit.Location = New System.Drawing.Point(12, 12)
        Me.tabOrbit.Name = "tabOrbit"
        Me.tabOrbit.SelectedIndex = 0
        Me.tabOrbit.Size = New System.Drawing.Size(1004, 510)
        Me.tabOrbit.TabIndex = 19
        '
        'tabOrbit_Orbit
        '
        Me.tabOrbit_Orbit.BackColor = System.Drawing.Color.Transparent
        Me.tabOrbit_Orbit.Controls.Add(Me.zfra)
        Me.tabOrbit_Orbit.Controls.Add(Me.zfrm_2)
        Me.tabOrbit_Orbit.Controls.Add(Me.fraAttitude)
        Me.tabOrbit_Orbit.Location = New System.Drawing.Point(4, 25)
        Me.tabOrbit_Orbit.Name = "tabOrbit_Orbit"
        Me.tabOrbit_Orbit.Padding = New System.Windows.Forms.Padding(3)
        Me.tabOrbit_Orbit.Size = New System.Drawing.Size(996, 481)
        Me.tabOrbit_Orbit.TabIndex = 0
        Me.tabOrbit_Orbit.Text = "Orbit/Attitude"
        Me.tabOrbit_Orbit.UseVisualStyleBackColor = True
        '
        'tabOrbit_Environment
        '
        Me.tabOrbit_Environment.Controls.Add(Me.TabControl1)
        Me.tabOrbit_Environment.Controls.Add(Me.zgrp_3)
        Me.tabOrbit_Environment.Controls.Add(Me.zgrp_2)
        Me.tabOrbit_Environment.Controls.Add(Me.zgrp_1)
        Me.tabOrbit_Environment.Location = New System.Drawing.Point(4, 25)
        Me.tabOrbit_Environment.Name = "tabOrbit_Environment"
        Me.tabOrbit_Environment.Padding = New System.Windows.Forms.Padding(3)
        Me.tabOrbit_Environment.Size = New System.Drawing.Size(996, 481)
        Me.tabOrbit_Environment.TabIndex = 1
        Me.tabOrbit_Environment.Text = "Environment"
        Me.tabOrbit_Environment.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(398, 15)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(565, 407)
        Me.TabControl1.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.TabControl1, "If CERES Data box is checked, monthly average values will be plotted in the first" &
        " tab." & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(10) & "On the CERES data tab, Earth IR and albedo radiation are plotted as a fun" &
        "ction of the " & Global.Microsoft.VisualBasic.ChrW(10) & "latitude for each month")
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.env_graph)
        Me.TabPage1.Location = New System.Drawing.Point(4, 25)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(557, 378)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Monthly data"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'env_graph
        '
        Me.env_graph.BackColor = System.Drawing.Color.PaleTurquoise
        Me.env_graph.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.env_graph.BorderlineColor = System.Drawing.Color.DarkCyan
        Me.env_graph.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.env_graph.BorderlineWidth = 2
        ChartArea1.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount
        ChartArea1.AxisX.Maximum = 12.0R
        ChartArea1.AxisX.Minimum = 0R
        ChartArea1.AxisX.Title = "Months"
        ChartArea1.AxisX.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        ChartArea1.AxisY.Minimum = 0R
        ChartArea1.AxisY.Title = "W/m�"
        ChartArea1.AxisY.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        ChartArea1.AxisY2.Maximum = 1.0R
        ChartArea1.AxisY2.Minimum = 0R
        ChartArea1.BackColor = System.Drawing.Color.Transparent
        ChartArea1.Name = "ChartArea1"
        Me.env_graph.ChartAreas.Add(ChartArea1)
        Legend1.Alignment = System.Drawing.StringAlignment.Center
        Legend1.BackColor = System.Drawing.Color.PaleTurquoise
        Legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom
        Legend1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Legend1.IsTextAutoFit = False
        Legend1.MaximumAutoSize = 30.0!
        Legend1.Name = "Legend1"
        Me.env_graph.Legends.Add(Legend1)
        Me.env_graph.Location = New System.Drawing.Point(2, 2)
        Me.env_graph.Margin = New System.Windows.Forms.Padding(2)
        Me.env_graph.Name = "env_graph"
        Series1.ChartArea = "ChartArea1"
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Me.env_graph.Series.Add(Series1)
        Me.env_graph.Size = New System.Drawing.Size(553, 344)
        Me.env_graph.TabIndex = 90
        Me.env_graph.Text = "Chart1"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.CERES_month)
        Me.TabPage2.Controls.Add(Me.CERES_graph)
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(557, 378)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "CERES data"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'CERES_month
        '
        Me.CERES_month.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CERES_month.ForeColor = System.Drawing.Color.Black
        Me.CERES_month.FormattingEnabled = True
        Me.CERES_month.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.CERES_month.Location = New System.Drawing.Point(214, 351)
        Me.CERES_month.Name = "CERES_month"
        Me.CERES_month.Size = New System.Drawing.Size(110, 24)
        Me.CERES_month.TabIndex = 92
        Me.CERES_month.Text = "January"
        '
        'CERES_graph
        '
        Me.CERES_graph.BackColor = System.Drawing.Color.PaleTurquoise
        Me.CERES_graph.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.CERES_graph.BorderlineColor = System.Drawing.Color.DarkCyan
        Me.CERES_graph.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.CERES_graph.BorderlineWidth = 2
        ChartArea2.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount
        ChartArea2.AxisX.Maximum = 90.0R
        ChartArea2.AxisX.Minimum = -90.0R
        ChartArea2.AxisX.Title = "Latitude"
        ChartArea2.AxisX.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        ChartArea2.AxisY.Maximum = 300.0R
        ChartArea2.AxisY.Minimum = 100.0R
        ChartArea2.AxisY.Title = "W/m�"
        ChartArea2.AxisY.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        ChartArea2.AxisY2.Maximum = 1.0R
        ChartArea2.AxisY2.Minimum = 0R
        ChartArea2.BackColor = System.Drawing.Color.Transparent
        ChartArea2.Name = "ChartArea2"
        Me.CERES_graph.ChartAreas.Add(ChartArea2)
        Legend2.Alignment = System.Drawing.StringAlignment.Center
        Legend2.AutoFitMinFontSize = 8
        Legend2.BackColor = System.Drawing.Color.PaleTurquoise
        Legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom
        Legend2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Legend2.IsTextAutoFit = False
        Legend2.Name = "Legend1"
        Me.CERES_graph.Legends.Add(Legend2)
        Me.CERES_graph.Location = New System.Drawing.Point(2, 2)
        Me.CERES_graph.Margin = New System.Windows.Forms.Padding(2)
        Me.CERES_graph.Name = "CERES_graph"
        Series2.ChartArea = "ChartArea2"
        Series2.Legend = "Legend1"
        Series2.Name = "Series1"
        Me.CERES_graph.Series.Add(Series2)
        Me.CERES_graph.Size = New System.Drawing.Size(553, 345)
        Me.CERES_graph.TabIndex = 91
        Me.CERES_graph.Text = "CERES_graph"
        '
        'zgrp_3
        '
        Me.zgrp_3.BackColor = System.Drawing.SystemColors.Control
        Me.zgrp_3.Controls.Add(Me.Label3)
        Me.zgrp_3.Controls.Add(Me.Earth_Ce_check)
        Me.zgrp_3.Controls.Add(Me.env_txt_ir)
        Me.zgrp_3.Controls.Add(Me.env_cmd_isoterre)
        Me.zgrp_3.Controls.Add(Me.env_list_months_ir)
        Me.zgrp_3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zgrp_3.ForeColor = System.Drawing.Color.Maroon
        Me.zgrp_3.Location = New System.Drawing.Point(47, 323)
        Me.zgrp_3.Name = "zgrp_3"
        Me.zgrp_3.Size = New System.Drawing.Size(345, 99)
        Me.zgrp_3.TabIndex = 0
        Me.zgrp_3.TabStop = False
        Me.zgrp_3.Text = "Earth infrared flux"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(194, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 16)
        Me.Label3.TabIndex = 93
        Me.Label3.Text = "W/m�"
        '
        'Earth_Ce_check
        '
        Me.Earth_Ce_check.AutoSize = True
        Me.Earth_Ce_check.Checked = True
        Me.Earth_Ce_check.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Earth_Ce_check.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Earth_Ce_check.ForeColor = System.Drawing.Color.Black
        Me.Earth_Ce_check.Location = New System.Drawing.Point(18, 26)
        Me.Earth_Ce_check.Name = "Earth_Ce_check"
        Me.Earth_Ce_check.Size = New System.Drawing.Size(105, 20)
        Me.Earth_Ce_check.TabIndex = 92
        Me.Earth_Ce_check.Text = "CERES Data"
        Me.ToolTip1.SetToolTip(Me.Earth_Ce_check, "Data taken from CERES observation. This data gives  a value of the Earth temperat" &
        "ure for every latitude and month.")
        Me.Earth_Ce_check.UseVisualStyleBackColor = True
        '
        'env_txt_ir
        '
        Me.env_txt_ir.Enabled = False
        Me.env_txt_ir.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.env_txt_ir.ForeColor = System.Drawing.Color.Black
        Me.env_txt_ir.Location = New System.Drawing.Point(134, 53)
        Me.env_txt_ir.Name = "env_txt_ir"
        Me.env_txt_ir.Size = New System.Drawing.Size(59, 23)
        Me.env_txt_ir.TabIndex = 2
        '
        'env_cmd_isoterre
        '
        Me.env_cmd_isoterre.Enabled = False
        Me.env_cmd_isoterre.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.env_cmd_isoterre.ForeColor = System.Drawing.Color.Black
        Me.env_cmd_isoterre.Location = New System.Drawing.Point(245, 51)
        Me.env_cmd_isoterre.Name = "env_cmd_isoterre"
        Me.env_cmd_isoterre.Size = New System.Drawing.Size(90, 27)
        Me.env_cmd_isoterre.TabIndex = 2
        Me.env_cmd_isoterre.Text = "ISO model"
        Me.ToolTip1.SetToolTip(Me.env_cmd_isoterre, "By clicking here, the Earth infrared radiation will be set to a isothermal model." &
        "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "The radiation will be the necessary to maintain Earth temperature to a constan" &
        "t value.")
        Me.env_cmd_isoterre.UseVisualStyleBackColor = True
        '
        'env_list_months_ir
        '
        Me.env_list_months_ir.Enabled = False
        Me.env_list_months_ir.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.env_list_months_ir.ForeColor = System.Drawing.Color.Black
        Me.env_list_months_ir.FormattingEnabled = True
        Me.env_list_months_ir.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.env_list_months_ir.Location = New System.Drawing.Point(18, 53)
        Me.env_list_months_ir.Name = "env_list_months_ir"
        Me.env_list_months_ir.Size = New System.Drawing.Size(110, 24)
        Me.env_list_months_ir.TabIndex = 1
        Me.env_list_months_ir.Text = "January"
        '
        'zgrp_2
        '
        Me.zgrp_2.BackColor = System.Drawing.SystemColors.Control
        Me.zgrp_2.Controls.Add(Me.Alb_Ce_check)
        Me.zgrp_2.Controls.Add(Me.zlbl_12)
        Me.zgrp_2.Controls.Add(Me.env_cmd_sin)
        Me.zgrp_2.Controls.Add(Me.env_txt_var)
        Me.zgrp_2.Controls.Add(Me.env_txt_albedo)
        Me.zgrp_2.Controls.Add(Me.env_list_months_alb)
        Me.zgrp_2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zgrp_2.ForeColor = System.Drawing.Color.Maroon
        Me.zgrp_2.Location = New System.Drawing.Point(47, 165)
        Me.zgrp_2.Name = "zgrp_2"
        Me.zgrp_2.Size = New System.Drawing.Size(345, 142)
        Me.zgrp_2.TabIndex = 0
        Me.zgrp_2.TabStop = False
        Me.zgrp_2.Text = "Earth albedo coefficient"
        '
        'Alb_Ce_check
        '
        Me.Alb_Ce_check.AutoSize = True
        Me.Alb_Ce_check.Checked = True
        Me.Alb_Ce_check.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Alb_Ce_check.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Alb_Ce_check.ForeColor = System.Drawing.Color.Black
        Me.Alb_Ce_check.Location = New System.Drawing.Point(18, 26)
        Me.Alb_Ce_check.Name = "Alb_Ce_check"
        Me.Alb_Ce_check.Size = New System.Drawing.Size(105, 20)
        Me.Alb_Ce_check.TabIndex = 91
        Me.Alb_Ce_check.Text = "CERES Data"
        Me.ToolTip1.SetToolTip(Me.Alb_Ce_check, "Data taken from CERES observation. This data gives  a value of the albedo coeffic" &
        "ient for every latitude and month.")
        Me.Alb_Ce_check.UseVisualStyleBackColor = True
        '
        'zlbl_12
        '
        Me.zlbl_12.AutoSize = True
        Me.zlbl_12.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_12.ForeColor = System.Drawing.Color.Black
        Me.zlbl_12.Location = New System.Drawing.Point(210, 101)
        Me.zlbl_12.Name = "zlbl_12"
        Me.zlbl_12.Size = New System.Drawing.Size(35, 18)
        Me.zlbl_12.TabIndex = 3
        Me.zlbl_12.Text = "+/-"
        '
        'env_cmd_sin
        '
        Me.env_cmd_sin.Enabled = False
        Me.env_cmd_sin.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.env_cmd_sin.ForeColor = System.Drawing.Color.Black
        Me.env_cmd_sin.Location = New System.Drawing.Point(94, 96)
        Me.env_cmd_sin.Name = "env_cmd_sin"
        Me.env_cmd_sin.Size = New System.Drawing.Size(110, 27)
        Me.env_cmd_sin.TabIndex = 2
        Me.env_cmd_sin.Text = "Sine function"
        Me.ToolTip1.SetToolTip(Me.env_cmd_sin, "By clicking here, the Albedo coefficient will have a sine-like form" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "where the pe" &
        "ak values are setted as the distance to the mean value " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "with the text box on th" &
        "e right")
        Me.env_cmd_sin.UseVisualStyleBackColor = True
        '
        'env_txt_var
        '
        Me.env_txt_var.Enabled = False
        Me.env_txt_var.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.env_txt_var.ForeColor = System.Drawing.Color.Black
        Me.env_txt_var.Location = New System.Drawing.Point(251, 100)
        Me.env_txt_var.Name = "env_txt_var"
        Me.env_txt_var.Size = New System.Drawing.Size(65, 23)
        Me.env_txt_var.TabIndex = 1
        Me.env_txt_var.Text = "0"
        '
        'env_txt_albedo
        '
        Me.env_txt_albedo.Enabled = False
        Me.env_txt_albedo.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.env_txt_albedo.ForeColor = System.Drawing.Color.Black
        Me.env_txt_albedo.Location = New System.Drawing.Point(144, 54)
        Me.env_txt_albedo.Name = "env_txt_albedo"
        Me.env_txt_albedo.Size = New System.Drawing.Size(74, 23)
        Me.env_txt_albedo.TabIndex = 1
        '
        'env_list_months_alb
        '
        Me.env_list_months_alb.Enabled = False
        Me.env_list_months_alb.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.env_list_months_alb.ForeColor = System.Drawing.Color.Black
        Me.env_list_months_alb.FormattingEnabled = True
        Me.env_list_months_alb.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.env_list_months_alb.Location = New System.Drawing.Point(18, 54)
        Me.env_list_months_alb.Name = "env_list_months_alb"
        Me.env_list_months_alb.Size = New System.Drawing.Size(110, 24)
        Me.env_list_months_alb.TabIndex = 0
        Me.env_list_months_alb.Text = "January"
        '
        'zgrp_1
        '
        Me.zgrp_1.BackColor = System.Drawing.SystemColors.Control
        Me.zgrp_1.Controls.Add(Me.zlbl_11)
        Me.zgrp_1.Controls.Add(Me.zlbl_13)
        Me.zgrp_1.Controls.Add(Me.Label2)
        Me.zgrp_1.Controls.Add(Me.zlbl_10)
        Me.zgrp_1.Controls.Add(Me.Label1)
        Me.zgrp_1.Controls.Add(Me.zlbl_9)
        Me.zgrp_1.Controls.Add(Me.env_txt_flux_1)
        Me.zgrp_1.Controls.Add(Me.env_txt_flux_0)
        Me.zgrp_1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zgrp_1.ForeColor = System.Drawing.Color.Maroon
        Me.zgrp_1.Location = New System.Drawing.Point(47, 40)
        Me.zgrp_1.Name = "zgrp_1"
        Me.zgrp_1.Size = New System.Drawing.Size(345, 107)
        Me.zgrp_1.TabIndex = 0
        Me.zgrp_1.TabStop = False
        Me.zgrp_1.Text = "Solar constant"
        '
        'zlbl_11
        '
        Me.zlbl_11.AutoSize = True
        Me.zlbl_11.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_11.ForeColor = System.Drawing.Color.Black
        Me.zlbl_11.Location = New System.Drawing.Point(15, 66)
        Me.zlbl_11.Name = "zlbl_11"
        Me.zlbl_11.Size = New System.Drawing.Size(67, 16)
        Me.zlbl_11.TabIndex = 1
        Me.zlbl_11.Text = "Maximum"
        '
        'zlbl_13
        '
        Me.zlbl_13.AutoSize = True
        Me.zlbl_13.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_13.ForeColor = System.Drawing.Color.Black
        Me.zlbl_13.Location = New System.Drawing.Point(173, 49)
        Me.zlbl_13.Name = "zlbl_13"
        Me.zlbl_13.Size = New System.Drawing.Size(45, 16)
        Me.zlbl_13.TabIndex = 1
        Me.zlbl_13.Text = "W/m�"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(19, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Minimum"
        '
        'zlbl_10
        '
        Me.zlbl_10.AutoSize = True
        Me.zlbl_10.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_10.ForeColor = System.Drawing.Color.Black
        Me.zlbl_10.Location = New System.Drawing.Point(19, 28)
        Me.zlbl_10.Name = "zlbl_10"
        Me.zlbl_10.Size = New System.Drawing.Size(63, 16)
        Me.zlbl_10.TabIndex = 1
        Me.zlbl_10.Text = "Minimum"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(220, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(109, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "(Yearly values)"
        '
        'zlbl_9
        '
        Me.zlbl_9.AutoSize = True
        Me.zlbl_9.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zlbl_9.ForeColor = System.Drawing.Color.Black
        Me.zlbl_9.Location = New System.Drawing.Point(220, 49)
        Me.zlbl_9.Name = "zlbl_9"
        Me.zlbl_9.Size = New System.Drawing.Size(109, 16)
        Me.zlbl_9.TabIndex = 1
        Me.zlbl_9.Text = "(Yearly values)"
        '
        'env_txt_flux_1
        '
        Me.env_txt_flux_1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.env_txt_flux_1.ForeColor = System.Drawing.Color.Black
        Me.env_txt_flux_1.Location = New System.Drawing.Point(88, 25)
        Me.env_txt_flux_1.Name = "env_txt_flux_1"
        Me.env_txt_flux_1.Size = New System.Drawing.Size(75, 23)
        Me.env_txt_flux_1.TabIndex = 0
        '
        'env_txt_flux_0
        '
        Me.env_txt_flux_0.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.env_txt_flux_0.ForeColor = System.Drawing.Color.Black
        Me.env_txt_flux_0.Location = New System.Drawing.Point(88, 63)
        Me.env_txt_flux_0.Name = "env_txt_flux_0"
        Me.env_txt_flux_0.Size = New System.Drawing.Size(75, 23)
        Me.env_txt_flux_0.TabIndex = 0
        '
        'dlgOrbitOpen
        '
        Me.dlgOrbitOpen.Filter = "(*.orb)|*.orb"
        '
        'dlgOrbitSave
        '
        Me.dlgOrbitSave.Filter = "(*.orb)|*.orb"
        '
        'orbit_lblmsg
        '
        Me.orbit_lblmsg.AutoSize = True
        Me.orbit_lblmsg.BackColor = System.Drawing.Color.Red
        Me.orbit_lblmsg.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.orbit_lblmsg.Location = New System.Drawing.Point(15, 540)
        Me.orbit_lblmsg.Name = "orbit_lblmsg"
        Me.orbit_lblmsg.Size = New System.Drawing.Size(0, 16)
        Me.orbit_lblmsg.TabIndex = 40
        '
        'frmOrbit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1032, 586)
        Me.ControlBox = False
        Me.Controls.Add(Me.orbit_lblmsg)
        Me.Controls.Add(Me.tabOrbit)
        Me.Controls.Add(Me.cmdOk)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(577, 305)
        Me.MaximizeBox = False
        Me.Name = "frmOrbit"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Orbitography"
        Me.zfra.ResumeLayout(False)
        Me.zfra.PerformLayout()
        Me.fraDatabase.ResumeLayout(False)
        Me.fraDatabase.PerformLayout()
        Me.fraType_Orbit.ResumeLayout(False)
        Me.fraType_Orbit.PerformLayout()
        Me.fraElements.ResumeLayout(False)
        Me.fraElements.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.fraAttitude.ResumeLayout(False)
        Me.fraAttitude.PerformLayout()
        Me.zfrm_2.ResumeLayout(False)
        Me.zfrm_2.PerformLayout()
        CType(Me.previewColor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabOrbit.ResumeLayout(False)
        Me.tabOrbit_Orbit.ResumeLayout(False)
        Me.tabOrbit_Environment.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.env_graph, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.CERES_graph, System.ComponentModel.ISupportInitialize).EndInit()
        Me.zgrp_3.ResumeLayout(False)
        Me.zgrp_3.PerformLayout()
        Me.zgrp_2.ResumeLayout(False)
        Me.zgrp_2.PerformLayout()
        Me.zgrp_1.ResumeLayout(False)
        Me.zgrp_1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ChangeColor As System.Windows.Forms.Button
    Friend WithEvents previewColor As System.Windows.Forms.PictureBox
    Public WithEvents lblDescription As System.Windows.Forms.Label
    Public WithEvents fraDatabase As System.Windows.Forms.GroupBox
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Public WithEvents cmbType_Sat As System.Windows.Forms.ComboBox
    Public WithEvents lstSatellites As System.Windows.Forms.ListBox
    Public WithEvents txtNORAD As System.Windows.Forms.TextBox
    Public WithEvents cmdUpdateNORAD As System.Windows.Forms.Button
    Public WithEvents zlbl_1 As System.Windows.Forms.Label
    Public WithEvents etat_connex As System.Windows.Forms.Label
    Public WithEvents zlbl_3 As System.Windows.Forms.Label
    Public WithEvents zlbl_2 As System.Windows.Forms.Label
    Friend WithEvents ColorBox As System.Windows.Forms.ColorDialog
    Friend WithEvents tabOrbit As System.Windows.Forms.TabControl
    Friend WithEvents tabOrbit_Orbit As System.Windows.Forms.TabPage
    Friend WithEvents tabOrbit_Environment As System.Windows.Forms.TabPage
    Friend WithEvents zgrp_3 As System.Windows.Forms.GroupBox
    Friend WithEvents zgrp_2 As System.Windows.Forms.GroupBox
    Friend WithEvents zgrp_1 As System.Windows.Forms.GroupBox
    Friend WithEvents env_txt_flux_1 As System.Windows.Forms.TextBox
    Friend WithEvents env_txt_flux_0 As System.Windows.Forms.TextBox
    Friend WithEvents zlbl_11 As System.Windows.Forms.Label
    Friend WithEvents zlbl_10 As System.Windows.Forms.Label
    Friend WithEvents zlbl_9 As System.Windows.Forms.Label
    Friend WithEvents env_list_months_alb As System.Windows.Forms.ComboBox
    Friend WithEvents env_cmd_sin As System.Windows.Forms.Button
    Friend WithEvents env_txt_albedo As System.Windows.Forms.TextBox
    Friend WithEvents zlbl_12 As System.Windows.Forms.Label
    Friend WithEvents env_list_months_ir As System.Windows.Forms.ComboBox
    Friend WithEvents env_txt_var As System.Windows.Forms.TextBox
    Friend WithEvents env_txt_ir As System.Windows.Forms.TextBox
    Friend WithEvents env_cmd_isoterre As System.Windows.Forms.Button
    Friend WithEvents env_graph As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents zlbl_13 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents cmdOrbitOpen As System.Windows.Forms.Button
    Public WithEvents cmdOrbitSave As System.Windows.Forms.Button
    Friend WithEvents dlgOrbitOpen As System.Windows.Forms.OpenFileDialog
    Friend WithEvents dlgOrbitSave As System.Windows.Forms.SaveFileDialog
    Friend WithEvents cmdOk As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents orbit_lblmsg As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Alb_Ce_check As System.Windows.Forms.CheckBox
    Friend WithEvents Earth_Ce_check As System.Windows.Forms.CheckBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents CERES_graph As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents CERES_month As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents cmbBckTrackCO As ComboBox
    Public WithEvents cmbFwdTrackCO As ComboBox
#End Region
End Class