﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting


<TestClass()> Public Class MathFncUnitTest



    ''' <summary>
    ''' Test 3D vector initialization function
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod> Public Sub vct_test()

        ' ARRANGE

        Dim v_expected As vector
        v_expected.X = 1
        v_expected.Y = 1
        v_expected.Z = 1

        ' ACT

        Dim v_actual = vct(1, 1, 1)

        ' ASSERT

        Assert.AreEqual(v_expected, v_actual)

    End Sub


    <TestMethod> Public Sub vctNorme_test()

        ' ARRANGE

        Dim v As vector = vct(6, 2, 3)
        Dim norm_expected As Double = 7

        ' ACT

        Dim norm_actual = vctNorme(v)

        ' ASSERT 

        Assert.AreEqual(norm_expected, norm_actual)

    End Sub



    ''' <summary>
    ''' Test the cross product function between 2 vectors
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()> Public Sub vct_cross_product_test()

        ' ARRANGE
        Dim v1 As vector = vct(1, 2, 3)
        Dim v2 As vector = vct(4, 5, 6)
        Dim v_expected As vector = vct(-3, 6, -3)

        ' ACT
        Dim v_cross As vector = vctCross(v1, v2)

        ' ASSERT
        Assert.AreEqual(v_expected, v_cross)

    End Sub









End Class
