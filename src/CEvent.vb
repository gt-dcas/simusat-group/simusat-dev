﻿Option Strict Off
Option Explicit On
Friend Class CEvent
    '
    Public EventName As String          ' Name of the event
    Public EventTime As Double          ' Time of occurrence (in seconds after the beginning of the simulation)
    Public EventDescription As String   ' Description of the event
    '
    ' Flywheel
    ' --------
    Public EventJR(2) As Double
    Public Eventom(2) As Double
    '
    ' Magnetic dipole
    ' ---------------
    Public Eventdipsat_active As vector
    '
    ' Dampers
    ' -------
    ' Add variable declaration (ex: position, velocity of the dampers)
    '
    ' Nozzle
    ' ------
    Public Eventjet(2) As Double
    '
    ''' <summary>
    ''' Load data of the event
    ''' </summary>
    ''' <remarks></remarks>
    Friend Sub loadEvent()
        '
        Dim i As Integer
        For i = 0 To 2
            JR(i) = EventJR(i)
            om(i) = Eventom(i)
            jet(i) = Eventjet(i)
        Next
        '
        dipsat_active.X = Eventdipsat_active.X
        dipsat_active.Y = Eventdipsat_active.Y
        dipsat_active.Z = Eventdipsat_active.Z
        ''
    End Sub

    ''' <summary>
    ''' Compare 2 events according to the time of occurrence
    ''' </summary>
    ''' <param name="otherEvent"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function isBiggerThan(ByRef otherEvent As CEvent) As Boolean
        isBiggerThan = (EventTime > otherEvent.EventTime)
    End Function

End Class
