﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting


<TestClass()> Public Class MathFncUnitTest

    Dim epsilon As Double = 0.01

    <TestMethod> Public Sub vctNorm_test()

        ' ARRANGE

        Dim v As vector = vct(6, 2, 3)
        Dim norm_expected As Double = 7

        ' ACT

        Dim norm_actual As Double = vctNorm(v)

        ' ASSERT 

        Assert.AreEqual(norm_expected, norm_actual)

    End Sub



    ''' <summary>
    ''' Test the cross product function between 2 vectors
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()> Public Sub vct_cross_product_test()

        ' ARRANGE
        Dim v1 As vector = vct(1, 2, 3)
        Dim v2 As vector = vct(4, 5, 6)
        Dim v_expected As vector = vct(-3, 6, -3)

        ' ACT
        Dim v_cross As vector = vctCross(v1, v2)

        ' ASSERT
        Assert.AreEqual(v_expected, v_cross)

    End Sub



    <TestMethod> Public Sub vctMultScalar_test()

        ' ARRANGE

        Dim v As vector = vct(1, 2, 3)
        Dim v_expected As vector = vct(2, 4, 6)
        Dim k As Double = 2

        ' ACT

        Dim v_actual As vector = vctMultScalar(v, k)

        ' ASSERT

        Assert.AreEqual(v_expected, v_actual)


    End Sub


    <TestMethod> Public Sub almostEqual_test()

        ' ARRANGE

        Dim x As Double = 5
        Dim y As Double = 5.005
        Dim delta As Double = 0.0001
        Dim expected As Boolean = False

        ' ACT

        Dim actual As Boolean = almostEqual(x, y, delta)


        ' ASSERT

        Assert.AreEqual(expected, actual)


    End Sub


    <TestMethod> Public Sub MatrixProd_test()

        ' ARRANGE

        Dim M(2, 2) As Double
        Dim N(2, 2) As Double

        For i As Integer = 0 To 2
            For j As Integer = 0 To 2
                M(i, j) = i * 5
            Next
        Next

        For i As Integer = 0 To 2
            For j As Integer = 0 To 2
                N(i, j) = i * 2
            Next
        Next

        Dim expected(2, 2) As Double

        For i As Integer = 0 To 2
            For j As Integer = 0 To 2
                expected(i, j) = i * 30
            Next
        Next


        ' ACT

        Dim actual As Double(,)

        actual = MatrixProd(M, N)


        ' ASSERT

        CollectionAssert.AreEqual(expected, actual)


    End Sub


    <TestMethod> Public Sub Huygens_test()

        ' ARRANGE

        Dim m(,) As Double = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}

        m(0, 0) = 0
        m(1, 1) = 0
        m(2, 2) = 0


        Dim mass As Double = 1
        Dim start As vector = vct(0, 0.4, 0)
        Dim dest As vector = vct(0.625, 0, 0)

        Dim h(2, 2) As Double


        Dim expected As Double(,) = {{0.16, 0.25, 0}, {0.25, 0.3906, 0}, {0, 0, 0.5506}}

        ' ACT

        Dim actual As Double(,) = Huygens(m, mass, start, dest)

        ' ASSERT

        Assert.AreEqual(expected(0, 0), actual(0, 0), epsilon)
        Assert.AreEqual(expected(0, 1), actual(0, 1), epsilon)
        Assert.AreEqual(expected(0, 2), actual(0, 2), epsilon)
        Assert.AreEqual(expected(1, 0), actual(1, 0), epsilon)
        Assert.AreEqual(expected(1, 1), actual(1, 1), epsilon)
        Assert.AreEqual(expected(1, 2), actual(1, 2), epsilon)
        Assert.AreEqual(expected(2, 0), actual(2, 0), epsilon)
        Assert.AreEqual(expected(2, 1), actual(2, 1), epsilon)
        Assert.AreEqual(expected(2, 2), actual(2, 2), epsilon)


    End Sub

    <TestMethod> Public Sub Actan_test()
        Dim x As Double
        ' ARRANGE
        For i As Integer = 0 To 4

            Select Case i
                Case i = 0
                    x = 0
                Case i = 1
                    x = Math.PI / 2
                Case i = 2
                    x = 3
                Case i = 3
                    x = 3 * Math.PI / 2
                Case i = 4
                    x = 7
            End Select

            ' ACT

            Dim actual As Double = Actan(Math.Sin(x), Math.Cos(x))


            ' ASSERT

            Assert.AreEqual(x, actual)
        Next

    End Sub


End Class
