﻿Module ESAmodel
    Dim inclinterp As vector
    Dim monthinterp As vector
    Dim tempInterp As vector
    Dim coefInterp As vector

    'Bibliographic reference:
    'Earth Albedo and Earth IR Temperature definition based on CERES measurements (ESA-TECMTT-TN-001975)

    Public Function EarthTemp(ByVal inclination As Double, ByVal month As Double) As Double
        'Whole file inserted in "lines" variable
        Dim i As Integer = 1 'line 'Starts in one because 0 is the headers
        Dim j As Integer = 0 'column
        Dim lines() As String = System.IO.File.ReadAllLines(My.Application.Info.DirectoryPath & EarthTempFile) 'ligne 0:header, 

        'We calculate the limits of the table
        Dim nlines As Integer = lines.Length - 1
        Dim ncol As Integer = ColumnNumber(lines)


        'Line selection
        While CsvVal(lines, i, 0) > inclination And i < nlines
            inclinterp.X = CsvVal(lines, i, 0)
            inclinterp.Y = CsvVal(lines, i + 1, 0)
            i = i + 1
        End While
        If i = 1 Then
            inclinterp.X = CsvVal(lines, i, 0)
            inclinterp.Y = CsvVal(lines, i + 1, 0)
        Else
            i = i - 1  'To always point to the first component
        End If



        'Column Selection
        j = 1
        While CsvVal(lines, 0, j) < month And j < ncol
            j = j + 1
        End While

        'Second component for interpolation
        tempInterp.X = CsvVal(lines, i, j)
        tempInterp.Y = CsvVal(lines, i + 1, j)

        '
        'Final interpolation
        '
        EarthTemp = Interpolation(inclinterp, tempInterp, inclination)

    End Function

    Public Function AlbedoCoef(ByVal inclination As Double, ByVal month As Double) As Double
        'Whole file inserted in "lines" variable
        Dim i As Integer = 1 'line 'Starts in one because 0 is the headers
        Dim j As Integer = 0 'column
        Dim lines() As String = System.IO.File.ReadAllLines(My.Application.Info.DirectoryPath & AlbedoCoefFile) 'ligne 0:header, 

        'We calculate the limits of the table
        Dim nlines As Integer = lines.Length - 1
        Dim ncol As Integer = ColumnNumber(lines)


        'Line selection
        While CsvVal(lines, i, 0) > inclination And i < nlines
            inclinterp.X = CsvVal(lines, i, 0)
            inclinterp.Y = CsvVal(lines, i + 1, 0)
            i = i + 1
        End While
        If i = 1 Then
            inclinterp.X = CsvVal(lines, i, 0)
            inclinterp.Y = CsvVal(lines, i + 1, 0)
        Else
            i = i - 1  'To always point to the first component
        End If



        'Column Selection
        j = 1
        While CsvVal(lines, 0, j) < month And j < ncol
            j = j + 1
        End While

        'Second component for interpolation
        coefInterp.X = CsvVal(lines, i, j)
        coefInterp.Y = CsvVal(lines, i + 1, j)

        '
        'Final interpolation
        '
        If Interpolation(inclinterp, coefInterp, inclination) >= 0 Then
            AlbedoCoef = Interpolation(inclinterp, coefInterp, inclination)
        Else : AlbedoCoef = 0
        End If


    End Function

    '
    'This function calculates the number of columns
    '
    Friend Function ColumnNumber(ByVal lines As Array) As Integer
        ColumnNumber = lines(0).Split(",").Length
    End Function

    '
    'This function interpolates in the table'
    '
    Friend Function Interpolation(ByVal inclinterp As vector, ByVal varInterp As vector, inclination As Double) As Double
        'Linear interpolation
        Interpolation = varInterp.X + (inclination - inclinterp.X) * (varInterp.Y - varInterp.X) / (inclinterp.Y - inclinterp.X)
    End Function

    '
    'This function takes a value from the table
    '
    Friend Function CsvVal(ByVal table As Array, ByVal i As Integer, ByVal j As Integer) As Double
        Dim line As String = table(i)
        Dim lineArray() As String = Split(line, ",")
        CsvVal = Val(lineArray(j))
    End Function

    Public Function AvgCERES(ByVal file As String, ByVal month As Double) As Double
        'Whole file inserted in "lines" variable
        Dim i As Integer = 1 'line 'Starts in one because 0 is the headers
        Dim j As Integer = 0 'column
        Dim lines() As String = System.IO.File.ReadAllLines(My.Application.Info.DirectoryPath & file) 'ligne 0:header, 
        'We calculate the limits of the table
        Dim nlines As Integer = lines.Length - 1
        Dim ncol As Integer = ColumnNumber(lines)
        Dim sum As Double = 0

        'Column Selection
        j = 1
        While CsvVal(lines, 0, j) < month And j < ncol
            j = j + 1
        End While

        'Line selection
        While i <= nlines
            sum += CsvVal(lines, i, j)
            i = i + 1
        End While
        '
        'Average
        '
        AvgCERES = sum / (nlines)

    End Function
End Module
