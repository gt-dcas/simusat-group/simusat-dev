﻿
Imports YamlDotNet.Serialization
Imports YamlDotNet.RepresentationModel


Public Class Form1
    Public Isc As Single

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Dim File_name As String = My.Application.Info.DirectoryPath & "\input.yaml"
        Dim YMLReader = System.IO.File.OpenText(File_name)

        Dim deserializerBuilder = New DeserializerBuilder()
        Dim deserializer = deserializerBuilder.Build()
        Dim itemTypes = deserializer.Deserialize(Of Input)(YMLReader)

        get_data_node(itemTypes, TreeView1.SelectedNode)

        YMLReader.Close()

    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Form2.Show()
    End Sub

    Private Sub get_data_node(ByVal deserialized As Input, ByVal Node As TreeNode)

        Select Case Node.Level
            Case 0

                Select Case Node.Text
                    Case "paths"
                        TextBox1.Text = "scilab_path :" & deserialized.paths.scilab_path.ToString & System.Environment.NewLine _
                        & "GMAT_BIN :" & deserialized.paths.GMAT_BIN.ToString & System.Environment.NewLine _
                        & "GMAT_BIN_linux :" & deserialized.paths.GMAT_BIN_linux.ToString & System.Environment.NewLine _
                        & "scilab_BIN_linux_docker :" & deserialized.paths.GMAT_BIN_linux_docker.ToString & System.Environment.NewLine _
                        & "input_dir:" & deserialized.paths.input_dir.ToString & System.Environment.NewLine _
                        & "output_dir:" & deserialized.paths.output_dir.ToString & System.Environment.NewLine
                    Case "SIMULATION"
                        TextBox1.Text = "report_name : " & deserialized.SIMULATION.report_name.ToString & System.Environment.NewLine _
                        & "total_time : " & deserialized.SIMULATION.total_time.ToString & System.Environment.NewLine _
                        & "dt : " & deserialized.SIMULATION.dt.ToString & System.Environment.NewLine
                    Case "ORBIT"
                        TextBox1.Text = "epoch : " & deserialized.ORBIT.epoch.ToString & System.Environment.NewLine _
                       & "SMA : " & deserialized.ORBIT.SMA.ToString & System.Environment.NewLine _
                       & "ECC : " & deserialized.ORBIT.ECC.ToString & System.Environment.NewLine _
                       & "INC : " & deserialized.ORBIT.INC.ToString & System.Environment.NewLine _
                       & "RAAN : " & deserialized.ORBIT.RAAN.ToString & System.Environment.NewLine _
                       & "AOP : " & deserialized.ORBIT.AOP.ToString & System.Environment.NewLine _
                       & "TA : " & deserialized.ORBIT.TA.ToString & System.Environment.NewLine
                    Case "POWER_BUDGET"
                        TextBox1.Text = "TotBatPower : " & deserialized.POWER_BUDGET.TotBatPower.ToString & System.Environment.NewLine _
                       & "SolarPower : " & deserialized.POWER_BUDGET.SolarPower.ToString & System.Environment.NewLine _
                       & "Pmesure : " & deserialized.POWER_BUDGET.Pmesure.ToString & System.Environment.NewLine _
                       & "Pvidage : " & deserialized.POWER_BUDGET.Pvidage.ToString & System.Environment.NewLine
                    Case "DATA_BUDGET"
                        TextBox1.Text = "TC_debit_nominal : " & deserialized.DATA_BUDGET.TC_debit_nominal.ToString & System.Environment.NewLine _
                       & "TM_debit_nominal : " & deserialized.DATA_BUDGET.TM_debit_nominal.ToString & System.Environment.NewLine
                    Case "CONSTANTS"
                        TextBox1.Text = "radius_earth : " & deserialized.CONSTANTS.radius_earth.ToString & System.Environment.NewLine _
                       & "c : " & deserialized.CONSTANTS.c.ToString & System.Environment.NewLine _
                       & "k_dB : " & deserialized.CONSTANTS.k_dB.ToString & System.Environment.NewLine
                    Case "SPACECRAFT_FLIGHT_DYNAMICS"
                        TextBox1.Text = "SC_altitude : " & deserialized.SPACECRAFT_FLIGHT_DYNAMICS.SC_altitude.ToString & System.Environment.NewLine
                    Case "GROUNDSTATION_LOCATION"
                        TextBox1.Text = "GS_altitude : " & deserialized.GROUNDSTATION_LOCATION.GS_altitude.ToString & System.Environment.NewLine _
                       & "GS_minElevation : " & deserialized.GROUNDSTATION_LOCATION.GS_minElevation.ToString & System.Environment.NewLine _
                       & "GS_latitude : " & deserialized.GROUNDSTATION_LOCATION.GS_latitude.ToString & System.Environment.NewLine _
                       & "GS_longitude : " & deserialized.GROUNDSTATION_LOCATION.GS_longitude.ToString & System.Environment.NewLine
                    Case "DOWNLINK"
                        TextBox1.Text = "frequency : " & deserialized.DOWNLINK.frequency.ToString & System.Environment.NewLine _
                       & "data_rate : " & deserialized.DOWNLINK.data_rate.ToString & System.Environment.NewLine _
                       & "required_BER_from_modulation : " & deserialized.DOWNLINK.required_BER_from_Modulation.ToString & System.Environment.NewLine _
                       & "modulation : " & deserialized.DOWNLINK.modulation.ToString & System.Environment.NewLine _
                       & "system_margin : " & deserialized.DOWNLINK.system_margin.ToString & System.Environment.NewLine
                    Case "UPLINK"
                        TextBox1.Text = "frequency : " & deserialized.UPLINK.frequency.ToString & System.Environment.NewLine _
                       & "data_rate : " & deserialized.UPLINK.data_rate.ToString & System.Environment.NewLine _
                       & "required_BER_from_modulation : " & deserialized.UPLINK.required_BER_from_Modulation.ToString & System.Environment.NewLine _
                       & "modulation : " & deserialized.UPLINK.modulation.ToString & System.Environment.NewLine _
                       & "system_margin : " & deserialized.UPLINK.system_margin.ToString & System.Environment.NewLine
                    Case "PROPAGATION_LOSSES"
                        TextBox1.Text = "loss_pol : " & deserialized.PROPAGATION_LOSSES.loss_pol.ToString & System.Environment.NewLine _
                       & "loss_atm : " & deserialized.PROPAGATION_LOSSES.loss_atm.ToString & System.Environment.NewLine _
                       & "loss_scin : " & deserialized.PROPAGATION_LOSSES.loss_scin.ToString & System.Environment.NewLine _
                       & "loss_rain : " & deserialized.PROPAGATION_LOSSES.loss_rain.ToString & System.Environment.NewLine _
                       & "loss_cloud : " & deserialized.PROPAGATION_LOSSES.loss_cloud.ToString & System.Environment.NewLine _
                       & "loss_si : " & deserialized.PROPAGATION_LOSSES.loss_si.ToString & System.Environment.NewLine _
                       & "loss_misc : " & deserialized.PROPAGATION_LOSSES.loss_misc.ToString & System.Environment.NewLine
                    Case "SPACECRAFT_TRANSMITTER"
                        TextBox1.Text = "SC_power_tx : " & deserialized.SPACECRAFT_TRANSMITTER.SC_power_tx.ToString & System.Environment.NewLine _
                       & "SC_ant_gain_tx : " & deserialized.SPACECRAFT_TRANSMITTER.SC_ant_gain_tx.ToString & System.Environment.NewLine _
                       & "SC_loss_cable_tx : " & deserialized.SPACECRAFT_TRANSMITTER.SC_loss_cable_tx.ToString & System.Environment.NewLine _
                       & "SC_loss_connector_tx : " & deserialized.SPACECRAFT_TRANSMITTER.SC_loss_connector_tx.ToString & System.Environment.NewLine _
                       & "SC_loss_feeder_tx : " & deserialized.SPACECRAFT_TRANSMITTER.SC_loss_feeder_tx.ToString & System.Environment.NewLine _
                       & "SC_misc : " & deserialized.SPACECRAFT_TRANSMITTER.SC_misc.ToString & System.Environment.NewLine _
                       & "SC_loss_point_tx : " & deserialized.SPACECRAFT_TRANSMITTER.SC_loss_point_tx.ToString & System.Environment.NewLine
                    Case "SPACECRAFT_RECEIVER"
                        TextBox1.Text = "SC_ant_gain_rx : " & deserialized.SPACECRAFT_RECEIVER.SC_ant_gain_rx.ToString & System.Environment.NewLine _
                       & "SC_eta_rx : " & deserialized.SPACECRAFT_RECEIVER.SC_eta_rx.ToString & System.Environment.NewLine _
                       & "SC_loss_point_rx : " & deserialized.SPACECRAFT_RECEIVER.SC_loss_point_rx.ToString & System.Environment.NewLine _
                       & "SC_loss_cable_rx : " & deserialized.SPACECRAFT_RECEIVER.SC_loss_cable_rx.ToString & System.Environment.NewLine _
                       & "SC_loss_connector_rx : " & deserialized.SPACECRAFT_RECEIVER.SC_loss_connector_rx.ToString & System.Environment.NewLine _
                       & "SC_LNA_gain : " & deserialized.SPACECRAFT_RECEIVER.SC_LNA_gain.ToString & System.Environment.NewLine _
                       & "SC_T_rx : " & deserialized.SPACECRAFT_RECEIVER.SC_T_rx.ToString & System.Environment.NewLine
                    Case "GROUND_STATION_RECEIVER"
                        TextBox1.Text = "eta_rx : " & deserialized.GROUND_STATION_RECEIVER.eta_rx.ToString & System.Environment.NewLine _
                       & "antennaDiameter : " & deserialized.GROUND_STATION_RECEIVER.antennaDiameter.ToString & System.Environment.NewLine _
                       & "GS_LNA_gain : " & deserialized.GROUND_STATION_RECEIVER.GS_LNA_gain.ToString & System.Environment.NewLine _
                       & "GS_T_rx : " & deserialized.GROUND_STATION_RECEIVER.GS_T_rx.ToString & System.Environment.NewLine _
                       & "GS_loss_point_rx : " & deserialized.GROUND_STATION_RECEIVER.GS_loss_point_rx.ToString & System.Environment.NewLine _
                       & "GS_loss_cable_rx : " & deserialized.GROUND_STATION_RECEIVER.GS_loss_cable_rx.ToString & System.Environment.NewLine _
                       & "GS_loss_cable_D_rx : " & deserialized.GROUND_STATION_RECEIVER.GS_loss_cable_D_rx.ToString & System.Environment.NewLine _
                       & "GS_loss_connector_rx : " & deserialized.GROUND_STATION_RECEIVER.GS_loss_connector_rx.ToString & System.Environment.NewLine
                    Case "GROUND_STATION_TRANSMITTER"
                        TextBox1.Text = "GS_power_tx : " & deserialized.GROUND_STATION_TRANSMITTER.GS_power_tx.ToString & System.Environment.NewLine _
                       & "GS_line_loss_tx : " & deserialized.GROUND_STATION_TRANSMITTER.GS_line_loss_tx.ToString & System.Environment.NewLine _
                       & "GS_loss_connector_tx : " & deserialized.GROUND_STATION_TRANSMITTER.GS_loss_connector_tx.ToString & System.Environment.NewLine _
                       & "GS_ant_gain_tx : " & deserialized.GROUND_STATION_TRANSMITTER.GS_ant_gain_tx.ToString & System.Environment.NewLine _
                       & "GS_loss_point_rx : " & deserialized.GROUND_STATION_TRANSMITTER.GS_loss_point_rx.ToString & System.Environment.NewLine
                    Case "SOLAR_CELLS"
                        TextBox1.Text = "Cell_type : " & deserialized.SOLAR_CELLS.Cell_type.ToString & System.Environment.NewLine _
                       & "Isc : " & deserialized.SOLAR_CELLS.Isc.ToString & System.Environment.NewLine
                    Case "SOLAR_PANELS"
                        TextBox1.Text = "Name : " & deserialized.SOLAR_PANELS.Name.ToString & System.Environment.NewLine _
                       & "Mounting_face : " & deserialized.SOLAR_PANELS.Mounting_face.ToString & System.Environment.NewLine
                End Select
            Case 1
                Isc = deserialized.SOLAR_CELLS.Isc
                Label1.Text = Isc
                Select Case Node.Parent.Text
                    Case "paths"
                        Select Case Node.Text
                            Case "scilab_path"
                                TextBox1.Text = deserialized.paths.scilab_path
                            Case "GMAT_BIN"
                                TextBox1.Text = deserialized.paths.GMAT_BIN
                            Case "GMAT_BIN_linux"
                                TextBox1.Text = deserialized.paths.GMAT_BIN_linux
                            Case "GMAT_BIN_linux_docker"
                                TextBox1.Text = deserialized.paths.GMAT_BIN_linux_docker
                            Case "input_dir"
                                TextBox1.Text = deserialized.paths.input_dir
                            Case "output_dir"
                                TextBox1.Text = deserialized.paths.output_dir

                        End Select
                    Case "SIMULATION"
                        Select Case Node.Text
                            Case "report_name"
                                TextBox1.Text = deserialized.SIMULATION.report_name
                            Case "total_time"
                                TextBox1.Text = deserialized.SIMULATION.total_time
                            Case "dt"
                                TextBox1.Text = deserialized.SIMULATION.dt
                        End Select
                    Case "ORBIT"
                        Select Case Node.Text
                            Case "epoch"
                                TextBox1.Text = deserialized.ORBIT.epoch
                            Case "SMA"
                                TextBox1.Text = deserialized.ORBIT.SMA
                            Case "ECC"
                                TextBox1.Text = deserialized.ORBIT.ECC
                            Case "INC"
                                TextBox1.Text = deserialized.ORBIT.INC
                            Case "RAAN"
                                TextBox1.Text = deserialized.ORBIT.RAAN
                            Case "AOP"
                                TextBox1.Text = deserialized.ORBIT.AOP
                            Case "TA"
                                TextBox1.Text = deserialized.ORBIT.TA
                        End Select
                    Case "POWER_BUDGET"
                        Select Case Node.Text
                            Case "TotBatPower"
                                TextBox1.Text = deserialized.POWER_BUDGET.TotBatPower
                            Case "SolarPower"
                                TextBox1.Text = deserialized.POWER_BUDGET.SolarPower
                            Case "Pmesure"
                                TextBox1.Text = deserialized.POWER_BUDGET.Pmesure
                            Case "Pvidage"
                                TextBox1.Text = deserialized.POWER_BUDGET.Pvidage
                        End Select
                    Case "DATA_BUDGET"
                        Select Case Node.Text
                            Case "TC_debit_nominal"
                                TextBox1.Text = deserialized.DATA_BUDGET.TC_debit_nominal
                            Case "TM_debit_nominal"
                                TextBox1.Text = deserialized.DATA_BUDGET.TM_debit_nominal
                        End Select
                    Case "CONSTANTS"
                        Select Case Node.Text
                            Case "radius_earth"
                                TextBox1.Text = deserialized.CONSTANTS.radius_earth
                            Case "c"
                                TextBox1.Text = deserialized.CONSTANTS.c
                            Case "k_dB"
                                TextBox1.Text = deserialized.CONSTANTS.k_dB
                        End Select
                    Case "SPACECRAFT_FLIGHT_DYNAMICS"
                        Select Case Node.Text
                            Case "SC_altitude"
                                TextBox1.Text = deserialized.SPACECRAFT_FLIGHT_DYNAMICS.SC_altitude
                        End Select
                    Case "GROUNDSTATION_LOCATION"
                        Select Case Node.Text
                            Case "GS_altitude"
                                TextBox1.Text = deserialized.GROUNDSTATION_LOCATION.GS_altitude
                            Case "GS_minElevation"
                                TextBox1.Text = deserialized.GROUNDSTATION_LOCATION.GS_minElevation
                            Case "GS_latitude"
                                TextBox1.Text = deserialized.GROUNDSTATION_LOCATION.GS_latitude
                            Case "GS_longitude"
                                TextBox1.Text = deserialized.GROUNDSTATION_LOCATION.GS_longitude
                        End Select
                    Case "DOWNLINK"
                        Select Case Node.Text
                            Case "frequency"
                                TextBox1.Text = deserialized.DOWNLINK.frequency
                            Case "data_rate"
                                TextBox1.Text = deserialized.DOWNLINK.data_rate
                            Case "required_BER_from_modulation"
                                TextBox1.Text = deserialized.DOWNLINK.required_BER_from_Modulation
                            Case "modulation"
                                TextBox1.Text = deserialized.DOWNLINK.modulation
                            Case "system_margin"
                                TextBox1.Text = deserialized.DOWNLINK.system_margin
                        End Select
                    Case "UPLINK"
                        Select Case Node.Text
                            Case "frequency"
                                TextBox1.Text = deserialized.UPLINK.frequency
                            Case "data_rate"
                                TextBox1.Text = deserialized.UPLINK.data_rate
                            Case "required_BER_from_modulation"
                                TextBox1.Text = deserialized.UPLINK.required_BER_from_Modulation
                            Case "modulation"
                                TextBox1.Text = deserialized.UPLINK.modulation
                            Case "system_margin"
                                TextBox1.Text = deserialized.UPLINK.system_margin
                        End Select
                    Case "PROPAGATION_LOSSES"
                        Select Case Node.Text
                            Case "loss_pol"
                                TextBox1.Text = deserialized.PROPAGATION_LOSSES.loss_pol
                            Case "loss_atm"
                                TextBox1.Text = deserialized.PROPAGATION_LOSSES.loss_atm
                            Case "loss_scin"
                                TextBox1.Text = deserialized.PROPAGATION_LOSSES.loss_scin
                            Case "loss_rain"
                                TextBox1.Text = deserialized.PROPAGATION_LOSSES.loss_rain
                            Case "loss_cloud"
                                TextBox1.Text = deserialized.PROPAGATION_LOSSES.loss_cloud
                            Case "loss_si"
                                TextBox1.Text = deserialized.PROPAGATION_LOSSES.loss_si
                            Case "loss_misc"
                                TextBox1.Text = deserialized.PROPAGATION_LOSSES.loss_misc
                        End Select
                    Case "SPACECRAFT_TRANSMITTER"
                        Select Case Node.Text
                            Case "SC_power_tx"
                                TextBox1.Text = deserialized.SPACECRAFT_TRANSMITTER.SC_power_tx
                            Case "SC_ant_gain_tx"
                                TextBox1.Text = deserialized.SPACECRAFT_TRANSMITTER.SC_ant_gain_tx
                            Case "SC_loss_cable_tx"
                                TextBox1.Text = deserialized.SPACECRAFT_TRANSMITTER.SC_loss_cable_tx
                            Case "SC_loss_connector_tx"
                                TextBox1.Text = deserialized.SPACECRAFT_TRANSMITTER.SC_loss_connector_tx
                            Case "SC_loss_feeder_tx"
                                TextBox1.Text = deserialized.SPACECRAFT_TRANSMITTER.SC_loss_feeder_tx
                            Case "SC_misc"
                                TextBox1.Text = deserialized.SPACECRAFT_TRANSMITTER.SC_misc
                            Case "SC_loss_point_tx"
                                TextBox1.Text = deserialized.SPACECRAFT_TRANSMITTER.SC_loss_point_tx
                        End Select
                    Case "SPACECRAFT_RECEIVER"
                        Select Case Node.Text
                            Case "SC_ant_gain_rx"
                                TextBox1.Text = deserialized.SPACECRAFT_RECEIVER.SC_ant_gain_rx
                            Case "SC_eta_rx"
                                TextBox1.Text = deserialized.SPACECRAFT_RECEIVER.SC_eta_rx
                            Case "SC_loss_point_rx"
                                TextBox1.Text = deserialized.SPACECRAFT_RECEIVER.SC_loss_point_rx
                            Case "SC_loss_cable_rx"
                                TextBox1.Text = deserialized.SPACECRAFT_RECEIVER.SC_loss_cable_rx
                            Case "SC_loss_connector_rx"
                                TextBox1.Text = deserialized.SPACECRAFT_RECEIVER.SC_loss_connector_rx
                            Case "SC_LNA_gain"
                                TextBox1.Text = deserialized.SPACECRAFT_RECEIVER.SC_LNA_gain
                            Case "SC_T_rx"
                                TextBox1.Text = deserialized.SPACECRAFT_RECEIVER.SC_T_rx
                        End Select
                    Case "GROUND_STATION_RECEIVER"
                        Select Case Node.Text
                            Case "eta_rx"
                                TextBox1.Text = deserialized.GROUND_STATION_RECEIVER.eta_rx
                            Case "antennaDiameter"
                                TextBox1.Text = deserialized.GROUND_STATION_RECEIVER.antennaDiameter
                            Case "GS_LNA_gain"
                                TextBox1.Text = deserialized.GROUND_STATION_RECEIVER.GS_LNA_gain
                            Case "GS_T_rx"
                                TextBox1.Text = deserialized.GROUND_STATION_RECEIVER.GS_T_rx
                            Case "GS_loss_point_rx"
                                TextBox1.Text = deserialized.GROUND_STATION_RECEIVER.GS_loss_point_rx
                            Case "GS_loss_cable_rx"
                                TextBox1.Text = deserialized.GROUND_STATION_RECEIVER.GS_loss_cable_rx
                            Case "GS_loss_cable_D_rx"
                                TextBox1.Text = deserialized.GROUND_STATION_RECEIVER.GS_loss_cable_D_rx
                            Case "GS_loss_connector_rx"
                                TextBox1.Text = deserialized.GROUND_STATION_RECEIVER.GS_loss_connector_rx
                        End Select
                    Case "GROUND_STATION_TRANSMITTER"
                        Select Case Node.Text
                            Case "GS_power_tx"
                                TextBox1.Text = deserialized.GROUND_STATION_TRANSMITTER.GS_power_tx
                            Case "GS_line_loss_tx"
                                TextBox1.Text = deserialized.GROUND_STATION_TRANSMITTER.GS_line_loss_tx
                            Case "GS_loss_connector_tx"
                                TextBox1.Text = deserialized.GROUND_STATION_TRANSMITTER.GS_loss_connector_tx
                            Case "GS_ant_gain_tx"
                                TextBox1.Text = deserialized.GROUND_STATION_TRANSMITTER.GS_ant_gain_tx
                            Case "GS_loss_point_rx"
                                TextBox1.Text = deserialized.GROUND_STATION_TRANSMITTER.GS_loss_point_rx
                        End Select
                    Case "SOLAR_CELLS"
                        Select Case Node.Text
                            Case "Cell_type"
                                TextBox1.Text = deserialized.SOLAR_CELLS.Cell_type
                            Case "Isc"
                                TextBox1.Text = deserialized.SOLAR_CELLS.Isc
                                'Isc = deserialized.SOLAR_CELLS.Isc
                                'TextBox1.Text = Isc


                        End Select

                        ' TEST PASSAGE DES PARAMETRES
                    Case "SOLAR_PANELS"

                        Select Case Node.Text

                            Case "Name"
                                'TextBox1.Text = deserialized.SOLAR_PANELS.Name
                                Name = deserialized.SOLAR_PANELS.Name
                                TextBox1.Text = Name
                                'Console.WriteLine("NAME " + TempPanel.PanelName)
                            Case "Mounting_face"
                                TextBox1.Text = deserialized.SOLAR_PANELS.Mounting_face

                        End Select
                End Select
        End Select

        Isc = deserialized.SOLAR_CELLS.Isc
        'Isc = 0.53
        Label1.Text = Isc

        'FUNCIONA SACANDO TODO SOLO ES NEVESARIO OPRIMIR EL BOTON



    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        TreeView1.Nodes.Add("paths")
        TreeView1.Nodes(0).Nodes.Add("scilab_path")
        TreeView1.Nodes(0).Nodes.Add("GMAT_BIN")
        TreeView1.Nodes(0).Nodes.Add("GMAT_BIN_linux")
        TreeView1.Nodes(0).Nodes.Add("GMAT_BIN_linux_docker")
        TreeView1.Nodes(0).Nodes.Add("input_dir")
        TreeView1.Nodes(0).Nodes.Add("output_dir")

        TreeView1.Nodes.Add("SIMULATION")
        TreeView1.Nodes(1).Nodes.Add("report_name")
        TreeView1.Nodes(1).Nodes.Add("total_time")
        TreeView1.Nodes(1).Nodes.Add("dt")

        TreeView1.Nodes.Add("ORBIT")
        TreeView1.Nodes(2).Nodes.Add("epoch")
        TreeView1.Nodes(2).Nodes.Add("SMA")
        TreeView1.Nodes(2).Nodes.Add("ECC")
        TreeView1.Nodes(2).Nodes.Add("INC")
        TreeView1.Nodes(2).Nodes.Add("RAAN")
        TreeView1.Nodes(2).Nodes.Add("AOP")
        TreeView1.Nodes(2).Nodes.Add("TA")

        TreeView1.Nodes.Add("POWER_BUDGET")
        TreeView1.Nodes(3).Nodes.Add("TotBatPower")
        TreeView1.Nodes(3).Nodes.Add("SolarPower")
        TreeView1.Nodes(3).Nodes.Add("Pmesure")
        TreeView1.Nodes(3).Nodes.Add("Pvidage")

        TreeView1.Nodes.Add("DATA_BUDGET")
        TreeView1.Nodes(4).Nodes.Add("TC_debit_nominal")
        TreeView1.Nodes(4).Nodes.Add("TM_debit_nominal")

        TreeView1.Nodes.Add("CONSTANTS")
        TreeView1.Nodes(5).Nodes.Add("radius_earth")
        TreeView1.Nodes(5).Nodes.Add("c")
        TreeView1.Nodes(5).Nodes.Add("k_dB")

        TreeView1.Nodes.Add("SPACECRAFT_FLIGHT_DYNAMICS")
        TreeView1.Nodes(6).Nodes.Add("SC_altitude")

        TreeView1.Nodes.Add("GROUNDSTATION_LOCATION")
        TreeView1.Nodes(7).Nodes.Add("GS_altitude")
        TreeView1.Nodes(7).Nodes.Add("GS_minElevation")
        TreeView1.Nodes(7).Nodes.Add("GS_latitude")
        TreeView1.Nodes(7).Nodes.Add("GS_longitude")

        TreeView1.Nodes.Add("DOWNLINK")
        TreeView1.Nodes(8).Nodes.Add("frequency")
        TreeView1.Nodes(8).Nodes.Add("data_rate")
        TreeView1.Nodes(8).Nodes.Add("required_BER_from_modulation")
        TreeView1.Nodes(8).Nodes.Add("modulation")
        TreeView1.Nodes(8).Nodes.Add("system_margin")

        TreeView1.Nodes.Add("UPLINK")
        TreeView1.Nodes(9).Nodes.Add("frequency")
        TreeView1.Nodes(9).Nodes.Add("data_rate")
        TreeView1.Nodes(9).Nodes.Add("required_BER_from_modulation")
        TreeView1.Nodes(9).Nodes.Add("modulation")
        TreeView1.Nodes(9).Nodes.Add("system_margin")

        TreeView1.Nodes.Add("PROPAGATION_LOSSES")
        TreeView1.Nodes(10).Nodes.Add("loss_pol")
        TreeView1.Nodes(10).Nodes.Add("loss_atm")
        TreeView1.Nodes(10).Nodes.Add("loss_scin")
        TreeView1.Nodes(10).Nodes.Add("loss_rain")
        TreeView1.Nodes(10).Nodes.Add("loss_cloud")
        TreeView1.Nodes(10).Nodes.Add("loss_si")
        TreeView1.Nodes(10).Nodes.Add("loss_misc")

        TreeView1.Nodes.Add("SPACECRAFT_TRANSMITTER")
        TreeView1.Nodes(11).Nodes.Add("SC_power_tx")
        TreeView1.Nodes(11).Nodes.Add("SC_ant_gain_tx")
        TreeView1.Nodes(11).Nodes.Add("SC_loss_cable_tx")
        TreeView1.Nodes(11).Nodes.Add("SC_loss_connector_tx")
        TreeView1.Nodes(11).Nodes.Add("SC_loss_feeder_tx")
        TreeView1.Nodes(11).Nodes.Add("SC_misc")
        TreeView1.Nodes(11).Nodes.Add("SC_loss_point_tx")

        TreeView1.Nodes.Add("SPACECRAFT_RECEIVER")
        TreeView1.Nodes(12).Nodes.Add("SC_ant_gain_rx")
        TreeView1.Nodes(12).Nodes.Add("SC_eta_rx")
        TreeView1.Nodes(12).Nodes.Add("SC_loss_point_rx")
        TreeView1.Nodes(12).Nodes.Add("SC_loss_cable_rx")
        TreeView1.Nodes(12).Nodes.Add("SC_loss_connector_rx")
        TreeView1.Nodes(12).Nodes.Add("SC_LNA_gain")
        TreeView1.Nodes(12).Nodes.Add("SC_T_rx")

        TreeView1.Nodes.Add("GROUND_STATION_RECEIVER")
        TreeView1.Nodes(13).Nodes.Add("eta_rx")
        TreeView1.Nodes(13).Nodes.Add("antennaDiameter")
        TreeView1.Nodes(13).Nodes.Add("GS_LNA_gain")
        TreeView1.Nodes(13).Nodes.Add("GS_T_rx")
        TreeView1.Nodes(13).Nodes.Add("GS_loss_point_rx")
        TreeView1.Nodes(13).Nodes.Add("GS_loss_cable_rx")
        TreeView1.Nodes(13).Nodes.Add("GS_loss_cable_D_rx")
        TreeView1.Nodes(13).Nodes.Add("GS_loss_connector_rx")

        TreeView1.Nodes.Add("GROUND_STATION_TRANSMITTER")
        TreeView1.Nodes(14).Nodes.Add("GS_power_tx")
        TreeView1.Nodes(14).Nodes.Add("GS_line_loss_tx")
        TreeView1.Nodes(14).Nodes.Add("GS_loss_connector_tx")
        TreeView1.Nodes(14).Nodes.Add("GS_ant_gain_tx")
        TreeView1.Nodes(14).Nodes.Add("GS_loss_point_rx")

        TreeView1.Nodes.Add("SOLAR_CELLS")
        TreeView1.Nodes(15).Nodes.Add("Cell_type")
        TreeView1.Nodes(15).Nodes.Add("Isc")

        TreeView1.Nodes.Add("SOLAR_PANELS")
        TreeView1.Nodes(16).Nodes.Add("Name")
        TreeView1.Nodes(16).Nodes.Add("Mounting_face")

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim reponse As String = ""

        Dim File_name As String = My.Application.Info.DirectoryPath & "\input.yaml"

        Dim YMLReader = System.IO.File.OpenText(File_name)

        Dim file_input = New IO.StreamReader(File_name)

        Dim YamlStream = New YamlStream()
        YamlStream.Load(file_input)

        Dim mapping As YamlMappingNode = YamlStream.Documents(0).RootNode

        For Each entry In mapping
            reponse &= entry.Value.ToString & System.Environment.NewLine
        Next

        MessageBox.Show(reponse)
    End Sub

End Class

