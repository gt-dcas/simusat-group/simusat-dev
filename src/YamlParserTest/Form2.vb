﻿Imports YamlDotNet.Serialization

Public Class Form2
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim Data = TextBox1.Text
        Dim Tree = Form1.TreeView1

        Dim File_name As String = My.Application.Info.DirectoryPath & "\input.yaml"
        Dim YMLReader = System.IO.File.OpenText(File_name)

        Dim deserializerBuilder = New DeserializerBuilder()
        Dim deserializer = deserializerBuilder.Build()
        Dim itemTypes = deserializer.Deserialize(Of Input)(YMLReader)

        YMLReader.Close()


        Dim YMLWriter = System.IO.File.CreateText(File_name)
        Dim SerializerBuilder = New SerializerBuilder()
        Dim serializer = SerializerBuilder.Build()

        Write_data_node(itemTypes, Tree.SelectedNode, Data)

        serializer.Serialize(YMLWriter, itemTypes)
        YMLWriter.Close()

        Me.Close()

    End Sub

    Private Sub Write_data_node(ByVal deserialized As Input, ByVal Node As TreeNode, ByVal data As String)

        Select Case Node.Parent.Text
            Case "paths"
                Select Case Node.Text
                    Case "scilab_path"
                        deserialized.paths.scilab_path = data
                    Case "GMAT_BIN"
                        deserialized.paths.GMAT_BIN = data
                    Case "GMAT_BIN_linux"
                        deserialized.paths.GMAT_BIN_linux = data
                    Case "GMAT_BIN_linux_docker"
                        deserialized.paths.GMAT_BIN_linux_docker = data
                End Select
            Case "SIMULATION"
                Select Case Node.Text
                    Case "report_name"
                        deserialized.SIMULATION.report_name = data
                    Case "total_time"
                        deserialized.SIMULATION.total_time = data
                    Case "dt"
                        deserialized.SIMULATION.dt = data
                End Select
            Case "ORBIT"
                Select Case Node.Text
                    Case "epoch"
                        deserialized.ORBIT.epoch = data
                    Case "SMA"
                        deserialized.ORBIT.SMA = data
                    Case "ECC"
                        deserialized.ORBIT.ECC = data
                    Case "INC"
                        deserialized.ORBIT.INC = data
                    Case "RAAN"
                        deserialized.ORBIT.RAAN = data
                    Case "AOP"
                        deserialized.ORBIT.AOP = data
                    Case "TA"
                        deserialized.ORBIT.TA = data
                End Select
            Case "POWER_BUDGET"
                Select Case Node.Text
                    Case "TotBatPower"
                        deserialized.POWER_BUDGET.TotBatPower = data
                    Case "SolarPower"
                        deserialized.POWER_BUDGET.SolarPower = data
                    Case "Pmesure"
                        deserialized.POWER_BUDGET.Pmesure = data
                    Case "Pvidage"
                        deserialized.POWER_BUDGET.Pvidage = data
                End Select
            Case "DATA_BUDGET"
                Select Case Node.Text
                    Case "TC_debit_nominal"
                        deserialized.DATA_BUDGET.TC_debit_nominal = data
                    Case "TM_debit_nominal"
                        deserialized.DATA_BUDGET.TM_debit_nominal = data
                End Select
            Case "CONSTANTS"
                Select Case Node.Text
                    Case "radius_earth"
                        deserialized.CONSTANTS.radius_earth = data
                    Case "c"
                        deserialized.CONSTANTS.c = data
                    Case "k_dB"
                        deserialized.CONSTANTS.k_dB = data
                End Select
            Case "SPACECRAFT_FLIGHT_DYNAMICS"
                Select Case Node.Text
                    Case "SC_altitude"
                        deserialized.SPACECRAFT_FLIGHT_DYNAMICS.SC_altitude = data
                End Select
            Case "GROUNDSTATION_LOCATION"
                Select Case Node.Text
                    Case "GS_altitude"
                        deserialized.GROUNDSTATION_LOCATION.GS_altitude = data
                    Case "GS_minElevation"
                        deserialized.GROUNDSTATION_LOCATION.GS_minElevation = data
                    Case "GS_latitude"
                        deserialized.GROUNDSTATION_LOCATION.GS_latitude = data
                    Case "GS_longitude"
                        deserialized.GROUNDSTATION_LOCATION.GS_longitude = data
                End Select
            Case "DOWNLINK"
                Select Case Node.Text
                    Case "frequency"
                        deserialized.DOWNLINK.frequency = data
                    Case "data_rate"
                        deserialized.DOWNLINK.data_rate = data
                    Case "required_BER_from_modulation"
                        deserialized.DOWNLINK.required_BER_from_Modulation = data
                    Case "modulation"
                        deserialized.DOWNLINK.modulation = data
                    Case "system_margin"
                        deserialized.DOWNLINK.system_margin = data
                End Select
            Case "UPLINK"
                Select Case Node.Text
                    Case "frequency"
                        deserialized.UPLINK.frequency = data
                    Case "data_rate"
                        deserialized.UPLINK.data_rate = data
                    Case "required_BER_from_modulation"
                        deserialized.UPLINK.required_BER_from_Modulation = data
                    Case "modulation"
                        deserialized.UPLINK.modulation = data
                    Case "system_margin"
                        deserialized.UPLINK.system_margin = data
                End Select
            Case "PROPAGATION_LOSSES"
                Select Case Node.Text
                    Case "loss_pol"
                        deserialized.PROPAGATION_LOSSES.loss_pol = data
                    Case "loss_atm"
                        deserialized.PROPAGATION_LOSSES.loss_atm = data
                    Case "loss_scin"
                        deserialized.PROPAGATION_LOSSES.loss_scin = data
                    Case "loss_rain"
                        deserialized.PROPAGATION_LOSSES.loss_rain = data
                    Case "loss_cloud"
                        deserialized.PROPAGATION_LOSSES.loss_cloud = data
                    Case "loss_si"
                        deserialized.PROPAGATION_LOSSES.loss_si = data
                    Case "loss_misc"
                        deserialized.PROPAGATION_LOSSES.loss_misc = data
                End Select
            Case "SPACECRAFT_TRANSMITTER"
                Select Case Node.Text
                    Case "SC_power_tx"
                        deserialized.SPACECRAFT_TRANSMITTER.SC_power_tx = data
                    Case "SC_ant_gain_tx"
                        deserialized.SPACECRAFT_TRANSMITTER.SC_ant_gain_tx = data
                    Case "SC_loss_cable_tx"
                        deserialized.SPACECRAFT_TRANSMITTER.SC_loss_cable_tx = data
                    Case "SC_loss_connector_tx"
                        deserialized.SPACECRAFT_TRANSMITTER.SC_loss_connector_tx = data
                    Case "SC_loss_feeder_tx"
                        deserialized.SPACECRAFT_TRANSMITTER.SC_loss_feeder_tx = data
                    Case "SC_misc"
                        deserialized.SPACECRAFT_TRANSMITTER.SC_misc = data
                    Case "SC_loss_point_tx"
                        deserialized.SPACECRAFT_TRANSMITTER.SC_loss_point_tx = data
                End Select
            Case "SPACECRAFT_RECEIVER"
                Select Case Node.Text
                    Case "SC_ant_gain_rx"
                        deserialized.SPACECRAFT_RECEIVER.SC_ant_gain_rx = data
                    Case "SC_eta_rx"
                        deserialized.SPACECRAFT_RECEIVER.SC_eta_rx = data
                    Case "SC_loss_point_rx"
                        deserialized.SPACECRAFT_RECEIVER.SC_loss_point_rx = data
                    Case "SC_loss_cable_rx"
                        deserialized.SPACECRAFT_RECEIVER.SC_loss_cable_rx = data
                    Case "SC_loss_connector_rx"
                        deserialized.SPACECRAFT_RECEIVER.SC_loss_connector_rx = data
                    Case "SC_LNA_gain"
                        deserialized.SPACECRAFT_RECEIVER.SC_LNA_gain = data
                    Case "SC_T_rx"
                        deserialized.SPACECRAFT_RECEIVER.SC_T_rx = data
                End Select
            Case "GROUND_STATION_RECEIVER"
                Select Case Node.Text
                    Case "eta_rx"
                        deserialized.GROUND_STATION_RECEIVER.eta_rx = data
                    Case "antennaDiameter"
                        deserialized.GROUND_STATION_RECEIVER.antennaDiameter = data
                    Case "GS_LNA_gain"
                        deserialized.GROUND_STATION_RECEIVER.GS_LNA_gain = data
                    Case "GS_T_rx"
                        deserialized.GROUND_STATION_RECEIVER.GS_T_rx = data
                    Case "GS_loss_point_rx"
                        deserialized.GROUND_STATION_RECEIVER.GS_loss_point_rx = data
                    Case "GS_loss_cable_rx"
                        deserialized.GROUND_STATION_RECEIVER.GS_loss_cable_rx = data
                    Case "GS_loss_cable_D_rx"
                        deserialized.GROUND_STATION_RECEIVER.GS_loss_cable_D_rx = data
                    Case "GS_loss_connector_rx"
                        deserialized.GROUND_STATION_RECEIVER.GS_loss_connector_rx = data
                End Select
            Case "GROUND_STATION_TRANSMITTER"
                Select Case Node.Text
                    Case "GS_power_tx"
                        deserialized.GROUND_STATION_TRANSMITTER.GS_power_tx = data
                    Case "GS_line_loss_tx"
                        deserialized.GROUND_STATION_TRANSMITTER.GS_line_loss_tx = data
                    Case "GS_loss_connector_tx"
                        deserialized.GROUND_STATION_TRANSMITTER.GS_loss_connector_tx = data
                    Case "GS_ant_gain_tx"
                        deserialized.GROUND_STATION_TRANSMITTER.GS_ant_gain_tx = data
                    Case "GS_loss_point_rx"
                        deserialized.GROUND_STATION_TRANSMITTER.GS_loss_point_rx = data
                End Select
        End Select


    End Sub
End Class