﻿Option Strict Off
Option Explicit On
Module Module1


    Class Input
        Public Property paths As T_paths
        Public Property SIMULATION As T_Simulation
        Public Property ORBIT As T_Orbit
        Public Property POWER_BUDGET As T_Power_budget
        Public Property DATA_BUDGET As T_Data_budget
        Public Property CONSTANTS As T_Constants
        Public Property SPACECRAFT_FLIGHT_DYNAMICS As T_Space_craft_flight_dynamics
        Public Property GROUNDSTATION_LOCATION As T_groundstation_location
        Public Property DOWNLINK As T_Downlink
        Public Property UPLINK As T_Uplink
        Public Property PROPAGATION_LOSSES As T_Propagation_losses
        Public Property SPACECRAFT_TRANSMITTER As T_spacecraft_transmitter
        Public Property SPACECRAFT_RECEIVER As T_spacecraft_receiver
        Public Property GROUND_STATION_RECEIVER As T_ground_station_receiver
        Public Property GROUND_STATION_TRANSMITTER As T_ground_station_transmitter
        Public Property CENTRAL_BODY_GEOMETRY As T_central_body_geometry
        Public Property SOLAR_PANELS As T_solar_panels
        Public Property SIMULATION_DATA As T_simulation_data
        Public Property SOLAR_CELLS As T_solar_cells
        Public Property ENERGY_MANAGMENT As T_energy_managment
        Public Property BATTERY As T_battery
        Public Property THERMICAL_DATA As T_thermical_data
    End Class

    Class T_paths
        Public Property scilab_path As String
        Public Property GMAT_BIN As String
        Public Property GMAT_BIN_linux As String
        Public Property GMAT_BIN_linux_docker As String
        Public Property input_dir As String
        Public Property output_dir As String

    End Class

    Class T_Simulation
        Public Property report_name As String
        Public Property total_time As Integer
        Public Property dt As Integer
    End Class

    Class T_Orbit
        Public Property epoch As String
        Public Property SMA As Double
        Public Property ECC As Double
        Public Property INC As Double
        Public Property RAAN As Double
        Public Property AOP As Double
        Public Property TA As Double
    End Class

    Class T_Power_budget
        Public Property TotBatPower As Integer
        Public Property SolarPower As Integer
        Public Property Pmesure As Integer
        Public Property Pvidage As Integer
    End Class

    Class T_Data_budget
        Public Property TC_debit_nominal As Integer
        Public Property TM_debit_nominal As Integer
    End Class

    Class T_Constants
        Public Property radius_earth As Double
        Public Property c As Integer
        Public Property k_dB As Double
    End Class
    Class T_Space_craft_flight_dynamics
        Public Property SC_altitude As Integer
    End Class
    Class T_groundstation_location
        Public Property GS_altitude As Integer
        Public Property GS_minElevation As Integer
        Public Property GS_latitude As Double
        Public Property GS_longitude As Double
    End Class
    Class T_Downlink
        Public Property frequency As Double
        Public Property data_rate As String
        Public Property required_BER_from_Modulation As Double
        Public Property modulation As String
        Public Property system_margin As Integer
    End Class
    Class T_Uplink
        Public Property frequency As Double
        Public Property data_rate As String
        Public Property required_BER_from_Modulation As Double
        Public Property modulation As String
        Public Property system_margin As Integer
    End Class

    Class T_Propagation_losses
        Public Property loss_pol As Integer
        Public Property loss_atm As Double
        Public Property loss_scin As Double
        Public Property loss_rain As Double
        Public Property loss_cloud As Double
        Public Property loss_si As Double
        Public Property loss_misc As Double
    End Class

    Class T_spacecraft_transmitter
        Public Property SC_power_tx As Integer
        Public Property SC_ant_gain_tx As Double
        Public Property SC_loss_cable_tx As Double
        Public Property SC_loss_connector_tx As Double
        Public Property SC_loss_feeder_tx As Double
        Public Property SC_misc As Integer
        Public Property SC_loss_point_tx As Integer
    End Class

    Class T_spacecraft_receiver
        Public Property SC_ant_gain_rx As Double
        Public Property SC_eta_rx As Double
        Public Property SC_loss_point_rx As Integer
        Public Property SC_loss_cable_rx As Double
        Public Property SC_loss_connector_rx As Double
        Public Property SC_LNA_gain As Integer
        Public Property SC_T_rx As Integer
    End Class

    Class T_ground_station_receiver
        Public Property eta_rx As Double
        Public Property antennaDiameter As Integer
        Public Property GS_LNA_gain As Integer
        Public Property GS_T_rx As Integer
        Public Property GS_loss_point_rx As Integer
        Public Property GS_loss_cable_rx As Integer
        Public Property GS_loss_cable_D_rx As Double
        Public Property GS_loss_connector_rx As Double
    End Class

    Class T_ground_station_transmitter
        Public Property GS_power_tx As Integer
        Public Property GS_line_loss_tx As Double
        Public Property GS_loss_connector_tx As Double
        Public Property GS_ant_gain_tx As Double
        Public Property GS_loss_point_rx As Integer
    End Class

    Class T_central_body_geometry
        Public Property Body_mass As Double
        Public Property Body_height As Single
        Public Property Body_width As Single
    End Class

    Class T_solar_panels
        Public Property Panels_number As Short
        Public Property Type As String
        Public Property Name As String
        Public Property Mounting_face As String
        Public Property Orientation As String
        Public Property Height_position As String
        Public Property Length As Single
        Public Property Width As Single
        Public Property Mass As Single
        Public Property Strings_number As Integer
        Public Property Cells_face_absorbtivity As Single
        Public Property Cells_face_emissivity As Single
        Public Property Back_face_absorbtivity As Single
        Public Property Back_face_emissivity As Single
        Public Property Specific_heat As Single
        Public Property Panel_conduction_with_central_body As Single
    End Class

    Class T_simulation_data
        Public Property Simulation_start_date As Double
        Public Property Simulation_time_step As Double
    End Class

    Class T_solar_cells
        Public Property Cell_type As Integer
        Public Property Isc As Single
        Public Property Imp As Single
        Public Property Voc As Single
        Public Property Vmp As Single
        Public Property Cell_disc As Single
        Public Property Cell_dimp As Single
        Public Property Cell_dvoc As Single
        Public Property Cell_dvmp As Single
        Public Property Cell_radisc As Single
        Public Property Cell_radimp As Single
        Public Property Cell_radvoc As Single
        Public Property Cell_radvmp As Single
        Public Property Number_per_string As Single
        Public Property Cell_area As Single
        Public Property Cell_efficiency As Single
        Public Property Reference_temperature As Single
        Public Property Divergence_incidence As Single
        Public Property Max_incidence As Single
    End Class

    Class T_energy_managment
        Public Property Initial_battery_charge As Single
        Public Property Max_charge_voltage As Single
        Public Property Holding_current As Single
        Public Property Max_charge_current As Single
        Public Property Recharge_coefficient As Single
        Public Property Battery_payload_efficiency As Single
        Public Property Panels_payload_efficiency As Single
        Public Property Regulated_voltage As Single
        Public Property Day_requested_power As Single
        Public Property Day_dissipated_power As Single
        Public Property Night_requested_power As Single
        Public Property Night_dissipated_power As Single
        Public Property Regulated_1_or_no_regulated_0_voltage As Integer
    End Class

    Class T_battery
        Public Property Battery_type As Byte
        Public Property Cells_number_serial As Byte
        Public Property Cells_number_parallel As Byte
        Public Property Nominal_capacity_per_cell As Single
    End Class

    Class T_thermical_data
        Public Property Panels_initial_temperature As Single
        Public Property Battery_temperature As Single
        Public Property Absorptivity_of_body_faces_plusX As Single
        Public Property Absorptivity_of_body_faces_lessX As Single
        Public Property Absorptivity_of_body_faces_plusY As Single
        Public Property Absorptivity_of_body_faces_lessY As Single
        Public Property Absorptivity_of_body_faces_plusZ As Single
        Public Property Absorptivity_of_body_faces_lessZ As Single
        Public Property Emissivity_of_body_faces_plusX As Single
        Public Property Emissivity_of_body_faces_lessX As Single
        Public Property Emissivity_of_body_faces_plusY As Single
        Public Property Emissivity_of_body_faces_lessY As Single
        Public Property Emissivity_of_body_faces_plusZ As Single
        Public Property Emissivity_of_body_faces_lessZ As Single
        Public Property Body_specific_heat As Single
        Public Property Body_initial_temperature As Single
    End Class
End Module

