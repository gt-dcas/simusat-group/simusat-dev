# Log

##### 04/06/2019

* Comparation des branches master et Michele_Stage2018_secondBranch grâce a Meld
* La branche de Michelle est la plus récent. M. Gateau fait un merge avec master.
* Création de la branche Angel_Stage2019, où toutes les développement seront faites.

##### 05/06/2019

* Toutes les libraries ont été téléchargées. Soit le README.txt soit le manual des installation des bibliothèques doit être mis à jour pour incluire l'installation de Newtonsoft.json, qui peut être trouvé dans le dossier dependencies
* Mis à jour de Visual Studio à la version 5 pour pouvoir utiliser json
* J'ai "joué" un peu simusat. **Bug trouvé** en essayant de créer un nouveau fichier. Le erreur se trouve dans Utilities.vb dans la fonction read_line(). **Solution non trouvé**.
* Réunion avec Annafederica. Notes dans le cahier.
* Test unitaires existant ont été testés. Toutes marchent bien. 
* Faite la traduction de Norme à norm comme était noté dans le code. Commit et push fait.

##### 06/06/2019

* Cours de thérmique d'Annafederica a été lu. Il y a des valeurs qui peuvent être interessants et des problèmes (p64) qui peuvent servi comme des test unitaires
* Raport de Mireia: p17-> table albedo vs. inclination. Intéresant pour l'implementer au code.
* **Bug fixed** New file marche bien maintenant. Le fichier en blanc n'était pas bien placé.

##### 07/06/2019

* Recherche d'information pour desssiner le première test unitaire: 1 noeud dans un orbit circulaire autour la Terre.
* Etude du fonctionnement du code en notant les fichiers que seront modifiés

##### 11/06/2019

* Création de conf_factor_earth fonction dans power.vb et son UnitTest déjà validé.
* Rénommé solar_flux comme solar_intensity 

##### 12/06/2019

* Code pour lire fichier csv avec le facteur de vue pour la Terre.
* Nominal unit test pour le code anterieur

##### 13/06/2019

* __Bug Fixed__ Maintenant le affichage du flux solaire pendant l'année dans l'onglet Environment marche bien et il est reescalé à chaque fois. J'ai changé la definition de ChartArea1 qui était fait dans un Class Privée pour Public WithEvents juste avant.

##### 14/06/2019

* Bug fixée hier vient d'être refixé. VS n'aimait pas la solution de hier et donc, ce qu'a été fait est de definir la valeur maximale du axis Y comme automatique à l'aide du designer.
* Tout la partie thermique qui été sur power.vb a été enlevée et puis collée dans un nouveau module: thermal.vb
* Etude de comme fonctione la partie thermique et réflexion de comme l'ammeillorer. 

##### 17/06/2019

* Realisation de module thermique tout simplifié pour ne prendre que la evolution de la température d'un point au cours d'un orbite.
* Essai de régler le calendier de frmGraphPwr.vb sans réussir

##### 18/06/2019

* __Error fixed__ dans l'évaluation de l'existance d'éclipse ou non dans thermalsimpl.vb _to be transported to thermal_
* Changement Body spec Heat valeur par defect dans frmMain.vb pour éviter des NaN sans ténir que faire une modification là.
* Ajout de code à frmReportPwr.vb pour créer un fichier (datos.txt dans le dossier bin) avec différents donnés thermiques pour son analysis et excel file crée dans BiblioAngel pour la lecture.
* Microbug identifié et fixé dans ReportPwr.
* Bug trouvé dans l'écriture de xls fichier dans le powerreport. Il ne marche pas. Solution non trouvé.

##### 19/06/2019

* Analysis de résultats.
* Ecriture de csv avec des modèles plus précises sur l'albédo et la temperature de la Terre. (bin/resources)
* Modifié PwrReport pour afficher aussi long y lat du satellite (utile pour modèle ESA)
* Module ESAmodel créé pour implementé les valeurs de l'ESA.
* thermalsimpl.vb modifié pour utiliser le modèle ESA. Tests à faire

##### 20/06/2019

* Erreurs fixés dans ESAmodel.vb
* Units test créés pour l'interpolation des donnés de CERES (ESAmodel)
* Creation de class pour calculer la partie thérmique pour une face du satellite. On va tester que tous les faces marchent bien.
* Modifié PwrReport pour afficher plus de valeurs intéresants pour l'analysis du comportement termique de chaque face.
* Truc bizarre dans Phi_Alb pour la face -Z. Deux points avec aucun sense. Erreur non trouvé.

##### 21/06/2019

* Erreur corrigé dans Phi_Alb. L'erreur s'agissait d'un erreur de pointeur dans l'interpolation des facteurs de vue. le problème vient d'être corrigé dans touts les cas.
* Toutes les faces comprobés. Apparement, il y a des problèmes avec des angles.

##### 24/06/2019

* A priori, erreur résoudre dans l'orientation des faces par rapport au Terre. Position de la Terre "changé" dans thermal1Face.vb
* Création de module thermique pour le cube complet (thermal6Faces). l'intéraction entre eux est trop simple: addition de tous les radiations reçus/émis. 

##### 25/06/2019

* Correction de face surface calculation (il y avait un érreur de défnition). Ainsi, l'usage des absorptivités et émissivités donnés par l'utilisateur est fonctionelle.
* __Bug fixed__ Maintenant, le sauvegarde des rapport en fichiers excel marchent bien. Celle de l'attitude marché déjà et ce qu'a  été fait est de comparer les deux. Les lignes qui donnent des problèmes étaient celles du format de chaque cellule. Dans le cas du rapport d'attitude, ces lignes étaient commentés.
* Aditionallement, le sauvgarde a été ameilloré et il permet de le faire en format .xls et .xlsx
* Révision des panneaux solairesgit 

##### 26/06/2019

* Test unitaire créé pour l'orientation de chaque face par rapport au Terre
* Correction de l'orientation des panneaux solaires par rapport au Terre. Les type 1 n'etaient pas bien orientés au Soleil et la position du Terre n'était pas bien calculée
* Test unitaire créé pour l'orientation d'un panneau type 2 par rapport au Terre et au Soleil.
* Test unitaire pour le type 1 ne marche pas bien. Il faut réviser sa définition.

##### 27/06/2019

*  Définition du surface de chaque panneau (oublié à calculer).
* Tous les panneaux semblent avoir des donnés correctes et donc, la partie termique marche bien. 
* Il faut créer des test.

##### 01/07/2019

* Réunion avec Thibault pour voir l'état du projet
* Conception du test plus dur pour la position de chaque face par rapport au Terre. Il est décidé de prendre les axes inertiels comme axes pricipaux pour faire vraiment varier les angles.

##### 02/07/2019

* Test pour tester l'orientation de chaque face par rapport au Terre codé. L'orientation marche bien.
* Tous les tests ont été redéfinis pour tester des différents situations. Tous ont été bien passés. 
* Verifier le bug de sélection de date dans l'onglet de Power Curves. Rien solution trouvée.

##### 03/07/2019

* Changement d'appel aux fichiers csv utilisés pour la partie thermique. Maintenant, leur path est dans un fichier json qui est appellé dans utilitaires.vb
* Validation des test faites avec Thibault. Erreurs rancontres notés pour être corrigés.
* Investigation d'où vient le problème.

##### 04/07/2019

* Correction de usage de fonction Arcsin (il était utilisée une fonction fait à la main) et addition de test unitaire pour fonction faite à la main de arctan
* Addition de orekit à la biblio (pour l'instant n'est pas utilié, mais peut être intérésant pour comparer le propagateur)
* Changement du test unitaire pour l'orientation face-Terre. Maintenant il y a un vrai bon résultat.
* En train de designer un test pour la temperature moyenne d'un satellite.

##### 05/07/2019

* Conception de test pour tester le calcul de temperature par rapport à un problème du poly  donné par Annafederica. Test passé.
* Conception de test pour Solar intensity. Test passé.
* Conception de test pour Albedo. Test passé.
* Introduction de la fonction flux_rad dans module thermal. Bien testé. Ceci permet d'avoir un code plus prope et d'avoir beacoup de choses testés.
* Essayer de récrire la fonction d'Eclipse. Non réusi.

##### 08/07/2019

* Rémodelation de la fonction eclipse (basOrbitography) pour utiliser la structure de vecteur et les fonction des operations vectorielles définis dans la class basMath.
* Correction en thermalWObodyconduction.vb dans le calcul de rayonnement de la Terre. La définition de la fonction flux_rad était fait pour Celsius degrees mais la temperature de la Terre est en Kelvin. Ce qui a été fait est de changer l'appel fait à la fonction flux_rad dans le cas de la temperature de la Terre.thermal
* Création de Eclipse_Test. Test passé.
* vector to Earth: target = (0,0,0). La Terre c'est le répère.
* Unit tests complétés pour l'orientation des panneaux solaires par rapport au Terre/Soleil.
* Ajout de message dans frmGraphPwr.vb pour montrer un message en dissant qu'il y a un NaN (si c'est le cas) pour prevenir l'utilisateur.

##### 09/07/2019

* Ajout de background gris pour les éclipses. Pour l'instant il y a une ligne gris qu'est adapté au timestep.
* Investigation de http client. Different sites webs dans l'email.

##### 10/07/2019

* Réunion avec Annafederica
* Travail sur le test de temperature pour une orbite complète. Sans conclusions pour l'instant.

##### 17/07/2019

* Validation du test de température moyenne en faisant la comparation entre ce qui donne Simusat et ce qui donne les calculs faits avec Excel.
* Addition des lignes grises dans les eclipses pour les Power Graph.
* Ajout des informations de la source des donnés qui apparaîssent dans le test de l'inclination des faces du satellite par rapport au Terre.



##### 18/07/2019

* Ajout de fonctionalité Hover Mouse pour le buttons ISO Model et Sine function dans le Tab Enviroment de Orbitography.
* Création de check button pour exprimer quel modèle d'albedo et de temperature de la Terre on veut utiliser (provisionel)

##### 22/07/2019

* Création de fonction pour calculer la valeur moyenne des donnés CERES pour chaque mois pour l'affichage dans l'onglet enviroment
* Création du test pour vérifier que la fonction marche bien.
* Actualisation de l'onglet enviroment (il faut créer les class et methods pour qu'elle soit totelement fonctionelle au niveau simulation. Au niveau affichage marche bien). Il manque que la sélection fait dans l'onglet soit réel.
* Petit avancement du rapport

##### 23/07/2019

* Correction de fonctionalité dans bouton de l'onglet environment
* Ajout d'option de sauvegarder Power Report en format .csv
* Ajout de structure _if_ dans thermal module pour vérifier la fonctionnalité de CERES boolean pour l'albédo.
* Trouvé que le nombre de strings des panneaux n'est pas défini dans aucun lieu. Il faut décider où le mettre.

##### 24/07/2019

* __Bug fixed__ Dans l'onlget orbit, même si l'heure d'epoch ou un satellite NORAD est sélectioné, la date de début de simulation (et current date) n'est pas actualisé dans la fenêtre Main.
* Changement de définition du surface des panneaux du _module thermal_ au point de définition des panneaux dans _frmAttCong_ 
* __Bug fixed__ Le chalendrier marche bien dans la fenêtre des Power Graph. Le problème était le Sub procedure utilisé.
* __New Bug__ Quand l'heure de l'époque es 23 il y a un érreur au niveau des fenêtres de Graph et Report parce que pour la date de fin ce qu'est fait est adder (en dur) une heure. (__FIXED__) En lieu d'ajouter en dur, on ajout 1/24 jour juliens à la date à convertir pour l'heure de fin.

##### 25/07/2019

* __Bug fixed__ maintenant, chaque fois que l'époque de l'orbite est changé, les dates de début et fin des toutes les fenêtres de _Report_ et _Graph_ changent aussi.
* Addition de structure IF pour décider utiliser les données de CERES où la valeur mensuelle dans le cas du flux infrarrouge.
* Avance de rapport

##### 29/07/2019

* Test fait pour vérifier que la partie thermique du logiciel marche bien. Le test compare ce qui calculer le logiciel avec un fichier csv avec le resultat du logiciel pour una fraction d'orbite.
* __BUG fixed__ Si on oublie de choisir le type d'orbite et on ne fait que définir les paramètres de l'orbit. Lorsque on fait click sur le bouton de simulation de _Main_, l'orbite de change pas à une orbite circulaire.
* Changement de la fenêtre de configuration de satellites pour définir le nombre de chaînes de cellules solaires.

##### 30/07/2019

* __BUG fixed__ Dans la fenêtre des graphiques thermiques et énergétiques, lorsque on ouvre la fenêtre et le satellite n'a pas de panneaux solaires, la section de paramètres des panneaux n'est pas désactivée par défaut. Le comportement désiré est que la section des panneaux est désactivée par défaut même si il y a des panneaux ou non. Ainsi, si il n'y a pas des panneaux, il doit être impossible d'activer cette section.

##### 31/07/2019

* Continuation avec le travail des bugs dans la liste de propriétés à afficher quand on enleve un panneau.
* Réunion avec Thibault.

##### 01/08/2019

* __Bug fixed__ la liste des propriètés à afficher s'actualise automatiquement lors qu'on enleve un panneau.

##### 05/08/2019

* Warning message in AttConf bug fixed. Il n' est affiché maintenant quand on fait un plot des thermal/energy propriétés.
* Changement de valeur par defaut pour le timestep changé a 60s
* Addition de ligne dans att_conf_load at frmAttConf.vb pour disable Management 
* Enlever message "You successfully created a new file"
* Change l'extension de fichier à .sc mais il permettre d'ouvrir les fichiers .att (ancien version)
* Bouger les boutons à l'interieur de la section Satellite Design
* Addition des données d'environnement dans les fichiers .orb
* Elimination des données d'environnement dans le fichier de satellite avec retrocompatibilité.
* Création des boutons open / save pour Mission Design
* Merged avec master.
* Version simusat 6.0

##### 06/08/2019

* Handled exceptions dans le bouton open mission de la fenêtre main et changement d' appel de ces fonctions.
* Orbit nom et le nom du fichier sont le même maintenant.
* Addition des unités et titres aux graphs d'environnement. 
* Addition de fonctionalité hover pour connaître la valeur des series à chaque point.
* <https://smallsats.org/2013/04/11/sun-synchronous-circular-orbit/

##### 22/08/2019

* Fait tutorial pour Simusat
* Changement des valeurs par défaut dans la partie thermique
* Changement de titre à Simusat 2019
* Maintenant si on enlève un panneau, les valeurs se remettent à 0.
* Handled exception à cause de retrocompatibilité dans la fonction Load File

##### 23/08/2019

* Bug résolu: superposition des panneaux à l'heure de les modifier.
* Creation de version 6.2 (version standalone inclu) et merge avec master

##### 28/08/2019

* Présentation PowerPoint fait pour présenter le travail fait à Stéphanie.
* Travaillé sur le PowerDiagram. Il faut faire que la partie Power est actualisé dans le Simulation Loop.

##### 29/08/2019

* Ajout des unités à l'etiquette du coefficient de conductivité.
* Réunion avec Stéphanie. Partie thermique validé. Il faut voir les torques.

##### 30/08/2019

* actualisation de blank_file.sc pour avoir autre date de début.
* Investigation des problemes avec les torques. Aucun résultat concluyant.

##### 02/09/2019

* Code pour la modification de l'attitude  modifié pour une bonne foncitonnement.
* Changement de lieu de blank_dile.sc, New changé à  Reset. Par défaut le nom de fichiers est celui du satellite.
* Changé la definition des distances au CG. 

##### 03/09/2019

* Ombre pour les époqiues des eclipses ajouté aux graphiques de torque.
* Maintenant il faut donner un nom au satellite quand on reset les données.
* Investigation du couple aero. Référence trouvé. Les équations semblent bien.