package executables;

import org.orekit.bodies.CelestialBodyFactory;
import org.orekit.bodies.CelestialBody;
import org.orekit.bodies.OneAxisEllipsoid;
import org.orekit.data.DataProvidersManager;
import org.orekit.data.DirectoryCrawler;
import org.orekit.errors.OrekitException;
import org.orekit.forces.drag.*;
import org.orekit.forces.radiation.IsotropicRadiationClassicalConvention;
import org.orekit.forces.radiation.SolarRadiationPressure;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.orbits.KeplerianOrbit;
import org.orekit.orbits.Orbit;
import org.orekit.orbits.PositionAngle;
import org.orekit.propagation.SpacecraftState;
import org.orekit.propagation.analytical.KeplerianPropagator;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScale;
import org.orekit.time.TimeScalesFactory;
import org.orekit.utils.Constants;
import org.orekit.utils.PVCoordinatesProvider;
import org.hipparchus.geometry.euclidean.threed.Rotation;
import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.orekit.forces.BoxAndSolarArraySpacecraft;
import org.orekit.forces.BoxAndSolarArraySpacecraft.Facet;
import org.orekit.forces.gravity.HolmesFeatherstoneAttractionModel;
import org.orekit.forces.gravity.potential.GravityFieldFactory;
import org.orekit.forces.gravity.potential.NormalizedSphericalHarmonicsProvider;
import org.orekit.models.earth.GeoMagneticField;
import org.orekit.models.earth.GeoMagneticFieldFactory;
import org.orekit.models.earth.GeoMagneticElements;
import org.orekit.forces.drag.DTM2000;

import java.io.File;

/**
 * @author j.hernanz & m.altrach
 * @date  May 2017
 */

public class Main {
	
	static double RE = Constants.WGS84_EARTH_EQUATORIAL_RADIUS; // Earth radius in meters
	
	public static double inclinaison_sunsync(double alt) {
		double cosi, cste;
		cste = 0.000000000000004773624117289;
		cosi = -(Math.pow(alt / 1000, 3.5)) * cste; // in this formula, based on version VB.Net, "alt" must be in kms
		return Math.acos(cosi); // in radians
	}

    public static void main(String[] args) throws OrekitException {
    	
    	//----------------------------
        //------- INITIALIZING -------
        //----------------------------
    	
        // We charge all the Orekit data
        File orekitData = new File("data/orekit-data");
        DataProvidersManager manager = DataProvidersManager.getInstance();
        manager.addProvider(new DirectoryCrawler(orekitData));

        // We create the CoordinatesFrames and the time scale
        Frame inertialFrame = FramesFactory.getEME2000(); // an inertia coordinate frame centered on Earth
        TimeScale utc = TimeScalesFactory.getUTC();
        AbsoluteDate initialDate = new AbsoluteDate(2010, 03, 21, 00, 00, 00.000, utc); // 21 March 2001

        //----------------------------
        //---------- ORBIT -----------
        //----------------------------
        
        // Orbit parameters : Sun synchronous at 800km
        double mu =  Constants.WGS84_EARTH_MU;
        double a = 800000 + RE; 			// semi-major axis in METERS !!
        double e = 0; 						// eccentricity
        double i = inclinaison_sunsync(a); 	// inclination (in radians)
        double omega = Math.toRadians(0); 	// perigee argument
        double raan = Math.toRadians(0); 	// right ascension of ascending node
        double lM = 0; 						// mean anomaly

        // We create a Keplerian orbit
        Orbit initialOrbit = new KeplerianOrbit(a, e, i, omega, raan, lM, PositionAngle.MEAN, inertialFrame, initialDate, mu);
        
        //----------------------------
        //-------- SATELLITE ---------
        //----------------------------
        
        double xLength = 0.8; //
        double yLength = 0.8; // dimensions of central body
        double zLength = 0.8; //
        Facet panel1 = new Facet(new Vector3D(1, 0, 0), yLength * zLength);
        Facet panel2 = new Facet(new Vector3D(0, 1, 0), xLength * zLength);
        Facet panel3 = new Facet(new Vector3D(-1, 0, 0), yLength * zLength);
        Facet panel4 = new Facet(new Vector3D(0, -1, 0), xLength * zLength);
        Facet[] panels = {panel1, panel2, panel3, panel4}; // list of solar panels
        PVCoordinatesProvider sun = CelestialBodyFactory.getSun();
        //CelestialBody sunBody = CelestialBodyFactory.getSun();
        double solarArrayArea = panel1.getArea() + panel2.getArea() + panel3.getArea() + panel4.getArea(); // total area of solar panels (m�)
        Vector3D solarArrayAxis = new Vector3D(0, 0, 1); // solar panels rotation axis in satellite frame
        double dragCoeff = 0.9; // drag coefficient
        double absCoeff = 0.1; // absorption coefficient
        double refCoeff = 0.7; // reflection coefficient
        Vector3D magneticProperties = new Vector3D(10, 10, 10);
        BoxAndSolarArraySpacecraft spacecraft = new BoxAndSolarArraySpacecraft(panels, sun, solarArrayArea, solarArrayAxis, dragCoeff, absCoeff, refCoeff);
        
        //----------------------------
        //------ SUN RADIATION -------
        //----------------------------

        /* Object physical and geometric information. Here you define the satellite. There are various classes
        to define the satellite: BoxAndSolarArraySpacecraft, IsotropicRadiationClassicalConvention... */
        double crossSection = xLength * yLength;
        IsotropicRadiationClassicalConvention spacecraft_rad = new IsotropicRadiationClassicalConvention(crossSection, absCoeff, refCoeff);
        SolarRadiationPressure srp = new SolarRadiationPressure(sun, RE, spacecraft);
        
        //----------------------------
        //----------- DRAG -----------
        //----------------------------
       
        /* For the isotropic drag we first need to select the atmosphere. There are various models, for example:
        for the HarrisPriester model we need to define the earth. */
        OneAxisEllipsoid earth = new OneAxisEllipsoid(RE, Constants.WGS84_EARTH_FLATTENING, inertialFrame);
        Atmosphere atmosphere = new HarrisPriester(sun, earth, 6); // cosine exponent 6 for polar orbits
        Frame atmFrame = atmosphere.getFrame();
        /* The created space craft is too simple and doesn't have a drag coefficient. We create it aside. There are other
        classes that allow you to create a space craft with dragCoeff and radiation coefficients all together (for
        example BoxAndSolarArraySpacecraft). */
        IsotropicDrag spacecraft_drag = new IsotropicDrag(crossSection, dragCoeff);
        DragForce dF =  new DragForce(atmosphere, spacecraft);

        //----------------------------
        //--------- GRAVITY ----------
        //----------------------------
        
        int degree = 15;
        int order = 15;
        NormalizedSphericalHarmonicsProvider provider = GravityFieldFactory.getNormalizedProvider(degree, order);
        HolmesFeatherstoneAttractionModel gravityField = new HolmesFeatherstoneAttractionModel(inertialFrame, provider);
        
        //----------------------------
        //------ MAGNETIC FIELD ------
        //----------------------------
        
        GeoMagneticField magneticFieldModel = GeoMagneticFieldFactory.getIGRF(2001.0); // using IGRF model
        
        //----------------------------
        //------- PROPAGATION --------
        //----------------------------
        
        // Selection of a Keplerian propagator
        KeplerianPropagator kepler = new KeplerianPropagator(initialOrbit);
        // Propagation
        double duration = 12 * 60 * 60.; // in seconds
        AbsoluteDate finalDate = initialDate.shiftedBy(duration);
        double stepT = 180.;
        int cpt = 1;
        for (AbsoluteDate currentDate = initialDate;
             currentDate.compareTo(finalDate) <= 0;
             currentDate = currentDate.shiftedBy(stepT)) {
            SpacecraftState currentState = kepler.propagate(currentDate);
            Frame satFrame = currentState.getFrame();
            Vector3D position = currentState.getPVCoordinates().getPosition(); // I think it should be in meters (rather than kms)
            Vector3D satVelocity = currentState.getPVCoordinates().getVelocity();
            double satMass = currentState.getMass();
            Rotation satRotation = currentState.getAttitude().getRotation();
                        
            //System.out.print("step " + cpt++ + " // ");
            //System.out.print("time: " + currentState.getDate() + " // ");
            //System.out.print(currentState.getOrbit() + " // ");
            //System.out.println("position: " + position + " // ");
            //System.out.println("velocity: " + satVelocity + " // ");
            
            // For the radiation pressure and the drag I think the derivatives should be use here to see how they evolve along the orbit
            
            //----------------------------
            //----------- DRAG -----------
            //----------------------------
            
            Vector3D atmVelocity = atmosphere.getVelocity(currentDate, position, atmFrame);
            Vector3D relatVelocity = satVelocity.subtract(atmVelocity);
            
            double density = atmosphere.getDensity(currentDate, position, inertialFrame);
           
            //Vector3D dragAcc = spacecraft_drag.dragAcceleration(currentDate, satFrame, position, satRotation, satMass, density, relatVelocity);
            //Vector3D dragAcc = spacecraft.dragAcceleration(currentDate, satFrame, position, satRotation, satMass, density, relatVelocity);
            
            //Vector3D sunPos = sunBody.getPVCoordinates(currentDate, inertialFrame).getPosition();
            //System.out.println(sunPos.getX() + "," + sunPos.getY() + "," + sunPos.getZ());
            
            //System.out.print("density: " + density + " // ");
            //System.out.println("drag acceleration: " + dragAcc);
            //System.out.println(dragAcc.getX() + "," + dragAcc.getY() + "," + dragAcc.getZ());
            
            
            //----------------------------
            //------ SUN RADIATION -------
            //----------------------------
            /*
            String supportedNames = "(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\\p{Digit}\\p{Digit}\\p{Digit}\\p{Digit}F10\\.(?:txt|TXT)";
            MarshallSolarActivityFutureEstimation.StrengthLevel strengthLevel = MarshallSolarActivityFutureEstimation.StrengthLevel.AVERAGE;
            MarshallSolarActivityFutureEstimation activity = new MarshallSolarActivityFutureEstimation(supportedNames, strengthLevel);
            double dbl_sunFlux = activity.getMeanFlux(currentDate);
            Vector3D vct_sunFlux = spacecraft.getNormal(currentDate, satFrame, position, satRotation).scalarMultiply(dbl_sunFlux);
            Vector3D radPressAcc = spacecraft.radiationPressureAcceleration(currentDate, satFrame, position, satRotation, satMass, vct_sunFlux);
            */            
            //System.out.println("solar pressure acceleration: " + radPressAcc);
            //System.out.println(radPressAcc.getX() + "," + radPressAcc.getY() + "," + radPressAcc.getZ());
            
          
            //----------------------------
            //--------- GRAVITY ----------
            //----------------------------
            
            Vector3D gravityAcc = new Vector3D(0, 0, (gravityField.value(currentDate, position)/RE));
            
            //System.out.println("gravity acceleration: " + gravityAcc);
            System.out.println(gravityAcc.getX() + "," + gravityAcc.getY() + "," + gravityAcc.getZ());
            
            
            //----------------------------
            //------ MAGNETIC FIELD ------
            //----------------------------
            
            double latitude = Math.toDegrees(Math.asin(position.getZ()/position.getNorm())); // in degrees
            double longitude = Math.toDegrees(Math.atan(position.getY()/position.getX())); // in degrees
            double altitude = (position.getNorm() - RE) / 1000; // in kms
            GeoMagneticElements magneticField = magneticFieldModel.calculateField(latitude, longitude, altitude);
            Vector3D magneticVector = magneticField.getFieldVector().scalarMultiply(10e-9); // nanoTesla to Tesla
            Vector3D magneticTorque = Vector3D.crossProduct(magneticProperties, magneticVector);
            
            //System.out.println("magnetic field: " + magneticVector);
            //System.out.println(magneticTorque.getX() + "," + magneticTorque.getY() + "," + magneticTorque.getZ());
            
        }

    } // end of main
}
