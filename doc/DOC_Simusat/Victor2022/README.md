# **DCAS : STAGE Simusat**


*Stagiaire:* Victor Cabanay

*Période:* 01/04/2022 - 31/08/2022

<!-- TOC -->

- [**DCAS : STAGE Simusat**](#dcas--stage-simusat)
  - [**[ 01/04/2022 ]**](#-01042022-)
  - [**[ 04/04/2022 ]**](#-04042022-)
  - [**[ 05/04/2022 ]**](#-05042022-)
  - [**[ 06/04/2022 ]**](#-06042022-)
  - [**[ 07/04/2022 ]**](#-07042022-)
  - [**[ 08/04/2022 ]**](#-08042022-)
  - [**[ 11/04/2022 ]**](#-11042022-)
  - [**[ 12/04/2022 ]**](#-12042022-)
  - [**[ 13/04/2022 ]**](#-13042022-)
  - [**[ 14/04/2022 ]**](#-14042022-)
  - [**[ 15/04/2022 ]**](#-15042022-)
  - [**[ 19/04/2022 ]**](#-19042022-)
  - [**[ 20/04/2022 ]**](#-20042022-)
  - [**[ 21/04/2022 ]**](#-21042022-)
  - [**[ 22/04/2022 ]**](#-22042022-)
  - [**[ 25/04/2022 ]**](#-25042022-)
  - [**[ 26/04/2022 ]**](#-26042022-)
  - [**[ 27/04/2022 ]**](#-27042022-)
  - [**[ 28/04/2022 ]**](#-28042022-)
  - [**[ 02/05/2022 ]**](#-02052022-)
  - [**[ 03/05/2022 ]**](#-03052022-)
  - [**[ 04/05/2022 ]**](#-04052022-)
  - [**[ 05/05/2022 ]**](#-05052022-)
  - [**[ 06/05/2022 ]**](#-06052022-)
  - [**[ 09/05/2022 ]**](#-09052022-)
  - [**[ 10/05/2022 ]**](#-10052022-)
  - [**[ 11/05/2022 ]**](#-11052022-)
  - [**[ 12/05/2022 ]**](#-12052022-)
  - [**[ 13/05/2022 ]**](#-13052022-)
  - [**[ 16/05/2022 ]**](#-16052022-)
  - [**[ 17/05/2022 ]**](#-17052022-)
  - [**[ 18/05/2022 ]**](#-18052022-)
  - [**[ 19/05/2022 ]**](#-19052022-)
  - [**[ 20/05/2022 ]**](#-20052022-)
  - [**[ 23/05/2022 ]**](#-23052022-)
  - [**[ 24/05/2022 ]**](#-24052022-)
  - [**[ 25/05/2022 ]**](#-25052022-)
  - [**[ 30/05/2022 ]**](#-30052022-)
  - [**[ 31/05/2022 ]**](#-31052022-)
  - [**[ 01/06/2022 ]**](#-01062022-)
  - [**[ 02/06/2022 ]**](#-02062022-)
  - [**[ 03/06/2022 ]**](#-03062022-)
  - [**[ 07/06/2022 ]**](#-07062022-)
  - [**[ 08/06/2022 ]**](#-08062022-)
  - [**[ 09/06/2022 ]**](#-09062022-)
  - [**[ 10/06/2022 ]**](#-10062022-)
  - [**[ 13/06/2022 ]**](#-13062022-)
  - [**[ 14/06/2022 ]**](#-14062022-)
  - [**[ 15/06/2022 ]**](#-15062022-)
  - [**[ 16/06/2022 ]**](#-16062022-)
  - [**[ 17/06/2022 ]**](#-17062022-)
  - [**[ 20/06/2022 ]**](#-20062022-)
  - [**[ 21/06/2022 ]**](#-21062022-)
  - [**[ 22/06/2022 ]**](#-22062022-)
  - [**[ 23/06/2022 ]**](#-23062022-)
  - [**[ 24/06/2022 ]**](#-24062022-)
  - [**[ 27/06/2022 ]**](#-27062022-)
  - [**[ 28/06/2022 ]**](#-28062022-)

<!-- /TOC -->



## **[ 01/04/2022 ]**

- Sujets administratifs. 
- Présentation équipe.
- Poste de travail.
- Démarrage de mettre au jour des logiciels à utiliser. PC personnelle.

## **[ 04/04/2022 ]**

- Les logiciels d'utilisation sont déjà prêts. PC personnelle
- Des recherches des tutoriels ;

**VBnet**

[VBnet tutoriel sur YouTube](https://www.youtube.com/playlist?list=PLRGx0oGhYQoH0OHLl6l6lZNck79kayjcA) 


**UnitTest**

[UnitTest tutoriel sur page web](https://www.typemock.com/unit-testing-vbnet/)


**Git**

[Git tutoriel sur YouTube 1](https://www.youtube.com/watch?v=RGOj5yH7evk&t=594s) 

[Git tutoriel sur YouTube 2](https://www.youtube.com/watch?v=USjZcfj8yxE&t=101s) 





## **[ 05/04/2022 ]**

- Exploration des codes sources de Simusat au sein de Git Hub.
- Rapport des stages des étudiants précédents.

- Création du fichier EXCEL: Simusat_VBnet_inventario.
- Interpretation des fichiers, dossiers, livrairies, etc.


## **[ 06/04/2022 ]**
- Procédures administratives relatives à l'établissement du - poste de travail. PC desk pas résolu
- Création d'un nouveau dossier dans la branche -Stage_2022_Victor pour bien mettre tout le contenu du test - VBnet et avancement du stage: .../simusat-dev/DOC_Simusat/Victor2022


**VBnet**

[VBnet tutoriel sur page web](https://www.tutlane.com/tutorial/visual-basic/vb-constructors) 


**Liaison Git et VScode**

[Git et VScode tutoriel sur page web](https://www.mclibre.org/consultar/informatica/lecciones/vsc-git-repositorio.html)

[Git et VScode tutoriel sur YouTube](https://www.youtube.com/watch?v=RGOj5yH7evk&t=594s)



## **[ 07/04/2022 ]**
- Matinée: Procédures de configuration relatives à l'établissement du poste de travail. PC desk résolu mais pas de connexion au réseau ISAE-PRO.(Connexion aux autres réseaux ISAE-EDU...)

- Finalement on a réussi à se connecter à Git via VScode et éditer/télécharger des fichiers

- Poursuite des tutoriels VBnet et de l'exploration des dossiers au sein de Git/simusat-dev

- Configuration et installation des logiciels sur PC desk

## **[ 08/04/2022 ]**
- Demande d'autorisation au SI pour l'installation des logicials sur PC desk: 
VisualStudio2019 et Simusat


- Comments:
   - Simusat n'a pas de Solar Wind ?

- Rapport Angel2019:
   - UnitTest...branch Angel_Stage2019/chercher sur Commits
     e.g. [DEV] Unit Test for Type 2 panel upgrade

- Je continue à suivre les tutoriels VBnet (très similaires à Java) et à regarder les rapports des stgiaires précédents.

## **[ 11/04/2022 ]** 

- On a bien pris en compte qu'il faut avoir un compte pro(au-delà de celui étudiant) pour avoir accès  à ISAE-PRO et les droits pour installer des logiciels sur PC desk.

- On a demandé au SI la compte pro(via RH le M. Gateau)

- Problème résolu concernant VBnet(VisualStudio2010) interface et control methods

- Rappel de certains concepts de l'inveronnement spacial :
  
   - *Wilfried Ley, Klaus Wittmann, Willi Hallmann - Handbook of Space Technology (Aerospace Series (PEP)) (2009)*

- Tutorials VBnet interface and control methods
  
  - [VBnet tutoriel sur page web 1](https://www.javatpoint.com/vb-net-form-controls)

  - [VBnet tutoriel sur page web 2](https://www.tutorialspoint.com/vb.net/vb.net_basic_controls.htm)

  - [VBnet tutoriel sur YouTube](https://www.youtube.com/watch?v=eOYZpYtLs9Y)

## **[ 12/04/2022 ]**

- Tutorials VBnet interface and control methods
  
  - [VBnet tutoriel sur page web 1](https://www.javatpoint.com/vb-net-form-controls)

- Commit des pojets VBnet sur Git

- Changement du fichier  Stage_Simusat_DCAS.txt par README.md en maîtrisant les markdowns.
- Tutoriels markdown
  - [Markdown tutoriel sur page web 1](https://www.markdownguide.org/basic-syntax/#overview)
  - [Markdown tutoriel sur page web 2](https://code.visualstudio.com/docs/languages/markdown)

- Démarrage du changement du fichier README Simusat. On doit ajouter :
  - Users intallation procedure
  - Developers installation procedure
  - Quik start
  - Etc, ...
   
## **[ 13/04/2022 ]**

- UnitTest HelloWorld
- Tutoriels UnitTest
   - [UnitTest tutoriel sur YouTube 1](https://www.youtube.com/watch?v=ZvRDo3LdaDM)

   - [UnitTest tutoriel sur YouTube 2](https://www.youtube.com/watch?v=e7EStbS1TWk)

   - [UnitTest tutoriel sur page web 1](https://learntutorials.net/fr/vb-net/topic/6843/test-d-unite-dans-vb-net)

   - [UnitTest tutoriel sur page web 2](https://docs.microsoft.com/fr-fr/visualstudio/test/getting-started-with-unit-testing?view=vs-2022&tabs=dotnet%2Cmstest)

- Problème pour lancer les UnitTests
  
  Pour résoudre ce problème il faut prendre en compte que le *Target framework* soit le même au moment de créer les UnitTest ainsi que les projets Vb. Le message d'erreur `error BC30652: Reference required to assembly 'System.Windows.Forms, Version=4.0.0.0, Culture=neutral`
. [Solution](https://docs.microsoft.com/en-us/dotnet/core/tutorials/library-with-visual-studio?pivots=dotnet-6-0)

## **[ 14/04/2022 ]** 

  - UnitTest

    Téléchargement des différents versions NET Framework pour assurer la compatibilité entre les unitTests el les projets.
     - Reférences :
       - [VisualStudio réference 1](https://docs.microsoft.com/fr-fr/visualstudio/ide/visual-studio-multi-targeting-overview?view=vs-2022)

       - [VisualStudio réference 2](https://docs.microsoft.com/fr-fr/cpp/build/how-to-modify-the-target-framework-and-platform-toolset?view=msvc-170)

       - [VisualStudio réference 3](https://stackoverflow.com/questions/58000123/visual-studio-cant-target-net-framework-4-8)

       - [VisualStudio réference 4](https://developercommunity.visualstudio.com/t/net-framework-48-sdk-and-targeting-pack-in-visual/580235)

   - Les différents types des unitTest 
      - [Réference 1](https://www.lambdatest.com/blog/nunit-vs-xunit-vs-mstest/)   

## **[ 15/04/2022 ]** 

- Pour le moment ne marchent que les test sur Unit Test Project (.NET Framework). C'est pas possible sur MSTest Project, xUnit Test Project, NUnit Test Project, des inconvénients par rapport aux *Target Framework*.

- Des Unit Tests trouvés sur Git (projet Simusat) utilisent  Unit Test Project (.NET Framework)
- Live Unit Testing is available only in VS 2017
- Réferences du jour : 
   - [Réference UnitTest 1](https://hub.packtpub.com/unit-testing-in-net-core-with-visual-studio-2017-for-better-code-quality/)  

   - [Réference UnitTest 2](http://devapps.be/podcast/les_tests_unitaires_dans_visualstudio_facile_christophe) 

- Interprétation des UnitTests faites par les stagiaires précédents  ([ThermalUnitTest.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/0e7f332d97662b8494cf8b55f0622560bf7e02dc/ThermalUnitTest.vb)) et SynthesisUnitTest (Git...[simusat/SynthesisUnitTest.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/SynthesisUnitTest.vb) )

- Rappel de certains concepts de l'inveronnement spacial :
  
   - *Wilfried Ley, Klaus Wittmann, Willi Hallmann - Handbook of Space Technology (Aerospace Series (PEP)) (2009)*

## **[ 19/04/2022 ]** 

- Interprétation des UnitTests faites par les stagiaires précédents  ([ThermalUnitTest.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/0e7f332d97662b8494cf8b55f0622560bf7e02dc/ThermalUnitTest.vb)) et SynthesisUnitTest (Git...[simusat/SynthesisUnitTest.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/SynthesisUnitTest.vb) )

 - Développement du [BE](https://sourceforge.isae.fr/projects/miniproject_intro_space_systems), partie thermique et de contrôle.

- Référence concepts de l'inveronnement spacial :

   - *Wilfried Ley, Klaus Wittmann, Willi Hallmann - Handbook of Space Technology (Aerospace Series (PEP)) (2009)*

## **[ 20/04/2022 ]** 

- Développement du [BE](https://sourceforge.isae.fr/projects/miniproject_intro_space_systems), partie thermique et de contrôle.
    - Obtentions des résultats faites à la main. On a trouvé des ordre de grandeurs pareils á ceux obtenus via Simusat 
- Référence concepts de l'inveronnement spacial :
  - *Wilfried Ley, Klaus Wittmann, Willi Hallmann - Handbook of Space Technology (Aerospace Series (PEP)) (2009)*

- Point d'avancement : 
   - Faire des tutoriels sur UnitTest (README)
   - Trouver des bugs sur *Energy Sub-System simulation*
  
  ## **[ 21/04/2022 ]** 

- Référence concepts Electrical Power Supply :
  - *Wilfried Ley, Klaus Wittmann, Willi Hallmann - Handbook of Space Technology (Aerospace Series (PEP)) (2009)*

- Lancement du logiciel Simusat sur VS2019

  - Configuration des bibliothèques
    - Inconvénient : il est pas possible de trouver le chamin d'accès au `System.Windows.Forms.DataVisualisations` 
    - Solution : Remove and add à la main cette bibliothèque
  - Stand-by : configuration des bibliothèques `Tao.OpenGl.dll and Tao.Platform.Windows.dll` concernant [CAttitudeSphere.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/CAttitudeSphere.vb) : 

       `Dim Object_Renamed As Tao.OpenGl.Glu.GLUquadric`

       `Dim arrowobj As Tao.OpenGl.Glu.GLUquadric`

 ## **[ 22/04/2022 ]** 

- Lancement du logiciel Simusat sur VS2019
- Résolution des inconvénients de mis en ouvre concernant les  bibliothèques
- Réferences : 
  - Tutoriel installation de [.NET Framework 3.5](https://www.groovypost.com/howto/enable-net-framework-2-windows-8/) 
  - Information sur les bibliothèques :
    - [Réf1](https://sourceforge.net/projects/taoframework/) 
    - [Réf2](http://www.cs.uky.edu/~cheng/cs633/OpenGL_Setup.pdf) 
    - [Réf3](https://members.hellug.gr/nkour/Tao.OpenGL_Builder/SimpleIntro_Borland.html) 

- Mise a jour de main `README.md` en ajoutant, dans la partie *Developers*, la résolution des inconvénients concernant les bibliothèques pour la mise en ouvre du logiciel

 ## **[ 25/04/2022 ]**

 - Des modifications dans main `README.md` en ajoutant la partie *Developers insallation procedure*.

- La branche correspondant au stagiaire Adrien ABADIE s'appelle [dev](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/tree/dev/DOC_Simusat/Adrien_2021)
- Des Bugs avec  *Overall Power Diagram*
  - Des solutions trouvées  en comparant les commits dans la branche *dev* : 
     - [compare1](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/commit/535e8d5fe35ad4791549ec861c9203ef050d40c4)  
     - [compare2](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/commit/cceb28b3b14d5ae73b6df3b4330ef92bedb7f7c4)

- Point d'avancement : 
  - Continuer à resoudre les erreurs rencontrées dans la partie thermique : *Overall Power Diagram*
  - Faire des unitTests pour la partie thermique
  - En cas de blocage, aller aux sujets suivants :
     - API rest. Voir [réf](https://www.toptal.com/bottle/building-a-rest-api-with-bottle-framework)
     - Angular. Voir [réf](https://angular.io/tutorial)

 ## **[ 26/04/2022 ]**

 - Continuation avec la recherche des erreurs dans *Overall Power Diagram*. Possible indicateur des erreurs :
   - `lab_pgs.../frmPowerDiagram.vb` -> `panels_power`
   - `panels_power.../power.vb` -> `panel.PanelPower`
   - `panel.PanelPower.../CSolarPanel.vb` -> `flux_incident_overall_power(panel, Time)`
   - `flux_incident_overall_power(panel, Time).../thermal.vb`

- Référence concepts Electrical Power Supply :
  - *Wilfried Ley, Klaus Wittmann, Willi Hallmann - Handbook of Space Technology (Aerospace Series (PEP)) (2009)*

## **[ 27/04/2022 ]**
- Il es presque certain que le probleme dans *Overall Power Diagram* provient de : 
   - `panel.PanelPower = flux_incident_overall_power(panel, Time) * cell_efficiency(panel.PanelTemp)`
   - On a changé  la valeur de la variable initial `panels_power` (line 123, sur [power.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/power.vb)) et il apparaissent des valeurs sur l'écran
   - On a testé les fonctions de l'expression  `panel.PanelPower = flux_incident_overall_power(panel, Time) * cell_efficiency(panel.PanelTemp)`. 
      - La fonction `cell_efficiency(panel.PanelTemp)` donne des valeurs nulls donc on a changé la valeur `intensite = 1`, line 184 sur [power.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/power.vb) pour tester si la focntion marche bien.
      - L'onglet *Overall Power Diagram* marche en changeant la valeur  `intensite = 0` donc il faut étudier cette partie du code là.
 - Attention : les valeurs ajoutées par défaut, il ne prend en compte pas les panneaux solaires donc il est nécessaire de les ajouter pour que le code puisse calculer la partie thermique.

## **[ 28/04/2022 ]**

Cotinuation avec la résolution des erreurs sur *Overall Power Diagram*. Des erreurs déjà résolus :
   - Managment/Distribution : des erreurs sur les *RadioButton*. Voir frmPowerConf.vb
   - Energy Sub-System Simulation : 
     - des erreurs sur `fl_charge` et `fl_decharge`. Voir [frmPowerDiagram.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/frmPowerConf.vb)
     - des erreurs sur `prg_chargemeter`: changement de la couleur selon l'état de charge
   - Il manque définir d'ou vient l'erreur lié à `intensite` dans la fonction `cell_efficiency(panel.PanelTemp)`. Voir [power.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/power.vb)

- Interprétation des UnitTests faites par les stagiaires précédents  ([ThermalUnitTest.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/0e7f332d97662b8494cf8b55f0622560bf7e02dc/ThermalUnitTest.vb)) et SynthesisUnitTest (Git...[simusat/SynthesisUnitTest.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/SynthesisUnitTest.vb) )

## **[ 02/05/2022 ]**

Cotinuation avec la résolution des erreurs sur *Overall Power Diagram*. Des erreurs déjà résolus :
  - Des graphiques sur l'onglet *Batteries*
  - Des erreurs type *NaN* appareaissent parfois dans l'onglet

Interprétation des UnitTests faites par Michele Genoni  ( [basMathUnitTest.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/basMathUnitTest.vb) ) et SynthesisUnitTest (Git...[simusat/SynthesisUnitTest.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/SynthesisUnitTest.vb) )

- Référence concepts Electrical Power Supply :
  - *(Aiaa Education Series) Wren Software, Inc. C. Brown - Elements of Spacecraft Design (2002)(1st ed.)(en)(610s)-AIAA (2003)*

## **[ 03/05/2022 ]**
Cotinuation avec la résolution des erreurs sur *Overall Power Diagram*. Des erreurs déjà résolus:
   - Affichage sur écran des valeurs, déjà résolu : faites  attention aux ordres des valurs saisies à la main ou celles déjà établies
   - Configuration des certains graphiques.
  
Référence concepts Electrical Power Supply :
  - *(Aiaa Education Series) Wren Software, Inc. C. Brown - Elements of Spacecraft Design (2002)(1st ed.)(en)(610s)-AIAA (2003)*
  - *(AIAA education series) Michael D. Griffin, James R. French - Space vehicle design-American Institute of Aeronautics and Astronautics, Inc (2004)*

Démarrage avec des unitTest pour l'*Electrical Power System* :
  - Création des unitTests dédiés à l'EPS,  voir [EPSUnitTest.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/SynthesisUnitTest.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/Stage_2022_Victor/EPSUnitTest.vb))

## **[ 04/05/2022 ]**

Continuation avec des unitTest pour l'*Electrical Power System* :
 - On prend comme valeurs de référence celles du BE et celles trouvées dans la bibliographie :
   -  *(AIAA education series) Michael D. Griffin, James R. French - Space vehicle design-American Institute of Aeronautics and Astronautics, Inc (2004)*
   -  *(Aiaa Education Series) Wren Software, Inc. C. Brown - Elements of Spacecraft Design (2002)(1st ed.)(en)(610s)-AIAA (2003)*
   -  *Wilfried Ley, Klaus Wittmann, Willi Hallmann - Handbook of Space Technology (Aerospace Series (PEP)) (2009)*

Des erreurs déjà résolus : 

- Energy Sub-System Simulation - Overall Diagram
  - Affichage des valeurs sur écran
     * frm.Main.vb/ line 984 :  `power.compute_power(dblSimulationTime, dblTimeStep * ticks / 1000)`
     * power.vb/ line 141 : `panel.PanelPower = ...`
  - Flèche dans la partie batterie
  - Barre de déchargement


- Energy Data
  - Managment Distribution : Regulation/ No regulation
  - Batteries : * des unités ajoutées dans les graphiques
                * doute sur Discharge ?
  - Sollar cells : * des unités ajoutées dans les graphiques
                   * doute sur Effect solar incidence ?


Point d'avancement :

- Finir les bugs et compléter les unitTests

- Version exe + stand alone. Voir ce qui a fait Angel

- Linux : voir si  le logiciel marche sur virtual box - ubuntu 20.04 wine 

- API rest. Voir ce qui a fait Adrien 

- CREME-scripts. Voir [creme-project](https://gitlab.isae-supaero.fr/creme-project/creme-scripts/-/blob/master/Python/Data/input_600km.yaml)

- Bottle Python. Voir [bottlepy.org](https://bottlepy.org/docs/dev/)

- NANOSPACE UI. Voir [dcas-nanostar.isae.fr](https://dcas-nanostar.isae.fr/login)

## **[ 05/05/2022 ]**

Continuation avec le développement d'*EPSUnitTest* : 
  - Creation des objets et saisie des valeurs pour créer les tests
    - On prend comme valeurs de référence celles du BE et celles trouvées dans la bibliographie :
     -  *(AIAA education series) Michael D. Griffin, James R. French - Space vehicle design-American Institute of Aeronautics and Astronautics, Inc (2004)*
     -  *(Aiaa Education Series) Wren Software, Inc. C. Brown - Elements of Spacecraft Design (2002)(1st ed.)(en)(610s)-AIAA (2003)*
     -  *Wilfried Ley, Klaus Wittmann, Willi Hallmann - Handbook of Space Technology (Aerospace Series (PEP)) (2009)* 
     -  BE Avant-projet
  - Nuveau test : `panelsPower()`. Voir [EPSUnitTest.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/SynthesisUnitTest.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/Stage_2022_Victor/EPSUnitTest.vb))

## **[ 06/05/2022 ]**

- Continuation avec le développement d'*EPSUnitTest*.
- Des erruers apparaissent en provenance d'autre unitTest.
   - ThermalUnitTest : `<TestMethod> Public Sub Report_test()`
- Premier lancement de la version *Setup*
  - Des erreurs dans la creation : `ERROR: An error occurred while validating. HRESULT = '80070005' `. Ils sont liés peut-être aux différentes versions bibliothèques présentes dans le projet.
  
## **[ 09/05/2022 ]**

- Le problème du *Setup* à cause de l'erreur `ERROR: An error occurred while validating. HRESULT = '80070005' ` est déjà résolu.
   - Il a fallu modifier le fichier `SimuSCA.vbproj` : `<Project DefaultTargets="Build" ToolsVersion="4.0" ...`

- Références : 
   - [stackoverflow.com](https://stackoverflow.com/questions/986224/why-do-i-get-an-error-occurred-while-validating-hresult-80004005-when-bui)
   - [microsoft.docs 1](https://docs.microsoft.com/en-gb/visualstudio/porting/port-migrate-and-upgrade-visual-studio-projects?view=vs-2022) 
   - [microsoft.docs 2](https://docs.microsoft.com/en-gb/visualstudio/msbuild/msbuild-toolset-toolsversion?view=vs-2022) 
   - [microsoft.docs 3](https://docs.microsoft.com/en-gb/visualstudio/msbuild/overriding-toolsversion-settings?view=vs-2022) 
   -  [video YouTube ](https://www.youtube.com/watch?v=7iVIfkVHKII) 

- Nouvelle version de *Simusat* disponible sur Git : [Simusta-v3.4](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/tree/Stage_2022_Victor/Setup%20Project/SetupVersions_StageVictor2022/SetupSimusat/Release)

- Ajout de la solution du problème *Setup* au main  `README `.

## **[ 10/05/2022 ]**
- Ajout de la solution du *Setup* problème au main  `README`. Voir *Setup Procedure*.


Point d'avancement :
  - Regarder les graphiques et ajouter des unités : *Charge, Discharge, Autodischarge, Available Capacity*.
  - Trouver un noveau exemple pour l'ajouter comme valeur par default.
  - Fair des changes sur *About Simusat*.
  - Des bugs à résoudre :
    -  par default, coche la case dans orbit data qui permet de voir l'orbite , sur 1 orbits stp (tant que j'y pense)
    - un petit message, dans "overall diagram'" "Warning : No solar panel" le cas échéant...  
- StandAloneVersion 

## **[ 11/05/2022 ]**

 - Ajout des unités : *Charge, Discharge, Autodischarge, Available Capacity*.
 - Recherge d'exemples pour les valeurs par default 

  - Références : 
     -  *(AIAA education series) Michael D. Griffin, James R. French - Space vehicle design-American Institute of Aeronautics and Astronautics, Inc (2004)*
     -  *(Aiaa Education Series) Wren Software, Inc. C. Brown - Elements of Spacecraft Design (2002)(1st ed.)(en)(610s)-AIAA (2003)*
     -  *Wilfried Ley, Klaus Wittmann, Willi Hallmann - Handbook of Space Technology (Aerospace Series (PEP)) (2009)* -
     -  [Space Science and Technologies ] Qi Chen, Zhigang Liu, Xiaofeng Zhang, Liying Zhu - Spacecraft Power System Technologies (2020, Springer)

- Lancement de la nouvelle version *Simusat_v6.4.2* : on a nommé une telle version (*.2*) pour savoir les versions délivrées pendant le stage Victor2022

## **[ 12/05/2022 ]**

- Update du script PC-desk en tant que stagiaire

- Des bugs résolus : 
   - *Footprint* du satellite sur le planisphère : le logiciel décoche la caisse lorsqu'on démarre
   - Update d'*About Simusat* en ajoutant les contributeurs
   - Des unités sur les graphiques

- A faire : 

  - main `README` :  Ajouter l'explication de ce qui se passe par rappor aux bibliothèques `.dll` dans la procedure de *Setup* el les fichiers du *bin*
   - un petit message, dans "overall diagram'" "Warning : No solar panel" le cas échéant...  
   - StandAloneVersion 
 
## **[ 13/05/2022 ]**

- Des bugs résolus : 
  -  un petit message, dans "overall diagram'" "Warning : No solar panel" le cas échéant...
  -  - *Footprint* du satellite sur le planisphère : le logiciel décoche la caisse lorsqu'on démarre. New update
  -  warning : *The minimum voltage produced by a string must be greater than the maximum voltage required on the power bus. which is normally the battery charging voltage.*. Dans ce cas-là il faut modifier le *solar string*.

## **[ 16/05/2022 ]**

- Des bugs résolus : 
  -  UPDATE : un petit message, dans "overall diagram'" "Warning : No solar panel" le cas échéant...
  -   UPDATE : main `README`.  Ajoute de l'explication de ce qui se passe par rappor aux bibliothèques `.dll` dans la procedure de *Setup* el les fichiers du *bin*
  -   UPDATE : certain paramètres concernant la procédure de *Setup*
  -   UPDATE : la valeur par default du nombre de cellules. Energy Data/Solar cells/Cells per string : `cell_ns = 15`. Voir [frmMain.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/Stage_2022_Victor/frmMain.vb), line 485.

## **[ 17/05/2022 ]**

- UPDATE. Warning : *The minimum voltage produced by a string must be greater than the maximum voltage required on the power bus. which is normally the battery charging voltage.* Dans ce cas-là il faut modifier le *solar string*. 
  - Le message devrait apparaitre en cas d'avoir solar panels.
- *Standalone version* sur le dossier [bin](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/tree/Stage_2022_Victor/bin). Il semble bien fonctionner sans l'installation des softwares.
   - L'app pour démarrer le logiciel s'appel *Simusat*. Elle se mettre à jour automatiquement lorsqu'on fait des modification. Voir [Simusat-v6.4.2_Standalone](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/tree/Stage_2022_Victor/StandAloneVersions)
   - Références :
      - [Page web 1](https://www.vbforums.com/showthread.php?342909-Steps-for-creating-VB-Net-standalone-exe-RESOLVED)
      - [Page web 2](https://localcoder.org/create-a-standalone-exe-without-the-need-to-install-net-framework)
      - [Video](https://www.youtube.com/watch?v=w4FUOec03iw)
- Il y un problème issue du *Mass, geometry*. Il semble ne fair pas un update; ce qui produce (parfois) dans *Overall diagram* n'afficher acune valeur concernant les panneaux.

## **[ 18/05/2022 ]**

- On a essayé de réorganiser les fichiers du project mais cela a produit(peut-être) la déconfiguration du *Setup*. Même si on a seuivi la procedure pour la création d'un nouveau setup, il y avait des problèmes. Ces derniers, ils ont ete évacués apparemment après d'avoir faire un *UPDATE* de l'OS.
- On a dû récupérer la version git du jour précédent et faire un *update* de l'OS. Cela a marché.
- Lorsqu'on  fait un *Reset (New Sat)*, il me donne des valeurs diférentes des valeurs par défault.
- Il y a un problème issue du *Mass, geometry*. Il semble ne fair pas un update; ce qui produce (parfois) dans *Overall diagram* n'afficher acune valeur concernant les panneaux.

## **[ 19/05/2022 ]**

**Bug résolu**. Il y avait un problème issue du *Mass, geometry*. Il semblait ne fair pas un update; ce qui produisait (parfois) dans *Overall diagram* n'afficher acune valeur concernant les panneaux.

A faire :

 - Essayer de déplacer la version *Setup* pour ré-organiser le projet.
 - Changer les valeurs qui apparaissent lorsqu'on fait *Reset (New Sat)* par celles par défault.
 - Update main README. Ajouter des commentaires concernant la possibilté d'ajouter des projet *Setup* déjà créés au projet courant.

Point d'avancement : 
- Regarder et installer CREME-scripts
- Fair des unitTest avec différents scénarios
- Corriger des uniTest déjà créés
- Version *Standalone* fournie pour les tests

## **[ 20/05/2022 ]**
- Approfondissement sur le lancememnt des logiciels *Windows* sur l'environnement *Linux*
- Installation de *WineHQ* sur Ubuntu 18.04 pour créér **l'invironnement Windows** sur Linux. Voir  [WineHQ Ubuntu](https://wiki.winehq.org/Ubuntu)
## **[ 23/05/2022 ]**
Continuation avec la mise à jour de *WineHQ* :
  - Installation de *Winetricks*. Voir [Installing WineHQ packages](https://wiki.winehq.org/Ubuntu).
  - Intallation des bibliothèques `.dll` sur WineHQ, grâce au script Winetricks.
    - Références :
      - [Ubuntu documentation](https://help.ubuntu.com/community/Wine) 
      - [Installation des Frameworks .NET](https://appdb.winehq.org/objectManager.php?sClass=version&iId=25478) 
  - Lancement de la version **standalone** du Simusat sur Linux, **réussie**.
    - Références :
      - [WineHQ forums](https://forum.winehq.org/viewtopic.php?t=24129) 
      - [Linux forums](https://unix.stackexchange.com/questions/337703/how-to-add-a-windows-program-under-wine-that-does-not-have-an-installer) 
## **[ 24/05/2022 ]**

- Des tests sur l'environnement Linux. Il semble que le logiciel marche bien, pas des erreurs pour l'instant.

- UPDATE main README. Ajout de la partie *Run Simusat in the Linux environment*.
- On a détecté la manque des unités sur les planisphère.
- A faire :

  - Essayer de déplacer la version Setup pour ré-organiser le projet.
  - Changer les valeurs qui apparaissent lorsqu'on fait Reset (New Sat) par celles par défault.

## **[ 25/05/2022 ]**

- Ré-configuration du projet réusi. Création de trois dossiers :
   - doc/...
   - src/...
   - setup/...

- Ajout des  unités sur le planisphère.
- Changement des valeurs par défault lorsqu'on fait Reset (New Sat)
- A faire :
  - Fair des unitTest avec différents scénarios
  - Ajouter des commentaires concernant la création du *Setup* : *Configuration Manager*

## **[ 30/05/2022 ]**
- Ajout des  unités sur l'espace. Il manque sur le satellite
- Démarrage avec le test d'intégration. Voir [IntegrationTest.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/Stage_2022_Victor/src/IntegrationTest.vb) 
- Ajout des commentaires concernant la création du *Setup* : *Configuration Manager*
- Recherche des mission spaciaux pour récupérer des données pour les tests d'intégration: 
  - [Réf1](http://www.eohandbook.com/eohb05/ceos/part3_3.html) 
  - [Réf2](https://earth.esa.int/web/eoportal/satellite-missions/c-missions/copernicus-sentinel-1) 
  - Réf3 : *2013_Book_HandbookOfSatelliteApplication J.P. Nelton, S. Madry, S. Camacho-Lara- Ed. Springer V1*

## **[ 31/05/2022 ]**

- RESOLU. Ajout des  unités sur *Display*, il manquait depuis le satellite.
- Test de la version *Standalone* sur les PCs du SI :
  - L'erreur trouvé viens de l'option de la trace du satellite sur le planisphère `satFTrackSize` ou `satBTrackSize` (dans **frmDisplay.vb**). Le logiciel se plante.


- Continuation avec le test d'intégration.
  
- Des erreurs d'affichage lorsqu'on essaye de visualizer les *forms*. Résolu.

## **[ 01/06/2022 ]**
- Continuation avec le test d'intégration.
- - L'erreur trouvé viens de l'option de la trace du satellite sur le planisphère `satFTrackSize` ou `satBTrackSize` (dans **frmDisplay.vb**). Le logiciel se plante.


## **[ 02/06/2022 ]**
- Continuation avec le test d'intégration.
  
- L'erreur trouvé viens de l'option de la trace du satellite sur le planisphère `satFTrackSize` ou `satBTrackSize` (dans **frmDisplay.vb**). Le logiciel se plante.
   - Le programme se plante puisque la valeur de l'inclination est nulle lorsqu'on choisit une orbite circulaire.
   - Peut-être une division par zéro mais je viens de retrouver que ce processus de calcul est lié au **tracksize, le pas pour la trace et le timestep**.
   - Je change la valeur lorsque l'inclination est nulle, par 0.5°. Une valeur plus petite ne marche pas.

## **[ 03/06/2022 ]**
- Continuation avec le test d'intégration :
  - Ajoute de `sat_orbit_scenarios_test()`.
- Continuation avec l'étude du bug concernant la trace du satellite sur le planisphère `satFTrackSize` ou `satBTrackSize` (dans **frmDisplay.vb**).

## **[ 07/06/2022 ]**
- Continuation avec le test d'intégration :
  - Update de `sat_orbit_scenarios_test()` : 
    - On a réusi à fair des simultions des orbits ainsi que des satellites. Il prend en compte le temps de simulation.

Continuation avec l'étude du bug concernant la trace du satellite sur le planisphère `satFTrackSize` ou `satBTrackSize` (dans **frmDisplay.vb**).

## **[ 08/06/2022 ]**
- Continuation avec le test d'intégration 
  

- Continuation avec l'étude du bug concernant la trace du satellite sur le planisphère `satFTrackSize` ou `satBTrackSize` (dans **frmDisplay.vb**).


- Point d'avancement :
   - Réorganiser le dossier dedié aux versions  standalone.
   - Essayer de mettre des *prints*/*try-catch* pour détecter le bug(concernant la trace du satellite sur le planisphère).

## **[ 09/06/2022 ]**

- Réorganisation du dossier dédié aux versions  standalone.

- Continuation avec le test d'intégration :
  - Ajout de *Thermal* et *Power* calculation aux différents scénarios 

- Point d'avancement :
   - Ajouter des données des références aux scénarios. Peut-etre celles déjà calculées
   - Mettre la version du Simusat au main `README` pour les téléchergement.

## **[ 10/06/2022 ]**
- Continuation avec l'étude du bug concernant la trace du satellite sur le planisphère `satFTrackSize` ou `satBTrackSize` (dans **frmDisplay.vb**).

- Problème pour générer les tests d'intégration lorsqu'on essaye de calculer la puissance des panneaux (dans une boucle).

## **[ 13/06/2022 ]**

- Update du main README :
  - On a ajouté les liens pour télécharger les version installable et standalone.
  - Des explicationes concernant l'orgnisation du projet
- Problème par rapport les tests d'intégration résolu.

## **[ 14/06/2022 ]**
- Continuation avec le test d'intégration : 
  - IntegrationTest 
  
## **[ 15/06/2022 ]**

- Problème pour générer les tests d'intégration lorsqu'on essaye de calculer de fair en boucle en comparant  la puissance des différents types des panneaux 

## **[ 16/06/2022 ]**

Tests d'intégration réussi
## **[ 17/06/2022 ]**

On a trouvé une possible solution pour les tracks des orbites circulaires. 

## **[ 20/06/2022 ]**

- On a trouvé une  solution assez proche pour les les tracks des orbites circulaires. Le probleme ne samble pas etre lié à  la division par cero sino à  la puissance de calcul ou aux valeurs des variables du type Double, Float, etc.
- On a démarré les solution des bugs déjà existants.

## **[ 21/06/2022 ]**

- Résolution des UnitTests, nouveaux et déjà existants.

- Résolution du bug à cause des orbites circulaires.

## **[ 22/06/2022 ]**

- Résolution des UnitTests, nouveaux et déjà existants.

- Résolution du bug à cause des orbites circulaires.
  
## **[ 23/06/2022 ]**

- Résolution des UnitTests, nouveaux et déjà existants.

- Résolution du bug à cause des orbites circulaires.

## **[ 24/06/2022 ]**

- **Réussie**: résolution des UnitTests, nouveaux et déjà existants.

- Résolution du bug à cause des orbites circulaires.

## **[ 27/06/2022 ]**

- Résolution du bug à cause des orbites circulaires.

## **[ 28/06/2022 ]**

- **Réussie**: résolution du bug à cause des orbites circulaires.

  - Touts les tests se sont passé avec succès.
  - Le problème etait lié au **timer** (soit intervalle et/ou tick) et à la sur-écriture des valeurs de tracksize.
  - Maintenant, on mets à jour les `tracksize` avant de démarrer l'orbit choisie.
  - On a changé le `timestep` des orbites circulaires.

