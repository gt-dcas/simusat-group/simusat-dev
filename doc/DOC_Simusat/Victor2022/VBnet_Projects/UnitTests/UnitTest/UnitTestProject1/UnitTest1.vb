﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports UnitTest

<TestClass()> Public Class UnitTest1

    <TestMethod()> Public Sub TestMethod1()
        ' TEST 1
        Dim form1 As New Form1
        Dim sum = form1.Sum(1, 2)
        Assert.AreEqual(3, sum)

    End Sub

    <TestMethod()> Public Sub TestMethod2()
        'TEST 2
        Dim x As Integer
        x = 6
        Assert.IsTrue(x < 10, “TestMethod2 was successful“)
        'Assert.IsTrue(x > 7, “TestMethod2 has failled“)
    End Sub

End Class