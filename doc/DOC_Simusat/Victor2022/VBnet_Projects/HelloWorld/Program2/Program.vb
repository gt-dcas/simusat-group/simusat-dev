Imports System



Module Module1

    'Visual Basic Iterate through Enum
    Enum Week

        Sunday
        Monday
        Tuesday
        Wednesday
        Thursday
        Friday
        Saturday

    End Enum


    Sub Main()
        Dim a As Integer = CInt(Week.Sunday)
        Dim b As Integer = CInt(Week.Monday)
        Dim c As Integer = CInt(Week.Tuesday)
        Console.WriteLine(Week.Sunday)
        Console.WriteLine(Week.Monday)
        Console.WriteLine("Sunday: {0}", a)
        Console.WriteLine("Monday: {0}", b)
        Console.WriteLine("Tuesday: {0}", c)

        Console.WriteLine("Week Enumeration Values")
        For Each w As String In [Enum].GetNames(GetType(Week))
            Console.WriteLine(w)
        Next

        Console.WriteLine("Visual Basic Split String into List")
        'Visual Basic Split String into List
        Dim msg As String = "Suresh,Rohini,Trishika,-Praveen%Sateesh"
        Dim list As IList(Of String) = New List(Of String)(msg.Split(New Char() {","c, "-"c, "%"c}, StringSplitOptions.RemoveEmptyEntries))
        For i As Integer = 0 To list.Count - 1
            Console.WriteLine(list(i))
        Next

        Console.WriteLine("Visual Basic Copy Methode Example")
        Dim msg1 As String = "Welcome to Tutlane"
        Dim msg2 As String = String.Copy(msg1)
        Console.WriteLine("String: {0}", msg1)
        Console.WriteLine("Copy String: {0}", msg2)
        Console.WriteLine("Reference Equals: {0}", Object.ReferenceEquals(msg, msg1))

        Console.WriteLine("Press Any Key to Exit..")
        Console.ReadLine()

    End Sub

End Module



Public Class User

    Public Name As String

    Private Location As String

    Public Sub New()

        Console.WriteLine("Base Class Constructor")

    End Sub

    Public Sub GetUserInfo(ByVal loc As String)

        Location = loc

        Console.WriteLine("Name: {0}", Name)

        Console.WriteLine("Location: {0}", Location)

    End Sub

End Class



Public Class Details

    Inherits User

    Public Age As Integer

    Public Sub New()

        Console.WriteLine("Child Class Constructor")

    End Sub

    Public Sub GetAge()

        Console.WriteLine("Age: {0}", Age)

    End Sub

End Class



Class Program

    Public Shared Sub Main(ByVal args As String())

        Dim d As Details = New Details()

        d.Name = "Suresh Dasari"

        ' Compile Time Error

        ' d.Location = "Hyderabad";

        d.Age = 32

        d.GetUserInfo("Hyderabad")

        d.GetAge()

        Console.WriteLine(vbLf & "Press Any Key to Exit..")

        Console.ReadLine()

    End Sub

End Class



