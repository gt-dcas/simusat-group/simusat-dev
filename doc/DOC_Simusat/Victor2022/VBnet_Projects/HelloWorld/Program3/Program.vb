Imports System

' Visual Basic Inheritance Example

Public Class User

    Public Name As String
    Private Location As String
    Public Sub New()
        Console.WriteLine("Base Class Constructor")
    End Sub

    Public Sub GetUserInfo(ByVal loc As String)
        Location = loc
        Console.WriteLine("Name: {0}", Name)
        Console.WriteLine("Location: {0}", Location)
    End Sub

End Class



Public Class Details
    Inherits User
    Public Age As Integer
    Public Sub New()
        Console.WriteLine("Child Class Constructor")
    End Sub

    Public Sub GetAge()
        Console.WriteLine("Age: {0}", Age)
    End Sub

End Class



Class Program

    Public Shared Sub Main(ByVal args As String())
        Dim d As Details = New Details()
        d.Name = "Suresh Dasari"
        ' Compile Time Error
        ' d.Location = "Hyderabad";
        d.Age = 32
        d.GetUserInfo("Hyderabad")
        d.GetAge()
        Console.WriteLine(vbLf & "Press Any Key to Exit..")
        Console.ReadLine()
    End Sub

End Class
