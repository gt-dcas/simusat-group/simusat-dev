﻿Imports System

Module Program1
    Public Class [Class]
        Public age As Integer
    End Class

    Sub Main()
        Console.WriteLine("Hello World!")

        'Visual Basic Data Types Example

        Dim id As Integer
        Dim name As String = "Suresh Dasari"
        Dim percentage As Double = 10.23
        Dim gender As Char = "M"c
        Dim isVerified As Boolean
        id = 10
        isVerified = True
        Console.WriteLine("Id:{0}", id)
        Console.WriteLine("Name:{0}", name)
        Console.WriteLine("Percentage:{0}", percentage)
        Console.WriteLine("Gender:{0}", gender)
        Console.WriteLine("Verfied:{0}", isVerified)

        ' Visual Basic Use Keywords as Variable Names

        Dim p1 As [Class] = New [Class]()
        p1.age = 10
        Console.WriteLine("Age: " & p1.age)

        'Visual Basic Method
        GreetMessage()

        'Syntax of Visual Basic if Statement
        Dim x As Integer = 20, y As Integer = 10
        If x >= 10 Then
            Console.WriteLine("Number Greater than 10")
        End If

        'Visual Basic If Else Statement Example
        If x >= 10 Then
            Console.WriteLine("x is Greater than or Equals to 10")
        Else
            Console.WriteLine("x is Less than 10")
        End If

        'Visual Basic Nested Ternary Operator

        Dim result As String
        If x > y Then
            result = "x value greater than y"
        ElseIf x < y Then
            result = "x value less than y"
        Else
            result = "x value equals to y"
        End If
        result = If((x > y), "x value greater than y", If((x < y), "x value less than y", "x value equals to y"))

        'Visual Basic Select Case Statement with Enum
        Dim loc As location = location.hyderabad

        Select Case loc
            Case location.chennai
                Console.WriteLine("Location: Chennai")
            Case location.guntur
                Console.WriteLine("Location: Guntur")
            Case location.hyderabad
                Console.WriteLine("Location: Hyderabad")
            Case Else
                Console.WriteLine("Not Known")
        End Select


        Console.WriteLine("Press Enter Key to Exit.")
        Console.ReadLine()


    End Sub

    'Visual Basic Method Example
    Public Sub GreetMessage()

        Console.WriteLine("Thank for your visit")

    End Sub

    Public Enum location
        hyderabad
        chennai
        guntur
    End Enum



    Public Class Users

        Public id As Integer = 0
        Public name As String = String.Empty

        Public Sub New()

            ' Constructor Statements

        End Sub

        Public Sub GetUserDetails(ByVal uid As Integer, ByVal uname As String)
            id = uid
            uname = name
            Console.WriteLine("Id: {0}, Name: {1}", id, name)

        End Sub

        Public Property Designation As Integer
        Public Property Location As String

    End Class


End Module
