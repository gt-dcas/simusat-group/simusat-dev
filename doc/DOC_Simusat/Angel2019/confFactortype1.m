x = [0.5 0];
y = [0 0];
eta = [0 0];
xi = [0 0];

%Corp
xi(2) = 0.5;
eta(2) = 1;

%Panneau
x(2) = 1+x(1);
y(2) = eta(2);


sum=0;
for l =1:2
    for k = 1:2
        for j = 1:2
            for i = 1:2
                K=(y(j)-eta(k))/sqrt(x(i)^2+xi(l)^2);
                G=((y(j)-eta(k))*sqrt(x(i)^2+xi(l)^2)*atan(K)-0.25*(x(i)^2+xi(l)^2)*(1-K^2)*log((x(i)^2+xi(l)^2)*(1+K^2)))/(2*pi);
                sum = sum+G*(-1)^(i+j+k+l);
            end
        end
    end
end

F_12=sum/((x(2)-x(1))*y(2))