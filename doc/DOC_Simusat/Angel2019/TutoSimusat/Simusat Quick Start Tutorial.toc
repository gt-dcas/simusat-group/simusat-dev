\contentsline {section}{Index}{1}% 
\contentsline {section}{\numberline {1}Introduction}{2}% 
\contentsline {section}{\numberline {2}Creating a satellite}{2}% 
\contentsline {subsection}{\numberline {2.1}Start defining our satellite}{2}% 
\contentsline {subsection}{\numberline {2.2}Create the solar panels}{2}% 
\contentsline {subsection}{\numberline {2.3}Create a gravity gradient boom}{3}% 
\contentsline {section}{\numberline {3}Creating the orbit}{3}% 
\contentsline {section}{\numberline {4}Simulation and output data}{4}% 
\contentsline {subsection}{\numberline {4.1}Simulation}{4}% 
\contentsline {subsection}{\numberline {4.2}Output data (Attitude analysis)}{5}% 
\contentsline {subsection}{\numberline {4.3}Check the results}{5}% 
\contentsline {section}{\numberline {5}A little thermal analysis}{6}% 
