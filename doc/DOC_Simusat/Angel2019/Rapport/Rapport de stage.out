\BOOKMARK [1][-]{section.1}{Introduction}{}% 1
\BOOKMARK [1][-]{section.2}{Methodology}{}% 2
\BOOKMARK [2][-]{subsection.2.1}{Programming environment}{section.2}% 3
\BOOKMARK [2][-]{subsection.2.2}{Forge: ISAE Openforge}{section.2}% 4
\BOOKMARK [2][-]{subsection.2.3}{Git}{section.2}% 5
\BOOKMARK [2][-]{subsection.2.4}{Unit tests and integration tests}{section.2}% 6
\BOOKMARK [1][-]{section.3}{Thermal module of the software}{}% 7
\BOOKMARK [2][-]{subsection.3.1}{Introduction}{section.3}% 8
\BOOKMARK [2][-]{subsection.3.2}{Thermal model}{section.3}% 9
\BOOKMARK [2][-]{subsection.3.3}{Thermal exchanges}{section.3}% 10
\BOOKMARK [3][-]{subsubsection.3.3.1}{Radiation}{subsection.3.3}% 11
\BOOKMARK [3][-]{subsubsection.3.3.2}{Conduction}{subsection.3.3}% 12
\BOOKMARK [3][-]{subsubsection.3.3.3}{Convection}{subsection.3.3}% 13
\BOOKMARK [2][-]{subsection.3.4}{Implementation}{section.3}% 14
\BOOKMARK [3][-]{subsubsection.3.4.1}{Central body}{subsection.3.4}% 15
\BOOKMARK [3][-]{subsubsection.3.4.2}{Solar panel}{subsection.3.4}% 16
\BOOKMARK [3][-]{subsubsection.3.4.3}{Central body - solar panel interaction}{subsection.3.4}% 17
\BOOKMARK [3][-]{subsubsection.3.4.4}{Energy balance solution}{subsection.3.4}% 18
\BOOKMARK [2][-]{subsection.3.5}{CERES model for albedo and Earth infra-red radiation}{section.3}% 19
\BOOKMARK [3][-]{subsubsection.3.5.1}{Introduction}{subsection.3.5}% 20
\BOOKMARK [3][-]{subsubsection.3.5.2}{Implementation of the model}{subsection.3.5}% 21
\BOOKMARK [2][-]{subsection.3.6}{Update of the tab Environment}{section.3}% 22
\BOOKMARK [2][-]{subsection.3.7}{Update of the Mass, geometry tab}{section.3}% 23
\BOOKMARK [2][-]{subsection.3.8}{Unit tests and integration test}{section.3}% 24
\BOOKMARK [1][-]{section.4}{Saved data restructuring}{}% 25
\BOOKMARK [2][-]{subsection.4.1}{.att files}{section.4}% 26
\BOOKMARK [2][-]{subsection.4.2}{.orb files}{section.4}% 27
\BOOKMARK [2][-]{subsection.4.3}{Main window update}{section.4}% 28
\BOOKMARK [1][-]{section.5}{Bonus development}{}% 29
\BOOKMARK [2][-]{subsection.5.1}{Shadow background for eclipses in Graphs}{section.5}% 30
\BOOKMARK [2][-]{subsection.5.2}{Aerodynamic torque correction}{section.5}% 31
\BOOKMARK [1][-]{section.6}{Bug Fixing}{}% 32
\BOOKMARK [2][-]{subsection.6.1}{Fatal bugs}{section.6}% 33
\BOOKMARK [2][-]{subsection.6.2}{Major bugs}{section.6}% 34
\BOOKMARK [2][-]{subsection.6.3}{Minor bugs}{section.6}% 35
\BOOKMARK [1][-]{section.7}{Software deployment}{}% 36
\BOOKMARK [2][-]{subsection.7.1}{Standalone version}{section.7}% 37
\BOOKMARK [2][-]{subsection.7.2}{Installable version}{section.7}% 38
\BOOKMARK [1][-]{section.8}{Conclusions and future works}{}% 39
\BOOKMARK [1][-]{section*.36}{Appendices}{}% 40
\BOOKMARK [1][-]{appendix.A}{Fatal bugs full description}{}% 41
\BOOKMARK [2][-]{subsection.A.1}{Resetting satellite data}{appendix.A}% 42
\BOOKMARK [2][-]{subsection.A.2}{InvalidCastExeption. Unable to convert the string to date}{appendix.A}% 43
\BOOKMARK [2][-]{subsection.A.3}{Automatic update of Parameters of the to-be-plotted list}{appendix.A}% 44
\BOOKMARK [2][-]{subsection.A.4}{Export of Power Report in Excel format}{appendix.A}% 45
\BOOKMARK [1][-]{appendix.B}{Major bugs full description}{}% 46
\BOOKMARK [2][-]{subsection.B.1}{Solar panel surface}{appendix.B}% 47
\BOOKMARK [2][-]{subsection.B.2}{Number of solar cell chains in the solar panels}{appendix.B}% 48
\BOOKMARK [2][-]{subsection.B.3}{Repetition of solar panel allowed if they are modified}{appendix.B}% 49
\BOOKMARK [2][-]{subsection.B.4}{Automatic orbit change if no type is selected}{appendix.B}% 50
\BOOKMARK [2][-]{subsection.B.5}{Change of satellite's attitude}{appendix.B}% 51
\BOOKMARK [1][-]{appendix.C}{Minor bugs full description}{}% 52
\BOOKMARK [2][-]{subsection.C.1}{Start and Current dates on the Main window are not updated after modifying the orbit epoch}{appendix.C}% 53
\BOOKMARK [2][-]{subsection.C.2}{Update of simulation dates in the windows Curves and Report}{appendix.C}% 54
\BOOKMARK [2][-]{subsection.C.3}{Curves: Energy window's calendar}{appendix.C}% 55
\BOOKMARK [2][-]{subsection.C.4}{Possible items for display in Curves: Energy}{appendix.C}% 56
\BOOKMARK [2][-]{subsection.C.5}{Panels Parameters are not disabled by default in Curves windows}{appendix.C}% 57
\BOOKMARK [2][-]{subsection.C.6}{Updating the Y axis in the solar flux display}{appendix.C}% 58
\BOOKMARK [2][-]{subsection.C.7}{Warning message in attitude configuration window when plotting battery parameters}{appendix.C}% 59
\BOOKMARK [2][-]{subsection.C.8}{Orbit file name changes after saving it}{appendix.C}% 60
\BOOKMARK [2][-]{subsection.C.9}{Mouse shown when there is an error in the Energy Report saving}{appendix.C}% 61
