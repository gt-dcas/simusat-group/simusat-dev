\contentsline {section}{\numberline {1}Introduction}{6}{section.1}% 
\contentsline {section}{\numberline {2}Methodology}{7}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Programming environment}{7}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}Forge: ISAE Openforge}{7}{subsection.2.2}% 
\contentsline {subsection}{\numberline {2.3}Git}{7}{subsection.2.3}% 
\contentsline {subsection}{\numberline {2.4}Unit tests and integration tests}{8}{subsection.2.4}% 
\contentsline {section}{\numberline {3}Thermal module of the software}{8}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Introduction}{8}{subsection.3.1}% 
\contentsline {subsection}{\numberline {3.2}Thermal model}{8}{subsection.3.2}% 
\contentsline {subsection}{\numberline {3.3}Thermal exchanges}{10}{subsection.3.3}% 
\contentsline {subsubsection}{\numberline {3.3.1}Radiation}{10}{subsubsection.3.3.1}% 
\contentsline {subsubsection}{\numberline {3.3.2}Conduction}{13}{subsubsection.3.3.2}% 
\contentsline {subsubsection}{\numberline {3.3.3}Convection}{14}{subsubsection.3.3.3}% 
\contentsline {subsection}{\numberline {3.4}Implementation}{14}{subsection.3.4}% 
\contentsline {subsubsection}{\numberline {3.4.1}Central body}{14}{subsubsection.3.4.1}% 
\contentsline {subsubsection}{\numberline {3.4.2}Solar panel}{14}{subsubsection.3.4.2}% 
\contentsline {subsubsection}{\numberline {3.4.3}Central body - solar panel interaction}{15}{subsubsection.3.4.3}% 
\contentsline {subsubsection}{\numberline {3.4.4}Energy balance solution}{16}{subsubsection.3.4.4}% 
\contentsline {subsection}{\numberline {3.5}CERES model for albedo and Earth infra-red radiation}{16}{subsection.3.5}% 
\contentsline {subsubsection}{\numberline {3.5.1}Introduction}{16}{subsubsection.3.5.1}% 
\contentsline {subsubsection}{\numberline {3.5.2}Implementation of the model}{16}{subsubsection.3.5.2}% 
\contentsline {subsection}{\numberline {3.6}Update of the tab \textit {Environment}}{17}{subsection.3.6}% 
\contentsline {subsection}{\numberline {3.7}Update of the \textit {Mass, geometry} tab}{18}{subsection.3.7}% 
\contentsline {subsection}{\numberline {3.8}Unit tests and integration test}{18}{subsection.3.8}% 
\contentsline {section}{\numberline {4}Saved data restructuring}{19}{section.4}% 
\contentsline {subsection}{\numberline {4.1}.att files}{20}{subsection.4.1}% 
\contentsline {subsection}{\numberline {4.2}.orb files}{21}{subsection.4.2}% 
\contentsline {subsection}{\numberline {4.3}Main window update}{21}{subsection.4.3}% 
\contentsline {section}{\numberline {5}Bonus development}{22}{section.5}% 
\contentsline {subsection}{\numberline {5.1}Shadow background for eclipses in Graphs}{22}{subsection.5.1}% 
\contentsline {subsection}{\numberline {5.2}Aerodynamic torque correction}{23}{subsection.5.2}% 
\contentsline {section}{\numberline {6}Bug Fixing}{25}{section.6}% 
\contentsline {subsection}{\numberline {6.1}Fatal bugs}{25}{subsection.6.1}% 
\contentsline {subsection}{\numberline {6.2}Major bugs}{26}{subsection.6.2}% 
\contentsline {subsection}{\numberline {6.3}Minor bugs}{26}{subsection.6.3}% 
\contentsline {section}{\numberline {7}Software deployment}{27}{section.7}% 
\contentsline {subsection}{\numberline {7.1}Standalone version}{28}{subsection.7.1}% 
\contentsline {subsection}{\numberline {7.2}Installable version}{28}{subsection.7.2}% 
\contentsline {section}{\numberline {8}Conclusions and future works}{28}{section.8}% 
\contentsline {section}{Appendices}{30}{section*.36}% 
\contentsline {section}{\numberline {A}Fatal bugs full description}{30}{appendix.A}% 
\contentsline {subsection}{\numberline {A.1}Resetting satellite data}{30}{subsection.A.1}% 
\contentsline {subsection}{\numberline {A.2}InvalidCastExeption. Unable to convert the string to date}{30}{subsection.A.2}% 
\contentsline {subsection}{\numberline {A.3}Automatic update of \textit {Parameters} of the to-be-plotted list}{30}{subsection.A.3}% 
\contentsline {subsection}{\numberline {A.4}Export of \textit {Power Report} in Excel format}{31}{subsection.A.4}% 
\contentsline {section}{\numberline {B}Major bugs full description}{31}{appendix.B}% 
\contentsline {subsection}{\numberline {B.1}Solar panel surface}{31}{subsection.B.1}% 
\contentsline {subsection}{\numberline {B.2}Number of solar cell chains in the solar panels}{32}{subsection.B.2}% 
\contentsline {subsection}{\numberline {B.3}Repetition of solar panel allowed if they are modified}{32}{subsection.B.3}% 
\contentsline {subsection}{\numberline {B.4}Automatic orbit change if no type is selected}{33}{subsection.B.4}% 
\contentsline {subsection}{\numberline {B.5}Change of satellite's attitude}{33}{subsection.B.5}% 
\contentsline {section}{\numberline {C}Minor bugs full description}{34}{appendix.C}% 
\contentsline {subsection}{\numberline {C.1}Start and Current dates on the Main window are not updated after modifying the orbit epoch}{34}{subsection.C.1}% 
\contentsline {subsection}{\numberline {C.2}Update of simulation dates in the windows \textit {Curves} and \textit {Report}}{34}{subsection.C.2}% 
\contentsline {subsection}{\numberline {C.3}\textit {Curves: Energy} window's calendar}{36}{subsection.C.3}% 
\contentsline {subsection}{\numberline {C.4}Possible items for display in \textit {Curves: Energy}}{36}{subsection.C.4}% 
\contentsline {subsection}{\numberline {C.5}\textit {Panels Parameters} are not disabled by default in \textit {Curves} windows}{37}{subsection.C.5}% 
\contentsline {subsection}{\numberline {C.6}Updating the Y axis in the solar flux display}{38}{subsection.C.6}% 
\contentsline {subsection}{\numberline {C.7}Warning message in attitude configuration window when plotting battery parameters}{38}{subsection.C.7}% 
\contentsline {subsection}{\numberline {C.8}Orbit file name changes after saving it}{39}{subsection.C.8}% 
\contentsline {subsection}{\numberline {C.9}Mouse shown when there is an error in the \textit {Energy Report} saving}{39}{subsection.C.9}% 
