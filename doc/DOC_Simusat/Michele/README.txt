READ ME


- demoBE --> it contains all the configuration file used for the newer version of Simusat. 
			It was not possible to do the same for the older version since the save file option does not work perfectly.
            It also contains the results of the test performed with Simusat VB6, using test information described in the pdf BEtemplate. 
			They are in a txt format and can be used for future tests. 
			
- images --> it contains all images used for the BEtemplate document

- summerStage --> pdf document and latex code. It lists what has been done during the internship and few errors that were identified but could not
                  be fixed due to lack of time			