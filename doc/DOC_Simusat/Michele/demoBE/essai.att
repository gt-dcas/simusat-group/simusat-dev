! ---------
! Satellite
! ---------
!
! Central body geometry
! ---------------------
 10                    Body mass
 3                     Body height
 1                     Body width
!
! Solar panels
! ------------
 4                     Panels number
Type2                  Type
1_Type2 [+ X]          Name
+ X                    Mounting face
                       Orientation
                       Height position
 3                     Length
 1                     Width
 1                     Mass
 0                     Strings number
 0.8                   Cells face absorbtivity
 0.9                   Cells face emissivity
 0                     Back face absorbtivity
 0                     Back face emissivity
 2500                  Specific heat
 4                     Panel conduction with central body
Type2                  Type
2_Type2 [- X]          Name
- X                    Mounting face
                       Orientation
                       Height position
 3                     Length
 1                     Width
 1                     Mass
 0                     Strings number
 0.8                   Cells face absorbtivity
 0.9                   Cells face emissivity
 0                     Back face absorbtivity
 0                     Back face emissivity
 2500                  Specific heat
 4                     Panel conduction with central body
Type2                  Type
3_Type2 [+ Y]          Name
+ Y                    Mounting face
                       Orientation
                       Height position
 3                     Length
 1                     Width
 1                     Mass
 0                     Strings number
 0.8                   Cells face absorbtivity
 0.9                   Cells face emissivity
 0                     Back face absorbtivity
 0                     Back face emissivity
 2500                  Specific heat
 4                     Panel conduction with central body
Type2                  Type
4_Type2 [- Y]          Name
- Y                    Mounting face
                       Orientation
                       Height position
 3                     Length
 1                     Width
 1                     Mass
 0                     Strings number
 0.8                   Cells face absorbtivity
 0.9                   Cells face emissivity
 0                     Back face absorbtivity
 0                     Back face emissivity
 2500                  Specific heat
 4                     Panel conduction with central body
!
! Simulation data
! ---------------
 2451989.5             Simulation start date
 0.00694444444444444   Simulation time step
!
!
! --------------------------
! Attitude control subsystem
! --------------------------
!
! Central body inertia
! --------------------
 0                     Body CG.X
 0                     Body CG.Y
 0                     Body CG.Z
 33.33                 Body Ixx
 33.33                 Body Iyy
 6.67                  Body Izz
 0                     Body Ixy
 0                     Body Ixz
 0                     Body Iyz
!
! Gravity gradient device
! -----------------------
 0                     Gradient gravity device mass
 1                     Gradient gravity device position X
 0                     Gradient gravity device position Y
 0                     Gradient gravity device position Z
!
! Pressure coefficients
! ---------------------
 0.9                   Normal CN +X
 0.9                   Normal CN +Y
 0.9                   Normal CN +Z
 0.81                  Normal CN -X
 0.81                  Normal CN -Y
 0.81                  Normal CN -Z
 0.85                  Tangential CT +X
 0.3                   Tangential CT +Y
 0.85                  Tangential CT +Z
 0.77                  Tangential CT -X
 0.27                  Tangential CT -Y
 0.77                  Tangential CT -Z
!
! Sun radiation coefficients
! --------------------------
 0.9                   Reflective CR +X
 0.5                   Reflective CR +Y
 0.9                   Reflective CR +Z
 0.81                  Reflective CR -X
 0.45                  Reflective CR -Y
 0.81                  Reflective CR -Z
 0.05                  Diffuse CD +X
 0.2                   Diffuse CD +Y
 0.05                  Diffuse CD +Z
 0.04                  Diffuse CD -X
 0.18                  Diffuse CD -Y
 0.04                  Diffuse CD -Z
!
! Magnetic dipole (because of passive properties)
! -----------------------------------------------
 10                    Magnetic moment X
 10                    Magnetic moment Y
 10                    Magnetic moment Z
!
! Flywheels
! ---------
 0                     Moment of inertia X
 0                     Moment of inertia Y
 0                     Moment of inertia Z
 0                     Rotational speed X
 0                     Rotational speed Y
 0                     Rotational speed Z
!
! Magnetic dipole (because of magnetic control system)
! ----------------------------------------------------
 0                     Magnetic moment X
 0                     Magnetic moment Y
 0                     Magnetic moment Z
!
! Dampers
! -------
 0                     Mass X
 0                     Mass Y
 0                     Mass Z
 0                     Position at rest X
 0                     Position at rest Y
 0                     Position at rest Z
 0                     Spring constant X
 0                     Spring constant Y
 0                     Spring constant Z
 0                     Damper constant X
 0                     Damper constant Y
 0                     Damper constant Z
!
! Nozzles
! -------
 0                     Control moment X
 0                     Control moment Y
 0                     Control moment Z
!
!
! ----------------
! Energy subsystem
! ----------------
!
! Solar cells
! -----------
 0                     Cell type
 0.53                  Isc
 0.5                   Imp
 1                     Voc
 0.86                  Vmp
 0.0003                dIsc/dT
 0.00025               dImp/dT
-0.002                 dVoc/dT
-0.002                 dVmp/dT
 1                     Isc/Isc0
 1                     Imp/Imp0
 1                     Voc/Voc0
 1                     Vmp/Vmp0
 1                     Number per string
 16                    Cell area
 0.18                  Cell efficiency
 28                    Reference temperature
 50                    Divergence incidence
 75                    Max incidence
!
! Environnement
! -------------
! Albedo
 0.38 
 0.38 
 0.38 
 0.38 
 0.38 
 0.38 
 0.38 
 0.38 
 0.38 
 0.38 
 0.38 
 0.38 
! Earth infrared flux
 216.5 
 214.75 
 211.76 
 208.06 
 204.93 
 202.96 
 202.7 
 204.18 
 207.11 
 210.66 
 214.1 
 216.23 
3.8000E+26             Solar flux
!
! Energy management
! -----------------
 100                   Initial battery charge
 1.55                  Max charge voltage
 0.001                 Holding current
 100                   Max charge current
 1.05                  Recharge coefficient
 90                    Battery/payload efficiency
 90                    Panels/payload efficiency
 0                     Regulated voltage
 50                    Day requested power
 15                    Day dissipated power
 50                    Night requested power
 15                    Night dissipated power
 0                     Regulated (1) or no regulated (0) voltage
!
! Battery
! -------
 0                     Battery type
 10                    Cells number serial
 1                     Cells number parallel
 10                    Nominal capacity per cell
! Data for charge curve drawing
 0 
 15 
 65 
 120 
 0 
 0.1 
 0.5 
 0.1 
 0 
-5 
-5 
-5 
 1.3 
 1.4 
 1.41 
 1.59 
 0 
-0.001 
-0.001 
-0.002 
 0 
 0.01 
 0.01 
 0.02 
! Data for discharge curve drawing
 0 
 15 
 95 
 100 
 0 
 0.1 
 0.1 
 0 
 0 
-5 
-2 
 0 
 1.3 
 1.21 
 1.2 
 1 
 0 
 0.001 
 0.001 
 0 
 0 
-0.01 
-0.01 
 0 
 1.2                   Influence of discharge current : Cmax/Cnom
 0.6                   Influence of discharge current : C10/Cnom � courant 10C
 30                    Influence of temperature : Cmin % at Tref
 50                    Influence of temperature : Temperature 99.9%
-50                    Influence of temperature : Temperature min
 100                   Autodischarge : time constant at 0� days
 15                    Autodischarge : time constant at 50� days
!
! Thermical data
! --------------
 25                    Panels initial temperature
 0                     Battery temperature
 0                     Absorptivity of body faces +X
 0                     Absorptivity of body faces -X
 0                     Absorptivity of body faces +Y
 0                     Absorptivity of body faces -Y
 0.95                  Absorptivity of body faces +Z
 0.25                  Absorptivity of body faces -Z
 0                     Emissivity of body faces +X
 0                     Emissivity of body faces -X
 0                     Emissivity of body faces +Y
 0                     Emissivity of body faces -Y
 0.9                   Emissivity of body faces +Z
 0.05                  Emissivity of body faces -Z
 50000                 Body specific heat
 25                    Body initial temperature
