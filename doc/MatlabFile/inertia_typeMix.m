clear variables; close all; clc;

%% initial data

format long 

inertia_body = [15 1 1 ;1 15 1;1 1 15];

mass_body = 10;
mass_boom = 2;

mass_pan = [1, 2, 5, 1, 1, 1];  % mass of each panel in order

mass_global = mass_body + mass_boom + sum(mass_pan)

pan_width = [0.8, 0.8, 0.8, 0.8, 0.8, 0.8];
pan_length = [2, 2, 2, 2, 0.8, 0.8];


%% cg calculations

cg_body = [0 0 0];
cg_boom = [5 0 0];
cg_bodyboom = 1 / (mass_boom + mass_body)*(mass_body*cg_body + mass_boom*cg_boom)

cg_pan = [1.4 0 -0.4;
         0 1.4 -0.4;
           -1.4 0 -0.4;
        0 -1.4 -0.4;
            0.4 0 0;
           -0.4 0 0];


cg_global = (1/mass_global)*((mass_body + mass_boom)*cg_bodyboom + mass_pan(1)*cg_pan(1,:) ...
    + mass_pan(2)*cg_pan(2,:) + mass_pan(3)*cg_pan(3,:) + mass_pan(4)*cg_pan(4,:) ...
    + mass_pan(5)*cg_pan(5,:) + mass_pan(6)*cg_pan(6,:))
    
                                                            

%% inertia calculations
% dest-start

% inertia matrix of the body referred to global cg
inertia_body_cg = huygens(inertia_body, mass_body, cg_body, cg_global);

% inertia matrix of the boom referred to global cg
inertia_boom_cg = huygens(zeros(3,3), mass_boom, cg_boom, cg_global);

% inertia body and boom referred to global cg
inertia_bodyboom_cg = inertia_body_cg + inertia_boom_cg


% solar arrays

% inertia matrix pan #1 TYPE 3 +X
inertia_pan1 = zeros(3,3);
inertia_pan1(1,1) = mass_pan(1)*pan_width(1)^2/12;
inertia_pan1(2,2) = mass_pan(1)*pan_length(1)^2/12;
inertia_pan1(3,3) = (mass_pan(1)/12)*(pan_width(1)^2 + pan_length(1)^2);

% inertia matrix of panel 1 referred to global cg   
inertia_pan1_cg = huygens(inertia_pan1, mass_pan(1), cg_pan(1,:), cg_global)

% inertia matrix pan #2  TYPE 3 +Y
inertia_pan2 = zeros(3,3);
inertia_pan2(1,1) = mass_pan(2)*pan_length(2)^2/12;
inertia_pan2(2,2) = mass_pan(2)*pan_width(2)^2/12;
inertia_pan2(3,3) = (mass_pan(2)/12)*(pan_width(2)^2 + pan_length(2)^2);

% inertia matrix of panel 2 referred to global cg   
inertia_pan2_cg = huygens(inertia_pan2, mass_pan(2), cg_pan(2,:), cg_global)

% inertia matrix pan #3  TYPE 3 -X
inertia_pan3 = zeros(3,3);
inertia_pan3(1,1) = mass_pan(3)*pan_width(3)^2/12;
inertia_pan3(2,2) = mass_pan(3)*pan_length(3)^2/12;
inertia_pan3(3,3) = (mass_pan(3)/12)*(pan_width(3)^2 + pan_length(3)^2);

% inertia matrix of panel 3 referred to global cg   
inertia_pan3_cg = huygens(inertia_pan3, mass_pan(3), cg_pan(3,:), cg_global)

% inertia matrix pan #4  TYPE 3 -Y
inertia_pan4 = zeros(3,3);
inertia_pan4(1,1) = mass_pan(4)*pan_length(4)^2/12;
inertia_pan4(2,2) = mass_pan(4)*pan_width(4)^2/12;
inertia_pan4(3,3) = (mass_pan(4)/12)*(pan_width(4)^2 + pan_length(4)^2);

% inertia matrix of panel 4 referred to global cg   
inertia_pan4_cg = huygens(inertia_pan4, mass_pan(4), cg_pan(4,:), cg_global)

% inertia matrix pan #5  TYPE 1 +X
inertia_pan5 = zeros(3,3);
inertia_pan5(1,1) = (mass_pan(5)/12)*(pan_width(5)^2 + pan_length(5)^2);
inertia_pan5(2,2) = mass_pan(5)*pan_width(5)^2/12;
inertia_pan5(3,3) = mass_pan(5)*pan_length(5)^2/12;

% inertia matrix of panel 5 referred to global cg   
inertia_pan5_cg = huygens(inertia_pan5, mass_pan(5), cg_pan(5,:), cg_global)

% inertia matrix pan #6  TYPE 1  -X
inertia_pan6 = zeros(3,3);
inertia_pan6(1,1) = (mass_pan(6)/12)*(pan_width(6)^2 + pan_length(6)^2);
inertia_pan6(2,2) = mass_pan(6)*pan_width(6)^2/12;
inertia_pan6(3,3) = mass_pan(6)*pan_length(6)^2/12;

% inertia matrix of panel 6 referred to global cg   
inertia_pan6_cg = huygens(inertia_pan6, mass_pan(6), cg_pan(6,:), cg_global)

%% global inertia satellite with 4 solar arrays type 3 and boom of 2 kg and 5m away along x axis

inertia_global = inertia_bodyboom_cg + inertia_pan1_cg + inertia_pan2_cg + inertia_pan3_cg + inertia_pan4_cg +inertia_pan5_cg + inertia_pan6_cg


