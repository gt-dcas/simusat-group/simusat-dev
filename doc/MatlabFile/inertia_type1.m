clear variables; close all; clc;

%% initial data

format long 

inertia_body = [15 1 1 ;1 15 1;1 1 15];

mass_body = 10;
mass_boom = 2;
mass_pan = 1;
mass_global = mass_body + mass_boom + mass_pan * 4;

pan_width = 0.8;
pan_length = 0.8;


%% cg calculations

cg_body = [0 0 0]; 
cg_boom = [5 0 0];
cg_bodyboom = 1 / (mass_boom + mass_body)*(mass_body*cg_body + mass_boom*cg_boom);
cg_pan1 = [0.8 0 0];
cg_pan2 = [0 0.8 0];
cg_pan3 = [-0.8 0 0];
cg_pan4 = [0 -0.8 0];
cg_panels = [cg_pan1; cg_pan2; cg_pan3; cg_pan4];

cg_global = 1/mass_global*((mass_body + mass_boom)*cg_bodyboom + mass_pan*(cg_pan1 + cg_pan2 + cg_pan3 + cg_pan4));


%% inertia calcualtions

% dest-start

% inertia matrix of the body referred to global cg
inertia_body_cg = huygens(inertia_body, mass_body, cg_body, cg_global);

% inertia matrix of the boom referred to global cg
inertia_boom_cg = huygens(zeros(3,3), mass_boom, cg_boom, cg_global);

inertia_bodyboom_cg = inertia_body_cg + inertia_boom_cg;



% solar arrays

I_width = mass_pan*pan_length^2/12;
I_length = mass_pan*pan_width^2/12;
I_h = (mass_pan/12)*(pan_width^2 + pan_length^2);

% inertia matrix pan #1
inertia_pan1 = zeros(3,3);
inertia_pan1(1,1) = I_length;
inertia_pan1(2,2) = I_width;
inertia_pan1(3,3) = I_h;

% inertia matrix pan #2
inertia_pan2 = zeros(3,3);
inertia_pan2(1,1) = I_length;
inertia_pan2(2,2) = I_width;
inertia_pan2(3,3) = I_h;

% inertia matrix pan #3
inertia_pan3 = zeros(3,3);
inertia_pan3(1,1) = I_length;
inertia_pan3(2,2) = I_width;
inertia_pan3(3,3) = I_h;

% inertia matrix pan #4
inertia_pan4 = zeros(3,3);
inertia_pan4(1,1) = I_length;
inertia_pan4(2,2) = I_width;
inertia_pan4(3,3) = I_h;


% huygens matrix pan #1    
inertia_pan1_cg = huygens(inertia_pan1, mass_pan, cg_pan1, cg_global);

% huygens matrix pan #2
inertia_pan2_cg = huygens(inertia_pan2, mass_pan, cg_pan2, cg_global);

% huygens matrix pan #3
inertia_pan3_cg = huygens(inertia_pan3, mass_pan, cg_pan3, cg_global);

% huygens matrix pan #4
inertia_pan4_cg = huygens(inertia_pan4, mass_pan, cg_pan4, cg_global);


% global inertia satellite with 4 solar arrays type 2 and boom of 2 kg and 5m away along x axis

inertia_global = inertia_bodyboom_cg + inertia_pan1_cg + inertia_pan2_cg + inertia_pan3_cg + inertia_pan4_cg


