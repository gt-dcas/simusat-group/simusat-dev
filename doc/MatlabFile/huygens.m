function [matrix] = huygens(initialMatrix, mass, startPoint, destPoint )

huyg_matrix = ones(3,3);

huyg_matrix(1,1) = (destPoint(2)-startPoint(2))^2 + (destPoint(3)-startPoint(3))^2;
huyg_matrix(2,2) = (destPoint(1)-startPoint(1))^2 + (destPoint(3)-startPoint(3))^2;
huyg_matrix(3,3) = (destPoint(1)-startPoint(1))^2 + (destPoint(2)-startPoint(2))^2;

huyg_matrix(1,2) = -(destPoint(1)-startPoint(1))*(destPoint(2)-startPoint(2));
huyg_matrix(1,3) = -(destPoint(1)-startPoint(1))*(destPoint(3)-startPoint(3));
huyg_matrix(2,3) = -(destPoint(2)-startPoint(2))*(destPoint(3)-startPoint(3));

huyg_matrix(2,1) = huyg_matrix(1,2);
huyg_matrix(3,1) = huyg_matrix(1,3);
huyg_matrix(3,2) = huyg_matrix(2,3);

matrix = initialMatrix + mass*huyg_matrix;
end

