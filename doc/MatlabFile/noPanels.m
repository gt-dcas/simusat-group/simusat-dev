clear variables; close all; clc;

%% initial data

format long 

inertia_body = [33.33 0 0 ;0 33.33 0;0 0 6.667];

mass_body = 10;
mass_boom = 2;
mass_global = mass_body + mass_boom;


%% cg calculations

cg_body = [0 0 0]; 
cg_boom = [5 0 0];
cg_bodyboom = 1 / (mass_boom + mass_body)*(mass_body*cg_body + mass_boom*cg_boom);

%% inertia calcualtions

% dest-start

% inertia matrix of the body referred to global cg
inertia_body_cg = huygens(inertia_body, mass_body, cg_body, cg_bodyboom);

% inertia matrix of the boom referred to global cg
inertia_boom_cg = huygens(zeros(3,3), mass_boom, cg_boom, cg_bodyboom);

inertia_bodyboom_cg = inertia_body_cg + inertia_boom_cg



