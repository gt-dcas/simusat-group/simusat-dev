clear variables; close all; clc;

%% initial data

format long 

inertia_body = [15 1 1 ;1 15 1;1 1 15];

mass_body = 10;
mass_boom = 2;
mass_pan = 1;
mass_global = mass_body + mass_boom + mass_pan * 4;

pan_width = 0.8;
pan_length = 0.8;


%% cg calculations

cg_body = [0 0 0];
cg_boom = [5 0 0];
cg_bodyboom = 1 / (mass_boom + mass_body)*(mass_body*cg_body + mass_boom*cg_boom);
cg_pan1 = [0.4 0 0];
cg_pan2 = [0 0.4 0];
cg_pan3 = [-0.4 0 0];
cg_pan4 = [0 -0.4 0];
cg_panels = [cg_pan1; cg_pan2; cg_pan3; cg_pan4];

cg_global = 1/mass_global*((mass_body + mass_boom)*cg_bodyboom + mass_pan*(cg_pan1 + cg_pan2 + cg_pan3 + cg_pan4));


%% inertia calcualtions

% dest-start
huyg_matrix1 = ones(3,3);
huyg_matrix1(1,1) = (cg_global(2)-cg_body(2))^2 + (cg_global(3)-cg_body(3))^2;
huyg_matrix1(2,2) = (cg_global(1)-cg_body(1))^2 + (cg_global(3)-cg_body(3))^2;
huyg_matrix1(3,3) = (cg_global(1)-cg_body(1))^2 + (cg_global(2)-cg_body(2))^2;

huyg_matrix1(1,2) = -(cg_global(1)-cg_body(1))*(cg_global(2)-cg_body(2));
huyg_matrix1(1,3) = -(cg_global(1)-cg_body(1))*(cg_global(3)-cg_body(3));
huyg_matrix1(2,3) = -(cg_global(2)-cg_body(2))*(cg_global(3)-cg_body(3));
huyg_matrix1(2,1) = huyg_matrix1(1,2);
huyg_matrix1(3,1) = huyg_matrix1(1,3);
huyg_matrix1(3,2) = huyg_matrix1(2,3);

% inertia matrix of the body referred to global cg
inertia_body_cg = inertia_body + mass_body * huyg_matrix1;


huyg_matrix2 = ones(3,3);
huyg_matrix2(1,1) = (cg_global(2)-cg_boom(2))^2 + (cg_global(3)-cg_boom(3))^2;
huyg_matrix2(2,2) = (cg_global(1)-cg_boom(1))^2 + (cg_global(3)-cg_boom(3))^2;
huyg_matrix2(3,3) = (cg_global(1)-cg_boom(1))^2 + (cg_global(2)-cg_boom(2))^2;

huyg_matrix2(1,2) = -(cg_global(1)-cg_boom(1))*(cg_global(2)-cg_boom(2));
huyg_matrix2(1,3) = -(cg_global(1)-cg_boom(1))*(cg_global(3)-cg_boom(3));
huyg_matrix2(2,3) = -(cg_global(2)-cg_boom(2))*(cg_global(3)-cg_boom(3));
huyg_matrix2(2,1) = huyg_matrix2(1,2);
huyg_matrix2(3,1) = huyg_matrix2(1,3);
huyg_matrix2(3,2) = huyg_matrix2(2,3);

% inertia matrix of the boom referred to global cg
inertia_boom_cg = mass_boom * huyg_matrix2;

inertia_bodyboom_cg = inertia_body_cg + inertia_boom_cg;

% solar arrays

I_width = mass_pan*pan_length^2/12;
I_length = mass_pan*pan_width^2/12;
I_h = (mass_pan/12)*(pan_width^2 + pan_length^2);

% inertia matrix pan #1
inertia_pan1 = zeros(3,3);
inertia_pan1(1,1) = I_h;
inertia_pan1(2,2) = I_width;
inertia_pan1(3,3) = I_length;

% inertia matrix pan #2
inertia_pan2 = zeros(3,3);
inertia_pan2(1,1) = I_width;
inertia_pan2(2,2) = I_h;
inertia_pan2(3,3) = I_length;

% inertia matrix pan #3
inertia_pan3 = zeros(3,3);
inertia_pan3(1,1) = I_h;
inertia_pan3(2,2) = I_width;
inertia_pan3(3,3) = I_length;

% inertia matrix pan #4
inertia_pan4 = zeros(3,3);
inertia_pan4(1,1) = I_width;
inertia_pan4(2,2) = I_h;
inertia_pan4(3,3) = I_length;


% huygens matrix pan #1    
huyg_pan_matrix1 = ones(3,3);
huyg_pan_matrix1(1,1) = (cg_global(2)-cg_pan1(2))^2 + (cg_global(3)-cg_pan1(3))^2;
huyg_pan_matrix1(2,2) = (cg_global(1)-cg_pan1(1))^2 + (cg_global(3)-cg_pan1(3))^2;
huyg_pan_matrix1(3,3) = (cg_global(1)-cg_pan1(1))^2 + (cg_global(2)-cg_pan1(2))^2;

huyg_pan_matrix1(1,2) = -(cg_global(1)-cg_pan1(1))*(cg_global(2)-cg_pan1(2));
huyg_pan_matrix1(1,3) = -(cg_global(1)-cg_pan1(1))*(cg_global(3)-cg_pan1(3));
huyg_pan_matrix1(2,3) = -(cg_global(2)-cg_pan1(2))*(cg_global(3)-cg_pan1(3));
huyg_pan_matrix1(2,1) = huyg_pan_matrix1(1,2);
huyg_pan_matrix1(3,1) = huyg_pan_matrix1(1,3);
huyg_pan_matrix1(3,2) = huyg_pan_matrix1(2,3);

inertia_pan1_cg = inertia_pan1 + mass_pan*huyg_pan_matrix1;

% huygens matrix pan #2
huyg_pan_matrix2 = ones(3,3);
huyg_pan_matrix2(1,1) = (cg_global(2)-cg_pan2(2))^2 + (cg_global(3)-cg_pan2(3))^2;
huyg_pan_matrix2(2,2) = (cg_global(1)-cg_pan2(1))^2 + (cg_global(3)-cg_pan2(3))^2;
huyg_pan_matrix2(3,3) = (cg_global(1)-cg_pan2(1))^2 + (cg_global(2)-cg_pan2(2))^2;

huyg_pan_matrix2(1,2) = -(cg_global(1)-cg_pan2(1))*(cg_global(2)-cg_pan2(2));
huyg_pan_matrix2(1,3) = -(cg_global(1)-cg_pan2(1))*(cg_global(3)-cg_pan2(3));
huyg_pan_matrix2(2,3) = -(cg_global(2)-cg_pan2(2))*(cg_global(3)-cg_pan2(3));
huyg_pan_matrix2(2,1) = huyg_pan_matrix2(1,2);
huyg_pan_matrix2(3,1) = huyg_pan_matrix2(1,3);
huyg_pan_matrix2(3,2) = huyg_pan_matrix2(2,3);

inertia_pan2_cg = inertia_pan2 + mass_pan*huyg_pan_matrix2;

% huygens matrix pan #3
huyg_pan_matrix3 = ones(3,3);
huyg_pan_matrix3(1,1) = (cg_global(2)-cg_pan3(2))^2 + (cg_global(3)-cg_pan3(3))^2;
huyg_pan_matrix3(2,2) = (cg_global(1)-cg_pan3(1))^2 + (cg_global(3)-cg_pan3(3))^2;
huyg_pan_matrix3(3,3) = (cg_global(1)-cg_pan3(1))^2 + (cg_global(2)-cg_pan3(2))^2;

huyg_pan_matrix3(1,2) = -(cg_global(1)-cg_pan3(1))*(cg_global(2)-cg_pan3(2));
huyg_pan_matrix3(1,3) = -(cg_global(1)-cg_pan3(1))*(cg_global(3)-cg_pan3(3));
huyg_pan_matrix3(2,3) = -(cg_global(2)-cg_pan3(2))*(cg_global(3)-cg_pan3(3));
huyg_pan_matrix3(2,1) = huyg_pan_matrix3(1,2);
huyg_pan_matrix3(3,1) = huyg_pan_matrix3(1,3);
huyg_pan_matrix3(3,2) = huyg_pan_matrix3(2,3);

inertia_pan3_cg = inertia_pan3 + mass_pan*huyg_pan_matrix3;

% huygens matrix pan #4
huyg_pan_matrix4 = ones(3,3);
huyg_pan_matrix4(1,1) = (cg_global(2)-cg_pan4(2))^2 + (cg_global(3)-cg_pan4(3))^2;
huyg_pan_matrix4(2,2) = (cg_global(1)-cg_pan4(1))^2 + (cg_global(3)-cg_pan4(3))^2;
huyg_pan_matrix4(3,3) = (cg_global(1)-cg_pan4(1))^2 + (cg_global(2)-cg_pan4(2))^2;

huyg_pan_matrix4(1,2) = -(cg_global(1)-cg_pan4(1))*(cg_global(2)-cg_pan4(2));
huyg_pan_matrix4(1,3) = -(cg_global(1)-cg_pan4(1))*(cg_global(3)-cg_pan4(3));
huyg_pan_matrix4(2,3) = -(cg_global(2)-cg_pan4(2))*(cg_global(3)-cg_pan4(3));
huyg_pan_matrix4(2,1) = huyg_pan_matrix4(1,2);
huyg_pan_matrix4(3,1) = huyg_pan_matrix4(1,3);
huyg_pan_matrix4(3,2) = huyg_pan_matrix4(2,3);

inertia_pan4_cg = inertia_pan4 + mass_pan*huyg_pan_matrix4;


%% global inertia satellite with 4 solar arrays type 2 and boom of 2 kg and 5m away along x axis

inertia_global = inertia_bodyboom_cg + inertia_pan1_cg + inertia_pan2_cg + inertia_pan3_cg + inertia_pan4_cg





























