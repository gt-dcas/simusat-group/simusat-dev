# **Simusat**
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

This is the **main README** for the *Simusat project*.

*author:* Département Conception et Conduit des véhicules Aéronautiques et Spatiaux (DCAS), ISAE-SUPAERO.

*latest version:* **Simusat-v6.4.4**

**Simusat project organisation**

The git repository of the present project is divided into three main parts :
  - doc :
   
     Here you will find the documentation of the developers as well as rapports, quickstart tutorial, examples, etc. 


  - setup :
   
      In this part you will find the installers of the different available versions of the software as well as the standalone version.

  - src : 
   
      In this part you will find the source code of the project and its respective dependencies. 

## **Introduction**

Simusat is a student-focused, research and teaching simulation software for pre-design of space missions that allows to calculate the thermal behaviour, attitude control (ADCS) and power management (EPS) of a space system.


![Simusat image](./src/ImgMainReadme.png)


## **Table of Contents**
<!-- TOC -->

- [**Simusat**](#simusat)
  - [**Introduction**](#introduction)
  - [**Table of Contents**](#table-of-contents)
  - [**Users installation procedure**](#users-installation-procedure)
    - [Download](#download)
  - [**Developers installation procedure**](#developers-installation-procedure)
    - [Download](#download-1)
    - [Visual Studio 2019 configuration](#visual-studio-2019-configuration)
    - [Setup procedure](#setup-procedure)
      - [If the Setup is already in the project](#if-the-setup-is-already-in-the-project)
      - [If the Setup is not in the project](#if-the-setup-is-not-in-the-project)
      - [How the ToolsVersion attribute works](#how-the-toolsversion-attribute-works)
  - [**Run Simusat in the Linux environment**](#run-simusat-in-the-linux-environment)
    - [WineHQ](#winehq)
      - [WineHQ installation procedure](#winehq-installation-procedure)
      - [Winetricks](#winetricks)
    - [Installing the libraries](#installing-the-libraries)
    - [Run the standalone version](#run-the-standalone-version)
  - [**Information about the unit tests**](#information-about-the-unit-tests)
    - [Procedure for unit test development](#procedure-for-unit-test-development)
  - [**To do**](#to-do)

<!-- /TOC -->

## **Users installation procedure**

The software need to be usable preferably only in any Windows
computer. In order to do so, two different versions of the software are available: a **standalone version** and an **installable version**. Both versions contain the same software but differ on the way the software is run in the computer.

It is possible to use the software in Linux platform but you should configure the environment where the programme will run. For this go to [Run Simusat in the Linux environment](#run-simusat-in-the-linux-environment).

### Download
In this section you can download the available versions of Simusat software :
 - **Standalone version**
  
    To download this version click here : [Simusat standalone](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/archive/master/simusat-dev-master.zip?path=src/bin)

 - **Installable version**
  
   To download this version click here : [Simusat installer](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/raw/master/setup/SetupSimusat/Release/SetupSimusat.exe)

## **Developers installation procedure**


To run the version of Simusat contained in the *Git repository* the following program is used :
  - Visual Studio 2019 (VS2019) on Windows 10 platform.
### Download

First, we need to clone the git repository that contains the **Simusat project** :

 ```bash
git clone https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev
```

### Visual Studio 2019 configuration

This step is necessary in order to avoid compilation errors and the subsequent execution of the code without problems. 

Once inside the repository it is necessary to execute the `SimuSCA.sln` file (see file location [here](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/tree/master/src)). Then you have to go on toolbar, 
  - Click Project/SimuSCA Properties/Application 
  - Choose Target framework `.NET Framework 4.7`
  - In the same tab go to *References* and make sur that all necessary libraries are installed (see [library installation tutorial](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/doc/DOC_Simusat/Installation_bibliotheques.pdf)).
  - If the libraries are not installed, you must add them. 
    - `Tao.OpenGl.dll` and 
`Tao.Platform.Windows.dll` libraries : 

      Some programs will want to download an older version of the .NET Framework, but this doesn’t work. You still need to enable it. For this you have to :

       - On Windows 10, hit the Windows key, type: *control panel*, and choose the top result.
       - From the Control Panel, click Programs and Features.
       - When the Programs and Features window comes up, click *Turn Windows Features On or Off*.
       - Then check .NET Framework 3.5 (includes .NET 2.0 and 3.0) and click OK.
       - Next, you’ll need to download files from Windows Update.
       - Then you have to install the necessary dependencies by going to the Git repository,( see [dependencies](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/tree/master/src/dependencies) ). Precisely `taoframework-2.1.0-Setup.exe`
       - Finally the libraries are installed generally in the next path `Program Files (x86)\TaoFramework\bin\`. You must add them by clicking on `add/Browse...` and then follow the path above to find them.
  - As the framework version `.NET Famework 4.7`, it is possible that there are some syntax problems with respect to previous versions, so the following modifications will have to be done. 
      - Before modification : `xlApp = New Excel.ApplicationClass` 
      - After modification : `xlApp = New Excel.Application`
   
    The modifications are the same in the next files :
      - [frmReportPwr.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/src/frmReportPwr.vb) : line 363
      - [frmReportAtt.vb](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/src/frmReportAtt.vb) : line 277

### Setup procedure
The purpose of this part is to summarize the *Setup* process and to evacuate possible problems that may occur. 

#### If the Setup is already in the project
In this case you have to add the **Setup** file at the current project :
  - First of all, find the folder where the setup is stored and inedtify the `SetupProject.vdproj` file. In thise case `SetupSimusat.vdproj`.
  - Next, run the current project; open the `SimuSCA.sln` file.
  - Go to *Solution Explorer* and click right over the solution; then *Add/Existing Project/* and find the `.vdproj` file cited in the first step.
  - Finally, you should see the *Setup project*  added to the current solution. See the *Solution Explorer*.

#### If the Setup is not in the project
- The first step is add the extention *Microsoft Visual Studio Installer Projects* to Visual Studio (VS). 
    - On Visual Studio click on *Extensions/Manage Extensions/*.
    - Then,  in the emerging tab search *Microsoft Visual Studio Installer Projects* and download it.
    - Next, you need restart VS.
- Back in the project, you have to add a new project from the current solution. For this click on *Solution 'Project'/add/New project.../*.
- From the emerging tab ('Add a new project') search for *Setup* and choose te first option : *Setup project*.
- Next, follow the steps up to the creation of the *Setup solution*. See the *Solution Explorer*.
- Finally, on the screen you will see three folders: Application Folder, User's Desktop and User's Programs Menu : 
  - Inside  *Application Folder*, click right and choose *Project Output...* . A pop-up window will appear and choose *Primary Output* between the options. A file with the name *Primary output from 'Project' (Active)* should have been created.
     -  Also you should see the `.dll` libraries linked to the current project if not you have to add them :
        - Go to the [bin](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/tree/master/src/bin) folder and copy and paste the files `.dll` in this folder (Application Folder).
    - In the same folder(Application Folder) you have to add all de folders that appear on [bin](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/tree/master/src/bin) folder : *Data, Quickstart and examples,  es, resources*.
  - Click right on *Primary output from 'Project' (Active)* to create our *Shortcut* file. You have the possibility to edit it by going to properties.
  - Copy this *Shortcut* file into the *User's Desktop* folder.
  - Into *User's Programs Menu* folder create another folder which is it the folder application for our programs menu. Create and copy a new *Shortcut* from *Application Folder*.
- Once the *Shortcut* files were copied to *User's Desktop* and  *User's Programs Menu/another folder*, click right on *Setup solution* and go to *Properties*.  A pop-up window will appear called *StorageSetup Property Pages*, then you must configure :
  - **Prerequisites...**: be sure to select the framework option on which the project runs, in this case `.NET Famework 4.7`. It will be downloaded and installed if it is not on the new installation platform when the software installation (Simusat) process takes place.
  - **Configuration Manager**: go to the setup project file, click on the configuration column and change **Debug** by **Release**.
  - Set the changes and close the window. 
- Finally, you have to build the solution. Click right on *Setup solution* and chose *Build* to initialize the setup creation process.



If the creation of the *Setup* file has failed check the `ToolsVersion` attribute on the Project element in the project file, in this case `SimuSCA.vbproj`. You should see : `<Project DefaultTargets="Build" ToolsVersion="4.0" xmlns="schemas.microsoft.com/developer/msbuild/2003">`.
  - If the attribute is different  from `"3.5"` or `"4.0"`, add one of these values.
  - Then build the *Setup Solution*.

For more details see [Tutorial Setup](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/blob/master/doc/DOC_Simusat/Angel2019/TutorialSetup/Creating%20Installer%20(setup)%20tutorial.pdf)  and/or [How to Create a Setup file in Visual Studio 2019](https://www.youtube.com/watch?v=7iVIfkVHKII).

Attention!. If you have done all the above steps and the *Setup* has not succeeded and a message like this appears : `ERROR: An error occurred while validating. HRESULT = '80070005'`. Try updating your operating system or restart your computer.

For more details above this error see the forum [stackoverflow.com](https://stackoverflow.com/questions/986224/why-do-i-get-an-error-occurred-while-validating-hresult-80004005-when-bui).

#### How the ToolsVersion attribute works

When you create a project in Visual Studio, or upgrade an existing project, an attribute named `ToolsVersion` is automatically included in the project file and its value corresponds to the version of MSBuild that is included in the Visual Studio edition. 

When a `ToolsVersion` value is defined in a project file, MSBuild uses that value to determine the values of the Toolset properties that are available to the project. One Toolset property is `$(MSBuildToolsPath)`, which specifies the path of the .NETFramework tools. Only that Toolset property (or `(MSBuildToolsPath)`), is required. For more information, see [Framework targeting overview](https://docs.microsoft.com/en-gb/visualstudio/ide/visual-studio-multi-targeting-overview?view=vs-2022).


## **Run Simusat in the Linux environment**

 The purpose of this section is to run Simusat in the *Linux environment* thanks to **WineHQ**. For this and in the next stages to be developed, the following working environment is the Ubuntu distribution, specifically `Ubuntu 18.04 LTS`.

### WineHQ
*WineHQ* is a compatibility layer capable of running Windows applications on several POSIX-compliant operating systems, such as Linux, macOS, & BSD. Wine translates Windows API calls into POSIX calls on-the-fly, eliminating the performance and memory penalties of other methods and allowing you to cleanly integrate Windows applications into your desktop.

#### WineHQ installation procedure
If you have previously installed a Wine package from another repository, please remove it and any packages that depend on it (e.g., wine-mono, wine-gecko, winetricks) before attempting to install the WineHQ packages, as they may cause dependency conflicts.

If your system is 64 bit, enable 32 bit architecture (if you haven't already): 
```bash
sudo dpkg --add-architecture i386 
```
Download and add the repository key: 
```bash
wget -O - https://dl.winehq.org/wine-builds/winehq.key | sudo apt-key add -
```
 Add wine repository:
 ```bash
sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ bionic main' apt-key #Note there’s A BLANK SPACE before the code name (e.g., focal, groovy, hirsute, bionic) in each command!
```
For **Ubuntu 18.04** and **Linux Mint 19.x** only, `libfaudio0` library is required to install from a third-party repository by running command:
```bash
sudo add-apt-repository ppa:cybermax-dexter/sdl2-backport
```

Update packages: 
```bash
sudo apt-get update
```

 Finally install the stable branch of Wine:
 ```bash
sudo apt install --install-recommends winehq-stable
```
If you get unmet dependency issue, try aptitude command instead (DO check what it is going to install/uninstall before type y to confirm):
 ```bash
sudo apt install aptitude && sudo aptitude install winehq-stable
```
For more information see the  [FAQ entry on dependency errors](https://wiki.winehq.org/FAQ#How_do_I_solve_dependency_errors_when_trying_to_install_Wine.3F) for tips on troubleshooting dependency issues.

For more details about the installation procedure see [winehq.org](https://wiki.winehq.org/Ubuntu).

#### Winetricks

*Winetricks* is a helper script to download and install various redistributable runtime libraries needed to run some programs in Wine. These may include replacements for components of Wine using closed source libraries. 

**Note:** Some of the packages listed below may not work well with older versions of Wine. As always we recommend you use the [latest version of Wine](https://wiki.winehq.org/Download).

**Installing winetricks**

To download and install the script, you can do it manually like this: 
 ```bash
wget  https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
chmod +x winetricks
sudo cp winetricks /usr/local/bin
```

**Using winetricks**

Once you've obtained winetricks you can run it simply by typing the code as you see on the console. You can also use `./winetricks` if you `chmod +x winetricks` first. If run without parameters, winetricks displays a GUI with a list of available packages. If you know the names of the package(s) you wish to install, you can append them to the winetricks command and it will immediately start the installation process.
 ```bash
sh winetricks
```

For more details about *Winetricks* see [winehq.org](https://wiki.winehq.org/Winetricks.). 

### Installing the libraries

Once you've installed *WineHQ* and *Winetricks*, it is necessary to configure the *Windows* environment and for this we must install the libraries on which the code is based.

By running the *Winetricks* script, you should see a GUI with a list of available options, select '*Select the default wineprefix*'. 
- A new displays appears; select '*Install a Windows DLL or component*' and accept it.
  - A pop-up screen with a list of installables packages appears. Chose the next ones: ***dotnet35, dotnet35, dotnet40, dotnet45, dotnet46, dotnet461, dotnet462, dotnet471***, then accepts to start with the installation procedure.
- Back to the screen (in which we choose '*Select the default wineprefix*') chose *'Browses files'*, a window with the *Wine directory* appears. You should see the 'drive_c' folder, enter there.
  - In this folder copy and page the `taoframework-2.1.0-setup.exe` file. Finally, run it. To get the file see [Visual Studio 2019 configuration](#visual-studio-2019-configuration).
- On the same screen as the previous step, select *'Run winecfg'*. You should see the *Wine Configuration menu*:
  - Go to *Libraries*: click on *New override for library:* and add the **mscoree** and **mscorwks** libraries. Then apply and accept the changes made.

Finally, you must do a systems update for the changes to take effect.

### Run the standalone version

In your desktop copy and page the folder that contains the standalone version of Simusat, in other words copy the **bin** folder of the project. Inside the bin folder you have to run the `Simusat.exe` application to start the program.

## **Information about the unit tests**

The unit tests are used to define and run tests to maintain code health, ensure code coverage, and find errors and faults before the users does it. It is necessary to run the unit tests frequently to make sure the code is working properly. 

To do this type of test we use the Visual Studio (VS2019) environnement. It is necessary to mention that **all resources are installed and available** in the Visual Studio package.

### Procedure for unit test development

Here are the main steps for the creation of the file where the test takes place :

- First of all, run the current project; open the `SimuSCA.sln` file.
- Go to *Solution Explorer* and click right over the solution; then *Add/New Project.../* .
- In the toolbar search *Unit Test Project (.NET Framework)*. Then follow the steps up to the creation of the *Unit Test*.
  - It is necessary to take into account that the `Target framework` version is the same at the time of creating the unit test as well as that of the *vb projects*. See [Visual Studio 2019 configuration](#visual-studio-2019-configuration).
- Finally, you should see the *Unit Test* added to the current solution. See the *Solution Explorer*.
- Now you can start coding your test.

## **To do**
Some progress has been made with respect to parser *yaml* files

[**Nanospace link**](https://gitlab.isae-supaero.fr/creme-project/creme-scripts/-/tree/Stage_2022_Victor/)


# troubleshooting


## Current ISAE INSTALL: 

### Sous windows:

- Laisser tous les paramètres par défaut et suivre les instructions ("install only for me" pour éviter les problèmes de droits d'administration)

```
C:\CAS\simusat_install\setup.exe
```

- L'installation se fait sous D:\CAS, un raccourci est créé sur le bureau (utilisable en local)
- Simusat (VB6 - V03) est ensuite utilisable normalement...

